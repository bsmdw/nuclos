package org.nuclos.common.dblayer;

import java.sql.ResultSetMetaData;

import org.nuclos.common.collection.Transformer;

public interface ResultSetMetaDataTransformer<T> extends Transformer<ResultSetMetaData, T> {
	
}
