//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.collection.Transformer;

public final class RigidUtils {

	private RigidUtils() {}

	public static final Pattern PARAM_PATTERN_$ = Pattern.compile("\\$\\{([^}]*)\\}");
	public static final Pattern PARAM_PATTERN_UID = Pattern.compile ("(uid[{][\\w|\\-\\[\\]]+[}])");

	/**
	 * checks if o1 equals o2, allowing <code>null</code> for both arguments.
	 * Note that this method doesn't work for arrays currently.
	 * 
	 * §postcondition (o1 == null &amp;&amp; o2 == null) --&gt; result
	 * §postcondition (o1 != null) --&gt; (result &lt;--&gt; o1.equals(o2))
	 * 
	 * @param o1
	 *            may be <code>null</code>
	 * @param o2
	 *            may be <code>null</code>
	 */
	public static boolean equal(Object o1, Object o2) {
		return (o1 == null) ? (o2 == null) : o1.equals(o2);
	}

	/**
	 * Note that this method doesn't work for arrays currently.
	 * 
	 * @param o
	 *            may be <code>null</code>
	 * @return hash code for o, as in <code>Object.hashCode()</code>
	 */
	public static int hashCode(Object o) {
		return (o == null) ? 0 : o.hashCode();
	}

	/**
	 * §postcondition (tValue != null) --&gt; (result == tValue)
	 * §postcondition (tValue == null) --&gt; (result == tDefault)
	 * 
	 * @param tValue
	 * @param tDefault
	 * @return tValue if <code>tValue != null</code>. <code>tDefault</code>
	 *         otherwise.
	 */
	public static <T> T defaultIfNull(T tValue, T tDefault) {
		return (tValue != null) ? tValue : tDefault;
	}

	/**
	 * §postcondition !"".equals(result)
	 * §postcondition ("".equals(s)) --&gt; (result == null)
	 * §postcondition (!"".equals(s)) --&gt; (result == s)
	 * 
	 * @param s
	 * @return <code>null</code>, if <code>s</code> is empty. <code>s</code>
	 *         otherwise
	 */
	public static String nullIfEmpty(String s) {
		final String result = (s != null && s.length() > 0) ? s : null;

		assert !"".equals(result);
		assert !("".equals(s)) || (result == null);
		assert !(!"".equals(s)) || (result == s);

		return result;
	}

	/**
	 * §postcondition (s == null) || (s.length() == 0) --&gt; result
	 * 
	 * @param s
	 * @return Does the given String look empty? Is it <code>null</code> or does
	 *         it contain only whitespace?
	 */
	public static boolean looksEmpty(String s) {
		final boolean result = (s == null)
				|| (org.apache.commons.lang.StringUtils.deleteWhitespace(s)
						.length() == 0);

		assert !((s == null) || (s.length() == 0)) || result;

		return result;
	}
	
	public static boolean hasOnlyAsciiLettersOrNumbers(String s) {
		return s.matches("^[a-zA-Z0-9]*$");
	}

	/**
	 * compares two objects. If the objects are not comparable, they are
	 * compared based on <code>toString()</code>.
	 * 
	 * @param o1
	 *            may be <code>null</code>
	 * @param o2
	 *            may be <code>null</code>
	 */
	public static int compare(Object o1, Object o2) {
		if (o1 == o2) {
			// performance shortcut, esp. for o1 == o2 == null.
			return 0;
		} else if (o1 instanceof Comparable) {
			return compareComparables((Comparable) o1, (Comparable) o2);
		} else {
			return compareBasedOnToString(o1, o2);
		}
	}

	/**
	 * Compares two <code>Comparables</code>, that may be <code>null</code>.
	 * <code>null</code> is less than all other values.
	 * 
	 * @param t1
	 *            may be <code>null</code>
	 * @param t2
	 *            may be <code>null</code>
	 */
	public static <T extends Comparable<T>> int compareComparables(T t1, T t2) {
		final int result;

		if (t1 == null) {
			result = (t2 == null) ? 0 : -1;
		} else if (t2 == null) {
			result = 1;
		} else {
			result = t1.compareTo(t2);
		}
		return result;
	}

	/**
	 * Compares two <code>Objects</code>, that may be <code>null</code>.
	 * <code>null</code> is less than all values. If both objects are not
	 * <code>null</code>, they are compared based on their string
	 * representations, according to <code>toString()</code>.
	 * 
	 * @param o1
	 *            may be <code>null</code>
	 * @param o2
	 *            may be <code>null</code>
	 */
	public static int compareBasedOnToString(Object o1, Object o2) {
		final int result;

		if (o1 == null) {
			result = (o2 == null) ? 0 : -1;
		} else if (o2 == null) {
			result = 1;
		} else {
			result = o1.toString().compareTo(o2.toString());
		}
		return result;
	}

	/**
	 * Get all parameters in placeholder format <code>${...}</code>.
	 * 
	 * @param s
	 *            the input string
	 * @return list of parameters in input string
	 */
	public static List<String> getParameters(Pattern pattern, String s) {
		List<String> result = new ArrayList<String>();
		Matcher m = pattern.matcher(s);
		while (m.find()) {
			result.add(m.group(1));
		}
		return result;
	}

	/**
	 * Replaces all embedded parameters of the form <code>${...}</code>. The
	 * replacement string is determined by calling a given transformer.
	 * 
	 * @param s
	 *            the input string
	 * @param t
	 *            the transformer which maps a parameter to its replacement
	 * @return the given string with all parameters replaced
	 */
	public static String replaceParameters(Pattern pattern, String s, Transformer<String, String> t) {
		if (s == null) {
			return null;
		}
		StringBuffer sb = new StringBuffer();
		Matcher m = pattern.matcher(s);
		while (m.find()) {
			String resId = m.group(1);
			String repString = t.transform(resId);
			m.appendReplacement(sb, Matcher.quoteReplacement(repString));
		}
		m.appendTail(sb);
		return sb.toString();
	}
	public static String replaceParametersRecursively(String text, final Properties props) {
		return replaceParametersRecursively(PARAM_PATTERN_$, text, props);
	}
	public static String replaceParametersRecursively(final Pattern pattern, String text, final Properties props) {
		return replaceParameters(pattern, text, new Transformer<String, String>() {
			LinkedList<String> nesting = new LinkedList<String>();

			@Override
			public String transform(String p) {
				if (nesting.contains(p))
					throw new IllegalArgumentException(
							"Recursive property definition for "
									+ nesting.getFirst());
				String rtext = props.getProperty(p);
				if (rtext == null)
					throw new IllegalArgumentException("Missing property " + p);
				nesting.push(p);
				String r = replaceParameters(pattern, rtext, this);
				nesting.pop();
				return r;
			}
		});
	}

	public static <T> T getSingleIfExist(Iterable<T> iterable) {
		if (iterable == null)
			return null;
		final Iterator<? extends T> iter = iterable.iterator();
		if (!iter.hasNext())
			return null;
		final T result = iter.next();
		if (iter.hasNext()) {
			throw new IllegalArgumentException(
					"More than (expected) one element in " + iterable);
		}
		return result;
	}

	public static <T> T getFirst(Iterable<? extends T> iterable, T def) {
		final Iterator<? extends T> iter = iterable.iterator();
		return iter.hasNext() ? iter.next() : def;
	}
	
	/**
	 * 
	 * @param values
	 * @return first not null element.
	 * 			If values is null or all elements are null, returns null.
	 */
	public static <T> T firstNonNull(T...values) {
		for (T t : values) {
			if (t != null)
				return t;
		}
		return null;
	}

	/**
	 * transforms the given collection into a new list by iterating it and
	 * performing the given <code>transformer</code> on each element, then
	 * adding the transformer's output to the result list.
	 * <p>
	 * This function is called "map" in functional languages like Haskell.
	 * <p>
	 * Concerning the name of this method: "map" would be misleading, as a map
	 * is something different in Java. "transformed" would have also been okay,
	 * indicating that this method is side-effect free, esp. that it doesn't
	 * change the input collection, but "transform" seems a less awkward name.
	 * Transforming a list in place isn't as common anyway, so
	 * "transformInPlace" is used for that case. The old name of this method
	 * "collect" was adapted from the Apache Commons CollectionUtils, but
	 * "collect" is already used in the sense of "(Daten) erfassen" in the
	 * Novabit common lib, and it doesn't explain what the method really does:
	 * transforming a collection into another.
	 * 
	 * §precondition coll != null
	 * §precondition transformer != null
	 * §postcondition result != null
	 * §postcondition result != coll
	 * 
	 * @param coll
	 *            isn't changed. (Note that generics and transforming a
	 *            Collection in place don't quite fit together!)
	 * @param transformer
	 * @return a new transformed list resulting from performing the
	 *         <code>transformer</code> on each element of <code>coll</code>.
	 */
	public static <I, O> List<O> transform(Iterable<I> coll,
			Transformer<? super I, O> transformer) {
		return transform(coll, transformer, null);
	}

	public static <I, O> List<O> transform(Iterable<I> coll,
			Transformer<? super I, O> transformer,
			Predicate<? super O> afterTransformPredicate) {
		final List<O> result = new ArrayList<O>(
				coll instanceof Collection ? size((Collection<?>) coll) : 10);
		if (coll != null) {
			for (I i : coll) {
				O transformedValue = transformer.transform(i);
				if (afterTransformPredicate == null
						|| afterTransformPredicate.evaluate(transformedValue)) {
					result.add(transformedValue);
				}
			}
		}
		assert result != null;
		assert result != coll;
		return result;
	}

	/**
	 * §postcondition (coll == null) --&gt; (result == 0)
	 * §postcondition (coll != null) --&gt; (result == coll.size())
	 */
	public static int size(Collection<?> coll) {
		return (coll == null) ? 0 : coll.size();
	}

	/**
	 * Returns a new list with {@code t1} as first element followed by all
	 * elements of {@code ts}.
	 */
	public static <T> List<T> asList(T t1, T... ts) {
		List<T> list = new ArrayList<T>(1 + ts.length);
		list.add(t1);
		for (T t : ts)
			list.add(t);
		return list;
	}

	/**
	 * §precondition coll != null
	 * §postcondition result != null
	 * §postcondition result != coll
	 * 
	 * @param coll
	 * @param comparator
	 *            Determines the ordering. If <code>null</code>, the natural
	 *            ordering of the elements is used.
	 * @return a new sorted List containing the elements of the given
	 *         Collection.
	 */
	public static <E> ArrayList<E> sorted(Collection<E> coll,
			Comparator<? super E> comparator) {
		final ArrayList<E> result = new ArrayList<E>(coll);
		Collections.sort(result, comparator);
		assert result != null;
		assert result != coll;
		return result;
	}

	public static <T> List<T> newOneElementArrayList(T t) {
		final List<T> result = new ArrayList<T>(1);
		result.add(t);
		return result;
	}

	public static <T> List<T> newOneElementLinkedList(T t) {
		final List<T> result = new LinkedList<T>();
		result.add(t);
		return result;
	}

	/**
	 * §postcondition date != null &lt;--&gt; result != null
	 * §postcondition date != null --&gt; result.getHours() == 0
	 * §postcondition date != null --&gt; result.getMinutes() == 0
	 * §postcondition date != null --&gt; result.getSeconds() == 0
	 * §todo consider calling this method getDay(). On the other hand, that name
	 *       could be confused with Date.getDay()...
	 *       
	 * @param date
	 *            is not changed by this method. May be <code>null</code>.
	 * @return the pure date (aka "day", that is the date without time of day)
	 *         of the given Date object (if any).
	 */
	public static Date getPureDate(Date date) {
		final Date result = (date == null) ? null
				: org.apache.commons.lang.time.DateUtils.truncate(date,
						Calendar.DATE);
		assert (date != null) == (result != null);

		assert (date == null) || result.getHours() == 0;
		assert (date == null) || result.getMinutes() == 0;
		assert (date == null) || result.getSeconds() == 0;
		return result;
	}

	public static <T> Iterable<T> replicate(final T value, final int count) {
		return new Iterable<T>() {
			@Override
			public Iterator<T> iterator() {
				return new Iterator<T>() {
					int remaining = count;

					@Override
					public boolean hasNext() {
						return remaining > 0;
					}

					@Override
					public T next() {
						if (remaining-- <= 0)
							throw new NoSuchElementException();
						return value;
					}

				    // default implementation in java8 @Override 
					public void remove() {
						throw new UnsupportedOperationException();
					}
				};
			}
		};
	}

	/**
	 * Tests whether obj is an instance of at least one of the given
	 * classes/interfaces.
	 */
	public static boolean isInstanceOf(Object obj, Class<?>... classes) {
		for (Class<?> clazz : classes) {
			if (clazz.isInstance(obj))
				return true;
		}
		return false;
	}

	/**
	 * concatenates the given Collections into one Collection.
	 * 
	 * @return the elements of all given Collections concatenated into one
	 *         Collection.
	 */
	public static <E> List<E> concat(Collection<? extends E> a,
			Collection<? extends E> b) {
		ArrayList<Collection<? extends E>> tmp = new ArrayList<Collection<? extends E>>(
				2);
		tmp.add(a);
		tmp.add(b);
		return concatAll(tmp);
	}

	/**
	 * concatenates the given Collections to one Collection.
	 * 
	 * §postcondition result != null
	 * 
	 * @param iterable
	 *            iterates thru the collections to be merged into one.
	 * @return the elements of all given Collections concatenated into one
	 *         Collection.
	 */
	public static <E> List<E> concatAll(
			Iterable<? extends Collection<? extends E>> iterable) {
		final List<E> result = new ArrayList<E>();
		for (Collection<? extends E> coll : iterable) {
			result.addAll(coll);
		}
		assert result != null;
		return result;
	}

	/**
	 * Filter an "in"-collection using a given predicate and filling the "out"
	 * collection. The method adds all matching elements of in to out
	 * 
	 * @param in
	 *            collection
	 * @param filter
	 *            the filter
	 * @param out
	 *            collection
	 * @return out
	 */
	private static <T, C extends Collection<T>> C applyFilter(
			Collection<? extends T> in, Predicate<? super T> filter, C out) {
		for (T t : in)
			if (filter.evaluate(t))
				out.add(t);
		return out;
	}

	/**
	 * Return a list of all elements of in that satisfy filter
	 * 
	 * @param in
	 *            the collection
	 * @param filter
	 *            the predicate
	 * @return a list of matches
	 */
	public static <T> List<T> applyFilter(Collection<? extends T> in,
			Predicate<? super T> filter) {
		return in == null ? null : applyFilter(in, filter, new ArrayList<T>());
	}

	public static <T> Comparator<T> byClassComparator(
			Class<? extends T>... classes
	) {
		final List<Class<? extends T>> list = Arrays.asList(classes);

		return new Comparator<T>() {
			@Override
			public int compare(T o1, T o2) {
				checkKnownClasses(o1, o2);

				return list.indexOf(o1.getClass()) - list.indexOf(o2.getClass());
			}

			/**
			 * Makes sure the comparator knows all classes, to prevent violations of the Comparator contract!
			 *
			 * @param objectsToCompare
			 */
			protected void checkKnownClasses(T... objectsToCompare) {
				for (T o : objectsToCompare) {
					if (!list.contains(o.getClass())) {
						throw new IllegalArgumentException("Class comparator tried to compare unknown class: " + o.getClass());
					}
				}
			}
		};
	}

	/**
	 * Equivalent to {@code concatAll(transform(input, transformer))}.
	 */
	public static <T, R> List<R> concatTransform(Collection<? extends T> input,
			Transformer<T, ? extends Collection<R>> transformer) {
		return concatAll(transform(input, transformer));
	}
	
	public static <X, Y> List<Pair<X, Y>> zip(Iterable<X> input1,
			Iterable<Y> input2) {
		List<Pair<X, Y>> list = new ArrayList<Pair<X, Y>>();
		Iterator<X> iter1 = input1.iterator();
		Iterator<Y> iter2 = input2.iterator();
		while (iter1.hasNext() && iter2.hasNext())
			list.add(new Pair<X, Y>(iter1.next(), iter2.next()));
		return list;
	}

	public static <X, Y> Pair<List<X>, List<Y>> unzip(Iterable<Pair<X, Y>> input) {
		List<X> x = new ArrayList<X>();
		List<Y> y = new ArrayList<Y>();
		for (Pair<X, Y> p : input) {
			x.add(p.x);
			y.add(p.y);
		}
		return Pair.makePair(x, y);
	}
	
	/**
	 * §precondition iterable != null
	 * §precondition cls != null
	 * §postcondition result != null
	 * §postcondition result != iterable
	 * 
	 * @param iterable
	 * @param cls
	 * @return a new list containing the elements of <code>iterable</code> that
	 *         are instances of the given class or instance of a superclass of
	 *         the given class.
	 */
	public static <I, O extends I> List<O> selectInstancesOf(
			Iterable<I> iterable, Class<? extends O> cls) {
		final List<I> lsti = select(iterable,
				RigidUtils.<I> isInstanceOf(cls));

		// As now we know that every member of lsti is instanceof (at least) O,
		// we can safely cast the result to List<O>:
		final List<O> result = (List<O>) lsti;

		assert result != null;
		assert result != iterable;

		return result;
	}
	
	/**
	 * @param cls
	 * @return a predicate that returns <code>true</code> iff the argument is either <code>null</code> or <code>instanceof cls</code>.
	 */
	public static <T> Predicate<T> isInstanceOf(Class<? extends T> cls) {
		return new InstanceOfPredicate<T>(cls);
	}

	/**
	 * inner class InstanceOfPredicate
	 */
	private static class InstanceOfPredicate<T> implements Predicate<T> {
		private final Class<? extends T> cls;

		InstanceOfPredicate(Class<? extends T> cls) {
			this.cls = cls;
		}

		@Override
		public boolean evaluate(T t) {
			return isInstanceOf(t, cls);
		}
	}
	
	/**
	 * Note that it is counterintuitive but true for the Java language, that <code>null</code> <code>instanceof</code> nothing,
	 * while on the other hand, <code>null</code> can be cast into anything.
	 * Hint: Maybe it's counterintuitive but a common OO phenomenon--and it makes sense: From the OO type system's
	 * perspective the null type (the type of <code>null</code>) is (must be (*)) a subtype of all other types.
	 * ((*) That's the reason why you can assign the null value to any other (reference) type).
	 * 
	 * §postcondition (t == null) --&gt; result
	 */
	public static <T> boolean isInstanceOf(T t, Class<? extends T> cls) {
		final boolean result = (t == null) || cls.isAssignableFrom(t.getClass());
		assert implies(t == null, result);
		return result;
	}
	
	/**
	 * Replacement for the missing implication operator ("--&gt;") in Java.
	 * <br>
	 * Note that for a boolean term "bConclusion" there is a difference between calling "implies(bPremise, bConclusion)" and
	 * executing "!bPremise || bConclusion" directly: In the former case, the term "bConclusion"
	 * is always evaluated, in the latter case, "bConclusion" is evaluated only if bPremise is true, because of the non-strict
	 * semantics of the "||" operator.
	 * @param bPremise
	 * @param bConclusion
	 * @return !bPremise || bConclusion
	 */
	public static boolean implies(boolean bPremise, boolean bConclusion) {
		return !bPremise || bConclusion;
	}
	
	/**
	 * Creates a new list out of the given <code>iterable</code>, containing all the elements that match the given
	 * <code>predicate</code>.
	 * <p>
	 * This function is called "filter" in Haskell.
	 * 
	 * §precondition iterable != null
	 * §precondition predicate != null
	 * §postcondition result != null
	 * §postcondition result != iterable
	 * 
	 * @param iterable
	 * @param predicate
	 * @return a new list containing the elements of <code>iterable</code> that match <code>predicate</code>.
	 */
	public static <E> List<E> select(Iterable<E> iterable, Predicate<? super E> predicate) {
		final List<E> result = new ArrayList<E>();
		for (E e : iterable) {
			if (predicate.evaluate(e)) {
				result.add(e);
			}
		}
		return result;
	}
	
	public static <T> Predicate<T> alwaysTrue() {
		return (Predicate<T>) AlwaysTruePredicate.INSTANCE;
	}

	public static <T> Predicate<T> alwaysFalse() {
		return (Predicate<T>) AlwaysFalsePredicate.INSTANCE;
	}
	
	private static enum AlwaysTruePredicate implements Predicate<Object> {
		INSTANCE;

		@Override
		public boolean evaluate(Object t) {
			return true;
		}
	}

	private static enum AlwaysFalsePredicate implements Predicate<Object> {
		INSTANCE;

		@Override
		public boolean evaluate(Object t) {
			return false;
		}
	}
	
	public static <T> Pair<List<T>, List<T>> split(Iterable<T> input,
			Predicate<? super T> test) {
		List<T> listTrue = new ArrayList<T>(), listFalse = new ArrayList<T>();
		split(input, test, listTrue, listFalse);
		return new Pair<List<T>, List<T>>(listTrue, listFalse);
	}

	/**
	 * splits <code>iterable</code> into the objects that satisfy or don't
	 * satisfy the given predicate, respectively.
	 * 
	 * §precondition iterable != null
	 * §precondition predicate != null
	 * §precondition collTrue != null
	 * §precondition collFalse != null
	 * 
	 * @param iterable
	 *            is not changed.
	 * @param predicate
	 * @param collTrue
	 *            on exit, contains the objects from <code>iterable</code> that
	 *            satisfy the <code>predicate</code>.
	 * @param collFalse
	 *            on exit, contains the objects from <code>iterable</code> that
	 *            don't satisfy the <code>predicate</code>.
	 */
	public static <E> void split(Iterable<E> iterable,
			Predicate<? super E> predicate, Collection<? super E> collTrue,
			Collection<? super E> collFalse) {
		collTrue.clear();
		collFalse.clear();
		for (E e : iterable) {
			// Unfortunately, the compiler complains about this one:
			// (predicate.evaluate(e) ? collTrue : collFalse).add(e);

			if (predicate.evaluate(e)) {
				collTrue.add(e);
			} else {
				collFalse.add(e);
			}
		}
	}
	
   public static <T> List<List<T>> splitEvery(Iterable<T> input, int size) {
	   	List<List<T>> listOfList = new LinkedList<List<T>>();
	   	List<T> list = null;
	   	int index = 0;
	   	for (T t : input) {
	   		if ((index % size) == 0) {
	   			list = new ArrayList<T>(size);
	   			listOfList.add(list);
	   		}
	   		index++;
	   		list.add(t);
	   	}
	   	return listOfList;
   }
		   
	/**
	 * §postcondition result &lt;--&gt; (s == null) || (s.length() == 0)
	 * 
	 * @param s
	 * @return Is s <code>null</code> or empty?
	 */
	public static boolean isNullOrEmpty(String s) {
		return (s == null || s.length() == 0);
	}
	
	public static String defaultIfNull(String x, String sDefault) {
		if (isNullOrEmpty(x)) {
			return sDefault;
		}
		return x;
	}
	
	public static Integer parseInt(String s, Integer def) {
		try {
			return Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return def;
		}
	}
	
	/**
	 * @param iterable
	 * @param predicate
	 * @return the first element, if any, of <code>iterable</code> that
	 *         satisfies <code>predicate</code>.
	 */
	public static <E> E findFirst(Iterable<E> iterable,
			Predicate<? super E> predicate) {
		E result = null;
		for (E e : iterable) {
			if (predicate.evaluate(e)) {
				result = e;
				break;
			}
		}
		return result;
	}
	
	/**
	 * Always use THIS ClassLoader from inside Nuclos!
	 * <p>
	 * NUCLOS-2164: 
	 * NEVER use some obscure, broken, and wrong <code>getClass().getClassLoader()</code>
	 * because YOU WILL break java web start.
	 * </p>
	 * @author Thomas Pasch
	 */
	public static ClassLoader getClassLoaderThatWorksForWebStart() {
		return new UtilsClassLoader();
	}
	
	/**
	 * 
	 * @param s String to be evalated. For null, false is returned
	 * @return Boolean if the String contains characters that are not letters, digits or underscore
	 */
	public static boolean needsToBeQuoted(String s) {
		if (s == null) {
			return false;
		}
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (!Character.isLetter(c) && !Character.isDigit(c) && c != '_') {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Es ist unklar warum diese Klasse benoetigt wird, eigentlich sollte "[B" ohne Probleme vom ContextClassloader gefunden werden.
	 */
	public static class UtilsClassLoader extends ClassLoader {

		public UtilsClassLoader() {
			super(Thread.currentThread().getContextClassLoader());
		}

		@Override
		protected Class<?> findClass(String name) throws ClassNotFoundException {
			if ("[B".equals(name)) {
				return byte[].class;
			}
			return super.findClass(name);
		}
		
	}

    public static int indexOf(byte[] outerArray, byte[] innerArray) {
        for(int i = 0; i < outerArray.length - innerArray.length; ++i) {
            boolean found = true;
            for(int j = 0; j < innerArray.length; ++j) {
               if (outerArray[i+j] != innerArray[j]) {
                   found = false;
                   break;
               }
            }
            if (found) return i;
         }
       return -1;  
    }
    
    public static byte[][] splitArray(byte[] outerArray, byte[] innerArray) {
    	int i = indexOf(outerArray, innerArray);
    	if (i < 0) {
    		return null;
    	}
    	
    	byte[] first = Arrays.copyOfRange(outerArray, 0, i);
    	byte[] second = Arrays.copyOfRange(outerArray, i, outerArray.length - i);
    	return new byte[][]{first, second};
    }
    
	public static boolean testByteArrayForXML(byte [] b) {
		if (b == null || b.length < 5) {
			return false;
		}
		
		return b[0] == '<' && b[1] == '?' && b[2] == 'x' && b[3] == 'm' && b[4] == 'l';
	}
	
	@SuppressWarnings({"unchecked"})
    public static <T> T uncheckedCast(Object obj) {
        return (T) obj;
    }

	public static Boolean parseBoolean(final Object oValue) {
		if (oValue == null) {
			return null;
		}
		if (oValue instanceof Boolean) {
			return (Boolean) oValue;
		}
		if (oValue instanceof String) {
			String sValue = (String) oValue;
			if ("1".equals(sValue) ||
					"t".equalsIgnoreCase(sValue) ||
					"true".equalsIgnoreCase(sValue) ||
					"y".equalsIgnoreCase(sValue) ||
					"yes".equalsIgnoreCase(sValue) ||
					"on".equalsIgnoreCase(sValue)) {
				return Boolean.TRUE;
			}
			return Boolean.parseBoolean(sValue);
		}
		if (oValue instanceof Number) {
			Number nValue = (Number) oValue;
			return nValue.intValue() != 0;
		}

		return Boolean.parseBoolean(oValue.toString());
	}

	public static boolean isNuclosStackFrame(
			final String stackFrame
	) {
		if (stackFrame == null) {
			return false;
		}

		return stackFrame.matches("^\\s*(at |Caused by: )?org\\.nucl.*")
				|| StringUtils.contains(stackFrame,"nuclet");
	}

	public static SecureRandom getSecureRandom() {
		try {
			return SecureRandom.getInstance("SHA1PRNG");
		} catch (NoSuchAlgorithmException e) {
			return new SecureRandom();
		}
	}
}
