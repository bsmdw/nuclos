package org.nuclos.common;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.IOException;

public interface INuclosImageProducer {
	
	int DEFAULTTHUMBSIZE = 20;
	
	BufferedImage getBufferedImage(byte []b) throws IOException;
	
	byte[] getScaledImageBytes(byte[] b, Dimension d) throws IOException;
	
	byte[] getThumbNail(BufferedImage buff) throws IOException;
	
}
