package org.nuclos.common;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public abstract class EntityContext implements Serializable {

	private static final long serialVersionUID = 1889732025606838483L;

	public abstract UID getMainEntity();
	public abstract UID getDependentEntity();
	public abstract UID getDependentEntityField();

	@Override
	public boolean equals(final Object o) {
		if (this == o) return true;

		if (o == null || getClass() != o.getClass()) return false;

		final EntityContextVO that = (EntityContextVO) o;

		return new EqualsBuilder()
				.append(getMainEntity(), that.getMainEntity())
				.append(getDependentEntity(), that.getDependentEntity())
				.append(getDependentEntityField(), that.getDependentEntityField())
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(getMainEntity())
				.append(getDependentEntity())
				.append(getDependentEntityField())
				.toHashCode();
	}

	@Override
	public String toString() {
		return "EntityContextVO{" +
				"mainEntity=" + getMainEntity() +
				", dependentEntity=" + getDependentEntity() +
				", dependentEntityField=" + getDependentEntityField() +
				'}';
	}

}
