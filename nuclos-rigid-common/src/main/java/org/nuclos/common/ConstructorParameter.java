//Copyright (C) 2019  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

public class ConstructorParameter {

	private final Object object;
	private final Class constructorClass;

	public ConstructorParameter(final Object object, final Class constructorClass) {
		this.object = object;
		this.constructorClass = constructorClass;
	}

	public Object getObject() {
		return object;
	}

	public Class getConstructorClass() {
		return constructorClass;
	}

}
