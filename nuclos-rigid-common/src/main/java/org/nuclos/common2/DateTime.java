//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common2;

import java.io.Serializable;
import java.util.Date;

import org.nuclos.common.RigidUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Date object for representing a date with time in our framework.
 * 
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:maik.stueker@novabit.de">Maik Stueker</a>
 * @version 00.01.000
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public final class DateTime implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1186508177333971545L;
	private Date value;
	
	/**
	 * @param timeInMillis
	 */
	public DateTime(long timeInMillis) {
		this(new Date(timeInMillis));
	}

	/**
	 * @param date
	 */
	public DateTime(Date date) {
		this.value = date;
	}

	public DateTime() {
		this(new Date());
	}
	
	public Date getDate() {
		return this.value;
	}

	public void setDate(Date date) {
		this.value = date;
	}

	@Override
	public String toString(){
		return getTime() + "";
	}
	
	public boolean before(Date when){
		return getTime() < when.getTime();
	}
	
	public boolean after(Date when){
		return getTime() > when.getTime();
	}
	
	public boolean before(DateTime when){
		return getTime() < when.getTime();
	}
	
	public boolean after(DateTime when){
		return getTime() > when.getTime();
	}
	
	public long getTime(){
		if (value == null) {
			return 0;
		} else {
			return value.getTime();
		}
	}

	@Override
	public boolean equals(Object that) {
		if (that instanceof DateTime) {
			return RigidUtils.equal(value, (((DateTime)that).value));
		} else {
			return super.equals(that);
		}
	}

	@Override
	public int hashCode() {
		if (value != null) {
			return value.hashCode();
		}
		return super.hashCode();
	}
	
	
}
