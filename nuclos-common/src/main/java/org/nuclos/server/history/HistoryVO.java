//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.history;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

import org.apache.log4j.Logger;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableFieldFormat;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.LangUtils;
import org.nuclos.server.common.valueobject.NuclosValueObject;

/**
 * Value object representing a logbook entry.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:ramin.goettlich@novabit.de">ramin.goettlich</a>
 * @version 01.00.000
 */
public class HistoryVO extends NuclosValueObject<Long> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2178456211884611216L;

	private static final Logger LOG = Logger.getLogger(HistoryVO.class);

	private Long objectId;
	private UID objectUid;
	private UID entity;
	private UID field;
	private UID systemField;
	private String sSavePoint;
	private String sUsername;
	private InternalTimestamp dValidUntil;
	private String sValueString;
	private Object oValueByte;
	private Long iValueRefId;
	private UID iValueRefUid;

	/**
	 * constructor to be called by server only
	 * @param evo contains the common fields.
	 */
	public HistoryVO(NuclosValueObject<Long> evo, Long objectId, UID objectUid, UID entity,
			UID field, UID systemField, String sSavePoint, String sUsername, InternalTimestamp dValidUntil,
			String sValueString, Object oValueByte, Long iValueRefId, UID iValueRefUid) {
		super(evo);
		this.objectId = objectId;
		this.objectUid = objectUid;
		this.entity = entity;
		this.field = field;
		this.systemField = systemField;
		this.sSavePoint = sSavePoint;
		this.sUsername = sUsername;
		this.dValidUntil = dValidUntil;
		this.sValueString = sValueString;
		this.oValueByte = oValueByte;
		this.iValueRefId = iValueRefId;
		this.iValueRefUid = iValueRefUid;
	}
	
	public Long getObjectId() {
		return objectId;
	}
	
	public UID getObjectUid() {
		return objectUid;
	}
	
	public UID getEntity() {
		return entity;
	}
	
	public UID getEntityField() {
		return field;
	}
	
	public UID getSystemField() {
		return systemField;
	}
	
	public String getSavePoint() {
		return sSavePoint;
	}

	public String getUsername() {
		return sUsername;
	}

	public InternalTimestamp getValidUntil() {
		return dValidUntil;
	}

	public String getValueString() {
		return sValueString;
	}

	public Object getValueByte() {
		return oValueByte;
	}

	public Long getValueRefId() {
		return iValueRefId;
	}

	public UID getValueRefUid() {
		return iValueRefUid;
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("id=").append(getId());
		result.append(",iObjectId=").append(getObjectId());
		result.append(",iObjectUid=").append(getObjectUid());
		result.append(",entity=").append(getEntity());
		result.append(",entityField=").append(getEntityField());
		result.append(",iSavePoint=").append(getSavePoint());
		result.append("]");
		return result.toString();
	}

	public static String getObjectValueAsString(FieldMeta<?> efMeta, Object oValue) {
		try {
			return oValue == null ? null : 
				CollectableFieldFormat.getInstance(efMeta == null ? oValue.getClass() : 
					LangUtils.getClassLoaderThatWorksForWebStart().loadClass(efMeta.getDataType()))
					.format(efMeta == null ? null : efMeta.getFormatOutput(), oValue);			
		} catch (Exception e) {
			LOG.warn("unable to get string to object", e);
			return null;
		}
	}

	public static Object getStringAsObjectValue(FieldMeta<?> efMeta, String oValue) {
		try {
			return oValue == null ? null : 
				CollectableFieldFormat.getInstance(efMeta == null ? oValue.getClass() : 
					LangUtils.getClassLoaderThatWorksForWebStart().loadClass(efMeta.getDataType()))
					.parse(efMeta == null ? null : efMeta.getFormatInput(), oValue);
		} catch (Exception e) {
			LOG.warn("unable to get string to object", e);
			return null;
		}
	}
	
	public static byte[] getObjectValueAsByteArray(Object oValue) {
		if (oValue == null)
			return null;

		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutput oos = null;
		try {
			oos = new ObjectOutputStream(baos);
			oos.writeObject(oValue);
			oos.flush();
			return baos.toByteArray();
		} catch (Exception e) {
			LOG.warn("unable to get object to bytearray", e);
		} finally {
			try { if (oos != null) oos.close(); } catch (Exception e) {}
			try { if (baos != null) baos.close(); } catch (Exception e) {}
		}
		return null;
	}
	
	public static Object getByteArrayAsObject(byte[] oValue) {
		if (oValue == null)
			return null;

		final ByteArrayInputStream bais = new ByteArrayInputStream(oValue);
		ObjectInput ois = null;
		try {
			ois = new ObjectInputStream(bais);
			return ois.readObject();
		} catch (Exception e) {
			LOG.warn("unable to get bytearray to object", e);
		} finally {
			try { if (ois != null) ois.close(); } catch (Exception e) {}
			try { if (bais != null) bais.close(); } catch (Exception e) {}
		}
		return null;
	}

}
