//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.attribute.ejb3;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.attribute.valueobject.AttributeCVO;

// @Remote
public interface AttributeFacadeRemote {

	/**
	 * @return a collection containing all dynamic attributes
	 */
	@RolesAllowed("Login")
	Collection<AttributeCVO> getAttributes(UID iGroupId);

	/**
	 * §precondition iAttributeId != null
	 * 
	 * @param iAttributeId id of attribute
	 * @return the attribute value object for the attribute with the given id
	 * @throws CommonPermissionException
	 */
	AttributeCVO get(UID iAttributeId)
		throws CommonFinderException, CommonPermissionException;

	/**
	 * invalidates the attribute cache (console function)
	 */
	@RolesAllowed("Login")
	void invalidateCache();

	/**
	 * @return the available functions for eg. calculated attributes
	 */
	@RolesAllowed("Login")
	Map<String, String> getCallableFunctions(final Class<?> result,final  Object... args);

	/**
	 * @return the layouts that contained this attribute
	 */
	@RolesAllowed("Login")
	Set<String> getAttributeLayouts(UID sAttributeName);
	
	/**
	 * @return the layouts that contained this attribute
	 */
	@RolesAllowed("Login")
	Set<String> getAttributeForModule(String iModuleId);

}
