package org.nuclos.server.i18n.language.data;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Locale;

import org.nuclos.api.locale.NuclosLocale;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;

public class DataLanguageVO extends EntityObjectVO<UID> implements Serializable, Comparator<DataLanguageVO> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4683514776788418422L;

	public DataLanguageVO(String language, String country, boolean isPrimary, Integer order) {
		
		super(E.DATA_LANGUAGE);
		
		setPrimaryKey(new UID(language + "_" + country));		
		setFieldValue(E.DATA_LANGUAGE.language, language);
		setFieldValue(E.DATA_LANGUAGE.country, country);
		setFieldValue(E.DATA_LANGUAGE.primaryLanguage, isPrimary);
		setFieldValue(E.DATA_LANGUAGE.order, order);
		
		flagNew();
	}
	
	public DataLanguageVO(EntityObjectVO<UID> voFromDB) {
		super(E.DATA_LANGUAGE);
		
		setFieldValue(E.DATA_LANGUAGE.language, 
				voFromDB.getFieldValue(E.DATA_LANGUAGE.language));
		setFieldValue(E.DATA_LANGUAGE.country, 
				voFromDB.getFieldValue(E.DATA_LANGUAGE.country));
		setFieldValue(E.DATA_LANGUAGE.primaryLanguage, 
				voFromDB.getFieldValue(E.DATA_LANGUAGE.primaryLanguage));
		setFieldValue(E.DATA_LANGUAGE.order, 
				voFromDB.getFieldValue(E.DATA_LANGUAGE.order));
		
		setPrimaryKey(voFromDB.getPrimaryKey());

		setChangedAt(voFromDB.getChangedAt());
		setCreatedAt(voFromDB.getCreatedAt());
		setChangedBy(voFromDB.getChangedBy());
		setCreatedBy(voFromDB.getCreatedBy());	
		setVersion(voFromDB.getVersion());
		
		// Note: following method only resets the state of the eo
		reset();
	}
	
	public Locale getLocale() {
		return new Locale(getLanguage(), getCountry());
	}
	
	public NuclosLocale getNuclosLocale() {
		return NuclosLocale.valueOf(getLanguage().toUpperCase() + "_" + getCountry().toUpperCase());
	}
	
	public String getLanguage() {
		return getFieldValue(E.DATA_LANGUAGE.language);
	}
	
	public String getCountry() {
		return getFieldValue(E.DATA_LANGUAGE.country);
	}
	
	private void setLanguage(String lang) {
		setFieldValue(E.DATA_LANGUAGE.language, lang);
	}
	
	private void setCountry(String country) {
		setFieldValue(E.DATA_LANGUAGE.country, country);
	}
	
	public void setOrder(int order) {
		setFieldValue(E.DATA_LANGUAGE.order, order);
	}
	
	public Integer getOrder() {
		return getFieldValue(E.DATA_LANGUAGE.order);
	}
	
	public void setIsPrimary(Boolean isPrimary) {
		setFieldValue(E.DATA_LANGUAGE.primaryLanguage, isPrimary);
	}
	
	public Boolean isPrimary() {
		return Boolean.valueOf(getFieldValue(E.DATA_LANGUAGE.primaryLanguage));
	}

	@Override
	public int compare(DataLanguageVO o1, DataLanguageVO o2) {		
		int compareTo = o1.getLanguage().compareTo(o2.getLanguage());
		
		if (compareTo == 0) {
			return o1.getCountry().compareTo(o2.getCountry());
		} else {
			return compareTo;
		}
	}
	
	@Override
	public String toString() {
		return getLocale().getDisplayLanguage() + " (" + getLocale().getCountry() + ")";
	}

	public String toStringShort() {
		return getLocale().getDisplayLanguage() + " (" + getLocale().getCountry() + ")";
	}
	
	@Override
	public boolean equals(Object obj) {
		
		boolean retVal = false;
		
		if (obj != null && obj instanceof DataLanguageVO) {
			DataLanguageVO dlvo = (DataLanguageVO) obj;		
			if(dlvo.getLocale().equals(this.getLocale())) {
				retVal = true;
			}
		}
		
		return retVal;
	}
	
	public boolean equalsLocale(Locale l) {
		boolean retVal = false;
		
		if (l != null) {
			if (this.getLocale().equals(l))
				retVal = true;
		}
		
		return retVal;
	}
	
	public DataLanguageVO copy() {
		DataLanguageVO retVal = new DataLanguageVO(super.copy());
		retVal.setOrder(this.getOrder());
		retVal.setIsPrimary(this.isPrimary());
		retVal.setLanguage(this.getLanguage());
		retVal.setCountry(this.getCountry());
		retVal.setPrimaryKey(this.getPrimaryKey());
		retVal.setVersion(this.getVersion());
		retVal.setChangedAt(this.getChangedAt());
		retVal.setCreatedAt(this.getCreatedAt());
		retVal.setChangedBy(this.getChangedBy());
		retVal.setCreatedBy(this.getCreatedBy());
		return retVal;
	}
}
