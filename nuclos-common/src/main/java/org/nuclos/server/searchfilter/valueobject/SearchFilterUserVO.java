//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.searchfilter.valueobject;

import java.util.Date;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.server.common.valueobject.NuclosValueObject;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Value object representing a searchfilteruser.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:martin.weber@novabit.de">Martin Weber</a>
 * @version 00.01.000
 */
public class SearchFilterUserVO extends NuclosValueObject<UID> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2370937007328731632L;
	private UID searchFilter;
	private UID user;
	private Boolean bEditable;
	private Boolean bCompulsory;
	private Date dValidFrom;
	private Date dValidUntil;
	
	public SearchFilterUserVO() {
	}
	
	public SearchFilterUserVO(UID uid, UID searchFilter, UID user, Boolean bEditable, Boolean bCompulsory, Date dValidFrom, Date dValidUntil,
			java.util.Date dCreated, String sCreated, java.util.Date dChanged, String sChanged, Integer iVersion) {
		super(uid, dCreated, sCreated, dChanged, sChanged, iVersion);
		
		setSearchFilter(searchFilter);
		setUser(user);
		setEditable(bEditable);
		setCompulsory(bCompulsory);
		setValidFrom(dValidFrom);
		setValidUntil(dValidUntil);
	}
	
	public void setSearchFilter(UID searchFilter) {
		this.searchFilter = searchFilter;
	}
	
	public UID getSearchFilter() {
		return this.searchFilter;
	}
	
	public void setUser(UID user) {
		this.user = user;
	}
	
	public UID getUser() {
		return this.user;
	}
	
	public void setEditable(Boolean bEditable) {
		this.bEditable = bEditable;
	}
	
	public Boolean isEditable() {
		return this.bEditable;
	}

	public Boolean isCompulsory() {
		return bCompulsory;
	}
	
	public void setCompulsory(Boolean b) {
		this.bCompulsory = b;
	}
	
	public void setValidFrom(Date dValidFrom) {
		this.dValidFrom = dValidFrom;
	}
	
	public Date getValidFrom() {
		return this.dValidFrom;
	}
	
	public void setValidUntil(Date dValidUntil) {
		this.dValidUntil = dValidUntil;
	}
	
	public Date getValidUntil() {
		return this.dValidUntil;
	}
	
	public static SearchFilterUserVO transformToSearchFilterUser(MasterDataVO<UID> mdVO_searchFilteruser) {
		SearchFilterUserVO searchFilterUser = new SearchFilterUserVO(
			mdVO_searchFilteruser.getId(),
			mdVO_searchFilteruser.getFieldUid(E.SEARCHFILTERUSER.searchfilter),
			mdVO_searchFilteruser.getFieldUid(E.SEARCHFILTERUSER.user),
			mdVO_searchFilteruser.getFieldValue(E.SEARCHFILTERUSER.editable),
			mdVO_searchFilteruser.getFieldValue(E.SEARCHFILTERUSER.compulsoryFilter),
			mdVO_searchFilteruser.getFieldValue(E.SEARCHFILTERUSER.validFrom),
			mdVO_searchFilteruser.getFieldValue(E.SEARCHFILTERUSER.validUntil),
			mdVO_searchFilteruser.getCreatedAt(),
			mdVO_searchFilteruser.getCreatedBy(),
			mdVO_searchFilteruser.getChangedAt(),
			mdVO_searchFilteruser.getChangedBy(),
			mdVO_searchFilteruser.getVersion());
		
		return searchFilterUser;
	}
	
	public static MasterDataVO<UID> transformToMasterData(SearchFilterUserVO searchFilterUser) {
		MasterDataVO<UID> mdVO = new MasterDataVO<UID>(
				E.SEARCHFILTERUSER.getUID(),
				searchFilterUser.getId(),
				searchFilterUser.getCreatedAt(),
				searchFilterUser.getCreatedBy(),
				searchFilterUser.getChangedAt(),
				searchFilterUser.getChangedBy(),
				searchFilterUser.getVersion(),
				null, null, null, false);
		
		mdVO.setFieldUid(E.SEARCHFILTERUSER.searchfilter, searchFilterUser.getSearchFilter());
		mdVO.setFieldUid(E.SEARCHFILTERUSER.user, searchFilterUser.getUser());
		mdVO.setFieldValue(E.SEARCHFILTERUSER.editable, searchFilterUser.isEditable());
		mdVO.setFieldValue(E.SEARCHFILTERUSER.compulsoryFilter, searchFilterUser.isCompulsory());
		mdVO.setFieldValue(E.SEARCHFILTERUSER.validFrom, searchFilterUser.getValidFrom());
		mdVO.setFieldValue(E.SEARCHFILTERUSER.validUntil, searchFilterUser.getValidUntil());
		
		return mdVO;
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("uid=").append(getId());
		result.append(",user=").append(getUser());
		result.append(",filter=").append(getSearchFilter());
		result.append("]");
		return result.toString();
	}

}
