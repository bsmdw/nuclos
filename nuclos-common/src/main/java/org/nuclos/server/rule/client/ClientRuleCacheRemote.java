package org.nuclos.server.rule.client;

import java.util.List;
import java.util.Map;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.vo.EntityObjectVO;

public interface ClientRuleCacheRemote {
	
	public List<EntityObjectVO<UID>> getBOsByNuclet(UID nucletUid);
	public List<FieldMeta<?>> getFieldsByBO(UID boUid);
	public Map<Pair<UID, UID>, List<FieldMeta<?>>> getReferencesByBO(UID boUid);
	public Map<UID, List<FieldMeta<?>>> getDependentsByBO(UID boUid);
	
}
