package org.nuclos.server.rule.client.vo;

import org.nuclos.common.UID;
import org.nuclos.server.common.valueobject.NuclosValueObject;

public class ClientRuleSourceVO extends NuclosValueObject<UID> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2016240426637172334L;
	private UID nucletId;
	private String name;
	private String description;
	private boolean isActive;
	private String ruleSource;
	private String ruleContextSource;
	private String ruleType;
	
	/**
	 * @param nvo
	 * @param id
	 * @param nucletId
	 * @param name
	 * @param description
	 * @param isActive
	 * @param ruleType
	 * @param ruleSource
	 * @param ruleContextSource
	 */
	public ClientRuleSourceVO(NuclosValueObject<UID> nvo, UID nucletId, String name,
			String description, boolean isActive, String ruleType, String ruleSource,
			String ruleContextSource) {
		
		super(nvo);
		this.nucletId = nucletId;
		this.name = name;
		this.description = description;
		this.isActive = isActive;
		this.ruleType = ruleType;
		this.ruleSource = ruleSource;
		this.ruleContextSource = ruleContextSource;
	}

	public UID getNucletId() {
		return nucletId;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public boolean isActive() {
		return isActive;
	}

	public String getRuleSource() {
		return ruleSource;
	}

	public String getRuleContextSource() {
		return ruleContextSource;
	}

	public String getRuleType() {
		return ruleType;
	}

}
