package org.nuclos.server.ruleengine;

import org.nuclos.api.context.InputRequiredException;
import org.nuclos.common2.exception.NuclosExceptions;

//A Wrapper Exception just in Order to shorten down the size of the StackTrace of the InputRequiredException in the Nuclos-API
//TODO: Both shouldn't be a Runtime Exception, in order to keep existing server rules alive, they sill are.

public class NuclosInputRequiredException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8242001227741574737L;
	private final int MAX_STACKTRACE_LENGTH = 3;

	public NuclosInputRequiredException(InputRequiredException ire) {
		super(ire);
		NuclosExceptions.shortenStackTrace(this, 1);
		NuclosExceptions.shortenStackTrace(ire, MAX_STACKTRACE_LENGTH);
	}

}
