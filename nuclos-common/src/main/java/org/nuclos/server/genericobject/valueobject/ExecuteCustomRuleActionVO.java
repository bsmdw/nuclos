package org.nuclos.server.genericobject.valueobject;

import java.io.Serializable;

/**
 * 
 * UI Action VO for Custom Rule 
 * 
 * ExecuteCustomRuleActionVO
 *
 */
public class ExecuteCustomRuleActionVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5508494887679124944L;
	private final String name;
	private final String label;
	private final String className;

	public ExecuteCustomRuleActionVO(final String className) {
		this(className, className, className);
	}

	public ExecuteCustomRuleActionVO(final String name, final String label, final String className) {
		super();
		this.name = name;
		this.label= label;
		this.className = className;
	}

	/**
	 * get name for action
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * get label for action
	 * 
	 * @return
	 */
	public String getLabel() {
		return label;
	}
	
	/**
	 * get classname for action
	 * 
	 * @return
	 */
	public String getClassName() {
		return className;
	}

	@Override
	public String toString() {
		return getLabel();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((className == null) ? 0 : className.hashCode());
		return result;
	}

	/*
	 * equal by full qualified class name
	 * 
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExecuteCustomRuleActionVO other = (ExecuteCustomRuleActionVO) obj;
		if (className == null) {
			if (other.className != null)
				return false;
		} else if (!className.equals(other.className))
			return false;
		return true;
	}


	
	
}
