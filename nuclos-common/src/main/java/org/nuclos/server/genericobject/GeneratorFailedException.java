package org.nuclos.server.genericobject;

import org.nuclos.api.exception.BusinessException;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.genericobject.ejb3.GenerationResult;

/**
 * Exception thrown if object generation fails.
 * <p>
 * This is the <em>only</em> (checked) exception that can be thrown as a result of a
 * object generation. This is because even failed object generations should
 * display the (incorrectly) generated object in the GUI client. For this,
 * the {@link GenerationResult} is included.
 * 
 * @author Thomas Pasch (javadoc)
 */
public class GeneratorFailedException extends CommonValidationException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7107688474396477958L;
	private final GenerationResult result;
	
	public GeneratorFailedException(String message, GenerationResult result, CommonBusinessException exception) {
		super(message, exception);
		this.result = result;
	}
	
	// needed for http://project.nuclos.de/browse/LIN-116
	public GeneratorFailedException(String message, GenerationResult result, BusinessException exception) {
		super(message, exception);
		this.result = result;
	}
	
	public GenerationResult getGenerationResult() {
		return result;
	}

}
