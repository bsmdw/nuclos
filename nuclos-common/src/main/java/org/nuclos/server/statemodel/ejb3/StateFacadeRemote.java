//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.statemodel.ejb3;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.statemodel.StatemodelClosure;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.nuclos.server.statemodel.NuclosSubsequentStateNotLegalException;
import org.nuclos.server.statemodel.valueobject.StateGraphVO;
import org.nuclos.server.statemodel.valueobject.StateHistoryVO;
import org.nuclos.server.statemodel.valueobject.StateModelVO;
import org.nuclos.server.statemodel.valueobject.StateTransitionVO;
import org.nuclos.server.statemodel.valueobject.StateVO;

// @Remote
public interface StateFacadeRemote {

	/**
	 * gets a complete state graph for a state model
	 * @param stateModelUid id of state model to get graph for
	 * @return state graph cvo containing the state graph information for the model with the given id
	 * @throws CommonPermissionException
	 */
	StateGraphVO getStateGraph(final UID stateModelUid)
		throws CommonFinderException, CommonPermissionException,
		NuclosBusinessException;

	/**
	 * method to insert, update or remove a complete state model in the database at once
	 * @param stategraphcvo state graph representation
	 * @return state model UID
	 * @throws CommonPermissionException
	 */
	UID setStateGraph(StateGraphVO stategraphcvo,
		IDependentDataMap mpDependants) throws CommonCreateException,
		CommonFinderException, CommonRemoveException,
		CommonValidationException, CommonStaleVersionException,
		CommonPermissionException, NuclosBusinessException;

	/**
	 * method to remove a complete state model with all usages in the database at once
	 * @param statemodelvo state model value object
	 * @throws CommonPermissionException
	 */
	void removeStateGraph(StateModelVO statemodelvo)
		throws CommonFinderException, CommonRemoveException,
		CommonStaleVersionException, CommonPermissionException,
		NuclosBusinessRuleException, NuclosBusinessException;

	/**
	 * @param usagecriteria
	 * @return the UID of the initial state of the statemodel corresponding to <code>usagecriteria</code>.
	 */
	UID getInitialStateId(UsageCriteria usagecriteria);

	/**
	 * @param usagecriteria
	 * @return the UID of the statemodel corresponding to <code>usagecriteria</code>.
	 */
	UID getStateModelId(UsageCriteria usagecriteria);

	/**
	 * @param stateModelUid statemodel UID
	 * @return the id of the statemodel corresponding to <code>usagecriteria</code>.
	 */
	Collection<StateVO> getStatesByModel(UID stateModelUid);

	/**
	 * method to get all state models
	 * @return collection of state model vo
	 */
	Collection<StateModelVO> getStateModels() throws CommonFinderException, CommonPermissionException, NuclosBusinessException;

	/**
	 * method to return the sorted list of state history entries for a given leased object
	 * 
	 * §nucleus.permission mayRead(module)
	 * 
	 * @param moduleUid UID of module for plausibility check
	 * @param genericObjectId id of leased object
	 * @return set of state history entries
	 */
	Collection<StateHistoryVO> getStateHistory(
		final UID moduleUid, final Long genericObjectId)
		throws CommonFinderException, CommonPermissionException;

	/**
	 * Retrieve all states in all state models for the module with the given id
	 * @param stateModelUid UID of module to retrieve states for
	 * @return Collection of all states for the given module
	 */
	Collection<StateVO> getStatesByModule(final UID stateModelUid);

	/**
	 * method to return the possible subsequent states for a given leased object
	 * 
	 * §nucleus.permission mayRead(module)
	 * 
	 * @param stateModelUid module id for plausibility check
	 * @param genericObjectId id of leased object to get subsequent states for
	 * @param bGetAutomaticStatesAlso should method also return states for automatic only 
	 * 	transitions? false for returning subsequent states to client, which generates buttons 
	 * 	for manual state changes
	 * @return set of possible subsequent states for given leased object
	 */
	Collection<StateVO> getSubsequentStates(final UID stateModelUid,
		final Long genericObjectId, boolean bGetAutomaticStatesAlso)
		throws CommonFinderException;
	
	/**
	 * checks if the given target state id is contained in the list of subsequent states for the given leased objects:
	 * @param stateModelUid
	 * @param genericObjectId
	 * @param targetStateUid
	 * @throws NuclosNoAdequateStatemodelException
	 * @return true/false if state change is allowed
	 */
	boolean checkTargetState(final UID stateModelUid, final Long genericObjectId,
	                         final UID targetStateUid)
			throws CommonFinderException, CommonPermissionException;

	/**
	 * method to change the status of a given leased object
	 * 
	 * §nucleus.permission mayWrite(module)
	 * 
	 * @param stateModelUid module id for plausibility check
	 * @param genericObjectId leased object id to change status for
	 * @param targetstateModelUid legal subsequent status id to set for given leased object
	 */
	void changeStateByUser(final UID stateModelUid,
		final Long genericObjectId, final UID targetstateModelUid, final String customUsage)
		throws NuclosBusinessException, CommonPermissionException,
		CommonPermissionException, CommonCreateException,
		NuclosSubsequentStateNotLegalException, CommonFinderException;
	
	/**
	 * method to modify and change state of a given object
	 * 
	 * §nucleus.permission mayWrite(module)
	 * 
	 * @param stateModelUid module id for plausibility check
	 * @param gowdvo object to change status for
	 * @param targetstateModelUid legal subsequent status id to set for given object
	 */
	void changeStateAndModifyByUser(final UID stateModelUid, 
		final GenericObjectWithDependantsVO gowdvo, final UID targetstateModelUid, final String customUsage)
		throws NuclosBusinessException, CommonPermissionException,
		CommonPermissionException, CommonCreateException,
		NuclosSubsequentStateNotLegalException, CommonFinderException, CommonRemoveException, 
		CommonStaleVersionException, CommonValidationException, CommonFatalException,
		NuclosCompileException;

	void invalidateCache();

	Map<FieldMeta<?>, String> getResourceSIdsForStateUid(UID stateUid) throws CommonFinderException;
	
	/**
	 * TODO: MULTINUCLET statemodel closure for state??	 
	 */
	StatemodelClosure getStatemodelClosureForEntity(final UID stateModelUid);
	
	StateTransitionVO findStateTransitionByNullAndTargetState(final UID targetStateUid);
	
	StateTransitionVO findStateTransitionBySourceAndTargetState(final UID sourceStateUid, final UID targetStateUid);
	
	List<StateTransitionVO> getOrderedStateTransitionsByStatemodel(final UID stateModelUid);

	/**
	 * returns the masterdata of a state as statevo
	 * 
	 * @param StateUid
	 * @return
	 */
	StateVO getState(final UID StateUid) throws CommonFinderException;

	/**
	 * copies the permissions of an statemodel from one given role.
	 * @param roleToCopy
	 * @param roleToCopyTo
	 */ 
	void copyRolePermissions(UID roleToCopy, UID roleToCopyTo) throws NuclosBusinessException;
}
