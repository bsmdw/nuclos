package org.nuclos.server.masterdata.valueobject;

import java.util.List;
import java.util.Map;

import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;

/**
 * Created by Oliver Brausch on 12.03.19.
 */
public class MDVOWrapperDisplayOneField<PK> extends MasterDataVOWrapper<PK> {
	private final UID displayField;
	public MDVOWrapperDisplayOneField(MasterDataVO<PK> mdvo, List<UID> wrapperFieldUids, Map<UID, Object> wrapperFields,
									  UID displayField) {
		super(mdvo, wrapperFieldUids, wrapperFields);
		this.displayField = displayField;
	}

	private String getDisplayString() {
		Object val = getFieldValue(displayField);
		return val != null ? val.toString() : null;
	}

	@Override
	public String toString() {
		return getDisplayString();
	}

}
