package org.nuclos.server.masterdata.ejb3;

import java.io.Serializable;

import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collection.Pair;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class CollectableFieldsByNameParams implements Serializable {

	private static final long serialVersionUID = -3706230606695999541L;

	private final UID entityUid;
	private final String stringifiedFieldDefinition;
	private final boolean checkValidity;
	private final UID mandator;
	private final UID dataLanguage;
	private final Pair<UID, CollectableSearchCondition> additionalCondition;

	private CollectableFieldsByNameParams(final Builder builder) {
		entityUid = builder.entityUid;
		stringifiedFieldDefinition = builder.stringifiedFieldDefinition;
		checkValidity = builder.checkValidity;
		mandator = builder.mandator;
		dataLanguage = builder.dataLanguage;
		additionalCondition = builder.additionalCondition;
	}

	public static Builder builder() {
		return new Builder();
	}

	public UID getEntityUid() {
		return entityUid;
	}

	public String getStringifiedFieldDefinition() {
		return stringifiedFieldDefinition;
	}

	public boolean isCheckValidity() {
		return checkValidity;
	}

	public UID getMandator() {
		return mandator;
	}

	public UID getDataLanguage() {
		return dataLanguage;
	}

	public Pair<UID, CollectableSearchCondition> getAdditionalCondition() {
		return additionalCondition;
	}

	public static final class Builder {
		private UID entityUid;
		private String stringifiedFieldDefinition;
		private boolean checkValidity;
		private UID mandator;
		private UID dataLanguage;
		private Pair<UID, CollectableSearchCondition> additionalCondition;

		private Builder() {
		}

		public Builder entityUid(final UID val) {
			entityUid = val;
			return this;
		}

		public Builder stringifiedFieldDefinition(final String val) {
			stringifiedFieldDefinition = val;
			return this;
		}

		public Builder checkValidity(final boolean val) {
			checkValidity = val;
			return this;
		}

		public Builder mandator(final UID val) {
			mandator = val;
			return this;
		}

		public Builder dataLanguage(final UID val) {
			dataLanguage = val;
			return this;
		}

		public Builder additionalCondition(final Pair<UID, CollectableSearchCondition> val) {
			additionalCondition = val;
			return this;
		}

		public CollectableFieldsByNameParams build() {
			return new CollectableFieldsByNameParams(this);
		}
	}
}
