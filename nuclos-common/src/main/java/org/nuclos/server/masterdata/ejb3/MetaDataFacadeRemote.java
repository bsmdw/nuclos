//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.masterdata.ejb3;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.CommonMetaDataServerProvider;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.LafParameterMap;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.transport.vo.EntityMetaTransport;
import org.nuclos.common.transport.vo.FieldMetaTransport;
import org.nuclos.common.valueobject.EntityRelationshipModelVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Remote facade for accessing meta data information from the
 * client side.
 * <p>
 * This implements {@link CommonMetaDataServerProvider}. You should implement
 * {@link org.nuclos.common.CommonMetaDataClientProvider} on the client side.
 * </p>
 */
// @Remote
public interface MetaDataFacadeRemote extends CommonMetaDataServerProvider {

	@Deprecated
	@RolesAllowed("Login")
	Object modifyEntityMetaData(UID entityUid, List<FieldMetaTransport> lstFields);

	// TODO: Do not return a error string. throw an exception.
	@RolesAllowed("Login")
	String createOrModifyEntity(EntityMetaTransport updatedMDEntity, 
			List<FieldMetaTransport> lstFields,
								boolean bRollBackOnStructureChangeExceptions,
								Map<String, Exception> structureChangeExceptions) throws NuclosBusinessException;

	@RolesAllowed("Login")
	void invalidateServerMetadata();

	@RolesAllowed("Login")
	Collection<MasterDataVO<UID>> hasEntityFieldInImportStructure(UID field);

	@RolesAllowed("Login")
	boolean hasEntityRows(UID entityUid);

	@RolesAllowed("Login")
	boolean hasEntityLayout(UID entity);

	/**
	 * @return Script (with results if selected)
	 */
	@RolesAllowed("Login")
	List<String> getDBTables();

	/**
	 * @return Script (with results if selected)
	 */
	@RolesAllowed("Login")
	Map<String, MasterDataVO<?>> getColumnsFromTable(String sTable);

	/**
	 * @return Script (with results if selected)
	 */
	@RolesAllowed("Login")
	List<String> getTablesFromSchema(String url, String user, String password, String schema);

	/**
	 * @return Script (with results if selected)
	 */
	@RolesAllowed("Login")
	List<MasterDataVO<UID>> transformTable(String url, String user, String password, String schema, String table);

	/**
	 * force to change internal entity name
	 */
	@RolesAllowed("Login")
	EntityRelationshipModelVO getEntityRelationshipModelVO(MasterDataVO<UID> vo);

	/**
	 * force to change internal entity name
	 */
	@RolesAllowed("Login")
	boolean isChangeDatabaseColumnToNotNullableAllowed(UID field);

	/**
	 * force to change internal entity name
	 */
	@RolesAllowed("Login")
	boolean isChangeDatabaseColumnToUniqueAllowed(UID field);

	@RolesAllowed("Login")
	Collection<EntityMeta<?>> getAllEntities();

	@RolesAllowed("Login")
	Map<UID, FieldMeta<?>> getAllEntityFieldsByEntity(UID entity);

	@RolesAllowed("Login")
	Map<UID, Map<UID, FieldMeta<?>>> getAllEntityFieldsByEntitiesGz(List<UID> entities);

	@RolesAllowed("Login")
	Collection<EntityMeta<?>> getNucletEntities();

	@RolesAllowed("Login")
	void removeEntity(UID entityUid, boolean dropLayout) throws CommonBusinessException;

	@RolesAllowed("Login")
	boolean hasEntityImportStructure(UID entityUID) throws CommonBusinessException;

	@RolesAllowed("Login")
	boolean hasEntityWorkflow(UID entityUID) throws CommonBusinessException;

	@RolesAllowed("Login")
	List<String> getVirtualEntities();

	@RolesAllowed("Login")
	List<FieldMeta<?>> getVirtualEntityFields(String virtualentity);

	@RolesAllowed("Login")
	void tryVirtualEntitySelect(EntityMeta<?> virtualentity) throws NuclosBusinessException;

	@RolesAllowed("Login")
	void tryRemoveProcess(EntityObjectVO<?> process) throws NuclosBusinessException;

	@RolesAllowed("Login")
	Collection<EntityMeta<?>> getSystemMetaData();

	@RolesAllowed("Login")
	Map<UID, LafParameterMap> getLafParameters();

	@RolesAllowed("Login")
	EntityMeta<?> transferTable(String url, String user, String password, String schema, String table, UID entityUID);

	@RolesAllowed("Login")
	List<EntityObjectVO<UID>> getNuclets();

}
