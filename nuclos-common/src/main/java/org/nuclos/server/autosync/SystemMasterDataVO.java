//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.autosync;

import java.util.Date;
import java.util.Map;

import org.nuclos.common.UID;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

public class SystemMasterDataVO<PK> extends MasterDataVO<PK> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7595291474677717751L;
	private static final Date SYSDATE = new Date();
	private static final String NUCLOS = "NUCLOS";

	public static final Date CREATED_DATE = SYSDATE;
	public static final String CREATED_USER = NUCLOS;
	public static final Date CHANGED_DATE = SYSDATE;
	public static final String CHANGED_USER = NUCLOS;
	public static final int VERSION = 1;	
	
	SystemMasterDataVO(UID entityUID, PK pk, Map<UID, Object> fields, Map<UID, UID> fieldUIDs) {
		super(entityUID, pk, CREATED_DATE, CREATED_USER, CHANGED_DATE, CHANGED_USER, VERSION, fields, null, fieldUIDs, true);
		getEntityObject().setComplete(true);
	}
}
