package org.nuclos.server.eventsupport.valueobject;

import org.nuclos.common.UID;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.valueobject.NuclosValueObject;

/**
 * Models the n:m assignment of EventSupports to Entities.
 */
public class EventSupportEventVO extends EventSupportVO
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4769019335762840591L;
	// Foreign keys
	private UID entityUID;
	private UID integrationPointUID;
	private UID processUID;
	private String processName;
	private UID stateUID;
	private String stateName;
	
	public EventSupportEventVO(NuclosValueObject<UID> nvo, String sEventSupportClass, String sEventSupportType, UID pEntityUID,
							   UID pIntegrationPointUID, UID pProcessUID, String sProcessName, UID pStateUID, String sStateName, Integer iOrder) {
		super(nvo, iOrder, sEventSupportClass,sEventSupportType);
		
		this.entityUID = pEntityUID;
		this.integrationPointUID = pIntegrationPointUID;
		this.processUID = pProcessUID;
		this.processName = sProcessName;
		this.stateUID = pStateUID;
		this.stateName = sStateName;
	}
	
	public EventSupportEventVO(String sEventSupportClass, String sEventSupportType, UID pEntityUID,
							   UID pIntegrationPointUID, UID pProcessUID, String sProcessName, UID pStateUID, String sStateName, Integer iOrder) {
		super(iOrder, sEventSupportClass,sEventSupportType);
		
		this.entityUID = pEntityUID;
		this.integrationPointUID = pIntegrationPointUID;
		this.processUID = pProcessUID;
		this.processName = sProcessName;
		this.stateUID = pStateUID;
		this.stateName = sStateName;
	}

	public UID getEntityUID() {
		return entityUID;
	}
	public void setEntityUID(UID pEntityUID) {
		this.entityUID = pEntityUID;
	}
	public UID getIntegrationPointUID() {
		return integrationPointUID;
	}
	public void setIntegrationPointUID(final UID integrationPointUID) {
		this.integrationPointUID = integrationPointUID;
	}

	public UID getProcessUID() {
		return processUID;
	}
	public void setProcessUID(UID pProcessUID) {
		this.processUID = pProcessUID;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public UID getStateUID() {
		return stateUID;
	}
	public void setStateUID(UID pStateUID) {
		this.stateUID = pStateUID;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	
	/**
	 * validity checker
	 */
	@Override
	public void validate() throws CommonValidationException {
		super.validate();
		
		if (getEntityUID() == null && getIntegrationPointUID() == null) {
			throw new CommonValidationException("ruleengine.error.validation.rule.name");
		}
		if (StringUtils.isNullOrEmpty(this.getEventSupportClass())) {
			throw new CommonValidationException("ruleengine.error.validation.rule.description");
		}
	
	}

}
