package org.nuclos.server.eventsupport.valueobject;

import java.util.Date;
import java.util.List;

import org.nuclos.common.UID;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.valueobject.NuclosValueObject;

public class EventSupportSourceVO extends NuclosValueObject<UID> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7790398185645103659L;
	private String sName;
	private String sDescription;
	private String sClassname;
	private List<String> sInterface;
	private String sPackage;
	private Date   dDateOfCompilation;
	private UID   nuclet;
	private boolean isActive;
	private boolean isSystem;
	
	public EventSupportSourceVO(NuclosValueObject nvo, String sName, String sDescription, String sClassname, 
			List<String> sInterface, String sPackage, Date dDateOfCompilation, UID nuclet, boolean pIsActive) {
		
		super(nvo);		
		this.sName = sName;
		this.sDescription = sDescription;
		this.sClassname = sClassname;
		this.sInterface = sInterface;
		this.sPackage = sPackage;
		this.dDateOfCompilation = dDateOfCompilation;
		this.nuclet = nuclet;
		this.isActive = pIsActive;
	}
	
	public String getName() {
		if (sName == null) {
			return sClassname;
		}
		return sName;
	}
	public void setName(String sName) {
		this.sName = sName;
	}
	public String getDescription() {
		return sDescription;
	}
	public void setDescription(String sDescription) {
		this.sDescription = sDescription;
	}
	public String getClassname() {
		return sClassname;
	}
	public void setClassname(String sClassname) {
		this.sClassname = sClassname;
	}
	public List<String> getInterface() {
		return sInterface;
	}
	public void setInterface(List<String> cInterface) {
		this.sInterface = cInterface;
	}

	public boolean isActive() {
		return isActive;
	}

	private String getEventSupportVO()
	{
		return this.getClassname();
	}
	
	public String getPackage() {
		return sPackage;
	}

	public void setPackage(String sPackage) {
		this.sPackage = sPackage;
	}

	
	public Date getDateOfCompilation() {
		return dDateOfCompilation;
	}

	public void setDateOfCompilation(Date dDateOfCompilation) {
		this.dDateOfCompilation = dDateOfCompilation;
	}

	public UID getNuclet() {
		return nuclet;
	}

	public void setNuclet(UID nuclet) {
		this.nuclet = nuclet;
	}

	/**
	 * validity checker
	 */
	@Override
	public void validate() throws CommonValidationException {
		
		if (StringUtils.isNullOrEmpty(this.getName())) {
			throw new CommonValidationException("eventsupport.error.validation.eventsupport.name");
		}
		if (StringUtils.isNullOrEmpty(this.getDescription())) {
			throw new CommonValidationException("eventsupport.error.validation.eventsupport.description");
		}
		if (StringUtils.isNullOrEmpty(this.getClassname())) {
			throw new CommonValidationException("ruleengine.error.validation.eventsupport.classname");
		}
	}

	@Override
	public int hashCode() {
		return (getEventSupportVO() != null ? getEventSupportVO().hashCode() : 0);
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof EventSupportSourceVO) {
			final EventSupportSourceVO that = (EventSupportSourceVO) o;
			// eventsupport objects are equal if there names are equal
			return getEventSupportVO().equals(that.getEventSupportVO());
		}
		return false;
	}
	
	@Override
	public String toString() {
		return getClassname();
	}

	public boolean isSystem() {
		return isSystem;
	}

	public void setSystem(boolean isSystem) {
		this.isSystem = isSystem;
	}

	public static EventSupportSourceVO getSystemRuleForExecuteBusinessRules(String sClassname) {
		EventSupportSourceVO result = new EventSupportSourceVO(new NuclosValueObject(),
				sClassname, sClassname, sClassname,
				null, sClassname.substring(sClassname.lastIndexOf(".")+1),
				null, null, true);
		result.setSystem(true);
		return result;
	}
}
