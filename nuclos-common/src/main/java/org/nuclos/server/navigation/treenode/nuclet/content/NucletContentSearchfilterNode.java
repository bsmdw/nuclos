//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.navigation.treenode.nuclet.content;

import org.nuclos.common.E;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;

/**
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.nuclos.de">www.nuclos.de</a>
 *
 * @author	<a href="mailto:maik.stueker@nuclos.de">maik.stueker</a>
 * @version 00.01.000
 */
public class NucletContentSearchfilterNode extends DefaultNucletContentEntryTreeNode {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7144498584991699793L;
	
	/**
	 * Fill only when needed...
	 */
	private static transient IMetaProvider MDP;

	public NucletContentSearchfilterNode(EntityObjectVO eo) {
		super(eo);
	}
	
	@Override
	public String getName() {
		UID entityUID = eo.getFieldUid(E.SEARCHFILTER.entity);
		String sEntityName = "";
		if (entityUID != null) {
			sEntityName = getMetaProvider().getEntity(entityUID).getEntityName() + ".";
		}
		return sEntityName + eo.getFieldValue(E.SEARCHFILTER.name) + " (" + eo.getCreatedBy() + ")";
	}
	
	private static IMetaProvider getMetaProvider() {
		if (MDP == null && SpringApplicationContextHolder.isSpringReady()) {
			MDP = (IMetaProvider) SpringApplicationContextHolder.getBean("metaDataProvider");
		}
		return MDP;
	}

}
