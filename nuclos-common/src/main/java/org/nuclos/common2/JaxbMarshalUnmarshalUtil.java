package org.nuclos.common2;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.DecimalFormat;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.sax.SAXSource;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/**
 * Hilfsmethoden fuer JAXB-Marshalling und -Unmarshalling
 */
public class JaxbMarshalUnmarshalUtil {
	static final DecimalFormat DF_2 = new DecimalFormat("#,##0.00");

	public static <T> T unmarshal(
			String xml,
			Class<T> clss
	) throws JAXBException, SAXException, ParserConfigurationException {

		InputStream stream = new ByteArrayInputStream(xml.getBytes());
		return unmarshal(stream, clss);
	}

	public static <T> T unmarshal(
			InputStream xmlStream,
			Class<T> clss
	) throws JAXBException, SAXException, ParserConfigurationException {

		SAXSource source = getSaxSource(xmlStream);

		JAXBContext jaxbContext = JAXBContext.newInstance(clss);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		T result = (T) jaxbUnmarshaller.unmarshal(source);

		return result;
	}

	/**
	 * Creates a SAXSource from the given InputStream using a non-validating XML reader.
	 * This prevents loading of external DTDs and therefore build errors when the DTD could not be loaded.
	 */
	private static SAXSource getSaxSource(final InputStream xmlStream) throws ParserConfigurationException, SAXException {
		InputSource inputSource = new InputSource(xmlStream);

		SAXParserFactory spf = SAXParserFactory.newInstance();
		spf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
		spf.setFeature("http://xml.org/sax/features/validation", false);
		spf.setNamespaceAware(true);

		XMLReader xmlReader = spf.newSAXParser().getXMLReader();
		return new SAXSource(xmlReader, inputSource);
	}
}