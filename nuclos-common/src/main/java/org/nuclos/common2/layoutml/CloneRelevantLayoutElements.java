package org.nuclos.common2.layoutml;

import java.util.Map;
import java.util.Set;

import org.nuclos.common.UID;

/**
 * Created by Sebastian Debring on 9/26/2018.
 */
public class CloneRelevantLayoutElements {
	private Set<UID> nonCloneableFields;
	private Set<UID> nonCloneableSubformReferences;
	private Map<UID, Set<UID>> nonCloneableSubformcolumns;
	private Set<UID> subformReferences;

	public CloneRelevantLayoutElements(final Set<UID> nonCloneableFields, final Set<UID> nonCloneableSubformReferences, final Map<UID, Set<UID>> nonCloneableSubformcolumns, final Set<UID> subformReferences) {
		this.nonCloneableFields = nonCloneableFields;
		this.nonCloneableSubformReferences = nonCloneableSubformReferences;
		this.nonCloneableSubformcolumns = nonCloneableSubformcolumns;
		this.subformReferences = subformReferences;
	}

	public Set<UID> getNonCloneableFields() {
		return nonCloneableFields;
	}

	public void setNonCloneableFields(final Set<UID> nonCloneableFields) {
		this.nonCloneableFields = nonCloneableFields;
	}

	public Set<UID> getNonCloneableSubformReferences() {
		return nonCloneableSubformReferences;
	}

	public void setNonCloneableSubformReferences(final Set<UID> nonCloneableSubformReferences) {
		this.nonCloneableSubformReferences = nonCloneableSubformReferences;
	}

	public Map<UID, Set<UID>> getNonCloneableSubformcolumns() {
		return nonCloneableSubformcolumns;
	}

	public void setNonCloneableSubformcolumns(final Map<UID, Set<UID>> nonCloneableSubformcolumns) {
		this.nonCloneableSubformcolumns = nonCloneableSubformcolumns;
	}

	public Set<UID> getSubformReferences() {
		return subformReferences;
	}

	public void setSubformReferences(final Set<UID> subformReferences) {
		this.subformReferences = subformReferences;
	}
}
