package org.nuclos.common2.layoutml;


public interface IWebConstraints extends LayoutMLConstants {
	int MINIMUM_SIZE = 1;
	int PREFERED_SIZE = 2;
	int STRICT_SIZE = 3;
	
	boolean isQuasiEmpty();
	
	void setSize(int type, int width, int height);

}
