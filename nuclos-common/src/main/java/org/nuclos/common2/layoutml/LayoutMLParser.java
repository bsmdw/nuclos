//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common2.layoutml;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.nuclos.common.E;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.layout.LayoutInfo;
import org.nuclos.common2.layoutml.NuclosRuleAction.NuclosRuleClearAction;
import org.nuclos.common2.layoutml.NuclosRuleAction.NuclosRuleRefreshValueListAction;
import org.nuclos.common2.layoutml.NuclosRuleAction.NuclosRuleReinitSubformAction;
import org.nuclos.common2.layoutml.NuclosRuleAction.NuclosRuleTransferLookedUpValueAction;
import org.nuclos.common2.layoutml.exception.LayoutMLException;
import org.nuclos.common2.layoutml.exception.LayoutMLParseException;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;
/**
 * Parser for the LayoutML. Provides methods to validate a LayoutML document and to extract
 * the names of the collectable nonCloneableFields used in the document.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version	01.00.00
 */

public class LayoutMLParser implements LayoutMLConstants {
	
	private static final Logger LOG = Logger.getLogger(LayoutMLParser.class);
	
	protected static final UID getEntityUID(String entity) {
		if (entity == null) {
			return null;
		}
		if (UID.isStringifiedUID(entity)) {
			return UID.parseUID(entity);
		} else {
			LOG.debug(String.format("Found unstringified entityUID \"%s\" in layout", entity));
			return null;
		}
	}
	
	protected static final UID getFieldUID(String field) {
		if (StringUtils.looksEmpty(field)) {
			return null;
		}
		final UID fieldUID;
		if (UID.isStringifiedUID(field)) {
			fieldUID = UID.parseUID(field);
		} else {
			fieldUID = null;
			LOG.warn(String.format("Found unstringified fieldUID \"%s\" in layout", field));
		}
		return fieldUID;
	}
	
	public LayoutMLParser() {
	}
	
	/**
	 * validates the LayoutML document in <code>inputsource</code>.
	 * @param inputsource
	 * @throws org.nuclos.common2.layoutml.exception.LayoutMLParseException when a parse exception occurs.
	 * @throws org.nuclos.common2.layoutml.exception.LayoutMLException when a general exception occurs.
	 * @throws java.io.IOException when an I/O error occurs (rather fatal)
	 */
	public void validate(InputSource inputsource) throws LayoutMLException, IOException {
		final BasicHandler handler = new BasicHandler();
		try {
			this.parse(inputsource, handler);
		}
		catch (SAXParseException ex) {
			throw new LayoutMLParseException(ex);
		}
		catch (SAXException ex) {
			throw new LayoutMLException(ex, handler.getDocumentLocator());
		}
	}

	/**
	 * retrieves the names of the collectable nonCloneableFields used in the LayoutML
	 * document in <code>inputsource</code>.
	 * 
	 * @param inputsource
	 * @return Set&lt;UID&gt; the UIDs of the collectable nonCloneableFields used in the
	 * 		LayoutML document in <code>inputsource</code>.
	 * @throws LayoutMLException when a general exception occurs.
	 */
	public Set<UID> getCollectableFieldUids(InputSource inputsource) throws LayoutMLException {
		return this.getElementUids(inputsource, ELEMENT_COLLECTABLECOMPONENT, ATTRIBUTE_NAME);
	}

	/**
	 * retrieves the names of the subform entities used in the LayoutML 
	 * document in <code>inputsource</code>.
	 * 
	 * @param inputsource
	 * @return Set&lt;UID&gt; the names of the subform entities used in the 
	 * 		LayoutML document in <code>inputsource</code>.
	 * @throws LayoutMLException when a general exception occurs.
	 */
	public Set<UID> getSubFormEntityUids(InputSource inputsource) throws LayoutMLException {
		return this.getElementUids(inputsource, ELEMENT_SUBFORM, ATTRIBUTE_ENTITY);
	}

	/**
	 * retrieves the names of the chart entities used in the LayoutML document 
	 * in <code>inputsource</code>.
	 * 
	 * @param inputsource
	 * @return Set&lt;String&gt; the UIDs of the chart entities used in the 
	 * 		LayoutML document in <code>inputsource</code>.
	 * @throws LayoutMLException when a general exception occurs.
	 */
	public Set<UID> getChartEntityUids(InputSource inputsource) throws LayoutMLException {
		return this.getElementUids(inputsource, ELEMENT_CHART, ATTRIBUTE_ENTITY);
	}
	
	/**
	 * retrieves the names of the subform entities along with their foreign 
	 * key nonCloneableFields used in the LayoutML document in <code>inputsource</code>.
	 * 
	 * @param inputsource
	 * @return Collection&lt;EntityAndField&gt; the names of the collectable 
	 * 		nonCloneableFields used in the LayoutML document in <code>inputsource</code>.
	 * @throws LayoutMLException when a general exception occurs.
	 */
	public Collection<EntityAndField> getSubFormEntityAndForeignKeyFields(InputSource inputsource) throws LayoutMLException {
		final GetSubFormEntityAndForeignKeyFieldHandler handler = new GetSubFormEntityAndForeignKeyFieldHandler();
		try {
			this.parse(inputsource, handler);
			return handler.getValues();
		}
		catch (SAXParseException ex) {
			throw new LayoutMLParseException(ex);
		}
		catch (SAXException ex) {
			throw new LayoutMLException(ex, handler.getDocumentLocator());
		}
		catch (IOException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * retrieves the names of the subform entities along with their parent 
	 * subform entity used in the LayoutML document in <code>inputsource</code>.
	 */
	public Map<EntityAndField, UID> getSubFormEntityAndParentSubFormEntities(InputSource inputsource) throws LayoutMLException {
		final GetSubFormEntityAndParentSubEntityFormHandler handler = new GetSubFormEntityAndParentSubEntityFormHandler();
		try {
			this.parse(inputsource, handler);
			return handler.getValues();
		}
		catch (SAXParseException ex) {
			throw new LayoutMLParseException(ex, handler.getDocumentLocator());
		}
		catch (SAXException ex) {
			throw new LayoutMLException(ex, handler.getDocumentLocator());
		}
		catch (IOException ex) {
			throw new CommonFatalException(ex);
		}
	}
	
	public ParsedLayoutComponents getParsedLayoutComponents(LayoutInfo layoutInfo) throws LayoutMLException {
		final GetWebComponentsHandler handler = new GetWebComponentsHandler(layoutInfo.getLayoutUID(), layoutInfo.isRestrictionsMustIgnoreGroovyRules());
		try {
			this.parse(layoutInfo.layoutInSource(), handler);
			return handler.getParsedLayoutComponents();
		}
		catch (SAXParseException ex) {
			throw new LayoutMLParseException(ex, handler.getDocumentLocator());
		}
		catch (SAXException ex) {
			throw new LayoutMLException(ex, handler.getDocumentLocator());
		}
		catch (IOException ex) {
			throw new CommonFatalException(ex);
		}
	}

	public CloneRelevantLayoutElements getNonCloneableLayoutElements(InputSource inputsource) throws LayoutMLException {
		final GetCloneRelevantLayoutElementsHandler handler = new GetCloneRelevantLayoutElementsHandler();
		try {
			this.parse(inputsource, handler);
			return handler.getCloneRelevantLayoutElements();
		}
		catch (SAXParseException ex) {
			throw new LayoutMLParseException(ex, handler.getDocumentLocator());
		}
		catch (SAXException ex) {
			throw new LayoutMLException(ex, handler.getDocumentLocator());
		}
		catch (IOException ex) {
			throw new CommonFatalException(ex);
		}
	}
	
	public Map<UID, List<NuclosRuleEvent>> getRuleEvents(InputSource inputsource) throws LayoutMLException {
		final GetRuleEventsHandler handler = new GetRuleEventsHandler();
		try {
			this.parse(inputsource, handler);
			return handler.getValues();
		}
		catch (SAXParseException ex) {
			throw new LayoutMLParseException(ex, handler.getDocumentLocator());
		}
		catch (SAXException ex) {
			throw new LayoutMLException(ex, handler.getDocumentLocator());
		}
		catch (IOException ex) {
			throw new CommonFatalException(ex);
		}
	}
		
	/**
	 * retrieves the names of the subform entities along with their parent 
	 * subform entity used in the LayoutML document in <code>inputsource</code>.
	 */
	public String replaceButtonArguments(String sLayoutML, UID sEntity, String newArgument, String oldArgument) throws LayoutMLException {
		final GetButtonArgumentHandler handler = new GetButtonArgumentHandler(sEntity, sLayoutML, newArgument, oldArgument);
		try {
			this.parse(new InputSource(new StringReader(sLayoutML)), handler);
			return handler.getLayout();
		}
		catch (SAXParseException ex) {
			throw new LayoutMLParseException(ex, handler.getDocumentLocator());
		}
		catch (SAXException ex) {
			throw new LayoutMLException(ex, handler.getDocumentLocator());
		}
		catch (IOException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * retrieves the names of the collectable nonCloneableFields used in the LayoutML document in <code>inputsource</code>.
	 * @param sAttribute
	 * @param inputsource
	 * @return the names of the collectable nonCloneableFields used in the LayoutML document in <code>inputsource</code>.
	 * @throws org.nuclos.common2.layoutml.exception.LayoutMLParseException when a parse exception occurs.
	 * @throws org.nuclos.common2.layoutml.exception.LayoutMLException when a general exception occurs.
	 * @throws org.nuclos.common2.exception.CommonFatalException when an I/O error occurs.
	 */
	private Set<String> getElementNames(InputSource inputsource, String sElement, String sAttribute) throws LayoutMLException {
		final GetElementAttributeHandler handler = new GetElementAttributeHandler(sElement, sAttribute);
		try {
			this.parse(inputsource, handler);
			return handler.getValues();
		}
		catch (SAXParseException ex) {
			throw new LayoutMLParseException(ex);
		}
		catch (SAXException ex) {
			throw new LayoutMLException(ex, handler.getDocumentLocator());
		}
		catch (IOException ex) {
			throw new CommonFatalException(ex);
		}
	}
	
	private Set<UID> getElementUids(InputSource inputsource, String sElement, String sAttribute) throws LayoutMLException {
		final GetElementUIDHandler handler = new GetElementUIDHandler(sElement, sAttribute);
		try {
			this.parse(inputsource, handler);
			return handler.getValues();
		}
		catch (SAXParseException ex) {
			throw new LayoutMLParseException(ex);
		}
		catch (SAXException ex) {
			throw new LayoutMLException(ex, handler.getDocumentLocator());
		}
		catch (IOException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * WORKAROUND for memory leak in SAXParser.parse().
	 * @param inputsource
	 * @param handler
	 * @throws SAXException
	 * @throws IOException
	 */
	protected final void parse(InputSource inputsource, final DefaultHandler handler) throws SAXException, IOException {
		this.getParser().parse(inputsource, handler);
	}

	/**
	 * @return a new validating SAXParser. This parser cannot be reused, because of a bug in the
	 * used SAX implementation, the handler that is given to SAXParser.parse is not released.
	 * This causes a memory leak. So, do not cache the returned parser.
	 */
	private SAXParser getParser() {
		final SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setValidating(true);
		try {
			return factory.newSAXParser();
		}
		catch (ParserConfigurationException ex) {
			throw new CommonFatalException(ex);
		}
		catch (SAXException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * inner class BasicHandler.
	 */
	protected static class BasicHandler extends DefaultHandler {
		private Locator locator;

		/**
		 * finds the location of the bundled DTD for the LayoutML
		 * @param sPublicId
		 * @param sSystemId
		 * @return
		 * @throws org.xml.sax.SAXException
		 */
		@Override
		public InputSource resolveEntity(String sPublicId, String sSystemId) throws SAXException {
			final String sMessage = "Could not find the DTD for the LayoutML.";
			try {
				InputSource result = null;
				if (sSystemId != null && sSystemId.equals(LAYOUTML_DTD_SYSTEMIDENTIFIER)) {
					final ClassLoader cl = LangUtils.getClassLoaderThatWorksForWebStart();
					final URL urlDtd = cl.getResource(LAYOUTML_DTD_RESSOURCEPATH);
					if (urlDtd == null) {
						throw new SAXException(sMessage);
					}
					result = new InputSource(new BufferedInputStream(urlDtd.openStream()));
				}
				return result;
			}
			catch (FileNotFoundException ex) {
				throw new SAXException(sMessage, ex);
			}
			catch (IOException ex) {
				throw new SAXException(sMessage, ex);
			}
		}

		/**
		 * This method is called by the SAX parser to provide a locator for this handler.
		 * @param locator
		 * @see #getDocumentLocator()
		 */
		@Override
		public void setDocumentLocator(Locator locator) {
			this.locator = locator;
		}

		/**
		 * @return a Locator reflecting the current location of the parser within the document. Should be non-null,
		 * but that depends on the SAX parser so don't rely on it.
		 */
		public Locator getDocumentLocator() {
			return this.locator;
		}

		/**
		 * called by the underlying SAX parser when a "regular" error occurs.
		 * Just throws <code>ex</code>, so the parser can take correspondent actions.
		 * @param ex
		 * @throws org.xml.sax.SAXException
		 */
		@Override
		public void error(SAXParseException ex) throws SAXException {
			throw ex;
		}

		/**
		 * called by the underlying SAX parser when a fatal error occurs.
		 * This method just calls its super method.
		 * @param ex
		 * @throws org.xml.sax.SAXException
		 */
		@Override
		public void fatalError(SAXParseException ex) throws SAXException {
			super.fatalError(ex);
		}

		protected static Boolean getBooleanValue(Attributes attributes, String sAttributeName) {
			return Boolean.TRUE.equals(getBooleanValueNullable(attributes, sAttributeName));
		}

		protected static Boolean getBooleanValueNullable(Attributes attributes, String sAttributeName) {
			final String bValue = attributes.getValue(sAttributeName);
			if (bValue != null) {
				if (bValue.equals(ATTRIBUTEVALUE_YES)) {
					return true;
				} else {
					return false;
				}
			}
			return null;
		}

		/**
		 * convenience method: gets an int value from an XML attribute.
		 * @param attributes the XML attributes of an XML element
		 * @param sAttributeName the name of the attribute to query
		 * @param iDefault the default value that is taken if there is no attribute with the given name
		 * @return the int value of the attribute or the default, if no matching attribute was found
		 */
		protected static int getIntValue(Attributes attributes, String sAttributeName, int iDefault) throws SAXException {
			final String sValue = attributes.getValue(sAttributeName);
			try {
				return (sValue == null) ? iDefault : Integer.parseInt(sValue);
			}
			catch (NumberFormatException ex) {
				throw new SAXException(StringUtils.getParameterizedExceptionMessage("LayoutMLParser.14", sAttributeName));
					//"Ganze Zahl erwartet f\u00fcr Attribut \"" + sAttributeName + "\".");
			}
		}

		/**
		 * convenience method: gets an Integer value from an XML attribute.
		 * @param attributes the XML attributes of an XML element
		 * @param sAttributeName the name of the attribute to query
		 * @return the Integer value of the attribute or null, if no matching attribute was found
		 */
		protected static Integer getIntegerValue(Attributes attributes, String sAttributeName) throws SAXException {
			final String sValue = attributes.getValue(sAttributeName);
			try {
				return (sValue == null) ? null : new Integer(sValue);
			}
			catch (NumberFormatException ex) {
				throw new SAXException(StringUtils.getParameterizedExceptionMessage("LayoutMLParser.14", sAttributeName));
					//"Ganze Zahl erwartet f\u00fcr Attribut \"" + sAttributeName + "\".");
			}
		}

		/**
		 * convenience method: gets a double value from an XML attribute.
		 * @param attributes the XML attributes of an XML element
		 * @param sAttributeName the name of the attribute to query
		 * @param dDefault the default value that is taken if there is no attribute with the given name
		 * @return the double value of the attribute or the default, if no matching attribute was found
		 */
		protected static double getDoubleValue(Attributes attributes, String sAttributeName, double dDefault)
				throws SAXException {
			final String sValue = attributes.getValue(sAttributeName);
			try {
				return (sValue == null) ? dDefault : Double.parseDouble(sValue);
			}
			catch (NumberFormatException ex) {
				throw new SAXException(StringUtils.getParameterizedExceptionMessage("LayoutMLParser.15", sAttributeName));
					//"Dezimalzahl erwartet f\u00fcr Attribut \"" + sAttributeName + "\".");
			}
		}


	}  // inner class BasicHandler

	/**
	 * inner class GetElementAttributeHandler.
	 * Gets the values of the given attribute in the given element occuring in the parsed LayoutML definition.
	 */
	private class GetElementAttributeHandler extends BasicHandler {
		private final String sElement;
		private final String sAttribute;

		private final Set<String> stValues = new HashSet<String>();

		GetElementAttributeHandler(String sElement, String sAttribute) {
			this.sElement = sElement;
			this.sAttribute = sAttribute;
		}

		/**
		 * called by the underlying SAX parser when a start element event occurs.
		 * Only regards <code>sElement</code> elements and collects their names.
		 * @param sUriNameSpace
		 * @param sSimpleName
		 * @param sQualifiedName
		 * @param attributes
		 * @throws org.xml.sax.SAXException
		 */
		@Override
		public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
				throws SAXException {
			if (sQualifiedName.equals(this.sElement)) {
				this.stValues.add(attributes.getValue(this.sAttribute));
			}
		}

		/**
		 * @return Set<String>
		 */
		public Set<String> getValues() {
			return Collections.unmodifiableSet(this.stValues);
		}

	}  // class GetElementAttributeHandler
	
	/**
	 * inner class GetElementUIDHandler.
	 * Gets the uid values of the given attribute in the given element occuring in the parsed LayoutML definition.
	 */
	private class GetElementUIDHandler extends BasicHandler {
		private final String sElement;
		private final String sAttribute;

		private final Set<UID> stValues = new HashSet<UID>();

		GetElementUIDHandler(String sElement, String sAttribute) {
			this.sElement = sElement;
			this.sAttribute = sAttribute;
		}

		/**
		 * called by the underlying SAX parser when a start element event occurs.
		 * Only regards <code>sElement</code> elements and collects their names.
		 * @param sUriNameSpace
		 * @param sSimpleName
		 * @param sQualifiedName
		 * @param attributes
		 * @throws org.xml.sax.SAXException
		 */
		@Override
		public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
				throws SAXException {
			if (sQualifiedName.equals(this.sElement)) {
				this.stValues.add(UID.parseUID(attributes.getValue(this.sAttribute)));
			}
		}

		/**
		 * @return Set<UID>
		 */
		public Set<UID> getValues() {
			return Collections.unmodifiableSet(this.stValues);
		}

	}  // class GetElementAttributeHandler

	/**
	 * inner class GetSubFormEntityAndForeignKeyFieldHandler.
	 */
	private class GetSubFormEntityAndForeignKeyFieldHandler extends BasicHandler {

		private final Set<EntityAndField> stValues = new HashSet<EntityAndField>();

		/**
		 * called by the underlying SAX parser when a start element event occurs.
		 * @param sUriNameSpace
		 * @param sSimpleName
		 * @param sQualifiedName
		 * @param attributes
		 * @throws org.xml.sax.SAXException
		 */
		@Override
		public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
				throws SAXException {
			if (sQualifiedName.equals(ELEMENT_SUBFORM)) {
				final UID entity = getEntityUID(attributes.getValue(ATTRIBUTE_ENTITY));
				final UID foreignKeyField = getFieldUID(attributes.getValue(ATTRIBUTE_FOREIGNKEYFIELDTOPARENT));
				this.stValues.add(new EntityAndField(entity, foreignKeyField));
			}
			if (sQualifiedName.equals(ELEMENT_CHART)) {
				final UID entity = getEntityUID(attributes.getValue(ATTRIBUTE_ENTITY));
				final UID foreignKeyField = getFieldUID(attributes.getValue(ATTRIBUTE_FOREIGNKEYFIELDTOPARENT));
				this.stValues.add(new EntityAndField(entity, foreignKeyField));
			}
			if(sQualifiedName.equals(ELEMENT_MATRIX)) {
				final UID entity = getEntityUID(attributes.getValue(ATTRIBUTE_ENTITY_Y));
				final UID foreignKeyField = getFieldUID(attributes.getValue(ATTRIBUTE_ENTITY_Y_PARENT_FIELD));
				this.stValues.add(new EntityAndField(entity, foreignKeyField));
				final UID uidMatrixEntity = getEntityUID(attributes.getValue(ATTRIBUTE_ENTITY_MATRIX));
				final UID uidMatrixField = getFieldUID(attributes.getValue(ATTRIBUTE_MATRIX_VALUE_FIELD));
				this.stValues.add(new EntityAndField(uidMatrixEntity, uidMatrixField));
			}
		}

		public Set<EntityAndField> getValues() {
			return Collections.unmodifiableSet(this.stValues);
		}

	}  // class GetSubFormEntityAndForeignKeyFieldHandler

	/**
	 * inner class GetSubFormEntityAndSubFormParentHandler.
	 */
	private class GetSubFormEntityAndParentSubEntityFormHandler extends BasicHandler {

		private final Map<EntityAndField, UID> mpValues = new HashMap<EntityAndField, UID>();

		/**
		 * called by the underlying SAX parser when a start element event occurs.
		 * @param sUriNameSpace
		 * @param sSimpleName
		 * @param sQualifiedName
		 * @param attributes
		 * @throws org.xml.sax.SAXException
		 */
		@Override
		public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
				throws SAXException {
			if (sQualifiedName.equals(ELEMENT_SUBFORM)) {
				final UID entity = getEntityUID(attributes.getValue(ATTRIBUTE_ENTITY));
				final UID foreignKeyField = getFieldUID(attributes.getValue(ATTRIBUTE_FOREIGNKEYFIELDTOPARENT));
						
				this.mpValues.put(new EntityAndField(entity, foreignKeyField), 
						attributes.getValue(ATTRIBUTE_PARENTSUBFORM) == null ? null : getEntityUID(attributes.getValue(ATTRIBUTE_PARENTSUBFORM)));
			}
			if (sQualifiedName.equals(ELEMENT_MATRIX)) {
				final UID matrixEntity = getEntityUID(attributes.getValue(ATTRIBUTE_ENTITY_MATRIX));
				final UID foreignKeyField = getFieldUID(attributes.getValue(ATTRIBUTE_MATRIX_PARENT_FIELD));
				final UID parentSubForm = getEntityUID(attributes.getValue(ATTRIBUTE_ENTITY_Y));
				this.mpValues.put(new EntityAndField(matrixEntity, foreignKeyField), parentSubForm);
			}
		}

		public Map<EntityAndField, UID> getValues() {
			return Collections.unmodifiableMap(this.mpValues);
		}

	}  // class GetSubFormEntityAndSubFormParentHandler
	
	
	/**
	 * inner class GetButtonArgumentHandler.
	 */
	private class GetButtonArgumentHandler extends BasicHandler {

		private static final String PROPERTY_RULE = "ruletoexecute";
		private static final String PROPERTY_STATE = "targetState";
		private static final String PROPERTY_GENERATOR = "generatortoexecute";
		
		private String sLayout;
		private final String sArgNew;
		private final String sArgOld;
		private final String sPropReplacement;
		
		public GetButtonArgumentHandler(UID sEntity, String sLayout, String sArgNew, String sArgOld) {
			this.sLayout = sLayout;
			this.sArgNew = sArgNew;
			this.sArgOld = sArgOld;
			
			if (sEntity.equals(E.SERVERCODE.getUID()))
				sPropReplacement = PROPERTY_RULE;
			else if (sEntity.equals(E.GENERATION.getUID()))
				sPropReplacement = PROPERTY_GENERATOR;
			else if (sEntity.equals(E.STATE.getUID()))
				sPropReplacement = PROPERTY_STATE;
			else
				throw new NuclosFatalException();
		}
		
		public String getLayout() {
			return sLayout;
		}
		
		/**
		 * called by the underlying SAX parser when a start element event occurs.
		 * @param sUriNameSpace
		 * @param sSimpleName
		 * @param sQualifiedName
		 * @param attributes
		 * @throws org.xml.sax.SAXException
		 */
		@Override
		public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
				throws SAXException {
			if (sQualifiedName.equals(ELEMENT_PROPERTY)) {
				final String sName = attributes.getValue(ATTRIBUTE_NAME);
				if (sName.equals(sPropReplacement)) {
					final String sValue = attributes.getValue(ATTRIBUTE_VALUE);
					if (sValue.equals(sArgOld)) {
						final String sOld = ELEMENT_PROPERTY + " " + ATTRIBUTE_NAME + "=\"" + sPropReplacement + "\" " + ATTRIBUTE_VALUE + "=\"" + sArgOld + "\"";
						final String sNew = ELEMENT_PROPERTY + " " + ATTRIBUTE_NAME + "=\"" + sPropReplacement + "\" " + ATTRIBUTE_VALUE + "=\"" + sArgNew + "\"";
						
						final String s = sLayout;
						sLayout = s.replaceAll(sOld, sNew);
						if (LangUtils.equal(sLayout, s)) {
							final StringBuffer sb = new StringBuffer();
							int idx = sLayout.indexOf(sOld);
							sb.append(sLayout.substring(0, idx));
							sb.append(sNew);
							sb.append(sLayout.substring(idx + sNew.length()));
							sLayout = sb.toString();
						}
					}
				}
			}
		}
	}  // class GetButtonArgumentHandler
	
	private static class GetWebComponentsHandler extends BasicHandler {

		private final Map<UID, List<AbstractWebComponent>> mpComps = new HashMap<UID, List<AbstractWebComponent>>();
		private final Map<UID, WebStaticComponent> mpStaticComps = new HashMap<UID, WebStaticComponent>();
		
		private final Stack<AbstractWebComponent> stkComps = new Stack<AbstractWebComponent>();
		private final Stack<IWebContainer> stkContainer = new Stack<IWebContainer>();
		private final Stack<IHasTableLayout> stkHasLayout = new Stack<IHasTableLayout>();
		
		private final Stack<WebSubform> stkSubforms = new Stack<WebSubform>();
		private final Stack<NuclosSubformColumn> stkSubformColumns = new Stack<NuclosSubformColumn>();
		private final Stack<ISupportsValueListProvider> stkSupportsVLPs = new Stack<ISupportsValueListProvider>();
		private final Stack<WebValueListProvider> stkVLPs = new Stack<WebValueListProvider>();

		private final Map<UID, List<NuclosRuleEvent>> mpRuleEvents = new HashMap<UID, List<NuclosRuleEvent>>();
		private NuclosRuleEvent lastRuleEvent;
		
		private final WebRoot webRoot;
		
		private GetWebComponentsHandler(UID layoutUID, boolean restrictionsMustIgnoreGroovyRules) {
			webRoot = new WebRoot(layoutUID);
			webRoot.setRestrictionsMustIgnoreGroovyRules(restrictionsMustIgnoreGroovyRules);
		}

		@Override
		public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
				throws SAXException {
			if (sQualifiedName.equals(ELEMENT_PANEL)) {
				WebPanel webPanel = new WebPanel(attributes);
				addWebComponent(webPanel);
				stkContainer.push(webPanel);
				stkHasLayout.push(webPanel);
				
			} else if (sQualifiedName.equals(ELEMENT_TABLELAYOUTCONSTRAINTS)) {
				if (!stkComps.empty()) {
					stkComps.peek().setConstraints(new WebTableLayoutConstraints(attributes));
				}
				
			} else if (sQualifiedName.equals(ELEMENT_MINIMUMSIZE)) {
				if (!stkComps.empty()) {
					stkComps.peek().setSize(IWebConstraints.MINIMUM_SIZE, attributes);
				}
				
			} else if (sQualifiedName.equals(ELEMENT_PREFERREDSIZE)) {
				if (!stkComps.empty()) {
					stkComps.peek().setSize(IWebConstraints.PREFERED_SIZE, attributes);
				}
				
			} else if (sQualifiedName.equals(ELEMENT_STRICTSIZE)) {
				if (!stkComps.empty()) {
					stkComps.peek().setSize(IWebConstraints.STRICT_SIZE, attributes);
				}
				
			} else if (sQualifiedName.equals(ELEMENT_TABLELAYOUT)) {
				if (!stkHasLayout.empty()) {
					stkHasLayout.peek().setWebTableLayout(new WebTableLayout(attributes));
				}
				
			} else if (sQualifiedName.equals(ELEMENT_EMPTYBORDER)) {
				if (!stkHasLayout.empty()) {
					stkHasLayout.peek().setCompBorder(new EmptyBorder(attributes));
				}
				
			} else if (sQualifiedName.equals(ELEMENT_TITLEDBORDER)) {
				if (!stkHasLayout.empty()) {
					stkHasLayout.peek().setCompBorder(new TitledBorder(attributes));
				}
				
			} else if (sQualifiedName.equals(ELEMENT_ETCHEDBORDER)) {
				if (!stkHasLayout.empty()) {
					stkHasLayout.peek().setCompBorder(new EtchedBevelBorder("etched", attributes));
				}
				
			} else if (sQualifiedName.equals(ELEMENT_BEVELBORDER)) {
				if (!stkHasLayout.empty()) {
					stkHasLayout.peek().setCompBorder(new EtchedBevelBorder("bevel", attributes));
				}
				
			} else if (sQualifiedName.equals(ELEMENT_LINEBORDER)) {
				if (!stkHasLayout.empty()) {
					stkHasLayout.peek().setCompBorder(new LineBorder(attributes));
				}
												
			} else if (sQualifiedName.equals(ELEMENT_BACKGROUND)) {
				if (!stkHasLayout.empty()) {
					stkHasLayout.peek().setBackground("rgb(" + attributes.getValue(ATTRIBUTE_RED) + ", " + attributes.getValue(ATTRIBUTE_GREEN) + ", " + attributes.getValue(ATTRIBUTE_BLUE) + ")");
					stkComps.peek().setBackground("rgb(" + attributes.getValue(ATTRIBUTE_RED) + ", " + attributes.getValue(ATTRIBUTE_GREEN) + ", " + attributes.getValue(ATTRIBUTE_BLUE) + ")");
				}
				
			} else if (sQualifiedName.equals(ELEMENT_SPLITPANE)) {
				WebSplitPane splitPane = new WebSplitPane(attributes);
				addWebComponent(splitPane);
				stkContainer.push(splitPane);

			} else if (sQualifiedName.equals(ELEMENT_SPLITPANECONSTRAINTS)) {
				if (!stkComps.empty()) {
					stkComps.peek().setConstraints(new WebSplitpaneConstraints(attributes));
				}
				
			} else if (sQualifiedName.equals(ELEMENT_TABBEDPANE)) {
				WebTabbedPane tabbedPane = new WebTabbedPane(attributes);
				addWebComponent(tabbedPane);
				stkContainer.push(tabbedPane);
				
			} else if (sQualifiedName.equals(ELEMENT_TABBEDPANECONSTRAINTS)) {
				if (!stkComps.empty()) {
					stkComps.peek().setConstraints(new WebTabbedpaneConstraints(attributes));
				}
				
			} else if (sQualifiedName.equals(ELEMENT_LABEL)) {
				addStaticComponent(sQualifiedName, attributes);
				
			} else if (sQualifiedName.equals(ELEMENT_TITLEDSEPARATOR)) {
				addStaticComponent(sQualifiedName, attributes);
				
			} else if (sQualifiedName.equals(ELEMENT_SEPARATOR)) {
				addStaticComponent(sQualifiedName, attributes);
				
			} else if (sQualifiedName.equals(ELEMENT_BUTTON)) {
				addStaticComponent(sQualifiedName, attributes);
							
			} else if (sQualifiedName.equals(ELEMENT_COLLECTABLECOMPONENT)) {
				WebCollectableComponent collectableComp = new WebCollectableComponent(attributes);
				addWebComponent(collectableComp);
				
				//This is important to be outside of "isJustLabel()"
				stkSupportsVLPs.push(collectableComp);
				
				if (!collectableComp.isJustLabel()) {
					addNuclosComponentToMap(collectableComp.getName(), collectableComp);
				}
				
			} else if (sQualifiedName.equals(ELEMENT_SUBFORM)) {
				WebSubform nuclosSubform = new WebSubform(attributes);
				addWebComponent(nuclosSubform);
				stkSubforms.push(nuclosSubform);
				if (nuclosSubform.getForeignkeyfield() != null) {
					addNuclosComponentToMap(nuclosSubform.getEntity(), nuclosSubform);					
				}
				
			} else if (sQualifiedName.equals(ELEMENT_SUBFORMCOLUMN)) {
				if (!stkSubforms.empty()) {
					WebSubform subform = stkSubforms.peek();
					NuclosSubformColumn subformColumn = new NuclosSubformColumn(attributes, subform.getEntity());
					stkSupportsVLPs.push(subformColumn);
					subform.addSubformColumn(subformColumn);
					stkSubformColumns.push(subformColumn);
				}
				
			} else if (sQualifiedName.equals(ELEMENT_CHART)) {
				WebChart nuclosChart = new WebChart(attributes);
				addWebComponent(nuclosChart);
				if (nuclosChart.getForeignkeyfield() != null) {
					addNuclosComponentToMap(nuclosChart.getEntity(), nuclosChart);					
				}
				
			} else if (sQualifiedName.equals(ELEMENT_MATRIX)) {
				WebMatrix matrix = new WebMatrix(attributes);
				if (matrix.getForeignkeyfield() != null) {
					addNuclosComponentToMap(matrix.getUID(), matrix);					
				}
				addWebComponent(matrix);

			} else if (sQualifiedName.equals(ELEMENT_PROPERTY)) {				
				if (!stkComps.empty()) {
					String name = attributes.getValue(ATTRIBUTE_NAME);
					String value = attributes.getValue(ATTRIBUTE_VALUE);
					if (!stkSubformColumns.empty()) {
						stkSubformColumns.peek().addProperty(name, value);
					} else {
						stkComps.peek().addProperty(name, value);					
					}
				}
				
			} else if (sQualifiedName.equals(ELEMENT_VALUELISTPROVIDER)) {
				if (!stkSupportsVLPs.empty()) {
					ISupportsValueListProvider supportsVLP = stkSupportsVLPs.peek();
					WebValueListProvider valueListProvider = new WebValueListProvider(attributes);
					stkVLPs.push(valueListProvider);
					supportsVLP.setValueListProvider(valueListProvider);
				}
				
			} else if (sQualifiedName.equals(ELEMENT_PARAMETER)) {
				if (!stkVLPs.empty()) {
					stkVLPs.peek().addParameter(attributes);
				}
				
			} else if (isLayoutGroovyElement(sQualifiedName)) {
				sbChars = new StringBuilder();
			} else {
				lastRuleEvent = parseRules(sQualifiedName, attributes, lastRuleEvent, mpRuleEvents);
			}
		}
		
		private void addStaticComponent(String type, Attributes attributes) {
			WebStaticComponent wsComp = new WebStaticComponent(type, attributes);
			addWebComponent(wsComp);
			mpStaticComps.put(wsComp.getUID(), wsComp);
		}
		
		private void addWebComponent(AbstractWebComponent comp) {
			stkComps.push(comp);
			if (stkContainer.empty()) {
				stkContainer.add(webRoot);
			}
			IWebContainer container = stkContainer.peek();
			container.addComponent(comp);
			comp.setContainer(container);
		}
		
		private void addNuclosComponentToMap(UID key, AbstractWebComponent nuclosComponent) {
			if (!mpComps.containsKey(key)) {
				mpComps.put(key, new ArrayList<AbstractWebComponent>());
			}
			mpComps.get(key).add(nuclosComponent);
		}
		
		@Override
		public void endElement(String sUriNameSpace, String sSimpleName, String sQualifiedName)
				throws SAXException {
			if (sQualifiedName.equals(ELEMENT_PANEL)) {
				stkContainer.pop();
				
			} else if (sQualifiedName.equals(ELEMENT_SPLITPANE)) {
				stkContainer.pop();
				
			} else if (sQualifiedName.equals(ELEMENT_TABBEDPANE)) {
				stkContainer.pop();

			} else if (sQualifiedName.equals(ELEMENT_SUBFORMCOLUMN)) {
				stkSubformColumns.pop();
				
			} else if (isLayoutGroovyElement(sQualifiedName)) {
				String reduced = StringUtils.reduceWhiteSpacesAndDeleteOtherNonLetterOrDigit(sbChars.toString());
				if (reduced != null && !reduced.isEmpty()) {
					//NUCLOS-4718
					boolean trivialTrue = "return true".equals(reduced) || "true".equals(reduced);
					boolean trivialFalse = "return false".equals(reduced) || "false".equals(reduced);
					boolean elementEnabled = ELEMENT_ENABLED.equals(sQualifiedName);
					
					if (trivialTrue && elementEnabled) {
						stkComps.peek().setForceDisable(false);
					} else if (trivialTrue) {
						stkComps.peek().addProperty(StringUtils.reduceWhiteSpacesAndDeleteOtherNonLetterOrDigit(sQualifiedName), "true");
					} else if (trivialFalse && elementEnabled) {
						stkComps.peek().setForceDisable(true);						
					} else if (trivialFalse) {
						stkComps.peek().addProperty(StringUtils.reduceWhiteSpacesAndDeleteOtherNonLetterOrDigit(sQualifiedName), "false");						
					} else if (!webRoot.isRestrictionsMustIgnoreGroovyRules()) {
						stkComps.peek().setForceDisable(true);						
					}
				}
				sbChars = null;
			}
		}
		
		private StringBuilder sbChars;
		@Override
		public void characters(char[] ac, int start, int length) throws SAXException {
			if (this.sbChars != null) {
				this.sbChars.append(ac, start, length);
			}
		}

		public ParsedLayoutComponents getParsedLayoutComponents() {
			return new ParsedLayoutComponents(mpComps, mpStaticComps, mpRuleEvents, webRoot);
		}
		
		private static boolean isLayoutGroovyElement(String sQualifiedName) {
			if (sQualifiedName.equals(ELEMENT_ENABLED)) {
				return true;
			} else if (sQualifiedName.equals(ELEMENT_NEW_ENABLED)) {
				return true;
			} else if (sQualifiedName.equals(ELEMENT_EDIT_ENABLED)) {
				return true;
			} else if (sQualifiedName.equals(ELEMENT_DELETE_ENABLED)) {
				return true;
			} else if (sQualifiedName.equals(ELEMENT_CLONE_ENABLED)) {
				return true;
			} else if (sQualifiedName.equals(ELEMENT_DYNAMIC_ROW_COLOR)) {
				return true;
			}
			return false;
		}
	}  // class GetComponentsHandler

	private static class GetCloneRelevantLayoutElementsHandler extends BasicHandler {

		private Set<UID> nonCloneableFields = new HashSet<>();
		private Set<UID> nonCloneableSubformReferences = new HashSet<>();
		private Map<UID, Set<UID>> mpNonCloneableSubformcolumns = new HashMap<>();
		private Set<UID> subformReferences = new HashSet<>();

		private final Stack<UID> stkSubforms = new Stack<>();

		@Override
		public void startElement(final String uri, final String localName, final String qName, final Attributes attributes) throws SAXException {
			if (qName.equals(ELEMENT_COLLECTABLECOMPONENT) && ATTRIBUTEVALUE_CONTROL.equals(attributes.getValue(ATTRIBUTE_SHOWONLY))) {
				String sNotCloneable = attributes.getValue(ATTRIBUTE_NOT_CLONEABLE);
				if (sNotCloneable != null && sNotCloneable.equals(ATTRIBUTEVALUE_YES)) {
					nonCloneableFields.add(UID.parseUID(attributes.getValue(ATTRIBUTE_NAME)));
				}
			} else if (qName.equals(ELEMENT_SUBFORM)) {
				UID uidSubformReference = UID.parseUID(attributes.getValue(ATTRIBUTE_FOREIGNKEYFIELDTOPARENT));
				stkSubforms.push(uidSubformReference);
				String sNotCloneable = attributes.getValue(ATTRIBUTE_NOT_CLONEABLE);
				if (sNotCloneable != null && sNotCloneable.equals(ATTRIBUTEVALUE_YES)) {
					nonCloneableSubformReferences.add(uidSubformReference);
				}
				subformReferences.add(uidSubformReference);
			} else if (qName.equals(ELEMENT_SUBFORMCOLUMN)) {
				UID uidSubform = stkSubforms.peek();
				if (!nonCloneableSubformReferences.contains(uidSubform)) {
					String sNotCloneable = attributes.getValue(ATTRIBUTE_NOT_CLONEABLE);
					if (sNotCloneable != null && sNotCloneable.equals(ATTRIBUTEVALUE_YES)) {
						Set<UID> subformcolumns = mpNonCloneableSubformcolumns.get(uidSubform);
						if (subformcolumns == null) {
							subformcolumns = new HashSet<>();
							mpNonCloneableSubformcolumns.put(uidSubform, subformcolumns);
						}
						subformcolumns.add(UID.parseUID(attributes.getValue(ATTRIBUTE_NAME)));
					}
				}
			}
		}

		@Override
		public void endElement(final String uri, final String localName, final String qName) throws SAXException {
			if (qName.equals(ELEMENT_SUBFORM)) {
				stkSubforms.pop();
			}
		}

		public CloneRelevantLayoutElements getCloneRelevantLayoutElements() {
			return new CloneRelevantLayoutElements(nonCloneableFields, nonCloneableSubformReferences, mpNonCloneableSubformcolumns, subformReferences);
		}
	}
	
	private static NuclosRuleEvent parseRules(String sQualifiedName, Attributes attributes,
			NuclosRuleEvent lastRuleEvent, Map<UID, List<NuclosRuleEvent>> mpRuleEvents) {
		if (sQualifiedName.equals(ELEMENT_RULE)) {
		}  else if (sQualifiedName.equals(ELEMENT_RULES)) {
		}  else if (sQualifiedName.equals(ELEMENT_EVENT)) {
			NuclosRuleEvent ruleEvent = new NuclosRuleEvent(attributes);
			//if the rule isn't just for sub-forms. If this is null, while being a layout-ml rule, it's for the main entity.
			//Null is allowed as key in the HashMap.
			UID subform = ruleEvent.getSubform();
			if (!mpRuleEvents.containsKey(subform)) {
				mpRuleEvents.put(subform, new ArrayList<NuclosRuleEvent>());
			}
			mpRuleEvents.get(subform).add(ruleEvent);
			lastRuleEvent = ruleEvent;
		}  else if (sQualifiedName.equals(ELEMENT_REFRESHVALUELIST)) {
			if (lastRuleEvent != null) {
				lastRuleEvent.addRuleAction(new NuclosRuleRefreshValueListAction(attributes));
			}
		} else if (sQualifiedName.equals(ELEMENT_REINIT_SUBFORM)) {
			if (lastRuleEvent != null) {
				lastRuleEvent.addRuleAction(new NuclosRuleReinitSubformAction(attributes));
			}
		} else if (sQualifiedName.equals(ELEMENT_CLEAR)) {
			if (lastRuleEvent != null) {
				lastRuleEvent.addRuleAction(new NuclosRuleClearAction(attributes));
			}
		}  else if (sQualifiedName.equals(ELEMENT_TRANSFERLOOKEDUPVALUE)) {
			if (lastRuleEvent != null) {
				lastRuleEvent.addRuleAction(new NuclosRuleTransferLookedUpValueAction(attributes));
			}				
		}
		return lastRuleEvent;
	}
	
	private static class GetRuleEventsHandler extends BasicHandler {

		private final Map<UID, List<NuclosRuleEvent>> mpRuleEvents = new HashMap<UID, List<NuclosRuleEvent>>();
		private NuclosRuleEvent lastRuleEvent;

		@Override
		public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
				throws SAXException {
			lastRuleEvent = parseRules(sQualifiedName, attributes, lastRuleEvent, mpRuleEvents);
		}

		public Map<UID, List<NuclosRuleEvent>> getValues() {
			return Collections.unmodifiableMap(this.mpRuleEvents);
		}
	}  // class GetRulesHandler
	
}  // class LayoutMLParser