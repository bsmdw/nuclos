package org.nuclos.common2.layoutml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.nuclos.common.UID;
import org.nuclos.common2.layoutml.NuclosRuleAction.NuclosCheckLayoutChangeAction;
import org.xml.sax.Attributes;

public class NuclosRuleEvent implements LayoutMLConstants {
	private final String type;
	private final UID sourceComponent;
	private final UID subform;
	private final List<NuclosRuleAction> ruleActions;
	
	NuclosRuleEvent(Attributes attributes) {
		this(attributes.getValue(ATTRIBUTE_TYPE), 
				UID.parseUID(attributes.getValue(ATTRIBUTE_SOURCECOMPONENT)),
				UID.parseUID(attributes.getValue(ATTRIBUTE_ENTITY)));
	}
	
	private NuclosRuleEvent(String type, UID sourceComponent, UID subform) {
		this.type = type;
		this.sourceComponent = sourceComponent;
		this.subform = subform;
		this.ruleActions = new ArrayList<NuclosRuleAction>();
	}

	//ATTRIBUTEVALUE_LOOKUP
	//ATTRIBUTEVALUE_VALUECHANGED
	public String getType() {
		return type;
	}

	public UID getSourceComponent() {
		return sourceComponent;
	}

	public UID getSubform() {
		return subform;
	}
	
	public void addRuleAction(NuclosRuleAction action) {
		ruleActions.add(action);
	}
	
	public List<NuclosRuleAction> getRuleActions() {
		return Collections.unmodifiableList(ruleActions);
	}
	
	public Set<UID> getTargetsOfValueChangingActions() {
		Set<UID> targets = new HashSet<UID>();

		if (ruleActions != null && !ruleActions.isEmpty()) {
			for (NuclosRuleAction action : ruleActions) {
				if (action.willChangeValue() && action.getTargetComponent() != null) {
					targets.add(action.getTargetComponent());
				}
			}
		}
		
		return targets;
	}
	
	public static NuclosRuleEvent createCheckLayoutChangeRule(UID sourceComponent) {
		NuclosRuleEvent ruleEvent = new NuclosRuleEvent(ATTRIBUTEVALUE_VALUECHANGED, sourceComponent, null);
		ruleEvent.addRuleAction(new NuclosCheckLayoutChangeAction());
		return ruleEvent;
	}
	
	@Override
	public String toString() {
		return "Type=" + type + " Actions=" + ruleActions;
	}
}
