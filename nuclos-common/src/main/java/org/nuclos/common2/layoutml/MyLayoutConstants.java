package org.nuclos.common2.layoutml;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;

public final class MyLayoutConstants {
	private static final Logger LOG = Logger.getLogger(MyLayoutConstants.class);

	private MyLayoutConstants() {
	}
	
	public static final Map<String, Integer> MPSWINGCONSTANTSORIENTATIONORIENTATION = new HashMap<String, Integer>();
	public static final Map<String, Integer> MPTOOLBARORIENTATION = new HashMap<String, Integer>();
	public static final Map<String, Integer> MPSCROLLPANE = new HashMap<String, Integer>();
	public static final Map<String, Integer> MPSWINGCONSTANTSSCROLLPANE = new HashMap<String, Integer>();
	
	static {
		MPSWINGCONSTANTSORIENTATIONORIENTATION.put("horizontal", javax.swing.SwingConstants.HORIZONTAL);
		MPSWINGCONSTANTSORIENTATIONORIENTATION.put("vertical", javax.swing.SwingConstants.VERTICAL);

		MPTOOLBARORIENTATION.putAll(MPSWINGCONSTANTSORIENTATIONORIENTATION);
		MPTOOLBARORIENTATION.put("hide", -1);

		// constants for Separator "scrollpane" parameter:
		MPSWINGCONSTANTSSCROLLPANE.put("horizontal", 0);
		MPSWINGCONSTANTSSCROLLPANE.put("vertical", 1);
		MPSWINGCONSTANTSSCROLLPANE.put("both", 2);

		MPSCROLLPANE.putAll(MPSWINGCONSTANTSSCROLLPANE);
		MPSCROLLPANE.put("none", -1);

	}
	
	public static int getIntValue(Attributes attributes, String sAttributeName, int iDefault) {
		final String sValue = attributes.getValue(sAttributeName);
		try {
			return (sValue == null) ? iDefault : Integer.parseInt(sValue);
		}
		catch (NumberFormatException ex) {
			LOG.warn(ex.getMessage(), ex);
			return iDefault;
		}
	}
	
}
