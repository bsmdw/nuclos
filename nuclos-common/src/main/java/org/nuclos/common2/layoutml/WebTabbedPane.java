package org.nuclos.common2.layoutml;

import org.xml.sax.Attributes;

public class WebTabbedPane extends AbstractWebContainer {
	
	public WebTabbedPane(Attributes attributes) {
		super(attributes);
	}
	
	@Override
	public String toString() {
		return "Name:" + getName() + " Comps:" + componentsString();
	}
	
	@Override
	public String getTitle() {
		String name = getName();
		if (name != null && !name.isEmpty()) {
			return name;
		}
		IWebContainer parent = getContainer();
		return parent != null ? parent.getTitle() : "";
	}

	@Override
	public String getType() {
		return "TABBEDPANE";
	}

	@Override
	protected void setWebTableLayout(WebTableLayout layout) {
		
	}

}
