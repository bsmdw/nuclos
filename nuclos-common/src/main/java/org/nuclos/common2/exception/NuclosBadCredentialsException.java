package org.nuclos.common2.exception;

import org.springframework.security.authentication.BadCredentialsException;

/**
 * Created by Oliver Brausch on 02.05.19.
 */
public class NuclosBadCredentialsException extends BadCredentialsException {
	public NuclosBadCredentialsException(String msg) {
		super(msg);
		NuclosExceptions.shortenStackTrace(this, 1);
	}
}
