package org.nuclos.common2.resource;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.resource.ejb3.CommonResourceFacade;
import org.nuclos.server.resource.valueobject.ResourceVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourcePropertySource;

public class ResourceResolver {
	
	private static final Logger LOG = Logger.getLogger(ResourceResolver.class);
	
	private static final String NUCLOS_RESOURCE_PATH = "org/nuclos/common2/resource/nuclos-resources.properties";
	
	public static final String DEFAULT_ICON = "org.nuclos.client.resource.icon.main.nuclet.png";
	
	//
	
	private final ResourcePropertySource nuclosResource;
	
	private final Map<UID,Resource> dbResourceUid2Resource = new ConcurrentHashMap<UID, Resource>();
	
	// Spring injection
	
	@Autowired
	private CommonResourceFacade resourceFacade;

	// end of Spring injection
	
	public ResourceResolver() throws IOException {
		nuclosResource = new ResourcePropertySource(new ClassPathResource(NUCLOS_RESOURCE_PATH));
	}
	
	public Resource resolveEntityIcon(String name) {
		final String realResource = (String) nuclosResource.getProperty("ENTITY_ICON;" + name);
		if (realResource == null) return null;
		return new ClassPathResource(realResource);
	}

	public Resource resolveHiddenEntityIcon(String name) {
		final String realResource = (String) nuclosResource.getProperty("ENTITY_ICON_HIDDEN;" + name);
		if (realResource == null) return null;
		return new ClassPathResource(realResource);
	}

	public Resource resolveSearchFilterIcon(String name) {
		final String realResource = (String) nuclosResource.getProperty("SEARCH_FILTER_ICON;" + name);
		if (realResource == null) return null;
		return new ClassPathResource(realResource);
	}
	
	public Resource resolveDbResourceIcon(UID resource) throws IOException {
		Resource result = dbResourceUid2Resource.get(resource);
		if (result == null) {
			result = createDbResourceIcon(resource);
			if (result != null) {
				dbResourceUid2Resource.put(resource, result);
			}
		}
		return result;
	}

	private Resource createDbResourceIcon(UID resourceUID) throws IOException {
		final Pair<ResourceVO, byte[]> rp;
		try {
			rp = resourceFacade.getResourceBytes(resourceUID);
		}
		catch (CommonFatalException e) {
			LOG.warn("No icon (and resource) for resource " + resourceUID);
			return null;
		}
		
		final ResourceVO resource = rp.getX();
		if (resource == null) {
			LOG.warn("No resource at all (=null) found for resourceUID '" + resourceUID);
			return null;
		}
		
		final byte[] content = rp.getY();
		if (content != null && content.length > 0) {
			LOG.info("Id resource found for " + resourceUID + " " + resource);
			final String base = FilenameUtils.getBaseName(resource.getFileName());
			final String ext = FilenameUtils.getExtension(resource.getFileName());
			final File file = File.createTempFile(base, "." + ext);
			file.deleteOnExit();
			IOUtils.copy(new ByteArrayInputStream(content), new FileOutputStream(file));
			return new FileSystemResource(file);
		}
		else {
			LOG.warn("No icon for resourceUID '" + resourceUID + "' (name:" + resource.getName() + ", file:" + resource.getFileName() + ")");
			return null;
		}
	}
	
	public String getEntityIconBase64(EntityMeta<?> meta) throws IOException {
		String nuclosRes = meta.getNuclosResource();
		return getEntityIconBase64(nuclosRes);
	}	
	
	public String getEntityIconBase64(String nuclosRes) throws IOException {
		if (nuclosRes == null) nuclosRes = DEFAULT_ICON;
		if (nuclosRes != null) {
			ResourceResolver rr = new ResourceResolver();
			Resource r  = rr.resolveEntityIcon(nuclosRes);
			if (r == null) r = rr.resolveHiddenEntityIcon(nuclosRes);
			byte[] imageBytes = IOUtils.toByteArray(r.getInputStream());
			if (imageBytes != null) return new String(Base64.encodeBase64(imageBytes));
		}
		return null;
	}	
}
