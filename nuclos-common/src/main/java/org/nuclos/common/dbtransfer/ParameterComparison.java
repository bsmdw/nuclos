//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.dbtransfer;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;

public class ParameterComparison extends EntityObjectVO<UID> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9128938582403336860L;
	private final boolean isNew;
	private final String currentValue;
	private final String nucletName;
	
	public ParameterComparison(EntityObjectVO<UID> eovo, boolean isNew, String currentValue, String nucletName) {
		super(E.NUCLETPARAMETER);
		setPrimaryKey(eovo.getPrimaryKey());
		mapField.putAll(eovo.getFieldValues());
		mapFieldUid.putAll(eovo.getFieldUids());
		
		this.isNew = isNew;
		this.currentValue = currentValue;
		this.nucletName = nucletName;
	}
	
	public boolean isNew() {
		return isNew;
	}
	
	public String getCurrentValue() {
		return currentValue;
	}
	
	public String getTitle() {
		return nucletName + "." + getFieldValue(E.NUCLETPARAMETER.name);
	}
	
	public boolean isValueChanged() {
		return isNew() || (super.getFieldValue(E.NUCLETPARAMETER.value) != null && !super.getFieldValue(E.NUCLETPARAMETER.value).equals(getCurrentValue())) 
		|| (super.getFieldValue(E.NUCLETPARAMETER.value) == null && getCurrentValue() != null);
	}
}
