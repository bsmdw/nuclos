//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.dbtransfer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.report.ejb3.IJobKey;

public class Transfer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3630583590469853083L;
	private final boolean nuclon;
	private byte[] transferFile;
	private String transferFileName;
	private Collection<EntityObjectVO<UID>> parameter;
	private Map<TransferOption, Serializable> transferOptions;
	private List<PreviewPart> previewParts;
	private TransferTreeNode nucletTree;
	
	private NucletContentMap importContentMap;
	private Set<UID> existingNucletUIDs;
	private Set<IJobKey> scheduledJobs;
	private boolean reScheduleJobs = true;

	private Map<UID, Object> defaultMandatoryIds;

	public Result result = new Result();

	public Transfer(
		boolean nuclon,
		byte[] transferFile,
		String transferFileName,
		Collection<EntityObjectVO<UID>> parameter,
		Map<TransferOption, Serializable> transferOptions,
		List<PreviewPart> previewParts)	{
		this.nuclon = nuclon;
		this.transferFile = transferFile;
		this.transferFileName = transferFileName;
		this.parameter = parameter;
		this.transferOptions = transferOptions;
		this.previewParts = previewParts;
		this.defaultMandatoryIds = new HashMap<>();
	}

	public Map<UID, Object> getDefaultMandatoryIds() {
		return defaultMandatoryIds;
	}

	public void setDefaultMandatoryIds(final Map<UID, Object> defaultMandatoryIds) {
		if (defaultMandatoryIds == null) {
			this.defaultMandatoryIds = new HashMap<>();
		} else {
			this.defaultMandatoryIds = defaultMandatoryIds;
		}
	}

	public Transfer(Transfer transfer) {
		this(transfer.nuclon, transfer.transferFile,transfer.transferFileName,transfer.parameter,transfer.transferOptions,transfer.previewParts);
		this.setImportContentMap(transfer.getImportContentMap());
		this.setExistingNucletUIDs(transfer.getExistingNucletUIDs());
		this.setScheduledJobs(transfer.getScheduledJobs());
		this.setNucletTree(transfer.getNucletTree());
		this.setDefaultMandatoryIds(transfer.getDefaultMandatoryIds());
		this.result.sbNonRepeatedValidationWarning.append(transfer.result.sbNonRepeatedValidationWarning);
	}

	public void appendInfo(String info) {
		result.sbInfo.append(info);
		result.sbInfo.append("<br />");
	}

	public byte[] getTransferFile() {
		return transferFile;
	}

	public String getTransferFileName() {
		return transferFileName;
	}

	public Collection<EntityObjectVO<UID>> getParameter() {
		return parameter;
	}

	public Map<TransferOption, Serializable> getTransferOptions() {
		return transferOptions;
	}

	public void setParameter(Collection<EntityObjectVO<UID>> parameter) {
		this.parameter = parameter;
	}

	public List<PreviewPart> getPreviewParts() {
    	return previewParts;
    }

	public NucletContentMap getImportContentMap() {
		return importContentMap;
	}

	public void setImportContentMap(NucletContentMap importContentMap) {
		this.importContentMap = importContentMap;
	}

	public Set<UID> getExistingNucletUIDs() {
		return existingNucletUIDs;
	}

	public void setExistingNucletUIDs(Set<UID> existingNucletUIDs) {
		this.existingNucletUIDs = existingNucletUIDs;
	}

	public Set<IJobKey> getScheduledJobs() {
		return scheduledJobs;
	}

	public void setScheduledJobs(Set<IJobKey> scheduledJobs) {
		this.scheduledJobs = scheduledJobs;
	}

	public boolean isReScheduleJobs() {
		return reScheduleJobs;
	}

	public void setReScheduleJobs(boolean reScheduleJobs) {
		this.reScheduleJobs = reScheduleJobs;
	}

	public void setNucletTree(final TransferTreeNode nucletTree) {
		this.nucletTree = nucletTree;
	}

	public TransferTreeNode getNucletTree() {
		return nucletTree;
	}

	public void addDefaultMandatoryId(final UID field, final Object id) {
		if (id != null) {
			this.defaultMandatoryIds.put(field, id);
		}
	}

	public static class Result implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -8536767995598684304L;
		public final StringBuffer sbCritical = new StringBuffer();
		public final StringBuffer sbWarning = new StringBuffer();
		public final StringBuffer sbInfo = new StringBuffer();
		public final List<String> script = new ArrayList<String>();
		public final Set<Pair<UID, String>> foundReferences = new HashSet<Pair<UID,String>>();

		public final StringBuffer sbNonRepeatedValidationWarning = new StringBuffer();

		public String getCriticals() {
			return sbCritical.toString();
		}

		public String getWarnings() {
			return sbWarning.toString();
		}

		public String getInfos() {
			return sbInfo.toString();
		}

		public void addWarning(StringBuffer s) {
			if (sbWarning.length() > 0) sbWarning.append("<br />");
			sbWarning.append(s);
		}

		public void addCritical(StringBuffer s) {
			if (sbCritical.length() > 0) sbCritical.append("<br />");
			sbCritical.append(s);
		}

		public boolean hasCriticals() {
			return sbCritical.length() > 0;
		}

		public boolean hasWarnings() {
			return sbWarning.length() > 0;
		}

		public boolean hasInfos() {
			return sbInfo.length() > 0;
		}

		public void newWarningLine(String warning) {
			if (warning != null) {
				if (sbWarning.length() > 0) sbWarning.append("<br />");
				sbWarning.append(warning);
			}
		}

		public void newCriticalLine(String critical) {
			if (critical != null) {
				if (sbCritical.length() > 0) sbCritical.append("<br />");
				sbCritical.append(critical);
			}
		}

	}

	public boolean isNuclon() {
		return nuclon;
	}

}
