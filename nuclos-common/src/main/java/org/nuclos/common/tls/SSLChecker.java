package org.nuclos.common.tls;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.apache.log4j.Logger;

/**
 * Tests a SSL connection to the given server URL.
 * Several template methods are used for callback purposes.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public abstract class SSLChecker implements Runnable {
	private static final Logger log = Logger.getLogger(SSLChecker.class);

	private final URL serverUrl;
	private X509TrustManager defaultTrustManager;

	protected abstract void notifyInProgress();

	protected abstract void notifyTrusted();

	protected abstract void notifyUntrusted();

	protected abstract void notifyInvalid();

	public SSLChecker(URL serverUrl) {
		this.serverUrl = serverUrl;
	}

	@Override
	public void run() {
		notifyInProgress();

		try {
			final int port = serverUrl.getPort() > 0 ? serverUrl.getPort() : 443;

			SSLSocketFactory sslsocketfactory = createCustomSSLSocketFactory();
			SSLSocket sslsocket = (SSLSocket) sslsocketfactory.createSocket(serverUrl.getHost(), port);

			InputStream in = sslsocket.getInputStream();
			OutputStream out = sslsocket.getOutputStream();

			// Write a test byte to get a reaction :)
			out.write(1);

			while (in.available() > 0) {
				System.out.print(in.read());
			}
			log.info("Successfully connected");

		} catch (Exception e) {
			log.error("SSL-Check failed", e);
		}
	}

	private SSLSocketFactory createCustomSSLSocketFactory() throws Exception {
		TrustManager[] byPassTrustManagers = new TrustManager[]{new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return new X509Certificate[0];
			}

			public void checkClientTrusted(X509Certificate[] chain, String authType) {
			}

			public void checkServerTrusted(X509Certificate[] chain, String authType) {
				try {
					for (X509Certificate certificate : chain) {
						certificate.checkValidity();
					}

					getDefaultTrustManager().checkServerTrusted(chain, authType);
				} catch (CertificateExpiredException | CertificateNotYetValidException e) {
					notifyInvalid();
					log.error("SSL certificate is invalid", e);
					return;
				} catch (CertificateException e) {
					notifyUntrusted();
					log.error("SSL certificate is untrusted", e);
					return;
				} catch (NoSuchAlgorithmException | KeyStoreException e) {
					log.error("SSL-Check failed", e);
					return;
				}

				notifyTrusted();
			}
		}};
		SSLContext sslContext = SSLContext.getInstance("SSL");
		sslContext.init(null, byPassTrustManagers, new SecureRandom());
		return sslContext.getSocketFactory();
	}

	private static X509TrustManager getDefaultTrustManager() throws NoSuchAlgorithmException, KeyStoreException {
		TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());

		trustManagerFactory.init((KeyStore) null);

		log.info("JVM Default Trust Managers:");
		for (TrustManager trustManager : trustManagerFactory.getTrustManagers()) {
			System.out.println(trustManager);

			if (trustManager instanceof X509TrustManager) {
				X509TrustManager x509TrustManager = (X509TrustManager) trustManager;
				log.info("\tAccepted issuers count : " + x509TrustManager.getAcceptedIssuers().length);
				return x509TrustManager;
			}
		}

		return null;
	}
}
