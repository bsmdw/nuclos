package org.nuclos.common.lucene;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;

public class LuceneSearchResult implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7151480251802706660L;
	private final UID entity;
	private final Object pk;
	private final FieldMeta<?> fm;
	private final String search;
	private EntityObjectVO<?> eo;

	public LuceneSearchResult(UID entity, Object pk, FieldMeta<?> fm, String search) {
		this.entity = entity;
		this.pk = pk;
		this.fm = fm;
		this.search = search;
	}
	
	public UID getEntity() {
		return entity;
	}
	
	public Object getPk() {
		return pk;
	}
	
	public FieldMeta<?> getField() {
		return fm;
	}
	
	public String getSearch() {
		return search;
	}
	
	public EntityObjectVO<?> getEo() {
		return eo;
	}
	
	public void setEoIfNotSet(EntityObjectVO<?> eo) {
		if (this.eo == null) {
			this.eo = eo;			
		}
	}
	
	private Object getPkOfEo() {
		return eo != null ? eo.getPrimaryKey() : pk;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof LuceneSearchResult) {
			LuceneSearchResult that = (LuceneSearchResult) obj;
			
			return LangUtils.equal(entity, that.entity) && LangUtils.equal(pk, that.pk) 
					&& LangUtils.equal(search, that.search) && LangUtils.equal(getPkOfEo(), that.getPkOfEo());
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		return LangUtils.hash(entity, pk, search, getPkOfEo());
	}

	//TODO: This code is based on the old code and has to be re-factored.
	public String getFormattedResultText(IMetaProvider metaProvider, SpringLocaleDelegate localeDelegate) {
		
		EntityMeta<?> em = metaProvider.getEntity(entity);
        String entityLabel = localeDelegate.getLabelFromMetaDataVO(em);
        
		String ss = getSearch().toLowerCase().trim();
        Map<String, String> matchMap = new HashMap<String, String>();
        Map<String, String> docuMap = new HashMap<String, String>();
        
		Collection<FieldMeta<?>> fms = em.getFields();
		for (FieldMeta<?> fm : fms) {
        	boolean isDocument = fm.getDataType().equals("org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile");

            String fieldValue;
            Object fld = getEo().getFieldValue(fm.getUID());
			if (fld instanceof Date) {
				fieldValue = localeDelegate.getDateFormat().format((Date)fld);
				
			} else if (fld instanceof String) {
				fieldValue = (String)fld;
				
			} else {
				fieldValue = fld == null ? "" : fld.toString();
				
			}
			String lcFieldValue = fieldValue.toLowerCase();
			
            String fieldLabel = localeDelegate.getLabelFromMetaFieldDataVO(fm);
            boolean hit = false;
            
            int matchIndex = lcFieldValue.indexOf(ss);
            if (matchIndex == 0) {
            	hit = true;
            } else while (matchIndex > 0) {
            	char c = lcFieldValue.charAt(matchIndex - 1);
            	if (!Character.isLetterOrDigit(c)) {
            		hit = true;
            		break;
            	}
            	matchIndex = lcFieldValue.indexOf(ss, matchIndex + 1);
            }
            
            if (hit) {
                int startIndex = matchIndex - 20;
                if (startIndex < 0) startIndex = 0;
                int endIndex = matchIndex + ss.length();
                int finalIndex = endIndex + 20;
                if (finalIndex > lcFieldValue.length()) finalIndex = lcFieldValue.length();
                String hilightedValue
                    = fieldValue.substring(startIndex, matchIndex)
                    + "<b>"
                    + fieldValue.substring(matchIndex, endIndex)
                    + "</b>"
                    + fieldValue.substring(endIndex, finalIndex);

                matchMap.put(fieldLabel, hilightedValue);
                if (matchMap.size() >= 2) {
                    break;                	
                }
            } else if (isDocument && fld != null) {
                docuMap.put(fieldLabel, fld.toString());
            }
		}
		
        if (matchMap.isEmpty()) {
        	matchMap = new HashMap<String, String>(docuMap);
        }
        
		// TODO DATALANGUAGE
		UID datalanguage = null;

	    String title = localeDelegate.getTreeViewLabel(getEo().getFieldValues(), getEo().getDalEntity(), metaProvider, datalanguage);
	    StringBuilder sb = new StringBuilder();
	    sb.append("<b>")
	    	.append(entityLabel)
	    	.append("</b>&nbsp;");
	    sb.append(title);
	    sb.append("<small>");
	    
	    String sep = ":<br><span class=\"result\">";

	    if (matchMap != null) {
		    for (String key : CollectionUtils.sorted(matchMap.keySet())) {
		    	sb.append(sep)
		    		.append(key)
		    		.append("=")
		    		.append(matchMap.get(key))
		    		.append(" ");
		    	sep = ", ";
		    }
			sb.append("</span>");
	    }
	    
	    return sb.toString();
	}
}
