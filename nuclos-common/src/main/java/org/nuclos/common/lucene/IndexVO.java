package org.nuclos.common.lucene;

import java.lang.ref.SoftReference;
import java.util.Map;

import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.IDalReadVO;
import org.nuclos.common.dal.vo.IDalVO;

public class IndexVO<PK> extends IndexStep {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4447532851365549252L;
	private final UID entity;
	private final PK pk;
	private final boolean flagNew;
	private final boolean flagRemoved;
	
	private final SoftReference<Map<UID, Object>> indexFields;
	
	public IndexVO(IDalReadVO<PK> idrVO, String transactId, final Map<UID, Object> mpValues) {
		super(transactId);
		
		this.entity = idrVO.getDalEntity();
		this.pk = idrVO.getPrimaryKey();
		
		this.flagRemoved = idrVO.getFlag() == IDalVO.STATE_REMOVED;
		this.flagNew = idrVO.getFlag() == IDalVO.STATE_NEW;
		
		if (idrVO.getFieldIds() != null) {
			for (UID field : idrVO.getFieldIds().keySet()) {
				mpValues.put(field, idrVO.getFieldIds().get(field));
			}
			
			for (UID field : idrVO.getFieldUids().keySet()) {
				UID uid = idrVO.getFieldUids().get(field);
				mpValues.put(field, uid);
			}
			
			for (UID field : idrVO.getFieldValues().keySet()) {
				if (!mpValues.containsKey(field) && !SF.isEOField(entity, field)) {
					Object val = idrVO.getFieldValues().get(field);
					if (val != null) {
						mpValues.put(field, val);						
					}
				}
			}			
		}
		
		//TODO: If the soft references are lost, reload them
		this.indexFields = new SoftReference<Map<UID, Object>>(mpValues);
	}
	
	public UID getEntity() {
		return entity;
	}
	
	public PK getPrimaryKey() {
		return pk;
	}
	
	public boolean isFlagRemoved() {
		return flagRemoved;
	}
	
	public boolean isFlagNew() {
		return flagNew;
	}
	
	public Map<UID, Object> indexFields() {
		return indexFields.get();
	}
	
	@Override
	public String toString() {
		return "entity=" + entity + ", pk=" + pk + " , transactId=" + getTransactId()
				+ "\nindexFields=" + indexFields();
	}
}
