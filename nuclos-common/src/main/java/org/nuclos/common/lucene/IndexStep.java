package org.nuclos.common.lucene;

import java.io.Serializable;

public abstract class IndexStep implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3962200160250351552L;
	private final String transactId;

	public IndexStep(String transactId) {
		this.transactId = transactId;
	}

	public String getTransactId() {
		return transactId;
	}
	
}
