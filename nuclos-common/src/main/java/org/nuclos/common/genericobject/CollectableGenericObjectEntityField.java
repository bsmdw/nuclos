//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.genericobject;

import java.util.Date;
import java.util.Map;
import java.util.prefs.Preferences;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.attribute.ComponentType;
import org.nuclos.common.attribute.DynamicAttributeVO;
import org.nuclos.common.collect.collectable.AbstractCollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableComponentTypes;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldFormat;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.security.Permission;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.RelativeDate;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.genericobject.valueobject.CanonicalAttributeFormat;

/**
 * Provides structural (meta) information about a field of a leased object.
 * Formerly known as <code>CollectableAttributeEntityField</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * <p>
 * TODO: Consider org.nuclos.client.common.CollectableEntityFieldPreferencesUtil
 * to write to {@link Preferences}.
 * 
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class CollectableGenericObjectEntityField extends AbstractCollectableEntityField {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9077682256233129157L;

	private final UID entity;

	protected Map<UID, Permission> mpPermissions = CollectionUtils.newHashMap();

	private final FieldMeta  entityFieldMeta;

	public CollectableGenericObjectEntityField(Map<UID, Permission> mpPermissions, FieldMeta entityFieldMeta, UID entity) {
		this.mpPermissions = mpPermissions;
		this.entityFieldMeta = entityFieldMeta;
		this.entity = entity;
	}

	public Map<UID, Permission> getPermissions() {
		return this.mpPermissions;
	}

	public FieldMeta getFieldMeta() {
		return entityFieldMeta;
	}

	@Override
	public String getDescription() {
		return SpringLocaleDelegate.getInstance().getText(entityFieldMeta.getLocaleResourceIdForDescription());
	}

	@Override
	public boolean isReferencing() {
		return (entityFieldMeta.getForeignEntity() != null || entityFieldMeta.getLookupEntity() != null);
	}

	@Override
	public UID getReferencedEntityUID() {
		return entityFieldMeta.getForeignEntity() != null ? entityFieldMeta.getForeignEntity() : entityFieldMeta.getLookupEntity();
	}

	@Override
	public String getReferencedEntityFieldName() {
		if (entityFieldMeta.getForeignEntity() != null)
			return entityFieldMeta.getForeignEntityField();
		return entityFieldMeta.getLookupEntityField();
	}

	@Override
	public Class<?> getJavaClass() {
		return entityFieldMeta.getJavaClass();
	}

	@Override
	public String getLabel() {
		return SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(entityFieldMeta);
	}

	@Override
	public Integer getMaxLength() {
		// for dates, the maximum length is always 10:
		/** @todo maybe this should be written to the database when the attribute is stored. */
		if (entityFieldMeta.getJavaClass().equals(java.util.Date.class)) {
			return 10;
		}
		return entityFieldMeta.getScale();
	}

	@Override
	public Integer getPrecision() {
		return entityFieldMeta.getPrecision();
	}

	@Override
	public UID getUID() {
		return entityFieldMeta.getUID();
	}

	@Override
	public String getFormatInput() {
		return entityFieldMeta.getFormatInput();
	}

	@Override
	public String getFormatOutput() {
		return entityFieldMeta.getFormatOutput();
	}

	@Override
	public int getFieldType() {
		return isReferencing() ? CollectableEntityField.TYPE_VALUEIDFIELD: CollectableEntityField.TYPE_VALUEFIELD;
	}

	@Override
	public boolean isRestrictedToValueList() {
		// NUCLOSINT-442 deactivated! We need a new concept for insertable...
//		return !this.attrcvo.isInsertable();
		if (this.isIdField())
			return true;
		else
			return !this.entityFieldMeta.isInsertable();
	}

	@Override
	public boolean isNullable() {
		return entityFieldMeta.isNullable();
	}

	public boolean isShowMnemonic() {
		return entityFieldMeta.isShowMnemonic();
	}

	@Override
	public CollectableField getDefault() {
		Object defaultValue = entityFieldMeta.getDefaultValue();
		try {
			final ClassLoader cl = LangUtils.getClassLoaderThatWorksForWebStart();
			final Class<?> clazz = cl.loadClass(entityFieldMeta.getDataType());
			final String dVal = entityFieldMeta.getDefaultValue();
			final CanonicalAttributeFormat caf = DynamicAttributeVO.getCanonicalFormat(clazz);
			if (StringUtils.isNullOrEmpty(dVal) || clazz.equals(Date.class)) {
				defaultValue = caf.parse(dVal);
			} else {
				final CollectableFieldFormat cff = CollectableFieldFormat.getInstance(clazz);
				final String fInput = entityFieldMeta.getFormatInput();
				defaultValue = caf.parse(cff.parse(fInput, dVal).toString());
			}
		}
		catch (ClassNotFoundException ex) {
			throw new NuclosFatalException(StringUtils.getParameterizedExceptionMessage("vofactory.exception.1", entityFieldMeta.getDataType()), ex);
		}
		catch (CommonValidationException ex) {
			throw new NuclosFatalException(StringUtils.getParameterizedExceptionMessage("vofactory.exception.2", entityFieldMeta.getFieldName(), entityFieldMeta.getDefaultValue()), ex);
		}
		final CollectableField result;

		if (this.isIdField()) {
			result = new CollectableValueIdField(entityFieldMeta.getDefaultForeignId(), defaultValue);
		}
		else {
			result = new CollectableValueField(defaultValue);
		}

		assert result != null;
		assert result.getFieldType() == this.getFieldType();
		if (!(this.getJavaClass()== Date.class && result.getValue() != null && result.getValue().toString().equalsIgnoreCase(RelativeDate.today().toString())))
			assert LangUtils.isInstanceOf(result.getValue(), this.getJavaClass());
		return result;
	}

	/**
	 * @return the default collectable component type for the three flags (searchable, modifiable, insertable).
	 */
	@Override
	public int getDefaultCollectableComponentType() {
		if (getDefaultComponentType() != null) {
			return super.getDefaultCollectableComponentType();
		}
		
		final int result;
		switch (ComponentType.findByFlags(entityFieldMeta.isSearchable(), entityFieldMeta.isModifiable(), entityFieldMeta.isInsertable())) {
			case TEXTFIELD:
				// fall back to the super implementation, as it is more precise here:
				result = super.getDefaultCollectableComponentType();
				break;
			case COMBOBOX:
			case DROPDOWN:
			case LOOKUP:
				result = CollectableComponentTypes.TYPE_COMBOBOX;
				break;
			case LISTOFVALUES:
				result = CollectableComponentTypes.TYPE_LISTOFVALUES;
				break;
			default:
				throw new NuclosFatalException("clctgoef.missing.componenttype.error");
					//"Es gibt keinen passenden Komponenten-Typ f\u00fcr diese Kombination.");
		}
		return result;
	}

	@Override
	public UID getEntityUID() {
		return entity;
	}

	@Override
	public String getDefaultComponentType() {
		if (entityFieldMeta == null) {
			return null;
		} else {
			return entityFieldMeta.getDefaultComponentType();
		}
	}

	@Override
	public boolean isCalculated() {
		return entityFieldMeta.isCalculated();
	}

	@Override
	public boolean isCalcOndemand() {
		return entityFieldMeta.isCalcOndemand();
	}

	@Override
	public boolean isCalcAttributeAllowCustomization() {
		return entityFieldMeta.isCalcAttributeAllowCustomization();
	}

	@Override
	public String getName() {
		return entityFieldMeta.getFieldName();
	}
	
	public boolean isLocalized() {
		return entityFieldMeta.isLocalized();
	}

	@Override
	public boolean isHidden() {
		return entityFieldMeta.isHidden();
	}

	@Override
	public boolean isModifiable() {
		return entityFieldMeta.isModifiable();
	}
}	// class CollectableGenericObjectEntityField
