package org.nuclos.common.api;

import java.io.Serializable;

/**
 * Implementation of an API command.
 *
 * TODO: Should be ApiCommandImpl<T extends Command>, API has to  be refactored first. See NUCLOS-5221
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class ApiCommandImpl<T> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2570990279337323566L;
	private final Integer receiverId;
	private final T command;

	public ApiCommandImpl(final Integer receiverId, final T command) {
		this.receiverId = receiverId;
		this.command = command;
	}

	public Integer getReceiverId() {
		return receiverId;
	}

	public T getCommand() {
		return command;
	}
}
