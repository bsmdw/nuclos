//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.common;

import org.nuclos.api.PreferencesImpl;
import org.nuclos.common.dal.vo.AbstractDalVOWithVersion;
import org.nuclos.common2.CloseableXStream;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.XStreamSupport;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.ConversionException;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class WorkspaceVO extends AbstractDalVOWithVersion<UID> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8609574098062721807L;
	
	private UID user;
	private String name;
	private UID assignedWorkspace;
	private UID nuclet;
	private Boolean isMaintenance = false;
	
	private WorkspaceDescription2 workspaceDescription;
	
	public WorkspaceVO(UID dalEntity) {
		this();
	}
	
	public WorkspaceVO() {
		super(E.WORKSPACE.getUID());
		this.workspaceDescription = new WorkspaceDescription2();
	}
	
	public WorkspaceVO(WorkspaceDescription2 wd) {
		super(E.WORKSPACE.getUID());
		this.workspaceDescription = wd;
	}

	public UID getUser() {
		return user;
	}

	public void setUser(UID user) {
		this.user = user;
	}

	
	public UID getNuclet() {
		return nuclet;
	}

	public void setNuclet(UID nuclet) {
		this.nuclet = nuclet;
	}

	public String getClbworkspace() {
		final XStreamSupport xs = XStreamSupport.getInstance();
		try (CloseableXStream closeable = xs.getCloseableStream()) {
			final XStream xstream = closeable.getStream();
			return xstream.toXML(workspaceDescription);
		}
	}

	public void setClbworkspace(String clbworkspace) {
		final XStreamSupport xs = XStreamSupport.getInstance();
		try (CloseableXStream closeable = xs.getCloseableStream()) {
			final XStream xstream = closeable.getStream();
			try {
				this.workspaceDescription = (WorkspaceDescription2) xstream.fromXML(clbworkspace);
			} catch (ConversionException cex) {
				if (cex.getMessage().contains(PreferencesImpl.class.getCanonicalName())) {
					// ConcurrentHashMap changed from Java 7 to Java 8
					xstream.registerConverter(new Converter() {
						@Override
						public boolean canConvert(Class type) {
							if (PreferencesImpl.class.equals(type)) {
								return true;
							}
							return false;
						}
						@Override
						public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
							// not in use
						}
						@Override
						public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
							return new PreferencesImpl();
						}
					});
				}
				// handling No such field org.nuclos.common.WorkspaceDescription2.layoutPreferences
				// see http://stackoverflow.com/questions/4409717/java-xstream-ignore-tag-that-doesnt-exist-in-xml
				xstream.ignoreUnknownElements("layoutPreferences");
				this.workspaceDescription = (WorkspaceDescription2) xstream.fromXML(clbworkspace);
			}
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		this.workspaceDescription.setName(name);
	}

	public UID getAssignedWorkspace() {
		return assignedWorkspace;
	}

	public void setAssignedWorkspace(UID assignedWorkspace) {
		this.assignedWorkspace = assignedWorkspace;
	}
	
	public boolean isAssigned() {
		return this.assignedWorkspace != null;
	}

	public WorkspaceDescription2 getWoDesc() {
		return workspaceDescription;
	}

	public void setWoDesc(WorkspaceDescription2 wd) {
		this.workspaceDescription = wd;
	}
	
	public void importHeader(WorkspaceDescription2 importWD) {
		setName(importWD.getName());
		this.workspaceDescription.importHeader(importWD);
	}

	public Boolean isMaintenance() {
		return isMaintenance;
	}

	public void setMaintenance(Boolean isMaintenance) {
		this.isMaintenance = isMaintenance;
	}

	@Override
	public int hashCode() {
		if (getPrimaryKey() == null)
			return 0;
		return getPrimaryKey().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof WorkspaceVO) {
			WorkspaceVO other = (WorkspaceVO) obj;
			return LangUtils.equal(getPrimaryKey(), other.getPrimaryKey());
		}
		return super.equals(obj);
	}
	
	public WorkspaceVO clone() {
		WorkspaceVO result = new WorkspaceVO();
		
		result.setPrimaryKey(getPrimaryKey());
		result.setVersion(getVersion());
		result.setCreatedAt(getCreatedAt());
		result.setCreatedBy(getCreatedBy());
		result.setChangedAt(getChangedAt());
		result.setChangedBy(getChangedBy());
		
		// clone the WorkspaceDescription object via XML serialization
		result.setClbworkspace(getClbworkspace());
		
		result.setName(getName());
		result.setAssignedWorkspace(getAssignedWorkspace());
		result.setUser(getUser());
		result.setNuclet(getNuclet());
		result.setMaintenance(isMaintenance());
		return result;
	}
	
}
