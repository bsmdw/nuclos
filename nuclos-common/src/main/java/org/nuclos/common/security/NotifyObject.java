//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.security;

import java.io.Serializable;

import org.nuclos.common2.LangUtils;

public class NotifyObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 633085422302170757L;

	/**
	 * Maybe null.
	 */
	private final String username;
	
	private final boolean refreshMenus;
	
	public NotifyObject(String username, boolean refreshMenus) {
		this.username = username;
		this.refreshMenus = refreshMenus;
	}
	
	public String getUsername() {
		return username;
	}
	
	public boolean getRefreshMenus() {
		return refreshMenus;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof NotifyObject)) return false;
		final NotifyObject other = (NotifyObject) o;
		return LangUtils.equal(username, other.username) && (refreshMenus == other.refreshMenus);
	}
	
	@Override
	public int hashCode() {
		int result = 3971;
		if (username != null) {
			result += 7 * username.hashCode();
		}
		if (refreshMenus) {
			result += 11;
		}
		return result;
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append(username).append(",");
		result.append(refreshMenus).append("]");
		return result.toString();
	}
	
}
