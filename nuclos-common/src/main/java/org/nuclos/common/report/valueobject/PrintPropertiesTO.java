//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.valueobject;

import java.io.Serializable;

import javax.print.PrintService;

import org.nuclos.api.print.PrintProperties;

/**
 * {@link PrintPropertiesTO} transport object
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public class PrintPropertiesTO extends PrintProperties implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6461297232013159100L;

	// print service is set from file service provider directly
	private transient PrintService printService;
	
	// for client side printing only!
	private String printerNameOnClient;
	private Integer trayNumberOnClient;
	
	public PrintPropertiesTO clone() {
		PrintPropertiesTO clone = new PrintPropertiesTO();
		
		// TODO implement clone in super class
		clone.setCopies(getCopies());
		clone.setDuplex(isDuplex());
		clone.setTrayId(getTrayId());
		clone.setPrintServiceId(getPrintServiceId());
				
		clone.printService = printService;
		clone.printerNameOnClient = printerNameOnClient;
		clone.trayNumberOnClient = trayNumberOnClient;
		return clone;
	}

	public PrintService getPrintService() {
		return printService;
	}

	public void setPrintService(PrintService printService) {
		this.printService = printService;
	}

	public String getPrinterNameOnClient() {
		return printerNameOnClient;
	}

	public void setPrinterNameOnClient(String printerNameOnClient) {
		this.printerNameOnClient = printerNameOnClient;
	}

	public Integer getTrayNumberOnClient() {
		return trayNumberOnClient;
	}

	public void setTrayNumberOnClient(Integer trayNumberOnClient) {
		this.trayNumberOnClient = trayNumberOnClient;
	}

}
