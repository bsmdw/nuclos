//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.valueobject;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.security.Permission;
import org.nuclos.common2.KeyEnum;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.valueobject.NuclosValueObject;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Value object representing a report definition.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:ramin.goettlich@novabit.de">ramin.goettlich</a>
 * @version 00.01.000
 */
public class DefaultReportVO extends NuclosValueObject<UID> implements ReportVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7389696032175547906L;
	private ReportType type;
	private UID dataSourceUID;
	private String sName;
	private String sDescription;
	private OutputType outputType;
	private String sParameter;
	private String sFilename;;
	private boolean bAttach;

	/**
	 * Convenience constructor for output of search result lists; used by client only.
	 * @param sName
	 */
	public DefaultReportVO(String sName) {
		this(null, sName, null, null, null, null, OutputType.SINGLE, true);
	}

	/**
	 * constructor used by client only
	 */
	public DefaultReportVO(ReportType type, String sName, String sDescription, UID dataSourceUID, 
			String sParameter, String sFilename, OutputType outputType, Boolean bAttach) {
		
		super();

		this.type = type;
		this.sName = sName;
		this.sDescription = sDescription;
		this.dataSourceUID = dataSourceUID;
		this.outputType = outputType;
		this.sParameter = sParameter;
		this.sFilename = sFilename;
		this.bAttach = bAttach == null ? true : bAttach.booleanValue();
	}

	/**
	 * constructor used by server only
	 * 
	 * §precondition permission != null
	 * 
	 * @param nvo contains the common fields.
	 */
	public DefaultReportVO(NuclosValueObject<UID> nvo, ReportType type, String sName, String sDescription, UID dataSourceUID,
			String sParameter, String sFilename, String sOutputtype, Boolean bAttach, Permission permission) {
		super(nvo);

		this.type = type;
		this.sName = sName;
		this.sDescription = sDescription;
		this.dataSourceUID = dataSourceUID;
		this.outputType = KeyEnum.Utils.findEnum(OutputType.class, sOutputtype);
		this.sParameter = sParameter;
		this.sFilename = sFilename;
		this.bAttach = bAttach == null ? true : bAttach.booleanValue();
	}

	/**
	 * creates a report vo from a suitable masterdata cvo.
	 * @param mdvo
	 */
	public DefaultReportVO(MasterDataVO<UID> mdvo) {
		this(mdvo.getNuclosValueObject(),
				null,
				mdvo.getFieldValue(E.REPORTEXECUTION.name),
				mdvo.getFieldValue(E.REPORTEXECUTION.description),
				mdvo.getFieldUid(E.REPORTEXECUTION.datasource),
				mdvo.getFieldValue(E.REPORTEXECUTION.outputtype),
				null,
				null,
				false,
				Permission.NONE);
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportVO#getType()
	 */
	@Override
	public ReportType getType() {
		return type;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportVO#setType(org.nuclos.common.report.valueobject.DefaultReportVO.ReportType)
	 */
	@Override
	public void setType(ReportType type) {
		this.type = type;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportVO#getName()
	 */
	@Override
	public String getName() {
		return sName;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportVO#setName(java.lang.String)
	 */
	@Override
	public void setName(String sName) {
		this.sName = sName;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportVO#getDescription()
	 */
	@Override
	public String getDescription() {
		return sDescription;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportVO#setDescription(java.lang.String)
	 */
	@Override
	public void setDescription(String sDescription) {
		this.sDescription = sDescription;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportVO#getDatasourceId()
	 */
	@Override
	public UID getDatasourceId() {
		return dataSourceUID;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportVO#setDatasourceId(org.nuclos.common.UID)
	 */
	@Override
	public void setDatasourceId(final UID dataSourceUID) {
		this.dataSourceUID = dataSourceUID;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportVO#getOutputType()
	 */
	@Override
	public OutputType getOutputType() {
		return outputType;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportVO#setOutputType(org.nuclos.common.report.valueobject.DefaultReportVO.OutputType)
	 */
	@Override
	public void setOutputType(OutputType outputtype) {
		this.outputType = outputtype;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportVO#getParameter()
	 */
	@Override
	public String getParameter() {
		return sParameter;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportVO#setParameter(java.lang.String)
	 */
	@Override
	public void setParameter(String sParameter) {
		this.sParameter = sParameter;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportVO#getFilename()
	 */
	@Override
	public String getFilename() {
		return sFilename;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportVO#setFilename(java.lang.String)
	 */
	@Override
	public void setFilename(String sFilename) {
		this.sFilename = sFilename;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportVO#getAttachDocument()
	 */
	@Override
	public boolean getAttachDocument() {
		return bAttach;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportVO#setAttachDocument(boolean)
	 */
	@Override
	public void setAttachDocument(boolean bAttach) {
		this.bAttach = bAttach;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportVO#validate()
	 */
	@Override
	public void validate() throws CommonValidationException {
		if (StringUtils.isNullOrEmpty(this.getName())) {
			throw new CommonValidationException("report.error.validation.value");
		}
		if (StringUtils.isNullOrEmpty(this.getDescription())) {
			throw new CommonValidationException("report.error.validation.description");
		}
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("id=").append(getId());
		result.append(",name=").append(getName());
		result.append(",dsId=").append(getDatasourceId());
		result.append("]");
		return result.toString();
	}

}	// class ReportVO
