//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.valueobject;

import org.nuclos.common.UID;
import org.nuclos.server.common.valueobject.NuclosValueObject;

public class DynamicEntityVO extends DatasourceVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7697327372826786407L;
	private String entity;
	private UID entityUID;

	public DynamicEntityVO(NuclosValueObject<UID> evo, String sName,
		String sDescription, final UID entityUID, Boolean bValid, String sDatasourceXML, final UID nucletUID) {
		super(evo, sName, sDescription, bValid, sDatasourceXML, nucletUID, PERMISSION_NONE);
		this.setEntityUID(entityUID);
	}

	public DynamicEntityVO(String sName, String sDescription, final UID entityUID,
		String sDatasourceXML, final UID nucletUID, Boolean bValid) {
		super(sName, sDescription, sDatasourceXML, bValid, nucletUID);
	}
	
	/*
	 * Method is needed!
	 *  - @see handling as BeanPropertyCollectableField
	 * @deprecated
	 */
	public String getEntity() {
    	return entity;
    }
	public void setEntity(final String entity) {
    	this.entity = entity;
    }
	
	public UID getEntityUID() {
    	return entityUID;
    }
	public void setEntityUID(final UID entityUID) {
    	this.entityUID = entityUID;
    }

}
