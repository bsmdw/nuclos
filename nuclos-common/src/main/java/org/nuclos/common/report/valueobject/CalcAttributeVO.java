//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.valueobject;

import org.nuclos.common.UID;
import org.nuclos.server.common.valueobject.NuclosValueObject;

public class CalcAttributeVO extends DatasourceVO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3678641682100940197L;

	public CalcAttributeVO(final NuclosValueObject<UID> evo, final String name,
		final String sDescription, final Boolean bValid, final String sDatasourceXML, final UID nucletUID) {
		super(evo, name, sDescription, bValid, sDatasourceXML, nucletUID, PERMISSION_NONE);
	}

	public CalcAttributeVO(final String name, String sDescription,
			final String sDatasourceXML, final Boolean bValid, final UID nucletUID) {
		super(name, sDescription, sDatasourceXML, bValid, nucletUID);
	}
	
}
