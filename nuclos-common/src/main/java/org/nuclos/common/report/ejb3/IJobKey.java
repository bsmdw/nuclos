package org.nuclos.common.report.ejb3;

public interface IJobKey {
	
	String getName();
	
	String getGroup();

}
