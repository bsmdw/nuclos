//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.valueobject;

import org.nuclos.api.print.PrintProperties;
import org.nuclos.api.report.OutputFormat;
import org.nuclos.common.UID;
import org.nuclos.common.report.ByteArrayCarrier;
import org.nuclos.common2.KeyEnum;
import org.nuclos.common2.Localizable;
import org.nuclos.server.common.valueobject.NuclosValueObject;

import net.sf.jasperreports.engine.JRReport;

/**
 * 
 * {@link ReportOutputVO} value object
 * 
 * 
 * FIXME systematically refactored former ReportOutputVO class without any further improvment
 * 
 * @author Moritz Neuhaeuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public interface ReportOutputVO extends OutputFormat {

	/**
	 * FIXME use java enum instead of evil {@link KeyEnum}
	 */
	public static enum PageOrientation implements KeyEnum<Byte> {

		LANDSCAPE(JRReport.ORIENTATION_LANDSCAPE), PORTRAIT(JRReport.ORIENTATION_PORTRAIT);

		private final byte orientation;

		private PageOrientation(byte orientation) {
			this.orientation = orientation;
		}

		@Override
		public Byte getValue() {
			return orientation;
		}

	}


	/**
	 * FIXME use java enum instead of evil {@link KeyEnum}
	 */
	public static enum Destination implements KeyEnum<String>, Localizable {

		SCREEN("Screen", "reportDestination.screen"),
		FILE("File", "reportDestination.file"),
		PRINTER_CLIENT("PrinterClient", "reportDestination.printer_client"),
		PRINTER_SERVER("PrinterServer", "reportDestination.printer_server"),
		DEFAULT_PRINTER_CLIENT("DefaultPrinterClient", "reportDestination.default_printer_client"),
		DEFAULT_PRINTER_SERVER("DefaultPrinterServer", "reportDestination.default_printer_server");

		private final String value;
		private final String resourceId;

		Destination(String value, String resourceId) {
			this.value = value;
			this.resourceId = resourceId;
		}

		public String getLabel() {
			return this.value;
		}

		@Override
		public String getValue() {
			return value;
		}

		@Override
		public String toString() {
			return this.getLabel();
		}

		@Override
		public String getResourceId() {
			return resourceId;
		}
	}

	public static enum Format implements KeyEnum<String> {

		PDF(".pdf"), XLS(".xls"), XLSX(".xlsx"), CSV(".csv"), DOC(".doc"), DOCX(".docx"), TSV(".tsv");

		private final String extension;

		private Format(String extension) {
			this.extension = extension;
		}

		@Override
		public String getValue() {
			return name();
		}

		public String getExtension() {
			return extension;
		}
	}

	public NuclosValueObject<UID> getValueObject();
	
	public UID getReportUID();

	public Destination getDestination();

	public Format getFormat();

	public PageOrientation getPageOrientation();

	public boolean isColumnScaled();

	public String getParameter();

	public String getFilename();

	public String getSourceFile();

	public ByteArrayCarrier getReportCLS();

	public UID getDatasourceUID();

	public String getSheetname();

	public ByteArrayCarrier getSourceFileContent();


	public boolean isFirstOfMany();

	public boolean isLastOfMany();

	public String getDescription();

	public String getLocale();

	public boolean getAttachDocument();

	public String toString();
	
	public PrintProperties getPrintProperties();

	/**
	 * is mandatory
	 * 
	 * @return is mandatory
	 */
	public boolean getIsMandatory();

	/**
	 * assigned user
	 * 
	 * @return user {@link UID}
	 */
	public UID getUserId();

	/**
	 * assigned role
	 *  
	 * @return role {@link UID}
	 */
	public UID getRoleId();

	/**
	 * custom parameter published to the report
	 * 
	 * @return parameter
	 */
	public String getCustomParameter();
}