//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report;

import java.io.Serializable;
import java.util.List;

import org.nuclos.common.collect.collectable.CollectableFieldFormat;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common2.StringUtils;

public class ReportFieldDefinition implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 463582148556456078L;
	private String name;
	private Class<?> clazz;
	private int maxlength;
	private String label;
	private String outputformat;

	public ReportFieldDefinition(String name, Class<?> clazz, String label) {
		super();
		this.name = name;
		this.clazz = clazz;
		this.label = label;
	}

	public String getName() {
		return name;
	}

	public Class<?> getJavaClass() {
		return clazz;
	}

	public void setMaxLength(int i) {
		this.maxlength = i;
	}
	
	public int getMaxLength() {
		return maxlength;
	}

	public String getLabel() {
		return label;
	}
	
	public String getOutputformat() {
		return outputformat;
	}

	public void setOutputformat(String outputformat) {
		this.outputformat = outputformat;
	}

	@Override
	public String toString() {
		return "FieldDefinition [name=" + name + ", clazz=" + clazz
			+ ", maxlength=" + maxlength + "]";
	}

	public static String getPath(String path, final ResultVO resultVO, final List<ReportFieldDefinition> fields) {
		if (resultVO.getRowCount() == 0)
			return null;

		return StringUtils.replaceParameters(path, new Transformer<String, String>() {

			public String transform(String i) {
				Object val = getValue(i);
				if (val == null) {
					return "";
				}
				ReportFieldDefinition field = null;
				for (int j = 0; j < fields.size(); j++) {
					if (fields.get(j).getLabel().equals(i)) {
						field = fields.get(j);
						break;
					}
				}

				if (field != null) {
					return CollectableFieldFormat.getInstance(field.getJavaClass()).format(field.getOutputformat(), val);
				}
				return val != null ? val.toString() : "";
			}

			protected Object getValue(String field) {
				int index = -1;
				for (int i = 0; i < resultVO.getColumns().size(); i++) {
					if (resultVO.getColumns().get(i).getColumnLabel().equals(field)) {
						index = i;
						break;
					}
				}

				if (index == -1)
					return "";

				return ((Object[])resultVO.getRows().get(0))[index];
			}
		});
	}
}
