package org.nuclos.common.report.ejb3;

import java.io.Serializable;

import org.nuclos.common.UID;
import org.nuclos.common2.LangUtils;

public class JobKeyImpl implements IJobKey, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2468189650466090880L;

	private final UID uid;
	
	private final String group;
	
	public JobKeyImpl(UID name, String group) {
		this.uid = name;
		this.group = group;
	}

	@Override
	public String getName() {
		return uid.toString();
	}

	@Override
	public String getGroup() {
		return group;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof IJobKey)) return false;
		final IJobKey other = (IJobKey) o;
		return LangUtils.equal(getName(), other.getName()) 
				&& LangUtils.equal(getGroup(), other.getGroup());
	}
	
	@Override
	public int hashCode() {
		int result = 9272;
		if (uid != null) {
			result += 3 * uid.hashCode();
		}
		if (group != null) {
			result += 5 * group.hashCode();
		}
		return result;
	}

}
