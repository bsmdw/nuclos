package org.nuclos.common.preferences;

import java.util.List;
import java.util.Map;

import org.nuclos.common.UID;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Sebastian Debring on 3/19/2019.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PerspectivePreferencesDTO {

	private String name;
	private String boMetaId;
	private UID layoutId;
	private UID searchTemplatePrefId;
	private UID sideviewMenuPrefId;
	private Map<String, UID> subformTablePrefIds;
	private boolean layoutForNew;

	public PerspectivePreferencesDTO() {
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getBoMetaId() {
		return boMetaId;
	}

	public void setBoMetaId(final String boMetaId) {
		this.boMetaId = boMetaId;
	}

	public UID getLayoutId() {
		return layoutId;
	}

	public void setLayoutId(final UID layoutId) {
		this.layoutId = layoutId;
	}

	public UID getSearchTemplatePrefId() {
		return searchTemplatePrefId;
	}

	public void setSearchTemplatePrefId(final UID searchTemplatePrefId) {
		this.searchTemplatePrefId = searchTemplatePrefId;
	}

	public UID getSideviewMenuPrefId() {
		return sideviewMenuPrefId;
	}

	public void setSideviewMenuPrefId(final UID sideviewMenuPrefId) {
		this.sideviewMenuPrefId = sideviewMenuPrefId;
	}

	public Map<String, UID> getSubformTablePrefIds() {
		return subformTablePrefIds;
	}

	public void setSubformTablePrefIds(final Map<String, UID> subformTablePrefIds) {
		this.subformTablePrefIds = subformTablePrefIds;
	}

	public boolean isLayoutForNew() {
		return layoutForNew;
	}

	public void setLayoutForNew(final boolean layoutForNew) {
		this.layoutForNew = layoutForNew;
	}
}
