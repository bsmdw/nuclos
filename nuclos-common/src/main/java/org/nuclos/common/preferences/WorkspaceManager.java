package org.nuclos.common.preferences;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.prefs.Preferences;

import org.apache.log4j.Logger;
import org.nuclos.common.MutableBoolean;
import org.nuclos.common.UID;
import org.nuclos.common.WorkspaceDescription2;
import org.nuclos.common.WorkspaceDescriptionDefaultsFactory;
import org.nuclos.common.WorkspaceVO;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common2.ClientPreferences;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.PreferencesException;
import org.nuclos.server.common.ejb3.CommonPreferencesFacade;

public class WorkspaceManager {
	public static final String PREFS_NODE_WORKSPACE_ORDER = "workspaceorder";
	public static final String PREFS_NODE_WORKSPACE_ORDER_IDS = "workspaceorderids";
	
	public static final String PREFS_NODE_DEFAULT_WORKSPACE = "defaultworkspace";
	public static final String PREFS_NODE_LAST_WORKSPACE = "lastworkspace";
	public static final String PREFS_NODE_LAST_WORKSPACE_ID = "lastworkspaceid";
	public static final String PREFS_NODE_LAST_ALWAYS_OPEN_WORKSPACE = "lastalwaysopenworkspace";
	public static final String PREFS_NODE_LAST_ALWAYS_OPEN_WORKSPACE_ID = "lastalwaysopenworkspaceid";
	
	private static Logger LOG = Logger.getLogger(WorkspaceManager.class);
	private final List<WorkspaceVO> workspaces = new ArrayList<WorkspaceVO>();
	private WorkspaceVO selectedWorkspace = null;
	private List<UID> workspaceOrderIds;
	private List<String> workspaceOrder;
	
	private String defaultWorkspace;
	private String lastWorkspace;
	private UID lastWorkspaceId;
	private String lastAlwaysOpenWorkspace;
	private UID lastAlwaysOpenWorkspaceId;
	
	public WorkspaceManager(SpringLocaleDelegate localeDelegate) throws PreferencesException {
		final Preferences prefs = ClientPreferences.getInstance().getUserPreferences().node(MainFramePrefs.PREFS_NODE_MAINFRAME);
		if (localeDelegate == null) localeDelegate = SpringLocaleDelegate.getInstance();

		defaultWorkspace = prefs.get(PREFS_NODE_DEFAULT_WORKSPACE, localeDelegate.getMessage(
				"Workspace.Default","Standard"));
		lastWorkspace = prefs.get(PREFS_NODE_LAST_WORKSPACE, localeDelegate.getMessage(
				"Workspace.Default","Standard"));
		lastWorkspaceId = new UID(prefs.get(PREFS_NODE_LAST_WORKSPACE_ID, ""));
		lastAlwaysOpenWorkspace = prefs.get(PREFS_NODE_LAST_ALWAYS_OPEN_WORKSPACE, null);
		lastAlwaysOpenWorkspaceId = new UID(prefs.get(PREFS_NODE_LAST_ALWAYS_OPEN_WORKSPACE_ID, ""));
		workspaceOrderIds =
				PreferencesUtils.getUIDList(prefs, PREFS_NODE_WORKSPACE_ORDER_IDS);
		workspaceOrder =
				PreferencesUtils.getStringList(prefs, PREFS_NODE_WORKSPACE_ORDER);
	}
	
	private static WorkspaceManager wm;
	public static WorkspaceManager INSTANCE(SpringLocaleDelegate localeDelegate) {
		if (wm == null) try {
			wm = new WorkspaceManager(localeDelegate);
		} catch (PreferencesException pe) {
			LOG.error(pe.getMessage(), pe);
		}
		return wm;
	}
	
	public WorkspaceVO initAndGetWorkspace(CommonPreferencesFacade preferencesFacade) {
		setup(preferencesFacade);
		WorkspaceVO wovo = getWorkspaceToRestore(preferencesFacade, new MutableBoolean());
		WorkspaceDescription2 wd = loadWorkspaceFromDB(wovo, preferencesFacade);
		wovo.setWoDesc(wd);
		WorkspaceVO wovoSel = setSelected(wovo, preferencesFacade);
		return wovoSel;
	}
	
	public void setup(CommonPreferencesFacade preferencesFacade) {
		Collection<WorkspaceVO> allWorkspaces = preferencesFacade.getWorkspaceHeaderOnly(); 
		
		Set<UID> addedIds = new HashSet<UID>();
		Set<String> addedNames = new HashSet<String>();
		boolean atLeastOneWorkspaceIdFound = false; //may be user is transferred on other system with different ids...
		for (UID woid : workspaceOrderIds) {
			for (WorkspaceVO wovo : allWorkspaces) {
				if (LangUtils.equal(woid, wovo.getPrimaryKey())) {
					if (!addedIds.contains(woid)) {
						addedIds.add(woid);
						atLeastOneWorkspaceIdFound = true;
						workspaces.add(wovo);
					}
				}
			}
		}			
		
		if (!atLeastOneWorkspaceIdFound) {
			// no id matches, try to restore order by names
			for (String woname : workspaceOrder) {
				for (WorkspaceVO wovo : allWorkspaces) {
					if (LangUtils.equal(woname, wovo.getName())) {
						if (!addedNames.contains(woname)) {
							addedNames.add(woname);
							workspaces.add(wovo);
						}
					}
				}
			}
		}
		
		// add workspaces which are not in order
		for (WorkspaceVO wovo : allWorkspaces) {
			if (!workspaces.contains(wovo)) {
				workspaces.add(wovo);
			}
		}
	}
	
	public WorkspaceVO setSelected(WorkspaceVO wovo, CommonPreferencesFacade preferencesFacade) {
		for (WorkspaceVO wovoI : workspaces) {
			if (wovoI.equals(wovo)) {
				selectedWorkspace = wovoI;
				if (selectedWorkspace.getAssignedWorkspace() != null) {
					try {
						final WorkspaceVO assigned = preferencesFacade.getWorkspace(selectedWorkspace.getAssignedWorkspace());
						LOG.info(assigned);
					} catch (CommonBusinessException e) {
						LOG.warn("FAILED", e);
					}
				}
				
				// preferences may be changed (e.g. restore to assigned)...
				if (selectedWorkspace != wovo) {
					selectedWorkspace.importHeader(wovo.getWoDesc());
					selectedWorkspace.getWoDesc().removeAllEntityPreferences();
					selectedWorkspace.getWoDesc().addAllEntityPreferences(wovo.getWoDesc().getEntityPreferences());
					selectedWorkspace.getWoDesc().removeAllLayoutPreferences();
					selectedWorkspace.getWoDesc().addAllLayoutPreferences(wovo.getWoDesc().getLayoutPreferences());
					selectedWorkspace.getWoDesc().removeAllParameters();
					selectedWorkspace.getWoDesc().setAllParameters(wovo.getWoDesc().getParameters());
				}
				return wovoI;
			}
		}
		return null;
	}
	
	public void reset() {
		workspaceOrder.clear();
		workspaceOrderIds.clear();
		for (WorkspaceVO wovo : workspaces) {
			workspaceOrderIds.add(wovo.getPrimaryKey());
		}
		workspaces.clear();
	}
	
	public void replace(WorkspaceVO oldWS, WorkspaceVO newWS) {
		final int index = workspaces.indexOf(oldWS);
		workspaces.remove(oldWS);
		workspaces.add(index, newWS);
	}
	
	public void add(WorkspaceVO wovo) {
		workspaces.add(wovo);
	}
	
	public void add(int index, WorkspaceVO wovo) {
		workspaces.add(index, wovo);
	}

	public void remove(WorkspaceVO wovo) {
		workspaces.remove(wovo);
	}

	public WorkspaceVO getSelected() {
		return selectedWorkspace;
	}
	
	public List<WorkspaceVO> getWorkspaces() {
		return Collections.unmodifiableList(workspaces);
	}
	
	public List<WorkspaceVO> getWorkspaceHeaders() {
		return new ArrayList<WorkspaceVO>(workspaces);
	}
	
	public String getDefaultWorkspace() {
		return defaultWorkspace;
	}

	public String getLastWorkspaceFromPreferences() {
		return lastWorkspace;
	}

	public UID getLastWorkspaceIdFromPreferences() {
		return lastWorkspaceId;
	}

	public String getLastAlwaysOpenWorkspaceFromPreferences() {
		return lastAlwaysOpenWorkspace;
	}
	
	public void setDefaultWorkspace(String defaultWorkspace) {
		this.defaultWorkspace = defaultWorkspace;
	}

	public void setLastAlwaysOpenWorkspace(String lastAlwaysOpenWorkspace) {
		this.lastAlwaysOpenWorkspace = lastAlwaysOpenWorkspace;
	}

	public void setLastAlwaysOpenWorkspaceId(UID lastAlwaysOpenWorkspaceId) {
		this.lastAlwaysOpenWorkspaceId = lastAlwaysOpenWorkspaceId;
	}

	public UID getLastAlwaysOpenWorkspaceIdFromPreferences() {
		return lastAlwaysOpenWorkspaceId;
	}
	
	public WorkspaceDescription2 createDefaultWorkspace() {
		WorkspaceDescription2 wd = new WorkspaceDescription2();
		wd.setName(getDefaultWorkspace());
		wd.setHideName(true);
		wd.setNuclosResource("org.nuclos.client.resource.icon.glyphish.174-imac.png");
		return wd;
	}
	
	public WorkspaceVO createAndStoreDefaultWorkspace(WorkspaceDescription2 wdOrigin, CommonPreferencesFacade preferencesFacade) throws CommonBusinessException {
		WorkspaceVO wovo = new WorkspaceVO(WorkspaceDescriptionDefaultsFactory.createOldMdiStyle());
		wovo.importHeader(wdOrigin);
		wovo = preferencesFacade.storeWorkspace(wovo);
		return wovo;
	}

	public WorkspaceVO getWorkspaceToRestore(CommonPreferencesFacade preferencesFacade, MutableBoolean newOneCreated) {
		WorkspaceVO wovoToRestore = null;
		
		List<WorkspaceVO> alwaysOpenWorkspaces = CollectionUtils.select(
				getWorkspaceHeaders(), new Predicate<WorkspaceVO>() {
			@Override
			public boolean evaluate(WorkspaceVO t) {
				return t.getWoDesc().isAlwaysOpenAtLogin();
			}
		});

		if (alwaysOpenWorkspaces.size() > 0) {
			if (alwaysOpenWorkspaces.size() == 1) {
				wovoToRestore = alwaysOpenWorkspaces.get(0);
			} else {
				if (lastAlwaysOpenWorkspaceId != null &&
						lastAlwaysOpenWorkspace != null) {
					for (WorkspaceVO wovo : alwaysOpenWorkspaces) {
						if (lastAlwaysOpenWorkspaceId.equals(wovo.getPrimaryKey())) {
							wovoToRestore = wovo;
							break; // priority of id is higher
						}
						// if id not found try to search for name
						if (lastAlwaysOpenWorkspace.equals(wovo.getName())) {
							wovoToRestore = wovo;
						}
					}
				} else {
					wovoToRestore = alwaysOpenWorkspaces.get(0); // select first always open workspace
				}
			}
		} else {
			for (WorkspaceVO wovo : getWorkspaceHeaders()) {
				if (lastWorkspaceId.equals(wovo.getPrimaryKey())) {
					wovoToRestore = wovo;
					break; // priority of id is higher
				}
				// if id not found try to search for name
				if (lastWorkspace.equals(wovo.getName())) {
					wovoToRestore = wovo;
				}
			}
		}


		if (wovoToRestore == null && !getWorkspaceHeaders().isEmpty()) {
			wovoToRestore = getWorkspaceHeaders().get(0);
		}

		if (wovoToRestore == null) try {
			wovoToRestore = createAndStoreDefaultWorkspace(createDefaultWorkspace(), preferencesFacade);
			newOneCreated.setValue(true);
			selectedWorkspace = wovoToRestore;
		} catch (CommonBusinessException cbe) {
			LOG.error(cbe.getMessage(), cbe);
			throw new NullPointerException();
		}
		return wovoToRestore;
	}
	
	public synchronized WorkspaceDescription2 loadWorkspaceFromDB(WorkspaceVO wovo, CommonPreferencesFacade preferencesFacade) {
		WorkspaceDescription2 wd;
		if (wovo.getWoDesc().getFrames().isEmpty()) {
			try {
				wd = preferencesFacade.getWorkspace(wovo.getPrimaryKey()).getWoDesc();
			} catch (Exception e) {
				try {
					wovo = createAndStoreDefaultWorkspace(wovo.getWoDesc(), preferencesFacade);
					wd = wovo.getWoDesc();
				} catch (Exception e1) {
					wd = WorkspaceDescriptionDefaultsFactory.createOldMdiStyle();
				}
			}
			if (wd.getFrames().isEmpty()) {
				wd = WorkspaceDescriptionDefaultsFactory.createOldMdiStyle();
			}
		} else {
			wd = wovo.getWoDesc();
		}

		try {
			wd.getHomeTabbed();
			wd.getHomeTreeTabbed();
		} catch (Exception e) {
			wd = WorkspaceDescriptionDefaultsFactory.createOldMdiStyle();
		}

		// set workspace preferences
		if (wovo.getWoDesc() != wd) {
			wovo.getWoDesc().removeAllEntityPreferences();
			wovo.getWoDesc().addAllEntityPreferences(wd.getEntityPreferences());
			wovo.getWoDesc().removeAllLayoutPreferences();
			wovo.getWoDesc().addAllLayoutPreferences(wd.getLayoutPreferences());
			wovo.getWoDesc().removeAllParameters();
			wovo.getWoDesc().setAllParameters(wd.getParameters());
		}

		if (wd.isAlwaysOpenAtLogin()) {
			setLastAlwaysOpenWorkspace(wovo.getName());
			setLastAlwaysOpenWorkspaceId(wovo.getPrimaryKey());
		}
		return wd;
	}
}