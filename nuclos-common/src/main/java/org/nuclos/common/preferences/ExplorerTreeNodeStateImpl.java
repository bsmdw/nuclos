package org.nuclos.common.preferences;

import java.io.Serializable;
import java.util.List;

public class ExplorerTreeNodeStateImpl implements IExplorerTreeNodeState, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3968499714556664236L;
	private final List<String> lstExpandedPaths;
	private final int[] selectedRows;
	
	public ExplorerTreeNodeStateImpl(List<String> lstExpandedPaths,
			int[] selectedRows) {
		this.lstExpandedPaths = lstExpandedPaths;
		this.selectedRows = selectedRows;
	}
	
	@Override
	public List<String> getLstExpandedPaths() {
		return lstExpandedPaths;
	}

	@Override
	public int[] getSelectedRows() {
		return selectedRows;
	}

}
