package org.nuclos.common.preferences;

import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

import org.apache.log4j.Logger;
import org.nuclos.common.CollectableEntityFieldWithEntityForExternal;
import org.nuclos.common.ModuleProvider;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.entityobject.CollectableEOEntityProvider;
import org.nuclos.common.genericobject.GenericObjectUtils;
import org.nuclos.common2.exception.PreferencesException;
import org.springframework.beans.factory.annotation.Autowired;

public class PreferencesSupport {
	
	private static final Logger LOG = Logger.getLogger(PreferencesSupport.class);	
	
	// Spring injection
	
	@Autowired
	private ModuleProvider moduleProvider;
	
	@Autowired
	private CollectableEOEntityProvider colllectableEntityProvider;
	
	// end of Spring injection
	
	PreferencesSupport() {
	}
	
	/**
	 * @deprecated This is an evil copy of 
	 * 		org.nuclos.client.genericobject.GenericObjectClientUtils.readCollectableEntityFieldsFromPreferences(Preferences, CollectableEntity)
	 * 		But it also works on MasterData. Thus it is difficult to get rid of it!
	 */
	public List<CollectableEntityField> readCollectableEntityFieldsFromPreferences(Preferences prefs, CollectableEntity clcte, String sPrefsNodeFields, String sPrefsNodeEntities) {
		List<UID> lstSelectedFields;
		List<UID> lstSelectedEntities;
		try {
			lstSelectedFields = PreferencesUtils.getUidList(prefs, sPrefsNodeFields);
			lstSelectedEntities = PreferencesUtils.getUidList(prefs, sPrefsNodeEntities);
		}
		catch (PreferencesException ex) {
			lstSelectedFields = new ArrayList<UID>();
			lstSelectedEntities = new ArrayList<UID>();
			// no exception is thrown here.
		}
		assert lstSelectedFields != null;
		assert lstSelectedEntities != null;

		if (lstSelectedFields.size() != lstSelectedEntities.size()) {
			lstSelectedFields = new ArrayList<UID>();
			lstSelectedEntities = new ArrayList<UID>();
		}

		final List<CollectableEntityField> result = new ArrayList<CollectableEntityField>();
		for (int i = 0; i < lstSelectedFields.size(); i++) {
			final UID field = lstSelectedFields.get(i);
			final UID entity = lstSelectedEntities.get(i);
			try {
				final CollectableEntity clcteForField = (entity == null) ? 
						clcte : colllectableEntityProvider.getCollectableEntity(entity);
				if (moduleProvider.isModule(clcteForField.getUID())) {
					result.add(GenericObjectUtils.getCollectableEntityFieldForResult(clcteForField, field, clcte));
				}
				else {
					result.add(new CollectableEntityFieldWithEntityForExternal(clcteForField, field, false, true));
				}		
			}
			catch (Exception ex) {
				// ignore unknown fields
				LOG.warn("readCollectableEntityFieldsFromPreferences failed (unknown field?): " + ex.getMessage());
			}
		}
		return result;
	}	

}
