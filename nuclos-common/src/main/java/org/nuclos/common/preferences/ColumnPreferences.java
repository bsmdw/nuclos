//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.preferences;

import java.io.Serializable;

import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;

public class ColumnPreferences implements Serializable {
	private static final long serialVersionUID = 6637996725938917463L;

	public static final int TYPE_DEFAULT = 0;
	public static final int TYPE_EOEntityField = 1;
	public static final int TYPE_GenericObjectEntityField = 2;
	public static final int TYPE_MasterDataForeignKeyEntityField = 3;
	public static final int TYPE_EntityFieldWithEntity = 4;
	public static final int TYPE_EntityFieldWithEntityForExternal = 5;

	private UID column;
	private UID entity;
	private int width;
	private boolean fixed;

	private int type;

	/**
	 * LIN-381:
	 * Filter String set on this column by ISubFormFilter.
	 * <p>
	 * Only available for SubForms, null in other cases.
	 */
	private Object columnFilter;

	/**
	 * LIN-381:
	 * Filter String set on this column by ISubFormFilter.
	 * <p>
	 * Only available for SubForms, null in other cases.
	 */
	private ComparisonOperator columnFilterOp;

	private Boolean webclientSelected;

	public ColumnPreferences() {
		// for debug
	}

	public UID getColumn() {
		return column;
	}
	public void setColumn(UID column) {
		this.column = column;
	}
	public int getWidth() {
		return width;
	}
	public int setWidth(Integer width) {
		if (width==null) width=0;
		else if (width > 16000) width = 16000; // NUCLOS-6553
		this.width = width;
		return width;
	}
	public UID getEntity() {
		return entity;
	}
	public void setEntity(UID entity) {
		this.entity = entity;
	}
	public boolean isFixed() {
		return fixed;
	}
	public void setFixed(Boolean fixed) {
		if (fixed==null) fixed=false;
		this.fixed = fixed;
	}
	public int getType() {
		return type;
	}
	public void setType(Integer type) {
		if (type==null) type=0;
		this.type = type;
	}
	public Object getColumnFilter() {
		return columnFilter;
	}
	public void setColumnFilter(Object columnFilter) {
		this.columnFilter = columnFilter;
	}
	public ComparisonOperator getColumnFilterOp() {
		return columnFilterOp;
	}
	public void setColumnFilterOp(ComparisonOperator op) {
		this.columnFilterOp = op;
	}
	public void setWebclientSelected(final Boolean webclientSelected) {
		this.webclientSelected = webclientSelected;
	}
	public Boolean getWebclientSelected() {
		return webclientSelected;
	}

	@Override
	public int hashCode() {
		if (column == null)
			return 0;
		return column.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ColumnPreferences) {
			ColumnPreferences other = (ColumnPreferences) obj;
			return RigidUtils.equal(getColumn(), other.getColumn()) &&
					RigidUtils.equal(getEntity(), other.getEntity()) &&
					RigidUtils.equal(getType(), other.getType());
		}
		return super.equals(obj);
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("ColumnPreferences[").append(entity).append(", ");
		sb.append(column).append(", ");
		sb.append(type).append(", ");
		sb.append(columnFilter);
		sb.append("]");
		return sb.toString();
	}

	public ColumnPreferences copy() {
		ColumnPreferences result = new ColumnPreferences();
		result.setColumn(column);
		result.setEntity(entity);
		result.setFixed(fixed);
		result.setWidth(width);
		result.setType(type);
		result.setColumnFilter(columnFilter);
		result.setColumnFilterOp(columnFilterOp);
		result.setWebclientSelected(webclientSelected);
		return result;
	}

}
