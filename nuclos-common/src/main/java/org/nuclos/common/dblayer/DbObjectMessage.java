//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.dblayer;

import java.io.Serializable;
import java.util.List;

import org.nuclos.api.Message;

public class DbObjectMessage implements Message, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4540184719983039584L;

	private final String message;
	
	private final String title;
	
	private final boolean overlay;
	
	private final List<String> script;
	
	private final StringBuffer warnings;

	public DbObjectMessage(String message, String title, boolean overlay, List<String> script, StringBuffer warnings) {
		this.message = message;
		this.title = title;
		this.overlay = overlay;
		this.script = script;
		this.warnings = warnings;
	}

	public String getMessage() {
		return message;
	}
	
	@Override
	public String getTitle() {
		return title;
	}

	public boolean isOverlay() {
		return overlay;
	}

	public List<String> getScript() {
		return script;
	}

	public StringBuffer getWarnings() {
		return warnings;
	}

	@Override
	public String toString() {
		return "DbObjectMessage[message=" + getMessage() + ",title=" + getTitle() + ",overlay=" + isOverlay() + "]";
	}

}
