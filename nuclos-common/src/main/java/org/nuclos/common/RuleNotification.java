//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import org.nuclos.common2.LangUtils;

/**
 * Notification sent by rules for messages on the client.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * @author	<a href="mailto:uwe.allner@novabit.de">uwe.allner</a>
 * @version 01.00.00
 */
public class RuleNotification extends SimpleClientNotification {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8097367590070992094L;
	private final String sRuleName;
	private String sourceIdentifier;
	private String targetIdentifier;
	private Object sourceId;
	private Object targetId;
	private UID sourceEntityUID;
	private UID targetEntityUID;


	public RuleNotification(Priority priority, String sMessage, String sRuleName) {
		super(priority, sMessage);

		this.sRuleName = sRuleName;
	}

	public String getRuleName() {
		return sRuleName;
	}

	public Object getSourceId() {
		return sourceId;
	}

	public void setSourceId(Object sourceId) {
		this.sourceId = sourceId;
	}

	public String getSourceIdentifier() {
		return sourceIdentifier;
	}

	public void setSourceIdentifier(String sourceIdentifier) {
		this.sourceIdentifier = sourceIdentifier;
	}

	public Object getTargetId() {
		return targetId;
	}

	public void setTargetId(Object targetId) {
		this.targetId = targetId;
	}

	public String getTargetIdentifier() {
		return targetIdentifier;
	}

	public void setTargetIdentifier(String targetIdentifier) {
		this.targetIdentifier = targetIdentifier;
	}

	public void setSourceEntityUID(final UID sourceEntityUID) {
		this.sourceEntityUID = sourceEntityUID;
	}

	public void setTargetEntityUID(final UID targetEntityUID) {
		this.targetEntityUID = targetEntityUID;
	}

	public UID getSourceEntityUID() {
		return sourceEntityUID;
	}

	public UID getTargetEntityUID() {
		return targetEntityUID;
	}

	@Override
	public boolean equals(Object other) {
		if (this == other) return true;
		if (!(other instanceof RuleNotification)) return false;
		final RuleNotification o = (RuleNotification) other;
		return LangUtils.equal(getMessage(), o.getMessage())
				&& LangUtils.equal(getSourceIdentifier(), o.getSourceIdentifier())
				&& LangUtils.equal(getTargetIdentifier(), o.getTargetIdentifier())
				&& LangUtils.equal(getSourceId(), o.getSourceId())
				&& LangUtils.equal(getTargetId(), o.getTargetId())
				&& LangUtils.equal(getSourceEntityUID(), o.getSourceEntityUID())
				&& LangUtils.equal(getTargetEntityUID(), o.getTargetEntityUID());
	}
	
	@Override 
	public int hashCode() {
		int result = 8728721;
		String s = getMessage();
		if (s != null) {
			result += 3 * s.hashCode();
		}
		s = getSourceIdentifier();
		if (s != null) {
			result += 5 * s.hashCode();
		}
		s = getTargetIdentifier();
		if (s != null) {
			result += 7 * s.hashCode();
		}
		Object i = getSourceId();
		if (i != null) {
			result += 11 * i.hashCode();
		}
		i = getTargetId();
		if (i != null) {
			result += 13 * i.hashCode();
		}
		UID uid = getSourceEntityUID();
		if (uid != null) {
			result += 21 * uid.hashCode();
		}
		uid = getTargetEntityUID();
		if (uid != null) {
			result += 23 * uid.hashCode();
		}
		return result;
	}

	@Override
	public String toString() {
		return sRuleName + ": " + getMessage();
	}
	
	public String toDescription() {
		final StringBuilder result = new StringBuilder();
		result.append("RuleNotification[");
		result.append("msg=").append(getMessage());
		result.append(",srcId").append(sourceId);
		result.append(",srcIdent=").append(sourceIdentifier);
		result.append(",targedId=").append(targetId);
		result.append(",targetIdent=").append(targetIdentifier);
		result.append(",srcEntity").append(sourceEntityUID);
		result.append(",targedEntity=").append(targetEntityUID);
		result.append(",rule=").append(sRuleName);
		result.append("]");
		return result.toString();
	}

}	// class RuleNotification
