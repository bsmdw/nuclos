package org.nuclos.common;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Created by Sebastian Debring on 4/1/2019.
 */
public class ImageUtils {

	public static byte[] scaleImage(final byte[] imageContent, int reqWidth, int reqHeight) throws IOException {
		INuclosImageProducer imageProducer = ImageProducerFactory.createImageProducer(imageContent);
		BufferedImage bufferedImage = imageProducer.getBufferedImage(imageContent);
		int iHeight = bufferedImage.getHeight();
		int iWidth = bufferedImage.getWidth();

		// always scale aspect ratio...
		int diffHeight = iWidth - reqWidth;
		int diffWidth = iHeight - reqHeight;
		if (diffHeight > 0 && reqWidth <= 0) {
			reqWidth = iWidth * reqHeight / iHeight;
		} else if (diffWidth > 0 && reqHeight <= 0) {
			reqHeight = iHeight * reqWidth / iWidth;
		} else if (diffWidth > 0 && diffWidth >= diffHeight) {
			reqWidth = iWidth * reqHeight / iHeight;
		} else if (diffHeight > 0 && diffHeight >= diffWidth) {
			reqHeight = iHeight * reqWidth / iWidth;
		} else {
			// scale up (not needed)
			return imageContent;
		}

		Dimension dim = new Dimension(reqWidth, reqHeight);
		return imageProducer.getScaledImageBytes(imageContent, dim);
	}
}
