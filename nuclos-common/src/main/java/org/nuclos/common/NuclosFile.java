//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.io.Serializable;

import org.nuclos.server.common.valueobject.DocumentFileBase;

/**
 * TODO: There is also org.nuclos.common2.File.
 * Perhaps there should be only one. (tp)
 */
public class NuclosFile implements org.nuclos.api.common.NuclosFile, Serializable {	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8384063293775609942L;
	private UID id;
	private Long attachmentId;
	private String name;
	private byte[] content;
	
	public NuclosFile(String fileName, byte[] fileContents) {
		super();
		this.id = DocumentFileBase.newFileUID();
		this.name = fileName;
		this.content = fileContents;
	}
	
	public NuclosFile(Long attachmentId, String fileName, byte[] fileContents) {
		super();
		this.attachmentId = attachmentId;
		this.name = fileName;
		this.content = fileContents;
	}
	
	public NuclosFile(UID id, String fileName, byte[] fileContents) {
		super();
		this.id = id;
		this.name = fileName;
		this.content = fileContents;
	}
	
	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public byte[] getContent() {
		return content;
	}
	
	@Override
	public void setContent(byte[] content) {
		this.content = content;
	}

	public UID getId() {
		return id;
	}

	public void setId(UID id) {
		this.id = id;
	}

	public Long getAttachmentId() {
		return attachmentId;
	}

	public void setAttachmentId(Long attachmentId) {
		this.attachmentId = attachmentId;
	}

	@Override
	public NuclosFileLink toLink() {
		return new NuclosFileLink(this);
	}
}
