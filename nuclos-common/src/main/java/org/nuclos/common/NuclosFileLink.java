package org.nuclos.common;

import org.nuclos.api.UID;
import org.nuclos.server.documentfile.DocumentFileFacadeRemote;

;

public class NuclosFileLink implements org.nuclos.api.common.NuclosFileLink {
	
	private UID fileId;
	
	private NuclosFile file;
	
	public NuclosFileLink(NuclosFile file) {
		if (file == null) {
			throw new IllegalArgumentException("file must not be null");
		}
		this.file = file;
		this.fileId = file.getId();
	}
	
	public NuclosFileLink(UID fileId) {
		if (fileId == null) {
			throw new IllegalArgumentException("fileId must not be null");
		}
		this.fileId = fileId;
	}
	
	public UID getFileId() {
		return fileId;
	}
	
	@Override
	public NuclosFile toFile() {
		if (file == null) {
			DocumentFileFacadeRemote documentFileFacade = (DocumentFileFacadeRemote) SpringApplicationContextHolder.getBean("documentFileService");
			file = documentFileFacade.loadContent((org.nuclos.common.UID) fileId);
		}
		return file;
	}
}
