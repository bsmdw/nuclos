package org.nuclos.common.customcomp.resplan;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;

/**
 * @author Oliver Brausch
 */

//Version
@XmlType
@XmlRootElement(name="planElement")
public class PlanElement<R> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7850597630687157855L;
	final public static int ENTRY = 1;
	final public static int MILESTONE = 2;
	final public static int RELATION = 3;
	
	final public String[] types = new String[]{
			"None", "Buchung", "Meilenstein", "Verbindung" 
	};
	
	private int type;
	private UID entity;
	private UID primaryField;
	private UID secondaryField;
	private UID bookerField;
	
	private UID dateFromField;
	private UID dateUntilField;
	private String timePeriodsString;
	private UID timeFromField;
	private UID timeUntilField;

	private String labelText;
	private String toolTipText;
	
	private String scriptingEntryCellMethod;
	
	private int presentation;
	private int fromPresentation;
	private int toPresentation;
	private boolean newRelationFromController;
	
	private String color;
	private String fromIcon;
	private String toIcon;
	
	private int fixedExtend;
	private int order;
	private boolean isCascade;

	private List<PlanElementLocaleVO> planElementLocale;
	
	/**
	 * DO NOT USE
	 * 
	 * Use ResPlanConfigVO.newPlanElementInstance() instead
	 */
	public PlanElement() {
		super();
	}
	
	public PlanElement(PlanElement ple) {
		type = ple.getType();
		entity = ple.getEntity();
		primaryField = ple.getPrimaryField();
		secondaryField = ple.getSecondaryField();
		bookerField = ple.getBookerField();
		
		dateFromField = ple.getDateFromField();
		dateUntilField = ple.getDateUntilField();
		timePeriodsString = ple.getTimePeriodsString();
		timeFromField = ple.getTimeFromField();
		timeUntilField = ple.getTimeUntilField();

		labelText = ple.getLabelText();
		toolTipText = ple.getToolTipText();
		
		scriptingEntryCellMethod = ple.getScriptingEntryCellMethod();
		
		presentation = ple.getPresentation();
		fromPresentation = ple.getFromPresentation();
		toPresentation = ple.getToPresentation();
		newRelationFromController = ple.isNewRelationFromController();
		
		color = ple.getColor();
		fromIcon = ple.getFromIcon();
		toIcon = ple.getToIcon();
		
		fixedExtend = ple.getFixedExtend();
		order = ple.getOrder();
		isCascade = ple.isCascade();

		planElementLocale = ple.getPlanElementLocaleVO();
	}

	public boolean isMileStone() {
		return type == MILESTONE;
	}
	
	public boolean hasTime() {
		return timeFromField != null && timeUntilField != null;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof PlanElement) {
			return hashCode() == obj.hashCode();
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return RigidUtils.hashCode(entity) ^ RigidUtils.hashCode(primaryField) ^ RigidUtils.hashCode(type) 
				^ RigidUtils.hashCode(primaryField) ^ RigidUtils.hashCode(secondaryField) ^ RigidUtils.hashCode(dateFromField);
	}

	@XmlElement(name="entity")
	public UID getEntity() {
		return entity;
	}

	public void setEntity(UID eEntity) {
		this.entity = eEntity;
	}

	@XmlElement(name="primaryField")
	public UID getPrimaryField() {
		return primaryField;
	}

	public void setPrimaryField(UID primaryField) {
		this.primaryField = primaryField;
	}
	
	@XmlElement(name="secondaryField")
	public UID getSecondaryField() {
		return secondaryField;
	}

	public void setSecondaryField(UID secondaryField) {
		this.secondaryField = secondaryField;
	}
	
	@XmlElement(name="BookerField")
	public UID getBookerField() {
		return bookerField;
	}

	public void setBookerField(UID secondaryField) {
		this.bookerField = secondaryField;
	}

	@XmlElement(name="dateFromField")
	public UID getDateFromField() {
		return dateFromField;
	}

	public void setDateFromField(UID dateFromField) {
		this.dateFromField = dateFromField;
	}

	@XmlElement(name="dateUntilField")
	public UID getDateUntilField() {
		return dateUntilField;
	}

	public void setDateUntilField(UID dateUntilField) {
		this.dateUntilField = dateUntilField;
	}

	@XmlElement(name="timePeriodsString")
	public String getTimePeriodsString() {
		return timePeriodsString;
	}

	public void setTimePeriodsString(String timePeriodsString) {
		this.timePeriodsString = timePeriodsString;
	}

	@XmlElement(name="timeFromField")
	public UID getTimeFromField() {
		return timeFromField;
	}

	public void setTimeFromField(UID timeFromField) {
		this.timeFromField = timeFromField;
	}

	@XmlElement(name="timeUntilField")
	public UID getTimeUntilField() {
		return timeUntilField;
	}

	public void setTimeUntilField(UID timeUntilField) {
		this.timeUntilField = timeUntilField;
	}

	@XmlElement(name="labelText")
	public String getLabelText() {
		return labelText;
	}

	public void setLabelText(String labelText) {
		this.labelText = labelText;
	}

	@XmlElement(name="toolTipText")
	public String getToolTipText() {
		return toolTipText;
	}

	public void setToolTipText(String toolTipText) {
		this.toolTipText = toolTipText;
	}

	@XmlElement(name="scriptingEntryCellMethod")
	public String getScriptingEntryCellMethod() {
		return scriptingEntryCellMethod;
	}

	public void setScriptingEntryCellMethod(String scriptingEntryCellMethod) {
		this.scriptingEntryCellMethod = scriptingEntryCellMethod;
	}

	@XmlElement(name="type")
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	@XmlElement(name="presentation")
	public int getPresentation() {
		return presentation;
	}

	public void setPresentation(int presentation) {
		this.presentation = presentation;
	}

	@XmlElement(name="fromPresentation")
	public int getFromPresentation() {
		return fromPresentation;
	}

	public void setFromPresentation(int fromPresentation) {
		this.fromPresentation = fromPresentation;
	}

	@XmlElement(name="toPresentation")
	public int getToPresentation() {
		return toPresentation;
	}

	public void setToPresentation(int toPresentation) {
		this.toPresentation = toPresentation;
	}

	@XmlElement(name="newRelationFromController")
	public boolean isNewRelationFromController() {
		return newRelationFromController;
	}

	public void setNewRelationFromController(boolean newRelationFromController) {
		this.newRelationFromController = newRelationFromController;
	}

	@XmlElement(name="color")
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@XmlElement(name="fromIcon")
	public String getFromIcon() {
		return fromIcon;
	}

	public void setFromIcon(String fromIcon) {
		this.fromIcon = fromIcon;
	}

	@XmlElement(name="toIcon")
	public String getToIcon() {
		return toIcon;
	}

	public void setToIcon(String toIcon) {
		this.toIcon = toIcon;
	}

	@XmlElement(name="planElementLocale")
	public List<PlanElementLocaleVO> getPlanElementLocaleVO() {
		return planElementLocale;
	}

	public void setPlanElementLocaleVO(List<PlanElementLocaleVO> planElementLocale) {
		this.planElementLocale = planElementLocale;
	}
	
	@XmlElement(name="fixedExtends")
	public int getFixedExtend() {
		return fixedExtend;
	}

	public void setFixedExtend(int fixedExtend) {
		this.fixedExtend = fixedExtend;
	}
	
	@XmlElement(name="order")
	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}
	
	@XmlElement(name="isCascade")
	public boolean isCascade() {
		return isCascade;
	}

	public void setCascade(boolean isCascade) {
		this.isCascade = isCascade;
	}
	
	public void transferResources(List<ResPlanResourceVO> resources) {
		planElementLocale = new ArrayList<PlanElementLocaleVO>();
		for (ResPlanResourceVO rvo : resources) {
			PlanElementLocaleVO peVO = new PlanElementLocaleVO();
			peVO.setLocaleId(rvo.getLocaleId());
			peVO.setLocaleLabel(rvo.getLocaleLabel());
			peVO.setBookingLabel(rvo.getBookingLabel());
			peVO.setBookingTooltip(rvo.getBookingTooltip());
			planElementLocale.add(peVO);
		}
	}
	
	public String toString() {
		return types[type];
	}
}