//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.cache;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.annotation.security.RolesAllowed;

import org.apache.log4j.Logger;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.TrueCondition;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.ejb3.EntityObjectFacadeRemote;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.cache.AbstractCache;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

/**
 * Some BOs any contain only a few instances but are used frequently. This
 * is a spring bean to cache them.
 * <p>
 * All instances (or all instances from a CollectableSearchExpression) are 
 * cached unconditionally. No eviction is done at any time. Instances
 * should be considered as immutable.
 * </p>
 * 
 * @author Thomas Pasch
 */
@Component
@RolesAllowed("Login")
public class AllEntityObjectsCache<PK> implements IAllEntityObjectsCache<PK> {
	
	private static final Logger LOG = Logger.getLogger(AllEntityObjectsCache.class);
	
	static final Cache NULL_CACHE = new AbstractCache() {

		@Override
		public Object getIfPresent(Object key) {
			throw new UnsupportedOperationException();
		}
	}; 
	
	private static IAllEntityObjectsCache INSTANCE;
	
	// Spring injection
	
	@Autowired
	private EntityObjectFacadeRemote entityObjectRemote;
	
	// end of Spring injection
	
	private final Cache<Object,EntityObjectVO<PK>> nullCache = (Cache<Object,EntityObjectVO<PK>>) NULL_CACHE;
	
	private final ConcurrentMap<UID, CollectableSearchExpression> configuration = 
			new ConcurrentHashMap<UID, CollectableSearchExpression>();
	
	private final ConcurrentMap<UID, Cache<Object, EntityObjectVO<PK>>> caches = 
			new ConcurrentHashMap<UID, Cache<Object, EntityObjectVO<PK>>>();
	
	AllEntityObjectsCache() {
		INSTANCE = this;
	}
	
	/**
	 * @deprecated Use spring injection instead.
	 * @return
	 */
	public static IAllEntityObjectsCache getInstance() {
		return INSTANCE;
	}
	
	@Override
	public void register(UID entity, boolean fillImmediate) throws CommonPermissionException {
		final CollectableSearchCondition cond = TrueCondition.TRUE;
		final CollectableSearchExpression expr = new CollectableSearchExpression(cond);
		register(entity, expr, fillImmediate);
	}
	
	@Override
	public void register(UID entity, CollectableSearchExpression expr, boolean fillImmediate) throws CommonPermissionException {
		final CollectableSearchExpression old = configuration.get(entity);
		if (old != null && old.equals(expr)) {
			final String msg = "Refill tried, but with different CollectableSearchExpression: old=" + old + " new=" + expr;
			LOG.error(msg);
			throw new IllegalStateException(msg);
		}
		configuration.put(entity, expr);
		if (fillImmediate) {
			refill(entity);
		}
	}
	
	@Override
	public void refill(UID entity) throws CommonPermissionException {
		final CollectableSearchExpression expr = configuration.get(entity);
		final long size = entityObjectRemote.countEntityObjectRows(entity, expr);
		if (size > 2000) {
			final String msg = "Entity with UID " + entity + " is too big to be cached completely";
			LOG.error(msg);
			throw new IllegalStateException(msg);
			// return;
		}
		final Collection<EntityObjectVO<PK>> allInstances = 
				entityObjectRemote.getEntityObjectsChunk(entity, expr, new ResultParams(size, true), null);
		
		final Cache<Object, EntityObjectVO<PK>> cache = CacheBuilder.newBuilder().build();
		for (EntityObjectVO<PK> i: allInstances) {
			cache.put((PK) i.getPrimaryKey(), i);
		}
		caches.put(entity, cache);
	}
	
	@Override
	public void invalidate(UID entity) {
		caches.put(entity, nullCache);
	}
	
	@Override
	public EntityObjectVO<PK> get(UID entity, PK id) throws CommonPermissionException {
		Cache<Object,EntityObjectVO<PK>> cache = caches.get(entity);
		if (cache == null || cache.equals(nullCache)) {
			refill(entity);
			cache = caches.get(entity);
		}
		return cache.getIfPresent(id);
	}
	
	@Override
	public Collection<EntityObjectVO<PK>> getAll(UID entity) throws CommonPermissionException {
		Cache<Object,EntityObjectVO<PK>> cache = caches.get(entity);
		if (cache == null || cache.equals(nullCache)) {
			refill(entity);
			cache = caches.get(entity);
		}
		return cache.asMap().values();
	}

	@Override
	public void delete(UID entity, PK pk, boolean deleteInDb) throws CommonPermissionException {
		if (deleteInDb) {
			entityObjectRemote.removeEntity(entity, pk);
		}
		final Cache<Object,EntityObjectVO<PK>> cache = caches.get(entity);
		if (cache != null && !cache.equals(nullCache)) {
			cache.invalidate(pk);
		}
	}

	@Override
	public void add(EntityObjectVO<PK> instance, boolean addToDb) 
			throws CommonPermissionException, CommonCreateException, CommonFinderException, 
			CommonRemoveException, CommonStaleVersionException, CommonValidationException, 
			NuclosBusinessException {
		
		final UID entity = instance.getDalEntity();
		if (addToDb) {
			if (instance.isFlagNew() || instance.getId() == null) {
				instance = entityObjectRemote.insert(instance);
			} else if (!instance.isFlagRemoved() && !instance.isFlagUnchanged()) {
				instance = entityObjectRemote.update(instance, false);
			}
		}
		final Cache<Object,EntityObjectVO<PK>> cache = caches.get(entity);
		if (cache != null && !cache.equals(nullCache)) {
			cache.put(instance.getPrimaryKey(), instance);
		}
	}
	
}
