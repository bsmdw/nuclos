package org.nuclos.common.cache;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

public class TestAllEntityObjectsCache<PK> implements IAllEntityObjectsCache<PK> {

	private final Cache<Object, EntityObjectVO<PK>> nullCache = (Cache<Object, EntityObjectVO<PK>>) AllEntityObjectsCache.NULL_CACHE;

	private static TestAllEntityObjectsCache INSTANCE;

	private final ConcurrentMap<UID, Cache<Object, EntityObjectVO<PK>>> caches =
			new ConcurrentHashMap<UID, Cache<Object, EntityObjectVO<PK>>>();

	TestAllEntityObjectsCache() {
		INSTANCE = this;
	}

	public static TestAllEntityObjectsCache getInstance() {
		if (INSTANCE == null) {
			throw new IllegalStateException("too early");
		}
		return INSTANCE;
	}

	public void put(EntityObjectVO<PK> en) {
		final UID entity = en.getDalEntity();
		Cache<Object, EntityObjectVO<PK>> cache = caches.get(entity);
		if (cache == null || cache.equals(nullCache)) {
			/*
			refill(entity);
			cache = caches.get(entity);
			 */
			cache = CacheBuilder.newBuilder().build();
			caches.put(entity, cache);
		}
		cache.put(en.getPrimaryKey(), en);
	}

	public void putAll(Iterable<EntityObjectVO<PK>> entities) {
		for (EntityObjectVO<PK> en : entities) {
			put(en);
		}
	}

	public void putAll(EntityObjectVO<PK>... entities) {
		for (EntityObjectVO<PK> en : entities) {
			put(en);
		}
	}

	@Override
	public EntityObjectVO<PK> get(UID entity, PK id) {
		Cache<Object, EntityObjectVO<PK>> cache = caches.get(entity);
		if (cache == null || cache.equals(nullCache)) {
			refill(entity);
			cache = caches.get(entity);
		}
		return cache.getIfPresent(id);
	}

	@Override
	public void register(UID entity, boolean fillImmediate) {
		// do nothing
	}

	@Override
	public void register(UID entity, CollectableSearchExpression expr, boolean fillImmediate) {
		// do nothing
	}

	@Override
	public void refill(UID entity) {
		// do nothing
	}

	@Override
	public void invalidate(UID entity) {
		// do nothing
	}

	@Override
	public Collection<EntityObjectVO<PK>> getAll(UID entity) {
		Cache<Object, EntityObjectVO<PK>> cache = caches.get(entity);
		if (cache == null || cache.equals(nullCache)) {
			refill(entity);
			cache = caches.get(entity);
		}
		return cache.asMap().values();
	}

	@Override
	public void delete(UID entity, PK pk, boolean deleteInDbIsDummy) throws CommonPermissionException {
		final Cache<Object,EntityObjectVO<PK>> cache = caches.get(entity);
		if (cache != null && !cache.equals(nullCache)) {
			cache.invalidate(pk);
		}
	}
	
	@Override
	public void add(EntityObjectVO<PK> instance, boolean addToDbIsDummy) 
			throws CommonPermissionException, CommonCreateException, CommonFinderException, 
			CommonRemoveException, CommonStaleVersionException, CommonValidationException, 
			NuclosBusinessException {
		
		final UID entity = instance.getDalEntity();
		final Cache<Object,EntityObjectVO<PK>> cache = caches.get(entity);
		if (cache != null && !cache.equals(nullCache)) {
			cache.put(instance.getPrimaryKey(), instance);
		}
	}
	
}
