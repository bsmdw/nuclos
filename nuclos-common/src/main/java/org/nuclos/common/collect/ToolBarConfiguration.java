//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.collect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;



public abstract class ToolBarConfiguration implements Serializable {
	
	private static final long serialVersionUID = 2165385133967323098L;

	public static final String MAIN_TOOL_BAR_KEY = "nuclos_maintoolbar";
	
	/**
	 * Context for configurable items
	 */
	public static enum Context {
		/**
		 * For system parameter and workspace parameter
		 */
		UNSPECIFIED,
		
		/**
		 * Entity with statemodel
		 */
		ENTITY_MODULE,
		
		/**
		 * Entity without statemodel
		 */
		ENTITY_MASTERDATA;
		
	}
	
	/*
	 * More elements necessary? 
	 * Use ';' delimiter in String result.
	 */
	private final ToolBarItemGroup mainToolBar;
	
	protected ToolBarConfiguration() {
		this(new ToolBarItemGroup(MAIN_TOOL_BAR_KEY));
	}
	
	protected ToolBarConfiguration(String sValue) {
		this((ToolBarItemGroup) ToolBarItemParser.parse(sValue));
	}
	
	protected ToolBarConfiguration(ToolBarItemGroup mainToolBar) {
		this.mainToolBar = mainToolBar;
	}
	
	public ToolBarItemGroup getMainToolBar() {
		return mainToolBar;
	}

	/**
	 * For editor only. 
	 * 
	 * @return all configurable items
	 */
	public List<ToolBarItem> getConfigurableItems(Context context) {
		return new ArrayList<ToolBarItem>();
	}
	
	public abstract ToolBarConfiguration getDefaultConfiguration();
	
	@Override
	public String toString() {
		return ToolBarItemParser.toString(mainToolBar);
	}
		
}
