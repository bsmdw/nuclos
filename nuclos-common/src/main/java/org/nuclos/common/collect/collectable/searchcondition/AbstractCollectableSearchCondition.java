//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.collect.collectable.searchcondition;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.simple.JSONArray;
import org.json.simple.JSONValue;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.entityobject.CollectableEOEntityField;

/**
 * Abstract implementation of a collectable search condition. Implements Transferable.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version	01.00.00
 */
public abstract class AbstractCollectableSearchCondition implements CollectableSearchCondition {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3138432067035224020L;

	public static final DataFlavor DATAFLAVOR_SEARCHCONDITION = new DataFlavor(CollectableSearchCondition.class,
			"Suchbedingung");
	
	// 

	private String conditionName;
	
	protected AbstractCollectableSearchCondition() {
	}

	@Override
	public final Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException {
		final Object result;

		if (flavor.equals(DATAFLAVOR_SEARCHCONDITION)) {
			result = this;
		}
		else if (flavor.equals(DataFlavor.stringFlavor)) {
			result = this.toString();
		}
		else {
			throw new UnsupportedFlavorException(flavor);
		}

		return result;
	}

	@Override
	public final DataFlavor[] getTransferDataFlavors() {
		return new DataFlavor[]{DATAFLAVOR_SEARCHCONDITION, DataFlavor.stringFlavor};
	}

	@Override
	public final boolean isDataFlavorSupported(DataFlavor flavor) {
		final DataFlavor[] flavors = this.getTransferDataFlavors();
		for (int i = 0; i < flavors.length; i++) {
			if (flavors[i].equals(flavor)) {
				return true;
			}
		}
		return false;
	}

	@Override
    public String getConditionName() {
    	return conditionName;
    }

	@Override
    public void setConditionName(String conditionName) {
    	this.conditionName = conditionName;
    }
	
	protected static void appendConditionNameIfAny(AbstractCollectableSearchCondition c, StringBuilder sb) {
		String conditionName = c.getConditionName();
		if (conditionName != null) {
			sb.append(':');
			sb.append(conditionName);
		}
	}
	
	@Override
	public String toString() {
		return getClass().getName() + ":" + hashCode() + ":" + conditionName;
	}
	
	public static CollectableSearchCondition createCondition(String representation, Map<FieldMeta<?>, String> mpFields) {
		if (representation == null || representation.isEmpty()) {
			return null;
		}
		
		String[] tokens = representation.split(":");
		if (tokens.length < 2) {
			return null;
		}
		
		String name = tokens[0];
		String operater = tokens[1];
		
		if (CompositeCollectableSearchCondition.NAME.equals(name)) {
			int brack1 = representation.indexOf('[');
			int brack2 = representation.lastIndexOf(']');
			
			if (brack1 < 0 || brack1 >= brack2) {
				return null;
			}
			
			String list = representation.substring(brack1 + 1, brack2);
			if (list.isEmpty()) {
				return null;
			}
			
			LogicalOperator lo = LogicalOperator.valueOf(operater);
			
			List<String> conditions = new ArrayList<String>();
			int openBrackets = 0;
			int startIndex = 0;

			for (int i = 0; i < list.length(); i++) {
				char c = list.charAt(i);
				if (c == ',') {
					if (openBrackets == 0) {
						conditions.add(list.substring(startIndex, i));
						startIndex = i + 1;
					}
				} else if (c == '[') {
					openBrackets++;
				} else if (c == ']') {
					openBrackets--;
				}
			}
			
			if (startIndex < list.length() - 1) {
				conditions.add(list.substring(startIndex, list.length()));
			}
						
			Collection<CollectableSearchCondition> collC = new ArrayList<CollectableSearchCondition>();
			
			for (String condition : conditions) {
				String repr = condition.trim();
				CollectableSearchCondition c = createCondition(repr, mpFields);
				if (c != null) {
					collC.add(c);
				}
			}
			
			if (!collC.isEmpty()) {
				return new CompositeCollectableSearchCondition(lo, collC);				
			}
			
		} else if (CollectableComparison.NAME.equals(name) || CollectableLikeCondition.NAME.equals(name) || CollectableInCondition.NAME.equals(name) 
				|| "GTE".equals(name) || "GT".equals(name) || "LTE".equals(name) || "LT".equals(name)) {
			if (tokens.length < 4) {
				return null;
			}
			
			String field = tokens[2];		
			Object operand = tokens[3];
			
			FieldMeta<?> efMeta = null;
			for(Entry<FieldMeta<?>, String> entry : mpFields.entrySet()) {
				// TODO Refactor RestSearchCondition: 
				// 				 FieldName != Webclient.getShortFieldName(..),
				//				 See org.nuclos.server.nbo.NuclosBusinessObjectBuilder.getFieldNameForFqn(FieldMeta<?>)
				//			>>>> Use FQNs!
				if (entry.getValue().endsWith("_"+field)) {
					efMeta = entry.getKey();
				}
			}
			if (efMeta == null) {
				return null;
			}
			
			CollectableEntityField cef = new CollectableEOEntityField(efMeta);
			
			if (CollectableComparison.NAME.equals(name)) {
				ComparisonOperator co;
				// more api like conditions...
				if ("GTE".equals(operater)) {
					co = ComparisonOperator.GREATER_OR_EQUAL;
				} else if ("GT".equals(operater)) {
					co = ComparisonOperator.GREATER;
				} else if ("LTE".equals(operater)) {
					co = ComparisonOperator.LESS_OR_EQUAL;
				} else if ("LT".equals(operater)) {
					co = ComparisonOperator.LESS;
				} else {
					co = ComparisonOperator.valueOf(operater);
				}
				
				if (efMeta.getJavaClass().equals(Boolean.class)) {
					operand = Boolean.parseBoolean(operand.toString());
				} else if (Date.class.isAssignableFrom(efMeta.getJavaClass())) {
					try {
						operand = new SimpleDateFormat("yyyy-MM-dd").parse(operand.toString());
					} catch (ParseException e) {
						throw new IllegalArgumentException("Date value '" + operand + "' is illegal");
					}
				} else if (efMeta.getJavaClass().equals(BigDecimal.class) || efMeta.getJavaClass().equals(Double.class)) {
					try {
					operand = new BigDecimal(operand.toString());
					} catch (NumberFormatException e) {
						throw new IllegalArgumentException("BigDecimal value '" + operand + "' is illegal");
					}
				} else if (efMeta.getJavaClass().equals(Integer.class)) {
					try {
					operand = new Integer(operand.toString());
					} catch (NumberFormatException e) {
						throw new IllegalArgumentException("Integer value '" + operand + "' is illegal");
					}
				} else if (efMeta.getJavaClass().equals(Long.class)) {
					try {
					operand = new Long(operand.toString());
					} catch (NumberFormatException e) {
						throw new IllegalArgumentException("Long value '" + operand + "' is illegal");
					}
				}
				
				CollectableField comparand = new CollectableValueField(operand);
				return new CollectableComparison(cef, co, comparand);
				
			} else if (CollectableLikeCondition.NAME.equals(name)) {
				return new CollectableLikeCondition(cef, operand.toString());
				
			} else if (CollectableInCondition.NAME.equals(name)) {
				Object json = JSONValue.parse((String)operand);
				if (json instanceof JSONArray) {
					List<Object> inComparands = new ArrayList<Object>();
					JSONArray array = (JSONArray) json;
					
					for (int i = 0; i < array.size(); i++) {
						Object obj = array.get(i);
						inComparands.add(obj);
					}
					
					//NUCLOS-4111 CollectableInCondition works with the String Name of a Reference Field
					//TODO: It would be better if it worked with the IDs
					return new CollectableInCondition<Object>(cef, inComparands);
				}
								
			}
			
		} 
		
		return null;
		
	}	
	
}  // class AbstractCollectableSearchCondition
