//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.collect;

import java.util.List;


public class NuclosDetailsSubformToolBarConfiguration extends ToolBarConfiguration {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6132803701603793761L;
	public static final NuclosDetailsSubformToolBarConfiguration DEFAULT = new NuclosDetailsSubformToolBarConfiguration(
			new ToolBarItemGroup(MAIN_TOOL_BAR_KEY)
				.addItem(NuclosToolBarItems.NEW)
				.addItem(NuclosToolBarItems.CLONE)
				.addItem(NuclosToolBarItems.DELETE)
				.addItem(NuclosToolBarItems.QUICKSELECTION)
				.addItem(NuclosToolBarItems.FILTER)
				.addItem(NuclosToolBarItems.TRANSFER)
				.addItem(NuclosToolBarItems.DOCUMENT_IMPORT)
				.addItem(NuclosToolBarItems.PRINT_REPORT)
				.addItem(NuclosToolBarItems.AUTONUMBER_PUSH_UP)
				.addItem(NuclosToolBarItems.AUTONUMBER_PUSH_DOWN)
				.addItem(NuclosToolBarItems.LIST_PROFILE)
				.addItem(NuclosToolBarItems.SUBFORM_DETAILS_VIEW)
				//.addItem(NuclosToolBarItems.SUBFORM_CHANGE_STATE)
			);

	@Override
	public ToolBarConfiguration getDefaultConfiguration() {
		return DEFAULT;
	}

	@Override
	public List<ToolBarItem> getConfigurableItems(Context context) {
		List<ToolBarItem> result = super.getConfigurableItems(context);
		result.add(NuclosToolBarItems.NEW);
		result.add(NuclosToolBarItems.CLONE);
		result.add(NuclosToolBarItems.DELETE);
		result.add(NuclosToolBarItems.QUICKSELECTION);
		result.add(NuclosToolBarItems.FILTER);
		result.add(NuclosToolBarItems.TRANSFER);
		result.add(NuclosToolBarItems.DOCUMENT_IMPORT);
		result.add(NuclosToolBarItems.PRINT_REPORT);
		result.add(NuclosToolBarItems.AUTONUMBER_PUSH_UP);
		result.add(NuclosToolBarItems.AUTONUMBER_PUSH_DOWN);
		result.add(NuclosToolBarItems.LIST_PROFILE);
		result.add(NuclosToolBarItems.SUBFORM_DETAILS_VIEW);
		result.add(NuclosToolBarItems.SUBFORM_DETAILS_ZOOM);
		//result.add(NuclosToolBarItems.SUBFORM_CHANGE_STATE);
		return result;
	}
	
	public NuclosDetailsSubformToolBarConfiguration(ToolBarItemGroup mainToolBar) {
		super(mainToolBar);
	}

	public NuclosDetailsSubformToolBarConfiguration(String value) {
		super(value);
	}
	
	public NuclosDetailsSubformToolBarConfiguration() {
	}

}
