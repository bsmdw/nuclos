//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.collect;

import java.util.List;


public class NuclosSearchToolBarConfiguration extends ToolBarConfiguration {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6354236232833950891L;
	public static final NuclosSearchToolBarConfiguration DEFAULT = new NuclosSearchToolBarConfiguration(
			new ToolBarItemGroup(MAIN_TOOL_BAR_KEY)
				.addItem(NuclosToolBarItems.COLLECT_INDICATOR)
				.addItem(NuclosToolBarItems.SEARCH)
				.addItem(NuclosToolBarItems.CLEAR_SEARCH_FORM)
				.addItem(NuclosToolBarItems.NEW)
				.addItem(NuclosToolBarItems.NEW_FROM_SEARCH)
				.addItem(NuclosToolBarItems.newExtras()
						.addItem(NuclosToolBarItems.SEARCH_EDITOR)
						.addItem(NuclosToolBarItems.MENU_SEPARATOR)
						.addItem(NuclosToolBarItems.SEARCH_DELETED)
						.addItem(NuclosToolBarItems.MENU_SEPARATOR)
						.addItem(NuclosToolBarItems.SAVE_FILTER)
						.addItem(NuclosToolBarItems.DELETE_FILTER))
				.addItem(NuclosToolBarItems.LOAD_FILTER)
				.addItem(NuclosToolBarItems.SEARCH_STATE)
				.addItem(NuclosToolBarItems.SEARCH_OWNER)
			);

	@Override
	public ToolBarConfiguration getDefaultConfiguration() {
		return DEFAULT;
	}

	@Override
	public List<ToolBarItem> getConfigurableItems(Context context) {
		List<ToolBarItem> result = super.getConfigurableItems(context);
		result.add(NuclosToolBarItems.COLLECT_INDICATOR);
		result.add(NuclosToolBarItems.SEARCH);
		result.add(NuclosToolBarItems.EXTRAS);
		result.add(NuclosToolBarItems.MENU_SEPARATOR);
		result.add(NuclosToolBarItems.CLEAR_SEARCH_FORM);
		result.add(NuclosToolBarItems.NEW);
		result.add(NuclosToolBarItems.NEW_FROM_SEARCH);
		result.add(NuclosToolBarItems.SEARCH_DELETED);
		result.add(NuclosToolBarItems.SEARCH_EDITOR);
		result.add(NuclosToolBarItems.LOAD_FILTER);
		result.add(NuclosToolBarItems.SEARCH_STATE);
		result.add(NuclosToolBarItems.SAVE_FILTER);
		result.add(NuclosToolBarItems.DELETE_FILTER);
		result.add(NuclosToolBarItems.SEARCH_OWNER);
		if (context == Context.UNSPECIFIED || context == Context.ENTITY_MODULE) {

		}
		if (context == Context.UNSPECIFIED || context == Context.ENTITY_MASTERDATA) {
			
		}
		return result;
	}
	
	public NuclosSearchToolBarConfiguration(ToolBarItemGroup mainToolBar) {
		super(mainToolBar);
	}

	public NuclosSearchToolBarConfiguration(String value) {
		super(value);
	}
	
	public NuclosSearchToolBarConfiguration() {
	}

}
