//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.collect;

import java.util.List;


public class NuclosResultToolBarConfiguration extends ToolBarConfiguration {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8253554767606676422L;
	public static final NuclosResultToolBarConfiguration DEFAULT = new NuclosResultToolBarConfiguration(
			new ToolBarItemGroup(MAIN_TOOL_BAR_KEY)
				.addItem(NuclosToolBarItems.COLLECT_INDICATOR)
				.addItem(NuclosToolBarItems.MULTI_EDIT)
				.addItem(NuclosToolBarItems.REFRESH_LIST)
				.addItem(NuclosToolBarItems.NEW)
				.addItem(NuclosToolBarItems.MULTI_DELETE)
				.addItem(NuclosToolBarItems.newExtras()
						.addItem(NuclosToolBarItems.BOOKMARK)
						.addItem(NuclosToolBarItems.CLONE)
						.addItem(NuclosToolBarItems.CONFIG_COLUMNS)
						.addItem(NuclosToolBarItems.MENU_SEPARATOR)
						.addItem(NuclosToolBarItems.SEARCH_DELETED)
						.addItem(NuclosToolBarItems.MULTI_SHOW_IN_EXPLORER)
						.addItem(NuclosToolBarItems.MULTI_DELETE_REAL))
				.addItem(NuclosToolBarItems.INFO)
				.addItem(NuclosToolBarItems.TEXT_SEARCH)
				.addItem(NuclosToolBarItems.LIST_PROFILE)
				.addItem(NuclosToolBarItems.SET_FILTER)
				.addItem(NuclosToolBarItems.FILTER)
				.addItem(NuclosToolBarItems.HELP)
			);
	
	@Override
	public ToolBarConfiguration getDefaultConfiguration() {
		return DEFAULT;
	}

	@Override
	public List<ToolBarItem> getConfigurableItems(Context context) {
		List<ToolBarItem> result = super.getConfigurableItems(context);
		result.add(NuclosToolBarItems.COLLECT_INDICATOR);
		result.add(NuclosToolBarItems.MULTI_EDIT);
		result.add(NuclosToolBarItems.EXTRAS);
		result.add(NuclosToolBarItems.MENU_SEPARATOR);
		result.add(NuclosToolBarItems.REFRESH_LIST);
		result.add(NuclosToolBarItems.NEW);
		result.add(NuclosToolBarItems.MULTI_DELETE);
		result.add(NuclosToolBarItems.CLONE);
		result.add(NuclosToolBarItems.BOOKMARK);
		result.add(NuclosToolBarItems.CONFIG_COLUMNS);
		result.add(NuclosToolBarItems.MULTI_SHOW_IN_EXPLORER);
		result.add(NuclosToolBarItems.INFO);
		result.add(NuclosToolBarItems.TEXT_SEARCH);
		result.add(NuclosToolBarItems.LIST_PROFILE);
		result.add(NuclosToolBarItems.RESET_FILTER);
		result.add(NuclosToolBarItems.SET_FILTER);
		result.add(NuclosToolBarItems.FILTER);

		if (context == Context.UNSPECIFIED || context == Context.ENTITY_MODULE) {
			result.add(NuclosToolBarItems.MULTI_DELETE_REAL);
			result.add(NuclosToolBarItems.SEARCH_DELETED);
		}
		if (context == Context.UNSPECIFIED || context == Context.ENTITY_MASTERDATA) {
			
		}
		return result;
	}
	
	public NuclosResultToolBarConfiguration(ToolBarItemGroup mainToolBar) {
		super(mainToolBar);
	}

	public NuclosResultToolBarConfiguration(String value) {
		super(value);
	}

	public NuclosResultToolBarConfiguration() {
	}
	
}
