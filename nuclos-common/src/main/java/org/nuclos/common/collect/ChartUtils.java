package org.nuclos.common.collect;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.nuclos.common.E;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.valuelistprovider.IDataSource;

public final class ChartUtils {
	private ChartUtils() {
	}
	
	public static List<DatasourceParameterVO> getParameterListForDataSource(UID entity, IDataSource dataSource) {
		DatasourceVO datasourceVO  = null;
		try {
			datasourceVO = dataSource.getChart(
					new UID(entity.getString().substring(E.CHART.getUID().getString().length() + 1)));
			if (datasourceVO != null) {
				final List<DatasourceParameterVO> lstParams = dataSource.getParametersFromXML(datasourceVO.getSource());
				for (Iterator<DatasourceParameterVO> iterator = lstParams.iterator(); iterator.hasNext();) {
					if (iterator.next().getParameter().equals("genericObject")) {
						iterator.remove();
					}
				}
				return lstParams;
			}
			return new ArrayList<DatasourceParameterVO>();
		} catch (Exception e) {
			throw new NuclosFatalException("init failed for datasource " + datasourceVO.getName(), e);
		}
	}
}