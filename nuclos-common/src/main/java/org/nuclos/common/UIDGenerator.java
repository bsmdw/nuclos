//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class UIDGenerator {

	public static void main(String[] args) {
		Collection<EntityMeta<?>> entities = E.getAllEntities();
		JPanel jpn = new JPanel(new GridLayout(3, 2));
		jpn.add(new JLabel("New entity UID "));
		String newEntityUid = null;
		while (newEntityUid == null) {
			newEntityUid = UID.generateString(4);
			if (UID.UID_NULL.getString().equals(newEntityUid)) {
				newEntityUid = null;
				continue;
			}
			for (EntityMeta<?> em : entities) {
				if (newEntityUid.equals(em.getUID().getString())) {
					newEntityUid = null;
					break;
				}
			}
		}
		JTextField tfNewEntity = new JTextField(10);
		tfNewEntity.setEditable(false);
		tfNewEntity.setText(newEntityUid);
		jpn.add(tfNewEntity);
		jpn.add(new JLabel("New field UID for entity "));
		List<EntityMeta<?>> entitiesSorted = new ArrayList<EntityMeta<?>>(entities);
		Collections.sort(entitiesSorted, new Comparator<EntityMeta<?>>() {
			@Override
			public int compare(EntityMeta<?> o1, EntityMeta<?> o2) {
				return RigidUtils.compare(o1.getEntityName(), o2.getEntityName());
			}
		});
		final JComboBox cbbxNewFieldFor = new JComboBox(entitiesSorted.toArray(new EntityMeta<?>[]{}));
		final JTextField tfNewField = new JTextField(10);
		tfNewField.setEditable(false);
		final String validChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		final Comparator<String> uidComparator = new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				 int len1 = o1.length();
			     int len2 = o2.length();
			     if (len1 != len2) {
			    	 return len1 - len2; 
			     }
			     char v1[] = o1.toCharArray();
			     char v2[] = o2.toCharArray();
			     int k = 0;
			     while (k < len1) {
			         char c1 = v1[k];
			         char c2 = v2[k];
			         if (c1 != c2) {
			        	 return validChars.indexOf(c1) - validChars.indexOf(c2);
			         }
			         k++;
			     }
			     return 0;
			}
		};
		cbbxNewFieldFor.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				EntityMeta<?> selectedEntity = (EntityMeta<?>) cbbxNewFieldFor.getSelectedItem();
				String selectedEntityUid = selectedEntity.getUID().getString();
				String maxCount = null;
				for (FieldMeta<?> fm : selectedEntity.getFields()) {
					String count = fm.getUID().getString().replaceFirst(selectedEntityUid, "");
					if (maxCount == null) {
						maxCount = count;
					} else {
						if (uidComparator.compare(count, maxCount) > 0) {
							maxCount = count;
						}
					}
				}
				final String newCount;
				if (maxCount == null) {
					newCount = validChars.substring(0, 1);
				} else {
					char last = maxCount.charAt(maxCount.length()-1);
					int index = validChars.indexOf(last);
					if (index + 1 >= validChars.length()) {
						newCount = maxCount.substring(0, maxCount.length()-1) + validChars.substring(0, 1) + validChars.substring(0, 1); 
					} else {
						newCount = maxCount.substring(0, maxCount.length()-1) + validChars.charAt(index + 1);
					}
				}
				tfNewField.setText(selectedEntityUid + newCount);
			}
		});
		jpn.add(cbbxNewFieldFor);
		jpn.add(new JLabel("New field UID "));
		jpn.add(tfNewField);
		JOptionPane.showMessageDialog(null, jpn, "UIDGenerator", JOptionPane.QUESTION_MESSAGE);
	}
	
}
