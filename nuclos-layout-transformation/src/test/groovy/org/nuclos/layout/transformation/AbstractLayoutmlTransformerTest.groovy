package org.nuclos.layout.transformation

import org.apache.commons.lang.NotImplementedException
import org.nuclos.cache.IFqnCache
import org.nuclos.common.*
import org.nuclos.common.dal.vo.EntityObjectVO
import org.nuclos.common2.jackson.MinimalTypeNameMixIn
import org.nuclos.schema.layout.rule.RuleAction
import org.nuclos.schema.layout.web.WebComponent

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.core.Version
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.module.SimpleModule

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
abstract class AbstractLayoutmlTransformerTest {
	ObjectMapper mapper
	IMetaProvider metaProvider

	protected ObjectMapper getMapper() {
		if (!mapper) {
			mapper = new ObjectMapper()
			mapper.serializationInclusion = JsonInclude.Include.NON_NULL
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
			mapper.configure(SerializationFeature.INDENT_OUTPUT, true)

			mapper.addMixIn(WebComponent.class, MinimalTypeNameMixIn.class)
			mapper.addMixIn(RuleAction.class, MinimalTypeNameMixIn.class)

			SimpleModule testModule = new SimpleModule("TestModule", new Version(1, 0, 0, null));
			testModule.addSerializer(new UidToFqnJacksonSerializer() {
				@Override
				IFqnCache getFqnCache() {
					return AbstractLayoutmlTransformerTest.this.fqnCache
				}
			})
			mapper.registerModule(testModule);
		}
		return mapper
	}

	/**
	 * Deserializes an entity meta JSON-file and fills the given maps.
	 *
	 * @param mapper
	 * @param jsonResourceName
	 * @param entityMap
	 * @param fieldMap
	 */
	private void addEntity(
			final ObjectMapper mapper,
			final String jsonResourceName,
			Map<UID, EntityMeta<?>> entityMap,
			Map<UID, FieldMeta<?>> fieldMap
	) {
		InputStream stream = AbstractLayoutmlTransformerTest.class.getResourceAsStream(jsonResourceName)
		NucletEntityMeta meta = mapper.readValue(stream, NucletEntityMeta.class)

		entityMap.put(meta.getUID(), meta)
		meta.fields.each {
			fieldMap.put(it.getUID(), it)
		}
	}

	/**
	 * Returns a FQN cache with some test data.
	 *
	 * TODO: This could be a serialized instance of MasterDataRestFqnCache.
	 *
	 * @return
	 */
	protected IFqnCache getFqnCache() {
		return new IFqnCache() {
			@Override
			String getFullQualifiedNucletName(final UID nucletUID) {
				"fqn_for_$nucletUID"
			}

			@Override
			UID translateFqn(final EntityMeta<?> eMeta, final String fqn) {
				new UID("uid_for_${eMeta}_$fqn")
			}

			@Override
			String translateUid(final UID uid) {
				"fqn_for_$uid"
			}

			@Override
			String translateUid(final EntityMeta<?> eMeta, final UID uid) {
				"fqn_for_${eMeta}_$uid"
			}

			@Override
			void invalidateCacheForEntity(final UID entityUID) {
			}
		}
	}

	/**
	 * Returns a meta provider with some test meta data.
	 *
	 * @return
	 */
	protected IMetaProvider getMetaProvider() {
		if (!metaProvider) {
			final Map<UID, EntityMeta<?>> entityMap = new HashMap<>()
			final Map<UID, FieldMeta<?>> fieldMap = new HashMap<>()

			ObjectMapper mapper = new ObjectMapper()
			mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL)

			addEntity(mapper, 'meta/example_rest_Order.json', entityMap, fieldMap)
			addEntity(mapper, 'meta/nuclet_test_other_TestLayoutComponents.json', entityMap, fieldMap)

			metaProvider = new IMetaProvider() {
				@Override
				Map<UID, LafParameterMap> getAllLafParameters() {
					throw new NotImplementedException()
				}

				@Override
				LafParameterMap getLafParameters(final UID entityUID) {
					throw new NotImplementedException()
				}

				@Override
				boolean checkEntity(final UID entityUID) {
					return entityMap.containsKey(entityUID);
				}

				@Override
				List<UID> getEntities(final UID nucletUID) {
					return entityMap.keySet().toList()
				}

				@Override
				Collection<FieldMeta<?>> getAllReferencingFields(final UID masterUID) {
					throw new NotImplementedException()
				}

				@Override
				FieldMeta<?> getCalcAttributeCustomization(final UID fieldUID, final String paramValues) {
					throw new NotImplementedException()
				}

				@Override
				Collection<EntityMeta<?>> getAllLanguageEntities() {
					throw new NotImplementedException()
				}

				@Override
				List<EntityObjectVO<UID>> getNuclets() {
					throw new NotImplementedException()
				}

				@Override
				Set<UID> getImplementingEntities(final UID genericEntityUID) {
					throw new NotImplementedException()
				}

				@Override
				Collection<EntityMeta<?>> getAllEntities() {
					return entityMap.values()
				}

				@Override
				def <PK> EntityMeta<PK> getEntity(final UID entityUID) {
					return entityMap.get(entityUID)
				}

				@Override
				Map<UID, FieldMeta<?>> getAllEntityFieldsByEntity(final UID entityUID) {
					throw new NotImplementedException()
				}

				@Override
				boolean checkEntityField(final UID fieldUID) {
					return fieldMap.containsKey(fieldUID)
				}

				@Override
				FieldMeta<?> getEntityField(final UID fieldUID) {
					return fieldMap.get(fieldUID)
				}

				@Override
				EntityMeta<?> getByTablename(final String sTableName) {
					throw new NotImplementedException()
				}

				@Override
				boolean isNuclosEntity(final UID entityUID) {
					throw new NotImplementedException()
				}
			}
		}

		return metaProvider
	}
}