package org.nuclos.layout.transformation.weblayout.calculated

import org.junit.Test
import org.nuclos.common2.JaxbMarshalUnmarshalUtil
import org.nuclos.layout.transformation.weblayout.LayoutmlToWeblayoutTransformerTest
import org.nuclos.layout.transformation.weblayout.fixed.LayoutmlToWeblayoutFixedTransformer
import org.nuclos.schema.layout.layoutml.Layoutml
import org.nuclos.schema.layout.web.WebButtonAddon
import org.nuclos.schema.layout.web.WebButtonChangeState
import org.nuclos.schema.layout.web.WebButtonDummy
import org.nuclos.schema.layout.web.WebButtonExecuteRule
import org.nuclos.schema.layout.web.WebButtonGenerateObject
import org.nuclos.schema.layout.web.WebButtonHyperlink
import org.nuclos.schema.layout.web.WebCheckbox
import org.nuclos.schema.layout.web.WebColorchooser
import org.nuclos.schema.layout.web.WebCombobox
import org.nuclos.schema.layout.web.WebComponent
import org.nuclos.schema.layout.web.WebContainer
import org.nuclos.schema.layout.web.WebDatechooser
import org.nuclos.schema.layout.web.WebFile
import org.nuclos.schema.layout.web.WebInputComponent
import org.nuclos.schema.layout.web.WebLabel
import org.nuclos.schema.layout.web.WebLabelStatic
import org.nuclos.schema.layout.web.WebLayout
import org.nuclos.schema.layout.web.WebListofvalues
import org.nuclos.schema.layout.web.WebMatrix
import org.nuclos.schema.layout.web.WebPanel
import org.nuclos.schema.layout.web.WebSubform
import org.nuclos.schema.layout.web.WebSubformColumn
import org.nuclos.schema.layout.web.WebTabcontainer
import org.nuclos.schema.layout.web.WebTextfield

import com.fasterxml.jackson.databind.ObjectMapper

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class LayoutmlToWeblayoutCalcTransformerTest extends LayoutmlToWeblayoutTransformerTest {

	@Test
	void testLayoutml2Weblayout() {
		InputStream xml = this.getClass().getResourceAsStream('../../layout/nuclet_test_other_TestLayoutComponents.xml')

		Layoutml layoutml = JaxbMarshalUnmarshalUtil.unmarshal(xml, Layoutml.class)
		def transformer = new LayoutmlToWeblayoutCalcTransformer(getMetaProvider(), layoutml)
		WebLayout result = transformer.transform()

		assert !result.calculated.cells.empty

		List<WebComponent> components = findAllComponents(result.calculated)
		assert components.find { it instanceof WebContainer && !it.opaque }
		assert components.find {
			it instanceof WebPanel && it.title && it.opaque && !it.backgroundColor && !it.borderWidth && !it.borderColor
		}
		assert components.find {
			it instanceof WebPanel && it.title && it.opaque && it.backgroundColor && it.borderWidth == '3px' && it.borderColor == '#FF0000'
		}
		assert components.find { it instanceof WebTextfield }
		assert components.find { it instanceof WebTextfield && it.name.contains('encryptedtext') }
		assert components.find { it instanceof WebDatechooser }
		assert components.find { it instanceof WebListofvalues }
		assert components.find {
			it instanceof WebCombobox && it.name == 'text' && it.valuelistProvider && it.valuelistProvider.parameter.size() > 0
		}
		assert components.find { it instanceof WebCombobox && !it.valuelistProvider }
		assert components.find { it instanceof WebTabcontainer }
		assert components.find { it instanceof WebSubform }
		assert components.find { it instanceof WebFile }
		assert components.find { it instanceof WebButtonDummy && it.label && it.disableDuringEdit && it.icon }
		assert components.find { it instanceof WebButtonExecuteRule && it.rule && it.label && it.icon }
		assert components.find { it instanceof WebButtonGenerateObject && it.objectGenerator && it.label && it.icon }
		assert components.find { it instanceof WebButtonChangeState /*&& it.targetState*/ && it.label && it.icon }
		assert components.find { it instanceof WebButtonHyperlink && it.hyperlinkField && it.label && it.icon }
		assert components.find { it instanceof WebCheckbox }

		// There should be static labels with and without font size
		assert components.find { it instanceof WebLabelStatic && it.text && it.fontSize }
		assert components.find { it instanceof WebLabelStatic && it.text && !it.fontSize }

		// There should be labels with and without font size
		assert components.find { it instanceof WebLabel && it.fontSize }
		assert components.find { it instanceof WebLabel && !it.fontSize }

		assert !components.find { it instanceof WebComponent && it.name == 'hiddenText' }
		assert !components.find { it instanceof WebSubformColumn && it.name == 'comment' && !it.visible }
		assert !components.find { it instanceof WebSubformColumn && it.name == 'documentfile' && it.visible }

		// Test for advanced component properties
		assert components.find { it instanceof WebTextfield && !it.advancedProperties.empty }
		assert !components.find { it instanceof WebSubform && it.name == 'comment' && !it.advancedProperties.empty }

		// Test for advanced component properties
		assert components.find { it instanceof WebTextfield && !it.advancedProperties.empty }
		assert !components.find { it instanceof WebSubform && it.name == 'comment' && !it.advancedProperties.empty }

		// Test for the other dummy button without icon
		assert components.find { it instanceof WebButtonDummy && !it.icon }

		assert components.find { it instanceof WebColorchooser && it.enabled }

		components.findAll { it instanceof WebInputComponent }.each {
			assert it.nextFocusField
		}

		assertUniqueIds(components)
	}

	private void assertUniqueIds(List<WebComponent> components) {
		Set<String> ids = new HashSet<>()

		components.each {
			if (it.id) {
				assert !ids.contains(it.id)
				ids.add(it.id)
			}
		}

		components.each {
			if (WebLabel.class.isAssignableFrom(it.class)) {
				WebLabel label = it as WebLabel
				// forId is null for static labels
				if (label.forId) {
					assert ids.contains(label.forId)
				}
			}
		}

		assert !ids.empty

		println "Found ${ids.size()} unique component IDs"
	}

	@Test
	void testWeblayoutFixed2JSON() {
		InputStream xml = this.getClass().getResourceAsStream('../../layout/example_rest_Order.xml')

		Layoutml layoutml = JaxbMarshalUnmarshalUtil.unmarshal(xml, Layoutml.class)
		def transformer = new LayoutmlToWeblayoutCalcTransformer(getMetaProvider(), layoutml)
		WebLayout result = transformer.transform()

		ObjectMapper mapper = getMapper()
		StringWriter stringWriter = new StringWriter()

		mapper.writeValue(stringWriter, result);
		String serializedValue = stringWriter.toString()

		assert serializedValue
	}

	@Test
	void testMatrixLayout() {
		InputStream xml = this.getClass().getResourceAsStream('../../layout/nuclet_test_matrix_Matrix.xml')

		Layoutml layoutml = JaxbMarshalUnmarshalUtil.unmarshal(xml, Layoutml.class)
		def transformer = new LayoutmlToWeblayoutFixedTransformer(getMetaProvider(), layoutml)
		WebLayout result = transformer.transform()

		assert !result.table.rows.empty

		List<WebComponent> components = findAllComponents(result.table)
		assert components.find {
			it instanceof WebMatrix
		}

	}

	@Test
	void layoutWithExtensionComponents() {
		InputStream xml = this.getClass().getResourceAsStream('../../layout/layout_with_extension_components.xml')

		Layoutml layoutml = JaxbMarshalUnmarshalUtil.unmarshal(xml, Layoutml.class)
		def transformer = new LayoutmlToWeblayoutCalcTransformer(getMetaProvider(), layoutml)
		WebLayout result = transformer.transform()

		List<WebComponent> components = findAllComponents(result.calculated)

		// There should be a normal Dummy button
		assert components.find { it instanceof WebButtonDummy && it.enabled && it.label == 'Dummy' }

		// And one WebAddon button for the extension/addon Action
		assert components.find { it instanceof WebButtonAddon && it.enabled && it.label == 'Extension' }
	}

	@Test
	void layoutWithTabindex() {
		InputStream xml = this.getClass().getResourceAsStream('../../layout/layout_with_tabindex.xml')

		Layoutml layoutml = JaxbMarshalUnmarshalUtil.unmarshal(xml, Layoutml.class)
		def transformer = new LayoutmlToWeblayoutCalcTransformer(getMetaProvider(), layoutml)
		WebLayout result = transformer.transform()

		List<WebComponent> components = findAllComponents(result.calculated)

		assert components.size() > 5

		components.findAll { it instanceof WebInputComponent }.each {
			assert it.nextFocusField
		}
	}

}
