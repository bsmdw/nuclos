package org.nuclos.layout.transformation.weblayout

import javax.xml.bind.JAXBElement

import org.nuclos.common.E
import org.nuclos.common.FieldMeta
import org.nuclos.common.IMetaProvider
import org.nuclos.common.UID
import org.nuclos.common2.layoutml.WebMatrix
import org.nuclos.layout.transformation.AbstractLayoutmlTransformer
import org.nuclos.schema.layout.layoutml.Boolean
import org.nuclos.schema.layout.layoutml.Layoutml
import org.nuclos.schema.layout.layoutml.Property
import org.nuclos.schema.layout.layoutml.Subform
import org.nuclos.schema.layout.layoutml.SubformColumn
import org.nuclos.schema.layout.layoutml.TablelayoutConstraints
import org.nuclos.schema.layout.web.ObjectFactory
import org.nuclos.schema.layout.web.WebAdvancedProperty
import org.nuclos.schema.layout.web.WebButton
import org.nuclos.schema.layout.web.WebCell
import org.nuclos.schema.layout.web.WebComponent
import org.nuclos.schema.layout.web.WebFile
import org.nuclos.schema.layout.web.WebGrid
import org.nuclos.schema.layout.web.WebInputComponent
import org.nuclos.schema.layout.web.WebLayout
import org.nuclos.schema.layout.web.WebRow
import org.nuclos.schema.layout.web.WebSubform
import org.nuclos.schema.layout.web.WebSubformColumn
import org.nuclos.schema.layout.web.WebTabcontainer
import org.nuclos.schema.layout.web.WebTable
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import groovy.transform.CompileStatic

/**
 * Transforms an instance of {@link Layoutml} to {@link WebLayout}.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
abstract class LayoutmlToWeblayoutTransformer extends AbstractLayoutmlTransformer<WebLayout> {
	private static final Logger log = LoggerFactory.getLogger(LayoutmlToWeblayoutTransformer.class);

	private final AttributeIdFactory attributeIdFactory = new AttributeIdFactory()

	protected final ObjectFactory factory
	protected WebComponentStructure webComponentStructure

	LayoutmlToWeblayoutTransformer(
			final IMetaProvider metaProvider,
			final Layoutml layoutml
	) {
		super(metaProvider, layoutml)

		factory = new ObjectFactory()
	}

	/**
	 * Performs a specific transformation, to be implemented by subclasses.
	 */
	abstract WebLayout internalTransform()

	/**
	 * Performs the transformation and returns the Weblayout.
	 */
	final WebLayout transform() {
		webComponentStructure = createWebComponentStructure()

		WebLayout webLayout = internalTransform()

		webComponentStructure.componentsAsFlatList
				.findAll { it instanceof WebInputComponent && !it.nextFocusField }
				.each {
					it.nextFocusField = findNextFocusField(it)
				}


		return webLayout
	}

	/**
	 * Finds the next input field to be focused after the given component.
	 * The focus traverses through the components in the order top -> bottom and left -> right.
	 */
	String findNextFocusField(final WebComponent webComponent) {
		WebInputComponent result = findNextFocusInputBelow(webComponent)

		if (!result) {
			result = findNextFocusInputRight(webComponent)
		}

		if (!result) {
			// The focus returns here from the last component.
			result = findFirstFocusField()
		}

		return result?.name
	}

	/**
	 * Finds the next input field below the given component, in the same column.
	 */
	WebInputComponent findNextFocusInputBelow(final WebComponent component) {
		webComponentStructure.componentsAsFlatList
				.findAll { isValidNextFocusField(component, it) && it.column == component.column && it.row > component.row }
				.sort { it.row } [0] as WebInputComponent
	}

	/**
	 * Finds the next input field right of the given component (top component of the respective column).
	 */
	WebInputComponent findNextFocusInputRight(final WebComponent component) {
		webComponentStructure.componentsAsFlatList
				.findAll { isValidNextFocusField(component, it) && it.column > component.column }
				.sort { [it.column, it.row] } [0] as WebInputComponent
	}

	/**
	 * Finds the first input field to be focused in this layout.
	 *
	 * TODO: Respect "initial focus" configuration of the layout here.
	 */
	WebInputComponent findFirstFocusField() {
		webComponentStructure.componentsAsFlatList
				.findAll { isValidNextFocusField(null, it) }
				.sort { [it.column, it.row] } [0] as WebInputComponent
	}

	/**
	 * Checks wether the given targetComponent is a valid "next focus field" for the sourceComponent.
	 */
	private static boolean isValidNextFocusField(WebComponent sourceComponent, WebComponent targetComponent) {
		targetComponent instanceof WebInputComponent &&
				!(targetComponent instanceof WebButton) &&
				!(targetComponent instanceof WebFile) &&
				!(targetComponent instanceof WebSubform) &&
				!(targetComponent instanceof WebMatrix) &&
				!(targetComponent instanceof WebTabcontainer) &&
				targetComponent.name &&
				targetComponent.name != sourceComponent?.name
	}

	/**
	 * Recursively searches for all input nonCloneableFields.
	 */
	private WebComponentStructure createWebComponentStructure() {
		WebComponentStructure structure = new WebComponentStructure()

		layoutml.layout.panel.containerOrLabelOrTextfield.each {
			structure.addAll(getWebComponents(it))
		}

		return structure
	}

	/**
	 * Tries to lookup the field name for the given UID string of the form "uid{...}".
	 *
	 * @param uidString
	 * @return The field name if found, or the given uidString if not found.
	 */
	DataType getFieldType(String uidString) {
		UID uid = UID.parseUID(uidString)
		if (uid) {
			FieldMeta<?> fieldMeta = metaProvider.getEntityField(uid)
			if (fieldMeta && fieldMeta.foreignEntity == E.DOCUMENTFILE.getUID()) {
				return DataType.FILE
			}
		}
		return null
	}

	/**
	 * Recursively visits and transforms all components in the given (JAXB-) element
	 * to the corresponding WebLayout components.
	 */
	List<WebComponent> getWebComponents(Object element) {
		List<WebComponent> result = []

		if (element instanceof JAXBElement) {
			element = (element as JAXBElement).value
		}

		ElementTransformer<?, ? extends WebComponent> transformer = TransformerRegistry.lookup(element.class)
		if (transformer) {
			WebComponent component = transformer.transform(this, element)
			if (component) {
				transferConstraints(element, component)
				transferAdvancedProperties(element, component)

				// TODO: Wrong place. There is a SubformTransformer for such stuff!
				if (element instanceof Subform) {
					((Subform)element).subformColumn.each {
						SubformColumn subformColumn = it
						if (component instanceof WebSubform) {
							def subformColumns = ((WebSubform) component).subformColumns
							WebSubformColumn webSubformColumn = subformColumns.find { it.name == subformColumn.name }
							transferAdvancedProperties(it, webSubformColumn)
						}
					}
				}

				result << component
			}
		} else {
			log.warn('No transformer for element: {}', element)
		}

		return result
	}


	WebComponent transferAdvancedProperties(final Object element, final WebComponent component) {
		List<WebAdvancedProperty> properties = getAdvancedProperties(element, component)

		if (!properties.empty) {
			component.advancedProperties.addAll(properties)
		}

		return component
	}

	WebSubformColumn transferAdvancedProperties(final Object element, final WebSubformColumn component) {
		List<WebAdvancedProperty> properties = getAdvancedProperties(element, component)

		if (!properties.empty) {
			component.advancedProperties.addAll(properties)
		}

		return component
	}

	private List<WebAdvancedProperty> getAdvancedProperties(element, component) {
		List<WebAdvancedProperty> result = new ArrayList<>()

		if (element.hasProperty('property')) {
			def properties = element.getProperties().get('property')
			if (properties instanceof List) {
				List<Property> propertiesList = (List) properties
				if (!propertiesList.empty) {
					result = propertiesList.collect { Property it ->
						WebAdvancedProperty property = factory.createWebAdvancedProperty()
						property.name = it.name
						property.value = it.value
						property
					}
				}
			}
		}

		return result;
	}

	/**
	 * Tries to extract the old tablelayout constraints from the given LayoutML element
	 * and to transfer them to the given WebLayout component.
	 *
	 * @param element
	 * @param component
	 * @return
	 */
	WebComponent transferConstraints(Object element, WebComponent component) {
		if (element.hasProperty('layoutconstraints')) {
			JAXBElement oldConstraints = element.getProperties().get('layoutconstraints')
			LayoutmlToWeblayoutTransformer.LayoutConstraints constraints = LayoutmlToWeblayoutTransformer.LayoutConstraints.from(oldConstraints)
			if (constraints) {
				component.row = constraints.row
				component.column = constraints.column
				if (constraints.rowspan > 1) {
					component.rowspan = constraints.rowspan
				}
				if (constraints.colspan > 1) {
					component.colspan = constraints.colspan
				}
			}
		} else {
			log.warn('Could not transfer layout constraints from element: {}', element)
		}

		// If there is an 'enabled' attribute, transfer it.
		// Do not overwrite, if it already exists on the target element.
		if (element.hasProperty('enabled') && component.hasProperty('enabled') && component['enabled'] == null) {
			component['enabled'] = element['enabled'] == Boolean.YES
		}

		return component
	}

	/**
	 * Populates the grid with rows and columns based on the row/column properties of the given components.
	 * Always uses 12 columns.
	 *
	 * @param grid
	 * @param components
	 * @return
	 */
	WebGrid populateGrid(WebGrid grid, List<WebComponent> components) {
		grid.rows.addAll(getRows(components, 12))

		return grid
	}

	/**
	 * Populates the table with rows and columns based on the row/column properties of the given components.
	 * Determines the columns based on the 'column' and 'colspan' properties of the components.
	 *
	 * @param grid
	 * @param components
	 * @return
	 */
	WebTable populateTable(WebTable table, List<WebComponent> components) {
		def columnCount = components.collect {
			def colspan = it.colspan ?: 1g
			it.column + colspan
		}.max()?.toInteger()
		if (columnCount) {
			table.rows.addAll(getRows(components, columnCount))
		} else {
			log.warn('No columns for table {}', table)
		}

		return table
	}

	/**
	 * Creates a list of rows filled with the given components.
	 * Each row has exactly columnCount (possibly empty) columns.
	 *
	 * @param components
	 * @param columnCount
	 * @return
	 */
	List<WebRow> getRows(List<WebComponent> components, Integer columnCount) {
		List<WebRow> result = []

		BigInteger rows = 0
		components.each {
			if (it?.row > rows) {
				rows = it.row
			}
		}

		ComponentMatrix matrix = new ComponentMatrix(components)

		if (rows) {
			(1..rows).each { rowNum ->
				WebRow row = factory.createWebRow()
				result << row

				// Each column can contain multiple components
				Map<BigInteger, WebComponent> rowColumns = [:]

				components.findAll { it.row == rowNum.toBigInteger() }.each {
					if (rowColumns[it.column]) {
						log.warn('Ignoring duplicate component in row {}, column {}', rowNum, it.column)
						return
					}
					rowColumns[it.column] = it
				}

				WebComponent previousComponent = null
				(1g..columnCount.toBigInteger()).each { colNum ->
					if (isCoveredByPreviousComponent(colNum, previousComponent)) {
						return
					}

					WebComponent columnComponent = rowColumns[colNum]
					if (columnComponent) {
						WebCell cell = factory.createWebCell()
						if (columnComponent.colspan > 1) {
							cell.colspan = columnComponent.colspan
						}
						if (columnComponent.rowspan > 1) {
							cell.rowspan = columnComponent.rowspan
						}
						cell.components << columnComponent
						previousComponent = columnComponent
						row.cells << cell
					} else if (!matrix.isFilled(rowNum as Integer, colNum as Integer)) {
						// TODO: Use a single empty cell with colspan/rowspan instead of multiple adjacent empty cells
						WebCell cell = factory.createWebCell()
						row.cells << cell
					}
				}
			}
		} else if (components.size() == 1) {
			WebRow row = factory.createWebRow()
			result << row
			WebCell cell = factory.createWebCell()
			if (columnCount > 1) {
				cell.colspan = columnCount.toBigInteger()
			}
			cell.components << components[0]
			row.cells << cell
		}

		return result;
	}

	protected boolean isCoveredByPreviousComponent(BigInteger colNum, WebComponent previousComponent) {
		if (previousComponent) {
			def previousColspan = previousComponent.colspan ?: 1g
			if (previousComponent.column + previousColspan > colNum) {
				return true
			}
		}
		return false
	}

	int getNewAttributeId(String attributeName) {
		attributeIdFactory.getNewId(attributeName)
	}

	Integer getExistingAttributeId(String attributeName) {
		attributeIdFactory.getExistingId(attributeName)
	}

	static class LayoutConstraints {
		BigInteger row
		BigInteger column
		BigInteger rowspan
		BigInteger colspan

		static LayoutConstraints from(JAXBElement element) {
			def value = element.value
			if (value instanceof TablelayoutConstraints) {
				TablelayoutConstraints constraints = (TablelayoutConstraints) value
				return new LayoutConstraints(
						row: constraints.row1.toBigInteger(),
						column: constraints.col1.toBigInteger(),
						rowspan: constraints.row2.toBigInteger() - constraints.row1.toBigInteger() + 1,
						colspan: constraints.col2.toBigInteger() - constraints.col1.toBigInteger() + 1
				)
			}

			log.warn('Unknown LayoutConstraints: {}', element)

			return null
		}
	}

	static enum DataType {
		FILE
	}

	/**
	 * Represents the state (filled with a component or empty) of every cell
	 * in a grid or table layout.
	 */
	static class ComponentMatrix {
		final Map<Integer, BitSet> rows = new TreeMap<>()

		ComponentMatrix(List<WebComponent> components) {
			components.each { component ->
				def rowspan = component.rowspan ?: 1g
				rowspan.times {
					BitSet row = getRow(component.row + it as Integer)
					def colspan = component.colspan ?: 1g
					row.set(component.column.toInteger(), component.column + colspan as Integer)
				}
			}
		}

		/**
		 * Determines if the cell at the given row/col is filled by a component.
		 *
		 * @param row
		 * @param col
		 * @return
		 */
		boolean isFilled(int row, int col) {
			rows[row]?.get(col)
		}

		BitSet getRow(int index) {
			if (!rows[index]) {
				rows[index] = new BitSet()
			}
			return rows[index]
		}

		String toString() {
			rows.values()*.toString().join('\n')
		}
	}
}
