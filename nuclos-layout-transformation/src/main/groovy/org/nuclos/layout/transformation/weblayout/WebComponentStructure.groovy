package org.nuclos.layout.transformation.weblayout

import org.nuclos.schema.layout.web.*

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class WebComponentStructure {
	private final List<WebComponent> componentsStructured = new ArrayList<>()
	private final List<WebComponent> componentsFlat = new ArrayList<>()

	void addAll(final List<WebComponent> webComponents) {
		componentsStructured.addAll(webComponents)
		webComponents.each {
			componentsFlat.addAll(findAllComponents(it))
		}
	}

	List<WebComponent> getComponentsStructured() {
		return componentsStructured
	}

	List<WebComponent> getComponentsAsFlatList() {
		return componentsFlat
	}

	protected List<WebComponent> findAllComponents(def componentOrContainer) {
		List<WebComponent> result = []

		if (componentOrContainer instanceof WebComponent) {
			WebComponent component = (WebComponent) componentOrContainer
			result << component
			if (component instanceof WebContainer) {
				WebContainer container = (WebContainer) component
				result << container
				result.addAll(findAllComponents(container.grid))
				result.addAll(findAllComponents(container.table))
				result.addAll(findAllComponents(container.calculated))
				container.components.each {
					result.addAll(findAllComponents(it))
				}
			} else if (component instanceof WebTabcontainer) {
				WebTabcontainer container = (WebTabcontainer) component
				result << container
				container.tabs.each {
					result.addAll(findAllComponents(it))
				}
			}
		} else if (componentOrContainer instanceof WebGrid) {
			WebGrid grid = (WebGrid) componentOrContainer
			grid.rows.each { row ->
				row.cells.each { cell ->
					cell.components.each {
						result.addAll(findAllComponents(it))
					}
				}
			}
		} else if (componentOrContainer instanceof WebTable) {
			WebTable table = (WebTable) componentOrContainer
			table.rows.each { row ->
				row.cells.each { cell ->
					cell.components.each {
						result.addAll(findAllComponents(it))
					}
				}
			}
		} else if (componentOrContainer instanceof WebGridCalculated) {
			WebGridCalculated grid = (WebGridCalculated) componentOrContainer
			grid.cells.each { cell ->
				cell.components.each {
					result.addAll(findAllComponents(it))
				}
			}
		} else if (componentOrContainer instanceof WebTab) {
			WebTab tab = (WebTab) componentOrContainer
			result.addAll(findAllComponents(tab.content))
		} else if (componentOrContainer != null) {
			println "Unknown component: $componentOrContainer"
		}

		return result
	}
}

