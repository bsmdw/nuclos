package org.nuclos.layout.transformation.weblayout

import org.nuclos.schema.layout.layoutml.Label
import org.nuclos.schema.layout.web.WebComponent
import org.nuclos.schema.layout.web.WebLabelStatic

import groovy.transform.CompileStatic
import groovy.transform.PackageScope

/**
 * @author Andreas L�mmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
@PackageScope
class LabelTransformer extends ElementTransformer<Label, WebComponent> {

	LabelTransformer() {
		super(Label.class)
	}

	WebComponent transform(LayoutmlToWeblayoutTransformer layoutTransformer, Label label) {
		WebLabelStatic result = factory.createWebLabelStatic()

		result.text = label.text
		result.alternativeTooltip = label.description
		result.fontSize = convertFontSize(label.font?.size)
		setTextFormatInWebComponent(result, label.textFormat)

		return result
	}
}
