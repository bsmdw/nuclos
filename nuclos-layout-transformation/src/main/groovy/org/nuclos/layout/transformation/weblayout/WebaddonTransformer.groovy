package org.nuclos.layout.transformation.weblayout

import org.nuclos.schema.layout.layoutml.Property
import org.nuclos.schema.layout.layoutml.Webaddon
import org.nuclos.schema.layout.web.WebAddon
import org.nuclos.schema.layout.web.WebComponent
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import groovy.transform.CompileStatic
import groovy.transform.PackageScope

@CompileStatic
@PackageScope
class WebaddonTransformer extends ElementTransformer<Webaddon, WebComponent> {
	private static final Logger log = LoggerFactory.getLogger(WebaddonTransformer.class)

	WebaddonTransformer() {
		super(Webaddon.class)
	}

	WebComponent transform(LayoutmlToWeblayoutTransformer layoutTransformer, Webaddon webaddon) {
		WebAddon result = factory.createWebAddon()

		result.id = webaddon.id
		result.name = webaddon.name

		webaddon.propertyType.each {
			def value = it.value
			if (value instanceof Property) {
				Property desktopProperty = (Property) value

				def webProperty = factory.createWebAdvancedProperty()
				webProperty.name = desktopProperty.name
				webProperty.value = desktopProperty.value
				result.advancedProperties << webProperty
			}
		}

		return result
	}
}
