package org.nuclos.layout.transformation.weblayout

import org.nuclos.schema.layout.layoutml.Matrix
import org.nuclos.schema.layout.web.WebComponent
import org.nuclos.schema.layout.web.WebMatrix
import org.nuclos.schema.layout.web.WebMatrixColumn

import groovy.transform.CompileStatic
import groovy.transform.PackageScope

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
@PackageScope
class MatrixTransformer extends ElementTransformer<Matrix, WebComponent> {

	MatrixTransformer() {
		super(Matrix.class)
	}

	WebComponent transform(LayoutmlToWeblayoutTransformer layoutTransformer, Matrix matrix) {
		WebMatrix result = factory.createWebMatrix()

		result.name = matrix.name

		copyProperties(matrix, result)

		matrix.matrixColumn.each {
			// Skip invisible matrix columns
			if (it.visible == org.nuclos.schema.layout.layoutml.Boolean.NO) {
				return
			}

			WebMatrixColumn matrixColumn = factory.createWebMatrixColumn()
			copyProperties(it, matrixColumn)
			result.matrixColumns << matrixColumn
		}

		return result
	}

	/**
	 * Tries to copy all properties which exist on both objects.
	 *
	 * @param fromObject
	 * @param toObject
	 */
	private void copyProperties(def fromObject, def toObject) {
		fromObject?.properties.each { property, value ->
			String key = "$property"
			if (toObject?.hasProperty(key)) {
				try {
					toObject[key] = value
				} catch (Exception ex) {
				}
			}
		}
	}
}
