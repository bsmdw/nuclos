package org.nuclos.server.autosync.migration.m_04_00_00;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.prefs.AbstractPreferences;
import java.util.prefs.BackingStoreException;

import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * A DOM based in-memory implementation of the Java preferences API.
 * 
 * see http://www.roseindia.net/java/example/java/util/importing-preferences.shtml
 * 
 * @author Thomas Pasch
 * @since Nuclos 3.9
 */
class Prefs_DOMPreferences extends AbstractPreferences {

	private final Document document;
	
	private Element currentElement;
	
	Prefs_DOMPreferences(DocumentBuilder builder, InputStream is, boolean user) throws IOException {
		super(null, "");
		try {
			this.document = builder.parse(is);
		}
		catch (SAXException e) {
			throw new IOException(e);
		}
		this.currentElement = createRoot(user);
	}
	
	Prefs_DOMPreferences(Document doc, boolean user) {
		super(null, "");
		this.document = doc;
		this.currentElement = null;
		this.currentElement = createRoot(user);
	}
	
	Prefs_DOMPreferences(Prefs_DOMPreferences parent, Element element) {
		super(parent, element.getNodeName());
		this.document = parent.document;
		this.currentElement = element;
		
		parent.currentElement.appendChild(element);
	}
	
	private Element createMap() {
		Element first = (Element) currentElement.getFirstChild();
		if (first == null) {
			first = (Element) currentElement.appendChild(document.createElement("map"));
		}
		else {
			assert "map".equals(first.getNodeName());
		}
		return first;
	}
	
	private Element createRoot(boolean user) {
		Element prefs = createPreferencesNode();
		Element first = (Element) prefs.getFirstChild();		
		if (first == null) {
			first = (Element) currentElement.appendChild(document.createElement("root"));
		}
		else {
			assert "root".equals(first.getNodeName());
		}
		if (user) {
			first.setAttribute("type", "user");
		}
		else {
			first.setAttribute("type", "system");
		}
		return first;
	}
	
	private Element createPreferencesNode() {
		Element prefs = (Element) document.getDocumentElement();
		if (prefs == null) {
			prefs = (Element) document.appendChild(document.createElement("preferences"));
			prefs.setAttribute("EXTERNAL_XML_VERSION", "1.0");
		}
		else {
			assert "preferences".equals(prefs.getNodeName());
		}
		
		/*DocumentType dt = document.getDoctype();
		if (dt == null) {
			throw new IllegalStateException("No document type");
		}
		if (!"preferences".equals(dt.getName())) {
			throw new IllegalStateException("Wrong document type: " + dt);
		}
		if (!"http://java.sun.com/dtd/preferences.dtd".equals(dt.getSystemId())) {
			throw new IllegalStateException("Wrong document type: " + dt);
		}*/
		
		if (currentElement == null) {
			currentElement = prefs;
		}
		return prefs;
	}
	
	// New methods
	
	Element getDOMElement() {
		return currentElement;
	}
	
	Document getDOMDocument() {
		return document;
	}
	
	// Overridden methods
	
    public static void importPreferences(InputStream is) {
    	throw new UnsupportedOperationException();
    }
    
    @Override
    public void exportNode(OutputStream os)
            throws IOException, BackingStoreException {
    	
    	throw new UnsupportedOperationException();
    }

    @Override
    public void exportSubtree(OutputStream os)
        throws IOException, BackingStoreException {
    	
    	throw new UnsupportedOperationException();
    }

	@Override
    public boolean isUserNode() {
		// ???
		final Element root = createRoot(true);
		return "user".equals(root.getAttribute("type"));
    }
	
	@Override
	public void sync() {
		// ??? do nothing
	}
	
	@Override
	public void flush() {
		// ??? do nothing
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Prefs_DOMPreferences)) return false;
		final Prefs_DOMPreferences other = (Prefs_DOMPreferences) o;
		return currentElement.equals(other.currentElement) && document.equals(other.document);
	}
	
	@Override 
	public int hashCode() {
		return 7 * currentElement.hashCode() + 931;
	}
	
	// Implementation of SPI 
	
	@Override
	protected void putSpi(String key, String value) {
		final Element map = createMap();
		final NodeList children = map.getChildNodes();
		Element entry = null;
		for (int i = 0; i < children.getLength(); ++i) {
			final Element child = (Element) children.item(i);
			if (key.equals(child.getAttribute("key"))) {
				entry = child;
				break;
			}
		}
		if (entry == null) {
			entry = (Element) map.appendChild(document.createElement("entry"));
			entry.setAttribute("key", key);
		}
		entry.setAttribute("value", value);
	}

	@Override
	protected String getSpi(String key) {
		final Element map = createMap();
		final NodeList children = map.getChildNodes();
		Element entry = null;
		for (int i = 0; i < children.getLength(); ++i) {
			final Element child = (Element) children.item(i);
			if (key.equals(child.getAttribute("key"))) {
				entry = child;
				break;
			}
		}
		final String result;
		if (entry == null) {
			result = null;
		}
		else {
			result = entry.getAttribute("value");
		}
		return result;
	}

	@Override
	protected void removeSpi(String key) {
		final Element map = createMap();
		final NodeList children = map.getChildNodes();
		Element entry = null;
		for (int i = 0; i < children.getLength(); ++i) {
			final Element child = (Element) children.item(i);
			if (key.equals(child.getAttribute("key"))) {
				entry = child;
				break;
			}
		}
		if (entry != null) {
			final Element el = (Element) map.removeChild(entry);
			assert el == entry;
		}
	}

	@Override
	protected void removeNodeSpi() throws BackingStoreException {
		final Prefs_DOMPreferences parent = (Prefs_DOMPreferences) parent();
		final Element c = (Element) parent.currentElement.removeChild(currentElement);
		assert c == currentElement;
	}

	@Override
	protected String[] keysSpi() throws BackingStoreException {
		final Element map = createMap();
		final NodeList children = map.getChildNodes();
		final String[] result = new String[children.getLength()];
		for (int i = 0; i < children.getLength(); ++i) {
			final Element child = (Element) children.item(i);
			assert "entry".equals(child.getNodeName());
			result[i] = child.getAttribute("key");
		}
		return result;
	}

	@Override
	protected String[] childrenNamesSpi() throws BackingStoreException {
		final Element map = createMap();
		final NodeList children = currentElement.getChildNodes();
		final String[] result = new String[children.getLength() - 1];
		// ingnore first entry - its the 'map' element
		for (int i = 1; i < children.getLength(); ++i) {
			final Element child = (Element) children.item(i);
			assert "node".equals(child.getNodeName());
			result[i - 1] = child.getAttribute("name");
		}
		return result;
	}

	@Override
	protected AbstractPreferences childSpi(String name) {
		final Element map = createMap();
		final NodeList children = currentElement.getChildNodes();
		Element entry = null;
		// ingnore first entry - its the 'map' element
		for (int i = 1; i < children.getLength(); ++i) {
			final Element child = (Element) children.item(i);
			assert "node".equals(child.getNodeName());
			if (name.equals(child.getAttribute("name"))) {
				entry = child;
				break;
			}
		}
		if (entry == null) {
			// create
			entry = document.createElement("node");
			entry.setAttribute("name", name);
		}
		// Implicit append
		return new Prefs_DOMPreferences(this, entry);
	}

	@Override
	protected void syncSpi() throws BackingStoreException {
		// ??? do nothing
	}

	@Override
	protected void flushSpi() throws BackingStoreException {
		// ??? do nothing
	}

}
