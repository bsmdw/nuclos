//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.server.autosync.migration.m_04_00_00;

import java.awt.Rectangle;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.nuclos.api.Preferences;
import org.nuclos.api.PreferencesImpl;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.WorkspaceException;

public class WO_WorkspaceDescriptionNew implements Serializable {
	private static final long serialVersionUID = 6637996725938917463L;

	private String name;
	private boolean hide;
	private boolean hideName;
	private boolean hideMenuBar;
	private boolean alwaysOpenAtLogin;
	private boolean useLastFrameSettings;
	private boolean alwaysReset;
	private String nuclosResource;
	private List<Frame> frames;
	private List<EntityPreferences> entityPreferences;
	private List<TasklistPreferences> tasklistPreferences;
	private Map<String, String> parameters;

	public WO_WorkspaceDescriptionNew() {
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isHide() {
		return hide;
	}

	public void setHide(boolean hide) {
		this.hide = hide;
	}

	public boolean isHideName() {
		return this.hideName;
	}

	public void setHideName(boolean hideName) {
		this.hideName = hideName;
	}

	public boolean isHideMenuBar() {
		return hideMenuBar;
	}

	public void setHideMenuBar(boolean hideMenuBar) {
		this.hideMenuBar = hideMenuBar;
	}

	public boolean isAlwaysOpenAtLogin() {
		return alwaysOpenAtLogin;
	}

	public void setAlwaysOpenAtLogin(boolean alwaysOpenAtLogin) {
		this.alwaysOpenAtLogin = alwaysOpenAtLogin;
	}

	public boolean isUseLastFrameSettings() {
		return useLastFrameSettings;
	}

	public void setUseLastFrameSettings(boolean useLastFrameSettings) {
		this.useLastFrameSettings = useLastFrameSettings;
	}

	public boolean isAlwaysReset() {
		return alwaysReset;
	}

	public void setAlwaysReset(boolean alwaysReset) {
		this.alwaysReset = alwaysReset;
	}

	public String getNuclosResource() {
		return this.nuclosResource;
	}

	public void setNuclosResource(String nuclosResource) {
		this.nuclosResource = nuclosResource;
	}

	private List<Frame> _getFrames() {
		if (this.frames == null)
			this.frames = new ArrayList<Frame>();
		return this.frames;
	}

	public List<Frame> getFrames() {
		return this._getFrames();
	}

	public void addFrame(Frame frame) {
		this._getFrames().add(frame);
	}

	private List<EntityPreferences> _getEntityPreferences() {
		if (this.entityPreferences == null)
			this.entityPreferences = new ArrayList<EntityPreferences>();
		return this.entityPreferences;
	}

	public List<EntityPreferences> getEntityPreferences() {
		return new ArrayList<EntityPreferences>(this._getEntityPreferences());
	}

	/**
	 *
	 * @param entity
	 * @return
	 */
	public EntityPreferences getEntityPreferences(UID entity) {
		EntityPreferences result = null;
		for (EntityPreferences ep : this._getEntityPreferences()) {
			if (RigidUtils.equal(entity, ep.getEntity())) {
				result = ep;
				break;
			}
		}
		if (result == null) {
			result = new EntityPreferences();
			result.setEntity(entity);
			this._getEntityPreferences().add(result);
		}
		return result;
	}

	/**
	 *
	 * @param entity
	 * @return
	 */
	public boolean containsEntityPreferences(UID entity) {
		for (EntityPreferences ep : this._getEntityPreferences()) {
			if (RigidUtils.equal(entity, ep.getEntity())) {
				return true;
			}
		}
		return false;
	}

	public void addEntityPreferences(EntityPreferences ep) {
		this._getEntityPreferences().add(ep);
	}

	public void addAllEntityPreferences(Collection<EntityPreferences> eps) {
		this._getEntityPreferences().addAll(eps);
	}

	public void removeEntityPreferences(EntityPreferences ep) {
		this._getEntityPreferences().remove(ep);
	}

	public void removeAllEntityPreferences() {
		this._getEntityPreferences().clear();
	}

	private List<TasklistPreferences> _getTasklistPreferences() {
		if (tasklistPreferences == null) {
			tasklistPreferences = new ArrayList<WO_WorkspaceDescriptionNew.TasklistPreferences>();
		}
		return tasklistPreferences;
	}
	
	public List<TasklistPreferences> getTasklistPreferences() {
		return _getTasklistPreferences();
	}

	public void setTasklistPreferences(List<TasklistPreferences> tasklistPreferences) {
		this.tasklistPreferences = tasklistPreferences;
	}
	
	public TasklistPreferences getTasklistPreferences(Integer type, String name) {
		if (type==null) type=0;
		for (TasklistPreferences tp : _getTasklistPreferences()) {
			if (RigidUtils.equal(tp.getType(), type) && RigidUtils.equal(tp.getName(), name)) {
				return tp;
			}
		}
		TasklistPreferences tp = new TasklistPreferences();
		tp.setType(type);
		tp.setName(name);
		tp.setTablePreferences(new TablePreferences());
		addTasklistPreferences(tp);
		return tp;
	}
	
	public boolean containsTasklistPreferences(Integer type, String name) {
		if (type==null) type=0;
		for (TasklistPreferences tp : this._getTasklistPreferences()) {
			if (RigidUtils.equal(tp.getType(), type) && RigidUtils.equal(tp.getName(), name)) {
				return true;
			}
		}
		return false;
	}
	
	public void addTasklistPreferences(TasklistPreferences prefs) {
		_getTasklistPreferences().add(prefs);
	}
	
	public void addAllTasklistPreferences(Collection<TasklistPreferences> tlps) {
		this._getTasklistPreferences().addAll(tlps);
	}
	
	public void removeTasklistPreferences(TasklistPreferences tp) {
		this._getTasklistPreferences().remove(tp);
	}
	
	public void removeAllTasklistPreferences() {
		this._getTasklistPreferences().clear();
	}
	
	private Map<String, String> _getParameters() {
		if (this.parameters == null) 
			this.parameters = new HashMap<String, String>();
		return this.parameters;
	}
	
	public String getParameter(String parameter) {
		return this._getParameters().get(parameter);
	}
	
	public Map<String, String> getParameters() {
		return new HashMap<String, String>(this._getParameters());
	}
	
	public void setParameter(String parameter, String value) {
		this._getParameters().put(parameter, value);
	}
	
	public void setAllParameters(Map<String, String> parameters) {
		this._getParameters().putAll(parameters);
	}
	
	public void removeParameter(String parameter) {
		this._getParameters().remove(parameter);
	}
	
	public void removeAllParameters() {
		this._getParameters().clear();
	}

	public static class Tab implements Serializable {
		private static final long serialVersionUID = 6637996725938917463L;

		private String label;
		private String iconResolver;
		private String icon;
		private boolean neverClose = false;
		private boolean fromAssigned = false;;
		private String preferencesXML;
		private String restoreController;
		public String getLabel() {
			return label;
		}
		public void setLabel(String label) {
			this.label = label;
		}
		public String getIconResolver() {
			return iconResolver;
		}
		public void setIconResolver(String iconResolver) {
			this.iconResolver = iconResolver;
		}
		public String getIcon() {
			return icon;
		}
		public void setIcon(String icon) {
			this.icon = icon;
		}
		public boolean isNeverClose() {
			return neverClose;
		}
		public void setNeverClose(boolean neverClose) {
			this.neverClose = neverClose;
		}
		public boolean isFromAssigned() {
			return fromAssigned;
		}
		public void setFromAssigned(boolean fromAssigned) {
			this.fromAssigned = fromAssigned;
		}
		public String getPreferencesXML() {
			return preferencesXML;
		}
		public void setPreferencesXML(String preferencesXML) {
			this.preferencesXML = preferencesXML;
		}
		public String getRestoreController() {
			return restoreController;
		}
		public void setRestoreController(String restoreController) {
			this.restoreController = restoreController;
		}
		@Override
		public boolean equals(Object obj) {
			if (obj instanceof Tab)
				return RigidUtils.equal(this.preferencesXML, ((Tab) obj).getPreferencesXML()) &&
						RigidUtils.equal(this.restoreController, ((Tab) obj).getRestoreController()) &&
						RigidUtils.equal(this.fromAssigned, ((Tab) obj).isFromAssigned());
			return super.equals(obj);
		}
	}

	public static class Tabbed implements NestedContent {
		private static final long serialVersionUID = 6637996725938917463L;

		private boolean home = false;
		private boolean homeTree = false;
		private boolean showEntity = true;
		private boolean showAdministration = false;
		private boolean showConfiguration = false;
		private boolean neverHideStartmenu = false;
		private boolean neverHideHistory = false;
		private boolean neverHideBookmark = false;
		private boolean alwaysHideStartmenu = false;
		private boolean alwaysHideHistory = false;
		private boolean alwaysHideBookmark = false;
		private boolean desktopActive = false;
		private boolean hideStartTab = false;

		private int selected;
		private List<Tab> tabs;
		private Set<UID> predefinedEntityOpenLocation;
		private Set<String> reducedStartmenus;
		private Set<String> reducedHistoryEntities;
		private Set<String> reducedBookmarkEntities;

		private Desktop desktop;

		public boolean isHome() {
			return home;
		}
		public void setHome(boolean home) {
			this.home = home;
		}
		public boolean isHomeTree() {
			return homeTree;
		}
		public void setHomeTree(boolean homeTree) {
			this.homeTree = homeTree;
		}
		public int getSelected() {
			return selected;
		}
		public void setSelected(Integer selected) {
			if (selected==null) selected=0;
			this.selected = selected;
		}
		public List<Tab> getTabs() {
			if (this.tabs==null) this.tabs = new ArrayList<Tab>();
			return new ArrayList<Tab>(this.tabs);
		}
		public void addTab(Tab tab) {
			if (tab==null) return;
			if (this.tabs==null) this.tabs = new ArrayList<Tab>();
			this.tabs.add(tab);
		}
		public void addAllTab(List<Tab> tabs) {
			if (tabs==null) return;
			if (this.tabs==null) this.tabs = new ArrayList<Tab>();
			this.tabs.addAll(tabs);
		}
		public void removeTab(Tab tab) {
			if (tab==null) return;
			if (this.tabs==null) this.tabs = new ArrayList<Tab>();
			this.tabs.remove(tab);
		}
		public void clearTabs() {
			if (this.tabs==null) this.tabs = new ArrayList<Tab>();
			this.tabs.clear();
		}
		public boolean isShowEntity() {
			return showEntity;
		}
		public void setShowEntity(boolean showEntity) {
			this.showEntity = showEntity;
		}
		public boolean isShowConfiguration() {
			return showConfiguration;
		}
		public void setShowConfiguration(boolean showConfiguration) {
			this.showConfiguration = showConfiguration;
		}
		public boolean isShowAdministration() {
			return showAdministration;
		}
		public void setShowAdministration(boolean showAdministration) {
			this.showAdministration = showAdministration;
		}
		public boolean isNeverHideStartmenu() {
			return neverHideStartmenu;
		}
		public void setNeverHideStartmenu(boolean neverHideStartmenu) {
			this.neverHideStartmenu = neverHideStartmenu;
		}
		public boolean isNeverHideHistory() {
			return neverHideHistory;
		}
		public void setNeverHideHistory(boolean neverHideHistory) {
			this.neverHideHistory = neverHideHistory;
		}
		public boolean isNeverHideBookmark() {
			return neverHideBookmark;
		}
		public void setNeverHideBookmark(boolean neverHideBookmark) {
			this.neverHideBookmark = neverHideBookmark;
		}
		public boolean isAlwaysHideStartmenu() {
			return alwaysHideStartmenu;
		}
		public void setAlwaysHideStartmenu(boolean alwaysHideStartmenu) {
			this.alwaysHideStartmenu = alwaysHideStartmenu;
		}
		public boolean isAlwaysHideHistory() {
			return alwaysHideHistory;
		}
		public void setAlwaysHideHistory(boolean alwaysHideHistory) {
			this.alwaysHideHistory = alwaysHideHistory;
		}
		public boolean isAlwaysHideBookmark() {
			return alwaysHideBookmark;
		}
		public void setAlwaysHideBookmark(boolean alwaysHideBookmark) {
			this.alwaysHideBookmark = alwaysHideBookmark;
		}
		public Set<UID> getPredefinedEntityOpenLocations() {
			if (this.predefinedEntityOpenLocation==null) this.predefinedEntityOpenLocation = new HashSet<UID>();
			return predefinedEntityOpenLocation;
		}
		public void addPredefinedEntityOpenLocation(UID entity) {
			if (entity==null) return;
			if (this.predefinedEntityOpenLocation==null) this.predefinedEntityOpenLocation = new HashSet<UID>();
			this.predefinedEntityOpenLocation.add(entity);
		}
		public void addAllPredefinedEntityOpenLocations(List<UID> entities) {
			if (entities==null) return;
			if (this.predefinedEntityOpenLocation==null) this.predefinedEntityOpenLocation = new HashSet<UID>();
			this.predefinedEntityOpenLocation.addAll(entities);
		}
		public Set<String> getReducedStartmenus() {
			if (this.reducedStartmenus==null) this.reducedStartmenus = new HashSet<String>();
			return reducedStartmenus;
		}
		public void addReducedStartmenu(String reducedStartmenu) {
			if (reducedStartmenu==null) return;
			if (this.reducedStartmenus==null) this.reducedStartmenus = new HashSet<String>();
			this.reducedStartmenus.add(reducedStartmenu);
		}
		public void addAllReducedStartmenus(Set<String> reducedStartmenus) {
			if (reducedStartmenus==null) return;
			if (this.reducedStartmenus==null) this.reducedStartmenus = new HashSet<String>();
			this.reducedStartmenus.addAll(reducedStartmenus);
		}
		public Set<String> getReducedHistoryEntities() {
			if (this.reducedHistoryEntities==null) this.reducedHistoryEntities = new HashSet<String>();
			return reducedHistoryEntities;
		}
		public void addReducedHistoryEntity(String reducedHistoryEntity) {
			if (reducedHistoryEntity==null) return;
			if (this.reducedHistoryEntities==null) this.reducedHistoryEntities = new HashSet<String>();
			this.reducedHistoryEntities.add(reducedHistoryEntity);
		}
		public void addAllReducedHistoryEntities(Set<String> reducedHistoryEntities) {
			if (reducedHistoryEntities==null) return;
			if (this.reducedHistoryEntities==null) this.reducedHistoryEntities = new HashSet<String>();
			this.reducedHistoryEntities.addAll(reducedHistoryEntities);
		}
		public Set<String> getReducedBookmarkEntities() {
			if (this.reducedBookmarkEntities==null) this.reducedBookmarkEntities = new HashSet<String>();
			return reducedBookmarkEntities;
		}
		public void addReducedBookmarkEntity(String reducedBookmarkEntity) {
			if (reducedBookmarkEntity==null) return;
			if (this.reducedBookmarkEntities==null) this.reducedBookmarkEntities = new HashSet<String>();
			this.reducedBookmarkEntities.add(reducedBookmarkEntity);
		}
		public void addAllReducedBookmarkEntities(Set<String> reducedBookmarkEntities) {
			if (reducedBookmarkEntities==null) return;
			if (this.reducedBookmarkEntities==null) this.reducedBookmarkEntities = new HashSet<String>();
			this.reducedBookmarkEntities.addAll(reducedBookmarkEntities);
		}
		public Desktop getDesktop() {
			return desktop;
		}
		public void setDesktop(Desktop desktop) {
			this.desktop = desktop;
		}
		public boolean isDesktopActive() {
			return desktopActive;
		}
		public void setDesktopActive(boolean desktopActive) {
			this.desktopActive = desktopActive;
		}
		public boolean isHideStartTab() {
			return hideStartTab;
		}
		public void setHideStartTab(boolean hideStartTab) {
			this.hideStartTab = hideStartTab;
		}
	}

	public static class Split implements NestedContent {
		private static final long serialVersionUID = 6637996725938917463L;
		
		public static final int FIXED_STATE_NONE = 0;
		public static final int FIXED_STATE_LEFT = 1;
		public static final int FIXED_STATE_RIGHT = 2;

		private boolean horizontal;
		private boolean expanded;
		private int position;
		private int fixedState;
		private final MutableContent contentA = new MutableContent();
		private final MutableContent contentB = new MutableContent();
		public boolean isHorizontal() {
			return horizontal;
		}
		public void setHorizontal(boolean horizontal) {
			this.horizontal = horizontal;
		}
		public boolean isExpanded() {
			return expanded;
		}
		public void setExpanded(boolean expanded) {
			this.expanded = expanded;
		}
		public int getPosition() {
			return position;
		}
		public void setPosition(Integer position) {
			if (position==null) position=0;
			this.position = position;
		}
		public int getFixedState() {
			return fixedState;
		}
		public void setFixedState(Integer fixedState) {
			if (fixedState==null) fixedState=0;
			this.fixedState = fixedState;
		}
		public MutableContent getContentA() {
			return contentA;
		}
		public MutableContent getContentB() {
			return contentB;
		}
	}

	public static class Frame implements Serializable {
		private static final long serialVersionUID = 6637996725938917463L;

		private boolean mainFrame;
		private int number = 0;
		private final MutableContent content = new MutableContent();
		private int extendedState;
		Rectangle normalBounds;
		public MutableContent getContent() {
			return content;
		}
		public int getExtendedState() {
			return extendedState;
		}
		public void setExtendedState(Integer extendedState) {
			if (extendedState==null) extendedState=0;
			this.extendedState = extendedState;
		}
		public Rectangle getNormalBounds() {
			return normalBounds;
		}
		public void setNormalBounds(Rectangle normalBounds) {
			this.normalBounds = normalBounds;
		}
		public boolean isMainFrame() {
			return mainFrame;
		}
		public void setMainFrame(boolean mainFrame) {
			this.mainFrame = mainFrame;
		}
		public int getNumber() {
			return number;
		}
		public void setNumber(Integer number) {
			if (number==null) number=0;
			this.number = number;
		}
	}

	public static class MutableContent implements NestedContent {
		private static final long serialVersionUID = 6637996725938917463L;

		private NestedContent content;
		public NestedContent getContent() {
			return content;
		}
		public void setContent(NestedContent content) {
			this.content = content;
		}
	}

	public static interface NestedContent extends Serializable {}
	
	/**
	 * 
	 * Support NUCLOS-1479 Profile Management
	 *
	 */
	public static interface ProfileManager extends Serializable{
		public TablePreferences getTablePreferences();
		public List<TablePreferences> getProfiles();
		public List<TablePreferences> getPersonalProfiles();
		public UID getEntity();
		public void clearTablePreferences();
	}

	public static class EntityPreferences implements ProfileManager,Serializable {
		private static final long serialVersionUID = 6637996725938917463L;

		private UID entity;
		// NUCLOS-1479 List of Profiles will replace the simple resultPreferences settings
		private List<TablePreferences> profiles;
		private List<TablePreferences> personalProfiles;
				
		private List<SubFormPreferences> subFormPreferences;
		private ConcurrentMap<String, Preferences> layoutComponentPreferences;
		
		private List<MatrixPreferences> matrixPreferences;

		public UID getEntity() {
			return entity;
		}
		public void setEntity(UID entity) {
			this.entity = entity;
		}
		
		@Override
		public TablePreferences getTablePreferences() {
			return this.getResultPreferences();
		}
		
		private List<MatrixPreferences> _getMatrixPreferences() {
			if(this.matrixPreferences == null)
				this.matrixPreferences = new ArrayList<WO_WorkspaceDescriptionNew.MatrixPreferences>();
			return this.matrixPreferences;
		}
		public List<MatrixPreferences> getMatrixPreferences () {
			return new ArrayList<WO_WorkspaceDescriptionNew.MatrixPreferences>(this._getMatrixPreferences());
		}

		private List<SubFormPreferences> _getSubFormPreferences() {
			if (this.subFormPreferences == null)
				this.subFormPreferences = new ArrayList<SubFormPreferences>();
			return this.subFormPreferences;
		}
		public List<SubFormPreferences> getSubFormPreferences() {
			return new ArrayList<SubFormPreferences>(this._getSubFormPreferences());
		}
		
		public TablePreferences getResultPreferences() {
			//return this._getTablePreferences();
			final List<TablePreferences> lstProfiles = new ArrayList<WO_WorkspaceDescriptionNew.TablePreferences>();
			lstProfiles.addAll(this.getProfiles());
			lstProfiles.addAll(this.getPersonalProfiles());
			
		    TablePreferences activeProfile = RigidUtils.findFirst(lstProfiles, new org.nuclos.common.collection.Predicate<TablePreferences>() {

		    	@Override
		    	public boolean evaluate(final TablePreferences tp) {
		    		return tp.isActive();
		    	}
		    });
		    
		    // active profile
		    
		    if (null == activeProfile) {
		    	if (!this.getPersonalProfiles().isEmpty()) {
		    		activeProfile = this.getPersonalProfiles().iterator().next();
		    		activeProfile.setActive(true);
		    	} else if (!this.getProfiles().isEmpty()) {
		    		activeProfile = this.getProfiles().iterator().next();
		    		activeProfile.setActive(true);
		    	}
		    }
		    // FIXME
		    if (null == activeProfile) {
		    	activeProfile = new TablePreferences();
		    	activeProfile.setActive(true);
		    	getPersonalProfiles().add(activeProfile);
		    }
		    return activeProfile;
		   
		}
		
		public void clearTablePreferences() {
			this.getResultPreferences().clear();
		}
		
		private void clearProfiles() {
			getProfiles().clear();
			getPersonalProfiles().clear();
		}
		
		private List<TablePreferences> _getProfiles() {
			if (this.profiles == null) {
				this.profiles = new ArrayList<WO_WorkspaceDescriptionNew.TablePreferences>();
			}
			return this.profiles;
		}
		
		@Override
		public List<TablePreferences> getProfiles() {
			return _getProfiles();
		}
		public void addProfile(TablePreferences tablePreferences) {
			_getProfiles().add(tablePreferences);
		}

		public List<TablePreferences> _getPersonalProfiles() {
			if (null == this.personalProfiles) {
				this.personalProfiles = new ArrayList<WO_WorkspaceDescriptionNew.TablePreferences>();
			}

			return this.personalProfiles;

		}
		@Override
		public List<TablePreferences> getPersonalProfiles() {
			return this._getPersonalProfiles();

		}
		public void addPersonalProfile(TablePreferences tablePreferences) {
			_getPersonalProfiles().add(tablePreferences);
		}
		
		/**
		 *
		 * @param subForm
		 * @return
		 */
		public SubFormPreferences getSubFormPreferences(UID subForm) {
			SubFormPreferences result = null;
			for (SubFormPreferences sfp : this._getSubFormPreferences()) {
				if (RigidUtils.equal(subForm, sfp.getEntity())) {
					result = sfp;
					break;
				}
			}
			if (result == null) {
				result = new SubFormPreferences();
				result.setEntity(subForm);
				this._getSubFormPreferences().add(result);
			}
			return result;
		}
		
		public void addMatrixPreferences(MatrixPreferences mp) {
			this._getMatrixPreferences().add(mp);
		}
		public void addAllMatrixPreferences(List<MatrixPreferences> lst) {
			this._getMatrixPreferences().addAll(lst);
		}
		public void removeMatrixPreferences(MatrixPreferences mp) {
			this._getMatrixPreferences().remove(mp);
		}
		public void removeAllMatrixPreferences(List<MatrixPreferences> lst) {
			this._getMatrixPreferences().removeAll(lst);
		}

		public void addSubFormPreferences(SubFormPreferences sfp) {
			this._getSubFormPreferences().add(sfp);
		}
		public void addAllSubFormPreferences(List<SubFormPreferences> sfps) {
			this._getSubFormPreferences().addAll(sfps);
		}
		public void removeSubFormPreferences(SubFormPreferences sfp) {
			this._getSubFormPreferences().remove(sfp);
		}
		public void removeAllSubFormPreferences() {
			this._getSubFormPreferences().clear();
		}
		public void clearResultPreferences() {
			getTablePreferences().clear();
		}
		
		private ConcurrentMap<String, Preferences> _getLayoutComponentPreferences() {
			if (this.layoutComponentPreferences == null)
				this.layoutComponentPreferences = new ConcurrentHashMap<String, Preferences>();
			return this.layoutComponentPreferences;
		}
		
		public Preferences getLayoutComponentPreferences(String name) {
			final ConcurrentMap<String, Preferences> prefs = _getLayoutComponentPreferences();
			if (!prefs.containsKey(name)) {
				prefs.put(name, new PreferencesImpl());
			}
			return prefs.get(name);
		}
		
		public Map<String, Preferences> getAllLayoutComponentPreferences() {
			return new HashMap<String, Preferences>(_getLayoutComponentPreferences());
		}
		
		public void addLayoutComponentPreferences(String name, Preferences prefs) {
			_getLayoutComponentPreferences().put(name, prefs);
		}
		
		public void addAllLayoutComponentPreferences(Map<String, Preferences> prefsMap) {
			_getLayoutComponentPreferences().putAll(prefsMap);
		}
		
		public void removeLayoutComponentPreferences(String name) {
			_getLayoutComponentPreferences().remove(name);
		}
		
		public void removeAllLayoutComponentPreferences() {
			_getLayoutComponentPreferences().clear();
		}

		@Override
		public int hashCode() {
			if (entity == null)
				return 0;
			return entity.hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;

			if (obj instanceof EntityPreferences) {
				EntityPreferences other = (EntityPreferences) obj;
				return RigidUtils.equal(getEntity(), other.getEntity());
			}
			return super.equals(obj);
		}

		@Override
		public String toString() {
			if (entity == null)
				return "null";
			return entity.toString();
		}
	}

	public static class SubFormPreferences implements ProfileManager, Serializable {
		private static final long serialVersionUID = 6637996725938917463L;

		private UID entity;
		// NUCLOS-1479 List of Profiles will replace the simple tablePreferences settings
		private List<TablePreferences> profiles;
		private List<TablePreferences> personalProfiles;

		public UID getEntity() {
			return entity;
		}

		public void setEntity(UID entity) {
			this.entity = entity;
		}
		
		@Override
		public void clearTablePreferences() {
			getProfiles().clear();
			getPersonalProfiles().clear();
		}
		
		@Override
		public TablePreferences getTablePreferences() {
			//return this._getTablePreferences();
			final List<TablePreferences> lstProfiles = new ArrayList<WO_WorkspaceDescriptionNew.TablePreferences>();
			lstProfiles.addAll(this.getProfiles());
			lstProfiles.addAll(this.getPersonalProfiles());
			
		    TablePreferences activeProfile = RigidUtils.findFirst(lstProfiles, new org.nuclos.common.collection.Predicate<TablePreferences>() {

		    	@Override
		    	public boolean evaluate(final TablePreferences tp) {
		    		return tp.isActive();
		    	}
		    });
		    
		    // active profile
		    if (null == activeProfile) {
		    	if (!this.getPersonalProfiles().isEmpty()) {
		    		activeProfile = this.getPersonalProfiles().iterator().next();
		    		activeProfile.setActive(true);
		    	} else if (!this.getProfiles().isEmpty()) {
		    		activeProfile = this.getProfiles().iterator().next();
		    		activeProfile.setActive(true);
		    	}
		    }
		    // FIXME
		    if (null == activeProfile) {
		    	activeProfile = new TablePreferences();
		    	activeProfile.setActive(true);
		    	getPersonalProfiles().add(activeProfile);
		    }
		    
		    return activeProfile;
		}

		private List<TablePreferences> _getProfiles() {
			if (this.profiles == null) {
				this.profiles = new ArrayList<WO_WorkspaceDescriptionNew.TablePreferences>();
			}
			return this.profiles;
		}
		
		@Override
		public List<TablePreferences> getProfiles() {
			return _getProfiles();
		}
		
		public void addProfile(TablePreferences tablePreferences) {
			_getProfiles().add(tablePreferences);
		}
		
		public List<TablePreferences> _getPersonalProfiles() {
			if (null == this.personalProfiles) {
				this.personalProfiles = new ArrayList<WO_WorkspaceDescriptionNew.TablePreferences>();
			}		
			if (this._getProfiles().isEmpty() && this.personalProfiles.isEmpty()) {
				this.personalProfiles.add(new TablePreferences());
				
			}
			return this.personalProfiles;

		}
		
		@Override
		public List<TablePreferences> getPersonalProfiles() {
			return this._getPersonalProfiles();

		}
		
		public void addPersonalProfile(TablePreferences tablePreferences) {
			_getPersonalProfiles().add(tablePreferences);
		}

		@Override
		public int hashCode() {
			if (entity == null)
				return 0;
			return entity.hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;

			if (obj instanceof SubFormPreferences) {
				SubFormPreferences other = (SubFormPreferences) obj;
				return RigidUtils.equal(getEntity(), other.getEntity());
			}
			return super.equals(obj);
		}

		@Override
		public String toString() {
			if (entity == null)
				return "null";
			return entity.toString();
		}
	}

	public static class TablePreferences implements Serializable {
		
		private static final long serialVersionUID = 6637996725938917463L;

		private List<ColumnPreferences> selectedColumnPreferences;
		private Set<UID> hiddenColumns;
		private List<ColumnSorting> columnSorting;
		private boolean isDynamicRowHeight;
		private String name;
		private boolean isActive;
		private String identifier;	// unique identifier
		
		public String getName() {
			return name;
		}
		
		private String _getIdentifier() {
			if (null == this.identifier) {
				this.identifier = UUID.randomUUID().toString();
			}
			return this.identifier.toString();
		}

		public String getIdentifier() {
			return this._getIdentifier();
		}
		
		public void setName(String name) {
			this.name = name;
		}

		private List<ColumnPreferences> _getSelectedColumnPreferences() {
			if (this.selectedColumnPreferences == null)
				this.selectedColumnPreferences = new ArrayList<ColumnPreferences>();
			return this.selectedColumnPreferences;
		}
		public List<ColumnPreferences> getSelectedColumnPreferences() {
			return new ArrayList<ColumnPreferences>(this._getSelectedColumnPreferences());
		}
		public void addSelectedColumnPreferences(ColumnPreferences cp) {
			this._getSelectedColumnPreferences().add(cp);
		}
		public void addSelectedColumnPreferencesInFront(ColumnPreferences cp) {
			this._getSelectedColumnPreferences().add(0, cp);
		}
		public void addAllSelectedColumnPreferencesInFront(List<ColumnPreferences> cps) {
			this._getSelectedColumnPreferences().addAll(0, cps);
		}
		public void addAllSelectedColumnPreferences(List<ColumnPreferences> cps) {
			this._getSelectedColumnPreferences().addAll(cps);
		}
		public void removeSelectedColumnPreferences(ColumnPreferences cp) {
			this._getSelectedColumnPreferences().remove(cp);
		}
		public void removeAllSelectedColumnPreferences() {
			this._getSelectedColumnPreferences().clear();
		}

		private List<ColumnSorting> _getColumnSortings() {
			if (this.columnSorting == null)
				this.columnSorting = new ArrayList<ColumnSorting>();
			return this.columnSorting;
		}
		public List<ColumnSorting> getColumnSortings() {
			return new ArrayList<ColumnSorting>(this._getColumnSortings());
		}
		public void addColumnSorting(ColumnSorting cs) {
			this._getColumnSortings().add(cs);
		}
		public void addAllColumnSortings(List<ColumnSorting> css) {
			this._getColumnSortings().addAll(css);
		}
		public void removeColumnSorting(ColumnSorting cs) {
			this._getColumnSortings().remove(cs);
		}
		public void removeAllColumnSortings() {
			this._getColumnSortings().clear();
		}

		private Set<UID> _getHiddenColumns() {
			if (this.hiddenColumns == null)
				this.hiddenColumns = new HashSet<UID>();
			return this.hiddenColumns;
		}
		public Set<UID> getHiddenColumns() {
			return new HashSet<UID>(this._getHiddenColumns());
		}
		public void addHiddenColumn(UID column) {
			this._getHiddenColumns().add(column);
		}
		public void addAllHiddenColumns(Set<UID> columns) {
			this._getHiddenColumns().addAll(columns);
		}
		public void removeHiddenColumn(UID column) {
			this._getHiddenColumns().remove(column);
		}
		public void removeAllHiddenColumns() {
			this._getHiddenColumns().clear();
		}
		
		public boolean isDynamicRowHeight() {
			return isDynamicRowHeight;
		}
		
		public void setDynamicRowHeight(boolean isDynamicRowHeight) {
			this.isDynamicRowHeight = isDynamicRowHeight;
		}
		
		public void setActive(boolean isActive) {
			this.isActive = isActive;
		}
		
		public boolean isActive() {
			return this.isActive;
		}

		public TablePreferences copy() {
			TablePreferences result = new TablePreferences();
			for (ColumnPreferences cp : this._getSelectedColumnPreferences())
				result.addSelectedColumnPreferences(cp.copy());
			for (UID hidden : this._getHiddenColumns())
				result.addHiddenColumn(hidden);
			for (ColumnSorting cs : this._getColumnSortings())
				result.addColumnSorting(cs.copy());
			
			result.setName(this.getName());
			result.setDynamicRowHeight(this.isDynamicRowHeight());
			return result;
		}

		public void clear() {
			this.removeAllSelectedColumnPreferences();
			this.removeAllHiddenColumns();
			this.removeAllColumnSortings();
			this.setName(null);
			this.setDynamicRowHeight(false);
		}

		public void clearAndImport(TablePreferences tp) {
			clear();
			final TablePreferences toImportPrefs = tp.copy();
			this.addAllSelectedColumnPreferences(toImportPrefs.getSelectedColumnPreferences());
			this.addAllHiddenColumns(toImportPrefs.getHiddenColumns());
			this.addAllColumnSortings(toImportPrefs.getColumnSortings());
			this.setName(toImportPrefs.getName());
			this.setDynamicRowHeight(toImportPrefs.isDynamicRowHeight());
		}
	}

	public static class ColumnPreferences implements Serializable {
		private static final long serialVersionUID = 6637996725938917463L;

		public static final int TYPE_DEFAULT = 0;
		public static final int TYPE_EOEntityField = 1;
		public static final int TYPE_GenericObjectEntityField = 2;
		public static final int TYPE_MasterDataForeignKeyEntityField = 3;
		public static final int TYPE_EntityFieldWithEntity = 4;
		public static final int TYPE_EntityFieldWithEntityForExternal = 5;

		private UID column;
		private UID entity;
		private int width;
		private boolean fixed;

		private int type;

		public UID getColumn() {
			return column;
		}
		public void setColumn(UID column) {
			this.column = column;
		}
		public int getWidth() {
			return width;
		}
		public void setWidth(Integer width) {
			if (width==null) width=0;
			this.width = width;
		}
		public UID getEntity() {
			return entity;
		}
		public void setEntity(UID entity) {
			this.entity = entity;
		}
		public boolean isFixed() {
			return fixed;
		}
		public void setFixed(Boolean fixed) {
			if (fixed==null) fixed=false;
			this.fixed = fixed;
		}
		public int getType() {
			return type;
		}
		public void setType(Integer type) {
			if (type==null) type=0;
			this.type = type;
		}
		@Override
		public int hashCode() {
			if (column == null)
				return 0;
			return column.hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof ColumnPreferences) {
				ColumnPreferences other = (ColumnPreferences) obj;
				return RigidUtils.equal(getColumn(), other.getColumn()) &&
					   RigidUtils.equal(getEntity(), other.getEntity()) &&
					   RigidUtils.equal(getType(), other.getType());
			}
			return super.equals(obj);
		}

		@Override
		public String toString() {
			final StringBuilder sb = new StringBuilder();
			sb.append("ColumnPreferences[").append(entity).append(", ");
			sb.append(column).append(", ");
			sb.append(type);
			sb.append("]");
			return sb.toString();
		}

		public ColumnPreferences copy() {
			ColumnPreferences result = new ColumnPreferences();
			result.setColumn(column);
			result.setEntity(entity);
			result.setFixed(fixed);
			result.setWidth(width);
			result.setType(type);
			return result;
		}
	}

	public static class ColumnSorting implements Serializable {
		private static final long serialVersionUID = 6637996725938917463L;

		private UID column;
		private boolean asc = true;

		public UID getColumn() {
			return column;
		}
		public void setColumn(UID column) {
			this.column = column;
		}
		public boolean isAsc() {
			return asc;
		}
		public void setAsc(boolean asc) {
			this.asc = asc;
		}

		@Override
		public int hashCode() {
			if (column == null)
				return 0;
			return column.hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof ColumnSorting) {
				ColumnSorting other = (ColumnSorting) obj;
				RigidUtils.equal(getColumn(), other.getColumn());
			}
			return super.equals(obj);
		}

		@Override
		public String toString() {
			if (column == null)
				return "null";
			return column.toString();
		}

		public ColumnSorting copy() {
			ColumnSorting result = new ColumnSorting();
			result.setColumn(getColumn());
			result.setAsc(isAsc());
			return result;
		}
	}

	public static class Color implements Serializable {
		private static final long serialVersionUID = 6637996725938917463L;
		private final int red, green, blue;
		public Color(int red, int green, int blue) {
			super();
			this.red = red;
			this.green = green;
			this.blue = blue;
		}
		public int getRed() {
			return red;
		}
		public int getGreen() {
			return green;
		}
		public int getBlue() {
			return blue;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj instanceof Color) {
				Color other = (Color) obj;
				return this.red == other.red &&
						this.green == other.green &&
						this.blue == other.blue;
			}
			return super.equals(obj);
		}
		@Override
		public int hashCode() {
			return Arrays.hashCode(new int[]{red, green, blue});
		}
		public java.awt.Color toColor() {
			return new java.awt.Color(red, green, blue);
		}
	}

	public static interface DesktopItem extends Serializable{}

	public static class Action implements Serializable {
		private static final long serialVersionUID = 6637996725938917463L;
		private String action;
		private Map<String, String> stringParams;
		private Map<String, Long> longParams;
		private Map<String, Boolean> booleanParams;
		public String getAction() {
			return action;
		}
		public void setAction(String action) {
			this.action = action;
		}
		public void putStringParameter(String key, String value) {
			_getStringParams().put(key, value);
		}
		public String getStringParameter(String key) {
			return _getStringParams().get(key);
		}
		public void putLongParameter(String key, Long value) {
			_getLongParams().put(key, value);
		}
		public Long getLongParameter(String key) {
			return _getLongParams().get(key);
		}
		public void putBooleanParameter(String key, Boolean value) {
			_getBooleanParams().put(key, value);
		}
		public Boolean getBooleanParameter(String key) {
			return _getBooleanParams().get(key);
		}
		private Map<String, String> _getStringParams() {
			if (stringParams == null) {
				stringParams = new HashMap<String, String>();
			}
			return stringParams;
		}
		private Map<String, Long> _getLongParams() {
			if (longParams == null) {
				longParams = new HashMap<String, Long>();
			}
			return longParams;
		}
		private Map<String, Boolean> _getBooleanParams() {
			if (booleanParams == null) {
				booleanParams = new HashMap<String, Boolean>();
			}
			return booleanParams;
		}
		@Override
		public boolean equals(Object obj) {
			if (obj == this)
			    return true;
			if (obj instanceof Action) {
				Action other = (Action) obj;
				if (!RigidUtils.equal(this.getAction(), other.getAction())) {
					return false;
				}
				if (!RigidUtils.equal(this._getStringParams(), other._getStringParams())) {
					return false;
				}
				if (!RigidUtils.equal(this._getLongParams(), other._getLongParams())) {
					return false;
				}
				if (!RigidUtils.equal(this._getBooleanParams(), other._getBooleanParams())) {
					return false;
				}
				return true;
			}
			return super.equals(obj);
		}
		@Override
		public int hashCode() {
			if (action != null) {
				return action.hashCode();
			}
			return 0;
		}
		@Override
		public String toString() {
			final StringBuffer result = new StringBuffer();
			result.append("Action=").append(getAction());
			result.append(",StringParams=").append(_getStringParams().toString());
			result.append(",LongParams=").append(_getLongParams().toString());
			result.append(",BooleanParams=").append(_getBooleanParams().toString());
			return result.toString();
		}

	}

	public static class MenuItem implements Serializable {
		private static final long serialVersionUID = 6637996725938917463L;
		private Action menuAction;
		public Action getMenuAction() {
			return menuAction;
		}
		public void setMenuAction(Action menuAction) {
			this.menuAction = menuAction;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj instanceof MenuItem) {
				MenuItem other = (MenuItem) obj;
				return RigidUtils.equal(this.menuAction, other.menuAction);
			}
			return super.equals(obj);
		}

	}
	
	public static class ApiDesktopItem implements DesktopItem {
		private static final long serialVersionUID = 4572201568744295412L;
		private String id;
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj instanceof ApiDesktopItem) {
				ApiDesktopItem other = (ApiDesktopItem) obj;
				return RigidUtils.equal(this.id, other.id);
			}
			return super.equals(obj);
		}
		@Override
		public int hashCode() {
			return RigidUtils.hashCode(id);
		}
	}

	public static class MenuButton implements DesktopItem {
		private static final long serialVersionUID = 6637996725938917463L;
		private Action menuAction;
		private UID resourceIcon, resourceIconHover; 
		private String nuclosResource, nuclosResourceHover;
		private List<MenuItem> menuItems;
		public Action getMenuAction() {
			return menuAction;
		}
		public void setMenuAction(Action menuAction) {
			this.menuAction = menuAction;
		}
		public UID getResourceIcon() {
			return resourceIcon;
		}
		public void setResourceIcon(UID resourceIconHover) {
			this.resourceIcon = resourceIconHover;
		}
		public UID getResourceIconHover() {
			return resourceIconHover;
		}
		public void setResourceIconHover(UID resourceIconHover) {
			this.resourceIconHover = resourceIconHover;
		}
		public String getNuclosResource() {
			return nuclosResource;
		}
		public void setNuclosResource(String nuclosResource) {
			this.nuclosResource = nuclosResource;
		}
		public String getNuclosResourceHover() {
			return nuclosResourceHover;
		}
		public void setNuclosResourceHover(String nuclosResourceHover) {
			this.nuclosResourceHover = nuclosResourceHover;
		}
		private List<MenuItem> _getMenuItems() {
			if (menuItems == null)
				menuItems = new ArrayList<MenuItem>();
			return menuItems;
		}
		public List<MenuItem> getMenuItems() {
			return new ArrayList<MenuItem>(_getMenuItems());
		}
		public void addMenuItem(MenuItem mi) {
			_getMenuItems().add(mi);
		}
		public void addMenuItem(int index, MenuItem mi) {
			_getMenuItems().add(index, mi);
		}
		public void addAllMenuItems(List<MenuItem> mis) {
			_getMenuItems().addAll(mis);
		}
		public boolean removeMenuItem(MenuItem mi) {
			return _getMenuItems().remove(mi);
		}
		public void removeAllMenuItems() {
			_getMenuItems().clear();
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj instanceof MenuButton) {
				MenuButton other = (MenuButton) obj;
				return RigidUtils.equal(this.menuAction, other.menuAction) &&
						RigidUtils.equal(this.menuItems, other.menuItems) &&
						RigidUtils.equal(this.nuclosResource, other.nuclosResource) &&
						RigidUtils.equal(this.resourceIcon, other.resourceIcon) &&
						RigidUtils.equal(this.resourceIconHover, other.resourceIconHover);
			}
			return super.equals(obj);
		}
	}

	public static class Desktop implements Serializable {
		private static final long serialVersionUID = 6637996725938917463L;
		public static final int LAYOUT_WRAP = 0;
		public static final int LAYOUT_ONE_ROW = 1;
		/** <code>SwingConstants.CENTER</code> */		public static final int HORIZONTAL_ALIGNMENT_CENTER = 0;
		/** <code>SwingConstants.LEFT</code> */			public static final int HORIZONTAL_ALIGNMENT_LEFT = 2;
		/** <code>SwingConstants.RIGHT</code> */		public static final int HORIZONTAL_ALIGNMENT_RIGHT = 4;
		private int horizontalGap, verticalGap, menuItemTextSize, layout, menuItemTextHorizontalPadding, menuItemTextHorizontalAlignment;
		private Color menuItemTextColor, menuItemTextHoverColor;
		private UID resourceMenuBackground, resourceMenuBackgroundHover, resourceBackground;
		private String nuclosResourceBackground;
		private UID resourceBackgroundNorthWest, resourceBackgroundNorth, resourceBackgroundNorthEast;
		private UID resourceBackgroundWest, resourceBackgroundCenter, resourceBackgroundEast;
		private UID resourceBackgroundSouthWest, resourceBackgroundSouth, resourceBackgroundSouthEast;
		private boolean hideToolBar = false;
		private boolean hideTabBar = false;
		private boolean rootpaneBackgroundColor = false;
		private boolean staticMenu = false;
		private List<DesktopItem> desktopItems;
		public int getHorizontalGap() {
			return horizontalGap;
		}
		public void setHorizontalGap(Integer horizontalGap) {
			if (horizontalGap==null) horizontalGap=0;
			this.horizontalGap = horizontalGap;
		}
		public int getLayout() {
			return layout;
		}
		public void setLayout(Integer layout) {
			if (layout==null) layout=0;
			this.layout = layout;
		}
		public int getVerticalGap() {
			return verticalGap;
		}
		public void setVerticalGap(Integer verticalGap) {
			if (verticalGap==null) verticalGap=0;
			this.verticalGap = verticalGap;
		}
		public int getMenuItemTextSize() {
			return menuItemTextSize;
		}
		public void setMenuItemTextSize(Integer menuItemTextSize) {
			if (menuItemTextSize==null) menuItemTextSize=0;
			this.menuItemTextSize = menuItemTextSize;
		}
		public int getMenuItemTextHorizontalPadding() {
			return menuItemTextHorizontalPadding;
		}
		public void setMenuItemTextHorizontalPadding(Integer menuItemTextHorizontalPadding) {
			if (menuItemTextHorizontalPadding==null) menuItemTextHorizontalPadding=0;
			this.menuItemTextHorizontalPadding = menuItemTextHorizontalPadding;
		}
		public int getMenuItemTextHorizontalAlignment() {
			return menuItemTextHorizontalAlignment;
		}
		public void setMenuItemTextHorizontalAlignment(
				Integer menuItemTextHorizontalAlignment) {
			if (menuItemTextHorizontalAlignment==null) menuItemTextHorizontalAlignment=0;
			this.menuItemTextHorizontalAlignment = menuItemTextHorizontalAlignment;
		}
		public Color getMenuItemTextColor() {
			return menuItemTextColor;
		}
		public void setMenuItemTextColor(Color menuItemTextColor) {
			this.menuItemTextColor = menuItemTextColor;
		}
		public void setMenuItemTextColor(java.awt.Color menuItemTextColor) {
			if (menuItemTextColor == null) {
				this.menuItemTextColor = null;
			} else {
				this.menuItemTextColor = new Color(menuItemTextColor.getRed(), menuItemTextColor.getGreen(), menuItemTextColor.getBlue());
			}
		}
		public Color getMenuItemTextHoverColor() {
			return menuItemTextHoverColor;
		}
		public void setMenuItemTextHoverColor(Color menuItemTextHoverColor) {
			this.menuItemTextHoverColor = menuItemTextHoverColor;
		}
		public void setMenuItemTextHoverColor(java.awt.Color menuItemTextHoverColor) {
			if (menuItemTextHoverColor == null) {
				this.menuItemTextHoverColor = null;
			} else {
				this.menuItemTextHoverColor = new Color(menuItemTextHoverColor.getRed(), menuItemTextHoverColor.getGreen(), menuItemTextHoverColor.getBlue());
			}
		}
		public UID getResourceMenuBackground() {
			return resourceMenuBackground;
		}
		public void setResourceMenuBackground(UID resourceMenuBackground) {
			this.resourceMenuBackground = resourceMenuBackground;
		}
		public UID getResourceMenuBackgroundHover() {
			return resourceMenuBackgroundHover;
		}
		public void setResourceMenuBackgroundHover(UID resourceMenuBackgroundHover) {
			this.resourceMenuBackgroundHover = resourceMenuBackgroundHover;
		}
		public UID getResourceBackground() {
			return resourceBackground;
		}
		public void setResourceBackground(UID resourceBackground) {
			this.resourceBackground = resourceBackground;
		}
		public UID getResourceBackgroundNorthWest() {
			return resourceBackgroundNorthWest;
		}
		public void setResourceBackgroundNorthWest(UID resourceBackgroundNorthWest) {
			this.resourceBackgroundNorthWest = resourceBackgroundNorthWest;
		}
		public UID getResourceBackgroundNorth() {
			return resourceBackgroundNorth;
		}
		public void setResourceBackgroundNorth(UID resourceBackgroundNorth) {
			this.resourceBackgroundNorth = resourceBackgroundNorth;
		}
		public UID getResourceBackgroundNorthEast() {
			return resourceBackgroundNorthEast;
		}
		public void setResourceBackgroundNorthEast(UID resourceBackgroundNorthEast) {
			this.resourceBackgroundNorthEast = resourceBackgroundNorthEast;
		}
		public UID getResourceBackgroundWest() {
			return resourceBackgroundWest;
		}
		public void setResourceBackgroundWest(UID resourceBackgroundWest) {
			this.resourceBackgroundWest = resourceBackgroundWest;
		}
		public UID getResourceBackgroundCenter() {
			return resourceBackgroundCenter;
		}
		public void setResourceBackgroundCenter(UID resourceBackgroundCenter) {
			this.resourceBackgroundCenter = resourceBackgroundCenter;
		}
		public UID getResourceBackgroundEast() {
			return resourceBackgroundEast;
		}
		public void setResourceBackgroundEast(UID resourceBackgroundEast) {
			this.resourceBackgroundEast = resourceBackgroundEast;
		}
		public UID getResourceBackgroundSouthWest() {
			return resourceBackgroundSouthWest;
		}
		public void setResourceBackgroundSouthWest(UID resourceBackgroundSouthWest) {
			this.resourceBackgroundSouthWest = resourceBackgroundSouthWest;
		}
		public UID getResourceBackgroundSouth() {
			return resourceBackgroundSouth;
		}
		public void setResourceBackgroundSouth(UID resourceBackgroundSouth) {
			this.resourceBackgroundSouth = resourceBackgroundSouth;
		}
		public UID getResourceBackgroundSouthEast() {
			return resourceBackgroundSouthEast;
		}
		public void setResourceBackgroundSouthEast(UID resourceBackgroundSouthEast) {
			this.resourceBackgroundSouthEast = resourceBackgroundSouthEast;
		}
		public String getNuclosResourceBackground() {
			return nuclosResourceBackground;
		}
		public void setNuclosResourceBackground(String nuclosResourceBackground) {
			this.nuclosResourceBackground = nuclosResourceBackground;
		}
		public boolean isHideToolBar() {
			return hideToolBar;
		}
		public void setHideToolBar(boolean hideToolBar) {
			this.hideToolBar = hideToolBar;
		}
		public boolean isHideTabBar() {
			return hideTabBar;
		}
		public void setHideTabBar(boolean hideTabBar) {
			this.hideTabBar = hideTabBar;
		}
		public boolean isRootpaneBackgroundColor() {
			return rootpaneBackgroundColor;
		}
		public void setRootpaneBackgroundColor(boolean rootpaneBackgroundColor) {
			this.rootpaneBackgroundColor = rootpaneBackgroundColor;
		}
		public boolean isStaticMenu() {
			return staticMenu;
		}
		public void setStaticMenu(boolean staticMenu) {
			this.staticMenu = staticMenu;
		}
		private List<DesktopItem> _getDesktopItems() {
			if (desktopItems == null)
				desktopItems = new ArrayList<DesktopItem>();
			return desktopItems;
		}
		public List<DesktopItem> getDesktopItems() {
			return new ArrayList<DesktopItem>(_getDesktopItems());
		}
		public void addDesktopItem(DesktopItem di) {
			_getDesktopItems().add(di);
		}
		public void addDesktopItem(int index, DesktopItem di) {
			_getDesktopItems().add(index, di);
		}
		public void addAllDesktopItems(List<DesktopItem> dis) {
			_getDesktopItems().addAll(dis);
		}
		public boolean removeDesktopItem(DesktopItem di) {
			return _getDesktopItems().remove(di);
		}
		public void removeAllDesktopItems() {
			_getDesktopItems().clear();
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj instanceof Desktop) {
				Desktop other = (Desktop) obj;
				return RigidUtils.equal(this.desktopItems, other.desktopItems) &&
						this.horizontalGap == other.horizontalGap &&
						this.verticalGap == other.verticalGap &&
						this.menuItemTextSize == other.menuItemTextSize &&
						this.layout == other.layout &&
						RigidUtils.equal(this.resourceMenuBackground, other.resourceMenuBackground) &&
						RigidUtils.equal(this.resourceMenuBackgroundHover, other.resourceMenuBackgroundHover) &&
						RigidUtils.equal(this.menuItemTextColor, other.menuItemTextColor) &&
						RigidUtils.equal(this.menuItemTextHoverColor, other.menuItemTextHoverColor);
			}
			return super.equals(obj);
		}
	}

	@Override
	public int hashCode() {
		if (name == null)
			return 0;
		return name.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof WO_WorkspaceDescriptionNew) {
			WO_WorkspaceDescriptionNew other = (WO_WorkspaceDescriptionNew) obj;
			RigidUtils.equal(getName(), other.getName());
		}
		return super.equals(obj);
	}
	
	@Override
	public String toString() {
		if (name == null)
			return "null";
		return name.toString();
	}

	/**
	 *
	 * @param wd
	 */
	public void importHeader(WO_WorkspaceDescriptionNew wd) {
		setName(wd.getName());
		setHide(wd.isHide());
		setHideName(wd.isHideName());
		setHideMenuBar(wd.isHideMenuBar());
		setAlwaysOpenAtLogin(wd.isAlwaysOpenAtLogin());
		setUseLastFrameSettings(wd.isUseLastFrameSettings());
		setNuclosResource(wd.getNuclosResource());
		setAlwaysReset(wd.isAlwaysReset());
	}

	/**
	 *
	 * @return
	 */
	public Frame getMainFrame() {
		for (Frame f : frames) {
			if (f.isMainFrame())
				return f;
		}
		throw new CommonFatalException("No main frame in workspace description");
	}

	public List<Tabbed> getTabbeds() {
		List<Tabbed> result = new ArrayList<Tabbed>();
		for (Frame f : getFrames()) {
			result.addAll(getTabbeds(f.getContent()));
		}
		return result;
	}

	public static List<Tabbed> getTabbeds(NestedContent nc) {
		List<Tabbed> result = new ArrayList<Tabbed>();
		if (nc instanceof MutableContent) {
			result.addAll(getTabbeds(((MutableContent) nc).getContent()));
		} else if (nc instanceof Split) {
			result.addAll(getTabbeds(((Split) nc).getContentA()));
			result.addAll(getTabbeds(((Split) nc).getContentB()));
		} else if (nc instanceof Tabbed) {
			result.add((Tabbed) nc);
		}
		return result;
	}

	public Tabbed getHomeTabbed() throws WorkspaceException {
		for (Tabbed tbb : getTabbeds()) {
			if (tbb.isHome()) {
				return tbb;
			}
		}
		throw new WorkspaceException("Workspace.contains.no.home.tabbed");
	}

	public Tabbed getHomeTreeTabbed() throws WorkspaceException {
		for (Tabbed tbb : getTabbeds()) {
			if (tbb.isHomeTree()) {
				return tbb;
			}
		}
		throw new WorkspaceException("Workspace.contains.no.home.tabbed");
	}

	public static class TasklistPreferences implements Serializable {
		
		private static final long serialVersionUID = -1237247495790530796L;
		
		public static final int GENERIC = -1;
		public static final int PERSONAL = 1;
		public static final int TIMELIMIT = 2;
		public static final int DYNAMIC = 3;
		
		private int type;
		private String name;
		private TablePreferences tablePreferences;
		
		public int getType() {
			return type;
		}
		
		public void setType(Integer type) {
			if(type==null) type=0;
			this.type = type;
		}
		
		public String getName() {
			return name;
		}
		
		public void setName(String name) {
			this.name = name;
		}
		
		private TablePreferences _getTablePreferences() {
			if (this.tablePreferences == null)
				this.tablePreferences = new TablePreferences();
			return this.tablePreferences;
		}
		public TablePreferences getTablePreferences() {
			return this._getTablePreferences();
		}

		public void clearTablePreferences() {
			this._getTablePreferences().clear();
		}
		
		public void setTablePreferences(TablePreferences tablePreferences) {
			this.tablePreferences = tablePreferences;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			result = prime * result + type;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			TasklistPreferences other = (TasklistPreferences) obj;
			if (name == null) {
				if (other.name != null)
					return false;
			}
			else if (!name.equals(other.name))
				return false;
			if (type != other.type)
				return false;
			return true;
		}
	}
	
public static class MatrixPreferences implements Serializable {
		
	private Boolean showOnly;
	private UID name;
	private SortedSet<String> available;
	private List<String> selected;
	private List<Pair<Integer, Integer>> sortKeys;
	private Map<UID, Integer> fixedFieldWidths;
	private SortedSet<UID> availablefixed;
	private List<UID> selectedfixed;
	private Map<String, List<String>> selectedGroupElements;
	private Map<String, SortedSet<String>> availableGroupElements;
	
	public Boolean getShowOnly() {
		return showOnly;
	}
	
	public void setShowOnly(Boolean showOnly) {
		this.showOnly = showOnly;
	}
	
	public void setFixedFieldWidths(Map<UID, Integer> fixedFieldWidths) {
		this.fixedFieldWidths = fixedFieldWidths;
	}
	
	public Map<UID, Integer> getFixedFieldWidths() {
		return this.fixedFieldWidths;
	}
	
	public void setSortKeys(List<Pair<Integer, Integer>> keys) {
		this.sortKeys = keys;
	}
	
	public List<Pair<Integer, Integer>> getSortKeys() {
		return this.sortKeys;
	}
	
	public UID getName() {
		return name;
	}
	
	public void setName(UID name) {
		this.name = name;
	}
	
	public SortedSet<String> getAvailable() {
		return available;
	}
	
	public void setAvailable(SortedSet<String> available) {
		this.available = available;
	}
	
	public List<String> getSelected() {
		return selected;
	}
	
	public void setSelected(List<String> selected) {
		this.selected = selected;
	}
	
	public SortedSet<UID> getAvailablefixed() {
		return availablefixed;
	}
	
	public void setAvailablefixed(SortedSet<UID> available) {
		this.availablefixed = available;
	}
	
	public List<UID> getSelectedfixed() {
		return selectedfixed;
	}
	
	public void setSelectedfixed(List<UID> selected) {
		this.selectedfixed = selected;
	}	
	
	public Map<String, List<String>> getSelectedGroupElements() {
		return this.selectedGroupElements;
	}
	
	public void setSelectedGroupElements(Map<String, List<String>> map) {
		this.selectedGroupElements = map;
	}
	
	public Map<String, SortedSet<String>> getAvailableGroupElements() {
		return this.availableGroupElements;
	}
	
	public void setAvailableGroupElements(Map<String, SortedSet<String>> map) {
		this.availableGroupElements = map;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((available == null) ? 0 : available.hashCode());
		result = prime
				* result
				+ ((availableGroupElements == null) ? 0
						: availableGroupElements.hashCode());
		result = prime
				* result
				+ ((availablefixed == null) ? 0 : availablefixed.hashCode());
		result = prime
				* result
				+ ((fixedFieldWidths == null) ? 0 : fixedFieldWidths
						.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((selected == null) ? 0 : selected.hashCode());
		result = prime
				* result
				+ ((selectedGroupElements == null) ? 0
						: selectedGroupElements.hashCode());
		result = prime * result
				+ ((selectedfixed == null) ? 0 : selectedfixed.hashCode());
		result = prime * result
				+ ((showOnly == null) ? 0 : showOnly.hashCode());
		result = prime * result
				+ ((sortKeys == null) ? 0 : sortKeys.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MatrixPreferences other = (MatrixPreferences) obj;
		if (available == null) {
			if (other.available != null)
				return false;
		} else if (!available.equals(other.available))
			return false;
		if (availableGroupElements == null) {
			if (other.availableGroupElements != null)
				return false;
		} else if (!availableGroupElements
				.equals(other.availableGroupElements))
			return false;
		if (availablefixed == null) {
			if (other.availablefixed != null)
				return false;
		} else if (!availablefixed.equals(other.availablefixed))
			return false;
		if (fixedFieldWidths == null) {
			if (other.fixedFieldWidths != null)
				return false;
		} else if (!fixedFieldWidths.equals(other.fixedFieldWidths))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (selected == null) {
			if (other.selected != null)
				return false;
		} else if (!selected.equals(other.selected))
			return false;
		if (selectedGroupElements == null) {
			if (other.selectedGroupElements != null)
				return false;
		} else if (!selectedGroupElements
				.equals(other.selectedGroupElements))
			return false;
		if (selectedfixed == null) {
			if (other.selectedfixed != null)
				return false;
		} else if (!selectedfixed.equals(other.selectedfixed))
			return false;
		if (showOnly == null) {
			if (other.showOnly != null)
				return false;
		} else if (!showOnly.equals(other.showOnly))
			return false;
		if (sortKeys == null) {
			if (other.sortKeys != null)
				return false;
		} else if (!sortKeys.equals(other.sortKeys))
			return false;
		return true;
	}
		
		
	}
}
