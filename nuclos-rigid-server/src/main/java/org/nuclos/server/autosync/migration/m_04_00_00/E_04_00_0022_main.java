package org.nuclos.server.autosync.migration.m_04_00_00;

import java.util.Collection;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.UID;
import org.nuclos.common.dblayer.Schema;
import org.nuclos.common.dblayer.SchemaHelperVersion;


@Schema (version="4.00.0000", helper = SchemaHelperVersion.V_4_0)
class E_04_00_0022_main extends E_04_00_0022_finish {
	
	private final static E_04_00_0022_main THIS = new E_04_00_0022_main();
	
	public static String getSchemaVersion() {
		return THIS._getSchemaVersion();
	}
	
	public static Collection<EntityMeta<?>> getAllEntities() {
		return THIS._getAllEntities();
	}

	public static <PK> EntityMeta<PK> getByUID(UID entityUID) {
		return THIS._getByUID(entityUID);
	}

	public static boolean isNuclosEntity(UID entityUID) {
		return THIS._isNuclosEntity(entityUID);
	}

	public static <PK> EntityMeta<PK> getByName(String entityName) {
		return THIS._getByName(entityName);
	}
	
	public static E_04_00_0022_main getThis() {
		return THIS;
	}
	
}
