//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dblayer;

import java.util.Collections;
import java.util.List;

import org.nuclos.common.DbField;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.NuclosImage;
import org.nuclos.common.NuclosPassword;
import org.nuclos.common.NuclosScript;
import org.nuclos.common.SF;
import org.nuclos.common.SimpleDbField;
import org.nuclos.common.UID;
import org.nuclos.common.report.ByteArrayCarrier;
import org.nuclos.common2.DateTime;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.server.common.valueobject.DocumentFileBase;
import org.nuclos.server.dblayer.statements.DbStructureChange;
import org.nuclos.server.dblayer.structure.DbColumnType;
import org.nuclos.server.dblayer.structure.DbColumnType.DbGenericType;
import org.nuclos.server.dblayer.structure.DbTable;

public class DbUtils {
	
	private DbUtils() {
		// Never invoked.
	}

	public static String getDbIdFieldName(FieldMeta field, boolean isUidEntity) {
		return getDbIdFieldName(field.getDbColumn(), field.getUnreferencedForeignEntity(), field.getUnreferencedForeignEntityField(), isUidEntity);
	}
	
	public static String getDbIdFieldName(MetaDbFieldWrapper field,	boolean isUidEntity) {
		return getDbIdFieldName(field.getDbColumn(), field.getUnreferencedForeignEntity(), field.getUnreferencedForeignEntityField(), isUidEntity);
	}
	
	public static String getDbIdFieldName(String dbColumn, UID unreferencedForeignEntity, String unreferencedForeignEntityField, boolean isUidEntity) {
		if (unreferencedForeignEntity != null && unreferencedForeignEntityField == null) {
			return dbColumn;
		}

		final String uFieldName = dbColumn.toUpperCase();

		// Already a (U)ID field name?
		if (uFieldName.equals("STRUID") || uFieldName.equals("INTID")) {
			return uFieldName;
		}

		final int index_ = uFieldName.indexOf("_");
		if (index_ < 0) {
			return (isUidEntity ? "STRUID_" : "INTID_") + uFieldName;
		}
		return (isUidEntity?"STRUID":"INTID")+uFieldName.substring(index_);
	}

	public static boolean isDbIdField(String fieldName) {
		final String uFieldName = fieldName.toUpperCase();
		return uFieldName.startsWith("INTID_") || 
				uFieldName.startsWith("STRUID_") ;
	}
	
	public static Class<?> getDbType(Class<?> javaType) {
		if (javaType == Object.class) {
			javaType = byte[].class; // Column stores data (blob)
		} else if (NuclosPassword.class == javaType) {
			return String.class;
		}
		return javaType;
	}

	public static Class<?> getDbType(String javaType) {
		try {
			return getDbType(Class.forName(javaType));
		}
		catch (ClassNotFoundException e) {
			throw new IllegalArgumentException(javaType);
		}
	}
	
	public static DbColumnType getDbColumnType(Class<?> javaClass, Integer oldScale, Integer oldPrecision, boolean isLocalized) {
		javaClass = getDbType(javaClass);
		DbGenericType genericType;
		Integer length = null, scale = null, precision = null;
		if (javaClass == String.class && oldScale == null) {
			if (isLocalized) {
				genericType = DbGenericType.NCLOB;
			} else {
				genericType = DbGenericType.CLOB;
			}
		} else if (javaClass == String.class) {
			if (isLocalized) {
				genericType = DbGenericType.NVARCHAR;	
			} else {
				genericType = DbGenericType.VARCHAR;				
			}
			length = oldScale;
		} else if (DocumentFileBase.class.isAssignableFrom(javaClass)) {
			if (oldScale == null) {
				if (isLocalized) {
					genericType = DbGenericType.NCLOB;
				} else {
					genericType = DbGenericType.CLOB;
				}
				
			} else {
				genericType = DbGenericType.VARCHAR;
				length = oldScale;
			}
		} else if (javaClass == UID.class) {
			genericType = DbGenericType.VARCHAR;
			length = SF.UID_SCALE;
		} else if (javaClass == Integer.class || javaClass == Long.class) {
			genericType = DbGenericType.NUMERIC;
			precision = oldScale;
			scale = 0;
		} else if (javaClass == Double.class) {
			genericType = DbGenericType.NUMERIC;
			precision = oldScale;
			scale = oldPrecision;
		} else if (javaClass == Boolean.class) {
			genericType = DbGenericType.BOOLEAN;
		} else if (javaClass == java.util.Date.class || javaClass == java.sql.Date.class || javaClass == java.sql.Timestamp.class) {
			genericType = DbGenericType.DATE;
		} else if (InternalTimestamp.class.isAssignableFrom(javaClass) || javaClass == DateTime.class) {
			genericType = DbGenericType.DATETIME;
		} else if (javaClass == byte[].class || javaClass == ByteArrayCarrier.class || javaClass == NuclosImage.class) {
			genericType = DbGenericType.BLOB;
		} else if (javaClass == NuclosPassword.class) {
			genericType = DbGenericType.VARCHAR;
//			length = CryptUtil.calcSizeForAESHexInputLength(oldScale);
		} else if (javaClass == NuclosScript.class) {
			genericType = DbGenericType.CLOB;
		} else if (javaClass == javax.json.JsonObject.class) {
			genericType = DbGenericType.CLOB;
		} else {
			throw new IllegalArgumentException("Unsupported DB column type mapping for " + javaClass);
		}
		return new DbColumnType(genericType, null, length, precision, scale);
	}

	public static DbColumnType getDbColumnType(MetaDbFieldWrapper meta) {
		try {
			return getDbColumnType(Class.forName(meta.getDataType()), meta.getScale(), meta.getPrecision(), meta.isLocalized());
		}
		catch (ClassNotFoundException e) {
			throw new NuclosFatalException(e);
		}
	}

	public static DbColumnType getDbColumnType(FieldMeta<?> meta) {
		try {
			return getDbColumnType(Class.forName(meta.getDataType()), meta.getScale(), meta.getPrecision(), meta.isLocalized());
		}
		catch (ClassNotFoundException e) {
			throw new NuclosFatalException(e);
		}
	}
	
	public static void logDMLError(StringBuffer sbErrorMessage, String error, List<?> statements) {
		logError(sbErrorMessage, error, statements, "DML");
	}

	public static void logDDLError(StringBuffer sbErrorMessage, String error, List<?> statements) {
		logError(sbErrorMessage, error, statements, "DDL");
	}

	public static void logError(StringBuffer sbErrorMessage, String error, List<?> statements, String errorType) {
		sbErrorMessage.append("<br />-------------------- "+ errorType + " Error --------------------");
		sbErrorMessage.append("<br />" + error);
		if (statements == null)
			return;
		sbErrorMessage.append("<br />"+ errorType + " Statement(s) --------------------");
		for (Object sql : statements)
			sbErrorMessage.append("<br />" + sql);
	}
	
	public static void renameTable(PersistentDbAccess dbAccess, String currentName, String newName) {
		DbTable currentTable = new DbTable(null, currentName, Collections.EMPTY_LIST);
		DbTable newTable = new DbTable(null, newName, Collections.EMPTY_LIST);
		DbStructureChange rename = new DbStructureChange(DbStructureChange.Type.MODIFY, currentTable, newTable);
		dbAccess.execute(rename);
	}
	
	public static DbField<UID> newSimpleUidDbField(DbField<?> field) {
		if (field instanceof FieldMeta) {
			return newSimpleUidDbField((FieldMeta<?>) field);
		}
		return SimpleDbField.create(field.getDbColumn(), UID.class);
	}

	public static DbField<Long> newSimpleLongDbField(DbField<?> field) {
		if (field instanceof FieldMeta) {
			return newSimpleLongDbField((FieldMeta<?>) field);
		}
		return SimpleDbField.create(field.getDbColumn(), Long.class);
	}

	public static DbField<UID> newSimpleUidDbField(FieldMeta<?> field) {
		return SimpleDbField.create(getDbIdFieldName(field, true), UID.class);
	}

	public static DbField<Long> newSimpleLongDbField(FieldMeta<?> field) {
		return SimpleDbField.create(getDbIdFieldName(field, false), Long.class);
	}

}
