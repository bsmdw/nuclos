package org.nuclos.server.dblayer.incubator;

import java.util.Arrays;

public class SQLInfo {
	private String sql;
	private Object[] parameters;
	
	public SQLInfo(String sql) {
		this.sql = sql;
		this.parameters = null;
	}
	
	public SQLInfo(String sql, Object...parameters) {
		this.sql = sql;
		this.parameters = parameters;
	}
	
	public boolean isInsertUpdateDelete() {
		if (sql == null || sql.length() < 7) {
			return false;
		}
		// TODO: Unreliable. Does not work for common table expressions, leading whitespace characters, etc.
		String test = sql.substring(0, 7);
		return test.equalsIgnoreCase("INSERT ")
				|| test.equalsIgnoreCase("UPDATE ")
				|| test.equalsIgnoreCase("DELETE ");
	}
	
	public String toString() {
		return sql + "\n=" + (parameters != null ? Arrays.asList(parameters) : "[]");
	}
	
	public String toString(long time) {
		return this + "=(" + time + " ms)";
    }
}
