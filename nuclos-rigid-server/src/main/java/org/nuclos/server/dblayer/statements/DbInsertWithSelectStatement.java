package org.nuclos.server.dblayer.statements;

import java.sql.SQLException;
import java.util.Set;

import org.nuclos.common.DbField;

public class DbInsertWithSelectStatement<PK> extends DbTableStatement<PK> {

	private final String dbSourceEntity;
	private final DbMap columnMap;
	private final DbMap columnConditionMap;
	
	public DbInsertWithSelectStatement(String dbEntity, String dbSourceEntity, 
			DbMap columnMap, DbMap columnConditionMap) {
		
		super(dbEntity);
		this.dbSourceEntity = dbSourceEntity;
		this.columnMap = columnMap;
		this.columnConditionMap= columnConditionMap;
	}
	
	public String getDbEntitySource() {
		return dbSourceEntity;
	}
	
	public Set<? extends DbField> getColumns() {
		return columnMap.keySet();
	}

	public DbMap getColumnMap() {
		return columnMap;
	}
	
	public DbMap getColumnConditionMap() {
		return columnConditionMap;
	}
	
	@Override
	public <T> T accept(DbStatementVisitor<T> visitor) throws SQLException {
		return visitor.visitInsertWithSelect(this);
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("entity=").append(getDbEntity());
		result.append("entitySource=").append(getDbEntitySource());
		result.append(", fields=").append(columnMap.keySet());
		result.append(", fieldsSource=").append(columnMap.values());
		result.append("]");
		return result.toString();
	}
	
}
