//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.server.dblayer.util;

import java.security.SecureRandom;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.nuclos.common.UID;

public class DbObjectUtils {
	
	private static SecureRandom random = new SecureRandom();

	public static String replaceName(String inString, String searchFor, String replacement) {
		final String start 	= "[ \\.,;:()=\\-+*/{}\\[\\]!\\\"'$%&<>|\t\r\n]{1}";
		final String end 	= "[ \\.,;:()=\\-+*/{}\\[\\]!\\\"'$%&<>|\t\r\n]{1}";
		final String eof 	= "\\z";
		
		if (inString == null) {
			return null;
		}
		if (inString.equalsIgnoreCase(searchFor)) {
			return replacement;
		}
		StringBuffer sb = new StringBuffer(inString.length());
		String result;
		
		Matcher m = Pattern.compile(start + searchFor + end, java.util.regex.Pattern.CASE_INSENSITIVE).matcher(inString);
		int index = 0;
		while (m.find()) {
			sb.append(inString.substring(index, m.start()+1));
			sb.append(replacement);
			index = m.start()+1+searchFor.length();
		}
		if (index < inString.length()) {
			sb.append(inString.substring(index));
		}
		result = sb.toString();
		
		m = Pattern.compile(start + searchFor + eof, java.util.regex.Pattern.CASE_INSENSITIVE).matcher(result);
		index = 0;
		if (m.find()) {
			sb = new StringBuffer(result.length());
			sb.append(result.substring(index, m.start()+1));
			sb.append(replacement);
			result = sb.toString();
		}
		
		return result;
	}
	
	public static interface LocalIdentifierStore {
		
		public boolean exist(String li);
		
		public void set(UID nucletUID, String li);
		
	}
	
	public static String createUniqueLocalIdentifier(UID nucletUID, LocalIdentifierStore liStore) {
		String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String digits = "0123456789";
		String all = characters+digits;
		int length = 4;
		int digit = random.nextInt(length-1)+1; // at least one digit
		char[] text = new char[length];
		text[0] = characters.charAt(random.nextInt(characters.length())); // first not a digit
	    for (int i = 1; i < length; i++) {
	    	if (digit == i) {
	    		text[i] = digits.charAt(random.nextInt(digits.length()));
	    	} else {
	    		text[i] = all.charAt(random.nextInt(all.length()));
	    	}
	    }
	    String result = new String(text);
		if (liStore.exist(result)) {
			return createUniqueLocalIdentifier(nucletUID, liStore);
		} else {
			liStore.set(nucletUID, result);
			return result;
		}
	}
	
	public static void main(String[] args) {
		System.out.println(replaceName(
				"SELECT (MAX(TO_NUMBER(substr(STRAUFTRAGSNR,4,6), '000000')))+1::integer \"number\""
				+ "\n  FROM SCHEMA.T_EO_AUFTRAG", 
				"T_EO_AUFTRAG", "C3PO_AUFTRAG"));
		System.out.println(replaceName(
				"SELECT (MAX(TO_NUMBER(substr(STRAUFTRAGSNR,4,6), '000000')))+1::integer \"number\""
				+ "\n  FROM SCHEMA.T_EO_AUFTRAG WHERE 1=1", 
				"T_EO_AUFTRAG", "C3PO_AUFTRAG"));
	}
	
}
