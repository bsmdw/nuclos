//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dblayer;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.ForwardOnlyResultSetTableFactory;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.IMetadataHandler;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.datatype.IDataTypeFactory;
import org.dbunit.dataset.stream.IDataSetProducer;
import org.dbunit.dataset.xml.XmlDataSet;
import org.dbunit.dataset.xml.XmlProducer;
import org.dbunit.operation.DatabaseOperation;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.DalCallResult;
import org.nuclos.common.dblayer.ResultSetMetaDataTransformer;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common2.BoDataSet;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.common.MyDataBaseConnection;
import org.nuclos.server.common.MyDataBaseConnection.IStreamingFilterProvider;
import org.nuclos.server.common.MyStreamingDataSet;
import org.nuclos.server.dblayer.impl.Base32;
import org.nuclos.server.dblayer.incubator.DbExecutor;
import org.nuclos.server.dblayer.incubator.DbExecutor.ConnectionRunner;
import org.nuclos.server.dblayer.incubator.DbExecutor.ResultSetRunner;
import org.nuclos.server.dblayer.incubator.SQLInfo;
import org.nuclos.server.dblayer.query.DbDelete;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.query.DbUpdate;
import org.nuclos.server.dblayer.statements.DbBuildableStatement;
import org.nuclos.server.dblayer.statements.DbStatement;
import org.nuclos.server.dblayer.structure.DbArtifact;
import org.nuclos.server.dblayer.structure.DbColumnType;
import org.nuclos.server.dblayer.structure.DbTable;
import org.nuclos.server.dblayer.structure.DbTableType;
import org.xml.sax.InputSource;

public abstract class DbAccess {

	public static final String SCHEMA = "schema";
	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";
	public static final String TABLESPACE = "tablespace";
	public static final String TABLESPACE_INDEX = "tablespace.index";
	public static final String STRUCTURE_CHANGELOG_DIR = "structure.changelog.dir";
	
	private static final Logger LOG = Logger.getLogger(DbAccess.class);

	protected DbType type;
	protected String catalog = null;
	protected String schema;
	protected DataSource dataSource;
	protected DbExecutor executor;
	protected Map<String, String> config;
	protected File structureChangeLogDir;

	protected DbAccess() {
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("type=").append(type);
		result.append(", schema=").append(schema);
		result.append(", catalog=").append(catalog);
		result.append(", ds=").append(dataSource);
		result.append(", ex=").append(executor);
		result.append("]");
		return result.toString();
	}

	public void init(DbType type, DataSource dataSource, Map<String, String> config) {
		this.type = type;
		this.config = new HashMap<String, String>(config);
		// this.executor = new DataSourceExecutor(dataSource, config.get(USERNAME), config.get(PASSWORD));
		if (config.containsKey(STRUCTURE_CHANGELOG_DIR)) {
			structureChangeLogDir = new File(config.get(STRUCTURE_CHANGELOG_DIR));
		}
		this.schema = resolveSchema(config);
	}

	public final DbExecutor getDbExecutor() {
		return executor;
	}

	protected String resolveSchema(Map<String, String> config) {
		final String givenSchema;
		if (!StringUtils.isBlank(config.get(SCHEMA))) {
			givenSchema = config.get(SCHEMA);
		} else {
			givenSchema = config.get(USERNAME);
		}
		try {
			String resolvedSchema = executor.execute(new ConnectionRunner<String>() {
				@Override
				public String perform(Connection conn) throws SQLException {
					String resolvedSchemaName = null;
					ResultSet rs = conn.getMetaData().getSchemas();
					try {
						while (rs.next()) {
							String dbSchemaName = rs.getString("TABLE_SCHEM");
							if (givenSchema.equalsIgnoreCase(dbSchemaName)) {
								resolvedSchemaName = dbSchemaName;
								if (givenSchema.equals(dbSchemaName)) {
									break;
								}
							}
						}
						return resolvedSchemaName;
	                } catch (SQLException e) {
	            		LOG.error("resolveSchema failed with " + e.toString() + ":\n\t" + givenSchema);
	                	throw e;
					} finally {
						rs.close();
					}
				}

				@Override
				public SQLInfo getInfo() {
					return new SQLInfo("METADATA SCHEMAS");
				}
			});
			if (resolvedSchema != null) {
				LOG.info(String.format("Schema name resolved to '%s'", resolvedSchema));
				return resolvedSchema;
			} else {
				LOG.error(String.format("Schema name '%s' not found", givenSchema));
				return givenSchema;
			}
		} catch (SQLException e) {
			LOG.warn("Exception during resolving schema names", e);
			return givenSchema;
		}
	}

	public DbType getDbType() {
		return type;
	}

	public String getSchemaName() {
		return schema;
	}
	
	public abstract Object getBooleanRepresentationInSQL(Boolean b);
	
	/**
	 * Returns the configuration in use with username and password erased.
	 * @return
	 */
	public Map<String, String> getConfig() {
		HashMap<String, String> configCopy = new HashMap<String, String>(config);
		configCopy.remove(USERNAME);
		configCopy.remove(PASSWORD);
		return configCopy;
	}

	public void close() {
	}
	
	public <PK> Long estimateCount(EntityMeta<PK> eMeta, DbQuery<PK> query) {		
		return null;
	}

	//
	// Statements
	//

	public int execute(DbBuildableStatement statement) throws DbException {
		return execute(Collections.singletonList(statement));
	}

	/**
	 * Executes the given database operations.
	 * <p>
	 * Note: This method may be overridden in order to support batch execution.
	 * It is up to the implementation to find consecutive commands which
	 * can be executed in as batch statements.
	 */
	public abstract int execute(List<? extends DbBuildableStatement> statements) throws DbException;

	public final int execute(DbBuildableStatement statement1, DbBuildableStatement...statements) throws DbException {
		return execute(RigidUtils.asList(statement1, statements));
	}

	//
	// Queries
	//

	public abstract DbQueryBuilder getQueryBuilder();
	
	public abstract int executeDelete(DbDelete delete) throws DbException;
	
	public abstract int executeUpdate(DbUpdate update) throws DbException;

	public abstract <T> List<T> executeQuery(DbQuery<? extends T> query) throws DbException;

	public abstract <T, R> List<R> executeQuery(DbQuery<T> query, Transformer<? super T, R> transformer) throws DbException;

	/**
	 * Executes a query which returns exactly one row.
	 * @throws DbInvalidResultSizeException if no or more than one result rows are returned
	 */
	public abstract <T> T executeQuerySingleResult(DbQuery<? extends T> query) throws DbException;

	public <T, R> R executeQuerySingleResult(DbQuery<T> query, Transformer<? super T, R> transformer) throws DbException {
		return transformer.transform(executeQuerySingleResult(query));
	}

	public abstract int executePlainUpdate(final String sql) throws DbException;
	
	public abstract <T> T executePlainQuery(String sql, int maxRows, ResultSetRunner<T> runner) throws DbException;

	public abstract ResultVO executePlainQueryAsResultVO(String sql, int maxRows, final boolean DEPRECATED_2017_ALLOWED) throws DbException;

	/**
	 * @author Thomas Pasch
	 * @since Nuclos 3.2.0
	 */
    public abstract DalCallResult executeBatch(final IBatch batch, EBatchType type);

	/**
	 * @author Thomas Pasch
	 * @since Nuclos 3.2.0
	 */
    public abstract List<String> getStatementsForLogging(final IBatch batch);
    
	public <T> T executeFunction(String functionName, Class<T> result, Object...args) throws DbException {
		return executeFunction(functionName, false, result, args);
	}
	public abstract <T> T executeFunction(String functionName, boolean silent, Class<T> result, Object...args) throws DbException;

	public void executeProcedure(String procedureName, Object...args) throws DbException {
		executeProcedure(procedureName, false, args);
	}
	public abstract void executeProcedure(String procedureName, boolean silent, Object...args) throws DbException;

	//
	// Useful informational functions (e.g. for debugging)
	//

	public abstract IBatch getBatchFor(DbStatement stmt) throws SQLException;

	//
	// Database schema metadata
	//

	public abstract Set<String> getTableNames(DbTableType tableType) throws DbException;

	public abstract Set<String> getCallableNames() throws DbException;

	public abstract DbTable getTableMetaData(String tableName) throws DbException;

	public abstract Collection<DbArtifact> getAllMetaData() throws DbException;

	public abstract Map<String, Object> getMetaDataInfo() throws DbException;

	public abstract Map<String, String> getDatabaseParameters() throws DbException;

	/** Tries to (re)validate all invalid database objects. */
	public abstract boolean validateObjects() throws SQLException;

	/** Tries to validate the given sql string. */
	public abstract <T> T checkSyntax(final String sql, final ResultSetMetaDataTransformer<T> transformer);
	
	public abstract String getTableAliasing();

	/**
	 * Invalidates all assumptions about the database made by the access layer.
	 * Should be called after the database has been (structurally) modified
	 * by another process or thread.
	 */
	public void invalidate() throws DbException {
		// Currently, we do nothing...
	}

	/**
	 * Tries to disable all constraints and triggers in the database temporarily,
	 * then executes the given {@link Runnable}'s run method, and finally tries
	 * to re-enable the all disabled constraints and triggers again.
	 */
	public abstract void runWithDisabledChecksAndTriggers(Runnable runnable) throws SQLException;

	protected abstract String getDataType(DbColumnType columnType);

	protected abstract DbException wrapSQLException(Long id, String message, SQLException ex);
	
	protected String getConfigParameter(String name, String defaultValue) {
		String value = RigidUtils.nullIfEmpty(config.get(name));
		return (value != null) ? value : defaultValue;
	}

	private static final Charset UTF_8 = Charset.forName("UTF-8");

	/**
	 * Generate a name from a given prefix and base name and, optionally, from some additional
	 * affixes.  This generated name must obey name limitations of the database.
	 */
	public abstract String generateName(String prefix, String base, String...affixes);

	/**
	 * Generate a name from a given base name and, optionally, from some additional
	 * affixes with at most {@code max} characters.
	 *
	 * Normally (base name is given) the generated name consists of the 
	 * prefix + base name + '{@code _}' + a Base32-encoded hash of the 
	 * base name and the affixes.  If this generated string exceeds {@code max} characters, 
	 * the base name part is truncated.
	 *
	 * In this case the hash itself is the first 30 bits of the SHA-1 digest 
	 * generated from the UTF-8 encoded base name and affixes separated by a NUL byte 
	 * (30 bits corresponds to 6 characters in Base32).
	 * 
	 * Otherwise (base name is null) the generated name consists of the
	 * prefix + '{@code _}' + a Base32-encoded hash of the the affixes (truncated if generated 
	 * string exceeds {@code max} characters)
	 */
	public static String generateName(String prefix, String base, int max, String...affixes) {
		if (affixes.length == 0 && base == null) {
			throw new IllegalArgumentException("At least one of affixes or base must not be null");
		}
		if (prefix == null) {
			prefix = "";
		}
		if (affixes.length == 0 && base != null && (prefix+base).length() <= max) {
			return prefix + base;
		}
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-1");
		} catch(NoSuchAlgorithmException e) {
			throw new CommonFatalException("java.security.MessageDigest does not support SHA-1");
		}
		md.update((prefix + RigidUtils.defaultIfNull(base, "")).getBytes(UTF_8));
		for (String s : affixes) {
			md.update(new byte[1]);
			md.update(s.getBytes(UTF_8));
		}
		byte[] digest = md.digest();

		StringBuilder sb = new StringBuilder(prefix);
		if (base != null) {
			sb.append(base);
			if (sb.length() > max - 7) {
				sb.setLength(max - 8); sb.append('_');
			}
			sb.append('_').append(toBase32(digest), 0, 6);
		} else {
			sb.append('_').append(toBase32(digest));
			if (sb.length() > max) {
				sb.setLength(max);
			}
		}
		return sb.toString();
	}

	private static String toBase32(byte[] b) {
		return Base32.encode(b);
	}

	public String getWildcardLikeSearchChar() {
		return "%";
	}
	
	public void shutdown() {
	}
	
	protected abstract Integer getDefaultFetchSize();
	
	protected abstract IDataTypeFactory getDataTypeFactory();
	
	protected abstract IMetadataHandler getMetadataHandler();
	
	public abstract void resetSequenceToValue(String sequenceName, long value);
	
	public abstract void cancelRunningStatements(Integer intitiatorId);
	
    public boolean supportsSubqueryPaging() {
    	return false;
    }
	
	private IDatabaseConnection getDbUnitConnection(boolean bCreateOwn) throws SQLException, DatabaseUnitException {
		IDatabaseConnection connection = executor.getDbUnitConnection(getSchemaName(), bCreateOwn);
		
		connection.getConfig().setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, getDataTypeFactory());
		connection.getConfig().setProperty(DatabaseConfig.PROPERTY_METADATA_HANDLER, getMetadataHandler());
		connection.getConfig().setProperty(DatabaseConfig.PROPERTY_RESULTSET_TABLE_FACTORY, new ForwardOnlyResultSetTableFactory());
		if (getDefaultFetchSize() != null) {
			connection.getConfig().setProperty(DatabaseConfig.PROPERTY_FETCH_SIZE, getDefaultFetchSize());			
		}
		return connection;
	}
	
	public BoDataSet exportDatabase(Collection<String> tables) {
		String[] tableNames = tables.toArray(new String[tables.size()]);

		try {
			final IDatabaseConnection connection = getDbUnitConnection(true);
			//This one if important for Tables with a very large number of rows
			connection.getConnection().setAutoCommit(false);
			final IDataSet fullDataSet = connection.createDataSet(tableNames);
			
			return new BoDataSet() {
				
				@Override
				public void write(OutputStream output) throws IOException {
					try {
						XmlDataSet.write(fullDataSet, output);
						connection.close();
						
					} catch (DataSetException dse) {
						throw new IOException(dse);
						
					} catch (SQLException dse) {
						throw new IOException(dse);
						
					}
					
				}
			};
			
		} catch (SQLException sqlex) {
			throw new CommonFatalException(sqlex.getMessage(), sqlex);
			
		} catch (DatabaseUnitException dbu) {
			throw new CommonFatalException(dbu.getMessage(), dbu);
			
		}
	}
	
	public void importDatabase(InputStream is, IStreamingFilterProvider filterProvider) {		
		try {
			final IDatabaseConnection connection = getDbUnitConnection(false);
			filterProvider.setMyDataBaseConnection((MyDataBaseConnection)connection);

			InputSource source = new InputSource(is);
	        IDataSetProducer producer = new XmlProducer(source);
	        
	        IDataSet dataSet = new MyStreamingDataSet(producer, filterProvider);
	        
	        DatabaseOperation.INSERT.execute(connection, dataSet);
						
		} catch (SQLException sqlex) {
			throw new CommonFatalException(sqlex.getMessage(), sqlex);
			
		} catch (DatabaseUnitException dbu) {
			throw new CommonFatalException(dbu.getMessage(), dbu);
			
		}
		
	}
	
	public SchemaDiff getSchemaDiff(Map<String, DbTable> schema) {
		try {
			
			IDatabaseConnection connection = getDbUnitConnection(false);
			IDataSet dbDataSet = connection.createDataSet();
			
	        Set<String> dbTables = new HashSet<String>();
	        for (String table : dbDataSet.getTableNames()) {
	        	dbTables.add(table.toUpperCase());
	        }

	        Set<String> metaTables = new HashSet<String>(schema.keySet());
	        
	        return new SchemaDiff(dbTables, metaTables);
						
		} catch (SQLException sqlex) {
			throw new CommonFatalException(sqlex.getMessage(), sqlex);
			
		} catch (DatabaseUnitException dbu) {
			throw new CommonFatalException(dbu.getMessage(), dbu);
			
		}
		
	}

 	public Long getMaxId(String tableName) {
 		String sql = "SELECT MAX(INTID) AS MAXID FROM " + tableName;
 		ResultVO resultVO = executePlainQueryAsResultVO(sql, 1, false);
 		Number result = (Number)resultVO.getRows().get(0)[0];
		return result.longValue();
 	}
 	
 	public boolean withOffsetZero() {
 		return true;
 	}

	/**
	 *
	 * @param s
	 * @return
	 */
	public String escapeLiteral(String s) throws SQLException {
    	return StringEscapeUtils.escapeSql(s);
	}
 	
}
