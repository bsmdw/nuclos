//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dblayer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.common.SysEntities;
import org.nuclos.common.UID;

public class MetaDbProvider {
	
	private final Map<UID, MetaDbEntityWrapper> mapMetaData = new LinkedHashMap<UID, MetaDbEntityWrapper>();
	private final Map<UID, MetaDbFieldWrapper> mapFieldMetaData = new LinkedHashMap<UID, MetaDbFieldWrapper>();
	private final Map<UID, Collection<MetaDbFieldWrapper>> mapFieldMetaDataByEntity = new LinkedHashMap<UID, Collection<MetaDbFieldWrapper>>();
	
	private final SysEntities sysEntities;
	private final IRigidMetaProvider originProvider;
	
	public MetaDbProvider(Collection<MetaDbEntityWrapper> entities, Collection<MetaDbFieldWrapper> fields, SysEntities sysEntities) {
		this.sysEntities = sysEntities;
		this.originProvider = null;
		
		for (MetaDbEntityWrapper eWrapper : entities) {
			final UID entityUID = eWrapper.getUID();
			mapMetaData.put(entityUID, eWrapper);
		}
		for (MetaDbFieldWrapper fWrapper : fields) {
			final UID entityUID = fWrapper.getEntity();
			Collection<MetaDbFieldWrapper> colFields = mapFieldMetaDataByEntity.get(entityUID);
			if (colFields == null) {
				colFields = new ArrayList<MetaDbFieldWrapper>();
				mapFieldMetaDataByEntity.put(entityUID, colFields);
			}
			colFields.add(fWrapper);
			mapFieldMetaData.put(fWrapper.getUID(), fWrapper);
		}
		
		for (EntityMeta<?> sysEntity : sysEntities._getAllEntities()) {
			final MetaDbEntityWrapper eWrapper = new MetaDbEntityWrapper(sysEntity);
			final UID entityUID = sysEntity.getUID();
			mapMetaData.put(entityUID, eWrapper);
			
			for (FieldMeta<?> sysField : sysEntity.getFields()) {
				final MetaDbFieldWrapper fWrapper = new MetaDbFieldWrapper(sysField);
				Collection<MetaDbFieldWrapper> colFields = mapFieldMetaDataByEntity.get(entityUID);
				if (colFields == null) {
					colFields = new ArrayList<MetaDbFieldWrapper>();
					mapFieldMetaDataByEntity.put(entityUID, colFields);
				}
				colFields.add(fWrapper);
				mapFieldMetaData.put(fWrapper.getUID(), fWrapper);
			}
		}
	}
	
	public MetaDbProvider(IRigidMetaProvider originProvider, final Collection<MetaDbEntityWrapper> additionalEntityWrapper, final Collection<MetaDbFieldWrapper> additionalFieldWrapper) {
		this.originProvider = originProvider;
		this.sysEntities = null;
		
		for (EntityMeta<?> entityMeta : originProvider.getAllEntities()) {
			final MetaDbEntityWrapper eWrapper = new MetaDbEntityWrapper(entityMeta);
			final UID entityUID = eWrapper.getUID();
			mapMetaData.put(entityUID, eWrapper);
			for (FieldMeta<?> fieldMeta : originProvider.getAllEntityFieldsByEntity(entityUID).values()) {
				final MetaDbFieldWrapper fWrapper = new MetaDbFieldWrapper(fieldMeta);
				Collection<MetaDbFieldWrapper> colFields = mapFieldMetaDataByEntity.get(entityUID);
				if (colFields == null) {
					colFields = new ArrayList<MetaDbFieldWrapper>();
					mapFieldMetaDataByEntity.put(entityUID, colFields);
				}
				colFields.add(fWrapper);
				mapFieldMetaData.put(fWrapper.getUID(), fWrapper);
			}
		}

		if (additionalEntityWrapper != null) {
			for (MetaDbEntityWrapper eWrapper : additionalEntityWrapper) {
				mapMetaData.put(eWrapper.getUID(), eWrapper);
			}
		}
		if (additionalFieldWrapper != null) {
			for (MetaDbFieldWrapper fWrapper : additionalFieldWrapper) {
				mapFieldMetaData.put(fWrapper.getUID(), fWrapper);
			}
		}
	}

	public Collection<MetaDbEntityWrapper> getAllEntities() {
		return mapMetaData.values();
	}

	public MetaDbEntityWrapper getEntity(UID entityUID) {
		return mapMetaData.get(entityUID);
	}
	
	public boolean hasEntity(UID entityUID) {
		return mapMetaData.keySet().contains(entityUID);
	}
	
	public Collection<MetaDbFieldWrapper> getAllEntityFieldsByEntity(UID entityUID) {
		Collection<MetaDbFieldWrapper> result = mapFieldMetaDataByEntity.get(entityUID);
		if (result == null) {
			return Collections.emptyList();
		}
		return result;
	}
	
	public MetaDbFieldWrapper getEntityField(UID fieldUID) {
		return mapFieldMetaData.get(fieldUID);
	}

	public boolean isNuclosEntity(UID entityUID) {
		if (sysEntities != null) {
			return sysEntities._isNuclosEntity(entityUID);
		}
		return originProvider.isNuclosEntity(entityUID);
	}

}
