package org.nuclos.server.dblayer;

import java.sql.Connection;
import java.util.Arrays;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.nuclos.server.database.NuclosBasicDatasource;
import org.springframework.stereotype.Service;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSortedSet;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Service
public class SQLKeywordProvider {

	private static final Logger LOG = Logger.getLogger(SQLKeywordProvider.class);

	private static final Set<String> SQL_92_KEYWORDS = ImmutableSet.of(
			"ABSOLUTE",
			"ACTION",
			"ADD",
			"ALL",
			"ALLOCATE",
			"ALTER",
			"AND",
			"ANY",
			"ARE",
			"AS",
			"ASC",
			"ASSERTION",
			"AT",
			"AUTHORIZATION",
			"AVG",
			"BEGIN",
			"BETWEEN",
			"BIT",
			"BIT_LENGTH",
			"BOTH",
			"BY",
			"CALL",
			"CASCADE",
			"CASCADED",
			"CASE",
			"CAST",
			"CATALOG",
			"CHAR",
			"CHARACTER",
			"CHARACTER_LENGTH",
			"CHAR_LENGTH",
			"CHECK",
			"CLOSE",
			"COALESCE",
			"COLLATE",
			"COLLATION",
			"COLUMN",
			"COMMIT",
			"CONDITION",
			"CONNECT",
			"CONNECTION",
			"CONSTRAINT",
			"CONSTRAINTS",
			"CONTAINS",
			"CONTINUE",
			"CONVERT",
			"CORRESPONDING",
			"COUNT",
			"CREATE",
			"CROSS",
			"CURRENT",
			"CURRENT_DATE",
			"CURRENT_PATH",
			"CURRENT_TIME",
			"CURRENT_TIMESTAMP",
			"CURRENT_USER",
			"CURSOR",
			"DATE",
			"DAY",
			"DEALLOCATE",
			"DEC",
			"DECIMAL",
			"DECLARE",
			"DEFAULT",
			"DEFERRABLE",
			"DEFERRED",
			"DELETE",
			"DESC",
			"DESCRIBE",
			"DESCRIPTOR",
			"DETERMINISTIC",
			"DIAGNOSTICS",
			"DISCONNECT",
			"DISTINCT",
			"DO",
			"DOMAIN",
			"DOUBLE",
			"DROP",
			"ELSE",
			"ELSEIF",
			"END",
			"ESCAPE",
			"EXCEPT",
			"EXCEPTION",
			"EXEC",
			"EXECUTE",
			"EXISTS",
			"EXIT",
			"EXTERNAL",
			"EXTRACT",
			"FALSE",
			"FETCH",
			"FIRST",
			"FLOAT",
			"FOR",
			"FOREIGN",
			"FOUND",
			"FROM",
			"FULL",
			"FUNCTION",
			"GET",
			"GLOBAL",
			"GO",
			"GOTO",
			"GRANT",
			"GROUP",
			"HANDLER",
			"HAVING",
			"HOUR",
			"IDENTITY",
			"IF",
			"IMMEDIATE",
			"IN",
			"INDICATOR",
			"INITIALLY",
			"INNER",
			"INOUT",
			"INPUT",
			"INSENSITIVE",
			"INSERT",
			"INT",
			"INTEGER",
			"INTERSECT",
			"INTERVAL",
			"INTO",
			"IS",
			"ISOLATION",
			"JOIN",
			"KEY",
			"LANGUAGE",
			"LAST",
			"LEADING",
			"LEAVE",
			"LEFT",
			"LEVEL",
			"LIKE",
			"LOCAL",
			"LOOP",
			"LOWER",
			"MATCH",
			"MAX",
			"MIN",
			"MINUTE",
			"MODULE",
			"MONTH",
			"NAMES",
			"NATIONAL",
			"NATURAL",
			"NCHAR",
			"NEXT",
			"NO",
			"NOT",
			"NULL",
			"NULLIF",
			"NUMERIC",
			"OCTET_LENGTH",
			"OF",
			"ON",
			"ONLY",
			"OPEN",
			"OPTION",
			"OR",
			"ORDER",
			"OUT",
			"OUTER",
			"OUTPUT",
			"OVERLAPS",
			"PAD",
			"PARAMETER",
			"PARTIAL",
			"PATH",
			"POSITION",
			"PRECISION",
			"PREPARE",
			"PRESERVE",
			"PRIMARY",
			"PRIOR",
			"PRIVILEGES",
			"PROCEDURE",
			"PUBLIC",
			"READ",
			"REAL",
			"REFERENCES",
			"RELATIVE",
			"REPEAT",
			"RESIGNAL",
			"RESTRICT",
			"RETURN",
			"RETURNS",
			"REVOKE",
			"RIGHT",
			"ROLLBACK",
			"ROUTINE",
			"ROWS",
			"SCHEMA",
			"SCROLL",
			"SECOND",
			"SECTION",
			"SELECT",
			"SESSION",
			"SESSION_USER",
			"SET",
			"SIGNAL",
			"SIZE",
			"SMALLINT",
			"SOME",
			"SPACE",
			"SPECIFIC",
			"SQL",
			"SQLCODE",
			"SQLERROR",
			"SQLEXCEPTION",
			"SQLSTATE",
			"SQLWARNING",
			"SUBSTRING",
			"SUM",
			"SYSTEM_USER",
			"TABLE",
			"TEMPORARY",
			"THEN",
			"TIME",
			"TIMESTAMP",
			"TIMEZONE_HOUR",
			"TIMEZONE_MINUTE",
			"TO",
			"TRAILING",
			"TRANSACTION",
			"TRANSLATE",
			"TRANSLATION",
			"TRIM",
			"TRUE",
			"UNDO",
			"UNION",
			"UNIQUE",
			"UNKNOWN",
			"UNTIL",
			"UPDATE",
			"UPPER",
			"USAGE",
			"USER",
			"USING",
			"VALUE",
			"VALUES",
			"VARCHAR",
			"VARYING",
			"VIEW",
			"WHEN",
			"WHENEVER",
			"WHERE",
			"WHILE",
			"WITH",
			"WORK",
			"WRITE",
			"YEAR",
			"ZONE"
	);

	private static volatile SQLKeywordProvider instance;

	private final NuclosBasicDatasource datasource;

	private Set<String> dbSpecificKeywords;

	public SQLKeywordProvider(final NuclosBasicDatasource datasource) {
		this.datasource = datasource;
		instance = this;
	}

	@PostConstruct
	public void init() {
		try (final Connection conn = datasource.getConnection()) {
			final String localKeywords = conn.getMetaData().getSQLKeywords();

			dbSpecificKeywords = ImmutableSortedSet.copyOf(
					Arrays.stream(localKeywords.split(","))
							.map(String::toUpperCase)
							.iterator()
			);
		} catch (Exception e) {
			LOG.error("Failed to query SQL keywords", e);
		}
	}

	/**
	 * TODO: Should not be called statically, but injected via Spring.
	 */
	@Deprecated
	public static boolean isKeyword(@Nullable final String word) {
		if (word == null) {
			return false;
		}

		final String upperWord = word.toUpperCase();

		if (SQL_92_KEYWORDS.contains(upperWord)) {
			return true;
		} else if (instance != null) {
			return instance.isDbSpecificKeyword(upperWord);
		}

		return false;
	}

	private boolean isDbSpecificKeyword(@Nonnull final String word) {
		if (dbSpecificKeywords != null) {
			return dbSpecificKeywords.contains(word.toUpperCase());
		}

		return false;
	}
}
