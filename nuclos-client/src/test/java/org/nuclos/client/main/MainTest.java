package org.nuclos.client.main;

import java.util.HashMap;

import org.junit.Test;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class MainTest {
	@Test
	public void testArgs() {
		HashMap<String, String> args = Main.parseArguments(new String[]{"test=a", "test2=123", "test3"});
		assert args.size() == 3;

		assert args.get("test").equals("a");
		assert args.get("test2").equals("123");
		assert args.get("test3") == null;
	}
}