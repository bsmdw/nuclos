package org.nuclos.client.launch;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.nuclos.client.LaunchListener;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.entityobject.EntityObjectDelegate;
import org.nuclos.client.main.Main;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.AbstractCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;

/**
 * 
 * Nuclos Launch Listener
 * 
 * open/select tab for businessobject on startup either by id or specific field value
 *
 */
// @Component
public class NuclosLaunchListener implements LaunchListener {
	
	private static final Logger LOG = Logger.getLogger(NuclosLaunchListener.class);
	
	private final MetaProvider metaProvider;

	private final static String PARAMETER_FIELD_UID = "fuid";
	private final static String PARAMETER_BO_UID = "bouid";
	private final static String PARAMETER_VALUE = "value";
	private final static String PARAMETER_PK = "pk";
	
	public NuclosLaunchListener() {
		metaProvider = MetaProvider.getInstance();
	}

	/**
	 * http://nuclosserver:8080/nuclos?fuid=<field-uid>&value=<lookup-value>
	 * http://nuclosserver:8080/nuclos?bouid=<field-uid>&pk=<pk> 
	 */
	@Override
	public void launched(Map<String, String> params) {

		final Iterator<Entry<String, String>> iterator = params.entrySet().iterator();
		if (!iterator.hasNext()) {
			LOG.warn("launch listener called with invalid parameter set: " + params);
			return;
		}
		final Entry<String, String> set = iterator.next();

		UID uidField = null;
		UID uidBo = null;
		String value = null;
		Object pk = null;

		for (final Entry<String, String> entry : params.entrySet()) {
			if (PARAMETER_FIELD_UID.equals(entry.getKey())) {
				uidField = new UID(entry.getValue());
			} else if (PARAMETER_BO_UID.equals(entry.getKey())) {
				uidBo = new UID(entry.getValue());
			} else if (PARAMETER_VALUE.equals(entry.getKey())) {
				value = entry.getValue();
			} else if (PARAMETER_PK.equals(entry.getKey())) {
				pk = entry.getValue();
			}
		}

		if (null != uidBo) {
			if (null == pk) {
				throw new IllegalArgumentException("Parameter " + PARAMETER_BO_UID + " is given, but without " + PARAMETER_PK + ".");	
			}
			
			lookupBo(uidBo, createPkFromString(uidBo, (String)pk));
		} else if (null != uidField) {
			if (null == value) {
				throw new IllegalArgumentException("Parameter " + PARAMETER_FIELD_UID + " is given, but without " + PARAMETER_VALUE + ".");
			}
			try {
				lookupValue(uidField, value);
			} catch (final CommonPermissionException ex) {
				throw new NuclosFatalException(ex); 
			}
		} else {
			throw new IllegalArgumentException("Parameter " + PARAMETER_FIELD_UID + " or " + PARAMETER_BO_UID + " must be provided.");
		}

		Main.getInstance().getMainFrame().setVisible(true);
	}

	/**
	 * 
	 * @param uidField	bo field {@link UID}
	 * @param value		lookup value
	 * 
	 * @throws CommonPermissionException
	 */
	private void lookupValue(final UID uidField, final String value) throws CommonPermissionException {

		FieldMeta<?> metaField = metaProvider.getEntityField(uidField);
		final UID uidBo = metaField.getEntity();

		AbstractCollectableSearchCondition comparison = null;
		if (StringUtils.isNullOrEmpty(value)) {
			// value is NULL
			comparison = SearchConditionUtils.newIsNullCondition(metaField);
		} else if (null != metaField.getForeignEntity()) {
			// field is reference field
			Object pk = createPkFromString(metaField.getForeignEntity(), value);
			if (pk instanceof Long) {
				comparison = SearchConditionUtils.newIdComparison(metaField, ComparisonOperator.EQUAL, (Long)pk);	
			} else if (pk instanceof UID) {
				comparison = SearchConditionUtils.newUidComparison(metaField, ComparisonOperator.EQUAL, (UID)pk);
			} else {
				throw new IllegalStateException("creation of comparison expression failed, unknown pk");
			}
		} else {
			// field is simple data field
			comparison = SearchConditionUtils.newComparison(metaField, ComparisonOperator.EQUAL, value);
		}
		
		final List<Long> lstIds = EntityObjectDelegate.getInstance().getEntityObjectIds(uidBo, new CollectableSearchExpression(comparison));

		if (lstIds.isEmpty()) {
			LOG.warn("no results for " + uidBo + " " + uidField + " with value " + value);
		}

		CollectController<Long, ?> ctrlTmp = null; 
		if (lstIds.size() == 1) {
			// find existing controller (tab)
			ctrlTmp = Main.getInstance().getMainController().findCollectControllerDisplaying(uidBo, lstIds.get(0));
		}
		final CollectController<Long, ?> ctrl = ctrlTmp;

		final Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				SwingUtilities.invokeLater(new Runnable() {

					@Override
					public void run() {
						try 
						{	
							if (lstIds.size() == 1) {

								if (null != ctrl) {
									// show existing tab
									ctrl.runViewResults(Collections.singletonList(lstIds.get(0)));
								} else {
									// create new tab
									Main.getInstance().getMainController().showDetails(uidBo, lstIds.get(0), true, null);
								}
							} else if (lstIds.size() > 1) {
								// show result list if multiple records matching the result
								Main.getInstance().getMainController().showList(uidBo, lstIds);
							} else {
								// do nothing
							}
						} catch (final Exception ex) {
							LOG.error("could not open businessobject " + uidBo + " with id " + lstIds, ex);
						}
					}
				});  
			}
		});
		t.start();
	}

	/**
	 * 
	 * @param uidBo	bo {@link UID}
	 * @param pk	pk
	 */
	private void lookupBo(final UID uidBo, final Object pk) {

		final CollectController<Object, ?> ctrl = Main.getInstance().getMainController().findCollectControllerDisplaying(uidBo, pk);

		final Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				SwingUtilities.invokeLater(new Runnable() {

					@Override
					public void run() {
						try {
							if (null != ctrl) {
								// show existing tab
								ctrl.runViewResults(Collections.singletonList(pk));
							} else {
								// create new tab
								Main.getInstance().getMainController().showDetails(uidBo, pk, true, null);
							}
						} catch (final Exception ex) {
							LOG.error("could not open businessobject " + uidBo + " with id " + pk, ex);
						}
					}
				});  
			}
		});
		t.start();

	}
	
	private final Object createPkFromString(final UID uidBo, final String pk) {
		final Class<?> clazzPK = metaProvider.getEntity(uidBo).getPkClass();
		Object result = null;
		if (Long.class.equals(clazzPK)) {
			// PK is Long
			result = Long.parseLong(pk);
		} else if (UID.class.equals(clazzPK)) {
			// PK is UID
			result = new UID(pk);
		} else {
			// PK is unknown
			throw new IllegalArgumentException("Unknown class for PK: " + clazzPK);
		}
		return result;
	}

}
