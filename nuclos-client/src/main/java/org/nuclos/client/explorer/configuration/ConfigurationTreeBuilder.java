//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.explorer.configuration;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.ObjectUtils;
import org.nuclos.common.EntityTreeViewVO;
import org.nuclos.common.UID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ConfigurationTreeBuilder
 * 
 * @author Moritz Neuhäuser &lt;moritz.neuhaeuser@nuclos.de&gt;
 */
public class ConfigurationTreeBuilder {

	private final static Logger LOG = LoggerFactory.getLogger(ConfigurationTreeBuilder.class);
	public static ConfigurationTreeModel build(ConfigurationEntityTreeNode rootNode, List<EntityTreeViewVO> views) {
		ConfigurationTreeModel model = new ConfigurationTreeModel(rootNode);
		createSubNodes(rootNode, views);
		return model;

	}
	
	public static void createChildNodes(EntityTreeViewVO parentVO, List<EntityTreeViewVO> views) {
		for (final EntityTreeViewVO vo : views) {
			if (ObjectUtils.equals(parentVO.getPrimaryKey(), vo.getParentSubnodeId())) {
				parentVO.addChildNode(vo);
				createChildNodes(vo, views);
			}
		}
	}

	private static void createSubNodes(ConfigurationTreeNode<?> parentNode, List<EntityTreeViewVO> views) {
		Collections.sort(views);
		
		UID uidParent = null;
		if (parentNode instanceof ConfigurationEntityTreeNode) {
			uidParent = null;
		} else if (parentNode instanceof ConfigurationEntityTreeSubNode) {
			uidParent = ((ConfigurationEntityTreeSubNode)parentNode).getContent().getPrimaryKey();
		}
		
		for (final EntityTreeViewVO vo : views) {
			final ConfigurationEntityTreeSubNode newNode = new ConfigurationEntityTreeSubNode(vo); 
			if (ObjectUtils.equals(vo.getParentSubnodeId(), uidParent)) {
				// ignore removed nodes
				if (!vo.isFlagRemoved()) {
					int idx = -1;
					if ((vo.getSortOrder() >= 0) && vo.getSortOrder() <= parentNode.getChildCount()) {
						idx = vo.getSortOrder();
					} else {
						idx = Math.max(parentNode.getChildCount() - 1, 0);
						LOG.error("something went wrong {} has no sorting order. use {} as sorting index", vo, idx);
					}
					
					parentNode.insert(newNode, idx);
					if (!vo.getChildNodes().isEmpty()) {
						createSubNodes(newNode, vo.getChildNodes());
					}
				}
			} else {
				continue;
			}

		}
	}








}
