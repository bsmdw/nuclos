package org.nuclos.client.explorer.node.eventsupport;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.nuclos.api.rule.GenerateFinalRule;
import org.nuclos.api.rule.GenerateRule;
import org.nuclos.api.rule.JobRule;
import org.nuclos.api.rule.StateChangeFinalRule;
import org.nuclos.api.rule.StateChangeRule;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.server.eventsupport.valueobject.EventSupportEventVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportTypeVO;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.job.valueobject.JobVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.navigation.treenode.AbstractTreeNode;
import org.nuclos.server.navigation.treenode.TreeNode;
import org.nuclos.server.statemodel.valueobject.StateModelVO;

public class EventSupportUsageNode extends AbstractTreeNode<UID> {

	EventSupportTargetType nodeType;
	EventSupportUsageNode parent;
	EventSupportSourceVO esesource;
	
	private String name;
	
	public EventSupportUsageNode(UID pk, EventSupportUsageNode pParent, String label, String name, String Description, EventSupportTargetType eventsupport) {
		super(pk);
		
		MasterDataVO<UID> masterDataVO = MasterDataCache.getInstance().get(E.SERVERCODE.getUID(), pk);
		esesource = 
				EventSupportRepository.getInstance().getEventSupportByClassname(masterDataVO.getFieldValue(E.SERVERCODE.name));
		if (EventSupportTargetType.EVENTSUPPORT.equals(eventsupport)) {
			setLabel(esesource.getName());
			setName(esesource.getClassname());
			setDescription(esesource.getDescription());
		} else if (EventSupportTargetType.EVENTSUPPORT_TYPE.equals(eventsupport)) {
			setLabel(label);
			setDescription(Description);
			setName(name);
		}
		else if (EventSupportTargetType.ENTITY.equals(eventsupport) ||
				EventSupportTargetType.ENTITY_INTEGRATION_POINT.equals(eventsupport)) {
			setLabel(label);
			setDescription(Description);
			setName(name);
		}
		else if (EventSupportTargetType.STATEMODEL.equals(eventsupport)) {
			setLabel(label);
			setDescription(Description);
			setName(name);
		}
		
		parent = pParent;
		nodeType = eventsupport;
	}
	
	public EventSupportTargetType getTreeNodeType() {
		return this.nodeType;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public TreeNode refreshed() throws CommonFinderException {
		return this;
	}

	@Override
	protected List<EventSupportUsageNode> getSubNodesImpl() throws RemoteException {
		List<EventSupportUsageNode> retVal = new ArrayList<EventSupportUsageNode>();
		
		try {
			if (EventSupportTargetType.EVENTSUPPORT.equals(nodeType)) {
				for (EventSupportTypeVO type : EventSupportRepository.getInstance().getEventSupportTypes()) {
					if (esesource != null && esesource.getInterface() != null && esesource.getInterface().contains(type.getClassname())) {
						retVal.add(
								new EventSupportUsageNode(getId(), this, type.getName(), type.getClassname(), type.getDescription(), EventSupportTargetType.EVENTSUPPORT_TYPE));					
					}
				}						
			} else if (EventSupportTargetType.EVENTSUPPORT_TYPE.equals(nodeType)) {
				// Entities
				List<EventSupportEventVO> eventSupportEntitiesByClassname = EventSupportRepository.getInstance().getEventSupportEntitiesByClassname(esesource.getClassname(), getName());
				for (EventSupportEventVO es : eventSupportEntitiesByClassname) {
					if (es.getEntityUID() != null) {
						MasterDataVO<UID> masterDataVO = MasterDataCache.getInstance().get(E.ENTITY.getUID(), es.getEntityUID());
						retVal.add(
								new EventSupportUsageNode(getId(), this,
										masterDataVO.getFieldValue(E.ENTITY.entity),
										masterDataVO.getFieldValue(E.ENTITY.entity),
										masterDataVO.getFieldValue(E.ENTITY.entity),
										EventSupportTargetType.ENTITY));
					}
					if (es.getIntegrationPointUID() != null) {
						MasterDataVO<UID> masterDataVO = MasterDataCache.getInstance().get(E.NUCLET_INTEGRATION_POINT.getUID(), es.getIntegrationPointUID());
						retVal.add(
								new EventSupportUsageNode(getId(), this,
										masterDataVO.getFieldValue(E.NUCLET_INTEGRATION_POINT.name),
										masterDataVO.getFieldValue(E.NUCLET_INTEGRATION_POINT.name),
										masterDataVO.getFieldValue(E.NUCLET_INTEGRATION_POINT.name),
										EventSupportTargetType.ENTITY_INTEGRATION_POINT));
					}
				}
				// statemodel
				if (getName().equals(StateChangeRule.class.getCanonicalName()) || getName().equals(StateChangeFinalRule.class.getCanonicalName())) {
					List<StateModelVO> stateModelByEventSupportClassname = EventSupportRepository.getInstance().getStateModelByEventSupportClassname(esesource.getClassname());
					for (StateModelVO state: stateModelByEventSupportClassname) {
						retVal.add(new EventSupportUsageNode(getId(), this, state.getName(),  state.getName(), state.getDescription(), EventSupportTargetType.STATEMODEL));
					}
				}
			
				// jobs
				List<JobVO> jobsByEventSupportClassname = EventSupportRepository.getInstance().getJobsByEventSupportClassname(esesource.getClassname());
				for (JobVO job: jobsByEventSupportClassname) {
					if (getName().equals(JobRule.class.getCanonicalName()))
						retVal.add(new EventSupportUsageNode(getId(), this, job.getName(),  job.getName(), job.getDescription(), EventSupportTargetType.JOB));
				}
				
				// generations
				List<GeneratorActionVO> generationsByEventSupportClassname = EventSupportRepository.getInstance().getGenerationsByEventSupportClassname(esesource.getClassname());
				for (GeneratorActionVO gen: generationsByEventSupportClassname) {
					if (getName().equals(GenerateRule.class.getCanonicalName()) || getName().equals(GenerateFinalRule.class.getCanonicalName()))
					retVal.add(new EventSupportUsageNode(getId(), this, gen.getName(),  gen.getLabel(), gen.getLabel(), EventSupportTargetType.GENERATION));
				}
			}
			
		} catch (Exception e) {}
			
		return retVal;
	}
	
}

