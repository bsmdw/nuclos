package org.nuclos.client.explorer;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;

import javax.swing.Action;
import javax.swing.JTree;
import javax.swing.tree.TreePath;

import org.nuclos.client.explorer.node.AbstractEventSupportExplorerNode;
import org.nuclos.client.rule.server.panel.AbstractEventSupportPropertyPanel;

public class EventSupportMouseListener extends MouseAdapter {
	
	private final JTree tree;
	private final AbstractEventSupportPropertyPanel propPanel;
	
	public EventSupportMouseListener(JTree pTree, AbstractEventSupportPropertyPanel pPropPanel) {
		tree = pTree;
		propPanel = pPropPanel;
	}

	@Override
	public void mousePressed(MouseEvent ev) {
		
		final int selRow = tree.getRowForLocation(ev.getX(), ev.getY());
		final TreePath treepath = tree.getPathForLocation(ev.getX(), ev.getY());
		
		if (selRow != -1) {
			final AbstractEventSupportExplorerNode node = (AbstractEventSupportExplorerNode) treepath.getLastPathComponent();

			// performs the defined action attached to a simple mouse click; typically null
			final Action actDefault = node.getTreeNodeActionOnMouseClick(tree);
			if (actDefault != null) {
				actDefault.actionPerformed(null);
			}
				
			// if the node isn't selected already:
			final TreePath[] aSelectionPaths = tree.getSelectionPaths();
			if (aSelectionPaths == null || !Arrays.asList(aSelectionPaths).contains(treepath)) {
				// select it (and unselect all others):
				tree.setSelectionPath(treepath);
			}
		}
		
	}

	@Override
	public void mouseReleased(MouseEvent ev) {
		//mouseEventOnNode(ev);
	}

	@Override
	public void mouseClicked(MouseEvent ev) {
	//	mouseEventOnNode(ev);
	}
}
