//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.explorer.configuration;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.EventListener;
import java.util.EventObject;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import javax.swing.AbstractAction;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.EventListenerList;

import org.apache.commons.lang.StringUtils;
import org.nuclos.client.command.OvOpAdapter;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.explorer.NodeActionContext;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.elementalcomponents.SaveAndCancelButtonPanel.SaveAndCancelButtonPanelControllable;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.ui.OverlayOptionPane;
import org.nuclos.client.ui.ReferenceUtils;
import org.nuclos.client.ui.util.TableLayoutBuilder;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.EntityTreeViewVO;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;

import info.clearthought.layout.TableLayout;

/**
 * CreateNewExplorerNodeDialog
 * 
 * @author Moritz Neuhäuser &lt;moritz.neuhaeuser@nuclos.de&gt;
 */
public class CreateNewExplorerNodeDialog extends JPanel implements SaveAndCancelButtonPanelControllable {
	private static final String NUCLOS_GENERICREFERENCE = "nuclos_genericobject";

	private static final Comparator<? super EntityItem> COMPARATOR_ENTITY_ITEM_BY_NAME = new EntityItemByNameComparator();
	private static final Comparator<? super EntityFieldItem> COMPARATOR_ENTITYFIELD_ITEM_BY_NAME = new EntityFieldItemByNameComparator();

	private final SpringLocaleDelegate localeDelegate;
	private DefaultConfigurationExplorerView view;
	private JPanel mainContainer;
	private JScrollPane scrollpane;
	private JComboBox cbxEntity;
	private JCheckBox ckInheritNodes;
	private JCheckBox ckActive;
	private JTextField txtNode;
	private JTextField txtNodeTooltip;
	private JComboBox cbxField;
	private JComboBox cbxFieldRoot;
	private JTextField txtGroupBy;
	private JCheckBox ckShowEntityFolder;
	
	private boolean saveEnabled = false;
//	private JButton btnSave;
//	private JButton btnCancel;
	private NodeActionContext<ConfigurationTreeNode<?>> nodeContext;
	private final MetaProvider prov;

	private final EventListenerList lstEventListener;
	private EditMode editMode;
	private JPanel sourroundingPanel;
	private JPanel outerPanel;

	private JScrollPane scrollAttribute;

	private NuclosWizardAttributeSingleSelectionList attributeList;

	private JButton btnAddNode;

	private JButton btnAddNodeTooltip;

	private JButton btnGroupBy;

	public static enum EditMode {
		EDIT,	/* edit existing node */
		NEW		/* new node */
	}
	
	public interface EntitySelectionChangedListener extends EventListener {

		void entitySelectionChanged(EntitySelectionChangedEvent e);
	}

	public static class EntitySelectionChangedEvent extends EventObject {
		private EntityItem item;

		public EntitySelectionChangedEvent(Object source, EntityItem item) {
			super(source);
			this.item = item;
		}

		public EntityItem getItem() {
			return item;
		}
	}
	
	public static class EntityItemByNameComparator implements Comparator<EntityItem> {

		@Override
		public int compare(EntityItem o1, EntityItem o2) {
			final Comparable v1 = (null != o1 && null != o1.getContent()) ? o1.getContent().getEntityName() : null;
			final Comparable v2 = (null != o2 && null != o2.getContent()) ? o2.getContent().getEntityName() : null;
			
			return LangUtils.compareComparables(v1, v2);
		}
		
	}
	
	public static class EntityItem {
		private final EntityMeta content;
		private final Collection<FieldMeta<?>> lstFields;

		public EntityItem(EntityMeta entityMetaVO, Collection<FieldMeta<?>> lstFields) {
			this.content = entityMetaVO;
			this.lstFields = lstFields;
		}
		public EntityItem(EntityMeta entityMetaVO) {
			this(entityMetaVO, new ArrayList<FieldMeta<?>>());
		}

		public Collection<FieldMeta<?>> getFields() {
			return lstFields;
		}

		public EntityMeta getContent() {
			return content;
		}

		@Override
		public String toString() {
			return content.toString();
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((content == null) ? 0 : content.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			EntityItem other = (EntityItem) obj;
			if (content == null) {
				if (other.content != null)
					return false;
			} else if (!content.equals(other.content))
				return false;
			return true;
		}


	}
	
	public static class EntityFieldItemByNameComparator implements Comparator<EntityFieldItem> {

		@Override
		public int compare(EntityFieldItem o1, EntityFieldItem o2) {
			final Comparable v1 = (null != o1 && null != o1.getContent()) ? o1.getContent().getFieldName() : null;
			final Comparable v2 = (null != o2 && null != o2.getContent()) ? o2.getContent().getFieldName() : null;
			return LangUtils.compareComparables(v1, v2);
		}
		
	}
	
	public static class EntityFieldItem {
		private final FieldMeta<?> content;
		private final String name;

		public EntityFieldItem(FieldMeta<?> fieldMetaVO) {
//			final String resourceId = fieldMetaVO.getLocaleResourceIdForLabel();
//			this.name = (null != resourceId) ? SpringLocaleDelegate.getInstance().getText(fieldMetaVO.getLocaleResourceIdForLabel()) : fieldMetaVO.getFieldName();
			this.name = fieldMetaVO.getFieldName();
			this.content = fieldMetaVO;
		}

		public FieldMeta<?> getContent() {
			return content;
		}

		@Override
		public String toString() {
			return name;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((content == null) ? 0 : content.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			EntityFieldItem other = (EntityFieldItem) obj;
			if (content == null) {
				if (other.content != null)
					return false;
			} else if (!content.equals(other.content))
				return false;
			return true;
		}


	}

	public CreateNewExplorerNodeDialog(MainFrameTab parent, DefaultConfigurationExplorerView view, NodeActionContext<ConfigurationTreeNode<?>> nodeContext, EditMode editMode) {
//		super(MainController.getMainFrame());
//		this.setUndecorated(true);
		
		lstEventListener = new EventListenerList();
		this.nodeContext = nodeContext;
		this.localeDelegate = SpringLocaleDelegate.getInstance();
		this.view = view;
		this.prov = MetaProvider.getInstance();
		this.editMode = editMode;
		mainContainer = new JPanel(new BorderLayout());
		
		this.outerPanel = new JPanel(new BorderLayout());
//		outerPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
		
		this.sourroundingPanel = new JPanel(new BorderLayout());
//		sourroundingPanel.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));

		scrollpane = new JScrollPane(mainContainer);
		scrollpane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollpane.getVerticalScrollBar().setUnitIncrement(20);

		attributeList = new NuclosWizardAttributeSingleSelectionList();
		scrollAttribute = new JScrollPane();
		scrollAttribute.setPreferredSize(new Dimension(150, 100));
		scrollAttribute.getViewport().add(attributeList);
//		sourroundingPanel.add(createFooterPanel(), BorderLayout.SOUTH);
		
		JPanel detailsPanel = createDetailsPanel();
		mainContainer.add(detailsPanel);
		sourroundingPanel.add(mainContainer, BorderLayout.CENTER);
		sourroundingPanel.add(scrollAttribute, BorderLayout.EAST);
		
		outerPanel.add(sourroundingPanel);
		add(outerPanel, BorderLayout.CENTER);
		
//		this.pack();

		// pack & center dialog
//		UIUtils.center(this, MainController.getMainFrame(), true);

//		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);		
//		setTitle(localeDelegate.getMessage("CreateNewExplorerNodeDialog.title","Unterknoten hinzufügen"));
//		setModal(true);
		
		String title = editMode == EditMode.EDIT ?  localeDelegate.getMessage("ConfigurationExplorer.editSubNode", "Unterknoten bearbeiten")
				: localeDelegate.getMessage("CreateNewExplorerNodeDialog.title", "Unterknoten hinzufügen");
		
		OverlayOptionPane.showConfirmDialog(parent, this, title, 
				OverlayOptionPane.OK_CANCEL_OPTION, true, new OvOpAdapter() {
			@Override
			public void done(int result) {
				if (OverlayOptionPane.OK_OPTION == result && saveEnabled) {
					performSaveAction();
				} else {
					performCancelAction();
				}
			}
		});
	}

//	private JPanel createFooterPanel() {
//		final JPanel panel = new JPanel();
//		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
//		btnSave = new JButton(localeDelegate.getMessage("CreateNewExplorerNodeDialog.save","Ok"));
//		btnSave.addActionListener(new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				performSaveAction();
//			}
//		});
//
//		btnCancel = new JButton(localeDelegate.getMessage("CreateNewExplorerNodeDialog.cancel","Abbrechen"));
//		btnCancel.addActionListener(new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				performCancelAction();
//			}
//		});
//		final TableLayoutBuilder tblLay = new TableLayoutBuilder(panel).columns(TableLayout.FILL, 10.0,  TableLayout.FILL);
//		tblLay.newRow().add(btnSave).skip().add(btnCancel);
//
//		return panel;
//
//	}
	
	private void initFields(EntityTreeViewVO vo) {
		if (null == vo) {
			// Default
	
			this.ckActive = createCkActive(true);
			this.ckActive.setName("active");
	
			this.ckInheritNodes = createCkInheritNodes(false);
			this.ckShowEntityFolder = createCkShowEntityFolder(false);
	
			this.txtNode = new JTextField();
			this.txtNode.setName("node");
			this.txtNode.setColumns(30);
	
			this.txtNodeTooltip = new JTextField();
			this.txtNodeTooltip.setName("nodetooltip");
	
			this.cbxField = createCbxField();
			this.cbxFieldRoot = createCbxRootField();
			
			this.txtGroupBy = new JTextField();
			this.txtGroupBy.setName("groupby");
			
			this.cbxEntity = createCbxEntity(null);
			cbxEntity.setName("entity");		
			
		} else {
			// fill by existing vo
			this.ckActive = createCkActive(vo.isActive());
			this.ckActive.setName("active");

			this.ckInheritNodes = createCkInheritNodes(vo.getIsInheritNodes());
			if (getNodeContext().getNode().getChildCount() > 0) {
				this.ckInheritNodes.setEnabled(false);
			} else {
				this.ckInheritNodes.setEnabled(true);
			}
			this.ckShowEntityFolder = createCkShowEntityFolder(vo.getIsShowEntityFolder());

			this.txtNode = new JTextField();
			this.txtNode.setName("node");
			this.txtNode.setColumns(30);

			this.txtNodeTooltip = new JTextField();
			this.txtNodeTooltip.setName("nodetooltip");
			
			this.txtGroupBy = new JTextField();
			this.txtGroupBy.setName("groupby");
			
			final EntityMeta<?> metaEntity = prov.getEntity(vo.getEntity());
			final Transformer<String, String> strUidTransformer = new Transformer<String, String>() {
				@Override
				public String transform(String i) {
					for(FieldMeta<?> field : metaEntity.getFields()) {
						if (field.getUID().getStringifiedDefinition().equals(i))
							return "${" + field.getFieldName() + "}";
					}
					return "";
				}					
			};
			
			final String node = RigidUtils.replaceParameters(RigidUtils.PARAM_PATTERN_UID, vo.getNode(), strUidTransformer);
			final String nodeTooltip = RigidUtils.replaceParameters(RigidUtils.PARAM_PATTERN_UID, vo.getNodeTooltip(), strUidTransformer);
			final String groupyBy = RigidUtils.replaceParameters(RigidUtils.PARAM_PATTERN_UID, vo.getGroupBy(), strUidTransformer);
			
			this.txtNode.setText(node);
			this.txtNodeTooltip.setText(nodeTooltip);
			this.txtGroupBy.setText(groupyBy);

			this.cbxField = createCbxField();
			UID uidFieldRoot = vo.getFieldRoot();
			if (null != uidFieldRoot) {
				this.cbxFieldRoot = createCbxRootField();
			} else {
				this.cbxFieldRoot = createCbxRootField();
			}

			this.cbxEntity = createCbxEntity(new EntityItem(prov.getEntity(vo.getEntity())));
			cbxEntity.setName("entity");
			cbxEntity.setEnabled(false);
			

			final DefaultComboBoxModel modelField = (DefaultComboBoxModel) cbxField.getModel();
			if (vo.getField() == null) {
				modelField.setSelectedItem(null);
			} else {
				cbxField.setSelectedIndex(modelField.getIndexOf(new EntityFieldItem(prov.getEntityField(vo.getField()))));
			}

			final DefaultComboBoxModel modelFieldRoot = (DefaultComboBoxModel) cbxFieldRoot.getModel();
			if (vo.getFieldRoot() == null) {
				modelFieldRoot.setSelectedItem(null);
			} else {
				cbxFieldRoot.setSelectedIndex(modelFieldRoot.getIndexOf(new EntityFieldItem(prov.getEntityField(vo.getFieldRoot()))));
			}
			
		}
		
		boolean enabled = !ckInheritNodes.isSelected();
		txtNode.setEnabled(enabled);
		txtNodeTooltip.setEnabled(enabled);
		cbxField.setEnabled(enabled);
		cbxFieldRoot.setEnabled(enabled);
		txtGroupBy.setEnabled(enabled);
		ckShowEntityFolder.setEnabled(enabled);
		
		this.ckInheritNodes.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				boolean enabled = true;
				if (ItemEvent.SELECTED == e.getStateChange()) {
					enabled = false;
				}
				txtNode.setEnabled(enabled);
				txtNodeTooltip.setEnabled(enabled);
				cbxField.setEnabled(enabled);
				cbxFieldRoot.setEnabled(enabled);
				ckShowEntityFolder.setEnabled(enabled);
				
			}
		});
//		btnSave.setEnabled(null != cbxEntity.getSelectedItem());
		saveEnabled = null != cbxEntity.getSelectedItem();
	}
	
	private JPanel createDetailsPanel() {

		this.btnAddNode = new JButton(new AbstractAction("<<") {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				final String value = StringUtils.trim(txtNode.getText() + " " + attributeList.getSelectedAttribute());
				txtNode.setText(value);
				
			}
		});
		
		
		this.btnAddNodeTooltip = new JButton(new AbstractAction("<<") {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				final String value = StringUtils.trim(txtNodeTooltip.getText() + " " + attributeList.getSelectedAttribute());
				txtNodeTooltip.setText(value);
				
			}
		});
		
		this.btnGroupBy = new JButton(new AbstractAction("<<") {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				final String value = StringUtils.trim(txtGroupBy.getText() + " " + attributeList.getSelectedAttribute());
				txtGroupBy.setText(value);
				
			}
		});

		btnAddNode.setEnabled(false);
		btnAddNodeTooltip.setEnabled(false);
		btnGroupBy.setEnabled(false);

		addEntitySelectionChangedListener(new EntitySelectionChangedListener() {

			@Override
			public void entitySelectionChanged(EntitySelectionChangedEvent e) {
				// fields
				fillField(cbxField);
				
				// root fields
				fillFieldRoot(cbxFieldRoot);
				
				if (EditMode.EDIT != editMode) { 
					selectFirst(cbxField);
					selectFirst(cbxFieldRoot);
				}

				final EntityItem entityItem = e.getItem();
				
				Collection<FieldMeta<?>> lstFieldMeta = new ArrayList<FieldMeta<?>>();
				boolean atLeastOneAttributeSelectable = false;
				if (null != entityItem) {
					if(isDynamicEntity(entityItem.getContent().getUID())) {
						ckInheritNodes.setEnabled(false);
						ckShowEntityFolder.setEnabled(true);
					}
					
					
					lstFieldMeta = prov.getAllEntityFieldsByEntity(entityItem.getContent().getUID()).values();
					attributeList.fill(entityItem.getContent(), lstFieldMeta);
					
					
					atLeastOneAttributeSelectable = attributeList.getModel().getSize() > 0;
					
					
//					btnSave.setEnabled(true);
					saveEnabled = true;
				} else {
//					btnSave.setEnabled(false);
					saveEnabled = false;
					attributeList.clear();
				}
				
				
				btnAddNode.setEnabled(atLeastOneAttributeSelectable);
				btnAddNodeTooltip.setEnabled(atLeastOneAttributeSelectable);
				btnGroupBy.setEnabled(atLeastOneAttributeSelectable);
				if (atLeastOneAttributeSelectable) {
					// attribute pre-selection
					attributeList.getSelectionModel().setSelectionInterval(0, 0);
				}
			}
		});
		
		if (EditMode.EDIT == editMode &&
				getNodeContext().getNode() instanceof ConfigurationEntityTreeSubNode
				) {
			initFields(((ConfigurationEntityTreeSubNode)getNodeContext().getNode()).getContent());
		} else {
			initFields(null);
			
		}
		
		final JPanel panel = new JPanel();

		// build up table layout
		final TableLayoutBuilder tblLay = new TableLayoutBuilder(panel).columns(TableLayout.PREFERRED, 10.0,  TableLayout.PREFERRED, 60.0, TableLayout.FILL);
		tblLay.gaps(5, 5);
		tblLay.newRow().addLocalizedLabel("CreateNewExplorerNodeDialog.entity").skip().add(cbxEntity).skip().skip();
		tblLay.newRow().addLocalizedLabel("CreateNewExplorerNodeDialog.active").skip().add(ckActive).skip().skip();
		tblLay.newRow().addLocalizedLabel("CreateNewExplorerNodeDialog.inherit").skip().add(ckInheritNodes).skip().skip();
		tblLay.newRow().addLocalizedLabel("CreateNewExplorerNodeDialog.node").skip().add(txtNode).add(btnAddNode).skip();
		tblLay.newRow().addLocalizedLabel("CreateNewExplorerNodeDialog.nodetooltip").skip().add(txtNodeTooltip).add(btnAddNodeTooltip).skip();
		tblLay.newRow().addLocalizedLabel("CreateNewExplorerNodeDialog.field").skip().add(cbxField).skip().skip();
		tblLay.newRow().addLocalizedLabel("CreateNewExplorerNodeDialog.fieldroot").skip().add(cbxFieldRoot).skip().skip();
		tblLay.newRow().addLocalizedLabel("CreateNewExplorerNodeDialog.groupby").skip().add(txtGroupBy).add(btnGroupBy).skip();
		tblLay.newRow().addLocalizedLabel("CreateNewExplorerNodeDialog.showentityfolder").skip().add(ckShowEntityFolder).skip().skip();

		panel.setVisible(true);
		return panel;
	}
	
	private void selectFirst(final JComboBox cbx) {
		if (null == cbx.getSelectedItem() && cbx.getItemCount() == 2) {
			cbx.setSelectedIndex(1);
		}
	}

	public NodeActionContext<ConfigurationTreeNode<?>> getNodeContext() {
		return nodeContext;
	}

	private JComboBox createCbxEntity(final EntityItem item) {
		this.cbxEntity = new JComboBox();
		final TreeSet<EntityItem> sortedItems = new TreeSet<CreateNewExplorerNodeDialog.EntityItem>(COMPARATOR_ENTITY_ITEM_BY_NAME);
		
		final DefaultComboBoxModel model = (DefaultComboBoxModel) this.cbxEntity.getModel();
		if (getNodeContext().getNode() instanceof ConfigurationTreeNode<?>) {
			((DefaultComboBoxModel)this.cbxEntity.getModel()).addElement(null);
			ConfigurationTreeNode<?> ets = (ConfigurationTreeNode<?>)getNodeContext().getNode();
			EntityMeta metaEntity = null;
			if (EditMode.EDIT == editMode) {
				metaEntity = prov.getEntity(((ConfigurationTreeNode<?>)ets.getParent()).getEntity());
			} else {
				metaEntity = prov.getEntity(ets.getEntity());
			}
			
			final Map<EntityMeta, List<FieldMeta<?>>> mpReferences = ReferenceUtils.findReferencingEntities(metaEntity.getUID());
			// append referencing entities
			for (final EntityMeta vo: mpReferences.keySet()) {
				sortedItems.add(new EntityItem(vo, mpReferences.get(vo)));

			}

			// append dynamic entities
			
			for (EntityMeta<?> meta : prov.getAllDynEntities()) {
				sortedItems.add(new EntityItem(meta, meta.getFields()));

			}
			

			if (metaEntity.isDynamic()) {
				for (final EntityMeta<?> meta : prov.getAllEntities()) {
					
					if (meta.isDynamic() || meta.isUidEntity()) {
						continue;
					}
					sortedItems.add(new EntityItem(meta, prov.getAllEntityFieldsByEntity(meta.getUID()).values()));
				}
			}
			
			// append GENERALSEARCHDOCUMENT entity for statemodel entites
			if (metaEntity.isStateModel() || metaEntity.isDynamic()) {
				sortedItems.add(new EntityItem(prov.getEntity(E.GENERALSEARCHDOCUMENT.getUID()), prov.getAllEntityFieldsByEntity(E.GENERALSEARCHDOCUMENT.getUID()).values()));
			}
			
			for (final EntityItem entityItem : sortedItems) {
				model.addElement(entityItem);
			}
			

		}

		// init listener
		this.cbxEntity.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (ItemEvent.SELECTED == e.getStateChange()) {
					if (e.getItem() instanceof EntityItem) {
						fireEntitySelectionChanged(new EntitySelectionChangedEvent(cbxEntity, (EntityItem)e.getItem()));
					} else {
						throw new IllegalStateException();
					}
				}else if (ItemEvent.DESELECTED == e.getStateChange()) {
					fireEntitySelectionChanged(new EntitySelectionChangedEvent(cbxEntity, null));
				} else {
					// throw new IllegalStateException();
				}
			}
		});
		
		int idx = model.getIndexOf(item);
		if (idx > -1) {
			model.setSelectedItem(model.getElementAt(idx));
		} else {
			if (EditMode.EDIT == editMode) {
				throw new IllegalStateException("could not restore " + item);
			}
		}
		
		return this.cbxEntity;


	}

	private JComboBox createCbxField() {
		final JComboBox cbx = new JComboBox();
		cbx.setName("field");
		return cbx;


	}

	private JComboBox createCbxRootField() {
		final JComboBox cbx = new JComboBox();
		cbx.setName("field_root");
		return cbx;
	}

	private void fillField(final JComboBox cbx) {
		final EntityItem item = ((EntityItem)cbxEntity.getSelectedItem());
		final DefaultComboBoxModel model = (DefaultComboBoxModel) cbx.getModel();
		// clear
		model.removeAllElements();
		final TreeSet<EntityFieldItem> sortedItems = new TreeSet<CreateNewExplorerNodeDialog.EntityFieldItem>(COMPARATOR_ENTITYFIELD_ITEM_BY_NAME);
		if (null != item) {
			sortedItems.add(null);
			// append referencing entities
			for (final FieldMeta<?> fieldMetaVO: item.getFields()) {
				sortedItems.add(new EntityFieldItem(fieldMetaVO));
			}
			for (EntityFieldItem entityFieldItem : sortedItems) {
				model.addElement(entityFieldItem);
			}
		}

	}
	
	private void fillFieldRoot(final JComboBox cbx) {
		final EntityItem item = ((EntityItem)cbxEntity.getSelectedItem());
		final DefaultComboBoxModel model = (DefaultComboBoxModel) cbx.getModel();
		// clear
		model.removeAllElements();

		if (null != item) {
			final TreeSet<EntityFieldItem> sortedItems = new TreeSet<CreateNewExplorerNodeDialog.EntityFieldItem>(COMPARATOR_ENTITYFIELD_ITEM_BY_NAME);
			ConfigurationTreeNode<?> etsRoot = (ConfigurationTreeNode<?>)view.getJTree().getModel().getRoot();
			final UID uidEntityRoot = etsRoot.getEntity();
			final Map<EntityMeta, List<FieldMeta<?>>> mpReferences = ReferenceUtils.findReferencingEntities(prov.getEntity(uidEntityRoot).getUID());
			

			sortedItems.add(null);
			if (item.getContent().isDynamic()) {
				for (final FieldMeta<?> fieldMetaVO: item.getFields()) {
					sortedItems.add(new EntityFieldItem(fieldMetaVO));
				}
			} else {
				for (final EntityMeta entityMetaVO: mpReferences.keySet()) {
					/*
				}
					if (item.getContent().getUID().equals(entityMetaVO.getUID()) 
							|| NUCLOS_GENERICREFERENCE.equals(entityMetaVO.getEntity())) {*/
					if (item.getContent().getUID().equals(entityMetaVO.getUID())) {
						for (final FieldMeta<?> fieldMetaVO: mpReferences.get(entityMetaVO)) {		
							sortedItems.add(new EntityFieldItem(fieldMetaVO));
						}
					}
				}
			}
			
			for (EntityFieldItem entityFieldItem : sortedItems) {
				model.addElement(entityFieldItem);
			}


		}
	}
	

	private JCheckBox createCkInheritNodes(boolean isInheritNodes) {
		final JCheckBox ck = new JCheckBox();
		ck.setName("inherit_nodes");
		ck.setSelected(isInheritNodes);
		return ck;
	}

	private JCheckBox createCkShowEntityFolder(boolean isShowEntityFolder) {
		final JCheckBox ck = new JCheckBox();
		ck.setName("show_entity_folder");
		ck.setSelected(isShowEntityFolder);
		return ck;
	}
	

	private JCheckBox createCkActive(boolean isActive) {
		final JCheckBox ck = new JCheckBox();
		ck.setName("active");
		ck.setSelected(isActive);
		return ck;
	}


	public void addEntitySelectionChangedListener(EntitySelectionChangedListener listener) {
		lstEventListener.add(EntitySelectionChangedListener.class, listener);
	}

	public void removeEntitySelectionChangedListener(EntitySelectionChangedListener listener) {
		lstEventListener.remove(EntitySelectionChangedListener.class, listener);
	}

	protected void fireEntitySelectionChanged(EntitySelectionChangedEvent e) {
		final Object[] listeners = lstEventListener.getListenerList();
		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == EntitySelectionChangedListener.class) {
				((EntitySelectionChangedListener) listeners[i+1]).entitySelectionChanged(e);
			}
		}

	}

	@Deprecated
	private boolean isDynamicEntity(final UID entity) {
		final EntityMeta metaEntity = prov.getEntity(entity);
		return metaEntity.isDynamic();
	}

	private EntityMeta getSelectedEntity() {
		EntityMeta result = null;
		if (null != cbxEntity.getSelectedItem()) {
			result = ((EntityItem)cbxEntity.getSelectedItem()).getContent();
		}
		return result;
	}

	private FieldMeta<?> getFieldRoot() {
		FieldMeta<?> result = null;
		if (null != cbxFieldRoot.getSelectedItem()) {
			result = ((EntityFieldItem)cbxFieldRoot.getSelectedItem()).getContent();
		}
		return result;
	}

	private FieldMeta<?> getField() {
		FieldMeta<?> result = null;
		if (null != cbxField.getSelectedItem()) {
			result = ((EntityFieldItem)cbxField.getSelectedItem()).getContent();
		}
		return result;
	}

	private boolean getIsInherit() {
		return ckInheritNodes.isSelected();

	}
	
	private boolean getIsActive() {
		return ckActive.isSelected();

	}
	
	private boolean getIsShowEntityFolder() {
		return ckShowEntityFolder.isSelected();

	}

	private String getNode() {
		return txtNode.getText();
	}

	private String getNodeTooltip() {
		return txtNodeTooltip.getText();
	}
	
	private String getGroupBy() {
		return txtGroupBy.getText();
	}

	@Override
	public void performSaveAction() {


		int sortOrder = 0;
		String foldername = "";
		UID uidField = null;
		UID uidFieldRoot = null;

		UID uidOriginEntity = null;
		UID entity = null;
		UID idParentSubNode = null;
		String node = null;
		String nodeTooltip = null;
		String groupBy = null;
		ConfigurationTreeNode<?> cnd = (ConfigurationTreeNode<?>)getNodeContext().getNode();
		boolean isInherit = getIsInherit();
		boolean isActive = getIsActive();
		boolean isShowEntityFolder = getIsShowEntityFolder();
		
		
		// replace fields
		final EntityMeta<?> metaEntity = prov.getEntity(getSelectedEntity().getUID());
		final Transformer<String, String> strUidTransformer = new Transformer<String, String>() {
			@Override
			public String transform(String i) {
				for(FieldMeta<?> field : metaEntity.getFields()) {
					if (field.getFieldName().equals(i))
						return field.getUID().getStringifiedDefinition();
				}
				return "";
			}					
		};
		
		node = RigidUtils.replaceParameters(RigidUtils.PARAM_PATTERN_$, getNode(), strUidTransformer);		
		nodeTooltip = RigidUtils.replaceParameters(RigidUtils.PARAM_PATTERN_$, getNodeTooltip(), strUidTransformer);
		groupBy = RigidUtils.replaceParameters(RigidUtils.PARAM_PATTERN_$, getGroupBy(), strUidTransformer);
		
		if (getNodeContext().getNode() instanceof ConfigurationEntityTreeSubNode) {
			final ConfigurationEntityTreeSubNode nd = (ConfigurationEntityTreeSubNode)getNodeContext().getNode();
			sortOrder = nd.getChildCount();
			foldername = "";
			uidField = (null != getField()) ? getField().getUID() : null ;
			uidFieldRoot = (null != getFieldRoot()) ? getFieldRoot().getUID() : null;

			uidOriginEntity = nd.getContent().getOriginEntity();
			entity = getSelectedEntity().getUID();
			idParentSubNode = nd.getContent().getPrimaryKey();


		} else if (getNodeContext().getNode() instanceof ConfigurationEntityTreeNode) {
			final ConfigurationEntityTreeNode nd = (ConfigurationEntityTreeNode)getNodeContext().getNode();
			sortOrder = nd.getChildCount();
			foldername = "";
			uidField = (null != getField()) ? getField().getUID() : null ;
			uidFieldRoot = (null != getFieldRoot()) ? getFieldRoot().getUID() : null;

			uidOriginEntity = nd.getContent().getUID();
			entity = getSelectedEntity().getUID();
			// is toplevel (no parent)
			idParentSubNode = null;
		}
		if (EditMode.EDIT == editMode) {
			// update
			final EntityTreeViewVO vo = ((ConfigurationEntityTreeSubNode)getNodeContext().getNode()).getContent();
			vo.setOriginEntity(uidOriginEntity);
			vo.setEntity(entity);
			vo.setField(uidField);
			vo.setFoldername(foldername);
			vo.setActive(isActive);
			//vo.setSortOrder(sortOrder);
			vo.setIsInheritNodes(isInherit);
			vo.setFieldRoot(uidFieldRoot);
			vo.setParentSubnodeId(idParentSubNode);
			vo.setNode(node);
			vo.setNodeTooltip(nodeTooltip);
			vo.setGroupBy(groupBy);
			vo.setIsShowEntityFolder(isShowEntityFolder);
			if (vo.isFlagUnchanged()) {
				vo.flagUpdate();
			}
		} else {
			final EntityTreeViewVO vo = new EntityTreeViewVO(null, 
					uidOriginEntity, 
					entity, 
					uidField, 
					foldername, 
					isActive, 
					sortOrder,
					isInherit, 
					uidFieldRoot, 
					idParentSubNode, 
					node, 
					nodeTooltip,
					groupBy,
					isShowEntityFolder
					);
			vo.flagNew();
			// insert new node entry 
			((ConfigurationTreeModel)view.getJTree().getModel()).insertNodeInto(new ConfigurationEntityTreeSubNode(vo), cnd, sortOrder);
		}
		
		// close dialog
//		this.dispose();
	}


	@Override
	public void performCancelAction() {
		// close dialog
//		this.dispose();
	}



}
