//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.searchfilter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.prefs.Preferences;

import org.nuclos.client.datasource.NuclosSearchConditionUtils;
import org.nuclos.client.genericobject.GenericObjectClientUtils;
import org.nuclos.client.genericobject.Modules;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableSorting;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.preferences.PreferencesUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.PreferencesException;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;

/**
 * An (module or masterdata) entity specific search filter, containing a user defined search condition, selection of visible columns
 * and sorting relevant columns.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * <p>
 * TODO: This looks *very* similiar to 
 * {@link org.nuclos.client.genericobject.resulttemplate.SearchResultTemplate}!
 * Perhaps we could unify both classes.
 * </p><p>
 * TODO: ModuleSearchFilter should contain a CollectableSearchExpression rather 
 * than a CollectableSearchCondition (along with other field's already contained in the expression).
 * </p>
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @author  <a href="mailto:corina.mandoki@novabit.de">Corina Mandoki</a>
 * @version 01.00.00
 */
public class EntitySearchFilter extends SearchFilter {

	/**
	 * list of visible columns
	 */
	private List<? extends CollectableEntityField> visibleFields;

	/**
	 * §todo Or better: the filter itself should contain a [GenericObject]CollectableSearchExpression
	 */
	private List<CollectableSorting> sortingOrder;

	public EntitySearchFilter() {
		this.sortingOrder = Collections.emptyList();
	}

	/**
	 * creates the default search filter with a specific name and without searchcondition.
	 * 
	 * §postcondition result.isDefaultFilter()
	 * §postcondition result.getSearchCondition() == null
	 * 
	 * @return new default search filter
	 */
	public static EntitySearchFilter newDefaultFilter() {
		final EntitySearchFilter result = new EntitySearchFilter() {
			@Override
			public boolean isDefaultFilter() {
				return true;
			}
		};
		final SpringLocaleDelegate sld = SpringLocaleDelegate.getInstance();
		final SearchFilterVO vo = result.getSearchFilterVO();
		vo.setFilterName(sld.getMessage("EntitySearchFilter.1", "<Alle>"));
		vo.setDescription(sld.getMessage("EntitySearchFilter.3", "Standardfilter (Keine Einschr\u00e4nkung)"));

		assert result.isDefaultFilter();
		assert result.getSearchCondition() == null;

		return result;
	}

	@Override
	public Object clone() {
		final EntitySearchFilter clone = (EntitySearchFilter) super.clone();
		if (this.visibleFields != null) {
			clone.visibleFields = new ArrayList<CollectableEntityField>(this.visibleFields);
		}
		if (this.sortingOrder != null) {
			clone.sortingOrder = new ArrayList<CollectableSorting>(this.sortingOrder);
		}
		return clone;
	}

	/**
	 * @return the internal search condition that is to be used for the actual search.
	 */
	@Override
	public CollectableSearchCondition getInternalSearchCondition() {
		final UID entity = getSearchFilterVO().getEntity();
		if (Modules.getInstance().isModule(entity)) {
			return GenericObjectClientUtils.getInternalSearchCondition(entity, getSearchCondition());
		}
		else {
			return getSearchCondition();
		}
	}

	/**
	 * @return the columns to be shown in the search result.
	 */
	public List<? extends CollectableEntityField> getVisibleColumns() {
		return visibleFields;
	}

	/**
	 * @param lstclctefweVisible the columns to be shown in the search result.
	 */
	public void setVisibleColumns(List<? extends CollectableEntityField> lstclctefweVisible) {
		this.visibleFields = lstclctefweVisible;
	}

	public List<CollectableSorting> getSortingOrder() {
		return sortingOrder;
	}

	/**
	 * @param sortingOrder List&lt;String&gt; the names of the columns defining the sorting of the result.
	 * Note that these columns must belong to the main entity.
	 */
	public void setSortingOrder(List<CollectableSorting> sortingOrder) {
		this.sortingOrder = sortingOrder;
	}

	/**
	 * @return this filter's search condition
	 */
	@Override
	public CollectableSearchCondition getSearchCondition() {
		return NuclosSearchConditionUtils.restorePlainSubConditions(super.getSearchCondition());
	}

	/**
	 * reads the search filter with the given name from the preferences.
	 * 
	 * §postcondition result != null
	 * 
	 * @param sFilterName
	 * @throws NoSuchElementException if there is no filter with the given name.
	 */
	static EntitySearchFilter get(String sFilterName, String sOwner, UID sEntity) {
		EntitySearchFilter result = SearchFilterCache.getInstance().getEntitySearchFilter(sFilterName, sOwner, sEntity);

		if (result == null) {
			throw new NoSuchElementException(SpringLocaleDelegate.getInstance().getMessage(
					"EntitySearchFilter.2", "Es existiert kein Suchfilter mit dem Namen {0}.", sFilterName));
		}

		assert result != null;
		return result;
	}

	public static void writeCollectableEntityFieldsToPreferences(Preferences prefs,
			List<? extends CollectableEntityField> lstclctefweSelected, String sPrefsNodeFields,
			String sPrefsNodeEntities) throws PreferencesException {

		final List<String> lstFieldNames = CollectionUtils.transform(lstclctefweSelected,
				new Transformer<CollectableEntityField, String>() {
					@Override
					public String transform(CollectableEntityField field) {
						return field.getUID().getString();
					}

				});
		PreferencesUtils.putStringList(prefs, sPrefsNodeFields, lstFieldNames);

		final List<String> lstEntityNames = CollectionUtils.transform(lstclctefweSelected,
				new CollectableEntityField.GetEntityUIDAsString());
		PreferencesUtils.putStringList(prefs, sPrefsNodeEntities, lstEntityNames);
	}

} // class ModuleSearchFilter
