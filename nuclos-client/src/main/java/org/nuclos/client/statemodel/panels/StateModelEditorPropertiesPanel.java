//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.statemodel.panels;

import java.awt.CardLayout;

import javax.swing.JPanel;

import org.nuclos.client.statemodel.StateModelEditor;
import org.nuclos.client.statemodel.controller.StatePropertiesController;
import org.nuclos.client.ui.UIUtils;

/**
 * Panel containing the properties for the various state model editor elements.
 *
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Boris.Sander@novabit.de">Boris Sander</a>
 * @version 01.00.00
 */
public class StateModelEditorPropertiesPanel extends JPanel {
	// @SuppressWarnings("unused") // created for constructor side-effects
	
	private final CardLayout cardLayout = new CardLayout();
	private final StatePropertiesPanel pnlStateProperties = new StatePropertiesPanel();
	private final NotePropertiesPanel pnlNote = new NotePropertiesPanel();

	private TransitionPropertiesPanel pnlTransitionProperties;
	
	private final JPanel pnlEmpty;

	// @SuppressWarnings({"deprecation", "unused"})
	private final StatePropertiesController ctlStateProperties;

	public StateModelEditorPropertiesPanel(StateModelEditor parent) {
		pnlEmpty = new StatePropertiesPanel();
		UIUtils.disableComponentsInContainer(pnlEmpty);
		pnlTransitionProperties = new TransitionPropertiesPanel(parent);
		ctlStateProperties = new StatePropertiesController(pnlStateProperties);
		this.init();
	}

	private void init() {
		this.setLayout(cardLayout);

		this.add(pnlStateProperties, "State");
		this.add(pnlTransitionProperties, "Transition");
		this.add(pnlNote, "Note");
		this.add(pnlEmpty, "None");
	}
	
	public TransitionPropertiesPanel getTransitionPropertiesPanel() {
		return this.pnlTransitionProperties;
	}
	
	public void setPanel(String sName) {
		cardLayout.show(this, sName);
	}

	public StatePropertiesPanel getStatePropertiesPanel() {
		return pnlStateProperties;
	}

	public NotePropertiesPanel getNotePanel() {
		return pnlNote;
	}
}	// class StateModelEditorPropertiesPanel
