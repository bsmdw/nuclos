//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.statemodel;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.nuclos.common.E;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeRemote;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.beans.factory.InitializingBean;

/**
 * Repository for roles.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Boris.Sander@novabit.de">Boris Sander</a>
 * @version 01.00.00
 */
public class RoleRepository implements InitializingBean {
	
	private static RoleRepository INSTANCE;
	
	//
	
	// Spring injection
	
	private MasterDataFacadeRemote masterDataFacadeRemote;
	
	// end of Spring injection

	private final Map<Object, MasterDataVO<UID>> mpRoles = CollectionUtils.newHashMap();

	public static RoleRepository getInstance() throws RemoteException {
		if (INSTANCE == null) {
			throw new IllegalStateException("too early");
		}
		return INSTANCE;
	}

	protected RoleRepository() throws RemoteException {
		INSTANCE = this;
	}
	
	public final void setMasterDataFacadeRemote(MasterDataFacadeRemote masterDataFacadeRemote) {
		this.masterDataFacadeRemote = masterDataFacadeRemote;
	}
	
	@Override
	public void afterPropertiesSet() {
		invalidate();
	}

	public void invalidate() {
		mpRoles.clear();

		try {
			for (MasterDataVO mdvo : masterDataFacadeRemote.getMasterDataWithCheck(E.ROLE.getUID(), null, true)) {
				mpRoles.put(mdvo.getPrimaryKey(), mdvo);
			}
		} catch (CommonPermissionException cpe) {
			// Should never happen
			throw new NuclosFatalException(cpe);
		}
	}

	public MasterDataVO<UID> getRole(UID iId) {
		return mpRoles.get(iId);
	}

	/**
	 *
	 * @param collID
	 * @return
	 */
	public List<MasterDataVO<UID>> selectRolesById(Collection<UID> collID) {
		final List<MasterDataVO<UID>> result = new LinkedList<MasterDataVO<UID>>();

		for (Iterator<UID> i = collID.iterator(); i.hasNext();) {
			final UID iId = i.next();
			final MasterDataVO<UID> cvo = mpRoles.get(iId);
			if (cvo != null) {
				result.add(cvo);
			}
		}
		return result;
	}

	/**
	 *
	 * @param collCVO
	 * @return
	 */
	public List<MasterDataVO<UID>> filterRolesByVO(Collection<MasterDataVO<UID>> collCVO) {
		Map<Object, MasterDataVO<UID>> filterMap = new HashMap<Object, MasterDataVO<UID>>();
		List<MasterDataVO<UID>> result = new LinkedList<MasterDataVO<UID>>();

		for (Iterator<MasterDataVO<UID>> i = collCVO.iterator(); i.hasNext();) {
			final MasterDataVO<UID> cvo = i.next();
			filterMap.put(cvo.getPrimaryKey(), cvo);
		}
		for (Iterator<MasterDataVO<UID>> i = mpRoles.values().iterator(); i.hasNext();) {
			final MasterDataVO<UID> cvo = i.next();
			if (!filterMap.containsKey(cvo.getPrimaryKey())) {
				result.add(cvo);
			}
		}
		return result;
	}
}
