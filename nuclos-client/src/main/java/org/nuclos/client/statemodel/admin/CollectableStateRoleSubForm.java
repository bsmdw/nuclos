//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.statemodel.admin;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.AbstractCollectable;
import org.nuclos.common.collect.collectable.AbstractCollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.DefaultCollectableEntityField;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.statemodel.valueobject.SubformPermissionVO;

/**
 * User role for a state (for state dependent user subform rights).
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:corina.mandoki@novabit.de">Corina Mandoki</a>
 * @version 01.00.00
 */
public class CollectableStateRoleSubForm extends AbstractCollectable<UID> {

	public static final UID FIELDNAME_ROLE = E.ROLESUBFORM.role.getUID();
	public static final UID FIELDNAME_SUBFORM = E.ROLESUBFORM.entity.getUID();
	public static final UID FIELDNAME_CANCREATE = E.ROLESUBFORM.create.getUID();
	public static final UID FIELDNAME_CANDELETE = E.ROLESUBFORM.delete.getUID();

	public static final UID entity = E.ROLESUBFORM.getUID();

	public static class Entity extends AbstractCollectableEntity {
		public Entity() {
			super(entity, SpringLocaleDelegate.getInstance().getMessage("CollectableStateRoleSubForm.9","Unterformulare f\u00fcr statusabh\u00e4ngige Rechte"));
			
			this.addCollectableEntityField(new DefaultCollectableEntityField(FIELDNAME_ROLE, String.class, 
					getSpringLocaleDelegate().getMessage("CollectableStateRoleSubForm.1","Benutzergruppe"),
					getSpringLocaleDelegate().getMessage("CollectableStateRoleSubForm.6","\u00dcbergeordnete Benutzergruppe (Rolle)"), 
					255, null, false, CollectableField.TYPE_VALUEIDFIELD, null, null, entity, null));
			
			this.addCollectableEntityField(new DefaultCollectableEntityField(FIELDNAME_SUBFORM, String.class, 
					getSpringLocaleDelegate().getMessage("CollectableStateRoleSubForm.7","Unterformular"), 
					getSpringLocaleDelegate().getMessage("CollectableStateRoleSubForm.8","Unterformular"), 
					255, null, false, CollectableField.TYPE_VALUEFIELD, null, null, entity, null));
			
			this.addCollectableEntityField(new DefaultCollectableEntityField(FIELDNAME_CANCREATE, Boolean.class,
					getSpringLocaleDelegate().getMessage("CollectableStateRoleSubForm.4","Anlegen erlaubt?"),
					getSpringLocaleDelegate().getMessage("CollectableStateRoleSubForm.4","Anlegen erlaubt?"),
					null, null, false, CollectableField.TYPE_VALUEFIELD, null, null, entity, null));

			this.addCollectableEntityField(new DefaultCollectableEntityField(FIELDNAME_CANDELETE, Boolean.class,
					getSpringLocaleDelegate().getMessage("CollectableStateRoleSubForm.5","Löschen erlaubt?"),
					getSpringLocaleDelegate().getMessage("CollectableStateRoleSubForm.5","Löschen erlaubt?"),
					null, null, false, CollectableField.TYPE_VALUEFIELD, null, null, entity, null));
		}
	}

	public static final CollectableEntity clcte = new Entity();

	private CollectableField clctfRole;
	private final SubformPermissionVO sfpvo;

	public CollectableStateRoleSubForm(CollectableField clctfRole, SubformPermissionVO sfpvo) {
		this.clctfRole = clctfRole;
		this.sfpvo = sfpvo;
	}
	
	protected CollectableEntity getCollectableEntity() {
		return clcte;
	}

	public SubformPermissionVO getSubformPermissionVO() {
		return this.sfpvo;
	}

	@Override
	public UID getId() {
		return sfpvo.getId();
	}

	@Override
	public void removeId() {
		sfpvo.setPrimaryKey(null);
	}

	/*
	@Override
	public String getIdentifierLabel() {
		return LangUtils.toString(this.getValue(FIELDNAME_SUBFORM));
	}
	 */

	/**
	 * @deprecated Not correctly implemented.
	 */
	@Override
	public int getVersion() {
		return -1;
	}

	@Override
	public CollectableField getField(UID fieldUid) throws CommonFatalException {
		final CollectableField result;

		if (fieldUid.equals(FIELDNAME_ROLE)) {
			result = this.clctfRole;
		}
		else if (fieldUid.equals(FIELDNAME_SUBFORM)) {
			result = new CollectableValueField(this.sfpvo.getSubform());
		}
		else if (fieldUid.equals(FIELDNAME_CANCREATE)) {
			result = new CollectableValueField(Boolean.valueOf(this.sfpvo.canCreate()));
		}
		else if (fieldUid.equals(FIELDNAME_CANDELETE)) {
			result = new CollectableValueField(Boolean.valueOf(this.sfpvo.canDelete()));
		}
		else {
			throw new IllegalArgumentException(getSpringLocaleDelegate().getMessage(
					"CollectableStateRoleSubForm.2","Feld nicht vorhanden: ") + fieldUid);
		}
		return result;
	}

	@Override
	public void setField(UID fieldUid, CollectableField clctfValue) {
		if (fieldUid.equals(FIELDNAME_ROLE)) {
			clctfRole = clctfValue;
		}
		else if (fieldUid.equals(FIELDNAME_SUBFORM)) {
			sfpvo.setSubform((UID) clctfValue.getValue());
		}
		else if (fieldUid.equals(FIELDNAME_CANCREATE)) {
			sfpvo.setCreate(((Boolean) clctfValue.getValue()).booleanValue());
		}
		else if (fieldUid.equals(FIELDNAME_CANDELETE)) {
			sfpvo.setDelete(((Boolean) clctfValue.getValue()).booleanValue());
		}
		else {
			throw new IllegalArgumentException(getSpringLocaleDelegate().getMessage(
					"CollectableStateRoleSubForm.3","Feld nicht vorhanden: ") + fieldUid);
		}

		assert this.getField(fieldUid).equals(clctfValue);
		this.bDirty = true;
	}

	@Override
	public UID getEntityUID() {
		return clcte.getUID();
	}

	@Override
	public UID getPrimaryKey() {
		return sfpvo.getPrimaryKey();
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("entity=").append(getCollectableEntity());
		result.append(",subformPermVo=").append(getSubformPermissionVO());
		result.append(",id=").append(getId());
		//Note: Swing (since J1.7 more than before) calls "toString()" often for whatever reason. getIdentifierLabel() shouldn't
		//be called therefore as it calls LocaleDelegate and thus uses lot of CPU Power.
//				result.append(",label=").append(getIdentifierLabel());
		result.append(",complete=").append(isComplete());
		result.append("]");
		return result.toString();
	}
	
	public static class MakeCollectable implements Transformer<SubformPermissionVO, CollectableStateRoleSubForm> {
		private final CollectableField clctfRole;

		public MakeCollectable(CollectableField clctfRole) {
			this.clctfRole = clctfRole;
		}

		@Override
		public CollectableStateRoleSubForm transform(SubformPermissionVO sfpvo) {
			return new CollectableStateRoleSubForm(clctfRole, sfpvo);
		}
	}	// inner class MakeCollectable

}
