//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JToolBar;

import org.apache.log4j.Logger;
import org.nuclos.client.common.KeyBindingProvider;
import org.nuclos.client.common.LafParameterHelper;
import org.nuclos.client.common.LafParameterHelper.BoolLafParameterEditor;
import org.nuclos.client.common.LafParameterHelper.GeneralLafParameterEditor;
import org.nuclos.client.common.LafParameterHelper.NotifyListener;
import org.nuclos.client.common.LafParameterHelper.ToolBarLafParameterEditor;
import org.nuclos.client.common.Utils;
import org.nuclos.client.main.MainController;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.ui.CenteringPanel;
import org.nuclos.client.ui.NuclosToolBar;
import org.nuclos.client.ui.PopupButton;
import org.nuclos.client.ui.StatusBarTextField;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.component.model.EditModel;
import org.nuclos.common.LafParameter;
import org.nuclos.common.LafParameterStorage;
import org.nuclos.common.UID;
import org.nuclos.common.collect.DetailsPresentation;
import org.nuclos.common.collect.NuclosToolBarItems;
import org.nuclos.common.collect.ToolBarConfiguration;
import org.nuclos.common.collect.ToolBarItem;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;

/**
 * The common base for <code>SearchPanel</code> and <code>DetailsPanel</code>.
 * Contains an <code>EditView</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * §todo extends CollectPanel.TabComponent?
 *
 * @author	<a href="mailto:christoph.radig@novabit.de">christoph.radig</a>
 * @version 01.00.00
 */
public abstract class SearchOrDetailsPanel extends MainFrameTab {

	private static final Logger LOG = Logger.getLogger(SearchOrDetailsPanel.class);
	
	private final JPanel main;
	
	/**
	 * the toolbar.
	 */
	private final NuclosToolBar toolBar = UIUtils.createNonFloatableToolBar();
	
	private final NotifyListener<ToolBarConfiguration,String> toolBarChangeListener = new NotifyListener<ToolBarConfiguration,String>() {
		@Override
		public void valueChanged(LafParameter<ToolBarConfiguration,String> parameter, LafParameterStorage storage, UID entityId, ToolBarConfiguration configuration) {
			toolBar.setToolBarItemGroup(configuration.getMainToolBar());
		}
	};

//	private int popbtnExtraIndex = -1;

	private final PopupButton popbtnExtra = new PopupButton(SpringLocaleDelegate.getInstance().getMessage(
			"PopupButton.Extras","Extras"));

	/**
	 * the center panel (between toolbar and status bar) containing the "edit component".
	 * <p>
	 * Non-final for close() (tp)
	 */
	protected CenteringPanel pnlCenter = new CenteringPanel(true);

	/**
	 * §invariant editview != null
	 * @see EditView
	 */
	protected EditView editview;

	/**
	 * The textfield in the status bar containing a message.
	 * 
	 * §todo encapsulate
	 */
	public final StatusBarTextField tfStatusBar = new StatusBarTextField(" ");
	
	private String sStatusBarText = null;
	private String sStatusBarSeparator = null;
	private String sStatusBarText2 = null;
	
	protected final UID entityId;
	
	protected final boolean searchPanel;

	protected SearchOrDetailsPanel(UID entityId, boolean bForSearch) {
		this.entityId = entityId;
		this.searchPanel = bForSearch;
		main = new JPanel(new BorderLayout());
		setLayeredComponent(main);

		this.popbtnExtra.putClientProperty(ToolBarItem.COMPONENT_PROPERTY_KEY, NuclosToolBarItems.EXTRAS);
		this.editview = new DefaultEditView(null, new DefaultCollectableComponentsProvider(), bForSearch, null);

		if (!bForSearch)
			showToolbar(false);
	}

	public final CenteringPanel getCenteringPanel() {
		return pnlCenter;
	}

	/**
	 * init after construct...
	 */
	protected void init() {
		if (searchPanel) {
			final ToolBarLafParameterEditor editor1 = new LafParameterHelper.NuclosSearchLafParameterEditor(
					LafParameter.nuclos_LAF_Tool_Bar_Search, entityId, toolBar);
			LafParameterHelper.installPopup(editor1, toolBarChangeListener);
		} else {
			final ToolBarLafParameterEditor editor2 = new LafParameterHelper.NuclosDetailLafParameterEditor(
					LafParameter.nuclos_LAF_Tool_Bar_Detail, entityId, toolBar);
			LafParameterHelper.installPopup(editor2, toolBarChangeListener);
			
			final GeneralLafParameterEditor<DetailsPresentation> editor3 = new GeneralLafParameterEditor<DetailsPresentation>(
					LafParameter.nuclos_LAF_Details_Presentation, entityId, toolBar);
			LafParameterHelper.installPopup(editor3);
			
			final BoolLafParameterEditor editor4 = new BoolLafParameterEditor(
					LafParameter.nuclos_LAF_Details_Navigation_Enabled, entityId, toolBar);
			LafParameterHelper.installPopup(editor4);
		}
		setupDefaultToolBarActions(toolBar);
		setNorthComponent(toolBar);
//		popbtnExtraIndex = getToolBarNextIndex();
//		if (popbtnExtra.getItemCount() > 0)
		
		
				
			final Action actSelectExtras = new AbstractAction() {
				@Override
	            public void actionPerformed(ActionEvent ev) {									
					popbtnExtra.doClick();					
				}
			};
			
			
			KeyBindingProvider.bindActionToComponent(KeyBindingProvider.MENU_EXTRAS, actSelectExtras, popbtnExtra);

			toolBar.add(popbtnExtra);
	}

//	public void updatePopupExtraVisibility() {
//		if (popbtnExtraIndex != -1 && toolBar.getComponentIndex(popbtnExtra) < 0) {
//			toolBar.add(popbtnExtra, popbtnExtraIndex);
//		}
//	}

	private void setNorthComponent(JComponent comp) {
		main.add(comp, BorderLayout.NORTH);
	}

	protected void setCenterComponent(JComponent comp) {
		main.add(comp, BorderLayout.CENTER);
	}

	protected void setSouthComponent(JComponent comp) {
		main.add(comp, BorderLayout.SOUTH);
	}

	protected abstract void setupDefaultToolBarActions( JToolBar toolBar);

	public void showToolbar(boolean show) {
		toolBar.setVisible(show);
	}

	public void addPopupExtraSeparator() {
//		updatePopupExtraVisibility();
		popbtnExtra.addSeparator();
	}

	public Component addPopupExtraComponent(Component comp) {
//		updatePopupExtraVisibility();
		return popbtnExtra.add(comp);
	}

	public void removePopupExtraComponent(Component comp) {
		popbtnExtra.remove(comp);
	}

	public JMenuItem addPopupExtraMenuItem(JMenuItem mi) {
//		updatePopupExtraVisibility();
		return popbtnExtra.add(mi);
	}

	public void removePopupExtrasMenuItem(JMenuItem mi) {
		popbtnExtra.remove(mi);
	}
	
	public PopupButton getPopButtonExtra() {
		return popbtnExtra;
	}

	/**
	 *
	 * @param comp
	 * @return index of comp in toolbar
	 */
	public int addToolBarComponent(Component comp) {
		toolBar.add(comp);
		toolBar.validate();
		return toolBar.getComponentIndex(comp);
	}

	/**
	 *
	 * @param comps
	 * @return index of comp in toolbar
	 */
	public int addToolBarComponents(List<Component> comps) {
		if (comps.size() == 0)
			return -1;

		for (Component comp : comps)
			toolBar.add(comp);
		toolBar.validate();
		return toolBar.getComponentIndex(comps.get(0));
	}

	public void addToolBarComponents(List<Component> comps, int index) {
		if (comps.size() == 0)
			return;

		// add last list entry first to toolbar
		List<Component> reversedComps = new ArrayList<Component>(comps);
		Collections.reverse(reversedComps);
		for (Component comp : reversedComps)
			toolBar.add(comp, index);
		toolBar.validate();
	}

	public int getToolBarNextIndex() {
		return toolBar.getComponentCount();
	}

	public int addToolBarSeparator() {
		toolBar.addSeparator();
		return toolBar.getComponentCount()-1;
	}

	public void addToolBarComponent(Component comp, int index) {
		toolBar.add(comp, index);
		toolBar.validate();
	}

	public void addToolBarHorizontalStruct(int width) {
		toolBar.add(Box.createHorizontalStrut(width));
	}

	public void removeToolBarComponent(Component comp) {
		toolBar.remove(comp);
		toolBar.revalidate();
	}

	public void removeToolBarComponents(List<Component> comps) {
		for (Component comp : comps)
			toolBar.remove(comp);
		toolBar.revalidate();
	}
	
	public void registerToolBarAction(ToolBarItem item, Action action) {
		toolBar.registerAction(item, action);
	}
	
	public void setToolBarConfiguration(ToolBarConfiguration configuration) {
		toolBar.setToolBarItemGroup(configuration.getMainToolBar());
	}

	/**
	 * §todo make private
	 * 
	 * @return the "edit component" containing all there is to edit (esp. the <code>CollectableComponent</code>s).
	 */
	public JComponent getEditComponent() {
		// could be null if controller has been closed
		final EditView view = getEditView();
		if (view == null) return null;
		return view.getJComponent();
	}

	/**
	 * §postcondition result != null
	 */
	public EditView getEditView() {
		return this.editview;
	}

	/**
	 * sets the edit view. This can be done dynamically.
	 * 
	 * §precondition editview != null
	 * §postcondition this.getEditView() == editview
	 */
	public void setEditView(EditView editview) {
		Component focusedComp = MainController.getMainFrame().getFocusOwner();
		if (focusedComp != null && this.getEditView() != null && this.getEditView().getJComponent() != null) {
			// If focused component is not in the to-be-replaced view, ignore it.
			if  (UIUtils.findJComponent(this.getEditView().getJComponent(), focusedComp) == null) {
				focusedComp = null;
			}
		}
		this.setEditComponent(editview.getJComponent());

		this.editview = editview;

		// NUCLOS-7519 Request focus if lost by replacing
		if (focusedComp != null) {
			if (editview.getJComponent() != null) {
				editview.getJComponent().requestFocus();
			}
			Utils.setInitialComponentFocus(editview, null);
		}

		assert this.getEditView() == editview;
	}

	/**
	 * @return the model of the edit view.
	 * @see #getEditView()
	 */
	public abstract EditModel getEditModel();

	/**
	 * sets the "edit component". This can be done dynamically.
	 * 
	 * §precondition compEdit != null
	 * 
	 * @param compEdit
	 * @see #getEditComponent()
	 */
	private void setEditComponent(JComponent compEdit) {
		this.pnlCenter.setCenteredComponent(compEdit);
	}

	public void setStatusBarText(String sText) {
		this.sStatusBarText = sText;
		this.setStatusBarText();
	}
	
	public void setStatusBarText2(String sText2) {
		setStatusBarText2(sText2, null);
	}
	
	public void setStatusBarText2(String sText2, String sSeparator) {
		this.sStatusBarText2 = sText2;
		this.sStatusBarSeparator = sSeparator;
		this.setStatusBarText();
	}
	
	public void setStatusBarText() {
		String text = StringUtils.looksEmpty(sStatusBarText)?"":sStatusBarText;
		if (!StringUtils.looksEmpty(sStatusBarText) && !StringUtils.looksEmpty(sStatusBarText2)) {
			text = text + (sStatusBarSeparator==null?" ":sStatusBarSeparator);
		}
		text = text + (StringUtils.looksEmpty(sStatusBarText2)?"":sStatusBarText2);
		this.tfStatusBar.setText(text);
		this.tfStatusBar.setCaretPosition(0);
		final String sStatusMultiLine = StringUtils.splitIntoSeparateLines(text, 100);
		final String sStatusToolTip = "<html>" + sStatusMultiLine.replaceAll("\n", "<br>") + "</html>";
		this.tfStatusBar.setToolTipText(sStatusToolTip);
	}

	public NuclosToolBar getToolBar() {
		return toolBar;
	}
	
	public void closeToolBar() {
		toolBar.close();
	}
}	// class SearchOrDetailsPanel
