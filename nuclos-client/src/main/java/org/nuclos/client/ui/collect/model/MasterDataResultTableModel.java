package org.nuclos.client.ui.collect.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.common.CollectableEntityFieldWithEntity;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

public class MasterDataResultTableModel<PK,Clct extends Collectable<PK>> extends SortableCollectableTableModelImpl<PK,Clct> {

	private final CollectableEntity entity;

	private MasterDataResultTableModel(CollectableEntity entity) {
		super(entity.getUID());
		this.entity = entity;
	}

	public MasterDataResultTableModel(CollectableEntity entity, List<? extends CollectableEntityField> list) {
		this(entity);
		setColumns(list);
	}

	@Override
	public CollectableField getValueAt(int iRow, int iColumn) {
		final Collectable clct = this.getCollectable(iRow);
		final CollectableEntityFieldWithEntity clctefwe = (CollectableEntityFieldWithEntity) this.getCollectableEntityField(iColumn);
		final UID sFieldName = clctefwe.getUID();
		final UID sFieldEntityName = clctefwe.getCollectableEntityUID();
		final UID sMainEntityName = getBaseEntityUid();

		CollectableField result;
		if (sFieldEntityName.equals(sMainEntityName)) {
			result = clct.getField(sFieldName);
		}
		else {
			final MasterDataVO mdwdvo = ((CollectableMasterDataWithDependants) clct).getMasterDataWithDependantsCVO();
			Set<IDependentKey> dependentKeys = mdwdvo.getDependents().getKeySet();
			for (IDependentKey dependentKey : dependentKeys) {
				UID refFieldEntityUid = MetaProvider.getInstance().getEntityField(dependentKey.getDependentRefFieldUID()).getEntity();
				if (refFieldEntityUid.equals(sFieldEntityName)) {
					//subform / key found
					final Collection<EntityObjectVO<?>> collmdvo = mdwdvo.getDependents().getData(dependentKey);
					result = new CollectableValueField(getValues(collmdvo, sFieldName));
				}
			}
			// subform not found
			result = new CollectableValueField(null);
		}

		return result;
	}

	public static List<Object> getValues(Collection<EntityObjectVO<?>> collmdvo, UID sFieldName) {
		final List<Object> result = new ArrayList<Object>();
		if (collmdvo != null) {
			for (Iterator<EntityObjectVO<?>> iter = collmdvo.iterator(); iter.hasNext();) {
				final EntityObjectVO<?> mdvo = iter.next();
				result.add(mdvo.getFieldValue(sFieldName, Object.class));
			}
		}
		return result;
	}
}
