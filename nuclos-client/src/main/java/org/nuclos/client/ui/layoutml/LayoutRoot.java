//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.layoutml;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JPanel;

import org.apache.log4j.Logger;
import org.nuclos.api.ui.layout.LayoutComponent;
import org.nuclos.client.ui.collect.CollectableComponentsProvider;
import org.nuclos.client.ui.collect.DefaultCollectableComponentsProvider;
import org.nuclos.client.ui.collect.DefaultLayoutComponentsProvider;
import org.nuclos.client.ui.collect.LayoutComponentsProvider;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModel;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelProvider;
import org.nuclos.client.ui.collect.component.model.DefaultDetailsEditModel;
import org.nuclos.client.ui.collect.component.model.DefaultSearchEditModel;
import org.nuclos.client.ui.collect.component.model.DetailsEditModel;
import org.nuclos.client.ui.collect.component.model.SearchEditModel;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.client.ui.matrix.JMatrixComponent;
import org.nuclos.common.UID;
import org.nuclos.common.collection.multimap.MultiListHashMap;
import org.nuclos.common.collection.multimap.MultiListMap;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.LangUtils;

/**
 * represents the output of the LayoutML parser.
 * Contains the root panel and the maps of collectable components, their models and subforms.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * 
 * §todo this would better be called LayoutMLParser.Output or LayoutMLParser.Result
 * §todo generify &lt;Clctcompmodel extends CollectableComponentModel&gt; CollectableComponentModelProvider
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class LayoutRoot<PK> implements CollectableComponentsProvider, CollectableComponentModelProvider, LayoutComponentsProvider<PK> {

	private static final Logger log = Logger.getLogger(LayoutRoot.class);

	/**
	 * true: Search components, false: Details components
	 */
	private final boolean bForSearch;

	/**
	 * the root component.
	 * @invariant != null
	 */
	private final JComponent compRoot;

	/**
	 * maps a field name to the associated CollectableComponents.
	 * @invariant != null
	 */
	private final CollectableComponentsProvider clctcompprovider;
	
	private final LayoutComponentsProvider<PK> layoutComponentsProvider;

	/**
	 * maps a field name to the associated CollectableComponentModel.
	 * @invariant != null
	 */
	private final Map<UID, CollectableComponentModel> mpclctcompmodel;

	/**
	 * maps an entity name to the associated subform.
	 */
	private final Map<UID, SubForm> mpsubform;
	
	/**
	 * maps an entity name to the associated matrix.
	 */
	private final Map<UID, JMatrixComponent> mpMatrix;

	/**
	 * <code>MultiMap</code> of parsed inter-field dependencies.
	 * Maps a field name <code>x</code> to a <code>Collection</code> of field names that <code>x</code> depends on.
	 */
	private final MultiListMap<UID, UID> mmpDependencies;

	private List<UID> lstOrderedFieldNames;

	private final EntityAndField eafnInitialFocus;

	/**
	 * creates the <code>LayoutRoot</code> out of its components.
	 * 
	 * §precondition compRoot != null
	 * §precondition clctcompprovider != null
	 * §precondition mpclctcompmodel != null
	 * §precondition clctcompprovider.keySet().equals(mpclctcompmodel.keySet())
	 * 
	 * @param bForSearch
	 * @param compRoot
	 * @param clctcompprovider
	 * @param mpsubform
	 * @param mmpDependencies
	 * @param eafnInitialFocus the entity name and field name of the component that is to get the focus initially.
	 */
	public LayoutRoot(boolean bForSearch, JComponent compRoot, CollectableComponentsProvider clctcompprovider, LayoutComponentsProvider layoutComponentsProvider,
			Map<UID, CollectableComponentModel> mpclctcompmodel,
			Map<UID, SubForm> mpsubform, Map<UID, JMatrixComponent> mpmatrix, MultiListMap<UID, UID> mmpDependencies,
			EntityAndField eafnInitialFocus) {

		// @todo check consistency of the field names or create the model out of the view right here.
//		if(!clctcompprovider.keySet().equals(mpclctcompmodel.keySet())) {
//			throw new IllegalArgumentException("clctcompprovider and mpclctcompmodel do not match.");
//		}

		this.bForSearch = bForSearch;
		this.compRoot = compRoot;
		this.clctcompprovider = clctcompprovider;
		this.mpclctcompmodel = mpclctcompmodel;
		this.mpsubform = mpsubform;
		this.mpMatrix = mpmatrix;
		this.mmpDependencies = mmpDependencies;
		this.eafnInitialFocus = eafnInitialFocus;
		this.layoutComponentsProvider = layoutComponentsProvider;
	}

	/**
	 * @return a new empty <code>LayoutRoot</code>.
	 * @param bForSearch
	 */
	public static <PK2> LayoutRoot<PK2> newEmptyLayoutRoot(boolean bForSearch) {
		return new LayoutRoot<PK2>(bForSearch, new JPanel(), new DefaultCollectableComponentsProvider(), new DefaultLayoutComponentsProvider<PK2>(),
				new HashMap<UID, CollectableComponentModel>(0), new HashMap<UID, SubForm>(), new HashMap<UID, JMatrixComponent>(),
				new MultiListHashMap<UID, UID>(), new EntityAndField((UID) null, UID.UID_NULL));
	}

	public boolean isForSearch() {
		return this.bForSearch;
	}

	public boolean isForDetails() {
		return !this.isForSearch();
	}

	/**
	 * §precondition this.isForSearch()
	 */
	public SearchEditModel getSearchEditModel() {
		if (!this.isForSearch()) {
			throw new IllegalStateException("this.isForSearch()");
		}
		return new DefaultSearchEditModel(this.getCollectableComponents());
	}

	/**
	 * §precondition this.isForDetails()
	 */
	public DetailsEditModel getDetailsEditModel() {
		if (!this.isForDetails()) {
			throw new IllegalStateException("this.isForDetails()");
		}
		return new DefaultDetailsEditModel(this.getCollectableComponents());
	}

	/**
	 * §postcondition result != null
	 * 
	 * @return the root component generated from the LayoutML parser.
	 */
	public JComponent getRootComponent() {
		return this.compRoot;
	}

	/**
	 * §postcondition result != null
	 * 
	 * @return the <code>CollectableComponent</code>s that were constructed by the parser.
	 */
	@Override
	public Collection<CollectableComponent> getCollectableComponents() {
		return this.clctcompprovider.getCollectableComponents();
	}
	
	/**
	 * NUCLEUSINT-442
	 * 
	 * §postcondition result != null
	 * 
	 * @return the <code>CollectableComponent Labels</code>s that were constructed by the parser.
	 */
	@Override
	public Collection<CollectableComponent> getCollectableLabels() {
		return this.clctcompprovider.getCollectableLabels();
	}

	/**
	 * §postcondition result != null
	 * 
	 * @param fieldUid
	 * @return the <code>CollectableComponent</code>s with the given field name that were constructed by the parser.
	 */
	@Override
	public Collection<CollectableComponent> getCollectableComponentsFor(UID fieldUid) {
		return this.clctcompprovider.getCollectableComponentsFor(fieldUid);
	}
	
	
	/**
	 * §postcondition result != null
	 * 
	 * @return the {@link LayoutComponent} that were constructed by the parser.
	 */
	@Override
	public Collection<LayoutComponent<PK>> getLayoutComponents() {
		return this.layoutComponentsProvider.getLayoutComponents();
	}
	
	/**
	 * §postcondition result != null
	 * 
	 * @return the {@link LayoutComponent} that were constructed by the parser.
	 */
	@Override
	public Collection<LayoutComponent<PK>> getLayoutComponentsFor(final String name) {
		return this.layoutComponentsProvider.getLayoutComponentsFor(name);
	}

	/**
	 * @return Map&lt;UID, CollectableComponentModel&gt; the map of parsed/constructed collectable component models.
	 * Maps a field name to a <code>CollectableComponentModel</code>.
	 * 
	 * @deprecated LayoutRoot implements CollectableComponentModelProvider - no need to return a map here.
	 */
	@Deprecated
	public Map<UID, ? extends CollectableComponentModel> getMapOfCollectableComponentModels() {
		return this.mpclctcompmodel;
	}

	@Override
	public Collection<? extends CollectableComponentModel> getCollectableComponentModels() {
		return Collections.unmodifiableCollection(this.mpclctcompmodel.values());
	}

	/**
	 * @param fieldUid
	 * @return the <code>CollectableComponentModel</code> with the given field name.
	 */
	@Override
	public CollectableComponentModel getCollectableComponentModelFor(UID fieldUid) {
		return this.mpclctcompmodel.get(fieldUid);
	}

	/**
	 * @return Map&lt;UID sEntityUid, SubForm&gt; the map of parsed/constructed subforms.
	 */
	public Map<UID, SubForm> getMapOfSubForms() {
		return this.mpsubform;
	}
	
	/**
	 * @return Map&lt;UID, JMatrixComponent&gt; the map of parsed/constructed JMatrixComponent.
	 */
	public Map<UID, JMatrixComponent> getMapOfMatrix() {
		return this.mpMatrix;
	}

	/**
	 * @return an unordered and unmodifiable <code>Collection</code> of the parsed field names.
	 */
	@Override
	public Collection<UID> getFieldUids() {
		return Collections.unmodifiableCollection(this.mpclctcompmodel.keySet());
	}

	/**
	 * @return List&lt;UID&gt; an ordered unmodifiable <code>List</code> of the parsed field names.
	 * The order respects the dependencies between fields:
	 * The order is such that if <code>o1</code> depends on <code>o2</code>,
	 * <code>o1</code> comes after <code>o2</code> in the list.
	 */
	public List<UID> getOrderedFieldUIDs() {
		if (this.lstOrderedFieldNames == null) {
			final List<UID> lst = new ArrayList<UID>(this.getFieldUids());
			Collections.sort(lst, new DependencyComparator(this.mmpDependencies));
			this.lstOrderedFieldNames = Collections.unmodifiableList(lst);
			/* 
			 * §todo OPTIMIZE: It would be okay to forget the multimap of dependencies here. 
			 */
		}
		return this.lstOrderedFieldNames;
	}

	/**
	 * @return the entity name and field name of the component, if any, that is to get the focus initially.
	 * <code>null</code> means there is no such component.
	 */
	public EntityAndField getInitialFocusEntityAndField() {
		return this.eafnInitialFocus;
	}

	/**
	 * Comparator for field names that takes the dependencies into account.
	 */
	private static class DependencyComparator implements Comparator<UID> {

		private final MultiListMap<UID, UID> mmapDependencies;

		DependencyComparator(MultiListMap<UID, UID> mmpDependencies) {
			this.mmapDependencies = mmpDependencies;
		}

		/**
		 * The following rules are applied (in this order):
		 * 1. sField1Name > sField2Name if there is a dependency sField1Name -> sField2Name
		 * 2. sField1Name < sField2Name if there is a dependency sField2Name -> sField1Name
		 * 3. sField1Name > sField2Name if there is a dependency sField1Name -> anything
		 * 4. sField1Name < sField2Name if there is a dependency sField2Name -> anything
		 * 5. sField1Name == sField2Name otherwise.
		 * @param field1Uid
		 * @param field2Uid
		 */
		@Override
		public int compare(UID field1Uid, UID field2Uid) {
			final int result = compareInternal(field1Uid, field2Uid);
			if ((compareInternal(field2Uid, field1Uid)) == result)
				return 0; // @see NUCLOS-2245 prevent java.lang.IllegalArgumentException: Comparison method violates its general contract! at java.util.TimSort.mergeLo(Unknown Source)
			
			if (log.isDebugEnabled()) {
				log.debug("compare(" + field1Uid + "," + field2Uid + ") = " + result);
			}
			return result;
		}
		public int compareInternal(UID field1Uid, UID field2Uid) {
			final int result;
			
			if (LangUtils.equal(field1Uid, field2Uid))
				return 0;
			
			if (dependsOn(field1Uid, field2Uid)) {
				result = 1;
			}
			else if (dependsOn(field2Uid, field1Uid)) {
				result = -1;
			}
			else {
				if (isDependant(field1Uid)) {
					// sField1Name is potentially greater than sField2Name.
					result = 1;
				}
				else if (isDependant(field2Uid)) {
					// sField1Name is potentially less than sField2Name.
					result = -1;
				}
				else {
					// both elements are neutral. Their order doesn't matter.
					result = 0;
				}
			}
			return result;
		}

		/**
		 * @param field1Uid
		 * @param field2Uid
		 * @return Does <code>sField1Name</code> depend on <code>sField2Name</code>?
		 */
		private boolean dependsOn(UID field1Uid, UID field2Uid) {
			return this.mmapDependencies.getValues(field1Uid).contains(field2Uid);
		}

		/**
		 * @param fieldUid
		 * @return Is <code>sFieldName</code> dependant at all, that is:
		 * Does <code>sFieldName</code> occur on any left side of a dependency?
		 */
		private boolean isDependant(UID fieldUid) {
			return this.mmapDependencies.containsKey(fieldUid);
		}

	}	// inner class DependencyComparator

}	// class LayoutRoot
