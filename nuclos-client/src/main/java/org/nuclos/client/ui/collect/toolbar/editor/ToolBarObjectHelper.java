//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.toolbar.editor;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Icon;

import org.nuclos.client.ui.collect.toolbar.INuclosToolBarItem;
import org.nuclos.client.ui.collect.toolbar.NuclosToolBarActions;
import org.nuclos.client.ui.collect.toolbar.ToolBarItemAction;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.collect.NuclosToolBarItems;
import org.nuclos.common.collect.ToolBarItem;
import org.nuclos.common.collect.ToolBarItem.Type;
import org.nuclos.common2.SpringLocaleDelegate;

class ToolBarObjectHelper {
	
	private static final SpringLocaleDelegate LOCALE = SpringLocaleDelegate.getInstance();
	
	private static Map<ToolBarItem, ToolBarItemAction> ACTIONS = new HashMap<ToolBarItem, ToolBarItemAction>();

	private static String getLabel(ToolBarItem item) {
		INuclosToolBarItem action = getAction(item);
		if (action != null) {
			return action.getLabel();
		}
		String resid = item.getKey();
		if (NuclosToolBarItems.LIST_PROFILE.equals(item)) {
			resid = "Workspace.Profile.ProfileMenu.10";
		} else if (NuclosToolBarItems.SEARCH_DELETED.equals(item)) {
			resid = "ToolBarObjectHelper.1";
		} else if (NuclosToolBarItems.TEXT_SEARCH.equals(item)) {
			resid = "text_search";
		} else if (NuclosToolBarItems.SEARCH_STATE.equals(item)) {
			resid = "attrState";
		}
		return LOCALE.getResource(resid, item.getKey());
	}
	
	private static String getTooltip(ToolBarItem item) {
		INuclosToolBarItem action = getAction(item);
		if (action != null) {
			return action.getTooltip();
		}
		return getLabel(item);
	}
	
	private static Icon getIcon(ToolBarItem item) {
		INuclosToolBarItem action = getAction(item);
		if (action != null) {
			return action.getIcon();
		}
		return null;
	}
	
	private static INuclosToolBarItem getAction(ToolBarItem item) {
		if (item.getType() == Type.ACTION || 
			item.getType() == Type.TOGGLE_ACTION ||
			item.getType() == Type.PSD_ACTION ||
			item.getType() == Type.PR_ACTION ||
			item.getType() == Type.PSR_ACTION) {
			init();
			return ACTIONS.get(item);
		}
		return null;
	}
	
	private static void init() {
		if (ACTIONS.isEmpty()) {
			Field[] fields = NuclosToolBarActions.class.getFields();
			for (Field field : fields) {
				try {
					Object item = field.get(null);
					if (item instanceof ToolBarItemAction) {
						ToolBarItemAction action = (ToolBarItemAction) item;
						ACTIONS.put(action, action);
					}
				} catch (Exception e) {
					throw new NuclosFatalException(e);
				} 
			}
		}
	}
	
	static ToolBarObject createToolBarObject(ToolBarItem item, ToolBarConfigurationEditor editor) {
		item = item.clone();
		String sShowText = item.getProperty(ToolBarItem.PROPERTY_SHOW_TEXT);
		boolean toolLabel = sShowText != null || Boolean.parseBoolean(sShowText);
		boolean menuLabel = true;
		boolean toolIcon = true;
		boolean menuIcon = true;
		
		String label = getLabel(item);
		String tooltip = getTooltip(item);
		Icon icon = getIcon(item);
		
		if (icon == null && toolLabel == false) {
			toolLabel = true;
		}
		
		ToolBarObject result = new ToolBarObject(
				item, 
				label, tooltip,
				icon, 
				toolLabel, menuLabel, toolIcon, menuIcon, editor);
		return result;
	}
}
