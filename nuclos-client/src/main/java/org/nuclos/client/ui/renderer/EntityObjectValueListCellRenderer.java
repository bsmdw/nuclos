package org.nuclos.client.ui.renderer;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.nuclos.client.ui.FormatUtils;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.collect.collectable.CollectableFieldFormat;
import org.nuclos.common.dal.vo.EntityObjectVO;

/**
 * Render a <em>value</em> field of an EntityObjectVO as list representation.
 * 
 * @param <PK> primary key of EntityObjectVO
 * 
 * @author Thomas Pasch
 * @since Nuclos 4.1
 */
public class EntityObjectValueListCellRenderer<PK> extends JLabel implements ListCellRenderer {
	
	private final FieldMeta<?> field;
	
	private final CollectableFieldFormat format;
	
	public EntityObjectValueListCellRenderer(FieldMeta<?> field) {
		this.field = field;
		this.format = FormatUtils.getCollectableFieldFormat(field);
	}

	@Override
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
			boolean cellHasFocus) {
		
		final EntityObjectVO<PK> eo = (EntityObjectVO<PK>) value;
		setText(format.format(field.getFormatOutput(), eo.getFieldValue(field.getUID())));
		return this;
	}

}
