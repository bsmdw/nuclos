package org.nuclos.client.ui.collect.search;

import java.util.List;

import org.nuclos.client.common.MetaProvider;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.searchcondition.SearchConditionUtils;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;

/**
 * Created by Oliver Brausch on 21.09.17.
 */
public final class ClientSearchUtils {
	private ClientSearchUtils() {
	}

	public static void addTextSearchToSearchExpression(String search, List<CollectableEntityField> cefs,
													   CollectableSearchExpression clctexpr, UID sEntity) {
		SearchConditionUtils.addTextSearchToSearchExpression(search, cefs, clctexpr, sEntity, MetaProvider.getInstance());
	}
}
