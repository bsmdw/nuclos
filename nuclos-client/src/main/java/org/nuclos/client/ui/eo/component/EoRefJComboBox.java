//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.eo.component;

import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;

import org.nuclos.client.ui.eo.mvc.IModelAwareGuiComponent;
import org.nuclos.client.ui.model.SortedListModel;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectUtils;
import org.nuclos.common.dal.vo.EntityObjectVO;

public class EoRefJComboBox<PK> extends JComboBox implements IModelAwareGuiComponent<PK> {
	
	private final UID writeRefTo;
	
	private EntityObjectVO<PK> associatedEo;
	
	public EoRefJComboBox(SortedListModel<EntityObjectVO<PK>> model, UID writeRefTo) {
		super(model);
		this.writeRefTo = writeRefTo;
	}
	
	@Override
    public void setModel(ComboBoxModel aModel) {
		super.setModel((SortedListModel<EntityObjectVO<PK>>) aModel);
	}

	@Override
	public void fromComponentToModel() {
		final EntityObjectVO<PK> selected = (EntityObjectVO<PK>) getSelectedItem();
		final PK guiPk;
		if (selected != null) {
			guiPk = selected.getPrimaryKey();
		} else {
			guiPk = null;
		}
		if (guiPk != null) {
			if (guiPk instanceof UID) {
				EntityObjectUtils.setOrUpdateUid(associatedEo, writeRefTo, (UID) guiPk);
				// to.setFieldUid(writeRefTo, (UID) guiPk);
			} else {
				EntityObjectUtils.setOrUpdateId(associatedEo, writeRefTo, (Long) guiPk);
				// to.setFieldId(writeRefTo, (Long) guiPk);
			}
		} else {
			EntityObjectUtils.setOrUpdateUid(associatedEo, writeRefTo, null);
			EntityObjectUtils.setOrUpdateId(associatedEo, writeRefTo, null);
			// to.setFieldId(writeRefTo, null);
			// to.setFieldUid(writeRefTo, null);
		}
	}

	@Override
	public void fromModelToComponent() {
		PK modelPk = (PK) associatedEo.getFieldId(writeRefTo);
		if (modelPk == null) {
			modelPk = (PK) associatedEo.getFieldUid(writeRefTo);
		}
		if (modelPk == null) {
			setSelectedItem(null);
		} else {
			boolean found = false;
			final SortedListModel<EntityObjectVO<PK>> model = (SortedListModel<EntityObjectVO<PK>>) getModel();
			EntityObjectVO<PK> eo = null;
			for (int i = 0; i < model.getSize(); ++i) {
				eo = model.getElementAt(i);
				if (modelPk.equals(eo.getPrimaryKey())) {
					found = true;
					break;
				}
			}
			if (found) {
				setSelectedItem(eo);
			} else {
				throw new IllegalStateException();
			}
		}
	}

	@Override
	public void setAssociatedEntityObject(EntityObjectVO<PK> eo) {
		this.associatedEo = eo;
	}

	@Override
	public EntityObjectVO<PK> getAssociatedEntityObject() {
		return associatedEo;
	}

}
