package org.nuclos.client.ui.labeled;

import java.awt.Component;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.nuclos.common.collect.collectable.CollectableValueIdField;

public class CheckComboRenderer<E> implements ListCellRenderer {
	JCheckBox checkBox;
	JLabel label;

    public CheckComboRenderer() {
    	checkBox = new JCheckBox();
    	label = new JLabel();
    }

    public Component getListCellRendererComponent(JList list,
                                                  Object value,
                                                  int index,
                                                  final boolean isSelected,
                                                  boolean cellHasFocus) {
		if (value instanceof CollectableValueIdField) {
			CollectableValueIdField valueIdField = (CollectableValueIdField) value;
			checkBox.setOpaque(true);
			if (isSelected) {
				checkBox.setBackground(list.getSelectionBackground());
				checkBox.setForeground(list.getSelectionForeground());
			} else {
				checkBox.setBackground(list.getBackground());
				checkBox.setForeground(list.getForeground());
			}
			if (valueIdField.getValue() != null) {
				checkBox.setText(valueIdField.toString());
				checkBox.setSelected(valueIdField.isSelected());
			    return checkBox;
			    
			}
		} else if (value instanceof String) {
			label.setText((String)value);
			
		} else if (value != null) {
			label.setText(value.toString());
			
		}
		return label;
    }
}
