//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.event;

import javax.swing.JTable;

/**
 * Interface for sources of {@link TablePopupMenuEvent}s.
 * <p>
 * Part of the support abstraction for popup menus on {@link JTable}s needed
 * for http://project.nuclos.de/browse/LIN-165.
 * 
 * @author Thomas Pasch
 * @since Nuclos 4.3.0
 */
public interface ITablePopupMenuEventSource {
	
	void addTablePopupMenuListener(ITablePopupMenuListener listener);
	
	void removeTablePopupMenuListener(ITablePopupMenuListener listener);

}
