package org.nuclos.client.ui.eo.component;

import javax.swing.event.DocumentEvent;
import javax.swing.event.UndoableEditEvent;
import javax.swing.text.PlainDocument;

public class SwitchableNotificationDocument extends PlainDocument {
	
	private volatile boolean notifyListeners = true;
	
	public SwitchableNotificationDocument() {
	}
	
	public SwitchableNotificationDocument(Content content) {
		super(content);
	}

    protected void fireInsertUpdate(DocumentEvent e) {
    	if (notifyListeners) {
    		super.fireInsertUpdate(e);
    	}
    }

    protected void fireChangedUpdate(DocumentEvent e) {
    	if (notifyListeners) {
    		super.fireChangedUpdate(e);
    	}
    }

    protected void fireRemoveUpdate(DocumentEvent e) {
    	if (notifyListeners) {
    		super.fireRemoveUpdate(e);
    	}
    }

    protected void fireUndoableEditUpdate(UndoableEditEvent e) {
    	if (notifyListeners) {
    		super.fireUndoableEditUpdate(e);
    	}
    }

	public boolean isNotifyListeners() {
		return notifyListeners;
	}

	public void setNotifyListeners(boolean notifyListeners) {
		this.notifyListeners = notifyListeners;
	}
	
}
