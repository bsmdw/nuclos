//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.ToolTipManager;
import javax.swing.table.TableCellRenderer;

import org.apache.log4j.Logger;
import org.jdesktop.jxlayer.JXLayer;
import org.nuclos.client.common.Utils;
import org.nuclos.client.genericobject.CollectableGenericObjectAttributeField;
import org.nuclos.client.masterdata.CollectableMasterDataField;
import org.nuclos.client.ui.TriStateCheckBox;
import org.nuclos.client.ui.collect.component.model.SearchComponentModelEvent;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableUtils;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.searchcondition.AtomicCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparisonDateValues;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparisonWithOtherField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparisonWithParameter;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIsNullCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableLikeCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.visit.AtomicVisitor;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common2.LangUtils;

/**
 * A <code>CollectableComponent</code> that presents a value in a <code>JCheckBox</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version	01.00.00
 */
public class CollectableCheckBox extends AbstractCollectableComponent {

	private static final Logger LOG = Logger.getLogger(CollectableCheckBox.class);

	private final ItemListener itemlistener = new ItemListener() {
		@Override
        public void itemStateChanged(ItemEvent ev) {
			try {
				CollectableCheckBox.this.viewToModel();
			}
			catch (CollectableFieldFormatException ex) {
				// this must never happen for a CollectableCheckBox
				assert false;
			}
		}
	};

	/**
	 * §postcondition this.isDetailsComponent()
	 */
	public CollectableCheckBox(CollectableEntityField clctef) {
		this(clctef, false);

		assert this.isDetailsComponent();
	}
	
	public CollectableCheckBox(CollectableEntityField clctef, boolean bSearchable) {
		super(clctef, new InnerCheckBox(), bSearchable);

		final InnerCheckBox chkbx = this.getTriStateCheckBox();		
		chkbx.setBackgroundColorProvider(new BackgroundColorProvider());
				
		chkbx.addFocusListener(new FocusListener() {
			
			Color color;
			Color colorFocusBackground = Utils.translateColorFromParameter(ParameterProvider.KEY_FOCUSSED_ITEM_BACKGROUND_COLOR);
			
			@Override
			public void focusLost(FocusEvent ev) {		
				Color col = _getBackgroundColor(false);
				if(col != null && col.equals(color)){
					ev.getComponent().setBackground(color);				
				}
				else {
					ev.getComponent().setBackground(col);
				}		
				((JComponent)ev.getComponent()).setOpaque(false);
			}
			
			@Override
			public void focusGained(FocusEvent ev) {
				color = ev.getComponent().getBackground();
				if (ev.getComponent() instanceof JComponent) {
					// assign focus color
					((JComponent)ev.getComponent()).setOpaque(true);
					ev.getComponent().setBackground(colorFocusBackground);
				}
			}
		});

		chkbx.setToolTipTextProvider(this);
		ToolTipManager.sharedInstance().registerComponent(chkbx);

		chkbx.addItemListener(this.itemlistener);
		chkbx.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {

					if (CollectableCheckBox.this.isSearchComponent()) {
						if (//ItemEvent.SELECTED == e.getStateChange() && //(see LIN-793)
								(ComparisonOperator.NONE == CollectableCheckBox.this.getComparisonOperator())) {
							CollectableCheckBox.this.setComparisonOperator(ComparisonOperator.EQUAL);
						}
					}				
			}
		});
	}

	@Override
	public JCheckBox getJComponent() {
		return (JCheckBox) super.getJComponent();
	}

	public JCheckBox getJCheckBox() {
		return this.getJComponent();
	}

	private InnerCheckBox getTriStateCheckBox() {
		return (InnerCheckBox) super.getJComponent();
	}

	@Override
	protected void updateView(CollectableField clctfValue) {
		final Boolean bValue = (Boolean) clctfValue.getValue();
		final TriStateCheckBox chkbx = this.getTriStateCheckBox();
		if (bValue == null) {
			chkbx.setUndefined();
		}
		else {
			chkbx.setSelected(bValue.booleanValue());
		}
	}

	@Override
    public CollectableField getFieldFromView() {
		final TriStateCheckBox chkbx = this.getTriStateCheckBox();
		return chkbx.isUndefined() ? CollectableValueField.NULL : new CollectableValueField(chkbx.isSelected());
	}

	@Override
	public void setLabelText(String sLabel) {
		this.getTriStateCheckBox().setText(sLabel);
	}

	@Override
	public void setInsertable(boolean bInsertable) {
		// do nothing
	}

	@Override
	public boolean hasComparisonOperator() {
		return true;
	}

	@Override
	protected ComparisonOperator[] getSupportedComparisonOperators() {
		return new ComparisonOperator[] {
				ComparisonOperator.NONE,
				ComparisonOperator.EQUAL
		};
	}
	
	@Override
	public void setComparisonOperator(ComparisonOperator compop) {
		super.setComparisonOperator(compop);
		if (ComparisonOperator.NONE == compop) {
			getTriStateCheckBox().setUndefined();
		}

	}

	
	@Override
	protected CollectableSearchCondition getSearchConditionFromView() throws CollectableFieldFormatException {
		if (!isSearchComponent()) {
			throw new IllegalStateException("!isSearchComponent()");
		}
		
		return getSearchConditionFromViewImpl(LangUtils.toString(getCurrentItem()));
	}
	
	/**
	 * Implementation of <code>CollectableComponentModelListener</code>.
	 * @param ev
	 */
	@Override
	public void searchConditionChangedInModel(final SearchComponentModelEvent ev) {
		// update the view:
		runLocked(new Runnable() {
			@Override
            public void run() {
				final CollectableSearchCondition cond = ev.getSearchComponentModel().getSearchCondition();
				if (cond == null) {
					resetWithComparison();
					updateView(getEntityField().getNullField());
					setComparisonOperator(ComparisonOperator.NONE);
				}
				else {
					final AtomicCollectableSearchCondition atomiccond = (AtomicCollectableSearchCondition) cond;

					updateView(atomiccond.accept(new AtomicVisitor<CollectableField, RuntimeException>() {
						@Override
						public CollectableField visitIsNullCondition(CollectableIsNullCondition isnullcond) {
							resetWithComparison();
							return getEntityField().getNullField();
						}

						@Override
						public CollectableField visitComparison(CollectableComparison comparison) {
							resetWithComparison();
							return comparison.getComparand();
						}

						@Override
						public CollectableField visitComparisonWithParameter(CollectableComparisonWithParameter comparisonwp) {
							setWithComparison(comparisonwp.getParameter());
							return getEntityField().getNullField();
						}

						@Override
						public CollectableField visitComparisonWithOtherField(CollectableComparisonWithOtherField comparisonwf) {
							setWithComparison(comparisonwf.getOtherField());
							return getEntityField().getNullField();
						}

						@Override
						public CollectableField visitComparisonDateValues(CollectableComparisonDateValues comparisondv) throws RuntimeException {
							setWithComparison(comparisondv.getDateValues());
							return getEntityField().getNullField();
						}

						@Override
						public CollectableField visitLikeCondition(CollectableLikeCondition likecond) {
							resetWithComparison();
							return CollectableUtils.newCollectableFieldForValue(getEntityField(), likecond.getLikeComparand());
						}
						
						@Override
						public <T> CollectableField visitInCondition(CollectableInCondition<T> incond) {
							resetWithComparison();
							return CollectableUtils.newCollectableFieldForValue(getEntityField(), incond.getInComparands());
						}
					}));
					setComparisonOperator(atomiccond.getComparisonOperator());
				}
			}
		});
	}

	
	private Object getCurrentItem() {
		return getTriStateCheckBox().isSelected();
	}
	

	@Override
	public TableCellRenderer getTableCellRenderer(boolean subform) {
		return new CheckBoxTableCellRenderer();
	}

	/** 
	 * TODO: This REALLY should be static - but how to archive this? (tp)
	 */
	protected class CheckBoxTableCellRenderer implements TableCellRenderer {
		@Override
        public Component getTableCellRendererComponent(JTable tbl, Object oValue, boolean bSelected, boolean bHasFocus,
				int iRow, int iColumn) {

			if (oValue instanceof CollectableField) {
				oValue = ((CollectableField) oValue).getValue();
			}
			Component comp = tbl.getDefaultRenderer(Boolean.class).getTableCellRendererComponent(tbl, oValue, bSelected, bHasFocus, iRow, iColumn);
			if (comp instanceof JCheckBox) {
				JCheckBox check = (JCheckBox) comp;
				check.setOpaque(true);
				check.setVerticalAlignment(SwingConstants.TOP);
				check.setHorizontalAlignment(JCheckBox.CENTER);
				((JCheckBox)comp).setOpaque(true);
			}
			
			setBackgroundColor(comp, tbl, oValue, bSelected, bHasFocus, iRow, iColumn);
			return comp;
		}
	}

	@Override
	public void bindLayoutNavigationSupportToProcessingComponent() {
		getTriStateCheckBox().setLayoutNavigationCollectable(this);
	}
}  // class CollectableCheckBox
