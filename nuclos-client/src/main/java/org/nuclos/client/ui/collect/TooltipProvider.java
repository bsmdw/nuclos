package org.nuclos.client.ui.collect;

import javax.swing.JToolTip;

public interface TooltipProvider {
	
	public JToolTip createToolTip();
	
}
