//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.resource;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractListModel;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;
import javax.swing.KeyStroke;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.event.ListSelectionListener;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXBusyLabel;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.resource.NuclosResourceCache;
import org.nuclos.client.resource.NuclosResourceCategory;
import org.nuclos.client.resource.ResourceCache;
import org.nuclos.client.resource.ResourceDelegate;
import org.nuclos.client.theme.NuclosThemeSettings;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.Icons;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;

public class ResourceIconChooser extends JPanel {
	
	private static final Logger LOG = Logger.getLogger(ResourceIconChooser.class);
	
	private final NuclosResourceCategory cat;
	
	private final JList list;
	private final JScrollPane listScroller;
	private final List<Pair<UID, String>> icons = new ArrayList<Pair<UID, String>>();
	
	private boolean loading;
	private String iconToSelect;
	private UID iconToSelectUID;
	
	private boolean saved;
	
	final ActionListener iconActionListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			 if (e.getActionCommand().compareTo("Copy")==0) {
				 int selectedIndex = list.getSelectedIndex();
				 if (selectedIndex > 0) {
					 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(getResourceIconName(selectedIndex)), null);
				 }
			 }
		}
	};
	
	/**
	 * 
	 * @param cat (<code>null</code> for custom resource icons)
	 */
	public ResourceIconChooser(NuclosResourceCategory cat) {
		this(0, cat);
	}
	
	/**
	 * 
	 * @param iconMaxSize
	 * @param cat (<code>null</code> for custom resource icons)
	 */
	public ResourceIconChooser(final int iconMaxSize, NuclosResourceCategory cat) {
		this.cat = cat;
		setLayout(new BorderLayout());
		
		list = new JList() {

			@Override
			public int getScrollableUnitIncrement(Rectangle visibleRect,
                                                  int orientation,
                                                  int direction) {
                int row;
                if (orientation == SwingConstants.VERTICAL &&
                      direction < 0 && (row = getFirstVisibleIndex()) != -1) {
                    Rectangle r = getCellBounds(row, row);
                    if ((r.y == visibleRect.y) && (row != 0))  {
                        Point loc = r.getLocation();
                        loc.y--;
                        int prevIndex = locationToIndex(loc);
                        Rectangle prevR = getCellBounds(prevIndex, prevIndex);

                        if (prevR == null || prevR.y >= r.y) {
                            return 0;
                        }
                        return prevR.height;
                    }
                }
                return super.getScrollableUnitIncrement(
                                visibleRect, orientation, direction);
            }
        };
        
        list.registerKeyboardAction(iconActionListener,"Copy", KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_DOWN_MASK), JComponent.WHEN_FOCUSED);
        list.registerKeyboardAction(iconActionListener,"Copy", KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.META_DOWN_MASK), JComponent.WHEN_FOCUSED);
        list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        list.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        list.setCellRenderer(new ListCellRenderer() {
			@Override
			public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
				JPanel btnPanel = new JPanel(new BorderLayout());
				btnPanel.setOpaque(false);
				
				Icon ico = (ImageIcon) value;
				if (ico != null) {
					if (iconMaxSize==0) {
						if (((ImageIcon) value).getIconWidth() > 256 ||
							((ImageIcon) value).getIconHeight() > 256) {
							ico = MainFrame.resizeAndCacheIcon(ico, 256);
						}
					} else {
						ico = MainFrame.resizeAndCacheIcon(ico, iconMaxSize);
					}
				}
				final JButton btn;
				if (ico != null)
					btn = new JButton(ico);
				else
					btn = new JButton();
				btn.setBorderPainted(false);
				btn.setContentAreaFilled(true);
				btn.addActionListener(iconActionListener);
				
				if (index > 0) btn.setToolTipText(ResourceIconChooser.this.getResourceIconName(index));
				
				if (isSelected && index > 0) {
					btn.setOpaque(true);
					btn.setBackground(NuclosThemeSettings.BACKGROUND_COLOR4);
					btnPanel.setBorder(BorderFactory.createLineBorder(NuclosThemeSettings.BACKGROUND_ROOTPANE, 1));
				} else {
					btn.setOpaque(false);
					btnPanel.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
				}
				btnPanel.add(btn, BorderLayout.CENTER);
				
				return btnPanel;
			}
		});
        list.setVisibleRowCount(-1);
        
        listScroller = new JScrollPane(list);
        listScroller.setPreferredSize(new Dimension(250, 80));
        listScroller.setAlignmentX(LEFT_ALIGNMENT);
        
        JXBusyLabel busyLabel = new JXBusyLabel();
        busyLabel.setBusy(true);
        
        JPanel busyPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        busyPanel.setBackground(Color.WHITE);
        busyPanel.add(busyLabel);
        
        add(new JScrollPane(busyPanel), BorderLayout.CENTER);
        
        loading = true;
        iconToSelect = null;
        iconToSelectUID = null;
        
        LoadWorker worker = new LoadWorker();
        worker.execute();
	}	
	
	public void removeBorder() {
		listScroller.setBorder(BorderFactory.createEmptyBorder());
	}
	
	private class LoadWorker extends SwingWorker<List<ImageIcon>, Object> {

		@Override
		protected List<ImageIcon> doInBackground() throws Exception {
			try {
				Map<Pair<UID, String>, ImageIcon> imageIcons = new HashMap<Pair<UID, String>, ImageIcon>();
				Pair<UID, String> emptyFirst = new Pair<UID, String>(null, null);
				icons.add(emptyFirst);
				imageIcons.put(emptyFirst, Icons.getInstance().getIconEmpty16());
				if (cat == null) {
					for (UID resUID : ResourceDelegate.getInstance().getResourceUIDs()) {
						try {
							ImageIcon iconResource = ResourceCache.getInstance().getIconResource(resUID);
							if (iconResource.getIconWidth() == -1 ||
									iconResource.getIconHeight() ==  -1) {
								LOG.warn("Resource "+ resUID.getString() + " is not an image format nuclos can handle.");
							} else {
								Pair<UID, String> iconKey = new Pair<UID, String>(resUID, ResourceCache.getInstance().getResourceById(resUID).getName());
								icons.add(iconKey);
								imageIcons.put(iconKey, iconResource);
							}
						} catch (Exception ex) {
							// ignore. not an image icon.
						}
					}
					Collections.sort(icons, new Comparator<Pair<UID, String>>() {
						@Override
						public int compare(Pair<UID, String> o1, Pair<UID, String> o2) {
							return StringUtils.compareIgnoreCase(o1.y, o2.y);
						}
					});
				} else {
					for (String iconName : NuclosResourceCache.getNuclosResourceIcons(cat)) {
						Pair<UID, String> iconKey = new Pair<UID, String>(null, iconName);
						icons.add(iconKey);
						imageIcons.put(iconKey, NuclosResourceCache.getNuclosResourceIcon(iconName));
					}
				}
				List<ImageIcon> result = new ArrayList<ImageIcon>();
				for (Pair<UID, String> iconKey : icons) {
					result.add(imageIcons.get(iconKey));
				}
				return result;
			}
			catch (Exception ex) {
				Errors.getInstance().showExceptionDialog(ResourceIconChooser.this, ex);
			}
			return null;
		}

		@Override
		protected void done() {
			try {
				final Object[] result = get().toArray();
				list.setModel(new AbstractListModel() {
	                public int getSize() { return result.length; }
	                public Object getElementAt(int i) { return result[i]; }
	            });
				removeAll();
				add(listScroller, BorderLayout.CENTER);
				
				list.revalidate();
				listScroller.revalidate();
				revalidate();
				repaint();
				
				loading = false;
				
				if (iconToSelect != null) {
					setSelected(iconToSelect);
					iconToSelect = null;
				}
				if (iconToSelectUID != null) {
					setSelected(iconToSelectUID);
					iconToSelectUID = null;
				}
			} catch (Exception e) {
				Errors.getInstance().showExceptionDialog(list, e);
			}
		}
		
	}
	
	public void clearSelection() {
		select(0);
	}
	
	public void setSelected(UID uid) {
		if (loading) {
			iconToSelectUID = uid;
		} else {
			int selectedIndex = -1;
			
			if (uid == null) {
				selectedIndex = 0;
			} else {
				for (int i = 0; i < icons.size(); i++) {
					if (icons.get(i) != null && icons.get(i).x != null && icons.get(i).x.equals(uid)) {
						selectedIndex = i;
					}
				}
			}
			
			select(selectedIndex);
		}
	}
	
	public void setSelected(String name) {
		if (loading) {
			iconToSelect = name;
		} else {
			int selectedIndex = -1;
			
			if (name == null) {
				selectedIndex = 0;
			} else {
				for (int i = 0; i < icons.size(); i++) {
					if (icons.get(i) != null && icons.get(i).y != null && icons.get(i).y.equals(name)) {
						selectedIndex = i;
					}
				}
			}
			
			select(selectedIndex);
		}
	}
	
	private void select(int selectedIndex) {
		if (selectedIndex >= 0) {
			final int select = selectedIndex;
    			list.setSelectedIndex(selectedIndex);
    			SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						try {
							list.scrollRectToVisible(list.getCellBounds(select, select));
						}
						catch (Exception e) {
							LOG.error("setSelected failed: " + e, e);
						}																									
					}
				});
		}
	}
	
	public UID getSelectedResourceIconUID() {
		return getResourceIconUID(getSelectedIndex());
	}
	
	public String getSelectedResourceIconName() {
		return getResourceIconName(getSelectedIndex());
	}
	
	public int getSelectedIndex() {
		return list.getSelectedIndex();
	}
	
	public UID getResourceIconUID(int index) {
		if (index == -1)
			return null;
		return icons.get(index).x;
	}
	
	public String getResourceIconName(int index) {
		if (index == -1)
			return null;
		return icons.get(index).y;
	}
	
	public void addListSelectionListener(ListSelectionListener lsl) {
		list.addListSelectionListener(lsl);
	}
	
	public void removeListSelectionListener(ListSelectionListener lsl) {
		list.removeListSelectionListener(lsl);
	}
	
	public void showDialog() {
		this.showDialog(null, null);
	}
	
	public void showDialog(UID selectedResourceUID) {
		this.showDialog(null, selectedResourceUID);
	}
	
	public void showDialog(String sSelectedResource) {
		this.showDialog(sSelectedResource, null);
	}
	
	private void showDialog(String sSelectedResource, UID selectedResourceUID) {
		final SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();
		
		JPanel contentPanel = new JPanel(new BorderLayout(5, 5));
		contentPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		contentPanel.add(this, BorderLayout.CENTER);
		
		JPanel actionsPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 2));
		JButton btSave = new JButton(localeDelegate.getMessage("ResourceIconChooser.1","Speichern"));
		JButton btCancel = new JButton(localeDelegate.getMessage("ResourceIconChooser.2","Abbrechen"));
		actionsPanel.add(btSave);
		actionsPanel.add(btCancel);
		contentPanel.add(actionsPanel, BorderLayout.SOUTH);
		
		if (sSelectedResource != null) {
			setSelected(sSelectedResource);
		} else if (selectedResourceUID != null) {
			setSelected(selectedResourceUID);
		}
		
		final JDialog dialog = new JDialog(Main.getInstance().getMainFrame(), localeDelegate.getMessage(
				"ResourceIconChooser.3","Ressource Icon auswählen"), true);
		dialog.setContentPane(contentPanel);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.getRootPane().setDefaultButton(btSave);
		Rectangle mfBounds = Main.getInstance().getMainFrame().getBounds();
		dialog.setBounds(mfBounds.x+(mfBounds.width/2)-480, mfBounds.y+(mfBounds.height/2)-300, 960, 600);
		dialog.setResizable(true);
		
		btSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (loading) {
					//  ignore, no save
				} else {
					saved = true;
				}
				dialog.dispose();
			}
		});
		
		btCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dialog.dispose();
			}
		});
		
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2) {
					saved = true;
					dialog.dispose();
				}
			}
		});
		
		dialog.setVisible(true);
	}
	
	public boolean isSaved() {
		return saved;
	}
	
	public static class Button extends JButton {
		
		private String sResource;
		private UID resourceUID;
		
		private String sLabel;
		
		private NuclosResourceCategory cat;
		
		private Collection<ItemListener> itemListener = new ArrayList<ItemListener>();
		
		public Button() {
			this(null, null, null, null);
		}
		
		/**
		 * 
		 * @param sLabel
		 * @param sResource (selected icon)
		 * @param cat (<code>null</code> for custom resource icons)
		 */
		public Button(String sLabel, String sResource, NuclosResourceCategory cat) {
			this(sLabel, sResource, null, cat);
		}
		
		/**
		 * 
		 * @param sLabel
		 * @param resourceUID (selected custom resource icon)
		 */
		public Button(String sLabel, UID resourceUID) {
			this(sLabel, null, resourceUID, null);
		}
		
		/**
		 * 
		 * @param sLabel
		 * @param sResource (selected icon)
		 * @param resourceUID (selected custom resource icon)
		 * @param cat (<code>null</code> for custom resource icons)
		 */
		private Button(String sLabel, String sResource, UID resourceUID, NuclosResourceCategory cat) {
			super();
			this.sLabel = sLabel;
			this.resourceUID = resourceUID;
			if (resourceUID != null) {
				this.sResource = ResourceCache.getInstance().getResourceById(resourceUID).getName();
			} else {
				this.sResource = sResource;
			}
			this.cat = cat;
			updateButtonText();
			addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					ResourceIconChooser iconChooser = new ResourceIconChooser(getCategory());
					if (getResource() != null) {
						iconChooser.showDialog(getResource());
					} else if (getResourceUID() != null) {
						iconChooser.showDialog(getResourceUID());
					} else {
						iconChooser.showDialog();
					}
					if (iconChooser.isSaved()) {
						try {
							UID resourceUid = iconChooser.getSelectedResourceIconUID();
							if(resourceUid == null) {
								setResource(iconChooser.getSelectedResourceIconName());
							}
							else {
								setResource(resourceUid);
							}
						} catch (Exception ex) {
							setResource(iconChooser.getSelectedResourceIconName());
						}
						fireItemStateChanged();
					}
				}
			});
		}
		
		public NuclosResourceCategory getCategory() {
			return cat;
		}
		
		public void setCategory(NuclosResourceCategory cat) {
			this.cat = cat;
		}
		
		public void setLabel(String sLabel) {
			this.sLabel = sLabel;
		}
		
		public String getResource() {
			return sResource;
		}
		
		public UID getResourceUID() {
			return resourceUID;
		}
		
		public void resetResource() {
			this.sResource = null;
			this.resourceUID = null;
			updateButtonText();
		}
		
		public void setResource(String sResource) {
			this.sResource = sResource;
			this.resourceUID = null;
			updateButtonText();
		}
		
		public void setResource(UID resourceUID) {
			this.resourceUID = resourceUID;
			if (resourceUID != null) {
				this.sResource = ResourceCache.getInstance().getResourceById(resourceUID).getName();
			} else {
				this.sResource = null;
			}
			updateButtonText();
		}
		
		private void updateButtonText() {
			setText(sLabel + (sResource==null?(resourceUID==null?"":": **"):(": " + sResource)));
		}
		
		public void addItemListener(ItemListener il) {
			itemListener.add(il);
		}
		
		public void removeItemListener(ItemListener il) {
			itemListener.remove(il);
		}
		
		private void fireItemStateChanged() {
			ItemEvent ie = new ItemEvent(this, ItemEvent.ITEM_STATE_CHANGED, this, ItemEvent.ITEM_STATE_CHANGED);
			for (ItemListener il : itemListener) {
				il.itemStateChanged(ie);
			}
		}
		
	}
	
	public static class LittleToggleButton extends JToggleButton {
		
		private static final Dimension SIZE = new Dimension(24, 24);
		
		private String sResource;
		private UID resourceUID;
		
		private NuclosResourceCategory cat;
		
		private Collection<ItemListener> itemListener = new ArrayList<ItemListener>();
		
		public LittleToggleButton() {
			this(null, null, null, null, null);
		}
		
		/**
		 * 
		 * @param sLabel
		 * @param resourceUID (selected custom resource icon)
		 */
		public LittleToggleButton(String sLabel, UID resourceUID) {
			this(sLabel, null, null, resourceUID, null);
		}
		
		/**
		 * 
		 * @param sLabel
		 * @param sResource (selected icon)
		 * @param cat (<code>null</code> for custom resource icons)
		 */
		public LittleToggleButton(String sLabel, String sResource, NuclosResourceCategory cat) {
			this(sLabel, null, sResource, null, cat);
		}
	
		/**
		 * 
		 * @param icon
		 * @param sResource (selected icon)
		 * @param cat (<code>null</code> for custom resource icons)
		 */
		public LittleToggleButton(Icon icon, String sResource, NuclosResourceCategory cat) {
			this(null, icon, sResource, null, cat);
		}
		
		/**
		 * 
		 * @param icon
		 * @param resourceUID (selected custom resource icon)
		 */
		public LittleToggleButton(Icon icon, UID resourceUID) {
			this(null, icon, null, resourceUID, null);
		}
			
		/**
		 * @param sLabel
		 * @param icon
		 * @param sResource (selected icon)
		 * @param resourceUID (selected custom resource icon)
		 * @param cat (<code>null</code> for custom resource icons)
		 */
		private LittleToggleButton(String sLabel, Icon icon, String sResource, UID resourceUID, NuclosResourceCategory cat) {
			super(sLabel);
			setIcon(icon);
			this.resourceUID = resourceUID;
			if (resourceUID != null) {
				this.sResource = ResourceCache.getInstance().getResourceById(resourceUID).getName();
			} else {
				this.sResource = sResource;
			}
			this.cat = cat;
			setFocusable(false);
			updateButtonState();
			addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					ResourceIconChooser iconChooser = new ResourceIconChooser(getCategory());
					iconChooser.showDialog(getResource());
					if (iconChooser.isSaved()) {
						setResource(iconChooser.getSelectedResourceIconUID());
						fireItemStateChanged();
					}
					updateButtonState();
				}
			});
		}

		public NuclosResourceCategory getCategory() {
			return cat;
		}
		
		public void setCategory(NuclosResourceCategory cat) {
			this.cat = cat;
		}
		
		public String getResource() {
			return sResource;
		}
		
		public UID getResourceUID() {
			return resourceUID;
		}
		
		public void setResource(String sResource) {
			this.sResource = sResource;
			updateButtonState();
		}
		
		public void setResource(UID resourceUID) {
			this.resourceUID = resourceUID;
			updateButtonState();
		}
		
		private void updateButtonState() {
			if (sResource==null && resourceUID==null) {
				setSelected(false);
			} else {
				setSelected(true);
			}
		}
		
		public void addItemListener(ItemListener il) {
			itemListener.add(il);
		}
		
		public void removeItemListener(ItemListener il) {
			itemListener.remove(il);
		}
		
		private void fireItemStateChanged() {
			ItemEvent ie = new ItemEvent(this, ItemEvent.ITEM_STATE_CHANGED, this, ItemEvent.ITEM_STATE_CHANGED);
			for (ItemListener il : itemListener) {
				il.itemStateChanged(ie);
			}
		}
		
		@Override
		public Dimension getPreferredSize() {
			return SIZE;
		}

		@Override
		public Dimension getSize() {
			return SIZE;
		}

		@Override
		public Dimension getMinimumSize() {
			return SIZE;
		}

		@Override
		public Dimension getMaximumSize() {
			return SIZE;
		}
		
	}
	
	public static void main(String[] args) {
		JFrame frm = new JFrame(ResourceIconChooser.class.getName());
		frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frm.setBounds(100, 100, 300, 600);
		
		ResourceIconChooser ric = new ResourceIconChooser(NuclosResourceCategory.ENTITY_ICON);
		ric.setSelected("org.nuclos.common.resource.icon.glyphish.88-beer-mug.png");
		frm.getContentPane().add(ric, BorderLayout.CENTER);
		
		frm.setVisible(true);
	}
	
}
