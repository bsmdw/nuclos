package org.nuclos.client.ui;

public interface TableRowMouseOverSupport {

	boolean isMouseOverRow(int iRow);
	
	void setMouseOverRow(int iRow);
	
	int getMouseOverRow();
	
}
