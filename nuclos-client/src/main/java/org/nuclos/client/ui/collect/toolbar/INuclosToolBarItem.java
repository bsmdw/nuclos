package org.nuclos.client.ui.collect.toolbar;

import javax.swing.Action;
import javax.swing.Icon;

public interface INuclosToolBarItem {

	String getLabel();

	String getTooltip();

	Icon getIcon();

	/**
	 * 
	 * @param action
	 * @return same action with set label, tooltip and icon
	 */
	<T extends Action> T init(Action action);

}
