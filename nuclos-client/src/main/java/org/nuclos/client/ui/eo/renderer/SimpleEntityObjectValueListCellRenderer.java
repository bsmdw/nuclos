//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.eo.renderer;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.nuclos.client.ui.eo.component.TypeConverterFactory;
import org.nuclos.client.ui.eo.mvc.ITypeConverter;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;

public class SimpleEntityObjectValueListCellRenderer<PK> implements ListCellRenderer {
	
	private final UID valueField;
	
	private final JLabel comp = new JLabel();
	
	private final ITypeConverter converter;
	
	public SimpleEntityObjectValueListCellRenderer(TypeConverterFactory tcFactory, UID valueField) {
		this.valueField = valueField;
		this.converter = tcFactory.getTypeConverterForField(valueField);
	}

	@Override
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
			boolean cellHasFocus) {
		
		// non-breakable space to ensure the option line is really displayed (in swing)
		String fieldValue = "\u00A0";
		final EntityObjectVO<PK> modelEo = (EntityObjectVO<PK>) value;
		if (modelEo != null) {
			final Object modelValue = modelEo.getFieldValue(valueField);
			fieldValue = (String) converter.fromModelToComponent(modelValue);
		}
		if (fieldValue == null) {
			fieldValue = "\u00A0";
		}
		comp.setText(fieldValue);
		return comp;
	}

}
