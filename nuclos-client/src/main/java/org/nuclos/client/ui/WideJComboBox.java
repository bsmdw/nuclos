package org.nuclos.client.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.Field;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.common.ParameterProvider;

public class WideJComboBox<E> extends JComboBox {

	private String type;
	private boolean layingOut = false;
	private int widestLengh = 0;
	private boolean wide = false;

	private JTextPane overlayTextPane = null;
	private JDialog overlayDialog = null;
	private final boolean showEllipsisAndOverlay;
	private FontMetrics fm;
	private Point locationOnScreen = null;

	public WideJComboBox() {
		super();

		this.showEllipsisAndOverlay = ClientParameterProvider.getInstance()
				.isEnabled(ParameterProvider.SHOW_OVERLAY_FOR_TOO_SMALL_TEXTFIELDS, false);

		this.addPropertyChangeListener("locationOnScreen", (e -> {
			if (e.getOldValue() == null || !e.getOldValue().equals(e.getNewValue())) {
				setOverlayBounds();
			}
		}));

		this.getEditor().getEditorComponent().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(final MouseEvent e) {
				if ((e.getModifiers() & InputEvent.BUTTON1_MASK) != 0 && e.isAltDown()) {
					showOverlay();
				}
			}
		});
	}

	private void init() {
		overlayDialog = new JDialog();
		overlayDialog.setUndecorated(true);
		overlayTextPane = new JTextPane();
		overlayTextPane.setContentType("text/html");
		final JScrollPane overlayScrollPane = new JScrollPane(overlayTextPane);
		overlayTextPane.setVisible(true);
		overlayTextPane.setEditable(true);
		overlayDialog.setLayout(new BorderLayout());
		overlayDialog.add(overlayScrollPane, BorderLayout.CENTER);
		overlayDialog.setFocusable(true);

		Action overlayCloseAction = new AbstractAction() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				closeOverlay();
			}
		};

		Action overlayCloseWithoutModificationAction = new AbstractAction() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				closeOverlay();
			}
		};

		overlayTextPane.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "closeOverlayWithoutModification");
		overlayTextPane.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, KeyEvent.CTRL_MASK), "closeOverlay");
		overlayTextPane.getActionMap().put("closeOverlay", overlayCloseAction);
		overlayTextPane.getActionMap().put("closeOverlayWithoutModification", overlayCloseWithoutModificationAction);

		overlayTextPane.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(final FocusEvent e) {
				if (overlayDialog.isVisible()) {
					closeOverlay();
				}
			}
		});
	}

	private void showOverlay() {
		if (showEllipsisAndOverlay) {
			if (overlayDialog == null || overlayTextPane == null) {
				init();
			}

			if (isTextTooLong()) {
				SwingUtilities.invokeLater(() -> {
					overlayDialog.setVisible(true);
					overlayDialog.setSize(Math.max(getWidth(), 200), 200);
					setOverlayBounds();
					overlayTextPane.setEditable(false);
					overlayTextPane.setText(getSelectedItem().toString());
					overlayTextPane.setBorder(getBorder());
					overlayTextPane.requestFocusInWindow();
				});
			}
		}
	}

	private void setOverlayBounds() {
		if (overlayDialog != null && overlayDialog.isVisible()) {
			SwingUtilities.invokeLater(() -> {
				boolean fitsDownwards = true;
				boolean fitsRight = true;
				Point root = new Point(WideJComboBox.this.getLocationOnScreen());
				int widthOverlay = overlayDialog.getWidth();
				int heigthOverlay = overlayDialog.getHeight();
				int widthField = WideJComboBox.this.getWidth();
				int heightField = WideJComboBox.this.getHeight();

				Window mainWindow = UIUtils.getWindowForComponent(this);
				if (root.y + heigthOverlay > mainWindow.getLocationOnScreen().y + mainWindow.getHeight()) {
					fitsDownwards = false;
				}
				if (root.x + widthOverlay > mainWindow.getLocationOnScreen().x + mainWindow.getWidth()) {
					fitsRight = false;
				}

				if (!fitsDownwards) {
					root.y = root.y - heigthOverlay + heightField;
				}
				if (!fitsRight) {
					root.x = root.x - widthOverlay + widthField;
				}
				overlayDialog.setBounds(
						root.x,
						root.y,
						widthOverlay,
						heigthOverlay);
			});
		}
	}

	private void closeOverlay() {
		if (overlayDialog != null) {
			SwingUtilities.invokeLater(() -> {
				overlayDialog.setVisible(false);
			});
		}
	}

	private boolean isTextTooLong() {
		Insets insets = getInsets();
		return getWidth() - insets.left - insets.right - getArrowButtonWidth() < getCachedFontMetrics().stringWidth(getSelectedItem().toString());
	}

	private int getArrowButtonWidth() {
		int buttonWidth = 0;
		try {
			Field fArrowButton = getUI().getClass().getSuperclass().getDeclaredField("arrowButton");
			fArrowButton.setAccessible(true);
			JButton arrowButton = (JButton) fArrowButton.get(getUI());
			buttonWidth = arrowButton.getWidth();
		} catch (NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return buttonWidth;
	}

	private FontMetrics getCachedFontMetrics() {
		if (fm == null) {
			fm = getFontMetrics(getFont());
		}
		return fm;
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (isShowing()) {
			Point newLocationOnScreen = getLocationOnScreen();
			if (locationOnScreen == null || !locationOnScreen.equals(newLocationOnScreen)) {
				firePropertyChange("locationOnScreen", locationOnScreen, newLocationOnScreen);
				locationOnScreen = newLocationOnScreen;
			}
		}
	}

	public boolean isWide() {
		return wide;
	}

	public void setWide(boolean wide) {
		this.wide = wide;
		widestLengh = getWidestItemWidth();

	}

	public Dimension getSize() {
		Dimension dim = super.getSize();
		if (!layingOut && isWide())
			dim.width = Math.max(widestLengh, dim.width);
		return dim;
	}

	public int getWidestItemWidth() {

		int numOfItems = this.getItemCount();
		Font font = this.getFont();
		FontMetrics metrics = this.getFontMetrics(font);
		int widest = 0;
		for (int i = 0; i < numOfItems; i++) {
			Object item = this.getItemAt(i);
			int lineWidth = item.toString() != null ? metrics.stringWidth(item.toString()) : 0;
			widest = Math.max(widest, lineWidth);
		}

		return widest + 5 + (getMaximumRowCount() < numOfItems ? 16 : 0);
	}

	public void doLayout() {
		try {
			layingOut = true;
			super.doLayout();
		} finally {
			layingOut = false;
		}
	}
}