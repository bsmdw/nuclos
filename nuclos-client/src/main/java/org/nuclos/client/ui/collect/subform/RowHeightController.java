package org.nuclos.client.ui.collect.subform;

import java.awt.Rectangle;
import java.util.HashMap;
import java.util.Map;

import javax.swing.SwingUtilities;

/**
 * Created by oliver brausch on 17.07.17.
 */
public class RowHeightController {

	private Map<Integer, Map<Integer, Integer>> cache = new HashMap<Integer, Map<Integer, Integer>>();

	private int iEditorCol = -1;
	private int iEditorRow = -1;
	private int iEditorHeight = -1;

	private final SubForm subform;

	public RowHeightController(SubForm subform) {
		this.subform = subform;
	}

	public void clear(int iCol, int iRow) {
		final Map<Integer, Integer> colCache = cache.get(iRow);
		if (colCache != null) {
			colCache.remove(iCol);
//				System.out.println("clearing row=" +iRow + " col=" + iCol);
		}
	}

	public void clear(int iRow) {
		cache.remove(iRow);
//			System.out.println("clearing row " +iRow);
	}

	public void clear() {
		cache.clear();
	}

	public void clearEditorHeight() {
		iEditorCol = -1;
		iEditorRow = -1;
		iEditorHeight = -1;
	}

	public void setEditorHeight(int iCol, int iRow, int iHeight) {
		if (!subform.isDynamicRowHeights())
			return;

		final int iOldEditorCol = iEditorCol;
		final int iOldEditorRow = iEditorRow;
		final int iOldEditorHeight = iEditorHeight;
		iEditorCol = iCol;
		iEditorRow = iRow;
		iEditorHeight = iHeight;

		if (iOldEditorRow != iEditorRow || iOldEditorCol != iEditorCol) {
			if (iOldEditorRow != -1) {
				subform.getSubformTable().setRowHeightStrict(iOldEditorRow, subform.getValidRowHeight(getMaxRowHeightCacheOnly(iOldEditorRow)));
			}
		}
		if (iOldEditorRow != iEditorRow || iOldEditorCol != iEditorCol || iOldEditorHeight != iEditorHeight) {
			subform.getSubformTable().setRowHeightStrict(iEditorRow, subform.getValidRowHeight(Math.max(iEditorHeight, getMaxRowHeightCacheOnly(iEditorRow))));
			final Rectangle r = subform.getSubformTable().getCellRect(iEditorRow, iEditorCol, false);
			r.y = r.y + r.height - 1;
			r.height = 1;

			if (!subform.getSubformScrollPane().getViewport().getViewRect().contains(r)) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						subform.getSubformScrollPane().getViewport().scrollRectToVisible(r);
					}
				});
			}
		}
	}

	public void setHeight(int iCol, int iRow, int iHeight) {
		if (!subform.isDynamicRowHeights())
			return;

		Map<Integer, Integer> colCache = cache.get(iRow);
		if (colCache == null) {
			colCache = new HashMap<Integer, Integer>();
			cache.put(iRow, colCache);
		}

		final Integer iOldHeight = colCache.put(iCol, iHeight);
		if (iOldHeight == null || iOldHeight.intValue() != iHeight) {
			final int iNewHeight = subform.getValidRowHeight(getMaxRowHeight(colCache));
			subform.getSubformTable().setRowHeightStrict(iRow, iNewHeight);
//				System.out.println("row=" + iRow + " col=" + iCol + " height=" + iHeight + " oldHeight=" + iOldHeight + " newHeight=" + iNewHeight + " heights=" + colCache);
		}
	}

	public int getMaxRowHeight(int iRow) {
		if (iRow == iEditorRow) {
			return Math.max(iEditorHeight, getMaxRowHeightCacheOnly(iRow));
		} else {
			return getMaxRowHeightCacheOnly(iRow);
		}
	}

	private int getMaxRowHeightCacheOnly(int iRow) {
		return getMaxRowHeight(cache.get(iRow));
	}

	private static int getMaxRowHeight(final Map<Integer, Integer> columnHeights) {
		if (columnHeights != null) {
			int result = 0;
			for (Integer iHeight : columnHeights.values()) {
				result = Math.max(result, iHeight);
			}
			return result;
		} else {
			return -1;
		}
	}
}

