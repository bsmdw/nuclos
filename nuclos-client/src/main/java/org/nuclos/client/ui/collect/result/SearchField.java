package org.nuclos.client.ui.collect.result;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.MouseInputAdapter;
import javax.swing.event.MouseInputListener;

import org.apache.log4j.Logger;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.ui.ListOfValues;
import org.nuclos.client.ui.RecyclableComponent;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SpringApplicationContextHolder;

@SuppressWarnings("serial")
public class SearchField extends JTextField implements RecyclableComponent {

	private static final Logger LOG = Logger.getLogger(SearchField.class);
	
    public static String placeholderText = "<Textfilter>";
    public static String labelText = "";
    public static int borderThickNess = 2;
    public static int textFieldHeight = 20;

	private static final Border CANCEL_BORDER = new CancelBorder();
	private boolean sendsNotificationForEachKeystroke = true;
	private boolean showingPlaceholderText = false;
	private boolean armed = false;
	private int quicksearch_delay_time = ListOfValues.QUICKSEARCH_DELAY_TIME;
	private boolean bToBeRecycled = false;
	private PlaceholderText plHolTextObj;
	private TimerTask lastTimerTask = null;
	
	public SearchField() {
		super(15);
		addFocusListener(plHolTextObj = new PlaceholderText(placeholderText));
		initBorder();
		initKeyListener();
		initQuickSearchDelayTime();
		setPreferredSize(new Dimension(300, textFieldHeight));
	}
	
	private void initQuickSearchDelayTime() {
		String sTime = ClientParameterProvider.getInstance().getValue(ParameterProvider.QUICKSEARCH_DELAY_TIME);
		if (sTime != null && !sTime.isEmpty()) try {
			quicksearch_delay_time = Integer.parseInt(sTime);
			if (quicksearch_delay_time == -1) {
				setSendsNotificationForEachKeystroke(false);
			}
		} catch (NumberFormatException nfe) {
		}
	}
	
	@Override
	public void parentVisible(boolean b) {
		bToBeRecycled = b == false && this.isVisible();
	}
	
	public boolean toBeRecycled() {
		return bToBeRecycled;
	}

	private void initBorder() {
		setBorder(new CompoundBorder(getBorder(), CANCEL_BORDER));
		MouseInputListener mouseInputListener = new CancelListener();
		addMouseListener(mouseInputListener);
		addMouseMotionListener(mouseInputListener);
	}

	private void initKeyListener() {
		addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					cancel();
				} else if (sendsNotificationForEachKeystroke) {
					if (quicksearch_delay_time > 0) {
						if (lastTimerTask != null) {
							lastTimerTask.cancel();
						}
						final Timer timer = (Timer) SpringApplicationContextHolder.getBean("timer");
						lastTimerTask = new TimerTask() {
							@Override
							public void run() {
								try {
									UIUtils.invokeOnDispatchThread(new Runnable() {
										@Override
										public void run() {
											maybeNotify();
										}
									});
								} catch (Exception e) {
									LOG.error("key released timer task failed: " + e, e);
								}
							}
						};
						timer.schedule(lastTimerTask, quicksearch_delay_time);
					} else {
						maybeNotify();
					}
//					maybeNotify();
				}
			}
		});
	}

	private void maybeNotify() {
		if (showingPlaceholderText) {
			return;
		}
		postActionEvent();
	}
	
	private void cancel() {
		setText("");
		postActionEvent();
	}
	
	public boolean isShowingPlaceHolder() {
		return showingPlaceholderText;
	}

	public void setSendsNotificationForEachKeystroke(boolean eachKeystroke) {
		this.sendsNotificationForEachKeystroke = eachKeystroke;
	}
	/**
	 * 
	 * Draws the cancel button as a gray circle with a white cross inside.
	 */

	static class CancelBorder extends EmptyBorder {
		private static final Color GRAY = new Color(0.8f, 0.8f, 0.8f);
		CancelBorder() {
			super(0, 0, 0, 15);
		}

		public void paintBorder(Component c, Graphics oldGraphics, int x,
				int y, int width, int height) {
			SearchField field = (SearchField) c;
			if (field.showingPlaceholderText || field.getText().length() == 0) {
				return;
			}
			Graphics2D g = (Graphics2D) oldGraphics;
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_ON);
			final int circleL = 14;
			final int circleX = x + width - circleL;
			final int circleY = y + (height - 1 - circleL) / 2;
			g.setColor(field.armed ? Color.LIGHT_GRAY : GRAY);
			g.fillOval(circleX, circleY, circleL, circleL);
			final int lineL = circleL - 8;
			final int lineX = circleX + 4;
			final int lineY = circleY + 4;
			g.setColor(Color.WHITE);
			g.drawLine(lineX, lineY, lineX + lineL, lineY + lineL);
			g.drawLine(lineX, lineY + lineL, lineX + lineL, lineY);
		}
	}

	/**
	 * 
	 * Handles a click on the cancel button by clearing the text and notifying
	 * 
	 * any ActionListeners.
	 */

	class CancelListener extends MouseInputAdapter {
		private boolean isOverButton(MouseEvent e) {
			// If the button is down, we might be outside the component
			// without having had mouseExited invoked.
			if (contains(e.getPoint()) == false) {
				return false;
			}
			// In lieu of proper hit-testing for the circle, check that
			// the mouse is somewhere in the border.
			Rectangle innerArea = SwingUtilities.calculateInnerArea(
					SearchField.this, null);
			return (innerArea.contains(e.getPoint()) == false);
		}

		public void mouseDragged(MouseEvent e) {
			arm(e);
		}

		public void mouseEntered(MouseEvent e) {
			arm(e);
		}

		public void mouseExited(MouseEvent e) {
			disarm();
		}

		public void mousePressed(MouseEvent e) {
			arm(e);
		}

		public void mouseReleased(MouseEvent e) {
			if (armed) {
				cancel();
			}
			disarm();
		}

		private void arm(MouseEvent e) {
			armed = (isOverButton(e) && SwingUtilities.isLeftMouseButton(e));
			repaint();
		}

		private void disarm() {
			armed = false;
			repaint();
		}
	}

	/**
	 * 
	 * Replaces the entered text with a gray placeholder string when the
	 * 
	 * search field doesn't have the focus. The entered text returns when
	 * 
	 * we get the focus back.
	 */
	
	public void activateField(String text) {
		if (text.isEmpty() || placeholderText.equals(text)) {
			plHolTextObj.previousText = "";
			showingPlaceholderText = true;
			setForeground(Color.GRAY);
			setText(placeholderText);
		} else {
			plHolTextObj.previousText = text;
			showingPlaceholderText = false;
			setForeground(Color.BLACK);		
			setText(text);
		}
	}

	class PlaceholderText implements FocusListener {
		private String placeholderText;
		String previousText = "";
		private Color previousColor;
		private int lastCaretPosition = -1;
		PlaceholderText(String placeholderText) {
			this.placeholderText = placeholderText;
			focusLost(null);
		}

		public void focusGained(FocusEvent e) {
			setForeground(previousColor);
			setText(previousText);
			showingPlaceholderText = false;
			if (lastCaretPosition >= 0) setCaretPosition(lastCaretPosition);
		}

		public void focusLost(FocusEvent e) {
			lastCaretPosition = getCaretPosition();
			if (lastTimerTask != null) lastTimerTask.cancel();
			previousText = getText();
			previousColor = getForeground();
			if (previousText.length() == 0) {
				showingPlaceholderText = true;
				setForeground(Color.GRAY);
				setText(placeholderText);
			}
		}
	}
	
	public JPanel getSearchFieldPanel(int borderNorth, int borderSouth) {
        JPanel searchFieldPanel = new JPanel();
        searchFieldPanel.setLayout(new BorderLayout());
        searchFieldPanel.setPreferredSize(new Dimension(40, textFieldHeight + borderNorth + borderSouth));
        if (borderNorth > 0) {
	        JPanel placeHolderNorth = new JPanel();
	        placeHolderNorth.setPreferredSize(new Dimension(40, borderNorth));
	        searchFieldPanel.add(placeHolderNorth, BorderLayout.NORTH);
        }
        searchFieldPanel.add(this, BorderLayout.CENTER);
        if (labelText == null || labelText.isEmpty()) {
                JPanel placeHolderWest = new JPanel();
                placeHolderWest.setPreferredSize(new Dimension(borderThickNess, textFieldHeight));
                searchFieldPanel.add(placeHolderWest, BorderLayout.WEST);                               
        } else {
                JLabel trulla = new JLabel(labelText);
                searchFieldPanel.add(trulla, BorderLayout.WEST);                                
        }
        JPanel placeHolderEast = new JPanel();
        placeHolderEast.setPreferredSize(new Dimension(borderThickNess, textFieldHeight));
        searchFieldPanel.add(placeHolderEast, BorderLayout.EAST);
        if (borderSouth > 0) {
	        JPanel placeHolderSouth = new JPanel();
	        placeHolderSouth.setPreferredSize(new Dimension(40, borderSouth));
	        searchFieldPanel.add(placeHolderSouth, BorderLayout.SOUTH);
        }
        return searchFieldPanel;
	}
}
