package org.nuclos.client.ui.collect.subform;

import java.util.List;

import org.nuclos.client.ui.collect.model.CollectableEntityFieldBasedTableModel;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;

/**
 * <code>TableModel</code> that can be used in a <code>SubForm</code>.
 */
public interface SubFormTableModel extends CollectableEntityFieldBasedTableModel {
	/**
	 * @param clctef
	 * @return a null value that can be set in a table cell for the given entity field.
	 */
	Object getNullValue(CollectableEntityField clctef);

	/**
	 * determines the (visible) columns of this model.
	 * Each column is represented by a <code>CollectableEntityField</code>.
	 * @param lstclctefColumns List&lt;CollectableEntityField&gt;
	 */
	void setColumns(List<? extends CollectableEntityField> lstclctefColumns);

	/**
	 * @param columnIndex
	 * @return minimum column width based on class
	 */
	int getMinimumColumnWidth(int columnIndex);

	/**
	 * @param fieldUid
	 * @return the index of the column with the given fieldname. -1 if none was found.
	 */
	int findColumnByFieldUid(UID fieldUid);

	void remove(int iRow);

	void remove(int[] rows);

}	// interface SubFormTableModel
