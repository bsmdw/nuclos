package org.nuclos.client.ui.labeled;

import java.util.HashSet;
import java.util.Set;

import javax.swing.plaf.basic.BasicComboBoxEditor;

import org.nuclos.common.collect.collectable.CollectableValueIdField;

public class MultiSelectComboBoxEditor extends BasicComboBoxEditor {
	private final ItemsProvider itemsProvider;
	private Set<Object> selectedItems;
	
	public MultiSelectComboBoxEditor(ItemsProvider itemsProvider, Set<Object> selectedItems) {
		this.itemsProvider = itemsProvider;
		this.selectedItems = selectedItems;
	}
	
	public Set<Object> getItems() {
		Set<Object> items = new HashSet<Object>();
		for (int i = 0; i < itemsProvider.getItemCount(); i++) {
			
			Object o = itemsProvider.getItemAt(i);
			if (o instanceof CollectableValueIdField) {
				CollectableValueIdField cvid = (CollectableValueIdField) o;
				if (cvid.isSelected() && cvid.getValue() != null && !cvid.getValue().toString().isEmpty()) {
					items.add(cvid);
					
				}
			}
		}
		if (selectedItems != null) {
			items.addAll(selectedItems);
		}
		return items;
	}
	
	public String getMultiSelectItemsString() {
		String text = "";
		
		Set<Object> items = getItems();
		if (items.size() >= 1) {
			text = items.iterator().next().toString();
			if (items.size() >= 2) {
				text = text + " (+" + (items.size() - 1) + ")";
				
			}
		}
		return text;
	}

	protected final ItemsProvider getItemsProvider() {
		return itemsProvider;
	}
	
}
