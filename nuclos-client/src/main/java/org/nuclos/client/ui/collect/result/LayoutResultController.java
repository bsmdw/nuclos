//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.result;

import java.util.List;

import javax.swing.RowSorter.SortKey;

import org.nuclos.client.common.controller.NuclosCollectControllerCommonState;
import org.nuclos.client.layout.admin.LayoutCollectController;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.collect.model.SortableCollectableTableModel;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableSorting;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonSortingException;

/**
 * A specialization of ResultController for use with an {@link LayoutCollectController}.
 * 
 * @author Thomas Pasch
 * @since Nuclos 3.1.01
 */
public class LayoutResultController<Clct extends CollectableMasterDataWithDependants<UID>> extends NuclosResultController<UID,Clct> {
	
	public LayoutResultController(CollectableEntity clcte, ISearchResultStrategy<UID,Clct> srs, NuclosCollectControllerCommonState state) {
		super(clcte, srs, state);
	}
	
	/**
	 * @deprecated You should really provide a CollectableEntity here.
	 */
	public LayoutResultController(UID entityUid, ISearchResultStrategy<UID,Clct> srs, NuclosCollectControllerCommonState state) {
		super(entityUid, srs, state);
	}
	
	/**
	 * @deprecated Remove this.
	 */
	@Override
	public List<CollectableSorting> getCollectableSortingSequence() {
		final LayoutCollectController controller = getLayoutCollectController();
		final SortableCollectableTableModel<UID,CollectableMasterDataWithDependants<UID>> rtm = controller.getResultTableModel(); 
		List<? extends SortKey> sortKeys = rtm.getSortKeys();
		boolean valid = true;
		for (SortKey sortKey : sortKeys) {
			final CollectableEntityField clctField = rtm.getCollectableEntityField(sortKey.getColumn());
			if(clctField.getUID().equals(E.LAYOUT.layoutML.getUID())){
				final String msg = SpringLocaleDelegate.getInstance().getMessage(
						"LayoutCollectController.7","Eine Sortierung nach der Spalte \"LayoutML\" ist nicht durchf\u00fchrbar.");
				Errors.getInstance().showExceptionDialog(controller.getTab(), msg, new CommonSortingException(msg));
				rtm.setSortKeys(controller.getLastSortKeys(), false);
				valid = false;
				break;
			}
		}
		if (valid) {
			controller.setLastSortKeys(sortKeys);
		}
		return super.getCollectableSortingSequence();
	}
	
	/**
	 * @deprecated Remove this.
	 */
	@Override
	protected boolean isFieldToBeDisplayedInTable(UID fieldUid) {
		return !E.LAYOUT.layoutML.getUID().equals(fieldUid);
	}
	
	private LayoutCollectController getLayoutCollectController() {
		return (LayoutCollectController) getCollectController();
	}

}
