//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.customcode;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;

import org.nuclos.client.common.LafParameterProvider;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterData;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.MasterDataCollectController;
import org.nuclos.client.rule.server.AutomaticRuleCompilationButtonFactory;
import org.nuclos.client.rule.server.RuleEditPanel;
import org.nuclos.client.ui.CommonAbstractAction;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.CollectState;
import org.nuclos.client.ui.collect.CollectStateAdapter;
import org.nuclos.client.ui.collect.CollectStateEvent;
import org.nuclos.common.LafParameter;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.NuclosToolBarItems;
import org.nuclos.common.collect.ToolBarConfiguration;
import org.nuclos.common.collect.exception.CollectableValidationException;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;

/**
 * CollectController for nuclos_code.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
public abstract class CodeCollectController extends MasterDataCollectController<UID> {
		
	protected RuleEditPanel pnlEdit;
	
	protected abstract CodeCheckAction getCodeCheckAction();
	protected abstract List<Component> getAdditionalToolBarComponents();
	protected abstract void readAdditionalValuesFromEditPanel(CollectableMasterDataWithDependants<UID> clct, boolean bSearchTab) throws CollectableValidationException;
	
	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton}
	 * to get an instance.
	 *
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	public CodeCollectController(UID entity, MainFrameTab tabIfAny) {
		super(entity, tabIfAny, null);

		this.getCollectStateModel().addCollectStateListener(new RuleCollectStateListener());
		
		getResultPanel().addToolBarComponent(SpringApplicationContextHolder.getBean(
				AutomaticRuleCompilationButtonFactory.class).newMenuItem());
	}


	
	protected void setupDetailsToolBar() {
		super.setupDetailsToolBar();
	
		// Add additional toolbar elements
		for (Component addComp : getAdditionalToolBarComponents())
			this.getDetailsPanel().addToolBarComponent(addComp);

		this.getDetailsPanel().registerToolBarAction(NuclosToolBarItems.INFO, getPointerAction());
		
		getDetailsPanel().addToolBarComponent(SpringApplicationContextHolder.getBean(
				AutomaticRuleCompilationButtonFactory.class).newMenuItem());
		
		ToolBarConfiguration configuration = 
				LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Tool_Bar_Detail,
						getEntityUid());
		
		this.getDetailsPanel().setToolBarConfiguration(configuration);
	}

	/**
	 * @return false
	 */
	@Override
	protected boolean isMultiEditAllowed() {
		return false;
	}

	/**
	 * @deprecated Move to DetailsController hierarchy and make protected again.
	 */
	@Override
	public void addAdditionalChangeListenersForDetails() {
		pnlEdit.addChangeListener(this.changelistenerDetailsChanged);
	}

	/**
	 * @deprecated Move to DetailsController hierarchy and make protected again.
	 */
	@Override
	public void removeAdditionalChangeListenersForDetails() {
		pnlEdit.removeChangeListener(this.changelistenerDetailsChanged);
	}
	 
	protected final UndoableEditListener undoableEditListener = new UndoableEditListener() {
		public void undoableEditHappened(UndoableEditEvent evt) {
			pnlEdit.getJavaEditorUndoManager().addEdit(evt.getEdit());
		}
	};

	@Override
	public void setupEditPanelForDetailsTab() {
		super.setupEditPanelForDetailsTab();

		if (pnlEdit == null) {
			pnlEdit = new RuleEditPanel(null);
		}

		JPanel editPanel = (JPanel) UIUtils.findJComponent(this.getDetailsPanel(), "editPanel");
		if (editPanel != null) {
			editPanel.removeAll();
			editPanel.setLayout(new BorderLayout());
			editPanel.add(pnlEdit, BorderLayout.CENTER);
		}
	}

		
	@Override
	protected void readValuesFromEditPanel(CollectableMasterDataWithDependants<UID> clct, boolean bSearchTab) throws CollectableValidationException {
		super.readValuesFromEditPanel(clct, bSearchTab);
		readAdditionalValuesFromEditPanel(clct, bSearchTab);
	}

	@Override
	public void refreshCurrentCollectable() throws CommonBusinessException {
		super.refreshCurrentCollectable();
		pnlEdit.clearMessages();
	}

	
	@Override
	protected String getTitle(int iTab, int iMode) {
		final SpringLocaleDelegate localeDelegate = getSpringLocaleDelegate();
		final String[] asTabs = { localeDelegate.getMessage("MasterDataCollectController.17", "Suche"), localeDelegate.getMessage("MasterDataCollectController.5", "Ergebnis"),
				localeDelegate.getMessage("MasterDataCollectController.2", "Details") };
		final String[] asDetailsMode = { localeDelegate.getMessage("MasterDataCollectController.18", "Undefiniert"), localeDelegate.getMessage("MasterDataCollectController.3", "Details"),
				localeDelegate.getMessage("MasterDataCollectController.1", "Bearbeiten"), localeDelegate.getMessage("MasterDataCollectController.10", "Neueingabe"),
				localeDelegate.getMessage("MasterDataCollectController.12", "Neueingabe (Ge\u00e4ndert)"), localeDelegate.getMessage("MasterDataCollectController.15", "Sammelbearbeitung"),
				localeDelegate.getMessage("MasterDataCollectController.16", "Sammelbearbeitung (Ge\u00e4ndert)"), localeDelegate.getMessage("MasterDataCollectController.11", "Neueingabe (\u00dcbernahme Suchwerte)") };

		String sPrefix;
		String sSuffix = "";
		final String sMode;

		switch (iTab) {
		case CollectState.OUTERSTATE_DETAILS:
			sPrefix = (iMode == 1 || iMode == 2) ? "" : this.getEntityLabel() + " ";
			sMode = asDetailsMode[iMode];
			if (CollectState.isDetailsModeViewOrEdit(iMode)) {
				CollectableMasterData<UID> clct = this.getSelectedCollectable();
				String sIdentifier = localeDelegate.getTreeViewLabel(clct, getEntityUid(), MetaProvider.getInstance(), null);
				if (sIdentifier == null) {
					sIdentifier = "<ID=" + clct.getPrimaryKey() + ">";
				}
				sPrefix += sIdentifier.substring(sIdentifier.lastIndexOf(".") + 1);
			}
			else if (CollectState.isDetailsModeMultiViewOrEdit(iMode)) {
				sSuffix = localeDelegate.getMessage("MasterDataCollectController.19", " von {0} Objekten", this.getSelectedCollectables().size());
			}
			break;
		default:
			sPrefix = this.getEntityLabel();
			sMode = asTabs[iTab];
		}
		
		final StringBuilder sb; 
		if (iTab == CollectState.OUTERSTATE_DETAILS && (iMode == 1 || iMode == 2))
			sb = new StringBuilder(sPrefix + (iMode == 2 ? " - " + sMode : "") + sSuffix);
		else
			sb = new StringBuilder(sPrefix + (sPrefix.length() > 0 ? " - " : "") + sMode + sSuffix);

		if (isSelectedCollectableMarkedAsDeleted())
			sb.append(getSpringLocaleDelegate().getMessage("MasterDataCollectController.21"," (Objekt ist als gel\u00f6scht markiert)"));
		else if (isHistoricalView())
			sb.append(getSpringLocaleDelegate().getMessage("MasterDataCollectController.22"," (Historischer Zustand vom {0})", getHistoricalDate()));

		return sb.toString();
	}

	private class RuleCollectStateListener extends CollectStateAdapter {
		@Override
		public void detailsModeEntered(CollectStateEvent ev) throws NuclosBusinessException {
			final boolean bWriteAllowed = SecurityCache.getInstance().isWriteAllowedForMasterData(entityUid);
			getCodeCheckAction().setEnabled(bWriteAllowed);
		}
	}

	abstract class CodeCheckAction extends CommonAbstractAction {

		abstract void cmdCheckRuleSource();
		
		public CodeCheckAction() {
			super(Icons.getInstance().getIconValidate16(), 
			getSpringLocaleDelegate().getMessage("RuleCollectController.2", "Quelltext pr\u00fcfen"));
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			cmdCheckRuleSource();
		}
	}
	
}  // class CodeCollectController
