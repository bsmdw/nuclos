//Copyright (C) 2018  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.dbtransfer;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.TreeCellRenderer;

import org.apache.commons.lang.mutable.MutableInt;
import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;
import org.jdesktop.swingx.treetable.MutableTreeTableNode;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.explorer.node.NucletContentExplorerNode;
import org.nuclos.client.ui.Icons;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.dbtransfer.TransferEO;
import org.nuclos.common.dbtransfer.TransferTreeNode;
import org.nuclos.common2.SpringLocaleDelegate;

public class TransferTreeTableNode extends DefaultMutableTreeTableNode {

	public final static Icon ICON_IMPORT_INSERT = Icons.getInstance().getIconNucletChangeContentAdded();
	public final static Icon ICON_IMPORT_DELETE = Icons.getInstance().getIconNucletChangeContentDeleted();
	public final static Icon ICON_IMPORT_UPDATE = Icons.getInstance().getIconNucletChangeContentModified();
	//public final static Icon ICON_IMPORT_UNCHANGED = Icons.getInstance().getIconNucletChangeContentUnchanged();
	public final static Icon ICON_IMPORT_UNDO = Icons.getInstance().getIconNucletChangeContentUndo();

	public final static Icon ICON_LOCAL_ADDED = Icons.getInstance().getIconNucletChangeContentAdded();
	public final static Icon ICON_LOCAL_CHANGES = Icons.getInstance().getIconNucletChangeContentModified();
	public final static Icon ICON_LOCAL_UNCHANGED = Icons.getInstance().getIconNucletChangeContentUnchanged();

	public final static Icon ICON_ACTION_DO = Icons.getInstance().getIconNucletChangeActionDo();
	public final static Icon ICON_ACTION_DO_DISABLED = Icons.getInstance().getIconNucletChangeActionDoDisabled();
	public final static Icon ICON_ACTION_IGNORE = Icons.getInstance().getIconNucletChangeActionIgnore();
	public final static Icon ICON_ACTION_IGNORE_DISABLED = Icons.getInstance().getIconNucletChangeActionIgnoreDisabled();

	public final static Icon ICON_ACTION_MULTIPLE = Icons.getInstance().getIconNucletChangeActionMultiple();
	public final static Icon ICON_ACTION_MULTIPLE_DO = Icons.getInstance().getIconNucletChangeActionMultipleDo();
	public final static Icon ICON_ACTION_MULTIPLE_IGNORE = Icons.getInstance().getIconNucletChangeActionMultipleIgnore();

	public final static Icon ICON_CHILDREN_CHANGES = Icons.getInstance().getIconNucletChangeChildrenChanges();

	private final static String RESOURCE_PREFIX = "transfer.tree.table.node.";

	private boolean bAllowOnlyLocalChanges;

	private String sNodeLabel = null;
	private Icon nodeIcon = null;

	private SpringLocaleDelegate localeDelegate;

	public TransferTreeTableNode(final TransferTreeNode ttn, final boolean bAllowOnlyLocalChanges) {
		super(ttn);
		setAllowOnlyLocalChanges(bAllowOnlyLocalChanges);
		setSpringLocaleDelegate(SpringApplicationContextHolder.getBean(SpringLocaleDelegate.class));
	}

	final void setSpringLocaleDelegate(SpringLocaleDelegate cld) {
		this.localeDelegate = cld;
	}

	public void setAllowOnlyLocalChanges(final boolean bAllowOnlyLocalChanges) {
		this.bAllowOnlyLocalChanges = bAllowOnlyLocalChanges;
		final List<MutableTreeTableNode> children;
		for (Enumeration e = children(); e.hasMoreElements();) {
			TransferTreeTableNode child = (TransferTreeTableNode) e.nextElement();
			child.setAllowOnlyLocalChanges(bAllowOnlyLocalChanges);
		}
	}

	public boolean isAllowOnlyLocalChanges() {
		return bAllowOnlyLocalChanges;
	}

	public Object getValueAt(int column) {
		final TransferTreeNode ttn = (TransferTreeNode) this.getUserObject();
		final TransferEO teo = ttn.getTransferEO();

		if (teo == null) { // ROOT
			return null;
		}

		switch (column) {
			case 0: return ttn.getLabel();
			case 1: {
				if (ttn.getChangeTypeImport() == null && ttn.getChangeTypeLocal() == null) {
					if (ttn.getChangeTypeImportWithChildren() != null) {
						return ICON_CHILDREN_CHANGES;
					}
					return null;
				} else if (ttn.isChangeType(TransferTreeNode.CHANGE_TYPE_IMPORT.INSERT)) {
					return ICON_IMPORT_INSERT;
				} else if (ttn.isChangeType(TransferTreeNode.CHANGE_TYPE_IMPORT.UPDATE)) {
					return ICON_IMPORT_UPDATE;
				} else if (ttn.isChangeType(TransferTreeNode.CHANGE_TYPE_IMPORT.DELETE)) {
					return ICON_IMPORT_DELETE;
				} else if (ttn.isChangeType(TransferTreeNode.CHANGE_TYPE_LOCAL.LOCAL_ADDED)) {
					return ICON_IMPORT_UNDO;
				} else {
					return ICON_IMPORT_UNDO;//ICON_IMPORT_UNCHANGED;
				}
			}
			case 2: {
				MutableInt countChangedIgnores = new MutableInt();
				MutableInt countChangedDo = new MutableInt();
				MutableInt countChanged = new MutableInt();
				ttn.countIgnoreChangesWithChildren(countChangedIgnores, countChangedDo, countChanged);
				if (countChangedIgnores.intValue() == countChanged.intValue()) {
					if (ttn.getChangeTypeLocal() == null && bAllowOnlyLocalChanges) {
						return ICON_ACTION_IGNORE_DISABLED;
					}
					return ICON_ACTION_IGNORE;
				}
				if (countChangedDo.intValue() == countChanged.intValue()) {
					if (ttn.getChangeTypeLocal() == null && bAllowOnlyLocalChanges) {
						return ICON_ACTION_DO_DISABLED;
					}
					return ICON_ACTION_DO;
				}
				if (ttn.getChangeTypeImport() != null || ttn.getChangeTypeLocal() != null) {
					if (ttn.isIgnoreChange()) {
						if (countChangedDo.intValue() > 0) {
							if (ttn.getChangeTypeLocal() == null && bAllowOnlyLocalChanges) {
								return ICON_ACTION_MULTIPLE;
							}
							return ICON_ACTION_MULTIPLE_IGNORE;
						}
						if (ttn.getChangeTypeLocal() == null && bAllowOnlyLocalChanges) {
							return ICON_ACTION_IGNORE_DISABLED;
						}
						return ICON_ACTION_IGNORE;
					} else {
						if (countChangedIgnores.intValue() > 0) {
							if (ttn.getChangeTypeLocal() == null && bAllowOnlyLocalChanges) {
								return ICON_ACTION_MULTIPLE;
							}
							return ICON_ACTION_MULTIPLE_DO;
						}
						if (ttn.getChangeTypeLocal() == null && bAllowOnlyLocalChanges) {
							return ICON_ACTION_DO_DISABLED;
						}
						return ICON_ACTION_DO;
					}
				} else {
					return ICON_ACTION_MULTIPLE;
				}
			}
			case 3: {
				if (ttn.getChangeTypeImport() == null && ttn.getChangeTypeLocal() == null) {
					if (ttn.getChangeTypeLocalWithChildren() != null) {
						return ICON_CHILDREN_CHANGES;
					}
					return null;
				} else if (ttn.getChangeTypeLocal() == null) {
					if (ttn.isChangeType(TransferTreeNode.CHANGE_TYPE_IMPORT.INSERT)) {
						return null;
					}
					return ICON_LOCAL_UNCHANGED;
				} else if (ttn.isChangeType(TransferTreeNode.CHANGE_TYPE_LOCAL.LOCAL_ADDED)) {
					return ICON_LOCAL_ADDED;
				} else if (ttn.isChangeType(TransferTreeNode.CHANGE_TYPE_LOCAL.LOCAL_CHANGES)) {
					return ICON_LOCAL_CHANGES;
				} else {
					return null;
				}
			}
			case 4: {
				if (ttn.getChangeTypeImport() != null && ttn.getChangeTypeLocal() != null) {
					return ttn.isIgnoreChange() ? infoBold(1) : info(2);
				} else if (ttn.isChangeType(TransferTreeNode.CHANGE_TYPE_IMPORT.INSERT)) {
					return ttn.isIgnoreChange() ? infoBold(3) : info(4);
				} else if (ttn.isChangeType(TransferTreeNode.CHANGE_TYPE_IMPORT.UPDATE)) {
					return ttn.isIgnoreChange() ? infoBold(5) : info(6);
				} else if (ttn.isChangeType(TransferTreeNode.CHANGE_TYPE_IMPORT.DELETE)) {
					return ttn.isIgnoreChange() ? infoBold(7) : info(8);
				} else if (ttn.isChangeType(TransferTreeNode.CHANGE_TYPE_LOCAL.LOCAL_ADDED)) {
					return ttn.isIgnoreChange() ? info(9) : infoBold(10);
				} else if (ttn.isChangeType(TransferTreeNode.CHANGE_TYPE_LOCAL.LOCAL_CHANGES)) {
					return ttn.isIgnoreChange() ? info(11) : infoBold(12);
				}
			}
			default: return null;
		}
	}

	private String infoBold(int iId) {
		return getResourceLabel(RESOURCE_PREFIX + iId, true);
	}

	private String info(int iId) {
		return getResourceLabel(RESOURCE_PREFIX + iId, false);
	}

	private String getResourceLabel(String resLabel, boolean bold) {
		final String msg = localeDelegate.getMsg(resLabel);
		if (bold) {
			return "<html><b>"+msg+"</b></html>";
		}
		return msg;
	}

	public int getColumnCount() {
		return 5;
	}

	public boolean isEditable(int column) {
		return false;
	}

	public void setValueAt(Object aValue, int column) {
		//this.setUserObject(aValue);
	}

	@Override
	public void setParent(MutableTreeTableNode newParent) {
		parent = newParent;
	}

	@Override
	protected List<MutableTreeTableNode> createChildrenList() {
		final TransferTreeNode ttn = (TransferTreeNode) this.getUserObject();
		List<MutableTreeTableNode> result = new ArrayList<>();
		for (int i = 0; i < ttn.getChildCount(); i++) {
			final TransferTreeNode child = ttn.getChildAt(i);
			final TransferTreeNode.CHANGE_TYPE_IMPORT changeTypeImport = child.getChangeTypeImportWithChildren();
			final TransferTreeNode.CHANGE_TYPE_LOCAL changeTypeLocal = child.getChangeTypeLocalWithChildren();
			if (changeTypeImport == null && changeTypeLocal == null) {
				continue;
			}
			TransferTreeTableNode tttn = new TransferTreeTableNode(child, bAllowOnlyLocalChanges);
			tttn.setParent(this);
			result.add(tttn);
		}
		return result;
	}

	public String getNodeLabel() {
		if (sNodeLabel == null) {
			final TransferTreeNode ttn = (TransferTreeNode) getUserObject();
			final TransferEO teo = ttn.getTransferEO();
			if (teo == null) {
				// root
				sNodeLabel = "";
			} else {
				final UID entityUID = teo.eo.getDalEntity();
				sNodeLabel = SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(MetaProvider.getInstance().getEntity(entityUID)) +
						": " + ttn.getLabel();
				nodeIcon = NucletContentExplorerNode.getIcon(entityUID);
			}
		}
		return sNodeLabel;
	}

	public Icon getNodeIcon() {
		getNodeLabel();
		return nodeIcon;
	}

	public String getToolTipText(int col) {
		if (col == 2) {
			final Object actionIcon = getValueAt(col);
			if (actionIcon == ICON_ACTION_DO || actionIcon == ICON_ACTION_DO_DISABLED) {
				return info(13);
			}
			if (actionIcon == ICON_ACTION_MULTIPLE_DO) {
				return info(14);
			}
			if (actionIcon == ICON_ACTION_IGNORE || actionIcon == ICON_ACTION_IGNORE_DISABLED) {
				return info(15);
			}
			if (actionIcon == ICON_ACTION_MULTIPLE_IGNORE) {
				return info(16);
			}
		}
		if (col == 1) {
			final Object importIcon = getValueAt(col);
			if ((importIcon == ICON_IMPORT_INSERT)) {
				return info(17);
			}
			if ((importIcon == ICON_IMPORT_UPDATE)) {
				return info(18);
			}
			if ((importIcon == ICON_IMPORT_DELETE)) {
				return info(19);
			}
			if ((importIcon == ICON_IMPORT_UNDO)) {
				return info(20);
			}
			if ((importIcon == ICON_CHILDREN_CHANGES)) {
				return info(24);
			}
		}
		if (col == 3) {
			final Object localIcon = getValueAt(col);
			if ((localIcon == ICON_LOCAL_ADDED)) {
				return info(21);
			}
			if ((localIcon == ICON_LOCAL_CHANGES)) {
				return info(22);
			}
			if ((localIcon == ICON_LOCAL_UNCHANGED)) {
				return info(23);
			}
			if ((localIcon == ICON_CHILDREN_CHANGES)) {
				return info(24);
			}
		}
		return null;
	}

	public static class TransferTreeCellRenderer implements TreeCellRenderer {
		private JLabel label;

		TransferTreeCellRenderer() {
			label = new JLabel();
		}

		public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded,
													  boolean leaf, int row, boolean hasFocus) {
			final TransferTreeTableNode tttn = ((TransferTreeTableNode) value);
			label.setText(tttn.getNodeLabel());
			label.setIcon(tttn.getNodeIcon());
			return label;
		}

	}
}
