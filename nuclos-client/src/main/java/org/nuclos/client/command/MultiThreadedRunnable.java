//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.command;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.nuclos.client.remote.NuclosHttpInvokerAttributeContext;
import org.nuclos.common2.CommonRunnable;



public abstract class MultiThreadedRunnable implements CommonRunnable {
	
	private NuclosHttpInvokerAttributeContext ctx;
	
	private final HashMap<String, Serializable> attrInput = new HashMap<String, Serializable>();
	
	private boolean attrInputIsSupported = false;
	
	private Integer attrMessageReceiverId;
	
	private MTRListener listener;
	
	public void setAttributeContext(NuclosHttpInvokerAttributeContext ctx) {
		this.ctx = ctx;
	}
	
	/**
	 * Call this method to repeat the setting of the id. 
	 * This is necessary for SwingWorkers and other Threads which calls the server. 
	 */
	public void repeatAttributeContextSetting() {
		this.ctx.setMessageReceiver(attrMessageReceiverId);
		this.ctx.clear();
		this.ctx.putAll(attrInput);
		this.ctx.setSupported(attrInputIsSupported);
	}

	public void setAttrMessageReceiver(Integer id) {
		this.attrMessageReceiverId = id;
		if (this.ctx == null) {
			throw new IllegalStateException("NuclosHttpInvokerAttributeContext is not set");
		}
		this.ctx.setMessageReceiver(id);
	}
	
	public void putAttrInput(String key, Serializable object) {
		this.attrInput.put(key, object);
		if (this.ctx == null) {
			throw new IllegalStateException("NuclosHttpInvokerAttributeContext is not set");
		}
		this.ctx.put(key, object);
	}

	public void putAllAttrInput(Map<String, Serializable> entries) {
		if (entries == null) {
			return;
		}
		this.attrInput.putAll(entries);
		if (this.ctx == null) {
			throw new IllegalStateException("NuclosHttpInvokerAttributeContext is not set");
		}
		this.ctx.putAll(entries);
	}
	
	public void clearAttrInput() {
		this.attrInput.clear();
		if (this.ctx == null) {
			throw new IllegalStateException("NuclosHttpInvokerAttributeContext is not set");
		}
		this.ctx.clear();
	}

	public void setAttrInputIsSupported(boolean value) {
		this.attrInputIsSupported = value;
		if (this.ctx == null) {
			throw new IllegalStateException("NuclosHttpInvokerAttributeContext is not set");
		}
		this.ctx.setSupported(value);
	}

	public void setListener(MTRListener listener) {
		this.listener = listener;
	}

	public void finishedWithError(Exception ex) {
		if (this.listener == null) {
			throw new IllegalStateException("MultiThreadedRunnable Listener is not set");
		}
		this.listener.error(ex);
	}

	public void finished() {
		if (this.listener == null) {
			throw new IllegalStateException("MultiThreadedRunnable Listener is not set");
		}
		this.listener.done();
	}

}
