package org.nuclos.client.command;

import java.awt.Component;

/**
 * "null command handler": does nothing. ("Null Object" pattern)
 */
public final class NullCommandHandler implements CommandHandler {
	@Override
    public void commandStarted(Component parent) {
	}

	@Override
    public void commandFinished(Component parent) {
	}
}	// class NullCommandHandler
