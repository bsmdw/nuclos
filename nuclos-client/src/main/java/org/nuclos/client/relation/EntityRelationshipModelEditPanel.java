//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.relation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EventObject;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.log4j.Logger;
import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.NuclosCollectControllerFactory;
import org.nuclos.client.generation.GenerationCollectController;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.masterdata.MetaDataDelegate;
import org.nuclos.client.ui.collect.CollectableComponentsProvider;
import org.nuclos.client.ui.collect.DefaultCollectableComponentsProvider;
import org.nuclos.client.ui.collect.component.CollectableTextField;
import org.nuclos.client.wizard.ShowNuclosWizard;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NucletFieldMeta;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.TranslationVO;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.transport.vo.FieldMetaTransport;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

import com.mxgraph.io.mxCodecRegistry;
import com.mxgraph.io.mxModelCodec;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;
import com.mxgraph.model.mxGraphModel;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.view.mxCellEditor;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxEventSource.mxIEventListener;
import com.mxgraph.util.mxPoint;
import com.mxgraph.util.mxUtils;
import com.mxgraph.view.mxGraph;

import info.clearthought.layout.TableLayout;

/**
 * Details edit panel for state model administration. Contains the state model editor.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
@Deprecated
public class EntityRelationshipModelEditPanel extends JPanel {
	
	private static final Logger LOG = Logger.getLogger(EntityRelationshipModelEditPanel.class);

	public static String[] labels = TranslationVO.LABELS_FIELD;
	public static String ENTITYSTYLE = "rounded=1";
	public static String DIAMONDARROW = "endArrow=diamond";
	public static String OPENARROW = "endArrow=open";
	public static String OVALARROW = "endArrow=oval";
	public static String EDGESTYLE = "edgeStyle";
	public static String ELBOWCONNECTOR = "mxEdgeStyle.ElbowConnector";
	public static String SYMBOLCOLOR = "#6482B9";
	
	private int xPos; 
	private int yPos;
	
	private JPanel mainPanel;
	
	private JPanel panelHeader;
	
	private CollectableTextField clcttfName = new CollectableTextField(
		EntityRelationshipModel.clcte.getEntityField(EntityRelationshipModel.FIELDNAME_NAME));
	
	private CollectableTextField clcttfDescription = new CollectableTextField(
		EntityRelationshipModel.clcte.getEntityField(EntityRelationshipModel.FIELDNAME_DESCRIPTION));
	
	private mxGraphComponent graphComponent;
	
	private MainFrame mf;
	
	private List<ChangeListener> lstChangeListener;
	
	private List<mxCell> lstRelations;
	
	private Map<EntityMeta<?>, Set<NucletFieldMeta<?>>> mpRemoveRelation;
	
	private boolean isPopupShown;
	
	// former Spring injection
	
	private SpringLocaleDelegate localeDelegate;
	
	// end of former Spring injection
	

	public EntityRelationshipModelEditPanel(MainFrame mf) {
		super(new BorderLayout());
		this.mf = mf;
		
		setSpringLocaleDelegate(SpringApplicationContextHolder.getBean(SpringLocaleDelegate.class));
		if (getSpringLocaleDelegate() == null) {
			setSpringLocaleDelegate(SpringLocaleDelegate.getInstance());
			LOG.warn("Setting SpringLocaleDelegate hasn't worked as expected");
		}
		
		lstChangeListener = new ArrayList<ChangeListener>();
		lstRelations = new ArrayList<mxCell>();
		mpRemoveRelation = new HashMap<EntityMeta<?>, Set<NucletFieldMeta<?>>>();
		init();		
	}
	
	final void setSpringLocaleDelegate(SpringLocaleDelegate cld) {
		this.localeDelegate = cld;
	}
	
	final SpringLocaleDelegate getSpringLocaleDelegate() {
		return localeDelegate;
	}
	
	public void setIsPopupShown(boolean shown) {
		this.isPopupShown = shown;
	}
	
	class MyGraph extends mxGraph {
		
		public MyGraph(mxGraphModel model) {
			super(model);
		}
		
		@Override
		public String getToolTipForCell(Object obj) {
			
			mxCell cell = (mxCell)obj;
			
			boolean blnShow = true;
			
			StringBuffer sb = new StringBuffer();
			sb.append("<html><body>");
			
			if(cell.getValue() != null && ENTITYSTYLE.equals(cell.getStyle())) {
				if(cell.getValue() instanceof EntityMeta) {
					EntityMeta vo = (EntityMeta)cell.getValue();
					sb.append(vo.getEntityName());
				}
			}
			else if(cell.getStyle() != null) {
				final SpringLocaleDelegate localeDelegate = getSpringLocaleDelegate();
				String sStyle = cell.getStyle();
				if(sStyle.indexOf(OPENARROW) >= 0) {
					if(cell.getSource() != null && cell.getTarget() != null) {
						EntityMeta voSource = (EntityMeta)cell.getSource().getValue();
						EntityMeta voTarget = (EntityMeta)cell.getTarget().getValue();
						sb.append(localeDelegate.getMessage("nuclos.entityrelation.editor.2", "", voSource.getEntityName(), voTarget.getEntityName()));
					}
					else 
						sb.append(localeDelegate.getMessage("nuclos.entityrelation.editor.1", "Bezug zu Stammdaten"));
				}
				else if(sStyle.indexOf(DIAMONDARROW) >= 0) {
					if(cell.getSource() != null && cell.getTarget() != null) {
						EntityMeta voSource = (EntityMeta)cell.getSource().getValue();
						EntityMeta voTarget = (EntityMeta)cell.getTarget().getValue();
						sb.append(localeDelegate.getMessage("nuclos.entityrelation.editor.4", "", voSource.getEntityName(), voTarget.getEntityName()));						
					}
					else
					sb.append(localeDelegate.getMessage("nuclos.entityrelation.editor.3", "Bezug zu Vorg\u00e4ngen (Unterformularbezug)"));
				}
				else if(sStyle.indexOf(OVALARROW) >= 0) {
					if(cell.getSource() != null && cell.getTarget() != null) {
						EntityMeta voSource = (EntityMeta)cell.getSource().getValue();
						EntityMeta voTarget = (EntityMeta)cell.getTarget().getValue();
						sb.append(localeDelegate.getMessage("nuclos.entityrelation.editor.6", "", voSource.getEntityName(), voTarget.getEntityName()));
					}
					else
						sb.append(localeDelegate.getMessage("nuclos.entityrelation.editor.5", ""));
				}
			}
			else {
				blnShow = false;
			}
			sb.append("</body></html>");
			
			return blnShow ? sb.toString() : "";
		}
		
	}
	
	protected void init() {
		final SpringLocaleDelegate localeDelegate = getSpringLocaleDelegate();
		mainPanel = new JPanel();
		
		double sizeHeader [][] = {{TableLayout.PREFERRED, 5, TableLayout.PREFERRED, 10}, {10, 25,10}};		
		panelHeader = new JPanel();
		panelHeader.setLayout(new TableLayout(sizeHeader));
		clcttfName.setLabelText(localeDelegate.getMessage("nuclos.entityfield.entityrelation.name.label","Name"));
		clcttfName.setToolTipText(localeDelegate.getMessage("nuclos.entityfield.entityrelation.name.description","Name"));
		clcttfName.setColumns(20);
		panelHeader.add(this.clcttfName.getJComponent(), "0,1");
		clcttfDescription.setLabelText(localeDelegate.getMessage("nuclos.entityfield.entityrelation.description.label","Beschreibung"));
		clcttfDescription.setToolTipText(localeDelegate.getMessage("nuclos.entityfield.entityrelation.description.description","Beschreibung"));
		clcttfDescription.setColumns(20);
		panelHeader.add(this.clcttfDescription.getJComponent(), "2,1");
		
		double size [][] = {{5,TableLayout.FILL, 5}, {35,5, TableLayout.FILL, 5}};
		
		TableLayout layout = new TableLayout(size);
		layout.setVGap(3);
		layout.setHGap(5);
		mainPanel.setLayout(layout);
		
		mainPanel.add(panelHeader, "1,0");
		MyGraphModel model = new MyGraphModel(graphComponent, this, mf);
		
		mxGraph myGraph = new MyGraph(model);

		mxCodecRegistry.register(new mxModelCodec(model));
		mxCodecRegistry.register(new mxModelCodec(new java.sql.Date(System.currentTimeMillis())));
		mxCodecRegistry.register(new mxModelCodec(new Integer(0)));
		
			
		addEventListener(myGraph);
		
		graphComponent = new mxGraphComponent(myGraph);
		graphComponent.setGridVisible(true);
		graphComponent.getViewport().setOpaque(false);
		graphComponent.setBackground(Color.WHITE);
		graphComponent.setToolTips(true);
		
		graphComponent.setCellEditor(new MyCellEditor(graphComponent));
		
		model.setGraphComponent(graphComponent);
		
		graphComponent.addKeyListener(new KeyAdapter() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				if(e.getKeyChar() == KeyEvent.VK_DELETE) {
					mxCell cell = (mxCell)graphComponent.getGraph().getSelectionModel().getCell();
					if(cell.getValue() instanceof EntityMeta) {
						int iEdge = cell.getEdgeCount();
						for(int i = 0; i < iEdge; i++){
							mxCell cellRelation = (mxCell)cell.getEdgeAt(i);
							getGraphModel().remove(cellRelation);
						}
						getGraphModel().remove(cell);				
						fireChangeListenEvent();
					}
					else if(cell.getValue() instanceof FieldMeta) {
						int opt = JOptionPane.showConfirmDialog(mainPanel, localeDelegate.getMessage(
								"nuclos.entityrelation.editor.7", "M\u00f6chten Sie die Verbindung wirklich l\u00f6sen?"));
						if(opt != 0){
							return;
						}
						mxCell cellSource = (mxCell)cell.getSource();
						if(cellSource != null && cellSource.getValue() instanceof EntityMeta) {
							EntityMeta<?> metaSource = (EntityMeta)cellSource.getValue();
							if(cell.getValue() instanceof FieldMeta) {
								NucletFieldMeta voField = (NucletFieldMeta)cell.getValue();
								voField.flagRemove();
								
								List<FieldMetaTransport> toList = new ArrayList<FieldMetaTransport>();
								FieldMetaTransport toField = new FieldMetaTransport();
								
								toField.setEntityFieldMeta(voField);
								toList.add(toField);
								
								MetaDataDelegate.getInstance().modifyEntityMetaData(metaSource.getUID(), toList);
								
								if(mpRemoveRelation.containsKey(metaSource)){
									mpRemoveRelation.get(metaSource).add(voField);							
								}
								else {
									Set<NucletFieldMeta<?>> s = new HashSet<NucletFieldMeta<?>>();
									s.add(voField);
									mpRemoveRelation.put(metaSource, s);
								}
								
							}
						}
						
						mxGraphModel model =  (mxGraphModel)graphComponent.getGraph().getModel();
						model.remove(cell);				
						EntityRelationshipModelEditPanel.this.fireChangeListenEvent();
					}
					else if(cell.getValue() != null && cell.getValue() instanceof String){
						String sValue = (String)cell.getValue();
						if(sValue.length() == 0) {
							getGraphModel().remove(cell);
							EntityRelationshipModelEditPanel.this.fireChangeListenEvent();
						}
					}
				}
			}
			
		});
		
		createMouseWheelListener();
		
		createMouseListener();		
		
		//mainPanel.add(graphComponent, "1,2, 4,4");
		mainPanel.add(graphComponent, "1,2");
		
		this.add(mainPanel);
		
	}
	
	mxGraphComponent getGraphComponent() {
		return graphComponent;
	}
	
	public mxGraphModel getGraphModel() {
		return (mxGraphModel)graphComponent.getGraph().getModel();
	}
	
	public void clearRelationModel() {
		try {
			mxGraphModel model =  (mxGraphModel)graphComponent.getGraph().getModel();
			model.clear();
			fireChangeListenEvent();
		}
		catch(Exception e) {
			LOG.warn("clearRelationModel failed: " + e, e);
		}
	}
	
	public void refresh() {
		graphComponent.repaint();
		graphComponent.getGraph().getView().reload();
	}
	 

	private void createMouseWheelListener() {
		graphComponent.getGraphControl().addMouseWheelListener(new MouseWheelListener() {
			
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				
				if(e.getModifiers() == InputEvent.CTRL_MASK) {
					if(e.getWheelRotation() <= 0) {						
						graphComponent.zoomIn();
					}
					else {
						if(graphComponent.getGraph().getView().getScale() > 0.2) 
							graphComponent.zoomOut();
					}
				}
			
			}
		});
	}

	private void createMouseListener() {
		graphComponent.getGraphControl().addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				if(isPopupShown) {
					isPopupShown = false;
					mxCell cell = (mxCell)graphComponent.getGraph().getSelectionModel().getCell();
					getGraphModel().remove(cell);
					return;
				}
				if(SwingUtilities.isRightMouseButton(e)) {
					xPos = e.getX();
					yPos = e.getY();
					Object obj = graphComponent.getCellAt(e.getX(), e.getY());
					
					if(obj instanceof mxCell) {
						mxCell cell = (mxCell)obj;
						if(cell.getStyle() != null && cell.getStyle().indexOf(ENTITYSTYLE) >= 0 && cell.getValue() instanceof EntityMeta) {
							JPopupMenu pop = createPopupMenuEntity(cell, false);
							pop.show(e.getComponent(), e.getX(), e.getY());
						}
						else if(cell.getStyle() != null && cell.getStyle().indexOf(ENTITYSTYLE) >= 0) {
							JPopupMenu pop = createPopupMenuEntity(cell, true);
							pop.show(e.getComponent(), e.getX(), e.getY());
						}
						else {
							if(cell.getStyle() != null && cell.getStyle().indexOf("oval") >= 0) {
								JPopupMenu pop = createRelationPopupMenu(cell, true, true);
								pop.show(e.getComponent(), e.getX(), e.getY());
							}
							else {
								JPopupMenu pop = createRelationPopupMenu(cell, true, false);
								pop.show(e.getComponent(), e.getX(), e.getY());
							}
						}
					}
					else {
						JPopupMenu pop = createPopupMenu();
						pop.show(e.getComponent(), e.getX(), e.getY());
					}
				}
				else if(SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2) {
					mxCell cell = (mxCell)graphComponent.getGraph().getSelectionModel().getCell();
					if(cell == null)
						return;
					if(cell.getValue() != null && cell.getValue() instanceof EntityMeta) {
						EntityMeta voMeta = (EntityMeta)cell.getValue();						
						EntityMeta vo = MetaProvider.getInstance().getEntity(voMeta.getUID());
						new ShowNuclosWizard.NuclosWizardEditRunnable(false, mf.getHomePane(), vo).run();
					}
					else if(cell.getValue() != null && cell.getValue() instanceof FieldMeta) {
						if(cell.getStyle() != null && cell.getStyle().indexOf(OPENARROW) >= 0)
							editMasterdataRelation(cell);
						else if(cell.getStyle() != null && cell.getStyle().indexOf(DIAMONDARROW) >= 0) {
							editSubformRelation(cell);
						}
						 
							
					}
					else if(cell.getValue() != null) {
						if(cell.getStyle() != null && cell.getStyle().indexOf(OVALARROW) >= 0) {
							try {
								
								mxCell cellSource = (mxCell)cell.getSource();
								mxCell cellTarget = (mxCell)cell.getTarget();
								
								EntityMeta sourceModule = (EntityMeta)cellSource.getValue();
								EntityMeta targetModule = (EntityMeta)cellTarget.getValue();
								
								UID sSourceModule = sourceModule.getUID();
								UID sTargetModule = targetModule.getUID();
								
								boolean blnFound = false;
								
								for(MasterDataVO voGeneration : MasterDataCache.getInstance().get(E.GENERATION.getUID())) {
									UID sSource = (UID)voGeneration.getFieldValue(E.GENERATION.sourceModule.getUID());
									UID sTarget = (UID)voGeneration.getFieldValue(E.GENERATION.targetModule.getUID());
									
									if(org.apache.commons.lang.ObjectUtils.equals(sSource, sSourceModule) && 
										org.apache.commons.lang.ObjectUtils.equals(sTarget, sTargetModule)){
										GenerationCollectController gcc = (GenerationCollectController)NuclosCollectControllerFactory.getInstance().newMasterDataCollectController(E.GENERATION.getUID(), null, null);
										gcc.runViewSingleCollectableWithId((UID)voGeneration.getPrimaryKey());
										blnFound = true;
										break;
									}
									
								}					
								if(!blnFound) {
									GenerationCollectController gcc = (GenerationCollectController)NuclosCollectControllerFactory.getInstance().newMasterDataCollectController(E.GENERATION.getUID(), null, null);
									Map<UID, Object> mp = new HashMap<UID, Object>();
									mp.put(E.GENERATION.sourceModule.getUID(), sSourceModule);									
									mp.put(E.GENERATION.targetModule.getUID(), sTargetModule);
									MasterDataVO<UID> vo = new MasterDataVO<UID>(E.GENERATION.getUID(), null, null, null, null, null, null, mp, null, null, true);
									gcc.runWithNewCollectableWithSomeFields(vo);
								}
							}
							catch(NuclosBusinessException e1) { 
								LOG.warn("mousePressed failed: " + e1, e1);
							}
							catch(CommonPermissionException e1) { 
								LOG.warn("mousePressed failed: " + e1, e1);
							}
							catch(CommonFatalException e1) { 
								LOG.warn("mousePressed failed: " + e1, e1);
							}
							catch(CommonBusinessException e1) { 
								LOG.warn("mousePressed failed: " + e1, e1);
							}				
						}
					}
				}
			}

		
		});
	}

	private void addEventListener(mxGraph myGraph) {
		myGraph.addListener(mxEvent.ADD_CELLS, new mxIEventListener() {
			
			@Override
			public void invoke(Object sender, mxEventObject evt) {
				fireChangeListenEvent();				
			}
		});
		
		myGraph.addListener(mxEvent.CELLS_ADDED, new mxIEventListener() {
			
			@Override
			public void invoke(Object sender, mxEventObject evt) {
				fireChangeListenEvent();				
			}
		});
		
		myGraph.addListener(mxEvent.ADD, new mxIEventListener() {
			
			@Override
			public void invoke(Object sender, mxEventObject evt) {
				fireChangeListenEvent();				
			}
		});
		
		myGraph.addListener(mxEvent.CONNECT_CELL, new mxIEventListener() {
			
			@Override
			public void invoke(Object sender, mxEventObject evt) {
				fireChangeListenEvent();				
			}
		});
		
		
		
		myGraph.addListener(mxEvent.CELL_CONNECTED, new mxIEventListener() {
			
			@Override
			public void invoke(Object sender, mxEventObject evt) {
				if(sender instanceof MyGraph) {
					MyGraph graph = (MyGraph)sender;
					mxCell cell = (mxCell)graph.getSelectionModel().getCell();					
					if(cell != null && cell.getStyle() != null) {
						if(cell.getStyle().indexOf(OPENARROW) >= 0 || cell.getStyle().indexOf(DIAMONDARROW) >= 0
							|| cell.getStyle().indexOf(OVALARROW) >= 0) {
							if(cell.getValue() != null && cell.getValue() instanceof FieldMeta) {
								FieldMeta voField = (FieldMeta)cell.getValue();
								Boolean blnSource = (Boolean)evt.getProperty("source");
								mxCell cellPrevious = (mxCell)evt.getProperty("previous");
								mxCell cellTerminal = (mxCell)evt.getProperty("terminal");
								if(blnSource){
									if(cellTerminal != null && cellPrevious != null) {
										EntityMeta vo = MetaProvider.getInstance().getEntity(voField.getEntity());
										if(!vo.getUID().equals(voField.getForeignEntity())) {
											cell.setSource(cellPrevious);
										}
									}
									else if(cellTerminal == null) {
										cell.setSource(cellPrevious);
									}
								}
								else {
									if(cellTerminal != null && cellPrevious != null) {
										EntityMeta vo = (EntityMeta)cellTerminal.getValue();
										if(!vo.getUID().equals(voField.getForeignEntity())) {
											cell.setTarget(cellPrevious);
										}
									}
									else if(cellTerminal == null) {
										cell.setTarget(cellPrevious);
									}
								}
							}
							else if(cell.getValue() != null && cell.getValue() instanceof FieldMeta) {
								
							}
						}
					}
				}
				fireChangeListenEvent();
			}
		});
		
		myGraph.addListener(mxEvent.CELLS_MOVED, new mxIEventListener() {
			
			@Override
			public void invoke(Object sender, mxEventObject evt) {
				graphComponent.getGraph().refresh();
				fireChangeListenEvent();
			}
		});
		
		myGraph.addListener(mxEvent.MOVE_CELLS, new mxIEventListener() {
			
			@Override
			public void invoke(Object sender, mxEventObject evt) {
				graphComponent.getGraph().refresh();
				fireChangeListenEvent();
			}
		});
		
		myGraph.addListener(mxEvent.CELLS_REMOVED, new mxIEventListener() {
			
			@Override
			public void invoke(Object sender, mxEventObject evt) {
				fireChangeListenEvent();
			}
		});
		
		
	}
	
	protected JPopupMenu createPopupMenu() {
		
		JPopupMenu pop = new JPopupMenu();
		
		JMenuItem i1 = new JMenuItem(getSpringLocaleDelegate().getMessage("nuclos.entityrelation.editor.16", "neue Entit\u00e4t"));
		i1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				int x = xPos;				
				int y = yPos;
				
				mxGeometry mxgeo = new mxGeometry(x, y, 100, 80);
				mxgeo.setSourcePoint(new mxPoint(100,100));
				mxgeo.setTargetPoint(new mxPoint(150,150));
				
				mxCell cell = new mxCell("", mxgeo, ENTITYSTYLE);
				cell.setVertex(true);
				mxCell cellRoot = (mxCell)graphComponent.getGraph().getModel().getRoot();
				mxCell cellContainer = (mxCell)cellRoot.getChildAt(0);
				int childcount = cellContainer.getChildCount();
				getGraphModel().add(cellContainer, cell, childcount);
				getGraphComponent().refresh();
				
			}
		});
		
		JMenuItem i3 = new JMenuItem(getSpringLocaleDelegate().getMessage("nuclos.entityrelation.editor.8", "zoom in"));
		i3.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				graphComponent.zoomIn();
			}
		});
		
		JMenuItem i4 = new JMenuItem(getSpringLocaleDelegate().getMessage("nuclos.entityrelation.editor.9", "zoom out"));
		i4.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				graphComponent.zoomOut();
			}
		});
		
		pop.add(i1);
		pop.addSeparator();
		pop.add(i3);
		pop.add(i4);
		
		return pop;	
	}
	
	
	protected JPopupMenu createRelationPopupMenu(final mxCell cell, boolean delete, boolean objectGeneration) {
		final SpringLocaleDelegate localeDelegate = getSpringLocaleDelegate();
		
		JPopupMenu pop = new JPopupMenu();
		JMenuItem i1 = new JMenuItem(localeDelegate.getMessage("nuclos.entityrelation.editor.10","Bezug zu Stammdaten bearbeiten"));
		
		i1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
					editMasterdataRelation(cell);
			}
		});
		
		JMenuItem i2 = new JMenuItem(localeDelegate.getMessage("nuclos.entityrelation.editor.11", "Unterfomularbezug bearbeiten"));
		i2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				editSubformRelation(cell);
			}

		});
		
		JMenuItem i4 = new JMenuItem(localeDelegate.getMessage("nuclos.entityrelation.editor.12", "Verbindung l\u00f6sen"));
		i4.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				int opt = JOptionPane.showConfirmDialog(mainPanel, localeDelegate.getMessage(
						"nuclos.entityrelation.editor.7", "M\u00f6chten Sie die Verbindung wirklich l\u00f6sen?")); 
				if(opt != 0){
					return;
				}
				mxCell cellSource = (mxCell)cell.getSource();
				if(cellSource != null && cellSource.getValue() instanceof EntityMeta) {
					EntityMeta<?> metaSource = (EntityMeta)cellSource.getValue();
					if(cell.getValue() instanceof FieldMeta) {
						NucletFieldMeta<?> voField = (NucletFieldMeta)cell.getValue();
						voField.flagRemove();
						
						List<FieldMetaTransport> toList = new ArrayList<FieldMetaTransport>();
						FieldMetaTransport toField = new FieldMetaTransport();
						toField.setEntityFieldMeta(voField);
						toList.add(toField);
						
						MetaDataDelegate.getInstance().modifyEntityMetaData(metaSource.getUID(), toList);
						
						if(mpRemoveRelation.containsKey(metaSource)){
							mpRemoveRelation.get(metaSource).add(voField);							
						}
						else {
							Set<NucletFieldMeta<?>> s = new HashSet<NucletFieldMeta<?>>();
							s.add(voField);
							mpRemoveRelation.put(metaSource, s);
						}
					}
					else if(cell.getValue() != null && cell.getValue() instanceof String) {
						
					}
				}
				
				mxGraphModel model =  (mxGraphModel)graphComponent.getGraph().getModel();
				model.remove(cell);				
				EntityRelationshipModelEditPanel.this.fireChangeListenEvent();
			}
		});
		
		JMenuItem i5 = new JMenuItem(localeDelegate.getMessage("nuclos.entityrelation.editor.13", "Objektgenerator bearbeiten"));
		i5.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					
					mxCell cellSource = (mxCell)cell.getSource();
					mxCell cellTarget = (mxCell)cell.getTarget();
					
					EntityMeta sourceModule = (EntityMeta)cellSource.getValue();
					EntityMeta targetModule = (EntityMeta)cellTarget.getValue();
					
					UID sSourceModule = sourceModule.getUID();
					UID sTargetModule = targetModule.getUID();
					
					boolean blnFound = false;
					
					for(MasterDataVO voGeneration : MasterDataCache.getInstance().get(E.GENERATION.getUID())) {
						UID sSource = (UID)voGeneration.getFieldValue(E.GENERATION.sourceModule.getUID());
						UID sTarget = (UID)voGeneration.getFieldValue(E.GENERATION.targetModule.getUID());
						
						if(org.apache.commons.lang.ObjectUtils.equals(sSource, sSourceModule) && 
							org.apache.commons.lang.ObjectUtils.equals(sTarget, sTargetModule)){
							GenerationCollectController gcc = (GenerationCollectController)NuclosCollectControllerFactory.getInstance().newMasterDataCollectController(E.GENERATION.getUID(), null, null);
							gcc.runViewSingleCollectableWithId((UID)voGeneration.getPrimaryKey());
							blnFound = true;
							break;
						}
						
					}					
					if(!blnFound) {
						GenerationCollectController gcc = (GenerationCollectController)NuclosCollectControllerFactory.getInstance().newMasterDataCollectController(E.GENERATION.getUID(), null, null);
						Map<UID, Object> mp = new HashMap<UID, Object>();
						mp.put(E.GENERATION.sourceModule.getUID(), sSourceModule);
						mp.put(E.GENERATION.targetModule.getUID(), sTargetModule);
						MasterDataVO<UID> vo = new MasterDataVO<UID>(E.GENERATION.getUID(), null, null, null, null, null, null, mp, null, null, true);
						gcc.runWithNewCollectableWithSomeFields(vo);
					}
				}
				catch(NuclosBusinessException e1) {
					LOG.warn("actionPerformed failed: " + e1, e1);
				}
				catch(CommonPermissionException e1) {
					LOG.warn("actionPerformed failed: " + e1, e1);
				}
				catch(CommonFatalException e1) {
					LOG.warn("actionPerformed failed: " + e1, e1);
				}
				catch(CommonBusinessException e1) {
					LOG.warn("actionPerformed failed: " + e1, e1);
				}				
			}
		});
		
		if(cell.getStyle() != null && cell.getStyle().indexOf(OPENARROW) >= 0) {
			i1.setSelected(true);
			pop.add(i1);
		}
		else if(cell.getStyle() != null && cell.getStyle().indexOf(DIAMONDARROW) >= 0) {
			i2.setSelected(true);
			pop.add(i2);
		}	
		
		if(objectGeneration) {
			//pop.addSeparator();
			pop.add(i5);
		}		
		
		if(delete) {
			pop.addSeparator();
			pop.add(i4);
		}	
		return pop;	
	}

	public Map<EntityMeta, Set<FieldMetaTransport>> getMetaDataModel() {
		Map<EntityMeta, Set<FieldMetaTransport>> mpModel = new HashMap<EntityMeta, Set<FieldMetaTransport>>();
		List<mxCell> lstRelation = new ArrayList<mxCell>();
		List<mxCell> lstRelationNotValid = new ArrayList<mxCell>();
		mxGraph graph = graphComponent.getGraph();
		mxCell root = (mxCell)graph.getModel().getRoot();
		int rootChildCount = root.getChildCount();
		if(rootChildCount == 1) {
			mxCell cellContainer = (mxCell)root.getChildAt(0);
			int childCount = cellContainer.getChildCount();
			for(int i = 0; i < childCount; i++) {
				mxCell cell = (mxCell)cellContainer.getChildAt(i);
				if((cell.getStyle() != null && cell.getStyle().indexOf(ENTITYSTYLE) >= 0) && cell.getValue() != null && cell.getValue() instanceof EntityMeta) {
					EntityMeta metaVO = (EntityMeta)cell.getValue();
					mpModel.put(metaVO, new HashSet<FieldMetaTransport>());
				}
				else if(cell.getStyle() != null && cell.getSource() != null && cell.getTarget() != null) {
					lstRelation.add(cell);
				}
				else if(cell.getStyle() != null && (cell.getSource() == null || cell.getTarget() == null)){
					lstRelationNotValid.add(cell);
				}
			}			
		}
		
		for(mxCell cell : lstRelationNotValid) {
			if(cell.getStyle().indexOf(OPENARROW) >= 0 || cell.getStyle().indexOf(DIAMONDARROW) >= 0) {
				if(cell.getValue() instanceof FieldMeta){
					FieldMeta voField = (FieldMeta)cell.getValue();
					String sMessage = "Verbindung von Referenz " + voField.getFieldName() + " sind nicht alle gegeben!";
					throw new NuclosFatalException(sMessage);
				}
				else {
					throw new NuclosFatalException("Nicht alle Verbindungen gesetzt");
				}
			}
		}
		
		for(mxCell cell : lstRelation) {
			if(cell.getStyle().indexOf(OPENARROW) >= 0 || cell.getStyle().indexOf(DIAMONDARROW) >= 0) {
				mxCell cellSource = (mxCell)cell.getSource();
				FieldMetaTransport toField = new FieldMetaTransport();
				NucletFieldMeta voField = new NucletFieldMeta();
				
				if(cellSource.getValue() instanceof EntityMeta) {					
					EntityMeta metaSource = (EntityMeta)cellSource.getValue(); 
					if(cell.getValue() instanceof FieldMeta) {
						NucletFieldMeta vo = (NucletFieldMeta)cell.getValue();
						voField = vo;
					
						boolean blnHasThisRelation = false;
						
						for(FieldMeta voMetaField : MetaProvider.getInstance().getAllEntityFieldsByEntity(metaSource.getUID()).values()) {
							if(voField.getForeignEntity().equals(voMetaField.getForeignEntity())) {
								blnHasThisRelation = true;
								break;
							}
						}
						if(!blnHasThisRelation) {
							
						}

						toField.setEntityFieldMeta(voField);
						
						MyGraphModel model = (MyGraphModel)graph.getModel();
						if(model.getTranslation().size() != 0)
							toField.setTranslation(model.getTranslation().get(voField));
						
						mpModel.get(metaSource).add(toField);

					}
				}
				
			}
			
		}
		
		for(EntityMeta voMeta : mpRemoveRelation.keySet()) {
			for(NucletFieldMeta voField : mpRemoveRelation.get(voMeta)) {
				voField.flagRemove();
				FieldMetaTransport toField = new FieldMetaTransport();
				toField.setEntityFieldMeta(voField);
				mpModel.get(voMeta).add(toField);
			}
		}
		
		return mpModel;
	}
	
	public void clearModel() {
		mpRemoveRelation = new HashMap<EntityMeta<?>, Set<NucletFieldMeta<?>>>();
		clcttfName.getJTextField().setText("");
		clcttfDescription.setField(new CollectableValueField(null));
		clcttfDescription.getJTextField().setText("");
		
	}
	
	public List<EntityMeta<?>> getEntitiesInModel() {
		List<EntityMeta<?>> lstEntites = new ArrayList<EntityMeta<?>>();
		
		mxGraph graph = graphComponent.getGraph();
		mxCell root = (mxCell)graph.getModel().getRoot();
		int rootChildCount = root.getChildCount();
		if(rootChildCount == 1) {
			mxCell cellContainer = (mxCell)root.getChildAt(0);
			int childCount = cellContainer.getChildCount();
			for(int i = 0; i < childCount; i++) {
				mxCell cell = (mxCell)cellContainer.getChildAt(i);
				if((cell.getStyle() != null && cell.getStyle().indexOf(ENTITYSTYLE) >= 0) && cell.getValue() != null && cell.getValue() instanceof EntityMeta) {
					EntityMeta metaVO = (EntityMeta)cell.getValue();
					lstEntites.add(metaVO);
				}
			}
		}
		
		return lstEntites;
	}
	
	public List<FieldMeta> getEntityFieldsInModel() {
		List<FieldMeta> lstEntites = new ArrayList<FieldMeta>();
		
		mxGraph graph = graphComponent.getGraph();
		mxCell root = (mxCell)graph.getModel().getRoot();
		int rootChildCount = root.getChildCount();
		if(rootChildCount == 1) {
			mxCell cellContainer = (mxCell)root.getChildAt(0);
			int childCount = cellContainer.getChildCount();
			for(int i = 0; i < childCount; i++) {
				mxCell cell = (mxCell)cellContainer.getChildAt(i);
				if((cell.getStyle() != null && (cell.getStyle().indexOf(OPENARROW) >= 0 || cell.getStyle().indexOf(DIAMONDARROW) >= 0)) && cell.getValue() != null && cell.getValue() instanceof FieldMeta) {
					FieldMeta metaVO = (FieldMeta)cell.getValue();
					lstEntites.add(metaVO);
				}
			}
		}
		
		return lstEntites;
	}


	protected JPopupMenu createPopupMenuEntity(final mxCell cell, boolean newCell) {
		final SpringLocaleDelegate localeDelegate = getSpringLocaleDelegate();
		
		JPopupMenu pop = new JPopupMenu();
		JMenuItem i1 = new JMenuItem(localeDelegate.getMessage("nuclos.entityrelation.editor.14","Symbol l\u00f6schen"));
		i1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				mxGraphModel model =  (mxGraphModel)graphComponent.getGraph().getModel();
				int iEdge = cell.getEdgeCount();
				for(int i = 0; i < iEdge; i++){
					mxCell cellRelation = (mxCell)cell.getEdgeAt(0);
					model.remove(cellRelation);
				}
				model.remove(cell);				
				fireChangeListenEvent();
			}
		});
		
		if(!newCell)
			pop.add(i1);
		
		if(cell.getStyle() == null || !(cell.getStyle().indexOf(ENTITYSTYLE) >= 0)) {
			return pop;
		}
		
		
		
		JMenuItem iWizard = new JMenuItem(localeDelegate.getMessage("nuclos.entityrelation.editor.15","Wizard \u00f6ffnen"));
		iWizard.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(cell.getValue() != null && cell.getValue() instanceof EntityMeta) {
					UID sValue = ((EntityMeta)cell.getValue()).getUID();
					if(sValue.getString().length() > 0) {
						try {
							final EntityMeta vo = MetaProvider.getInstance().getEntity(sValue);
							new ShowNuclosWizard.NuclosWizardEditRunnable(false, mf.getHomePane(), vo).run();
						}
						catch(Exception e1) {
							// neue Entity
							LOG.info("actionPerformed: " + e1 + " (new entity?)");
						}			
					}
				}
			}
		});
		
		if(!newCell) {
			//pop.addSeparator();
			pop.add(iWizard);			
		}
		else {			
			JMenuItem iNew = new JMenuItem(localeDelegate.getMessage("nuclos.entityrelation.editor.16","neue Entit\u00e4t"));
			iNew.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					if(cell.getValue() != null && cell.getValue() instanceof EntityMeta) {
						final EntityMeta voTMP = (EntityMeta)cell.getValue();
						final EntityMeta vo = MetaProvider.getInstance().getEntity(voTMP.getUID());
						new ShowNuclosWizard.NuclosWizardEditRunnable(false, mf.getHomePane(), vo).run();
					}
					else {
						cell.setValue(localeDelegate.getMessage("nuclos.entityrelation.editor.16","neue Entit\u00e4t"));
						mxGraph graph = graphComponent.getGraph();
						graph.refresh();
					}
				}
			});			
			pop.add(iNew);
		}
		//pop.addSeparator();
		
		Collection<EntityMeta<?>> colMetaVO = MetaProvider.getInstance().getAllEntities();
		
		
		List<EntityMeta> lst = new ArrayList<EntityMeta>(colMetaVO);
		
		Collections.sort(lst, new Comparator<EntityMeta>() {

			@Override
			public int compare(EntityMeta o1, EntityMeta o2) {
				return o1.getUID().compareTo(o2.getUID());
			}
			
			
		});
		
		for(final EntityMeta vo : lst) {
			if(E.isNuclosEntity(vo.getUID()))
				continue;

			JCheckBoxMenuItem menu = new JCheckBoxMenuItem(MetaProvider.getInstance().getRepresentativeEntityName(vo.getUID()));
			menu.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					final JMenuItem item = (JMenuItem)e.getSource();
					SwingUtilities.invokeLater(new Runnable() {
						
						@Override
						public void run() {
							cell.setValue(vo);							
							graphComponent.repaint();
							fireChangeListenEvent();
						}
					});
					
				}
			});
			if(cell.getValue() != null && cell.getValue() instanceof EntityMeta) {
				EntityMeta sValue = (EntityMeta)cell.getValue();
				if(vo.getUID().equals(sValue.getUID())) {
					menu.setSelected(true);
				}
			}
			//pop.add(menu);
		}
		
		
	
		return pop;	
		
	}

	public CollectableComponentsProvider newCollectableComponentsProvider() {
		return new DefaultCollectableComponentsProvider(clcttfName, clcttfDescription);
	}
	
	public void addChangeListener(ChangeListener cl) {
		this.lstChangeListener.add(cl);
	}
	
	public void removeChangeListener(ChangeListener cl) {
		this.lstChangeListener.remove(cl);
	}
	
	public void fireChangeListenEvent() {
		for(ChangeListener cl : lstChangeListener) {
			cl.stateChanged(new ChangeEvent(this));
		}
	}
	
	private int getBestYPoint() {
		int y = 10;

		mxGraph graph = graphComponent.getGraph();
		mxCell root = (mxCell)graph.getModel().getRoot();
		int rootChildCount = root.getChildCount();		
		if(rootChildCount == 1) {
			mxCell cellContainer = (mxCell)root.getChildAt(0);
			int childCount = cellContainer.getChildCount();
			if(childCount == 0) {
				return 10;
			}
				
			for(int i = 0; i < childCount; i++) {
				mxCell cellTmp = (mxCell)cellContainer.getChildAt(i);
				if(cellTmp.getValue() instanceof EntityMeta) {
					if(cellTmp.getGeometry().getY() >= y) {
						y += 100;
					}
				}
			}
		}
		
		return y;
	}
	
	
	public void showDataModel(List<EntityMeta<?>> lstEntites, boolean blnNew) {
		
		mxCell cellRoot = (mxCell)graphComponent.getGraph().getModel().getRoot();
		mxCell cellContainer = (mxCell)cellRoot.getChildAt(0);
		
		
		int x = 10;
		int y = getBestYPoint();
		int index = 0;		
		
		int maxInARow = 8;		
		
		for(EntityMeta voMeta : lstEntites) {
			
			mxGeometry mxgeo = new mxGeometry(x, y, 100, 80);
			
			mxCell child = new mxCell(voMeta, mxgeo, ENTITYSTYLE);
			child.setVertex(true);
			graphComponent.getGraph().getModel().add(cellContainer, child, index++);
			x += 150;
			if(index % maxInARow == 0){
				y += 100;
				x = 10;
			}
		}
		if(!blnNew)
			lstEntites.addAll(getEntitiesInModel());
		
		for(EntityMeta voMeta : lstEntites) {
			if(E.isNuclosEntity(voMeta.getUID()))
				continue;
			
			boolean relation = false;
			UID sForeign = null;
			FieldMeta voForeignField = null;
			for(FieldMeta voField : MetaProvider.getInstance().getAllEntityFieldsByEntity(voMeta.getUID()).values()) {
				if(voField.getForeignEntity() != null) {
					boolean blnNextRelation = false;
					for(FieldMeta voFieldInModel : getEntityFieldsInModel()) {
						if(voFieldInModel.getUID().equals(voField.getUID())) {
							blnNextRelation = true;
							break;
						}								
					}
					if(blnNextRelation)
						continue;
					relation = true;
					sForeign = voField.getForeignEntity();
					voForeignField = voField;
					if(relation) {
						
						if(E.isNuclosEntity(voForeignField.getEntity()))
							continue;

						mxGeometry mxgeo = new mxGeometry(x, y, 100, 80);
						mxgeo.setSourcePoint(new mxPoint(100,100));
						mxgeo.setTargetPoint(new mxPoint(150,150));
						
						mxCell child = new mxCell(voForeignField, mxgeo, mxConstants.ARROW_OPEN);
						if(voField.getDbColumn().startsWith("INTID_")){
							child = new mxCell(voForeignField, mxgeo, mxConstants.ARROW_DIAMOND);
						}
						boolean targetFound = false;
						boolean sourceFound = false;
						for(EntityMeta vo : lstEntites){
							if(vo.getUID().equals(sForeign))
								targetFound = true;
							if(vo.getUID().equals(voMeta.getUID()))
								sourceFound = true;
							
						}
						if(targetFound && sourceFound){
							child.setTarget(getMasterDataMetaVOCell(sForeign));
							child.setSource(getMasterDataMetaVOCell(voMeta.getUID()));
							
							mxCell[] cells = {child};
							
							if(voField.getDbColumn().startsWith("INTID_")){
								mxUtils.setCellStyles(graphComponent.getGraph().getModel(), cells, mxConstants.STYLE_ENDARROW, mxConstants.ARROW_DIAMOND);
							}
							else {
								mxUtils.setCellStyles(graphComponent.getGraph().getModel(), cells, mxConstants.STYLE_ENDARROW, mxConstants.ARROW_OPEN);
							}
							
							mxUtils.setCellStyles(graphComponent.getGraph().getModel(), cells, mxConstants.STYLE_ENDSIZE, "12");
							mxUtils.setCellStyles(graphComponent.getGraph().getModel(), cells, mxConstants.STYLE_STROKECOLOR, SYMBOLCOLOR);
							mxUtils.setCellStyles(graphComponent.getGraph().getModel(), cells, mxConstants.STYLE_ELBOW , mxConstants.ELBOW_VERTICAL);
							mxUtils.setCellStyles(graphComponent.getGraph().getModel(), cells, EDGESTYLE , ELBOWCONNECTOR);
							child.setEdge(true);
							graphComponent.getGraph().getModel().add(cellContainer, child, index++);
							x += 120;
							y += 100;
						}
					}
				}
					
			}
			
		}		
		
		for(MasterDataVO<?> voGeneration : MasterDataCache.getInstance().get(E.GENERATION.getUID())) {
			final UID sSourceModule = (UID)voGeneration.getFieldValue(E.GENERATION.sourceModule.getUID());
			final UID sTargetModule = (UID)voGeneration.getFieldValue(E.GENERATION.targetModule.getUID());
			
			boolean targetFound = false;
			boolean sourceFound = false;
			for(EntityMeta<?> vo : lstEntites){
				if(vo.getUID().equals(sTargetModule))
					targetFound = true;
				if(vo.getUID().equals(sSourceModule))
					sourceFound = true;
				
			}
			if(targetFound && sourceFound){
				mxGeometry mxgeo = new mxGeometry(x, y, 100, 80);
				mxgeo.setSourcePoint(new mxPoint(100,100));
				mxgeo.setTargetPoint(new mxPoint(150,150));
				
				mxCell child = new mxCell("", mxgeo, mxConstants.ARROW_OPEN);
				child.setSource(getMasterDataMetaVOCell(sSourceModule));
				child.setTarget(getMasterDataMetaVOCell(sTargetModule));
				
				mxCell[] cells = {child};
				mxUtils.setCellStyles(graphComponent.getGraph().getModel(), cells, mxConstants.STYLE_ENDARROW, mxConstants.ARROW_OVAL);
				mxUtils.setCellStyles(graphComponent.getGraph().getModel(), cells, mxConstants.STYLE_ENDSIZE, "12");
				mxUtils.setCellStyles(graphComponent.getGraph().getModel(), cells, mxConstants.STYLE_STROKECOLOR, SYMBOLCOLOR);
				mxUtils.setCellStyles(graphComponent.getGraph().getModel(), cells, mxConstants.STYLE_ELBOW , mxConstants.ELBOW_VERTICAL);
				mxUtils.setCellStyles(graphComponent.getGraph().getModel(), cells, EDGESTYLE , ELBOWCONNECTOR);
				child.setEdge(true);
				graphComponent.getGraph().getModel().add(cellContainer, child, index++);
				x += 120;
				y += 100;
			}
			
		}
		
		this.fireChangeListenEvent();
		
	}
	
	public void removeNotExistentEntitiesFromModel() {
		Collection<EntityMeta<?>> lstEntities = MetaProvider.getInstance().getAllEntities();
		for(EntityMeta voInModel : getEntitiesInModel()) {
			if(!lstEntities.contains(voInModel)) {
				mxCell cellRemove = getCellByEntityName(voInModel.getUID());
				int iEdge = cellRemove.getEdgeCount();
				for(int i = 0; i < iEdge; i++){
					mxCell cellRelation = (mxCell)cellRemove.getEdgeAt(i);
					getGraphModel().remove(cellRelation);
				}
				getGraphModel().remove(cellRemove);				
				fireChangeListenEvent();
			}
				
		}

	}
	
	public void loadReferenz() {
		mxGraph graph = graphComponent.getGraph();
		mxCell root = (mxCell)graph.getModel().getRoot();
		int rootChildCount = root.getChildCount();
		if(rootChildCount == 1) {
			mxCell cellContainer = (mxCell)root.getChildAt(0);
			int childCount = cellContainer.getChildCount();
			for(int i = 0; i < childCount; i++) {
				mxCell cell = (mxCell)cellContainer.getChildAt(i);
				if(cell.getValue() instanceof FieldMeta) {
					FieldMeta voField = (FieldMeta)cell.getValue();
					if(cell.getSource() == null) {
						UID sourceId = voField.getEntity();
						EntityMeta voSource = MetaProvider.getInstance().getEntity(sourceId);
						mxCell cellSource = getCellByEntityName(voSource.getUID());
						cell.setSource(cellSource);
					}
					if(cell.getTarget() == null) {
						EntityMeta voTarget = MetaProvider.getInstance().getEntity(voField.getForeignEntity());
						mxCell cellTarget = getCellByEntityName(voTarget.getUID());
						cell.setTarget(cellTarget);
					}
					
					if(cell.getSource().getValue() instanceof EntityMeta) {
						EntityMeta voSource = (EntityMeta)cell.getSource().getValue();
						try {
							voField = MetaDataDelegate.getInstance().getEntityField(voField.getUID());
							cell.setValue(voField);
						}
						catch(Exception e) {
							LOG.info("loadReferenz: " + e);
						}
					}
				}
				
			}			
		}
		
		graph.refresh();
		
	}
	
	private mxCell getCellByEntityName(UID name) {
		mxCell cell = null;
		
		mxGraph graph = graphComponent.getGraph();
		mxCell root = (mxCell)graph.getModel().getRoot();
		int rootChildCount = root.getChildCount();
		if(rootChildCount == 1) {
			mxCell cellContainer = (mxCell)root.getChildAt(0);
			int childCount = cellContainer.getChildCount();
			for(int i = 0; i < childCount; i++) {
				mxCell cellTmp = (mxCell)cellContainer.getChildAt(i);
				if(cellTmp.getValue() instanceof EntityMeta) {
					EntityMeta vo = (EntityMeta)cellTmp.getValue();
					if(vo.getUID().equals(name)) {
						cell = cellTmp;
					}						
				}
			}
		}
		
		return cell;
	}
	
	protected mxCell getMasterDataMetaVOCell(UID sEntity) {
		mxCell cellRoot = (mxCell)graphComponent.getGraph().getModel().getRoot();
		mxCell cellContainer = (mxCell)cellRoot.getChildAt(0);
		int count = cellContainer.getChildCount();
		for(int i = 0; i < count; i++) {
			mxCell cell = (mxCell)cellContainer.getChildAt(i);
			if(cell.getValue() != null && cell.getValue() instanceof EntityMeta) {
				EntityMeta voMeta = (EntityMeta)cell.getValue();
				if(voMeta.getUID().equals(sEntity)) {
					return cell;
				}
			}
		}
		
		return null;		
	}
	
	private void editSubformRelation(final mxCell cell) {
        if(cell.getValue() != null && (cell.getValue() instanceof String || cell.getValue() instanceof FieldMeta)) {
			mxCell target = (mxCell)cell.getTarget();
			mxCell source = (mxCell)cell.getSource();
			EntityMeta<?> voSource = (EntityMeta)source.getValue();
			EntityMeta voTarget = (EntityMeta)target.getValue();
			String sFieldName = null;
			boolean blnNotSet = true;
			final SpringLocaleDelegate localeDelegate = getSpringLocaleDelegate();
			while(blnNotSet) {
				if(cell.getValue() instanceof FieldMeta) {
					String sDefault = ((FieldMeta)cell.getValue()).getFieldName();
					sFieldName = JOptionPane.showInputDialog(EntityRelationshipModelEditPanel.this, 
						localeDelegate.getMessage("nuclos.entityrelation.editor.17", "Bitte geben Sie den Namen des Feldes an!"), sDefault);
				}
				else 
					sFieldName = JOptionPane.showInputDialog(EntityRelationshipModelEditPanel.this, localeDelegate.getMessage(
							"nuclos.entityrelation.editor.17", "Bitte geben Sie den Namen des Feldes an!"));
				if(sFieldName == null || sFieldName.length() < 1) {
					if(cell.getValue() instanceof String)
						getGraphModel().remove(cell);					
					return;
				}
				else if(sFieldName != null) {
					blnNotSet = false;
				}
				
				for(FieldMeta voField : MetaDataDelegate.getInstance().getAllEntityFieldsByEntity(voSource.getUID()).values()){
					if(voField.getFieldName().equals(sFieldName)){
						JOptionPane.showMessageDialog(EntityRelationshipModelEditPanel.this, localeDelegate.getMessage(
								"nuclos.entityrelation.editor.18", "Der Feldname ist schon vorhanden"));
						blnNotSet = true;
						break;
					}
				}
			}
			
			NucletFieldMeta vo = null;
			if(cell.getValue() instanceof NucletFieldMeta) {
				vo = (NucletFieldMeta)cell.getValue();
				vo.flagUpdate();
			}
			else {
				vo = new NucletFieldMeta();
				vo.setModifiable(true);
			   vo.setLogBookTracking(false);
			   vo.setReadonly(false);
			   vo.setShowMnemonic(true);
			   vo.setInsertable(true);
			   vo.setSearchable(true);
			   vo.setNullable(false);
			   vo.setUnique(true);
			   vo.setDataType("java.lang.String");
		   }
		   
		   List<TranslationVO> lstTranslation = new ArrayList<TranslationVO>();
		   for(LocaleInfo voLocale : LocaleDelegate.getInstance().getAllLocales(false)) {
				String sLocaleLabel = voLocale.getLanguage(); 
				UID iLocaleID = voLocale.getLocale();  
				String sCountry = voLocale.getTitle();
				Map<String, String> map = new HashMap<String, String>();
				
				TranslationVO translation = new TranslationVO(voLocale, map);
				for(String sLabel : labels) {									
					translation.getLabels().put(sLabel, sFieldName);
				}
				lstTranslation.add(translation);
			}
		   
		   vo.setForeignEntity(voTarget.getUID());		   
		   vo.setFieldName(sFieldName);
		   if(cell.getValue() instanceof String) {
			   
			   vo.setDbColumn("INTID_" + sFieldName);
		   }   
			   
			cell.setValue(vo);
			
			List<FieldMetaTransport> toList = new ArrayList<FieldMetaTransport>();
			
			FieldMetaTransport toField = new FieldMetaTransport();
			toField.setEntityFieldMeta(vo);
			toField.setTranslation(lstTranslation);
			toList.add(toField);
			
			MetaDataDelegate.getInstance().modifyEntityMetaData(voSource.getUID(), toList);
			EntityRelationshipModelEditPanel.this.loadReferenz();
		}
    }

	
	private void editMasterdataRelation(mxCell cell) {
		NucletFieldMeta voField = null;
        RelationAttributePanel panel = new RelationAttributePanel(RelationAttributePanel.TYPE_ENTITY);
        UID sSource = null;
        UID sTarget = null;
        EntityMeta<?> voSourceModify = null;
        if(cell.getValue() != null && cell.getValue() instanceof NucletFieldMeta) {
        	voField = (NucletFieldMeta)cell.getValue();
        	
        	EntityMeta voSource = (EntityMeta)cell.getSource().getValue();
        	voSourceModify = voSource;
        	EntityMeta voTarget = (EntityMeta)cell.getTarget().getValue();
        	sSource = voSource.getUID();
        	sTarget = voTarget.getUID();
        	EntityMeta voForeign = MetaProvider.getInstance().getEntity(voSource.getUID());
        	EntityMeta voEntity = MetaProvider.getInstance().getEntity(voTarget.getUID());
         	panel.setEntity(voEntity);
         	panel.setEntitySource(voForeign);
         	panel.setEntityFields(MetaDataDelegate.getInstance().getAllEntityFieldsByEntity(voForeign.getUID()).values());

         	if(voField.getUID() != null) {
         		voField = (NucletFieldMeta<?>)MetaProvider.getInstance().getEntityField(voField.getUID());
         		List<TranslationVO> lstTranslation = new ArrayList<TranslationVO>();
        		for(LocaleInfo lInfo : LocaleDelegate.getInstance().getAllLocales(false)) {
        			Map<String, String> mp = new HashMap<String, String>();
        			
        			mp.put(TranslationVO.LABELS_FIELD[0], LocaleDelegate.getInstance().getResourceByStringId(lInfo, voField.getLocaleResourceIdForLabel()));
        			mp.put(TranslationVO.LABELS_FIELD[1], LocaleDelegate.getInstance().getResourceByStringId(lInfo, voField.getLocaleResourceIdForDescription()));
        			TranslationVO voTrans = new TranslationVO(lInfo, mp);
        			lstTranslation.add(voTrans);
        		}	
        		panel.setTranslation(lstTranslation);
        		panel.setFieldValues(voField);
         	}
         	else {
         		MyGraphModel model = (MyGraphModel)graphComponent.getGraph().getModel();
         		panel.setFieldValues(voField);	
        		panel.setTranslationAndMore(model.getTranslation().get(voField));
         	}
        }
        else if(cell.getValue() != null && cell.getValue() instanceof String) {
        	EntityMeta voSource = (EntityMeta)cell.getSource().getValue();
        	EntityMeta voTarget = (EntityMeta)cell.getTarget().getValue();
        	sSource = voSource.getUID();
        	sTarget = voTarget.getUID();
        	
        	EntityMeta voForeign = MetaProvider.getInstance().getEntity(voSource.getUID());
        	EntityMeta voEntity = MetaProvider.getInstance().getEntity(voTarget.getUID());
        	voSourceModify = voForeign;
        	panel.setEntity(voEntity);
         	panel.setEntitySource(voForeign);
         	panel.setEntityFields(MetaDataDelegate.getInstance().getAllEntityFieldsByEntity(voSource.getUID()).values());
        }
        
        double cellsDialog [][] = {{5, TableLayout.PREFERRED, 5}, {5, TableLayout.PREFERRED,5}};	
        JDialog dia = new JDialog(mf);
        dia.setLayout(new TableLayout(cellsDialog));
        dia.setTitle("Verbindung von " + sSource +" zu "+ sTarget + " bearbeiten");
        dia.setLocationRelativeTo(EntityRelationshipModelEditPanel.this);
        dia.add(panel, "1,1");
        dia.setModal(true);
        panel.setDialog(dia);
        dia.pack();
        dia.setVisible(true);	

        if(panel.getState() == 1) {
        	NucletFieldMeta vo = panel.getField();
        	cell.setValue(vo);
        	
        	EntityRelationshipModelEditPanel.this.fireChangeListenEvent();
        	
        	List<FieldMetaTransport> toList = new ArrayList<FieldMetaTransport>();
        	
        	FieldMetaTransport toField = new FieldMetaTransport();
        	toField.setEntityFieldMeta(vo);
        	toField.setTranslation(panel.getTranslation().getRows());
        	toList.add(toField);
        	
        	MetaDataDelegate.getInstance().modifyEntityMetaData(voSourceModify.getUID(), toList);
        	
        	MyGraphModel model = (MyGraphModel)graphComponent.getGraph().getModel();
        	model.getTranslation().put(vo,panel.getTranslation().getRows());
        	getGraphComponent().refresh();
        	
        	loadReferenz();
        	
        }
        else {
        	if(cell.getValue() instanceof String)
        		getGraphModel().remove(cell);					
        }
    }	
	
	class MyCellEditor extends mxCellEditor {

		public MyCellEditor(mxGraphComponent graphComponent) {
			super(graphComponent);
		}

		@Override
		public Component getEditor() {
			return new JLabel();
		}

		@Override
		public void startEditing(Object cell, EventObject trigger) {
			stopEditing(true);
		}
	}

	
}	// class StateModelEditPanel