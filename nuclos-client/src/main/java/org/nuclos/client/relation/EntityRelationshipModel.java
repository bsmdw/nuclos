//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.relation;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.AbstractCollectableBean;
import org.nuclos.common.collect.collectable.AbstractCollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.DefaultCollectableEntityField;
import org.nuclos.common.valueobject.EntityRelationshipModelVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Makes a StateModelVO look like a Collectable.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
@Deprecated
public class EntityRelationshipModel extends AbstractCollectableBean<EntityRelationshipModelVO,UID> {

	public static final UID ENTITYRELATION_ENTITY = E.ENTITYRELATION.getUID();

	public static final UID FIELDNAME_NAME = E.ENTITYRELATION.name.getUID();
	public static final UID FIELDNAME_DESCRIPTION = E.ENTITYRELATION.description.getUID();

	private final boolean bComplete;
	
	String xmlModel;
	
	static EntityRelationshipModelVO vo = new EntityRelationshipModelVO("", "");

	public static class Entity extends AbstractCollectableEntity {

		private Entity() {
			super(ENTITYRELATION_ENTITY, "Relationen erstellen");
			
			this.addCollectableEntityField(new DefaultCollectableEntityField(FIELDNAME_NAME, String.class, 
					getSpringLocaleDelegate().getMessage("CollectableStateModel.3","Name"),
					getSpringLocaleDelegate().getMessage("CollectableRelationModel.4","Name des Relationenmodells"), 
					255, null, false, CollectableField.TYPE_VALUEFIELD, null, null, ENTITYRELATION_ENTITY, null));
			this.addCollectableEntityField(new DefaultCollectableEntityField(FIELDNAME_DESCRIPTION, String.class,
					getSpringLocaleDelegate().getMessage("CollectableStateModel.1","Beschreibung"), 
					getSpringLocaleDelegate().getMessage("CollectableRelationModel.2","Beschreibung des Relationenmodells"), 
					4000, null, true, CollectableField.TYPE_VALUEFIELD, null, null, ENTITYRELATION_ENTITY, null));
		}

	}	// inner class Entity

	public static final CollectableEntity clcte = new Entity();

	public EntityRelationshipModel() {		
		this(vo, true);
	}
	
	public EntityRelationshipModel(EntityRelationshipModelVO voMaster) {		
		this(voMaster, true);		
	}

	private EntityRelationshipModel(EntityRelationshipModelVO vo, boolean bComplete) {
		super(vo);
		this.bComplete = bComplete;
	}

	@Override
	public boolean isComplete() {
		return this.bComplete;
	}


	@Override
	public UID getId() {
		return getBean().getId();
	}

	@Override
	public void removeId() {
		getBean().setPrimaryKey(null);
	}

	@Override
	protected CollectableEntity getCollectableEntity() {
		return clcte;
	}

	/*
	@Override
	public String getIdentifierLabel() {
		return this.getBean().getName();
	}
	 */

	@Override
	public int getVersion() {
		return getBean().getVersion();
	}

	@Override
	public Object getValue(UID fieldUid) {
		return getField(fieldUid).getValue();
	}
	
	public String getXMLModel() {
		return xmlModel;
	}
	
	public void setXMLModel(String xml) {
		this.xmlModel = xml;
	}
	
	public MasterDataVO<UID> getMasterDataVO() {	
		MasterDataVO<UID> mdvo = new MasterDataVO<UID>(getCollectableEntity().getUID(), getId(), 
				getBean().getCreatedAt(), getBean().getCreatedBy(),
			this.getBean().getChangedAt(), this.getBean().getChangedBy(), this.getVersion());
		mdvo.setFieldValue(FIELDNAME_NAME, this.getValue(FIELDNAME_NAME));
		mdvo.setFieldValue(FIELDNAME_DESCRIPTION, this.getValue(FIELDNAME_DESCRIPTION));
		
		return mdvo;
	}

	@Override
	public UID getEntityUID() {
		return clcte.getUID();
	}

	@Override
	public UID getPrimaryKey() {
		return getBean().getPrimaryKey();
	}	

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("entity=").append(getCollectableEntity());
		result.append(",vo=").append(getBean());
		result.append(",mdVo=").append(getMasterDataVO());
		result.append(",id=").append(getId());
		//Note: Swing (since J1.7 more than before) calls "toString()" often for whatever reason. getIdentifierLabel() shouldn't
		//be called therefore as it calls LocaleDelegate and thus uses lot of CPU Power.
//				result.append(",label=").append(getIdentifierLabel());
		result.append(",complete=").append(isComplete());
		result.append("]");
		return result.toString();
	}

}	// class CollectableStateModel
