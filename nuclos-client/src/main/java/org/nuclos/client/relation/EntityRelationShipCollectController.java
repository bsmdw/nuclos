//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.relation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JDialog;

import org.nuclos.client.common.LafParameterProvider;
import org.nuclos.client.common.NuclosCollectController;
import org.nuclos.client.common.NuclosResultPanel;
import org.nuclos.client.common.controller.NuclosCollectControllerCommonState;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.mainframe.IconResolver;
import org.nuclos.client.main.mainframe.IconResolverConstants;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterData;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.masterdata.MetaDataDelegate;
import org.nuclos.client.ui.collect.CollectPanel;
import org.nuclos.client.ui.collect.DefaultEditView;
import org.nuclos.client.ui.collect.component.CollectableTextField;
import org.nuclos.client.ui.collect.result.ResultPanel;
import org.nuclos.client.ui.layoutml.LayoutRoot;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.LafParameter;
import org.nuclos.common.UID;
import org.nuclos.common.collect.ControllerPresentation;
import org.nuclos.common.collect.ToolBarConfiguration;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.masterdata.CollectableMasterDataEntity;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.w3c.dom.Document;

import com.mxgraph.io.mxCodec;
import com.mxgraph.util.mxUtils;
import com.mxgraph.util.mxXmlUtils;

import info.clearthought.layout.TableLayout;

/**
 * Controller for collecting state models.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
@Deprecated
public class EntityRelationShipCollectController extends NuclosCollectController<UID,EntityRelationshipModel> {
	
	final CollectableTextField clcttfName = new CollectableTextField(
		EntityRelationshipModel.clcte.getEntityField(EntityRelationshipModel.FIELDNAME_NAME));

	final CollectableTextField clcttfDescription = new CollectableTextField(
		EntityRelationshipModel.clcte.getEntityField(EntityRelationshipModel.FIELDNAME_DESCRIPTION));

	private final CollectPanel<UID,EntityRelationshipModel> pnlCollect
		= new EntityRelationshipCollectPanel(EntityRelationshipModel.clcte.getUID(), false, false);
	
	private final EntityRelationshipModelEditPanel pnlEdit;
	
	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton} 
	 * to get an instance.
	 * 
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	public EntityRelationShipCollectController(MainFrame mf, MainFrameTab tabIfAny, NuclosCollectControllerCommonState state) {
		super(EntityRelationshipModel.clcte, tabIfAny, state);
		
		this.initialize(this.pnlCollect);
		
		//this.mf = mf;
		
		getTab().setLayeredComponent(pnlCollect);
		
		pnlEdit = new EntityRelationshipModelEditPanel(mf);
		
		this.getDetailsPanel().setEditView(DefaultEditView.newDetailsEditView(pnlEdit, pnlEdit.newCollectableComponentsProvider()));
		
		getTab().setTitle(getSpringLocaleDelegate().getMessage("nuclos.entityrelation.controller.2","Relationen Editor"));
		//final JPanel pnlCustomToolBarAreaDetails = new JPanel();		
		//pnlCustomToolBarAreaDetails.setLayout(new BorderLayout());
		//pnlCustomToolBarAreaDetails.setPreferredSize(new Dimension(25,25));
		JButton bt = new JButton(getSpringLocaleDelegate().getMessage(
				"nuclos.entityrelation.controller.3", "Entit\u00e4ten ausw\u00e4hlen"));
		//JToolBar bar = UIUtils.createNonFloatableToolBar();
		//bar.setOpaque(false);
		//bar.add(bt);
		//bar.add(Box.createHorizontalGlue());
		
		bt.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				showEntityDialog();				
			}
		});
		
		//pnlCustomToolBarAreaDetails.add(bar, BorderLayout.CENTER);		
	
		//this.getDetailsPanel().setCustomToolBarArea(pnlCustomToolBarAreaDetails);
		this.getDetailsPanel().addToolBarComponent(bt);
	}
	
	@Override
	protected void cmdEnterNewMode() {
		super.cmdEnterNewMode();
		pnlEdit.getGraphModel().clear();
		showEntityDialog();
	}
	
	@Override
	protected LayoutRoot<UID> getLayoutRoot() {
		return null;
	}
	
	private void showEntityDialog() {
		
		clcttfName.setField(new CollectableValueField(null));
		clcttfDescription.setField(new CollectableValueField(null));
		List<EntityMeta<?>> lstInModel = pnlEdit.getEntitiesInModel();
		
		double cells [][] = {{5, TableLayout.PREFERRED, 5}, {5, TableLayout.PREFERRED,5}};	
		
		JDialog dia = new JDialog(Main.getInstance().getMainFrame(), getSpringLocaleDelegate().getMessage(
				"nuclos.entityrelation.controller.1", "Entit\u00e4ten f\u00fcr Datenmodell ausw\u00e4hlen"), true);
		EntityChoicePanel panel = new EntityChoicePanel(dia, lstInModel);
		dia.setLayout(new TableLayout(cells));
		dia.add(panel, "1,1");
		dia.pack();
		dia.setLocationRelativeTo(this.getTab());
		dia.setVisible(true);
	
		List<EntityMeta<?>> lstEntites = panel.getSelectedEntites();
		pnlEdit.showDataModel(lstEntites, true);
	}

	/**
	 * @deprecated Move to DetailsController hierarchy and make protected again.
	 */
	@Override
	public void addAdditionalChangeListenersForDetails() {
		this.pnlEdit.addChangeListener(this.changelistenerDetailsChanged);		
	}

	/**
	 * @deprecated Move to DetailsController hierarchy and make protected again.
	 */
	@Override
	public void removeAdditionalChangeListenersForDetails() {
		this.pnlEdit.removeChangeListener(this.changelistenerDetailsChanged);
	}


	@Override
	protected void deleteCollectable(EntityRelationshipModel clct, final Map<String, Serializable> applyMultiEditContext)	throws CommonBusinessException {
		
		MasterDataDelegate.getInstance().remove(E.ENTITYRELATION.getUID(), clct.getMasterDataVO(), getCustomUsage());
		clcttfName.setField(new CollectableValueField(null));
		clcttfDescription.setField(new CollectableValueField(null));
		pnlEdit.getGraphModel().clear();
		pnlEdit.clearModel();
	
	}

	@Override
	public EntityRelationshipModel findCollectableById(UID entityUid, UID oId, final int dependantsDepth) throws CommonBusinessException {
		final MasterDataVO<UID> vo = MasterDataDelegate.getInstance().get(E.ENTITYRELATION.getUID(), oId);
		
		final EntityRelationshipModel model = new EntityRelationshipModel(MetaDataDelegate.getInstance().getEntityRelationshipModelVO(vo));
		
		byte[] b = (byte[])vo.getFieldValue(E.ENTITYRELATION.layout.getUID());
		String xml = new String(b);
		model.setXMLModel(xml);
		
		return model;
	}

	@Override
	protected EntityRelationshipModel findCollectableByIdWithoutDependants(UID entityUid, UID oId) throws CommonBusinessException {
		return findCollectableById(entityUid, oId, 0);
	}

	@Override
	protected String getEntityLabel() {
		return getSpringLocaleDelegate().getText("nuclos.entity.entityrelation.label", "Relationen Editor") ;
	}

	@Override
	protected EntityRelationshipModel insertCollectable(EntityRelationshipModel clctNew) throws CommonBusinessException {
				
		EntityMeta<UID> metaVO = (EntityMeta<UID>)MasterDataDelegate.getInstance().getMetaData(E.ENTITYRELATION.getUID());
		CollectableMasterDataEntity masterDataEntity = new CollectableMasterDataEntity(metaVO);
		CollectableMasterData<UID> masterData = new CollectableMasterData<UID>(masterDataEntity, new MasterDataVO(masterDataEntity.getMeta(), false));
		MasterDataVO<UID> mdvo = masterData.getMasterDataCVO();
		mdvo.setFieldValue(E.ENTITYRELATION.name.getUID(), clctNew.getField(E.ENTITYRELATION.name.getUID()).getValue());
		mdvo.setFieldValue(E.ENTITYRELATION.description.getUID(), clctNew.getField(E.ENTITYRELATION.description.getUID()).getValue());
		
		String xml = getXML();
		
		mdvo.setFieldValue(E.ENTITYRELATION.layout.getUID(), xml.getBytes());		
		
		MasterDataVO<UID> vo = MasterDataDelegate.getInstance().create(E.ENTITYRELATION.getUID(), mdvo, null, getCustomUsage());
		
		pnlEdit.clearModel();
		
				
		EntityRelationshipModel model = new EntityRelationshipModel(MetaDataDelegate.getInstance().getEntityRelationshipModelVO(vo));
		model.setXMLModel(xml);
		
		return model;
	}
	
	@Override
	public EntityRelationshipModel newCollectable() {
		clcttfName.setField(new CollectableValueField(null));
		clcttfDescription.setField(new CollectableValueField(null));
		EntityRelationshipModel cltl = new EntityRelationshipModel();			
		return cltl;		
	}
	
	private String getXML() {
		mxCodec codec = new mxCodec();
		String xml = mxUtils.getXml(codec.encode(pnlEdit.getGraphComponent().getGraph().getModel()));
		return xml;
	}

	@Override
	protected EntityRelationshipModel updateCollectable(EntityRelationshipModel clct, Object oAdditionalData, final Map<String, Serializable> applyMultiEditContext, boolean isCollectiveProcessing) throws CommonBusinessException {
		MasterDataVO<UID> mdvo = clct.getMasterDataVO();		
		String xml = getXML();
		mdvo.setFieldValue(E.ENTITYRELATION.layout.getUID(), xml.getBytes());		
		UID iId = MasterDataDelegate.getInstance().update(E.ENTITYRELATION.getUID(), mdvo, null, getCustomUsage(), false);
		pnlEdit.clearModel();
		EntityRelationshipModel model = findCollectableById(E.ENTITYRELATION.getUID(), iId, -1);
		return model;
	}
	
	@Override
	protected void unsafeFillDetailsPanel(EntityRelationshipModel clct)	throws CommonBusinessException {
		super.unsafeFillDetailsPanel(clct);
		
		if(clct.getXMLModel() != null) {
			Document document = mxXmlUtils.parseXml(clct.getXMLModel());
	
			mxCodec codec = new mxCodec(document);
			codec.decode(document.getDocumentElement(), pnlEdit.getGraphComponent().getGraph().getModel());
			pnlEdit.fireChangeListenEvent();
			pnlEdit.loadReferenz();
			pnlEdit.removeNotExistentEntitiesFromModel();
		}
		
	}

	@Override
	protected EntityRelationshipModel newCollectableWithDefaultValues(boolean forInsert) {
		return new EntityRelationshipModel();
	}

	
	private class EntityRelationshipCollectPanel extends CollectPanel<UID,EntityRelationshipModel> {

		EntityRelationshipCollectPanel(UID entityId, boolean bSearchPanelAvailable, boolean bShowSearch) {
			super(entityId, bSearchPanelAvailable, bShowSearch, LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Details_Presentation, entityId), ControllerPresentation.DEFAULT);
		}

		@Override
		public ResultPanel<UID,EntityRelationshipModel> newResultPanel(UID entityUid) {
			return new NuclosResultPanel<UID,EntityRelationshipModel>(entityUid, ControllerPresentation.DEFAULT);
		}
	}
	
	@Override
	protected boolean isDeleteAllowed(EntityRelationshipModel clct) {
		return true;
	}

	@Override
	protected boolean isDeleteSelectedCollectableAllowed() {
		return true;
	}

	@Override
	protected boolean isCloneAllowed() {
		return false;
	}

	@Override
	protected boolean isNewAllowed() {
		return true;
	}
	
	@Override
	public Pair<IconResolver, String> getIconAndResolver() {
		return new Pair<IconResolver, String>(IconResolverConstants.NUCLOS_RESOURCE_ICON_RESOLVER, "org.nuclos.client.resource.icon.glyphish-blue.55-network.png");
	}
	
	@Override
	public void init() {
		super.init();
		
		this.setupResultToolBar();
		this.setupDetailsToolBar();
		
		ToolBarConfiguration configuration = 
				LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Tool_Bar_Result, getEntityUid());
		this.getResultPanel().setToolBarConfiguration(configuration);
		configuration = 
				LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Tool_Bar_Result, getEntityUid());
		this.getResultPanel().setToolBarConfiguration(configuration);
		configuration = 
				LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Tool_Bar_Search, getEntityUid());
		this.getSearchPanel().setToolBarConfiguration(configuration);
	}
	
	@Override
	protected void initialize(CollectPanel<UID, EntityRelationshipModel> pnlCollect) {
		super.initialize(pnlCollect);
		revalidateAdditionalSearchField();
	}
	
}	// class StateModelCollectController