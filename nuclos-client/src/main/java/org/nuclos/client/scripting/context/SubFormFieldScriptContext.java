//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.scripting.context;

import org.nuclos.client.common.AbstractDetailsSubFormController;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.NucletConstants;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.entityobject.CollectableEOEntityField;
import org.nuclos.common2.LangUtils;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

public class SubFormFieldScriptContext<PK> extends SubformControllerScriptContext<PK> {

	private final CollectableEntityField field;

	public SubFormFieldScriptContext(CollectController<?,?> parent, AbstractDetailsSubFormController<?,?> parentSfc, AbstractDetailsSubFormController<?,?> sfc,
			Collectable<?> c, CollectableEntityField field) {
		super(parent, parentSfc, sfc, c);
		this.field = field;
	}

	public String getField() {
		EntityMeta<PK> md = (EntityMeta<PK>) MetaProvider.getInstance().getEntity(field.getEntityUID());
		String sLocalIdentifier = NucletConstants.DEFAULT_LOCALIDENTIFIER;
		if (md.getNuclet() != null) {
			for (MasterDataVO<?> mdvo : MasterDataCache.getInstance().get(E.NUCLET.getUID())) {
				if(mdvo.getPrimaryKey().equals(md.getNuclet())){
					sLocalIdentifier = mdvo.getFieldValue(E.NUCLET.localidentifier);
					break;
				}
			}
		}
		return "#{" + sLocalIdentifier + "." + md.getEntityName() + "." + ((CollectableEOEntityField)field).getMeta().getFieldName() + "}";
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj instanceof SubFormFieldScriptContext) {
			SubFormFieldScriptContext that = (SubFormFieldScriptContext)obj;
			return LangUtils.equal(field, that.field)
					&& super.equals(that);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return LangUtils.hash(field, super.hashCode());
	}

	@Override
	public String toString() {
		return "Field:" + getField()+
				"\n" + super.toString();
	}
}
