//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.scripting.expressions;

import java.text.MessageFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.nuclos.api.exception.InvalidExpressionException;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NucletConstants;
import org.nuclos.common.NuclosScript;
import org.nuclos.common.UID;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

public abstract class ExpressionParser {
	
	private static final Logger LOG = Logger.getLogger(ExpressionParser.class);
	
	private static final Pattern EXPRESSION_PATTERN = Pattern.compile("\\#\\{([^}]*)\\}");
	private static final Pattern FUNCTION_PATTERN = Pattern.compile("\\#FUNCTION\\{([^}]*)\\}");
	
	public static Object parse(String expression, ExpressionEvaluator eval) {
		try {
			final String[] parts = parseExpression(expression);
			
			final UID nuclet = getNucletByExpressionPart(parts[0]);
			final UID entity = getEntityByNucletAndExpressionPart(nuclet, parts[1]);
			
			if (parts.length == 2) {
				return eval.evaluate(new EntityExpression(nuclet, entity));
			}
			
			if (parts.length == 3 && "id".equals(parts[2])) {
				return eval.evaluate(new FieldPkExpression(nuclet, entity));
			}
			
			final UID field = getFieldByEntityAndExpressionPart(entity, parts[2]);;
			
			if (parts.length == 3) {
				return eval.evaluate(new FieldValueExpression(nuclet, entity, field));
			}
			if (parts.length == 4) {
				if ("value".equals(parts[3])) {
					return eval.evaluate(new FieldValueExpression(nuclet, entity, field));
				}
				else if ("id".equals(parts[3])) {
					return eval.evaluate(new FieldIdExpression(nuclet, entity, field));
				}
				else if ("context".equals(parts[3])) {
					return eval.evaluate(new FieldRefObjectExpression(nuclet, entity, field));
				}
			}	
		} catch (Exception e) {
			LOG.warn("caught invalid expression: " + e.getMessage());
		}
		
		throw new InvalidExpressionException(expression);
	}
	
	private static UID getNucletByExpressionPart(String expressionPart) {
		if (expressionPart.equalsIgnoreCase(NucletConstants.DEFAULT_LOCALIDENTIFIER))
			return null;
		
		for (MasterDataVO<?> mdvo : MasterDataCache.getInstance().get(E.NUCLET.getUID())) {
			if (StringUtils.equalsIgnoreCase((String)mdvo.getFieldValue(E.NUCLET.localidentifier.getUID()), expressionPart))
				return (UID)mdvo.getPrimaryKey();
		}
		throw new IllegalArgumentException("did not find localidentifier in nuclets: " + expressionPart);
	}
	private static UID getEntityByNucletAndExpressionPart(UID nuclet, String expressionPart) {
		for (MasterDataVO<?> mdvo : MasterDataCache.getInstance().get(E.ENTITY.getUID())) {
			if (LangUtils.equal(mdvo.getFieldUid(E.ENTITY.nuclet.getUID()), nuclet)) {
				if (StringUtils.equalsIgnoreCase((String)mdvo.getFieldValue(E.ENTITY.entity.getUID()), expressionPart))
					return (UID)mdvo.getPrimaryKey();
			}
		}
		throw new IllegalArgumentException("did not find entity for nuclet " +
				(nuclet != null ? nuclet.getStringifiedDefinition() : NucletConstants.DEFAULT_LOCALIDENTIFIER) + ": " + expressionPart);
	}
	private static UID getFieldByEntityAndExpressionPart(UID entity, String expressionPart) {
		for (FieldMeta<?> efMeta : MetaProvider.getInstance().getEntity(entity).getFields()) {
			if (StringUtils.equalsIgnoreCase(efMeta.getFieldName(), expressionPart)) {
				return efMeta.getUID();
			}
		}
		throw new IllegalArgumentException("did not find field in entity " + entity.getStringifiedDefinition() + ": " + expressionPart);
	}
	private static String[] parseExpression(String expression) {
		Matcher m = EXPRESSION_PATTERN.matcher(expression);
		if (m.matches()) {
			String s = expression.substring(2, expression.length() - 1);
			return s.split("\\.");
		}
		else {
			throw new InvalidExpressionException(expression);
		}
	}
	
	private static String getNucletExpression(EntityMeta<?> meta) {
		if (meta.getNuclet() == null)
			return NucletConstants.DEFAULT_LOCALIDENTIFIER;
		for (MasterDataVO<?> mdvo : MasterDataCache.getInstance().get(E.NUCLET.getUID())) {
			if (LangUtils.equal(mdvo.getPrimaryKey(), meta.getNuclet()))
				return (String)mdvo.getFieldValue(E.NUCLET.localidentifier.getUID());
		}
		throw new IllegalArgumentException("did not find nuclet for entity " + meta.getEntityName() + ": " + meta.getNuclet().getStringifiedDefinition());
	}

	public static String getExpression(EntityMeta<?> meta) {
		return MessageFormat.format("{0}.{1}", getNucletExpression(meta), meta.getEntityName());
	}
	public static String getExpression(EntityMeta<?> meta, FieldMeta<?> field) {
		return MessageFormat.format("{0}.{1}.{2}", getNucletExpression(meta), meta.getEntityName(), field.getFieldName());
	}
	public static String getExpression(UID entityUID, UID fieldUID) {
		return getExpression(MetaProvider.getInstance().getEntity(entityUID), MetaProvider.getInstance().getEntityField(fieldUID));
	}

	public static String parse(String expression) {
		Matcher m = FUNCTION_PATTERN.matcher(expression);
		if (m.matches()) {
			return expression.substring(expression.indexOf('{') + 1, expression.length() - 1);
		} else {
			throw new InvalidExpressionException(expression);
		}
	}

	public static boolean contains(NuclosScript ns, String prefix) {
		return Pattern.compile("\\#\\{" + prefix + "(\\.([a-z]*))?\\}").matcher(ns.getSource()).find();
	}
	
	public static void main(String[] args) {
		String[] parts = parseExpression("#{T_EO.Baum.knoten}");
		for (String p : parts) {
			System.out.println(p);
		}
	}
}
