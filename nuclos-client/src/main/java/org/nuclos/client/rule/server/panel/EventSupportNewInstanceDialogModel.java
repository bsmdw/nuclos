package org.nuclos.client.rule.server.panel;

public class EventSupportNewInstanceDialogModel {
	
	private String sClassname;
	private String sDescription;
	
	public String getClassname() {
		return sClassname;
	}
	public void setClassname(String sClassname) {
		this.sClassname = sClassname;
	}
	public String getDescription() {
		return sDescription;
	}
	public void setDescription(String sDescription) {
		this.sDescription = sDescription;
	}
	
}
