//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.rule.server;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.JEditorPane;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.text.Element;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.combobox.ListComboBoxModel;
import org.jdesktop.swingx.renderer.DefaultListRenderer;
import org.jdesktop.swingx.renderer.IconValue;
import org.jdesktop.swingx.renderer.LabelProvider;
import org.jdesktop.swingx.renderer.StringValue;
import org.nuclos.client.main.Main;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.SimpleDocumentListener;
import org.nuclos.client.ui.gc.IReferenceHolder;
import org.nuclos.client.ui.gc.ListenerUtil;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.NuclosCompileException.ErrorMessage;

import jsyntaxpane.syntaxkits.JavaSyntaxKit;

/**
 * Panel for editing of rules.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Boris.Sander@novabit.de">Boris Sander</a>
 * @version 01.00.00
 */
public class RuleEditPanel extends JPanel {

	private static final Logger LOG = Logger.getLogger(RuleEditPanel.class);
	
	private static class RuleEditorPane extends JEditorPane implements IReferenceHolder {
		
		private final List<EventListener> refs = new LinkedList<EventListener>();
		
		private RuleEditorPane() {
		}
		
		@Override
		public Dimension getPreferredScrollableViewportSize() {
			return new Dimension(0,200);// just a hack.
		}

		@Override
		public void addRef(EventListener o) {
			refs.add(o);
		}
	}
	
	private static class ErrorList extends JList {
		
		private ErrorList() {
		}
		
		@Override
		public Dimension getPreferredScrollableViewportSize() {
			return new Dimension(0,50);// just a hack.
		}
	}
	
	private static class MySimpleDocumentListener extends SimpleDocumentListener {
		
		private final List<ChangeListener> changeListeners = new ArrayList<ChangeListener>();
		
		private MySimpleDocumentListener() {
		}

		@Override
		public void documentChanged(DocumentEvent ev) {
			fireChange();
		}
		
		private void fireChange() {
			for(ChangeListener changelistener : changeListeners) {
				changelistener.stateChanged(new ChangeEvent(this));
			}
		}
		
		void addChangeListener(ChangeListener changelistener) {
			changeListeners.add(changelistener);
		}

		void removeChangeListener(ChangeListener changelistener) {
			changeListeners.remove(changelistener);
		}

	}
	
	private static class UndoAction extends AbstractAction {
		
		private final UndoManager undo;
		
		private UndoAction(UndoManager undo) {
			super("Undo");
			if (undo == null) {
				throw new NullPointerException();
			}
			this.undo = undo;
		}

		@Override
		public void actionPerformed(ActionEvent evt) {
			try {
				if (undo.canUndo()) {
					undo.undo();
				}
			} catch (CannotUndoException e) {
			}
		}

	}
	
	private static class RedoAction extends AbstractAction {
		
		private final UndoManager undo;

		private RedoAction(UndoManager undo) {
			super("Redo");
			if (undo == null) {
				throw new NullPointerException();
			}
			this.undo = undo;
		}

		@Override
		public void actionPerformed(ActionEvent evt) {
			try {
				if (undo.canRedo()) {
					undo.redo();
				}
			} catch (CannotRedoException e) {
			}
		}

	}
	
	private class MyMouseAdapter extends MouseAdapter {
		
		private final RuleEditorPane pnlJavaEditor;
		
		private final ErrorList errorMessagesList;
		
		private MyMouseAdapter(ErrorList errorMessagesList, RuleEditorPane pnlJavaEditor) {
			this.errorMessagesList = errorMessagesList;
			this.pnlJavaEditor = pnlJavaEditor;
		}
		
		@Override
		public void mouseClicked(MouseEvent evt) {
			if (evt.getClickCount() == 2) {
				int index = errorMessagesList.locationToIndex(evt.getPoint());
				Rectangle cellBounds = errorMessagesList.getCellBounds(index, index);
				if (cellBounds != null && cellBounds.contains(evt.getPoint())) {
					ErrorMessage error = (ErrorMessage) errorMessagesList.getModel().getElementAt(index);
					if (error != null) {
						if (uid.equals(error.getUid())) {
							if (error.getLineNumber() != ErrorMessage.NOPOS) {
								int p1 = -1, p2 = -1;
								if (error.getPosition() != ErrorMessage.NOPOS) {
									p1 = (int) error.getStartPosition();
									p2 = (int) error.getEndPosition();
								} else if (error.getLineNumber() != ErrorMessage.NOPOS) {
									Element e = pnlJavaEditor.getDocument().getDefaultRootElement().getElement((int) error.getLineNumber());
									p1 = e.getStartOffset();
									if (error.getColumnNumber() != -1)
										p1 += (int) error.getColumnNumber();
									p2 = p1;
								}
								if (p1 != -1)
									pnlJavaEditor.select(p1, p2);
								pnlJavaEditor.requestFocusInWindow();
							}
						}
						else {
							if (error.getUid() != null) try {
								Main.getInstance().getMainController().showDetails(E.SERVERCODE.getUID(), error.getUid());
							} catch (Exception e) {
								LOG.error(e);
							}
						}
					}
				}
			}
		}		
		
	}

	private final RuleHeaderPanel pnlHeader;
	private final RuleEditorPane pnlJavaEditor;
	private final ErrorList errorMessagesList;
	private final UndoManager undo;

	private final MySimpleDocumentListener sdl = new MySimpleDocumentListener();
	
	private UID uid;
	private UID nucletId;
	private UID entityUid;
	
	public RuleEditPanel(JPanel pnlUsage) {
		super(new BorderLayout());

		pnlHeader = new RuleHeaderPanel();
		pnlJavaEditor = new RuleEditorPane();
		errorMessagesList = new ErrorList();
		errorMessagesList.setBackground(pnlJavaEditor.getBackground());
		errorMessagesList.setForeground(Color.RED);
		errorMessagesList.addMouseListener(new MyMouseAdapter(errorMessagesList, pnlJavaEditor));
		errorMessagesList.setCellRenderer(new DefaultListRenderer(new LabelProvider(new ErrorMessageConverter())));

		final JSplitPane splitpn = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		splitpn.setTopComponent(new JScrollPane(pnlJavaEditor));
		splitpn.setBottomComponent(new JScrollPane(errorMessagesList));
		splitpn.setResizeWeight(1.0);

		jsyntaxpane.DefaultSyntaxKit.initKit();
		// NUCLOS-4076, NUCLOS-4347
		if (JSyntaxPaneReplaceScriptAction.isReplacementNeeded()) {
			jsyntaxpane.DefaultSyntaxKit.getConfig(JavaSyntaxKit.class).put("Action.insert-date","org.nuclos.client.rule.server.JSyntaxPaneReplaceScriptAction");
		}
		pnlJavaEditor.setContentType("text/java");
		pnlJavaEditor.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
		ListenerUtil.registerDocumentListener(pnlJavaEditor.getDocument(), pnlJavaEditor, sdl);
		
		undo = new UndoManager();
    	pnlJavaEditor.getActionMap().put("Undo", new UndoAction(undo));		
    	// Bind the undo action to ctl-Z (or command-Z on mac)
    	pnlJavaEditor.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_Z, Toolkit
    			.getDefaultToolkit().getMenuShortcutKeyMask()), "Undo");
		
    	pnlJavaEditor.getActionMap().put("Redo", new RedoAction(undo));		
    	// Bind the undo action to ctl-Y (or command-Y on mac)
    	pnlJavaEditor.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_Y, Toolkit
    			.getDefaultToolkit().getMenuShortcutKeyMask()), "Redo");

		final JPanel pnlRulesEditor = new JPanel(new BorderLayout());
		pnlRulesEditor.add(splitpn, BorderLayout.CENTER);

		final JTabbedPane tabpn = new JTabbedPane();
		tabpn.add(SpringLocaleDelegate.getInstance().getMessage("RuleEditPanel.1","Regel"), pnlRulesEditor);

		if(pnlUsage != null) {
			add(pnlHeader, BorderLayout.NORTH);
			tabpn.add(SpringLocaleDelegate.getInstance().getMessage("RuleEditPanel.2","Verwendung"), pnlUsage);
		}
		this.add(tabpn, BorderLayout.CENTER);
	}

	RuleHeaderPanel getHeaderPanel() {
		return pnlHeader;
	}

	public JEditorPane getJavaEditorPanel() {
		return pnlJavaEditor;
	}

	public UndoManager getJavaEditorUndoManager() {
		return undo;
	}

	public void setMessages(List<ErrorMessage> messages) {
		if (messages != null) {
			errorMessagesList.setModel(new ListComboBoxModel<ErrorMessage>(messages));
		} else {
			errorMessagesList.setModel(new DefaultListModel());
		}
	}

	public void clearMessages() {
		setMessages(null);
	}

	public void addChangeListener(ChangeListener changelistener) {
		sdl.addChangeListener(changelistener);
	}

	public void removeChangeListener(ChangeListener changelistener) {
		sdl.removeChangeListener(changelistener);
	}

	public UID getUid() {
		return uid;
	}

	public UID getEntityUid() {
	 return this.entityUid;	
	}
	
	public void setEntityUid(UID uidToSet) {
		this.entityUid = uidToSet;
	}
	
	public void setUid(UID id) {
		this.uid = id;
	}

	public UID getNucletUid() {
		return nucletId;
	}

	public void setNucletUid(UID nucletId) {
		this.nucletId = nucletId;
	}

	static class ErrorMessageConverter implements StringValue, IconValue {

		@Override
		public String getString(Object value) {
			ErrorMessage error = (ErrorMessage) value;
			if (error.getPosition() != ErrorMessage.NOPOS) {
				return String.format("%s - %d:%d: %s", error.getSource(), error.getLineNumber(), error.getColumnNumber(),
					error.getMessage(null).replace("\n", ", "));
			} else {
				return error.getMessage(null);
			}
		}
		
		@Override
		public Icon getIcon(Object value) {
			switch (((ErrorMessage) value).getKind()) {
			case ERROR:
				return Icons.getInstance().getIconJobError();
			case WARNING:
			case MANDATORY_WARNING:
				return Icons.getInstance().getIconJobWarning();
			default:
				return Icons.getInstance().getIconJobUnknown();
			}
		}
	}
}
