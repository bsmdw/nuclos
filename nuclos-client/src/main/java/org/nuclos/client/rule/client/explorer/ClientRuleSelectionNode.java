package org.nuclos.client.rule.client.explorer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.rule.client.ClientRuleRepository;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.server.navigation.treenode.TreeNode;

public class ClientRuleSelectionNode extends AbstractClientRuleSelectionNode {
	
	public ClientRuleSelectionNode(ClientRuleEntityNode entityNode) {
		this(entityNode.getId(), entityNode.getRootId(), entityNode.getNodeId(), entityNode.getEntityUID(),
				entityNode.getLabel(), entityNode.getDescription(), entityNode.getIdentifier());
	}
	
	@Override
	public boolean isSelectable() {
		return false;
	}
	
	public ClientRuleSelectionNode(Object id, Object rootId, UID nodeId,
			UID entityUID, String label, String description, String identifier) {
		super(id, rootId, nodeId, entityUID, label, description, identifier);
	}

	@Override
	public List<? extends TreeNode> getSubNodes() {
		
		List<AbstractClientRuleSelectionNode> retVal = new ArrayList<AbstractClientRuleSelectionNode>();
		
		// References
		retVal.add(new ClientRuleSelectGroupRefNode(id, rootId, nodeId, entityUID, "Referenzen", "Referenzen", "Referenzen"));
		// Attributes
		retVal.add(new ClientRuleSelectGroupAttrNode(id, rootId, nodeId, entityUID, "Attribute", "Attribute", "Attribute"));
		// Dependents
		retVal.add(new ClientRuleSelectGroupDepNode(id, rootId, nodeId, entityUID, "Dependents", "Dependents", "Dependents"));
		
		return retVal;
	}
}

class ClientRuleSelectGroupAttrNode extends AbstractClientRuleSelectionNode {
	public ClientRuleSelectGroupAttrNode(Object id, Object rootId, UID nodeId,
			UID entityUID, String label, String description, String identifier) {
		super(id, rootId, nodeId, entityUID, label, description, identifier);
	}

	@Override
	public boolean isSelectable() {
		return false;
	}
	
	@Override
	public List<? extends TreeNode> getSubNodes() {
		List<ClientRuleSelectAttributeNode> retVal = new ArrayList<ClientRuleSelectAttributeNode>();
		
		List<FieldMeta<?>> fieldsByBO = ClientRuleRepository.getInstance().getFieldsByBO(this.nodeId);
		
		for (FieldMeta fm : fieldsByBO) {
			retVal.add(new ClientRuleSelectAttributeNode(fm));
		}
		
		return retVal;
	}
}

class ClientRuleSelectGroupRefNode extends AbstractClientRuleSelectionNode {
	public ClientRuleSelectGroupRefNode(Object id, Object rootId, UID nodeId,
			UID entityUID, String label, String description, String identifier) {
		super(id, rootId, nodeId, entityUID, label, description, identifier);
	}

	@Override
	public boolean isSelectable() {
		return false;
	}
	
	@Override
	public List<? extends TreeNode> getSubNodes() {
		
		List<ClientRuleSelectReferenceNode> retVal = new ArrayList<ClientRuleSelectReferenceNode>();
		Map<Pair<UID, UID>, List<FieldMeta<?>>> referencesByBO = ClientRuleRepository.getInstance().getReferencesByBO(nodeId);
		
		for (Pair<UID, UID> pair : referencesByBO.keySet()) {
				retVal.add(new ClientRuleSelectReferenceNode(pair.getY(), MetaProvider.getInstance().getEntityField(pair.getX()), 
						referencesByBO.get(pair)));
		}		
		Collections.sort(retVal, new Comparator<ClientRuleSelectReferenceNode>() {

			@Override
			public int compare(ClientRuleSelectReferenceNode o1,
					ClientRuleSelectReferenceNode o2) {
				return o1.getLabel().compareTo(o2.getLabel());
			}
		});
		return retVal;
	}
}


class ClientRuleSelectReferenceNode extends AbstractClientRuleSelectionNode {
	private List<FieldMeta<?>> fields;
	
	@Override
	public boolean isSelectable() {
		return false;
	}
	
	public ClientRuleSelectReferenceNode(UID bo, FieldMeta<?> refField, List<FieldMeta<?>> fields) {
		super(bo, null, bo, bo, refField.getFieldName(), refField.getFieldName(), refField.getFieldName());
		this.fields = fields;
	}

	@Override
	public List<? extends TreeNode> getSubNodes() {
		
		List<ClientRuleSelectAttributeNode> retVal = new ArrayList<ClientRuleSelectAttributeNode>();
		
		for (FieldMeta fm : fields) {
			retVal.add(new ClientRuleSelectAttributeNode(fm));
		}
		return retVal;
	}
	
}
	
class ClientRuleSelectAttributeNode extends AbstractClientRuleSelectionNode {
	
	@Override
	public boolean isSelectable() {
		return true;
	}
	
	public ClientRuleSelectAttributeNode(FieldMeta field) {
		super(field.getUID(), null, field.getUID(), field.getEntity(), field.getFieldName(), field.getFieldName(),field.getFieldName());
	}

	@Override
	public String getLabel() {
		return SF.PK_UID.getFieldName().equals(this.label) ? "id" : this.label ;
	}
	
	@Override
	public List<? extends TreeNode> getSubNodes() {
		return null;
	}
}

class ClientRuleSelectDependentNode extends AbstractClientRuleSelectionNode {
	
	private List<FieldMeta<?>> fields;
	
	@Override
	public boolean isSelectable() {
		return false;
	}
	
	public ClientRuleSelectDependentNode(UID bo, EntityMeta<?> entity, List<FieldMeta<?>> fields) {
		super(bo, null, bo, bo, entity.getEntityName(), entity.getEntityName(), entity.getEntityName());
		this.fields = fields;
	}
	
	@Override
	public List<? extends TreeNode> getSubNodes() {
		List<ClientRuleSelectAttributeNode> retVal = new ArrayList<ClientRuleSelectAttributeNode>();
		
		for (FieldMeta fm : this.fields) {
			retVal.add(new ClientRuleSelectAttributeNode(fm));
		}
		return retVal;
	}
}

class ClientRuleSelectGroupDepNode extends AbstractClientRuleSelectionNode {
	public ClientRuleSelectGroupDepNode(Object id, Object rootId, UID nodeId,
			UID entityUID, String label, String description, String identifier) {
		super(id, rootId, nodeId, entityUID, label, description, identifier);
	}

	@Override
	public boolean isSelectable() {
		return false;
	}
	
	@Override
	public List<? extends TreeNode> getSubNodes() {
		List<ClientRuleSelectDependentNode> retVal = new ArrayList<ClientRuleSelectDependentNode>();
		
		Map<UID, List<FieldMeta<?>>> dependentsByBO = ClientRuleRepository.getInstance().getDependentsByBO(nodeId);
		
		for (UID uid: dependentsByBO.keySet()) {
			retVal.add(new ClientRuleSelectDependentNode(uid, MetaProvider.getInstance().getEntity(uid), dependentsByBO.get(uid)));
		}
	
		Collections.sort(retVal, new Comparator<ClientRuleSelectDependentNode>() {

			@Override
			public int compare(ClientRuleSelectDependentNode o1,
					ClientRuleSelectDependentNode o2) {
				return o1.getLabel().compareTo(o2.getLabel());
			}
			
		});
		
		return retVal;
	}
}
