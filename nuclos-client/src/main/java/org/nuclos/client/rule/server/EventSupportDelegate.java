package org.nuclos.client.rule.server;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nuclos.client.explorer.node.eventsupport.EventSupportTargetType;
import org.nuclos.client.explorer.node.eventsupport.EventSupportUsageNode;
import org.nuclos.client.statemodel.StateDelegate;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.common2.exception.NuclosCompileException.ErrorMessage;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeRemote;
import org.nuclos.server.eventsupport.valueobject.EventSupportCommunicationPortVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportEventVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportGenerationVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportJobVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportTransitionVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportVO;
import org.nuclos.server.eventsupport.valueobject.ProcessVO;
import org.nuclos.server.navigation.treenode.TreeNode;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.nuclos.server.statemodel.valueobject.StateTransitionVO;

public class EventSupportDelegate {

	private static EventSupportDelegate INSTANCE;
	private static final Logger LOG = Logger.getLogger(EventSupportDelegate.class);	
	
	private EventSupportDelegate() {}

	public static EventSupportDelegate getInstance()
	{
		if (INSTANCE == null) {
			INSTANCE = SpringApplicationContextHolder.getBean(EventSupportDelegate.class);
		}
		return INSTANCE;
	}
	
	private EventSupportFacadeRemote eventSupportFacadeRemote;

	public final void setEventSupportFacadeRemote(EventSupportFacadeRemote eventSupportFacadeRemote) {
		this.eventSupportFacadeRemote = eventSupportFacadeRemote;
	}
	
	public void forceEventSupportCompilation() throws NuclosBusinessException, NuclosCompileException {
		this.eventSupportFacadeRemote.forceEventSupportCompilation();
	}
	
	public EventSupportJobVO createEventSupportJob(EventSupportJobVO esjVOToInsert) 
			throws NuclosBusinessRuleException, CommonPermissionException, CommonValidationException, CommonCreateException {
		EventSupportJobVO retVal = eventSupportFacadeRemote.createEventSupportJob(esjVOToInsert);			
		return retVal;
	}
	
	public EventSupportTransitionVO createEventSupportTransition(EventSupportTransitionVO eseVOToInsert) 
			throws NuclosBusinessRuleException, CommonPermissionException, CommonValidationException, CommonCreateException {
		EventSupportTransitionVO retVal = eventSupportFacadeRemote.createEventSupportTransition(eseVOToInsert);		
		EventSupportRepository.getInstance().updateEventSupports();
		StateDelegate.getInstance().invalidateCache();
		return retVal;
	}
	
	public EventSupportGenerationVO createEventSupportGeneration(EventSupportGenerationVO eseVOToInsert) 
			throws NuclosBusinessRuleException, CommonPermissionException, CommonValidationException, CommonCreateException {
		EventSupportGenerationVO retVal = eventSupportFacadeRemote.createEventSupportGeneration(eseVOToInsert);
		return retVal;
	}
	
	public EventSupportCommunicationPortVO createEventSupportCommunicationPort(EventSupportCommunicationPortVO eseVOToInsert) 
			throws NuclosBusinessRuleException, CommonPermissionException, CommonValidationException, CommonCreateException {
		EventSupportCommunicationPortVO retVal = eventSupportFacadeRemote.createEventSupportCommunicationPort(eseVOToInsert);
		return retVal;
	}
	
	public EventSupportEventVO createEventSupportEvent(EventSupportEventVO eseVOToInsert) 
			throws NuclosBusinessRuleException, CommonPermissionException, CommonValidationException, CommonCreateException
	{
		EventSupportEventVO retVal = eventSupportFacadeRemote.createEventSupportEvent(eseVOToInsert);
		return retVal;
	}
	
	public void deleteEventSupportTransition(EventSupportTransitionVO eseVOToUpdate) 
			throws NuclosBusinessRuleException, CommonPermissionException, CommonValidationException, 
			CommonFinderException, CommonRemoveException, CommonStaleVersionException, NuclosCompileException {
		
			eventSupportFacadeRemote.deleteEventSupportTransition(eseVOToUpdate);
			EventSupportRepository.getInstance().updateEventSupports();
			StateDelegate.getInstance().invalidateCache();
		
	}
	
	public void deleteEventSupportJob(EventSupportJobVO eseVOToUpdate, boolean updateRepository) 
			throws NuclosBusinessRuleException, CommonPermissionException, CommonValidationException, 
			CommonFinderException, CommonRemoveException, CommonStaleVersionException, NuclosCompileException {
		
			eventSupportFacadeRemote.deleteEventSupportJob(eseVOToUpdate);
			if (updateRepository) {
				EventSupportRepository.getInstance().updateEventSupports();
			}
	}

	public void deleteEventSupportGeneration(EventSupportGenerationVO eseVOToUpdate, boolean updateRepository) 
			throws NuclosBusinessRuleException, CommonPermissionException, CommonValidationException, 
			CommonFinderException, CommonRemoveException, CommonStaleVersionException, NuclosCompileException {
		
			eventSupportFacadeRemote.deleteEventSupportGeneration(eseVOToUpdate);
			if (updateRepository) {
				EventSupportRepository.getInstance().updateEventSupports();
			}
	}
	
	public void deleteEventSupportCommunicationPort(EventSupportCommunicationPortVO eseVOToUpdate, boolean updateRepository) 
			throws NuclosBusinessRuleException, CommonPermissionException, CommonValidationException, 
			CommonFinderException, CommonRemoveException, CommonStaleVersionException, NuclosCompileException {
		
			eventSupportFacadeRemote.deleteEventSupportCommunicationPort(eseVOToUpdate);
			if (updateRepository) {
				EventSupportRepository.getInstance().updateEventSupports();
			}
	}

	public void deleteEventSupportEvent(EventSupportEventVO eseVOToUpdate, boolean updateRepository) 
			throws NuclosBusinessRuleException, CommonPermissionException, CommonValidationException, 
			CommonFinderException, CommonRemoveException, CommonStaleVersionException, NuclosCompileException {
		
			eventSupportFacadeRemote.deleteEventSupportEvent(eseVOToUpdate);
			if (updateRepository) {
				EventSupportRepository.getInstance().updateEventSupports();
			}
	}
		
	public void deleteEventSupportUsage(List<EventSupportVO> eseVOToDelete, boolean updateRepository)
			throws NuclosBusinessRuleException, CommonPermissionException, CommonValidationException, 
			CommonFinderException, CommonRemoveException, CommonStaleVersionException, NuclosCompileException {
		
		for (EventSupportVO elm : eseVOToDelete) {
			if (elm instanceof EventSupportEventVO) {
				deleteEventSupportEvent((EventSupportEventVO) elm, false);
			}
			else if (elm instanceof EventSupportJobVO) {
				deleteEventSupportJob((EventSupportJobVO) elm, false);
			}
			else if (elm instanceof EventSupportTransitionVO) {
				deleteEventSupportTransition((EventSupportTransitionVO) elm);
			} 
			else if (elm instanceof EventSupportGenerationVO) {
				deleteEventSupportGeneration((EventSupportGenerationVO) elm, false);
			} 			
		}
		
		if (updateRepository) {
			EventSupportRepository.getInstance().updateEventSupports();
		}
	}
	
	public void deleteEventSupport(EventSupportSourceVO eseVOToDelete, boolean updateRepository) 
			throws CommonPermissionException, CommonValidationException, NuclosCompileException {
		
		this.eventSupportFacadeRemote.deleteEventSupport(eseVOToDelete);
		if (updateRepository) {
			EventSupportRepository.getInstance().updateEventSupports();
		}
	}
	
	public EventSupportEventVO modifyEventSupportEvent(EventSupportEventVO eseVOToUpdate, UID oldState, UID oldProcess) 
			throws NuclosBusinessRuleException, CommonPermissionException, CommonValidationException, 
			CommonFinderException, CommonCreateException, CommonRemoveException, 
			CommonStaleVersionException, NuclosCompileException {
		
		EventSupportEventVO retVal = eventSupportFacadeRemote.modifyEventSupportEvent(eseVOToUpdate, oldState, oldProcess);
		EventSupportRepository.getInstance().updateEventSupports();
		return retVal;
	}
	
	public EventSupportTransitionVO modifyEventSupportTransition(EventSupportTransitionVO eseVOToUpdate, UID oldTrans) 
			throws NuclosBusinessRuleException, CommonPermissionException, CommonValidationException, 
			CommonCreateException, CommonFinderException, CommonRemoveException, 
			CommonStaleVersionException, NuclosCompileException {
		
		EventSupportTransitionVO retVal = eventSupportFacadeRemote.modifyEventSupportTransition(eseVOToUpdate, oldTrans);
		EventSupportRepository.getInstance().updateEventSupports();
		StateDelegate.getInstance().invalidateCache();
		return retVal;
	}
	
	public EventSupportJobVO modifyEventSupportJob(EventSupportJobVO eseVOToUpdate) 
			throws NuclosBusinessRuleException, CommonPermissionException, CommonValidationException, 
			CommonCreateException, CommonFinderException, CommonRemoveException, 
			CommonStaleVersionException, NuclosCompileException {
		
		EventSupportJobVO retVal = eventSupportFacadeRemote.modifyEventSupportJob(eseVOToUpdate);
		EventSupportRepository.getInstance().updateEventSupports();
		return retVal;
	}
	
	public EventSupportGenerationVO modifyEventSupportGeneration(EventSupportGenerationVO eseVOToUpdate) 
			throws NuclosBusinessRuleException, CommonPermissionException, CommonValidationException, 
			CommonCreateException, CommonFinderException, CommonRemoveException, 
			CommonStaleVersionException, NuclosCompileException {
		
		EventSupportGenerationVO retVal = eventSupportFacadeRemote.modifyEventSupportGeneration(eseVOToUpdate);
		EventSupportRepository.getInstance().updateEventSupports();
		return retVal;
	}
	
	public EventSupportCommunicationPortVO modifyEventSupportCommunicationPort(EventSupportCommunicationPortVO eseVOToUpdate) 
			throws NuclosBusinessRuleException, CommonPermissionException, CommonValidationException, 
			CommonCreateException, CommonFinderException, CommonRemoveException, 
			CommonStaleVersionException, NuclosCompileException {
		
		EventSupportCommunicationPortVO retVal = eventSupportFacadeRemote.modifyEventSupportCommunicationPort(eseVOToUpdate);
		EventSupportRepository.getInstance().updateEventSupports();
		return retVal;
	}
	
	public Collection<ProcessVO> getProcessesByModuleId(UID entityUid)
	{
		Collection<ProcessVO> retVal = EventSupportRepository.getInstance().getProcessesByModuleUid(entityUid);
		return retVal;
	}
	

	public List<EventSupportTransitionVO> getStateTransitionsBySupportType(UID moduleId, String supporttype) 
			throws CommonFinderException, CommonPermissionException {
		List<EventSupportTransitionVO> retVal = new ArrayList<EventSupportTransitionVO>();
		for (StateTransitionVO stVO : StateDelegate.getInstance().getOrderedStateTransitionsByStatemodel(moduleId)) {
			Collection<EventSupportTransitionVO> eventSupportsByTransitionId = EventSupportRepository.getInstance().getEventSupportsByTransitionUid(stVO.getId());
						EventSupportRepository.getInstance().getEventSupportsByTransitionUid(stVO.getPrimaryKey());
			for (EventSupportTransitionVO estVO : eventSupportsByTransitionId) {
				if (estVO.getEventSupportClassType().equals(supporttype)) {
					retVal.add(estVO);
				}
			}	
		}			
		return retVal;
	}

	public Collection<EventSupportSourceVO> findEventSupportsByUsageAndEvent(String sEventName, UsageCriteria usagecriteria) {
		Collection<EventSupportSourceVO> retVal = new ArrayList<EventSupportSourceVO>();
		retVal.addAll(eventSupportFacadeRemote.findEventSupportsByUsageAndEntity(sEventName, usagecriteria));
		return retVal;
	}
	
	/**
	 * 
	 * @throws CommonBusinessException
	 */
	public void createBusinessObjects() throws CommonBusinessException, InterruptedException{
		eventSupportFacadeRemote.createBusinessObjects();
	}
		
	public Map<String, ErrorMessage> getCompileExceptionMessages() {
		return eventSupportFacadeRemote.getCompileExceptionMessages();
	}
	
	@SuppressWarnings("unchecked") 
	public void invalidateCaches(EntityMeta<UID>... entities) {
		for (EntityMeta<UID> entity : entities) {
			this.eventSupportFacadeRemote.invalidateCaches(entity);			
		}
		this.invalidateClientCachesOnly();
	}

	public void invalidateCaches() {
		this.eventSupportFacadeRemote.invalidateCaches();
		this.invalidateClientCachesOnly();
	}

	public void invalidateClientCachesOnly() {
		EventSupportRepository.getInstance().updateEventSupports();
	}

	public TreeNode createExplorerNode(UID pk) throws CommonFinderException{
		if (pk == null)
			throw new CommonFinderException("pk must not be null");
	
		return new EventSupportUsageNode(pk, null, null, null, null, EventSupportTargetType.EVENTSUPPORT);
	}
	
}
