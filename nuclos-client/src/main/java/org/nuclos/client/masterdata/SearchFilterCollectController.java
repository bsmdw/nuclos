//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

import org.nuclos.client.common.DependantCollectableMasterDataMap;
import org.nuclos.client.common.controller.NuclosCollectControllerCommonState;
import org.nuclos.client.main.MainController;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.searchfilter.SearchFilterCache;
import org.nuclos.client.searchfilter.SearchFilterDelegate;
import org.nuclos.client.searchfilter.SearchFilterMode;
import org.nuclos.client.searchfilter.SearchFilterResourceTableModel;
import org.nuclos.client.searchfilter.SearchFilterResultController;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.collect.CollectState;
import org.nuclos.client.ui.collect.CollectStateAdapter;
import org.nuclos.client.ui.collect.CollectStateEvent;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModel;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelAdapter;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelEvent;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelListener;
import org.nuclos.client.ui.collect.result.NuclosSearchResultStrategy;
import org.nuclos.client.ui.labeled.LabeledTextField;
import org.nuclos.client.wizard.util.NuclosWizardUtils;
import org.nuclos.common.E;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.exception.CollectableValidationException;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common2.KeyEnum;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Controller for searchfilters.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:martin.weber@novabit.de">Martin Weber</a>
 * @version 01.00.00
 */
public class SearchFilterCollectController extends MasterDataCollectController<UID> {

	private Integer iSearchDeleted = null;

	public final static UID FIELD_SEARCHFILTER = E.SEARCHFILTER.clbsearchfilter.getUID();
	public final static UID FIELD_LABELRES = E.SEARCHFILTER.labelres.getUID();
	public final static UID FIELD_DESCRIPTIONRES = E.SEARCHFILTER.descriptionres.getUID();

	private SearchFilterResourceTableModel tablemodel = new SearchFilterResourceTableModel();
	private JTable table = new JTable(tablemodel);

	private final CollectableComponentModelListener ccml_fastSelectInResult = new CollectableComponentModelAdapter() {
		@Override
		public void collectableFieldChangedInModel(CollectableComponentModelEvent ev) {
			final CollectableComponentModel fastSelectInResult = getDetailsEditView().getModel().getCollectableComponentModelFor(
					E.SEARCHFILTER.fastSelectInResult.getUID());
			setInputFieldsEnabled(fastSelectInResult.getField().isNull() ? false : (Boolean)fastSelectInResult.getField().getValue());
		}
	};
	
	protected void setInputFieldsEnabled(boolean fastSelectInResult) {
		for (CollectableComponent comp : getDetailsEditView().getCollectableComponentsFor(E.SEARCHFILTER.order.getUID()))
			comp.setEnabled(fastSelectInResult);
//		for (CollectableComponent comp : getDetailsEditView().getCollectableComponentsFor(E.SEARCHFILTER.fastSelectDefault.getUID()))
//			comp.setEnabled(fastSelectInResult);
		for (CollectableComponent comp : getDetailsEditView().getCollectableComponentsFor(E.SEARCHFILTER.fastSelectIcon.getUID()))
			comp.setEnabled(fastSelectInResult);
		for (CollectableComponent comp : getDetailsEditView().getCollectableComponentsFor(E.SEARCHFILTER.fastSelectNuclosIcon.getUID()))
			comp.setEnabled(fastSelectInResult);
	}


	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton} 
	 * to get an instance.
	 * 
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	public SearchFilterCollectController(MainFrameTab tabIfAny, NuclosCollectControllerCommonState state){
		super(E.SEARCHFILTER, tabIfAny, 
				new SearchFilterResultController<CollectableMasterDataWithDependants<UID>>(E.SEARCHFILTER.getUID(),
						new NuclosSearchResultStrategy<UID,CollectableMasterDataWithDependants<UID>>(), state), null);

		// a searchfilter can only be created via a searchpanel of a collectcontroller
		getNewAction().setEnabled(false);

		setSearchDeletedRendererInResultTable();
        getResultTable().getModel().addTableModelListener( new TableModelListener() {
 			@Override
            public void tableChanged(TableModelEvent e) {
 				setSearchDeletedRendererInResultTable();
			}
        });
        
		this.getCollectStateModel().addCollectStateListener(new CollectStateAdapter() {
			@Override
			public void detailsModeEntered(CollectStateEvent ev) throws CommonBusinessException {
				final CollectableComponentModel model = getDetailsEditView().getModel().getCollectableComponentModelFor(E.SEARCHFILTER.fastSelectInResult.getUID());
				if (model != null) {
					model.addCollectableComponentModelListener(null, ccml_fastSelectInResult);
					setInputFieldsEnabled(model.getField().isNull() ? false : (Boolean)model.getField().getValue());
				}
			}

			@Override
			public void detailsModeLeft(CollectStateEvent ev) throws CommonBusinessException {
				if (getDetailsEditView().getModel().getCollectableComponentModelFor(E.SEARCHFILTER.fastSelectInResult.getUID()) != null) {
					getDetailsEditView().getModel().getCollectableComponentModelFor(E.SEARCHFILTER.fastSelectInResult.getUID()).removeCollectableComponentModelListener(ccml_fastSelectInResult);
				}
			}
		});
	}
	
	@Override
	public void init() {
		super.init();
		
		final JCheckBox overrideSearchable = new JCheckBox(SpringLocaleDelegate.getInstance().getMessage("OverrideSearchableForAllEntities.1","Enable search forms in all business objects"));
		overrideSearchable.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("OverrideSearchableForAllEntities.2","Overrides searchable flag temporary"));
		overrideSearchable.setSelected(MainController.isOverrideSearchableForAllEntities());
		overrideSearchable.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				MainController.setOverrideSearchableForAllEntities(overrideSearchable.isSelected());
			}
		});
		
		getResultPanel().getToolBar().add(overrideSearchable);
	}

	@Override
   protected CollectableMasterDataWithDependants<UID> updateCollectable(CollectableMasterDataWithDependants<UID> clct, Object oAdditionalData, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
      final DependantCollectableMasterDataMap mpclctDependants = (DependantCollectableMasterDataMap) oAdditionalData;

      stopEditing();
      
      final MasterDataVO<UID> mdvoOld = mddelegate.get(getEntityUid(), clct.getMasterDataCVO().getPrimaryKey());
      String clbSearchFilter = clct.getMasterDataCVO().getFieldValue(FIELD_SEARCHFILTER, String.class);
      clbSearchFilter = clbSearchFilter.replaceAll("node name=\"" + mdvoOld.getFieldValue(E.SEARCHFILTER.name.getUID()) + "\"", 
    		  "node name=\""+ clct.getMasterDataCVO().getFieldValue(E.SEARCHFILTER.name.getUID())+"\"");
      clct.getMasterDataCVO().setFieldValue(FIELD_SEARCHFILTER, clbSearchFilter);
      
      final UID oId = (UID) SearchFilterDelegate.getInstance().update(getEntityUid(), clct.getMasterDataCVO(), 
    		  mpclctDependants.toDependentDataMap(), tablemodel.getRows());

      final MasterDataVO<UID> mdvoUpdated = mddelegate.get(getEntityUid(), oId);

      return new CollectableMasterDataWithDependants<UID>(clct.getCollectableEntity(), 
    		  new MasterDataVO<UID>(mdvoUpdated, this.readDependants(mdvoUpdated.getId())));
   }

	// a searchfilter can only be created via a searchpanel of a collectcontroller
	@Override
	protected boolean isCloneAllowed() {
		return false;
	}

	private void setSearchDeletedRendererInResultTable() {
		SwingUtilities.invokeLater( new Runnable() {
			@Override
            public void run() {
				TableColumn column = null;
				final int idx_active = getResultTableModel().findColumnByFieldUid(E.SEARCHFILTER.searchDeleted.getUID());
				if( idx_active >= 0 ) {
					column = getResultTable().getColumnModel().getColumn(idx_active);
					column.setCellRenderer(new SearchDeletedCellRenderer());
				}
				getResultTable().validate();
			}
		});
	}

	private	class SearchDeletedCellRenderer extends DefaultTableCellRenderer {

		public SearchDeletedCellRenderer() {
			super();
		}

		@Override
		public Component getTableCellRendererComponent(JTable table, Object oValue, boolean bSelected, boolean bHasFocus, int iRow, int iColumn) {
            final JLabel jLabel = (JLabel)super.getTableCellRendererComponent(table, oValue, bSelected, bHasFocus, iRow, iColumn);

            if (oValue != null && oValue instanceof CollectableField) {
            	CollectableField cf = (CollectableField)oValue;
            	if (cf.getValue() != null) {
            		SearchFilterMode mode = KeyEnum.Utils.findEnum(SearchFilterMode.class, (Integer)((CollectableField)oValue).getValue());
                    jLabel.setText(getSpringLocaleDelegate().getText(mode.getResourceId()));
                    jLabel.setHorizontalAlignment(SwingConstants.LEFT);
            	}
            	else {
            		jLabel.setText("");
            	}
            }
            return jLabel;
		}
	}  // inner class SearchDeletedCellRenderer

	// remember the origin value if field 'searchDeleted'
	@Override
	public CollectableMasterDataWithDependants<UID> findCollectableById(UID entityUid, UID oId, final int dependantsDepth) throws CommonBusinessException {
		final CollectableMasterDataWithDependants<UID> clctmdwd =  super.findCollectableById(entityUid, oId, dependantsDepth);

		iSearchDeleted = (Integer)clctmdwd.getField(E.SEARCHFILTER.searchDeleted.getUID()).getValue();

		return clctmdwd;
	}

	// transform the numeric value of field 'searchDeleted' into a string representation
	@Override
	protected void unsafeFillDetailsPanel(CollectableMasterDataWithDependants<UID> clct) throws NuclosBusinessException {
		super.unsafeFillDetailsPanel(clct);

		if (clct != null && clct.getId() != null) {
			try {
				tablemodel.setRows(SearchFilterDelegate.getInstance().getResources(clct.getMasterDataCVO().getPrimaryKey()));
			} catch (CommonBusinessException e1) {
				throw new NuclosBusinessException(e1);
			}
		}

		JTabbedPane tabbedpane = getTabbedPane(getDetailsPanel());
		Component c = tabbedpane.getComponentAt(2);

		JTextField txtField = new JTextField();
		txtField.getDocument().addDocumentListener(new ResourceDocumentListener());

		txtField.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());
		DefaultCellEditor editor = new DefaultCellEditor(txtField);
		editor.setClickCountToStart(1);

		for(TableColumn col : CollectionUtils.iterableEnum(table.getColumnModel().getColumns())) {
			col.setCellEditor(editor);
		}

		table.getTableHeader().addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				stopEditing();
			}
		});

		table.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);

		if (c instanceof JPanel) {
			JPanel tab = (JPanel) c;
			tab.removeAll();
			tab.setLayout(new BorderLayout());
			tab.add(new JScrollPane(table), BorderLayout.CENTER);
		}
	}

	private static JTabbedPane getTabbedPane(Component component) {
		if (component instanceof JTabbedPane) {
			return (JTabbedPane) component;
		}

		if (component instanceof Container) {
			Container container = (Container) component;
			for (Component c : container.getComponents()) {
				JTabbedPane result = getTabbedPane(c);
				if (result != null) {
					return result;
				}
			}
		}
		return null;
	}

	public void stopEditing() {
		if (table.getCellEditor() != null) {
			table.getCellEditor().stopCellEditing();
		}
	}

	// transform the numeric value of field 'searchDeleted' into a string representation
	@Override
	protected void readValuesFromEditPanel(CollectableMasterDataWithDependants<UID> clct, boolean bSearchTab) throws CollectableValidationException {
		final Collection<CollectableComponent> collclctcomp = this.getDetailsPanel().getLayoutRoot().getCollectableComponentsFor(E.SEARCHFILTER.searchDeleted.getUID());

		Iterator<CollectableComponent> iClctComp = collclctcomp.iterator();
		if (iClctComp.hasNext()) {
			JComponent comp = iClctComp.next().getJComponent();

			if (comp instanceof LabeledTextField) {
				if (iSearchDeleted == null) {
					((LabeledTextField)comp).getTextField().setText(null);
				}
				else {
					((LabeledTextField)comp).getTextField().setText(""+iSearchDeleted);
				}
			}
		}

		super.readValuesFromEditPanel(clct, bSearchTab);
	}

	@Override
	protected void deleteCollectable(CollectableMasterDataWithDependants<UID> clct, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		String name = (String) clct.getMasterDataWithDependantsCVO().getFieldValue(E.SEARCHFILTER.name.getUID());
		UID entity = clct.getMasterDataWithDependantsCVO().getFieldUid(E.SEARCHFILTER.entity.getUID());
		mddelegate.remove(getEntityUid(), clct.getMasterDataCVO(), getCustomUsage());
		if(!StringUtils.isNullOrEmpty(name))
			SearchFilterCache.getInstance().removeFilter(name, clct.getMasterDataWithDependantsCVO().getCreatedBy(), entity);
	}

	public void enterEditMode() {
		if (getCollectState().getInnerState() == CollectState.DETAILSMODE_VIEW) {
			try {
				this.setCollectState(CollectState.OUTERSTATE_DETAILS, CollectState.DETAILSMODE_EDIT);
			}
			catch (CommonBusinessException ex) {
				Errors.getInstance().showExceptionDialog(this.getTab(), ex);
			}
		}
	}

	private class ResourceDocumentListener implements DocumentListener {

		@Override
		public void insertUpdate(DocumentEvent e) {
			enterEditMode();
		}

		@Override
		public void removeUpdate(DocumentEvent e) {
			enterEditMode();
		}

		@Override
		public void changedUpdate(DocumentEvent e) {
			enterEditMode();
		}
	}
}
