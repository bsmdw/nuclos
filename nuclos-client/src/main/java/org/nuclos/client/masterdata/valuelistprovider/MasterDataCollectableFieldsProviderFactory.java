//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.valuelistprovider;

import java.lang.reflect.Constructor;

import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.datasource.DatasourceDelegate;
import org.nuclos.client.genericobject.Modules;
import org.nuclos.client.genericobject.valuelistprovider.AttributeCollectableFieldsProvider;
import org.nuclos.client.genericobject.valuelistprovider.GenericObjectCollectableFieldsProvider;
import org.nuclos.client.genericobject.valuelistprovider.MandatorCollectableFieldsProvider;
import org.nuclos.client.genericobject.valuelistprovider.ModuleSubEntityCollectableFieldsProvider;
import org.nuclos.client.genericobject.valuelistprovider.PrintServiceTraysCollectableFieldsProvider;
import org.nuclos.client.genericobject.valuelistprovider.PrintServiceTraysSelfCollectableFieldsProvider;
import org.nuclos.client.genericobject.valuelistprovider.ProcessCollectableFieldsProvider;
import org.nuclos.client.genericobject.valuelistprovider.StatusAndNumeralCollectableFieldsProvider;
import org.nuclos.client.genericobject.valuelistprovider.generation.GenerationAttributeCollectableFieldsProvider;
import org.nuclos.client.genericobject.valuelistprovider.generation.GenerationSourceTypeCollectableFieldsProvider;
import org.nuclos.client.genericobject.valuelistprovider.generation.GenerationSubEntityCollectableFieldsProvider;
import org.nuclos.client.job.valuelistprovider.JobDBObjectCollectableFieldsProvider;
import org.nuclos.client.tasklist.DynamicTasklistAttributeProvider;
import org.nuclos.client.valuelistprovider.AttributeGroupFunctionCollectableFieldsProvider;
import org.nuclos.client.valuelistprovider.DBObjectCollectableFieldsProvider;
import org.nuclos.client.valuelistprovider.DBObjectTypeCollectableFieldsProvider;
import org.nuclos.client.valuelistprovider.DBTypeCollectableFieldsProvider;
import org.nuclos.client.valuelistprovider.EntityCollectableFieldsProvider;
import org.nuclos.client.valuelistprovider.EntityCollectableIdFieldsProvider;
import org.nuclos.client.valuelistprovider.NucletIntegrationPointsCollectableFieldsProvider;
import org.nuclos.client.valuelistprovider.NucletsCollectableFieldsProvider;
import org.nuclos.client.valuelistprovider.cache.CollectableFieldsProviderCache;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosConstants;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableFieldsProviderFactory;
import org.nuclos.common.collect.collectable.DefaultCollectableEntityProvider;
import org.nuclos.common.collect.collectable.ValueListProviderType;
import org.nuclos.common.fileimport.ImportMode;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonFatalException;

/**
 * <code>CollectableFieldsProviderFactory</code> for masterdata.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */

public class MasterDataCollectableFieldsProviderFactory implements CollectableFieldsProviderFactory {
	
	public MasterDataCollectableFieldsProviderFactory() {
		
	}
	
	public MasterDataCollectableFieldsProviderFactory(UID entity) {
		//TODO: Entity not needed anymore, only because of Reflection still available
	}

	@Override
    public CollectableFieldsProvider newDefaultCollectableFieldsProvider(UID fieldUid) {
		final UID entityUid = MetaProvider.getInstance().getEntityField(fieldUid).getEntity();
		final CollectableEntity _clcte = DefaultCollectableEntityProvider.getInstance().getCollectableEntity(entityUid);
		if (entityUid == null || LangUtils.equal(entityUid, _clcte == null ? null : _clcte.getUID())) {
			return newMasterDataCollectableFieldsProvider(_clcte.getUID(), fieldUid);
		} else {
			return newMasterDataCollectableFieldsProvider(entityUid, fieldUid);
		}
	}

	@Override
    public CollectableFieldsProvider newDependantCollectableFieldsProvider(UID fieldUid) {
		final UID entityUid = MetaProvider.getInstance().getEntityField(fieldUid).getEntity();

		final CollectableEntity _clcte = DefaultCollectableEntityProvider.getInstance().getCollectableEntity(entityUid);
		final CollectableEntityField _clctef = _clcte.getEntityField(fieldUid);

		if (!_clctef.isReferencing()) {
			throw new NuclosFatalException(SpringLocaleDelegate.getInstance().getMessage(
					"GenericObjectCollectableFieldsProviderFactory.1",
					"Das Feld {0} in der Entit\u00e4t {1} referenziert keine andere Entit\u00e4t.", 
					_clctef.getUID(), _clcte.getUID()));
		}
		return new DependantMasterDataCollectableFieldsProvider(_clcte.getUID(), fieldUid);
	}

	/**
	 * @deprecated Use {@link #newCustomCollectableFieldsProvider(ValueListProviderType, UID)} instead.
	 */
	@Override
    public CollectableFieldsProvider newCustomCollectableFieldsProvider(String sCustomType, UID fieldUid) {
		if (UID.isStringifiedUID(sCustomType)) { //@todo. normaly we have to give type information here.
			UID baseEntityUID = null;
			if(fieldUid != null) {
				FieldMeta<?> fmeta = MetaProvider.getInstance().getEntityField(fieldUid);
				baseEntityUID = fmeta.getForeignEntity()!=null? fmeta.getForeignEntity(): fmeta.getLookupEntity();
			}
			return new DatasourceBasedCollectableFieldsProvider(true, UID.parseUID(sCustomType), DatasourceDelegate.getInstance(), baseEntityUID, fieldUid);
		}
		return newCustomCollectableFieldsProvider(ValueListProviderType.toValueListProviderType(sCustomType), fieldUid);
	}
	
	@Override
    public CollectableFieldsProvider newCustomCollectableFieldsProvider(ValueListProviderType sCustomType, UID fieldUid) {
		if (sCustomType == null)
			throw new IllegalArgumentException("ValueListProviderType has not to be null!");
		
		final CollectableFieldsProvider result;
		switch (sCustomType) {
		case DEFAULT:
			result = newDefaultCollectableFieldsProvider(fieldUid);
			break;
		case ENTITY:
			result = new EntityCollectableFieldsProvider();
			break;
		case ATTRIBUTE_GROUP:
			result = new AttributeGroupFunctionCollectableFieldsProvider();
			break;
		case ENTITY_ID:
			result = new EntityCollectableIdFieldsProvider();
			break;
		case PROCESS:
			result = new ProcessCollectableFieldsProvider();
			break;
		case MANDATOR:
			result = new MandatorCollectableFieldsProvider(null, fieldUid);
			break;
		case ATTRIBUTE:
			result = new AttributeCollectableFieldsProvider();
			break;
		case GENERATION_SOURCE:
			result = new GenerationSourceTypeCollectableFieldsProvider();
			break;
		case GENERATION_ATTRIBUTE:
			result = new GenerationAttributeCollectableFieldsProvider();
			break;
		case GENERATION_SUBENTITY:
			result = new GenerationSubEntityCollectableFieldsProvider();
			break;
		case MODULE_SUBENTITY:
			result = new ModuleSubEntityCollectableFieldsProvider();
			break;
		case ALL_REPORTS:
			result = new AllReportsCollectableFieldsProvider();
			break;
		case ALL_GENERATIONS:
			result = new AllGenerationsCollectableFieldsProvider();
			break;
		case ALL_RECORD_GRANTS:
			result = new AllRecordgrantsCollectableFieldsProvider();
			break;
		case MASTERDATA_ENTITY:
			result = new MasterDataEntityCollectableFieldsProvider();
			break;
		case MASTERDATA_FIELDS:
			result = new MasterDataEntityFieldsCollectableFieldsProvider();
			break;
		case PARAMETERS:
			result = new ParametersCollectableFieldsProvider();
			break;
		case SELECTIVE_ATTRIBUTE:
			result = new SelectiveAttributeCollectableFieldProvider();
			break;
		case MODULE_SUBENTITY_FIELD:
			result = new AllCollectableEntityFieldsCollectableFieldsProvider();
			break;
		case DATASOURCE:
			FieldMeta<?> fmeta = MetaProvider.getInstance().getEntityField(fieldUid);
			UID baseEntityUID = fmeta.getForeignEntity()!=null? fmeta.getForeignEntity(): fmeta.getLookupEntity();
			result = new DatasourceBasedCollectableFieldsProvider(false, DatasourceDelegate.getInstance(), baseEntityUID, fieldUid);
			break;
		case SUBENTITY:
			result = new SubformEntityCollectableFieldsProvider();
			break;
		case MASTERDATA_SUBENTITIES:
			result = new MasterDataSubformEntityCollectableFieldsProvider();
			break;
		case FOREIGN_ENTITY_FIELDS:
			result = new ForeignEntityFieldsCollectableFieldsProvider();
			break;
		case JOB_DB_OBJECTS:
			result = new JobDBObjectCollectableFieldsProvider();
			break;
		case ENTITY_FIELDS:
			result = new EntityFieldsCollectableFieldsProvider();
			break;
		case FIELD_OR_ATTRIBUTE:
			result = new FieldOrAttributeCollectableFieldsProvider();
			break;
		case DEPENDENTS:
			result = new DependantsCollectableFieldsProvider();
			break;
		case STATUS_NUMERAL:
			result = new StatusAndNumeralCollectableFieldsProvider();
			break;
		case IMPORT_ENTITIES:
			result = new ImportEntityCollectableFieldsProvider();
			break;
		case IMPORT_FIELDS:
			result = new ImportEntityFieldsCollectableFieldsProvider();
			break;
		case REFERENCED_ENTITY_FIELD:
			result = new ReferencedEntityFieldCollectableFieldsProvider();
			break;
		case CSV_IMPORT_STRUCTURES:
			result = new CsvImportStructureCollectableFieldsProvider(ImportMode.NUCLOSIMPORT);
			break;
		case XML_IMPORT_STRUCTURES:
			result = new XmlImportStructureCollectableFieldsProvider(ImportMode.NUCLOSIMPORT);
			break;
		case SUBENTITY_FIELDS:
			result = new SubFormFieldsCollectableFieldsProvider();
			break;
		case ACTIONS:
			result = new RoleActionsCollectableFieldsProvider();
			break;
		case GENERIC:
			result = new GenericCollectableFieldsProvider();
			break;
		case ENUM:
			result = new EnumCollectableFieldsProvider();
			break;
		case DB_TYPE:
			result = new DBTypeCollectableFieldsProvider();
			break;
		case DB_OBJECT_TYPE:
			result = new DBObjectTypeCollectableFieldsProvider();
			break;
		case DB_OBJECT:
			result = new DBObjectCollectableFieldsProvider();
			break;
		case ROLE_HIERACHY_USERS:
			result = new RoleHierarchyUsersCollectableFieldsProvider();
			break;
		case READABLE_USERS:
			result = new UserCollectableFieldsProvider();
			break;
		case LOCALES:
			result = new LocaleCollectableFieldsProvider();
			break;
		case ASSIGN_WORKSPACES:
			result = new AssignableWorkspaceCollectableFieldsProvider();
			break;
		case SHAREABLE_PREFERENCES:
			result = new ShareablePreferencesCollectableFieldsProvider();
			break;
		case CUSTOM_REST_RULES:
			result = new CustomRestRulesCollectableFieldsProvider();
			break;
		case SYSTEM_PARAMETER:
			result = new SystemParameterCollectableFieldsProvider();
			break;
		case DYNAMIC_TASKLIST_ATTRIBUTES:
			result = new DynamicTasklistAttributeProvider();
			break;
		case PRINTSERVICE_TRAYS:
			result = new PrintServiceTraysCollectableFieldsProvider();
			break;
		case PRINTSERVICE_TRAYS_SELF:
			result = new PrintServiceTraysSelfCollectableFieldsProvider();
			break;
		case COMMUNICATION_INTERFACE_PARAMETER:
			result = new CommunicationInterfaceParameterCollectableFieldsProvider();
			break;
		case USER_COMMUNICATION_ACCOUNT:
			result = new UserCommunicationAccountCollectableFieldsProvider();
			break;
		case NUCLETS:
			result = new NucletsCollectableFieldsProvider();
			break;
		case NUCLET_INTEGRATION_POINTS:
			result = new NucletIntegrationPointsCollectableFieldsProvider();
			break;
		default:
			throw new NuclosFatalException(SpringLocaleDelegate.getInstance().getMessage(
					"GenericObjectCollectableFieldsProviderFactory.2",
					"Unbekannter valuelist-provider Typ: {0}", sCustomType));
		}
		return result;
	}

	/**
	 * helper method (also used in GenericObjectCollectableFieldsProviderFactory)
	 * 
	 * §precondition sEntityName != null
	 */
	public static CollectableFieldsProvider newMasterDataCollectableFieldsProvider(UID entityUid, UID fieldUid) {
		final CollectableEntity clcte = DefaultCollectableEntityProvider.getInstance().getCollectableEntity(entityUid);
		final CollectableEntityField clctef = clcte.getEntityField(fieldUid);
		if (!clctef.isReferencing()) {
			throw new NuclosFatalException(SpringLocaleDelegate.getInstance().getMessage(
					"GenericObjectCollectableFieldsProviderFactory.1",
					"Das Feld {0} referenziert keine andere Entit\u00e4t.", 
					fieldUid));
		}

		if (Modules.getInstance().isModule(clctef.getReferencedEntityUID())) {
			return new GenericObjectCollectableFieldsProvider(clctef);
		} else {
			MasterDataCollectableFieldsProvider mdcfp = new MasterDataCollectableFieldsProvider(clctef.getReferencedEntityUID());
			mdcfp.setParameter(NuclosConstants.VLP_STRINGIFIED_FIELD_DEFINITION_PARAMETER, clctef.getReferencedEntityFieldName());
			return mdcfp;
		}
	}

	public static CollectableFieldsProviderFactory newFactory(UID entityUid) {
		try {
			final String sClassName = LangUtils.defaultIfNull(
					ApplicationProperties.getInstance().getMasterDataCollectableFieldsProviderFactoryClassName(),
					MasterDataCollectableFieldsProviderFactory.class.getName());

			final Class<? extends MasterDataCollectableFieldsProviderFactory> clsmdcfpf 
				= LangUtils.getClassLoaderThatWorksForWebStart().loadClass(sClassName).asSubclass(MasterDataCollectableFieldsProviderFactory.class);
			final Constructor<? extends MasterDataCollectableFieldsProviderFactory> ctor = clsmdcfpf.getConstructor(UID.class);

			return ctor.newInstance(entityUid);
		}
		catch (Exception ex) {
			throw new CommonFatalException("MasterDataCollectableFieldsProviderFactory cannot be created.", ex);
		}
	}

	public static CollectableFieldsProviderFactory newFactory(UID entityUid, CollectableFieldsProviderCache cache) {
		CollectableFieldsProviderFactory factory = newFactory(entityUid);
		return (cache != null) ? cache.makeCachingFieldsProviderFactory(factory) : factory;
	}
	
}	// class MasterDataCollectableFieldsProviderFactory
