//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.masterdata.valuelistprovider.MasterDataCollectableFieldsProviderFactory;
import org.nuclos.client.ui.collect.component.CollectableComponentFactory;
import org.nuclos.client.ui.layoutml.LayoutMLParser;
import org.nuclos.client.ui.layoutml.LayoutRoot;
import org.nuclos.client.valuelistprovider.cache.CollectableFieldsProviderCache;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.masterdata.CollectableMasterDataEntity;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.layoutml.exception.LayoutMLException;
import org.xml.sax.InputSource;

/**
 * Helper class for master data layouts.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class MasterDataLayoutHelper {

	private final static Set<EntityMeta<?>> simpleLayoutEntities;	
	static {
		final Set<EntityMeta<?>> set = new HashSet<EntityMeta<?>>();
		set.add(E.CHART);
		set.add(E.DATASOURCE);
		set.add(E.DYNAMICENTITY);
		set.add(E.VALUELISTPROVIDER);
		set.add(E.RECORDGRANT);
		set.add(E.DYNAMICTASKLIST);
		set.add(E.CALCATTRIBUTE);
		set.add(E.STATEMODEL);
		set.add(E.JOBCONTROLLER);
		set.add(E.ENTITYRELATION);
		simpleLayoutEntities = Collections.unmodifiableSet(set);
	}

	private MasterDataLayoutHelper() {
	}

	public static Reader getLayoutMLReader(UID entityUid, boolean bSearch, String customUsage) throws NuclosBusinessException {
		checkLayoutMLExistence(entityUid, bSearch);
		return new StringReader(MasterDataDelegate.getInstance().getLayoutML(entityUid, bSearch, customUsage));
	}

	public static LayoutRoot<?> newLayoutRoot(UID entityUid, boolean bSearch, String customUsage) throws NuclosBusinessException {
		final LayoutMLParser parser = new LayoutMLParser();

		final CollectableMasterDataEntity clcte = new CollectableMasterDataEntity(
				MetaProvider.getInstance().getEntity(entityUid));
		LayoutRoot<?> result;

		final Reader reader = MasterDataLayoutHelper.getLayoutMLReader(entityUid, bSearch, customUsage);
		final UID layoutUID = MasterDataDelegate.getInstance().getLayoutUid(entityUid, bSearch, customUsage);

		final InputSource isrc = new InputSource(new BufferedReader(reader));

		try {
			result = parser.getResult(layoutUID, isrc, clcte, bSearch, false, null, MasterDataCollectableFieldsProviderFactory.newFactory(
					entityUid, new CollectableFieldsProviderCache()), CollectableComponentFactory.getInstance());
		}
		catch (LayoutMLException ex) {
			throw new NuclosFatalException(ex);
		}
		catch (IOException ex) {
			throw new NuclosFatalException(ex);
		}

		return result;
	}

	public static boolean isLayoutMLAvailable(UID entityUid, boolean bSearch) {
		return MasterDataDelegate.getInstance().getLayoutML(entityUid, bSearch) != null;
	}

	public static boolean hasSpecialLayout(UID entityUid) {
		final MetaProvider mProv = MetaProvider.getInstance();
		final EntityMeta<?> entity = mProv.getEntity(entityUid);
		// return simpleLayoutEntities.contains(E.getByUID(entityUid));
		return simpleLayoutEntities.contains(entity);
	}

	public static void checkLayoutMLExistence(UID entityUid, boolean bSearchable) throws NuclosBusinessException {
		final SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();
		if (!isLayoutMLAvailable(entityUid, bSearchable) && !hasSpecialLayout(entityUid)) {
			final String form = bSearchable ? localeDelegate.getResource("R00011933", "Suchmaske") 
					: localeDelegate.getResource("R00022868", "Eingabemaske");
			final String entityName = localeDelegate.getLabelFromMetaDataVO(
					MasterDataDelegate.getInstance().getMetaData(entityUid));
			throw new NuclosBusinessException(localeDelegate.getMessage(
					"masterdata.error.layout.missing", "Das Layout f\u00fcr die {0} der Entit\u00e4t '{1}' fehlt", form, entityName));
		}
	}

}	// class MasterDataLayoutHelper
