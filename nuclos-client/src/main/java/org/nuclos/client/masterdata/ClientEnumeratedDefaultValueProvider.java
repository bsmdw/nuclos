//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.nuclos.client.entityobject.EntityObjectDelegate;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.format.RefValueExtractor;
import org.nuclos.common.masterdata.EnumeratedDefaultValueProvider;
import org.nuclos.common2.DateUtils;
import org.nuclos.common2.RelativeDate;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;

public class ClientEnumeratedDefaultValueProvider implements EnumeratedDefaultValueProvider {
	
	@Override
	public CollectableField getDefaultValue(FieldMeta<?> fieldmeta) {
		try {
			final String sDefault = fieldmeta.getDefaultValue();
			final Long iDefaultId = fieldmeta.getDefaultForeignId();
			
			if (iDefaultId != null) {
				EntityObjectVO<?> referencedObject = EntityObjectDelegate.getInstance().getReferenced(fieldmeta.getForeignEntity(), fieldmeta.getUID(), iDefaultId);
				if (referencedObject == null) {
					throw new NuclosFatalException(StringUtils.getParameterizedExceptionMessage("error.enumerated.default.value.notfound", fieldmeta.getLocaleResourceIdForLabel()));
				}
				String stringifiedValue = RefValueExtractor.get(referencedObject, fieldmeta.getUID(), null);
				return new CollectableValueIdField(iDefaultId, stringifiedValue);
				
			} else {
				String dataType = fieldmeta.getDataType();
				if(dataType.equals("java.lang.Double")) {
					return new CollectableValueField(Double.parseDouble(sDefault.replace(',', '.')));
				}
				else if(dataType.equals("java.lang.Integer")) {
					return new CollectableValueField(Integer.parseInt(sDefault));
				}
				else if(dataType.equals("java.lang.Boolean")) {
					if("ja".equals(sDefault))
						return new CollectableValueField(Boolean.TRUE);
					else
						return new CollectableValueField(Boolean.FALSE);
				}
				else if(dataType.equals("java.util.Date")) {
					if (RelativeDate.today().toString().equals(sDefault)) {
						return new CollectableValueField(DateUtils.today());
					}
					else {
						// NUCLOS-1914
						final String format = fieldmeta.getFormatInput();
						final DateFormat formatter;
						if (format != null) {
							formatter = new SimpleDateFormat(format);
						} else {
							formatter = SpringLocaleDelegate.getInstance().getDateFormat();
						}
						return new CollectableValueField(formatter.parse(sDefault));
					}
				}
				else {
					return new CollectableValueField(sDefault);
				}
			}
		}
		catch (Exception ex) {
			throw new NuclosFatalException(ex);
		}
	}
	
}
