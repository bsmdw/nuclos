//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.valuelistprovider;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.nuclos.client.datasource.DatasourceDelegate;
import org.nuclos.client.i18n.language.data.DataLanguageContext;
import org.nuclos.common.NuclosConstants;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.querybuilder.DatasourceUtils;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common.valuelistprovider.IDataSource;
import org.nuclos.common.valuelistprovider.VLPQuery;
import org.nuclos.common.valuelistprovider.VLPUtils;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;

/**
 * Collectable fields provider retrieving its values from a given datasource with
 * a specified set of datasource parameters.
 *
 * Valid parameters for this collectable field provider are:
 * datasource - name of this datasource for use in detail mode
 * datasource-search - name of the datasource for use in search mode; if not specified, "datasource" is used there, too
 * fieldname - name of the result column of the datasource to use as the source of values
 * id-fieldname - name of the result column of the datasource to use as the source of ids
 * _searchmode - automatically set by the component, depending on the use of the component as search or details component
 *       (now org.nuclos.common.NuclosConstants.VLP_SEARCHMODE_PARAMETER)
 *
 * Every other parameter is given to the datasource as parameter for it.
 * These parameters must be specified in the datasource to take effect.
 *
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * @author	<a href="mailto:uwe.allner@novabit.de">uwe.allner</a>
 * @version 01.00.00
 *
 * TODO: validating given parameters and return fields - depends on better interface for datasources
 * (e.g. information about contained result fields, parameters etc. without the need to parse XML)
 */
public class DatasourceBasedCollectableFieldsProvider implements CacheableCollectableFieldsProvider, DefaultValueProvider,
		MandatorRestrictionCollectableFieldsProvider, CollectableFieldsProviderWithParameters {

	private boolean bSearchmode = false;
	private String sValueFieldName = null;
	private String sIdFieldName = null;
	private String sDefaultMarkerFieldName = null;
	private DatasourceVO dsvo = null;
	private DatasourceVO dsvoSearch = null;
	private final Map<String, Object> mpParameters = new HashMap<String, Object>();
	private List<DatasourceParameterVO> collParameters;
	private final UID baseEntityUID;
	private final UID entityFieldUID;
	private UID mandatorUID;

	private final boolean isValuelistProviderDatasource;
	private final IDataSource dataSource;

	public DatasourceBasedCollectableFieldsProvider(boolean isValuelistProviderDatasource, IDataSource dataSource, UID baseEntityUID, UID entityFieldUID) {
		this(isValuelistProviderDatasource, null, dataSource, baseEntityUID, entityFieldUID);
	}
	public DatasourceBasedCollectableFieldsProvider(boolean isValuelistProviderDatasource, UID oValue, IDataSource dataSource, UID baseEntityUID, UID entityFieldUID) {
		this.isValuelistProviderDatasource = isValuelistProviderDatasource;
		this.dataSource = dataSource;
		this.baseEntityUID = baseEntityUID;
		this.entityFieldUID = entityFieldUID;
		if (isValuelistProviderDatasource) {
			if (oValue != null) {
				try {
					dsvo = dataSource.getValuelistProvider(oValue);
					setParameters(collParameters = dataSource.getParametersFromXML(dsvo.getSource()));
				}
				catch (CommonBusinessException e) {
					throw new CommonFatalException(
							SpringLocaleDelegate.getInstance().getMessage(
									"datasource.collectable.fieldsprovider", "Fehler beim Laden der Datenquelle ''{0}'':\n", oValue), e);
				}
				catch (NullPointerException e) {
					throw new CommonFatalException(
							SpringLocaleDelegate.getInstance().getMessage(
									"datasource.collectable.fieldsprovider", "Fehler beim Laden der Datenquelle ''{0}'':\n", oValue), e);
				}
			}
		}
	}

	public boolean isValuelistProviderDatasource() {
		return isValuelistProviderDatasource;
	}

	/**
	 * Every parameter except datasource and _searchmode will be transferred to the specified datasource.
	 */
	@Override
	public void setParameter(String sName, Object oValue) {
		if (!isValuelistProviderDatasource) {
			if (sName.equals("datasource") && (dsvo == null || !dsvo.getName().equals(oValue))) {
				try {
					String sDatasourceName = (String) oValue;
					dsvo = dataSource.getDatasourceByName(sDatasourceName);
					setParameters(collParameters = dataSource.getParametersFromXML(dsvo.getSource()));

					if (StringUtils.isNullOrEmpty(sIdFieldName)) {
						for (String sColumn : DatasourceUtils.getColumnsWithoutQuotes(
								dataSource.getColumnsFromVLP(getDatasourceVO().getId(), Number.class))) {
							final String sColumnTmp = VLPUtils.extractFieldName(sColumn);
							if ("intid".equalsIgnoreCase(sColumnTmp)) {
								sIdFieldName = sColumnTmp;
								mpParameters.put(ValuelistProviderVO.DATASOURCE_IDFIELD, sIdFieldName);
								break;
							}
						}
					}
				}
				catch (CommonBusinessException e) {
					throw new CommonFatalException(
							SpringLocaleDelegate.getInstance().getMessage(
									"datasource.collectable.fieldsprovider", "Fehler beim Laden der Datenquelle ''{0}'':\n", oValue), e);
					//"Fehler beim Laden der Datenquelle \"" + oValue + "\":\n", e);
				}
			}
			else if (sName.equals("datasource-search") && (dsvoSearch == null || !dsvoSearch.getName().equals(oValue))) {
				try {
					String sDatasourceName = (String) oValue;
					dsvoSearch = dataSource.getDatasourceByName(sDatasourceName);
					setParameters(dataSource.getParametersFromXML(dsvoSearch.getSource()));
					
					if (StringUtils.isNullOrEmpty(sIdFieldName)) {
						for (String sColumn : DatasourceUtils.getColumnsWithoutQuotes(
								dataSource.getColumnsFromVLP(getDatasourceVO().getId(), Number.class))) {
							final String sColumnTmp = VLPUtils.extractFieldName(sColumn);
							if ("intid".equalsIgnoreCase(sColumnTmp)) {
								sIdFieldName = sColumnTmp;
								mpParameters.put(ValuelistProviderVO.DATASOURCE_IDFIELD, sIdFieldName);
								break;
							}
						}
					}
				}
				catch (CommonBusinessException e) {
					throw new CommonFatalException(
							SpringLocaleDelegate.getInstance().getMessage(
									"datasource.collectable.fieldsprovider", "Fehler beim Laden der Datenquelle ''{0}'':\n", oValue), e);
						//"Fehler beim Laden der Datenquelle \"" + oValue + "\":\n", e);
				}
			}
		}

		if(sName.equals(ValuelistProviderVO.DATASOURCE_NAMEFIELD)) {
			// TODO: validate existence
			sValueFieldName = VLPUtils.getNameField(oValue, mpParameters);
		}
		else if(sName.equals(ValuelistProviderVO.DATASOURCE_IDFIELD)) {
			// TODO: validate existence
			sIdFieldName = VLPUtils.getIdField(oValue, mpParameters, getDatasourceVO().getId(), dataSource);
		}
		else if(sName.equals("default-fieldname")) {
			// TODO: validate existence
			sDefaultMarkerFieldName = VLPUtils.extractFieldName((String) oValue);
		}
		else if(sName.equals("_searchmode")) {
			bSearchmode = (Boolean) oValue;
			// NUCLOS-4300
			mpParameters.put("searchmode", bSearchmode ? 1 : 0);
		}
		else if(sName.equals(NuclosConstants.VLP_MANDATOR_PARAMETER)) {
			if (oValue instanceof String) {
				final String sValue = (String) oValue;
				if (!StringUtils.looksEmpty(sValue)) {
					mandatorUID = new UID(sValue);
				} else {
					mandatorUID = null;
				}
			} else {
				mandatorUID = (UID) oValue;
			}
		}
		// NUCLEUSINT-1077
		else {
			mpParameters.put(sName, parseParameter(sName, oValue));
		}
	}
	
	public boolean requiresParameter(String sParameterName) {
		if (sParameterName == null) {
			return false;
		}
		for(DatasourceParameterVO dpvo : collParameters) {
			if (sParameterName.equals(dpvo.getParameter())) {
				return true;
			}
		}
		return false;
	}
	
	private Object parseParameter(String sName, Object oValue) {
		if (oValue instanceof Collection) {
			Collection<?> col = (Collection<?>) oValue;
			List<Object> result = new ArrayList<Object>();
			for (Object o : col) result.add(parseSingleParameter(sName, o));
			return result;
		} 
		return parseSingleParameter(sName, oValue);
	}

	private Object parseSingleParameter(String sName, Object oValue) {
		if (collParameters != null) {
			for(DatasourceParameterVO dpvo : collParameters) {
				if(dpvo.getParameter().equals(sName)) {
					return DatasourceUtils.parseParameterValue(dpvo, oValue,
							DatasourceDelegate.getInstance().getBooleanRepresentationInSQL(Boolean.TRUE),
							DatasourceDelegate.getInstance().getBooleanRepresentationInSQL(Boolean.FALSE));
				}
			}
		}
		return oValue;
	}

	private void setParameters(List<DatasourceParameterVO> collParameters) {
		this.collParameters = collParameters;
		for (String param : mpParameters.keySet()) {
			mpParameters.put(param, parseParameter(param, mpParameters.get(param)));
		}
	}
	
	private List<Serializable> getKey() {
		DatasourceVO dsvo = getDatasourceVO();
		if (dsvo != null) {
			return Arrays.asList(
				dsvo.getName(),
				sValueFieldName,
				sIdFieldName,
				sDefaultMarkerFieldName,
				baseEntityUID,
				mandatorUID,
				new HashMap<String, Object>(mpParameters));
		}
		return null;		
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof DatasourceBasedCollectableFieldsProvider) {
			DatasourceBasedCollectableFieldsProvider other = (DatasourceBasedCollectableFieldsProvider) obj;
			return LangUtils.equal(getKey(), other.getKey());
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return LangUtils.hashCode(getKey());
	}

	@Override
	public Object getCacheKey() {
		return getKey();
	}
	
	public DatasourceVO getDatasourceVO() {
		return (bSearchmode && dsvoSearch != null)? dsvoSearch : dsvo;
	}

	/**
	 * This provider caches its entries as long as datasource and parameters stay the same.
	 * Perhaps this may not be correct in some cases.
	 * 
	 * Note form Oliver Brausch (25. February 2013):
	 * Actually the provide cache doesn't work at all. For the BMW Project "Konfigurationsplanung" this has fatal
	 * consequences as the same SQL Statements are fired again and again, causing many round-trips and redundant SQL executions.
	 * 
	 * Therefore there is a simple cache implemented which catches any plain SQL Statement that has been executed before.
	 * This cache is default deactivated, in case it should be uses, the static boolean "cachePlainSQLStatements" must
	 * be set to true in advance.
	 * 
	 * Addition (06. July 2013):
	 * There is also the option to enable the cache via SystemParameter("CACHE_VLP_FOR_SEARCH_CB");
	 * 
	 * Addition (26. May 2014):
	 * Cache is always on, SystemParameter no working. It's no SQLCache as the SQL is built and processed in the server only.
	 */
	
	private static Map<Map<String, Object>, DataSourceCache> dataSourceCaches = 
			new ConcurrentHashMap<Map<String,Object>, DataSourceCache>();
	
	private static class DataSourceCache {
		private static Map<Map<String, Object>, List<CollectableValueIdField>> cachedResults = new HashMap<Map<String,Object>, List<CollectableValueIdField>>();
		private static Map<Map<String, Object>, Long> cachedResultsTime = new HashMap<Map<String,Object>, Long>();
		private static void invalidateDataSourceCache() {
			cachedResults.clear();
			cachedResultsTime.clear();
		}
		
		private static List<CollectableValueIdField> checkSQLCache(Map<String, Object> querySignature, int cacheExpiration) {
			Long time = cachedResultsTime.get(querySignature);
			if (time != null && System.currentTimeMillis() - time < cacheExpiration) {
				return cachedResults.get(querySignature);			
			}
			return null;
		}
				
		final private UID valuelistProviderUid;
		final private Map<String, Object> queryParams;
		final private Map<String, Object> querySignature;
		final private String sIdFieldName;
		final private UID baseEntityUID;
		final private UID entityFieldUID;
		final private UID mandatorUID;
		private DataSourceCache(UID valuelistProviderUid, String source, Map<String, Object> queryParams, String sIdFieldName, UID baseEntityUID, UID entityFieldUID, UID mandatorUID) {
			this.valuelistProviderUid = valuelistProviderUid;
			this.queryParams = queryParams;
			this.sIdFieldName = sIdFieldName;
			this.baseEntityUID = baseEntityUID;
			this.entityFieldUID = entityFieldUID;
			this.mandatorUID = mandatorUID;
			this.querySignature = new HashMap<String, Object>(queryParams);
			querySignature.put("vlpid", valuelistProviderUid);
			querySignature.put("source", source);
			querySignature.put("sIdFieldName", sIdFieldName);
			querySignature.put("baseEntityUID", baseEntityUID);
			querySignature.put("mandatorUID", mandatorUID);
		}
		
		private List<CollectableValueIdField> getCachedResult(IDataSource iDataSource) throws CommonBusinessException {
			List<CollectableValueIdField> result = checkSQLCache(querySignature, iDataSource.cacheExpiration());
			if (result != null) return result;
			
			//Synchronizing is switched off because there is always trouble with data. Anyway it was only in order to spare some SQL-Statements not
			//for data consistency
			//synchronized (this) {
				try {
					result = checkSQLCache(querySignature, iDataSource.cacheExpiration());
					if (result == null) {
						
						UID language = DataLanguageContext.getDataUserLanguage() != null ? 
								DataLanguageContext.getDataUserLanguage() : DataLanguageContext.getDataSystemLanguage();

						result = iDataSource.executeQueryForVLP(
								new VLPQuery(valuelistProviderUid)
										.setQueryParams(queryParams)
										.setIdFieldName(sIdFieldName)
										.setBaseEntityUID(baseEntityUID)
										.setEntityFieldUID(entityFieldUID)
										.setMandatorUID(mandatorUID)
										.setLanguageUID(language)
						);

						cachedResults.put(querySignature, result);
						cachedResultsTime.put(querySignature, System.currentTimeMillis());
					}
				} catch (Exception e) {
					if (e instanceof CommonBusinessException) {
						throw (CommonBusinessException) e;
					} else {
						throw new NuclosFatalException(e);
					}
				}					
			//}
			return result;
		}
	}
	
	public static void invalidateDataSourceCache() {
		DataSourceCache.invalidateDataSourceCache();
	}
	
	private List<CollectableValueIdField> getCachedResult(UID valuelistProviderUid, String source, Map<String, Object> queryParams, String sIdFieldName, UID baseEntityUID, UID entityFieldUID, UID mandator) throws CommonBusinessException {
		DataSourceCache dataSourceCache = new DataSourceCache(valuelistProviderUid, source, queryParams, sIdFieldName, baseEntityUID, entityFieldUID, mandator);
		if (!dataSourceCaches.containsKey(dataSourceCache.querySignature)) {
			dataSourceCaches.put(dataSourceCache.querySignature, dataSourceCache);
			
		}
		return dataSourceCaches.get(dataSourceCache.querySignature).getCachedResult(dataSource);
	}
	//End of Plain-SQL Cache

	/**
	 * This provider caches its entries as long as datasource and parameters stay the same.
	 * Perhaps this may not be correct in some cases.
	 */
	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {

		final DatasourceVO dsvoUsed = getDatasourceVO();
		if (dsvoUsed != null) {

			// Create copy and set all parameters not specified so far with null value to complete SQL statement in datasource
			Map<String, Object> queryParams = new HashMap<>(mpParameters);
			for(DatasourceParameterVO dpvo : collParameters) {
				String param = dpvo.getParameter();
				queryParams.putIfAbsent(param, null);
			}

			// refresh list from datasource
			String source = dsvoUsed.getSource();
			UID valuelistProviderUid = dsvoUsed.getId();
			final List result;
			
			UID language = DataLanguageContext.getDataUserLanguage() != null ? 
					DataLanguageContext.getDataUserLanguage() : DataLanguageContext.getDataSystemLanguage();
			
			//boolean isMandator = dataSource.isMandator(baseEntityUID);
			
			result = getCachedResult(valuelistProviderUid, source, queryParams, sIdFieldName, baseEntityUID, entityFieldUID, mandatorUID);

			return (List<CollectableField>) result;

			// The result can be sorted by a SQL 'ORDER BY'.
			//
			// For example for value list provider used as a ComboBox in a subform,
			// the data source defined should contain an order by if needed.
			//
			// It should never been sorted here by name. (tp)
			// Plain wrong:
			// Collections.sort(lstFields);
		}
		return new ArrayList<>();
	}

	/**
	 * @return the mpParameters
	 */
	public Map<String, Object> getValueListParameter() {
		Map<String, Object> params = new HashMap<String, Object>(mpParameters);

		// Assume null for missing params
		for (DatasourceParameterVO param: collParameters) {
			if (!params.containsKey(param.getParameter())) {
				params.put(param.getParameter(), null);
			}
		}

		return params;
	}

	@Override
	public CollectableField getDefaultValue() {
		if (isSupported()) {
			try {
				UID language = DataLanguageContext.getDataUserLanguage() != null ?
						DataLanguageContext.getDataUserLanguage() : DataLanguageContext.getDataSystemLanguage();
				return dataSource.getDefaultValue(dsvo.getId(), sValueFieldName, sIdFieldName, sDefaultMarkerFieldName, mpParameters, baseEntityUID, mandatorUID, language);
			}
			catch (CommonBusinessException e) {
				throw new NuclosFatalException(e);
			}
		}
		return null;
	}

	@Override
	public boolean isSupported() {
		return !StringUtils.isNullOrEmpty(sDefaultMarkerFieldName);
	}
	
	@Override
	public UID getMandator() {
		return mandatorUID;
	}
	
	public UID getBaseEntity() {
		return baseEntityUID;
	}

	@Override
	public String toString() {
		return "DSBasedFieldsProvider[" + getCacheKey() + "] (" + hashCode() + ")";
	}

}
