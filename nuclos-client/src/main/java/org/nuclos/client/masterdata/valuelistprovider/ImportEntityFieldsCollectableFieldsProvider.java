//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.valuelistprovider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.fileimport.ImportMode;
import org.nuclos.common2.KeyEnum;
import org.nuclos.common2.exception.CommonBusinessException;

public class ImportEntityFieldsCollectableFieldsProvider implements NonCacheableCollectableFieldsProvider {

	private static final Logger LOG = Logger.getLogger(ImportEntityFieldsCollectableFieldsProvider.class);
	
	//Do not change this string-identifier!
	public static final String ENTITY_UID = "entityId";
	
	public static final String IMPORT_MODE = "mode";

	// 
	
	private UID entityUid = null;
	private ImportMode mode = null;
	
	/**
	 * @deprecated
	 */
	ImportEntityFieldsCollectableFieldsProvider() {
	}

	public ImportEntityFieldsCollectableFieldsProvider(UID entityUid, String importMode) {
		setParameter(ENTITY_UID, entityUid);
		setParameter(IMPORT_MODE, importMode);
	}
	
	public ImportEntityFieldsCollectableFieldsProvider(UID entityUid, ImportMode importMode) {
		setParameter(ENTITY_UID, entityUid);
		this.mode = importMode;
	}
	
	/**
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String name, Object value) {
		if (name.equals(ENTITY_UID)) {
			entityUid = (UID) value;
		} else if (name.equals(IMPORT_MODE)) {
			if (value != null) {
				this.mode = KeyEnum.Utils.findEnum(ImportMode.class, (String) value);
			}
			else {
				this.mode = null;
			}
		}
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		final List<CollectableField> result = new ArrayList<CollectableField>();
		if (entityUid != null) {
			final MetaProvider mProv = MetaProvider.getInstance();
			final EntityMeta<?> entitymeta = mProv.getEntity(entityUid);
			final Map<UID, FieldMeta<?>> fieldMap = mProv.getAllEntityFieldsByEntity(entityUid); 
			for (UID fieldUid : fieldMap.keySet()) {
				final FieldMeta<?> fieldMeta = fieldMap.get(fieldUid);
				if (!SF.isEOField(entityUid, fieldUid) || fieldUid.equals(SF.PROCESS.getUID(entityUid))) {
					result.add(new CollectableValueIdField(fieldUid, fieldMeta.getFieldName()));
				}
			}
			if (mode != null && mode.equals(ImportMode.DBIMPORT) && entitymeta.isStateModel()) {
				result.add(new CollectableValueIdField(SF.STATE.getUID(entitymeta), SF.STATE.getFieldName()));
			}
			Collections.sort(result);
		}
		return result;
	}
	
}
