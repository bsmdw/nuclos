//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.datatransfer;

import java.awt.event.ActionEvent;

import javax.swing.Action;

import org.nuclos.common.UID;

/**
 * a leased object id and a module.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */

public class MasterDataIdAndEntity<PK> {

	public static final java.awt.datatransfer.DataFlavor dataFlavor = MasterDataIdAndEntityDataFlavor.FLAVOR;

	private final PK oId;
	private final UID entityUid;
	private final UID nodeUid;
	private final String sLabel;

	private final Action actRemove;

	/**
	 * @param oId
	 * @param entityUid
	 * @param sLabel an identifying label
	 */
	public MasterDataIdAndEntity(PK oId, UID entityUid, UID nodeUid, String sLabel) {
		this(oId, entityUid, nodeUid, sLabel, null);
	}

	/**
	 * @param oId
	 * @param entityUid
	 * @param sLabel an identifying label
	 * @param actRemove may be null
	 */
	public MasterDataIdAndEntity(PK oId, UID entityUid, UID nodeUid, String sLabel, Action actRemove) {
		this.oId = oId;
		this.entityUid = entityUid;
		this.nodeUid = nodeUid;
		this.sLabel = sLabel;
		this.actRemove = actRemove;
	}

	public static class MasterDataIdAndEntityDataFlavor extends java.awt.datatransfer.DataFlavor {

		public static final MasterDataIdAndEntityDataFlavor FLAVOR = new MasterDataIdAndEntityDataFlavor();
		
		private MasterDataIdAndEntityDataFlavor() {
			super(MasterDataIdAndEntity.class, null);
		}

	}

	public PK getId() {
		return oId;
	}

	public UID getEntityUid() {
		return entityUid;
	}
	
	public UID getNodeUid() {
		return nodeUid;
	}

	public String getLabel() {
		return sLabel;
	}

	public void removeFromSourceTree() {
		if (actRemove != null)
			actRemove.actionPerformed(new ActionEvent(this, -1, "removeFromSourceTree"));
	}

}	// class MasterDataIdAndEntity
