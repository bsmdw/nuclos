//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.datasource.querybuilder.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Insets;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.plaf.basic.BasicComboBoxEditor;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import org.jfree.util.Log;
import org.nuclos.client.datasource.querybuilder.QueryBuilderIcons;
import org.nuclos.client.datasource.querybuilder.controller.QueryBuilderController;
import org.nuclos.client.datasource.querybuilder.shapes.gui.ConstraintColumn;
import org.nuclos.client.datasource.querybuilder.shapes.gui.TableList;
import org.nuclos.client.ui.event.TableColumnModelAdapter;
import org.nuclos.client.ui.table.CommonJTable;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.TransformerUtils;
import org.nuclos.common.database.query.definition.Column;
import org.nuclos.common.database.query.definition.Constraint;
import org.nuclos.common.database.query.definition.Table;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;

/**
 * Column selection table.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Boris.Sander@novabit.de">Boris Sander</a>
 * @version 01.00.00
 */
public class ColumnSelectionTable extends CommonJTable {
	
	private final Map<String, Integer> mapColumnWidths = new HashMap<String, Integer>();
	private boolean ignoreWidthChange = false;
	private final JLabel textWidthCalculation = new JLabel();

	private static class CheckBoxCellRenderer extends DefaultTableCellRenderer {

		private JCheckBox checkBox = new JCheckBox();

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
				int row, int column) {

			checkBox.setHorizontalAlignment(SwingConstants.CENTER);
			checkBox.setSelected((Boolean) value);
			if (isSelected) {
				checkBox.setBackground(Color.LIGHT_GRAY);
			}
			else {
				checkBox.setBackground(Color.WHITE);
			}
			return checkBox;
		}
	}
	
	
	/**
	 * cell renderer, that sets a distance of 5 pixel to the left side of the text in the cells,
	 * so that the user can read the contents better
	 */
	protected class ColumnSelectionTableCellRenderer extends DefaultTableCellRenderer {

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
				int row, int column) {
			Component comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			
			if (comp instanceof JLabel) {
				JLabel label = (JLabel)comp;
				label.setFont(TableList.TableListCellRenderer.regularFont);
				Insets insets = label.getInsets();
				insets.left = 5;
				label.setBorder(new EmptyBorder(insets));
				return label;
			}

			return comp;
		}
	}


	private static class ComboBoxEditor extends DefaultCellEditor {

		ComboBoxEditor(JComboBox cb) {
			super(cb);
			cb.setEditable(false);
			cb.setFont(TableList.TableListCellRenderer.regularFont);
			setClickCountToStart(1);
		}
	}

	private static class TextFieldEditor extends DefaultCellEditor {
		
		private final JTextField textField;
		
		TextFieldEditor(JTextField textField) {
			super(textField);
			setClickCountToStart(1);
			this.textField = textField;
			this.textField.setFont(TableList.TableListCellRenderer.regularFont);
		}
		
		public JTextField getTextField() {
			return this.textField;
		}
		
		private static class MyDocumentListener implements DocumentListener {
			private final ColumnSelectionTable table;

			MyDocumentListener(ColumnSelectionTable table) {
				this.table = table;
			}

			@Override
			public void changedUpdate(DocumentEvent ev) {
				// this is never called.
				assert false;
			}

			@Override
			public void insertUpdate(final DocumentEvent ev) {
				//if (ev.getLength() > 0) {
					((ColumnSelectionTableModel)table.getModel()).fireTableDataChanged();
				//}				
			}
			@Override
			public void removeUpdate(final DocumentEvent ev) {
				//if (ev.getLength() > 0) {
					((ColumnSelectionTableModel)table.getModel()).fireTableDataChanged();
				//}				
			}
		}
		
	}

	private class HeaderRenderer extends DefaultTableCellRenderer {

		private final JLabel label = new JLabel(QueryBuilderIcons.iconEmpty16);

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
				int row, int column) {
			label.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
			label.setHorizontalAlignment(SwingConstants.CENTER);
			if (ColumnSelectionTable.this.iMarkedColumn == column) {
				label.setBackground(new Color(128, 128, 255));
				label.setIcon(QueryBuilderIcons.iconDown16);
			}
			else {
				label.setIcon(QueryBuilderIcons.iconEmpty16);
				if (isSelected) {
					label.setBackground(Color.GRAY);
				}
				else {
					label.setBackground(Color.LIGHT_GRAY);
				}
			}
			return label;
		}
	}

	private ComboBoxEditor tableEditor;
	private DefaultCellEditor visibleEditor;
	private ComboBoxEditor groupByEditor;
	private ComboBoxEditor orderByEditor;
	private TextFieldEditor conditionEditor;

	protected QueryBuilderController controller;

	protected CheckBoxCellRenderer checkBoxCellRenderer = new CheckBoxCellRenderer();
	protected ColumnSelectionTableCellRenderer cellRenderer = new ColumnSelectionTableCellRenderer();
	protected TableList.TableListCellRenderer columnCellRenderer = new TableList.TableListCellRenderer();
	protected DropTarget dropTarget;
	protected int iMarkedColumn = -1;

	/**
	 * @param controller
	 */
	public ColumnSelectionTable(QueryBuilderController controller) {
		setCellSelectionEnabled(true);
		setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		textWidthCalculation.setFont(TableList.TableListCellRenderer.regularFont);
		getColumnModel().addColumnModelListener(new TableColumnModelAdapter() {
			@Override
			public void columnAdded(TableColumnModelEvent e) {
				restoreColumnWidths();
			}
			@Override
			public void columnRemoved(TableColumnModelEvent e) {
				
			}
			@Override
			public void columnMoved(TableColumnModelEvent e) {
				//restoreColumnWidths();
			}
			@Override
			public void columnMarginChanged(ChangeEvent e) {
				storeColumnWidths();
			}
			
		});
		this.controller = controller;

		tableEditor = new ComboBoxEditor(new JComboBox());

		final JCheckBox chkbxVisible = new JCheckBox();
		visibleEditor = new DefaultCellEditor(chkbxVisible);
		chkbxVisible.setHorizontalAlignment(SwingConstants.CENTER);
		groupByEditor = new ComboBoxEditor(new JComboBox(CollectionUtils.transform(Arrays.asList(DatasourceVO.GroupBy.values()), TransformerUtils.toStringTransformer()).toArray()));
		orderByEditor = new ComboBoxEditor(new JComboBox(CollectionUtils.transform(Arrays.asList(DatasourceVO.OrderBy.values()), TransformerUtils.toStringTransformer()).toArray()));
		conditionEditor = new TextFieldEditor(new JTextField());
		conditionEditor.getTextField().getDocument().addDocumentListener(new TextFieldEditor.MyDocumentListener(this));

		((JComboBox) tableEditor.getComponent()).addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent ev) {
				if (ev.getStateChange() == ItemEvent.SELECTED) {
					final Table table = (Table) ev.getItem();
					if (table != null) {
//			       initColumns(table);
					}
				}
			}
		});

		dropTarget = new DropTarget(this, DnDConstants.ACTION_LINK, controller.getDropTargetListener());

		getTableHeader().setDefaultRenderer(new HeaderRenderer());
	}

	/**
	 *
	 * @param dataModel
	 */
	@Override
	public void setModel(TableModel dataModel) {
		setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		super.setModel(dataModel);
	}

	/**
	 *
	 * @param row
	 * @param column
	 * @return table cell editor
	 */
	@Override
	public TableCellEditor getCellEditor(int row, final int column) {
		TableCellEditor result = null;
		switch (row) {
			case ColumnSelectionTableModel.TABLE_ROW:
				result = tableEditor;
				break;
			case ColumnSelectionTableModel.COLUMN_ROW:
				final Table table = (Table) getValueAt(ColumnSelectionTableModel.TABLE_ROW, column);
				final JComboBox cb = new JComboBox(new DefaultComboBoxModel(getColumns(table).toArray()));
				cb.setFont(TableList.TableListCellRenderer.regularFont);
				cb.setRenderer(new TableList.TableListCellRenderer());
				final TableCellEditor cellEditor = new DefaultCellEditor(cb);
				cb.setEditable(true);
				cb.setEditor(new BasicComboBoxEditor() {
					@Override
					public void focusGained(FocusEvent e) {
						super.focusGained(e);
						Object item = cb.getEditor().getItem();
						if (item instanceof Column) {
							Column col = (Column)item;
							if (LangUtils.equal(((JTextField)getEditorComponent()).getText(), col.getFieldNameIfAny())
									&& !col.isExpression()) {
								final String sText = col.getNameWithAlias();
								setItem(sText);
							}
						}
					}
					@Override
					public void focusLost(FocusEvent e) {
						super.focusLost(e);
						Object item = cb.getEditor().getItem();
						if (item instanceof String) {
							String sItem = (String) item;
							sItem = sItem.trim();
							for (int i = 0; i < cb.getItemCount(); i++) {
								Object cbItem = cb.getItemAt(i);
								if (cbItem instanceof Column) {
									Column col = (Column)cbItem;
									if (LangUtils.equal(col.getNameWithAlias(), sItem)) {
										cb.setSelectedIndex(i);
										setValueAt(col, ColumnSelectionTableModel.COLUMN_ROW, column);
										break;
									}
								}
							}
						}
					}
				});
				final BasicComboBoxEditor cbEditor = (BasicComboBoxEditor) cb.getEditor();
				final JTextField tfEditor = (JTextField) cbEditor.getEditorComponent();
				tfEditor.addFocusListener(cbEditor);
				result = cellEditor;
				break;
			case ColumnSelectionTableModel.VISIBLE_ROW:
				result = visibleEditor;
				break;
			case ColumnSelectionTableModel.GROUPBY_ROW:
				result = groupByEditor;
				break;
			case ColumnSelectionTableModel.ORDERBY_ROW:
				result = orderByEditor;
				break;
			default:
				result = conditionEditor;
		}
		return result;
	}

	@Override
	public TableCellRenderer getCellRenderer(int row, int column) {
		if (row == ColumnSelectionTableModel.VISIBLE_ROW) {
			return checkBoxCellRenderer;
		} else if (row == ColumnSelectionTableModel.COLUMN_ROW) {
			return columnCellRenderer;
		}
		return cellRenderer;//super.getCellRenderer(row, column);
	}

	/**
	 *
	 * @param tables
	 */
	public void updateTables(List<Table> tables) {
		final JComboBox comboBox = ((JComboBox) tableEditor.getTableCellEditorComponent(this, null, false, 0, 0));
		Collections.sort(tables, new Comparator<Table>() {
			@Override
			public int compare(Table t0, Table t1) {
				return StringUtils.compareIgnoreCase(t0.toString(), t1.toString());
			}
		});
		comboBox.setModel(new DefaultComboBoxModel(tables.toArray()));
	}

	/**
	 *
	 */
	private List<ConstraintColumn> getColumns(Table table) {
		if (table != null) {
			final Map<String, ConstraintColumn> mpconstraintcolumn = CollectionUtils.newHashMap();
			final List<ConstraintColumn> lstconstraintcolumn = new ArrayList<ConstraintColumn>();
			for (Column column : table.getColumns()) {
				final ConstraintColumn constraintcolumn = new ConstraintColumn(column.getTable(), column.getName(), column.getType(),
					column.getLength(), column.getPrecision(), column.getScale(), column.isNullable());
				mpconstraintcolumn.put(constraintcolumn.getName().toUpperCase(), constraintcolumn);
				lstconstraintcolumn.add(constraintcolumn);
			}

			for (Constraint constraint : (table.getConstraints())) {
				for (Column column : constraint.getColumns()) {
					final ConstraintColumn constraintcolumn = mpconstraintcolumn.get(column.getName().toUpperCase());
					if (constraintcolumn != null) {
						constraintcolumn.addConstraint(constraint);
					}
				}
			}
			
			Collections.sort(lstconstraintcolumn);
			return lstconstraintcolumn;
		}
		return Collections.emptyList();
	}

	/**
	 *
	 * @return drop target
	 */
	@Override
	public DropTarget getDropTarget() {
		return dropTarget;
	}

	/**
	 *
	 * @param dtde
	 */
	public void dragOver(DropTargetDragEvent dtde) {
		final Point2D p = dtde.getLocation();
		final int iOldIndex = iMarkedColumn;
		iMarkedColumn = getColumnModel().getColumnIndexAtX((int) p.getX());
		if (iOldIndex != iMarkedColumn) {
			getTableHeader().repaint();
		}
	}

	/**
	 *
	 * @param dtde
	 */
	public void drop(DropTargetDropEvent dtde) {
		iMarkedColumn = -1;
		getTableHeader().repaint();
	}

	/**
	 *
	 * @return marked index
	 */
	public int getMarkedIndex() {
		return iMarkedColumn;
	}

	/**
	 *
	 * @return next available index
	 */
	public int getNextAvailableIndex() {
		return ((ColumnSelectionTableModel) getModel()).getColumnCount();
		/*
		for (int i = 0; i < ColumnSelectionTableModel.COLUMN_COUNT; i++) {
			final ColumnEntry entry = ((ColumnSelectionTableModel) getModel()).getColumn(i);
			if (entry.getTable() == null && entry.getColumn() == null) {
				return i;
			}
		}
		return -1; // unlikely
		 */
	}
	
	private void storeColumnWidths() {
		try {
			if (ignoreWidthChange) {
				return;
			}
			for (int i = 0; i < getColumnCount(); i++) {
				TableColumn tableColumn = getColumnModel().getColumn(i);
				int width = tableColumn.getWidth();
				ColumnSelectionTableModel tablemodel = (ColumnSelectionTableModel) getModel();
				ColumnEntry ce = tablemodel.getColumn(i);
				if (ce != null && ce.getColumn() != null) {
					mapColumnWidths.put(ce.getColumn().toString(), width);
				}
			}
		} catch (Exception ex) {
			Log.error(ex.getMessage(), ex);
		}
	} 
	
	private void restoreColumnWidths() {
		try {
			ignoreWidthChange = true;
			for (int i = 0; i < getColumnCount(); i++) {
				TableColumn tableColumn = getColumnModel().getColumn(i);
				ColumnSelectionTableModel tablemodel = (ColumnSelectionTableModel) getModel();
				ColumnEntry ce = tablemodel.getColumn(i);
				Integer width = null;
				if (ce != null && ce.getColumn() != null) {
					width = mapColumnWidths.get(ce.getColumn().toString());
				}
				if (width == null) {
					if (ce != null && ce.getColumn() != null) {
						int w1 = SwingUtilities.computeStringWidth(textWidthCalculation.getFontMetrics(
								textWidthCalculation.getFont()), 
									ce.getColumn().toString()) + 22;
						int w2 = SwingUtilities.computeStringWidth(textWidthCalculation.getFontMetrics(
								textWidthCalculation.getFont()), 
									LangUtils.defaultIfNull(ce.getAlias(), " ")) + 10;
						int w3 = SwingUtilities.computeStringWidth(textWidthCalculation.getFontMetrics(
								textWidthCalculation.getFont()), 
									LangUtils.defaultIfNull(ce.getColumn().getTable(), " ").toString()) + 10;
						width = Math.max(Math.max(w1, w2), w3);
						width = Math.min(240, width);
					}
					if (width == null) {
						width = 120;
					}
				}
				tableColumn.setPreferredWidth(width);
			}
		} catch (Exception ex) {
			Log.error(ex.getMessage(), ex);
		} finally {
			ignoreWidthChange = false;
		}
	}

}	// class ColumnSelectionTable
