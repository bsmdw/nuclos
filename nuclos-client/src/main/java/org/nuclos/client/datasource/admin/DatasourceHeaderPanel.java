//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.datasource.admin;

import java.awt.BorderLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.ui.collect.CollectableComponentsProvider;
import org.nuclos.client.ui.collect.DefaultCollectableComponentsProvider;
import org.nuclos.client.ui.collect.component.CollectableCheckBox;
import org.nuclos.client.ui.collect.component.CollectableComboBox;
import org.nuclos.client.ui.collect.component.CollectableTextField;
import org.nuclos.client.ui.collect.component.LabeledCollectableComponentWithVLP;
import org.nuclos.client.ui.util.TableLayoutBuilder;
import org.nuclos.client.valuelistprovider.EntityCollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common2.SpringLocaleDelegate;

import info.clearthought.layout.TableLayout;

/**
 * Header for the edit panels.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */

public class DatasourceHeaderPanel extends JPanel {

	private final JPanel pnlTextFields = new JPanel();

	private final CollectableTextField clcttfName;
	private final LabeledCollectableComponentWithVLP clbxEntity;
	private final CollectableTextField clcttfDescription;
	private final LabeledCollectableComponentWithVLP clbxParentEntity;
	private final JTextField tfDetailsearchdescriptionEn;
	private final JTextField tfDetailsearchdescriptionDe;
	private final JLabel lDetailsearchdescriptionEn;
	private final JLabel lDetailsearchdescriptionDe;
	private final JLabel lWithRuleClass;
	private final CollectableCheckBox clctchkbxWithRuleClass;

	public DatasourceHeaderPanel(
			CollectableEntityField clctefName,
			CollectableEntityField clctefEntity,
			CollectableEntityField clctefDescription, 
			CollectableEntityField clctefParentEntity,
			boolean withDetailsearchDescription,
			CollectableEntityField clctefWithRuleClass) {
		super(new BorderLayout());
		final SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();
		clcttfName = new CollectableTextField(clctefName);
		if (clctefEntity != null) {
			clbxEntity = new CollectableComboBox(clctefEntity);
			clbxEntity.setValueListProvider(new EntityCollectableFieldsProvider());
			clbxEntity.getValueListProvider().setParameter("menupath", "optional");
	        clbxEntity.refreshValueList(false);
		} else {
			clbxEntity = null;
		}
		clcttfDescription = new CollectableTextField(clctefDescription);
		if (clctefParentEntity != null) {
			clbxParentEntity = new CollectableComboBox(clctefParentEntity);
			clbxParentEntity.setValueListProvider(new EntityCollectableFieldsProvider());
			clbxParentEntity.getValueListProvider().setParameter("menupath", "optional");
			clbxParentEntity.refreshValueList(false);
		} else {
			clbxParentEntity = null;
		}
		if (withDetailsearchDescription == true) {
			tfDetailsearchdescriptionEn = new JTextField();
			tfDetailsearchdescriptionDe = new JTextField();
			lDetailsearchdescriptionEn = new JLabel();
			lDetailsearchdescriptionDe = new JLabel();
		} else {
			tfDetailsearchdescriptionEn = null;
			tfDetailsearchdescriptionDe = null;
			lDetailsearchdescriptionEn = null;
			lDetailsearchdescriptionDe = null;
		}
		if (clctefWithRuleClass != null) {
			lWithRuleClass = new JLabel(localeDelegate.getMsg("entityField.withRuleClass.label"));
			lWithRuleClass.setToolTipText(localeDelegate.getMsg("entityField.withRuleClass.desc"));
			clctchkbxWithRuleClass = new CollectableCheckBox(clctefWithRuleClass);
			clctchkbxWithRuleClass.setToolTipText(localeDelegate.getMsg("entityField.withRuleClass.desc"));
			lWithRuleClass.setLabelFor(clctchkbxWithRuleClass.getJCheckBox());
		} else {
			lWithRuleClass = null;
			clctchkbxWithRuleClass = null;
		}
		
		clcttfName.setLabelText(localeDelegate.getMessage("DatasourceHeaderPanel.3","Name"));
		clcttfName.setToolTipText(localeDelegate.getMessage("DatasourceHeaderPanel.4","Name des Reports/Formulars"));
		clcttfName.setColumns(50);
		
		if (clctefEntity != null) {
			clbxEntity.setLabelText(clctefEntity.getLabel());
			clbxEntity.setToolTipText(clctefEntity.getDescription());
		}
		
		clcttfDescription.setLabelText(localeDelegate.getMessage("DatasourceHeaderPanel.1","Beschreibung"));
		clcttfDescription.setToolTipText(localeDelegate.getMessage("DatasourceHeaderPanel.2","Beschreibung des Reports/Formulars"));
		
		if (clctefParentEntity != null) {
			clbxParentEntity.setLabelText(clctefParentEntity.getLabel());
			clbxParentEntity.setToolTipText(clctefParentEntity.getDescription());
		}
		
		if (withDetailsearchDescription == true) {
			lDetailsearchdescriptionDe.setText(localeDelegate.getMessage("DatasourceHeaderPanel.5","Beschreibung Detailsuche (DE)"));
			tfDetailsearchdescriptionDe.setToolTipText(localeDelegate.getMessage("DatasourceHeaderPanel.6","Beschreibung wird in Ergebnisliste der Detailsuche angezeigt"));
			lDetailsearchdescriptionEn.setText(localeDelegate.getMessage("DatasourceHeaderPanel.7","Beschreibung Detailsuche (EN)"));
			tfDetailsearchdescriptionEn.setToolTipText(localeDelegate.getMessage("DatasourceHeaderPanel.8","Beschreibung wird in Ergebnisliste der Detailsuche angezeigt"));
		}
		
		this.add(pnlTextFields, BorderLayout.CENTER);
		pnlTextFields.setBorder(BorderFactory.createEmptyBorder(5, 5, 10, 5));
		
		TableLayoutBuilder tbllay = new TableLayoutBuilder(pnlTextFields).columns(TableLayout.PREFERRED, 5.0, 300.0, 5.0, TableLayout.PREFERRED, 5.0, 300.0, TableLayout.FILL);
		
		tbllay.newRow().add(clcttfName.getJLabel()).skip().add(this.clcttfName.getJTextField(), 5);
		if (clctefEntity != null) {
			tbllay.newRow(2.0).newRow().add(clbxEntity.getJLabel()).skip().add(clbxEntity.getJComboBox());
		}
		if (clctefParentEntity != null) {
			if (clctefEntity == null) {
				tbllay.newRow(2.0).newRow();
			} else {
				tbllay.skip();
			}
			tbllay.add(clbxParentEntity.getJLabel()).skip().add(clbxParentEntity.getJComboBox());
		}
		tbllay.newRow(2.0).newRow().add(clcttfDescription.getJLabel()).skip().add(this.clcttfDescription.getJTextField(), 6);
		if (withDetailsearchDescription == true) {
			tbllay.newRow(2.0).newRow().add(lDetailsearchdescriptionDe).skip().add(this.tfDetailsearchdescriptionDe, 6);
			tbllay.newRow(2.0).newRow().add(lDetailsearchdescriptionEn).skip().add(this.tfDetailsearchdescriptionEn, 6);
		}
		if (clctefWithRuleClass != null) {
			if (SecurityCache.getInstance().isSuperUser()) {
				// only visible for Superusers...
				tbllay.newRow(2.0).newRow().add(lWithRuleClass).skip().add(this.clctchkbxWithRuleClass.getJCheckBox(), 6);
			}
		}
	}

	public CollectableComponentsProvider newCollectableComponentsProvider() {
		final DefaultCollectableComponentsProvider result = new DefaultCollectableComponentsProvider(clcttfName, clcttfDescription);
		if (clbxEntity != null) {
			result.addCollectableComponent(clbxEntity);
		}
		if (clbxParentEntity != null) {
			result.addCollectableComponent(clbxParentEntity);
		}
		if (clctchkbxWithRuleClass != null) {
			result.addCollectableComponent(clctchkbxWithRuleClass);
		}
		return result;
	}
	
	CollectableTextField getNameField() {
		return clcttfName;
	}
	
	CollectableTextField getDescriptionField() {
		return clcttfDescription;
	}
	
	JTextField getDetailsearchdescriptionDeField() {
		return tfDetailsearchdescriptionDe;
	}
	
	JTextField getDetailsearchdescriptionEnField() {
		return tfDetailsearchdescriptionEn;
	}
	
	LabeledCollectableComponentWithVLP getEntityComboBox() {
		return clbxEntity;
	}
	
	LabeledCollectableComponentWithVLP getParentEntityComboBox() {
		return clbxParentEntity;
	}

	CollectableCheckBox getWithRuleClassCheckBox() {
		return clctchkbxWithRuleClass;
	}


}	// class StateModelHeaderPanel
