//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.datasource;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.database.query.definition.Schema;
import org.nuclos.common.database.query.definition.Table;
import org.nuclos.common.querybuilder.NuclosDatasourceException;
import org.nuclos.common.report.ejb3.DatasourceFacadeRemote;
import org.nuclos.common.report.valueobject.CalcAttributeVO;
import org.nuclos.common.report.valueobject.ChartVO;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.DynamicEntityVO;
import org.nuclos.common.report.valueobject.DynamicTasklistVO;
import org.nuclos.common.report.valueobject.RecordGrantVO;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common.valuelistprovider.IDataSource;
import org.nuclos.common.valuelistprovider.VLPQuery;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;

/**
 * The Delegate for DatasourceFacadeBean. <br>
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author <a href="mailto:Lars.Rueckemann@novabit.de">Lars Rueckemann</a>
 * @version 01.00.00
 */
// @Component
public class DatasourceDelegate implements IDataSource {
	
	private static DatasourceDelegate INSTANCE;
	
	//

	private DatasourceFacadeRemote dataSourceFacade;

	DatasourceDelegate() {
		INSTANCE = this;
	}

	private boolean booleanRepresentationInSQLForNullRead = false;
	private Object booleanRepresentationInSQLForNull = null;

	private boolean booleanRepresentationInSQLForTrueRead = false;
	private Object booleanRepresentationInSQLForTrue = null;

	private boolean booleanRepresentationInSQLForFalseRead = false;
	private Object booleanRepresentationInSQLForFalse = null;

	public static DatasourceDelegate getInstance() {
		if (INSTANCE == null) {
			throw new IllegalStateException("too early");
		}
		return INSTANCE;
	}
	
	public final void setDatasourceFacadeRemote(DatasourceFacadeRemote datasourceFacadeRemote) {
		this.dataSourceFacade = datasourceFacadeRemote;
	}

	private DatasourceFacadeRemote getDatasourceFacade() throws NuclosFatalException {
		if (dataSourceFacade == null) {
			throw new IllegalStateException("too early");
		}
		return dataSourceFacade;
	}

	public List<DatasourceVO> getUsagesForDatasource(UID iDatasourceId) throws CommonFinderException, CommonPermissionException {
		try {
			return this.getDatasourceFacade().getUsagesForDatasource(iDatasourceId);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * get a list of DatasourceVO which uses the datasource
	 *
	 * @param datasourceVO
	 *            could also be an INSTANCE of <code>DynamicEntityVO</code> or
	 *            <code>ValuelistProviderVO</code>
	 * @return
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	public List<DatasourceVO> getUsagesForDatasource(DatasourceVO datasourceVO) throws CommonFinderException, CommonPermissionException {
		try {
			return this.getDatasourceFacade().getUsagesForDatasource(datasourceVO);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	public List<DatasourceVO> getUsingByForDatasource(UID iDatasourceId) throws CommonFinderException, CommonPermissionException {
		try {
			return this.getDatasourceFacade().getUsingByForDatasource(iDatasourceId);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	public void setInvalid(final List<DatasourceVO> lstdatasourcevo) throws CommonFinderException, CommonPermissionException, CommonStaleVersionException, CommonValidationException,
			NuclosBusinessRuleException {
		try {
			for (DatasourceVO datasourcevo : lstdatasourcevo) {
				datasourcevo.setValid(false);
				this.getDatasourceFacade().modify(datasourcevo);
			}
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * Get datasource with the specified name.
	 */
	@Override
	public DatasourceVO getDatasourceByName(String sDataSource) throws CommonBusinessException {
		try {
			return getDatasourceFacade().getDatasourceByName(sDataSource);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * Get dynamic entity with the specified name.
	 */
	public DynamicEntityVO getDynamicEntityByName(UID dynamicEntityUid) throws CommonBusinessException {
		try {
			return getDatasourceFacade().getDynamicEntity(dynamicEntityUid);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}
	
	public List<DatasourceParameterVO> getParameters(String sDataSource) throws CommonBusinessException {
		try {
			DatasourceVO dsvo = getDatasourceByName(sDataSource);
			return this.getDatasourceFacade().getParameters(dsvo.getId());
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * @return collection of all DynamicEntityVOs
	 */
	public Collection<DynamicEntityVO> getAllDynamicEntities() {
		try {
			return getDatasourceFacade().getDynamicEntities();
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}
	
	/**
	 * get dynamic entity value object
	 *
	 * @param iDynamicEntityId
	 *            primary key of dynamic entity
	 * @return DynamicEntityVO
	 */
	public DynamicEntityVO getDynamicEntity(UID iDynamicEntityId) throws CommonPermissionException {
		try {
			return getDatasourceFacade().getDynamicEntity(iDynamicEntityId);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		} catch (CommonFinderException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * @return collection of all ChartVOs
	 */
	public Collection<ChartVO> getAllCharts() {
		try {
			return getDatasourceFacade().getCharts();
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}
	
	/**
	 * get chart value object
	 * 
	 * @param iChartId
	 *            primary key of chart
	 * @return ChartVO
	 */
	public ChartVO getChart(UID iChartId) throws CommonPermissionException {
		try {
			return getDatasourceFacade().getChart(iChartId);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		} catch (CommonFinderException ex) {
			throw new CommonFatalException(ex);
		}
	}
	
	/**
	 * @return collection of all ValuelistProviderVOs
	 */
	public Collection<ValuelistProviderVO> getAllValuelistProvider() throws CommonPermissionException {
		try {
			return getDatasourceFacade().getValuelistProvider();
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * get valuelist provider value object
	 *
	 * @param valuelistProviderUid
	 *            primary key of valuelist provider
	 * @return ValuelistProviderVO
	 */
	@Override
	public ValuelistProviderVO getValuelistProvider(UID valuelistProviderUid) throws CommonPermissionException {
		try {
			return getDatasourceFacade().getValuelistProvider(valuelistProviderUid);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		} catch (CommonFinderException ex) {
			throw new CommonFatalException(ex);
		}
	}

	public ValuelistProviderVO getValuelistProviderByName(String sDataSource) throws CommonBusinessException {
		try {
			for (ValuelistProviderVO vlpVO : getAllValuelistProvider()) {
				if (vlpVO.getName().equals(sDataSource))
					return vlpVO;
			}
			return null;
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}
	
	/**
	 * @return collection of all ValuelistProviderVOs
	 */
	public Collection<CalcAttributeVO> getAllCalcAttributes() throws CommonPermissionException {
		try {
			return getDatasourceFacade().getCalcAttributes();
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}
	
	@Override
	public CalcAttributeVO getCalcAttribute(UID calcAttributeUid) throws CommonPermissionException {
		try {
			return getDatasourceFacade().getCalcAttribute(calcAttributeUid);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		} catch (CommonFinderException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * @param sDatasourceXML
	 * @return the parameters of the datasource
	 */
	@Override
	public List<DatasourceParameterVO> getParametersFromXML(String sDatasourceXML) throws CommonBusinessException {
		try {
			return getDatasourceFacade().getParametersFromXML(sDatasourceXML);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * @param dsvo
	 * @return the newly created datasource
	 */
	public DatasourceVO create(DatasourceVO dsvo, IDependentDataMap dependants, List<UID> lstUsedDatasources) throws CommonBusinessException {
		try {
			return getDatasourceFacade().create(dsvo, dependants, lstUsedDatasources);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * @param datasourceVO
	 */
	public void remove(DatasourceVO datasourceVO) throws CommonBusinessException {
		try {
			getDatasourceFacade().remove(datasourceVO);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * @param dsvo
	 * @return the modified datasource
	 */
	public DatasourceVO modify(DatasourceVO dsvo, IDependentDataMap dependants, List<UID> lstUsedDatasources) throws CommonBusinessException {
		try {
			return getDatasourceFacade().modify(dsvo, null, lstUsedDatasources);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * execute a datasource by datasource id
	 *
	 * @param datasourceUID
	 * @param mpParams
	 * @return
	 * @throws NuclosBusinessException
	 */
	public ResultVO executeQuery(UID datasourceUID, Map<String, Object> mpParams, Integer iMaxRowCount, UID languageUID) throws CommonBusinessException {
		try {
			return getDatasourceFacade().executeQuery(datasourceUID, mpParams, iMaxRowCount, languageUID);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * execute a datasource by datasource xml
	 *
	 * @param sDatasourceXML
	 * @param mpParams
	 * @param iMaxRowCount
	 * @return
	 * @throws CommonBusinessException
	 */
	public ResultVO executeQueryForDataSource(DatasourceVO datasourcevo, Map<String, Object> mpParams, UID entity, Integer iMaxRowCount, UID languageUID) throws CommonBusinessException {
		try {
			return getDatasourceFacade().executeQueryForDataSource(datasourcevo, mpParams, entity, iMaxRowCount, languageUID);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}
	
	/**
	 * execute a datasource by datasource xml
	 */
	public List<CollectableValueIdField> executeQueryForVLP(VLPQuery query) throws CommonBusinessException {
		try {
			return getDatasourceFacade().executeQueryForVLP(query);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	public List<String> getColumns(String sql) {
		try {
			return getDatasourceFacade().getColumns(sql);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}
	
	@Override
	public List<String> getColumnsFromVLP(UID valuelistProviderUid, Class clazz) throws CommonBusinessException {
		try {
			return getDatasourceFacade().getColumnsFromVLP(valuelistProviderUid, clazz);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}


	public String createSQL(DatasourceVO datasourcevo) throws NuclosBusinessException {
		try {
			return getDatasourceFacade().createSQL(datasourcevo);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * @return collection of all datasources the current user has access to
	 */
	public Collection<DatasourceVO> getAllDatasources() throws CommonPermissionException {
		try {
			return getDatasourceFacade().getDatasources();
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * @return collection of all datasources the current user is the owner og
	 */
	public Collection<DatasourceVO> getOwnDatasources() {
		try {
			return getDatasourceFacade().getDatasourcesForCurrentUser();
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * Get datasource with the specified id.
	 *
	 * @param iId
	 * @return the datasource with the specified id.
	 */
	public DatasourceVO getDatasource(UID iId) throws CommonBusinessException {
		try {
			return getDatasourceFacade().get(iId);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 *
	 * @return
	 */
	public Schema getSchemaTables(UID dsEntity) throws CommonPermissionException {
		Schema result = null;
		try {
			result = getDatasourceFacade().getSchemaTables(dsEntity);
		}
		catch (RuntimeException e) {
			throw new NuclosFatalException(e);
		}
		return result;
	}

	/**
	 *
	 * @param table
	 * @return
	 */
	public Table getSchemaColumns(Table table, UID dsEntity) throws CommonPermissionException {
		try {
			return getDatasourceFacade().getSchemaColumns(table, dsEntity);
		}
		catch (RuntimeException e) {
			throw new NuclosFatalException(e);
		}
	}

	/**
	 * @return collection of all RecordGrantVOs
	 */
	public Collection<RecordGrantVO> getAllRecordGrant() throws CommonPermissionException {
		try {
			return getDatasourceFacade().getRecordGrant();
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * get RecordGrant value object
	 *
	 * @param recordGrantUid
	 *            primary key of RecordGrant
	 * @return RecordGrantVO
	 */
	public RecordGrantVO getRecordGrant(UID recordGrantUid) throws CommonPermissionException {
		try {
			return getDatasourceFacade().getRecordGrant(recordGrantUid);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	public Collection<DynamicTasklistVO> getAllDynamicTasklists() throws CommonPermissionException {
		try {
			return getDatasourceFacade().getDynamicTasklists();
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	public DynamicTasklistVO getDynamicTasklist(UID dynamicTasklistUid) throws CommonPermissionException {
		try {
			return getDatasourceFacade().getDynamicTasklist(dynamicTasklistUid);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	public Set<String> getDynamicTasklistAttributes(UID dtlId) throws CommonPermissionException, NuclosDatasourceException {
		return getDatasourceFacade().getDynamicTasklistAttributes(dtlId);
	}

	public ResultVO getDynamicTasklistData(UID dtlId) throws CommonPermissionException, NuclosDatasourceException {
		return getDatasourceFacade().getDynamicTasklistData(dtlId);
	}

	@Override
	public CollectableField getDefaultValue(UID datasource, String valuefield, String idfield, String defaultfield, Map<String, Object> params, UID baseEntityUID, UID mandatorUID, UID languageUID) throws CommonBusinessException {
		return getDatasourceFacade().getDefaultValue(datasource, valuefield, idfield, defaultfield, params, baseEntityUID, mandatorUID, languageUID);
	}

	@Override
	public int cacheExpiration() {
		return ClientParameterProvider.getInstance().getIntValue(ParameterProvider.KEY_VLP_RESULTCACHE_EXPIRATION, 4000);
	}

	@Override
	public boolean isMandator(UID baseEntityUID) {
		try {
			return getDatasourceFacade().isMandator(baseEntityUID);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}
	
	public Object getBooleanRepresentationInSQL(Boolean b) {
		if (b == null) {
			if (!booleanRepresentationInSQLForNullRead) {
				booleanRepresentationInSQLForNull = getDatasourceFacade().getBooleanRepresentationInSQL(null);
				booleanRepresentationInSQLForNullRead = true;
			}
			return booleanRepresentationInSQLForNull;
		} else if (b) {
			if (!booleanRepresentationInSQLForTrueRead) {
				booleanRepresentationInSQLForTrue = getDatasourceFacade().getBooleanRepresentationInSQL(true);
				booleanRepresentationInSQLForTrueRead = true;
			}
			return booleanRepresentationInSQLForTrue;
		} else {
			if (!booleanRepresentationInSQLForFalseRead) {
				booleanRepresentationInSQLForFalse = getDatasourceFacade().getBooleanRepresentationInSQL(false);
				booleanRepresentationInSQLForFalseRead = true;
			}
			return booleanRepresentationInSQLForFalse;
		}
	}

} // class DatasourceDelegate
