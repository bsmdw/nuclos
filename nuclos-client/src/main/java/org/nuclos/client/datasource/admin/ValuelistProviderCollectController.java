//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.datasource.admin;

import java.awt.Component;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.datasource.querybuilder.QueryBuilderEditor;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.collect.CollectPanel;
import org.nuclos.common.CollectableEntityFieldWithEntity;
import org.nuclos.common.E;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.exception.CollectableValidationException;
import org.nuclos.common.database.query.definition.QueryTable;
import org.nuclos.common.masterdata.CollectableMasterDataEntity;
import org.nuclos.common.report.ejb3.DatasourceFacadeRemote;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common2.IOUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <code>CollectController</code> for entity "valuelistProvider".
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Boris.Sander@novabit.de">Boris Sander</a>
 * @version 01.00.00
 */
public class ValuelistProviderCollectController extends AbstractDatasourceCollectController<ValuelistProviderVO> {
	private final static Logger LOG = LoggerFactory.getLogger(ValuelistProviderCollectController.class);
	
	// these names must not be used in the parameters section of the valuelist provider
	public static String[] INTERNAL_IDENTIFIERS = {""};
	
	// former Spring injection
	
	private DatasourceFacadeRemote datasourceFacadeRemote;
	
	// end of former Spring injection
	
	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton} 
	 * to get an instance.
	 * 
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	@Deprecated
	public ValuelistProviderCollectController(MainFrameTab tabIfAny) {
		super(new CollectableMasterDataEntity(E.VALUELISTPROVIDER), tabIfAny);
		setDatasourceFacadeRemote(SpringApplicationContextHolder.getBean(DatasourceFacadeRemote.class));
		
		CollectableMasterDataEntity clctEntity = new CollectableMasterDataEntity(E.VALUELISTPROVIDER);
		initializeDatasourceCollectController(
			new DatasourceEditPanel(this, 
				new CollectableEntityFieldWithEntity(clctEntity, E.VALUELISTPROVIDER.name.getUID()),
				null,
				new CollectableEntityFieldWithEntity(clctEntity, E.VALUELISTPROVIDER.description.getUID()),
				null,
				true, null));
	}
	
	@Override
	protected void initialize(CollectPanel<UID, CollectableDataSource<ValuelistProviderVO>> pnlCollect) {
		super.initialize(pnlCollect);
		pnlEdit.getHeader().getDetailsearchdescriptionDeField().getDocument().addDocumentListener(new HeaderPanelDocumentListener());
		pnlEdit.getHeader().getDetailsearchdescriptionEnField().getDocument().addDocumentListener(new HeaderPanelDocumentListener());
	}
	
	@Override
	public void refreshCurrentCollectable(boolean withMultiThreader) throws CommonBusinessException {
		super.refreshCurrentCollectable(withMultiThreader);
		boolean bDetailsChangedIgnored = isDetailsChangedIgnored();
		setDetailsChangedIgnored(true);
		CollectableDataSource<ValuelistProviderVO> clct = readSelectedCollectable();
		pnlEdit.getHeader().getDetailsearchdescriptionDeField().setText(clct.getDatasourceVO().getDetailsearchdescription(Locale.GERMAN));
		pnlEdit.getHeader().getDetailsearchdescriptionEnField().setText(clct.getDatasourceVO().getDetailsearchdescription(Locale.ENGLISH));
		setDetailsChangedIgnored(bDetailsChangedIgnored);
	}
	
	@Override
	protected void readValuesFromEditPanel(CollectableDataSource<ValuelistProviderVO> clct, boolean bSearchTab) throws CollectableValidationException {
		super.readValuesFromEditPanel(clct, bSearchTab);
		clct.getDatasourceVO().setDetailsearchdescription(pnlEdit.getHeader().getDetailsearchdescriptionDeField().getText(), Locale.GERMAN);
		clct.getDatasourceVO().setDetailsearchdescription(pnlEdit.getHeader().getDetailsearchdescriptionEnField().getText(), Locale.ENGLISH);
	}
	
	@Override
	protected void unsafeFillDetailsPanel(CollectableDataSource<ValuelistProviderVO> clct) throws CommonBusinessException {
		super.unsafeFillDetailsPanel(clct);
		if (!this.getCollectState().isDetailsModeNew()) {
			if (this.getSelectedCollectable() != null) {
				clct = readSelectedCollectable();
				pnlEdit.getHeader().getDetailsearchdescriptionDeField().setText(clct.getDatasourceVO().getDetailsearchdescription(Locale.GERMAN));
				pnlEdit.getHeader().getDetailsearchdescriptionEnField().setText(clct.getDatasourceVO().getDetailsearchdescription(Locale.ENGLISH));
			}
		} else {
			pnlEdit.getHeader().getDetailsearchdescriptionDeField().setText(null);
			pnlEdit.getHeader().getDetailsearchdescriptionEnField().setText(null);
		}
	}
	
	final void setDatasourceFacadeRemote(DatasourceFacadeRemote datasourceFacadeRemote) {
		this.datasourceFacadeRemote = datasourceFacadeRemote;
	}

	final DatasourceFacadeRemote getDatasourceFacadeRemote() {
		return datasourceFacadeRemote;
	}

	@Override
	public void detailsChanged(Component compSource) {
		super.detailsChanged(compSource);
	}

	@Override
	protected boolean isNewAllowed() {
		return isSaveAllowed(false);
	}

	@Override
	protected boolean isSaveAllowed(final boolean useCurrentCollectable) {
		return SecurityCache.getInstance().isWriteAllowedForMasterData(entityUid);
	}

	@Override
	protected boolean isDeleteSelectedCollectableAllowed(){
		return SecurityCache.getInstance().isDeleteAllowedForMasterData(entityUid);
	}

	@Override
	public CollectableDataSource<ValuelistProviderVO> findCollectableById(UID sEntity, UID oId, final int dependantsDepth) throws CommonBusinessException {
		// TODO Multinuclet Refactoring method signature
		return new CollectableDataSource<ValuelistProviderVO>(datasourcedelegate.getValuelistProvider(oId));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CollectableDataSource<ValuelistProviderVO> newCollectable() {
		return new CollectableDataSource<ValuelistProviderVO>(new ValuelistProviderVO(null, null, null, null, null));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected boolean stopEditingInDetails() {
		return pnlEdit.stopEditing();
	}

	/**
	 * generate the sql statement from the current definition of this datasource
	 * @return
	 * @throws CommonBusinessException
	 */
	@Override
	public String generateSql() throws CommonBusinessException {
		return datasourcedelegate.createSQL(new ValuelistProviderVO(null, null, pnlEdit.getQueryEditor().getXML(new DatasourceEntityOptions(false)), false, null));
	}

	/**
	 * @param sXML
	 */
	@Override
	protected void importXML(String sXML) throws NuclosBusinessException {
		final String sWarnings = QueryBuilderEditor.getSkippedElements(pnlEdit.getQueryEditor().setXML(sXML));
		if (sWarnings.length() > 0) {
			JOptionPane.showMessageDialog(getTab(), getSpringLocaleDelegate().getMessage(
					"DatasourceCollectController.12","Folgende Elemente existieren nicht mehr in dem aktuellen Datenbankschema\n und wurden daher entfernt:") + "\n" + sWarnings);
		}
		detailsChanged(pnlEdit.getQueryEditor());
	}

	/**
	 * @param file
	 * @throws IOException
	 * @throws NuclosBusinessException
	 */
	@Override
	protected void exportXML(File file) throws IOException, CommonBusinessException {
		final String sReportXML = pnlEdit.getQueryEditor().getXML(new DatasourceEntityOptions(false));
		IOUtils.writeToTextFile(file, sReportXML, "UTF-8");
	}

	@Override
	protected void validateSQL() {
		try {
			getDatasourceFacadeRemote().validateSqlFromXML(new ValuelistProviderVO(null, null, pnlEdit.getQueryEditor().getXML(new DatasourceEntityOptions(false)), false, null));
			JOptionPane.showMessageDialog(getTab(), getSpringLocaleDelegate().getMessage(
					"DatasourceCollectController.10","Die SQL Abfrage ist syntaktisch korrekt."));
		}
		catch (Exception ex) {
			Errors.getInstance().showExceptionDialog(getTab(), ex);
		}
	}

	@Override
	public Set<String> getQueryTypes() {
		HashSet<String> result = new HashSet<String>();
		result.add(QueryTable.QUERY_TYPE_VALUELIST_PROVIDER);
		return result;
	}

	@Override
	public boolean isWithParameterEditor() {
		return true;
	}

	@Override
	public boolean isParameterEditorWithValuelistProviderColumn() {
		return true;
	}

	@Override
	public boolean isParameterEditorWithLabelColumn() {
		return true;
	}
	
	@Override
	public boolean save() throws CommonBusinessException {
		
		// parameter validation
		final String xmlSource = pnlEdit.getQueryEditor().getXML(new DatasourceEntityOptions(true));
		if (null != xmlSource) {
			validateParameters(datasourcedelegate.getParametersFromXML(xmlSource));
		} else {
			LOG.warn("could not validate parameters, datasource xml source is not initialized");
		}
		boolean result = super.save();
		return result;
		
	}
	
	/**
	 * Validate Parameters
	 * 
	 * @param parametersFromXML	datasource as xml
	 * 
	 * @throws CommonBusinessException for any validation violation
	 */
	protected void validateParameters(
			List<DatasourceParameterVO> parametersFromXML) throws CommonBusinessException {
		final List<String> internal = Arrays.asList(INTERNAL_IDENTIFIERS);
		LOG.trace("validation for internal identifier use {}", StringUtils.logFormat("internal identifiers:", internal));
		for (final DatasourceParameterVO param : parametersFromXML) {
			 if (internal.contains(param.getParameter())) {
				throw new CommonBusinessException(
						 SpringLocaleDelegate.getInstance().getMessage(
								 "valuelistprovider.validation.name constraint violation",
								 "Der Parameter \"{0}\" muss umbenannt werden, er wird intern verwendet.", 
								 param.getParameter())
						 ) {
				};
			 }
		}
		LOG.trace("validation passed");
	}
	
	private class HeaderPanelDocumentListener implements DocumentListener {

		@Override
		public void insertUpdate(DocumentEvent e) {
			detailsChanged(pnlEdit);
		}

		@Override
		public void removeUpdate(DocumentEvent e) {
			detailsChanged(pnlEdit);
		}

		@Override
		public void changedUpdate(DocumentEvent e) {
			detailsChanged(pnlEdit);
		}
		
	}
	/*
	private class ValueListProviderCollectStateListener extends CollectController.DefaultCollectStateListener {
		@Override
		public void detailsModeEntered(CollectStateEvent ev) throws CommonBusinessException {
			final int iDetailsMode = ev.getNewCollectState().getInnerState();

			switch (iDetailsMode) {
				case CollectState.DETAILSMODE_VIEW:
					CollectController.this.setCollectableComponentModelsInDetailsPanelMultiEditable(false);
					Clct clct = CollectController.this.getCompleteSelectedCollectable(CollectController.this.isDetailsModeViewLoadingWithoutDependants());
					CollectController.this.safeFillDetailsPanel(clct);
					if (clct != null && clct.getId() != null && clct.getId() instanceof Long) {
						String label = getLabelForStartTab();
						Main.getInstance().getMainFrame().addHistory(getEntityUid(), (Long) clct.getId(), label);
					}
					break;
		}
	}*/

}
