//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.layout.wysiwyg.editor.ui.panels.layoutmlrule;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.layout.wysiwyg.WYSIWYGMetaInformation;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels.LAYOUTML_RULE_EDITOR;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableComponent;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGMatrix;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGSubForm;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGSubFormColumn;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyValueValuelistProvider;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.WYSIWYGLayoutEditorPanel;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.layoutmlrules.LayoutMLRule;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.layoutmlrules.LayoutMLRuleAction;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.layoutmlrules.LayoutMLRuleEventType;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.layoutmlrules.LayoutMLRules;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.layoutml.LayoutMLConstants;

/**
 * Class for (de)activating Actions
 * 
 * Some Events and Actions are not added to components by layoutml Parser
 *
 * @author hartmut.beckschulze
 */
public class LayoutMLRuleValidationLayer{

	public static HashMap<String, String> eventType = new HashMap<String, String>();
	public static HashMap<String, String> actionType = new HashMap<String, String>();


	{
		/** key - label */
		eventType.put(LayoutMLRuleEventType.LOOKUP, LAYOUTML_RULE_EDITOR.NAME_FOR_EVENT_LOOKUP);
		eventType.put(LayoutMLRuleEventType.VALUE_CHANGED, LAYOUTML_RULE_EDITOR.NAME_FOR_EVENT_VALUE_CHANGED);

		actionType.put(LayoutMLRuleAction.TRANSFER_LOOKEDUP_VALUE, LAYOUTML_RULE_EDITOR.NAME_FOR_ACTION_TRANSFER_LOOKEDUP_VALUE);
		actionType.put(LayoutMLRuleAction.CLEAR, LAYOUTML_RULE_EDITOR.NAME_FOR_ACTION_CLEAR);
		actionType.put(LayoutMLRuleAction.ENABLE, LAYOUTML_RULE_EDITOR.NAME_FOR_ACTION_ENABLE);
		actionType.put(LayoutMLRuleAction.REFRESH_VALUELIST, LAYOUTML_RULE_EDITOR.NAME_FOR_ACTION_REFRESH_VALUELIST);
		actionType.put(LayoutMLRuleAction.REINIT_SUBFORM, LAYOUTML_RULE_EDITOR.NAME_FOR_ACTION_REINIT_SUBFORM);
	}
	
	/**
	 * This method checks which controlType the component is of and returns the fitting Actions
	 * for the Event.
	 * @param eventType
	 * @param controlType
	 * @return {@link String}[] 
	 */
	public static String[] getActionsForEventType(String eventType, String controlType) {
		if (LayoutMLConstants.ELEMENT_SUBFORMCOLUMN.equals(controlType)) {
			return getActionsForSubformColumns(eventType);
		}
		else if (LayoutMLConstants.ELEMENT_REINIT_SUBFORM.equals(controlType)) {
			return getActionsForSubformColumns(eventType);
		}
		else if(LayoutMLConstants.ELEMENT_MATRIXCOLUMN.equals(controlType)) {
			return getActionsForSubformColumns(eventType);
		}
		else {
			return getActionsForCollectableComponents(eventType, controlType);
		}
	}
	
	/**
	 * This Method returns the possible EventTypes for Components depending on their controlType
	 * @param metaInformation 
	 * @param layoutMLRule 
	 * @param controlType
	 * @return {@link String}[] 
	 */
	public static String[] getEventTypesForComponents(WYSIWYGMetaInformation metaInformation, LayoutMLRule layoutMLRule, String controlType) {
		if (LayoutMLConstants.ELEMENT_SUBFORMCOLUMN.equals(controlType)) {
			boolean lovORcombobox = layoutMLRule.isListOfValues();
			if (lovORcombobox)
				return getEventTypesForSubformColumns(true);
			else
				return getEventTypesForSubformColumns(false);
		}
		else if(LayoutMLConstants.ELEMENT_REINIT_SUBFORM.equals(controlType)) {
			return getEventTypesForSubformColumns(true);
		}
		else if(LayoutMLConstants.ELEMENT_MATRIXCOLUMN.equals(controlType)) {
			return getEventTypesForSubformColumns(true);
		}
		
		return getEventTypesForCollectableComponents(metaInformation, layoutMLRule, controlType);
	}
	
	/**
	 * @param lovORcombobox 
	 * @return possible {@link LayoutMLRuleEventType} for {@link WYSIWYGSubFormColumn}
	 */
	private static String[] getEventTypesForSubformColumns(boolean lovORcombobox) {
		if (lovORcombobox)
			return new String[]{LayoutMLRuleEventType.LOOKUP, LayoutMLRuleEventType.VALUE_CHANGED};

		return new String[]{LayoutMLRuleEventType.VALUE_CHANGED};
	}
	
	/**
	 * @param layoutMLRule 
	 * @param metaInformation 
	 * @return possible {@link LayoutMLRuleEventType} for {@link WYSIWYGCollectableComponent}
	 */
	private static String[] getEventTypesForCollectableComponents(WYSIWYGMetaInformation meta, LayoutMLRule layoutMLRule, String controlType) {
		if (LayoutMLConstants.ATTRIBUTEVALUE_LISTOFVALUES.equals(controlType)) {
			return new String[]{LayoutMLRuleEventType.LOOKUP, LayoutMLRuleEventType.VALUE_CHANGED};
		} else if (LayoutMLConstants.CONTROLTYPE_COMBOBOX.equals(controlType)) {
			UID linkedEntity = meta.getLinkedEntityForAttribute(layoutMLRule.getComponentEntity(), layoutMLRule.getComponentUID());
			if (linkedEntity != null) {
				return new String[]{LayoutMLRuleEventType.LOOKUP, LayoutMLRuleEventType.VALUE_CHANGED};
			}
		}
		return new String[]{LayoutMLRuleEventType.VALUE_CHANGED};
	}
	
	/**
	 * @param eventType {@link LayoutMLRuleEventType} 
	 * @return {@link LayoutMLRuleAction} allowed for this {@link LayoutMLRuleEventType}
	 */
	private static String[] getActionsForSubformColumns(String eventType) {
		if(LayoutMLRuleEventType.LOOKUP.equals(eventType))
			return new String[]{LayoutMLRuleAction.TRANSFER_LOOKEDUP_VALUE};
		if(LayoutMLRuleEventType.VALUE_CHANGED.equals(eventType))
			return new String[]{LayoutMLRuleAction.CLEAR, LayoutMLRuleAction.REFRESH_VALUELIST, LayoutMLRuleAction.REINIT_SUBFORM};
		return null;
	}
	
	/**
	 * @param controlType the ControlType for the {@link WYSIWYGCollectableComponent}
	 * @param eventType {@link LayoutMLRuleEventType} 
	 * @return {@link LayoutMLRuleAction} allowed for this {@link LayoutMLRuleEventType}
	 */
	private static String[] getActionsForCollectableComponents(String eventType, String controlType) {
		if(LayoutMLRuleEventType.LOOKUP.equals(eventType))
			return new String[]{LayoutMLRuleAction.TRANSFER_LOOKEDUP_VALUE};
		if(LayoutMLRuleEventType.VALUE_CHANGED.equals(eventType))
			return new String[]{LayoutMLRuleAction.CLEAR, LayoutMLRuleAction.REFRESH_VALUELIST, LayoutMLRuleAction.REINIT_SUBFORM};
		return null;		
	}
	
	/**
	 * 
	 * @param editorPanel
	 * @param entity
	 * @return
	 */
	public static List<FieldMeta<?>> getSubformColumnsWithValueListProvider(WYSIWYGLayoutEditorPanel editorPanel, UID entity) {
		final List<WYSIWYGComponent> subforms = new ArrayList<WYSIWYGComponent>();
		//getting all Subforms
		editorPanel.getWYSIWYGComponents(WYSIWYGSubForm.class, editorPanel.getMainEditorPanel(), subforms);
		for (WYSIWYGComponent subform : subforms) {
			if (entity.equals(((WYSIWYGSubForm)subform).getEntityUID())){
				return LayoutMLRuleValidationLayer.getValueListProviderComponents(editorPanel, (WYSIWYGSubForm)subform);
			}
		}		
		return null;
	}
	
	public static List<FieldMeta<?>> getMatrixColumnsWithValueListProvider(WYSIWYGLayoutEditorPanel editorPanel, UID entity) {
		final List<WYSIWYGComponent> matrixs = new ArrayList<WYSIWYGComponent>();
		//getting all Subforms
		editorPanel.getWYSIWYGComponents(WYSIWYGMatrix.class, editorPanel.getMainEditorPanel(), matrixs);
		for (WYSIWYGComponent matrix : matrixs) {
			if (entity.equals(((WYSIWYGMatrix)matrix).getEntityY())){
				return LayoutMLRuleValidationLayer.getValueListProviderComponentsFromMatrix(editorPanel, (WYSIWYGMatrix)matrix);
			}
		}		
		return Collections.emptyList();
	}
	
	/**
	 * 
	 * @param editorPanel
	 * @return
	 */
	public static List<FieldMeta<?>> getCollectableComponentsWithValueListProvider(WYSIWYGLayoutEditorPanel editorPanel) {
		return LayoutMLRuleValidationLayer.getValueListProviderComponents(editorPanel.getMainEditorPanel(), null);
	}
		
	/**
	 * 
	 * @param editorPanel the {@link WYSIWYGLayoutEditorPanel}
	 * @param subform optional the {@link WYSIWYGSubForm} for getting the {@link WYSIWYGSubFormColumn} with {@link PropertyValueValuelistProvider}
	 * @return a {@link String}[] with the names of the {@link WYSIWYGComponent}s found, null if there are none
	 */
	private static List<FieldMeta<?>> getValueListProviderComponents(WYSIWYGLayoutEditorPanel editorPanel, WYSIWYGSubForm subform) {
		List<FieldMeta<?>> result = null;
		if (subform != null) {
			// just subformcolumns with vp
			result = subform.getColumnsWithValueListProvider();
		} else {
			// all collectable components with vp
			result = editorPanel.getCollectableComponentsWithValuelistProvider();
		}
		
		if (result != null) {
			Collections.sort(result, new Comparator<FieldMeta<?>>() {
				@Override
				public int compare(FieldMeta<?> o1, FieldMeta<?> o2) {
					return SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(o1)
							.compareToIgnoreCase(SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(o2));
				}
			});
			return result;
		}
		return null;
	}
	
	private static List<FieldMeta<?>> getValueListProviderComponentsFromMatrix(WYSIWYGLayoutEditorPanel editorPanel, WYSIWYGMatrix matrix) {
		List<FieldMeta<?>> result = new ArrayList<FieldMeta<?>>();
		if (matrix != null) {
			// just subformcolumns with vp
			result = matrix.getColumnsWithValueListProvider();
		} else {
			// all collectable components with vp
			result = editorPanel.getCollectableComponentsWithValuelistProvider();
		}
		
		if (result != null) {
			Collections.sort(result, new Comparator<FieldMeta<?>>() {
				@Override
				public int compare(FieldMeta<?> o1, FieldMeta<?> o2) {
					return SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(o1)
							.compareToIgnoreCase(SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(o2));
				}
			});
			return result;
		}
		return result;
	}
	
	/**
	 * 
	 * @param editorPanel the {@link WYSIWYGLayoutEditorPanel} for getting the {@link WYSIWYGComponent}
	 * @return a {@link String}[] with the names of all {@link WYSIWYGCollectableComponent}, null if there are none
	 */
	public static List<FieldMeta<?>> getAllCollectableComponents(WYSIWYGLayoutEditorPanel editorPanel) {
		final List<FieldMeta<?>> result = editorPanel.getCollectableComponents();
		if (result != null) {
			Collections.sort(result, new Comparator<FieldMeta<?>>() {
				@Override
				public int compare(FieldMeta<?> o1, FieldMeta<?> o2) {
					return SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(o1)
							.compareToIgnoreCase(SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(o2));
				}
			});
		}	
		return result;		
	}
	
	/**
	 * 
	 * @param editorPanel the {@link WYSIWYGLayoutEditorPanel} for getting the {@link WYSIWYGComponent}
	 * @return a {@link String}[] with the names of all {@link WYSIWYGCollectableComponent}, null if there are none
	 */
	public static List<FieldMeta<?>> getAllCollectableComponentsWithReference(WYSIWYGLayoutEditorPanel editorPanel, final UID foreignUID, final UID entity) {
		
		List<FieldMeta<?>> result = null;
		
		if (foreignUID != null && entity != null) {
			for (FieldMeta field : editorPanel.getMetaInformation().getSubFormColumns(foreignUID)) {
				if (entity.equals(field.getForeignEntity())) {
					if (result == null)
						result = new ArrayList<FieldMeta<?>>();
					
					result.add(field);
				}
			}
		}
		
		if (result != null) {
			Collections.sort(result, new Comparator<FieldMeta<?>>() {
				@Override
				public int compare(FieldMeta<?> o1, FieldMeta<?> o2) {
					return SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(o1)
							.compareToIgnoreCase(SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(o2));
				}
			});
		}	
		return result;		
	}
	
	/**
	 * @param component the {@link WYSIWYGSubForm} to get the Names from
	 * @return a {@link String}[] with the Names of the Columns, may be null if there are none
	 */
	public static List<FieldMeta<?>> getAllSubformColumns(WYSIWYGComponent component) {
		WYSIWYGSubForm subform = null;
		if (component instanceof WYSIWYGSubFormColumn) {
			subform = ((WYSIWYGSubFormColumn)component).getSubForm();
		}
		
		if (subform == null)
			return null;
		
		final List<UID> columnUIDs = subform.getColumnUIDs();
		if (columnUIDs != null) {
			final List<FieldMeta<?>> result = CollectionUtils.transform(columnUIDs, new Transformer<UID, FieldMeta<?>>() {
				@Override
				public FieldMeta<?> transform(UID i) {
					return MetaProvider.getInstance().getEntityField(i);
				}
			});
			Collections.sort(result, new Comparator<FieldMeta<?>>() {
				@Override
				public int compare(FieldMeta<?> o1, FieldMeta<?> o2) {
					return SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(o1)
							.compareToIgnoreCase(SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(o2));
				}
			});
			return result;
		}
		return null;
	}
	
	/**
	 * 
	 * @param editorPanel
	 * @return
	 */
	public static List<EntityMeta<?>> getAllSubformOrMatrixEntities(WYSIWYGLayoutEditorPanel editorPanel) {
		final List<UID> subformUIDs = editorPanel.getSubFormEntityUIDs();
		final List<UID> matrixUIDs = editorPanel.getMatrixEntityUIDs();
		List<EntityMeta<?>> result = new ArrayList<EntityMeta<?>>();
		if (subformUIDs.size() > 0) {
			result = CollectionUtils.transform(subformUIDs, new Transformer<UID, EntityMeta<?>>() {
				@Override
				public EntityMeta<?> transform(UID i) {
					return MetaProvider.getInstance().getEntity(i);
				}
			});			
		}
		if(matrixUIDs.size() > 0) {
			result.addAll(CollectionUtils.transform(matrixUIDs, new Transformer<UID, EntityMeta<?>>() {
				@Override
				public EntityMeta<?> transform(UID i) {
					return MetaProvider.getInstance().getEntity(i);
				}
			}));
		}
		
		return result.isEmpty() ? null : result;
	}
	
	public static void validateLayoutMLRules(LayoutMLRules layoutMLRules) throws NuclosBusinessException {
		StringBuffer errors = new StringBuffer();
		errors.append(WYSIWYGStringsAndLabels.ERROR_MESSAGES.ERROR_VALIDATING_LAYOUTMLRULES);
		boolean error = false;
		
		for(LayoutMLRule layoutMLRule : layoutMLRules.getRules()) {
			String ruleName = layoutMLRule.getRuleName() + "\n";
			if (layoutMLRule.isSubformEntity()){
				if (layoutMLRule.getComponentEntity() == null) {
					errors.append(ruleName);
					ruleName = "";
					errors.append(WYSIWYGStringsAndLabels.LAYOUTML_RULE_EDITOR.LABEL_ENTITY + " " + WYSIWYGStringsAndLabels.ERROR_MESSAGES.TEXT_MISSING + "\n");
					error = true;
				}
			}
			if (layoutMLRule.getComponentUID() == null){
				errors.append(ruleName);
				ruleName = "";
				errors.append(WYSIWYGStringsAndLabels.LAYOUTML_RULE_EDITOR.LABEL_SOURCE_COMPONENT + " " + WYSIWYGStringsAndLabels.ERROR_MESSAGES.TEXT_MISSING + "\n");
				error = true;
			}
			if (StringUtils.isNullOrEmpty(layoutMLRule.getLayoutMLRuleEventType().getEventType())) {
				errors.append(ruleName);
				ruleName = "";
				errors.append(WYSIWYGStringsAndLabels.LAYOUTML_RULE_EDITOR.LABEL_EVENT_TRIGGERING_RULE + " " + WYSIWYGStringsAndLabels.ERROR_MESSAGES.TEXT_MISSING + "\n");
				error = true;
			}
			if (layoutMLRule.getLayoutMLRuleActions().getSingleActions().isEmpty()) {
				errors.append(ruleName);
				ruleName = "";
				errors.append(WYSIWYGStringsAndLabels.LAYOUTML_RULE_EDITOR.LABEL_ACTION_SELECTOR + " " + WYSIWYGStringsAndLabels.ERROR_MESSAGES.TEXT_MISSING + "\n");
				error = true;
			}
				
			for (LayoutMLRuleAction action : layoutMLRule.getLayoutMLRuleActions().getSingleActions()) {
				if (StringUtils.isNullOrEmpty(action.getRuleAction())){
					errors.append(ruleName);
					ruleName = "";
					errors.append(WYSIWYGStringsAndLabels.LAYOUTML_RULE_EDITOR.LABEL_ACTION_SELECTOR + " " + WYSIWYGStringsAndLabels.ERROR_MESSAGES.TEXT_MISSING + "\n");
					error = true;
				} else {
				if (!layoutMLRule.isSubformEntity() && LayoutMLRuleAction.CLEAR.equals(action.getRuleAction())) {
					if (action.getTargetComponent() == null){
						errors.append(ruleName);
						ruleName = "";
						errors.append(WYSIWYGStringsAndLabels.LAYOUTML_RULE_EDITOR.LABEL_ACTION_SELECTOR + " " + actionType.get(action.getRuleAction()) + "\n");
						errors.append(WYSIWYGStringsAndLabels.LAYOUTML_RULE_EDITOR.LABEL_TARGET_COMPONENT + " " + WYSIWYGStringsAndLabels.ERROR_MESSAGES.TEXT_MISSING + "\n");
						error = true;
					}
				} else if (!layoutMLRule.isSubformEntity() && LayoutMLRuleAction.REFRESH_VALUELIST.equals(action.getRuleAction())) {
					if (action.getEntity() == null) {
						if (action.getTargetComponent() == null){
							errors.append(ruleName);
							ruleName = "";
							errors.append(WYSIWYGStringsAndLabels.LAYOUTML_RULE_EDITOR.LABEL_ACTION_SELECTOR + " " + actionType.get(action.getRuleAction()) + "\n");
							errors.append(WYSIWYGStringsAndLabels.LAYOUTML_RULE_EDITOR.LABEL_TARGET_COMPONENT + " " + WYSIWYGStringsAndLabels.ERROR_MESSAGES.TEXT_MISSING + "\n");
							error = true;
						}
					}
				} else if (layoutMLRule.isSubformEntity() && LayoutMLRuleAction.REFRESH_VALUELIST.equals(action.getRuleAction())) {
					if (action.getEntity() == null) {
						errors.append(ruleName);
						ruleName = "";
						errors.append(WYSIWYGStringsAndLabels.LAYOUTML_RULE_EDITOR.LABEL_ACTION_SELECTOR + " " + actionType.get(action.getRuleAction()) + "\n");
						errors.append(WYSIWYGStringsAndLabels.LAYOUTML_RULE_EDITOR.LABEL_ENTITY + " " + WYSIWYGStringsAndLabels.ERROR_MESSAGES.TEXT_MISSING + "\n");
						error = true;
					}
						if (action.getTargetComponent() == null){
							errors.append(ruleName);
							ruleName = "";
							errors.append(WYSIWYGStringsAndLabels.LAYOUTML_RULE_EDITOR.LABEL_ACTION_SELECTOR + " " + actionType.get(action.getRuleAction()) + "\n");
							errors.append(WYSIWYGStringsAndLabels.LAYOUTML_RULE_EDITOR.LABEL_TARGET_COMPONENT + " " + WYSIWYGStringsAndLabels.ERROR_MESSAGES.TEXT_MISSING + "\n");
							error = true;
						}
					} else if (LayoutMLRuleAction.TRANSFER_LOOKEDUP_VALUE.equals(action.getRuleAction())) {
						if (action.getTargetComponent() == null){
							errors.append(ruleName);
							ruleName = "";
							errors.append(WYSIWYGStringsAndLabels.LAYOUTML_RULE_EDITOR.LABEL_ACTION_SELECTOR + " " + actionType.get(action.getRuleAction()) + "\n");
							errors.append(WYSIWYGStringsAndLabels.LAYOUTML_RULE_EDITOR.LABEL_TARGET_COMPONENT + " " + WYSIWYGStringsAndLabels.ERROR_MESSAGES.TEXT_MISSING + "\n");
							error = true;
						}
						if (action.getSourceField() == null){
							errors.append(ruleName);
							ruleName = "";
							errors.append(WYSIWYGStringsAndLabels.LAYOUTML_RULE_EDITOR.LABEL_ACTION_SELECTOR + " " + actionType.get(action.getRuleAction()) + "\n");
							errors.append(WYSIWYGStringsAndLabels.LAYOUTML_RULE_EDITOR.LABEL_SOURCE_COMPONENT + " " + WYSIWYGStringsAndLabels.ERROR_MESSAGES.TEXT_MISSING + "\n");
							error = true;
						}
				} 
			}
				if (StringUtils.isNullOrEmpty(ruleName))
					errors.append("\n");
			}
			if (StringUtils.isNullOrEmpty(ruleName))
				errors.append("\n");
		}
		if (error)
			throw new NuclosBusinessException(errors.toString());
		
	}
	
}
