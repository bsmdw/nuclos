//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.layout.wysiwyg.component;

import java.awt.EventQueue;
import java.awt.KeyboardFocusManager;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.Future;

import javax.swing.JButton;
import javax.swing.SwingUtilities;

import org.nuclos.api.rule.CustomRule;
import org.nuclos.client.command.ExecutionResultListener;
import org.nuclos.client.common.EntityCollectController;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.genericobject.GenericObjectCollectController;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels.STATIC_BUTTON;
import org.nuclos.client.masterdata.MasterDataCollectController;
import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.collect.CollectActionAdapter;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.client.ui.collect.UserCancelledException;
import org.nuclos.client.ui.layer.LayerLock;
import org.nuclos.client.ui.layer.LockingResultListener;
import org.nuclos.client.ui.layoutml.LayoutMLParser;
import org.nuclos.common.Actions;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.server.common.valueobject.NuclosValueObject;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.ruleengine.NuclosFatalRuleException;


/**
 * Button Action executing a Business Rule on click.
 * <br>
 * NUCLOSINT-743 Rule Button Action
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author <a href="mailto:hartmut.beckschulze@novabit.de">hartmut.beckschulze</a>
 * @version 01.00.00
 */
public class ExecuteRuleButtonAction<PK,Clct extends Collectable<PK>> implements CollectActionAdapter<PK,Clct> {
	
	@Override
	public void run(final JButton btn, final CollectController<PK,Clct> controller, final Properties probs) {
		run(btn, controller, probs, null);
	}
	
	public void run(final JButton btn, final CollectController<PK,Clct> controller, final Properties probs,
			final ExecutionResultListener resultListener) {
		if (!controller.getDetailsPanel().isVisible()) {
			return;
		}

		if (!SecurityCache.getInstance().isActionAllowed(Actions.ACTION_EXECUTE_RULE_BY_USER)) {
			return;
		}

		final String rule = probs.getProperty(STATIC_BUTTON.EXECUTE_RULE_ACTION_ARGUMENT);
		
		EventSupportSourceVO ruleToExecute = EventSupportRepository.getInstance().getEventSupportByClassname(rule);
		
		if (ruleToExecute == null && rule.startsWith("org.nuclos.businessentity.rule")) {
			NuclosValueObject dummyNvo = new NuclosValueObject();
			EventSupportSourceVO dummyVo = new EventSupportSourceVO(dummyNvo, rule, rule, rule,
					Arrays.asList(CustomRule.class.getCanonicalName()), rule.substring(0, rule.lastIndexOf(".")),
					new Date(), null, false);
			ruleToExecute = dummyVo;
		}
		if (ruleToExecute != null) {
			if (controller instanceof EntityCollectController) {
				final Future<LayerLock> lock = controller.lockFrame();
				final LockingResultListener<Exception> exResult = new LockingResultListener<Exception>() {
					@Override
					public void done(Exception result) {
						controller.unLockFrame(getLayerLock());
						
						final boolean withoutError = result == null;
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								try {
									if (withoutError) {
										controller.refreshCurrentCollectable(false);
									}
									if (withoutError && resultListener != null) {
										resultListener.done();
									}
								} catch (CommonBusinessException e) {
									Errors.getInstance().showExceptionDialog(controller.getCollectPanel(), e);
								}
								
								// Must be invoked later, else focus is not set with compound components like LOVs
								EventQueue.invokeLater(new Runnable() {
									@Override
						            public void run() {
										if (btn.getClientProperty(LayoutMLParser.ATTRIBUTE_NEXTFOCUSONACTION) != null 
												&& btn.getClientProperty(LayoutMLParser.ATTRIBUTE_NEXTFOCUSONACTION).equals(Boolean.TRUE)) {
											KeyboardFocusManager.getCurrentKeyboardFocusManager().focusNextComponent(btn);
										}
									}
								});
							}
						});
						
						if (result != null 
								&& result.getClass() == NuclosFatalRuleException.class
								&& result.getCause() instanceof Exception) {
							result = (Exception) result.getCause();
						}
							
						if (result != null) {
							if (UserCancelledException.class.isAssignableFrom(result.getClass())){
								if (resultListener != null) resultListener.error(result);
							} else if (CommonFinderException.class.isAssignableFrom(result.getClass())){
								if (resultListener != null) resultListener.error(result);
								Errors.getInstance().showExceptionDialog(controller.getCollectPanel(), result);
							} else if (CommonBusinessException.class.isAssignableFrom(result.getClass())){
								if (resultListener != null) resultListener.error(result);
								if (controller instanceof EntityCollectController) {
									EntityCollectController<PK,Clct> eController = (EntityCollectController<PK,Clct>) controller;
									if (!eController.handleException(result)) {
										Errors.getInstance().showExceptionDialog(controller.getCollectPanel(), result);
									}
								} else {
									Errors.getInstance().showExceptionDialog(controller.getCollectPanel(), result);
								}
							} else {
								if (resultListener != null) {
									resultListener.error(result);
								}
								Errors.getInstance().showExceptionDialog(controller.getCollectPanel(), result);
							}
						}
					}

					@Override
					public Future<LayerLock> getLayerLock() {
						return lock;
					}
				};
				((EntityCollectController) controller).executeBusinessRules(
						Collections.singletonList(ruleToExecute), exResult);
			}
		}
	}

	@Override
	public boolean isRunnable(CollectController<PK,Clct> controller, Properties probs) {
		if (!controller.getDetailsPanel().isVisible()) {
			return false;
		}
		if (!SecurityCache.getInstance().isActionAllowed(Actions.ACTION_EXECUTE_RULE_BY_USER)) {
			return false;
		}
		final String rule = probs.getProperty(STATIC_BUTTON.EXECUTE_RULE_ACTION_ARGUMENT);
		if (rule != null && rule.startsWith("org.nuclos.businessentity.rule")) {
			return true;
		}
		final EventSupportSourceVO ruleToExecute = EventSupportRepository.getInstance().getEventSupportByClassname(rule);
		if (ruleToExecute != null) {
			final Object c = controller;
			if (c instanceof MasterDataCollectController<?>) {
				if (((MasterDataCollectController<PK>) c).getUserRules().contains(ruleToExecute))
					return true;
			}
			else if (c instanceof GenericObjectCollectController) {
				if (((GenericObjectCollectController) c).getUserRules().contains(ruleToExecute))
					return true;
			}
		}
		return false;
	}
}
