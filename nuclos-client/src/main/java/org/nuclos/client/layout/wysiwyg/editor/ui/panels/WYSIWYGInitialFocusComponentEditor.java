//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.layout.wysiwyg.editor.ui.panels;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.AbstractButton;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;

import org.apache.log4j.Logger;
import org.nuclos.client.NuclosIcons;
import org.nuclos.client.common.EntityUtils;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels.INITIAL_FOCUS_EDITOR;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.WYSIWYGEditorsToolbar.WYSIWYGToolbarAttachable;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.elementalcomponents.SaveAndCancelButtonPanel;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.elementalcomponents.SaveAndCancelButtonPanel.SaveAndCancelButtonPanelControllable;
import org.nuclos.client.layout.wysiwyg.editor.util.InterfaceGuidelines;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.WYSIWYGInitialFocusComponent;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonFatalException;

import info.clearthought.layout.TableLayout;
import info.clearthought.layout.TableLayoutConstraints;

/**
 * This Class provides a GUI for setting the {@link WYSIWYGInitialFocusComponent}.<br>
 * It is attached to the {@link WYSIWYGEditorsToolbar}.<br>
 * 
 * 
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * 
 * @author <a href="mailto:thomas.schiffmann@novabit.de">thomas.schiffmann</a>
 * @version 01.00.00
 */
public class WYSIWYGInitialFocusComponentEditor implements WYSIWYGToolbarAttachable {
	
	private static final Logger LOG = Logger.getLogger(WYSIWYGInitialFocusComponentEditor.class);
	
	private String path = "org/nuclos/client/layout/wysiwyg/editor/ui/panels/icons/";

	private Icon iconInitialFocus = new ImageIcon(
			org.nuclos.common2.LangUtils.getClassLoaderThatWorksForWebStart().getResource(
						path + "initial-focus-component.png"));

	private WYSIWYGLayoutEditorPanel editorPanel = null;
	
	private JButton[] toolbarItems = null;
	
	/**
	 * @param e the {@link WYSIWYGLayoutEditorPanel}
	 */
	public WYSIWYGInitialFocusComponentEditor(WYSIWYGLayoutEditorPanel e) {
		this.editorPanel = e;
		
		JButton button = new JButton(iconInitialFocus);
		button.setToolTipText(INITIAL_FOCUS_EDITOR.TOOLBAR_TOOLTIP);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new WYSIWYGInitialFocusComponentDialog();
			}
		});
		toolbarItems = new JButton[] {button};
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.editor.ui.panels.WYSIWYGEditorsToolbar.WYSIWYGToolbarAttachable#getToolbarItems()
	 */
	@Override
	public JComponent[] getToolbarItems() {
		return toolbarItems;
	}

	/**
	 * This Class provides the Editordialog for {@link WYSIWYGInitialFocusComponentEditor}.<br>
	 * Its called from the Click action in {@link WYSIWYGInitialFocusComponentEditor#WYSIWYGInitialFocusComponentEditor(WYSIWYGLayoutEditorPanel)}
	 * 
	 * 
	 * <br>
	 * Created by Novabit Informationssysteme GmbH <br>
	 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
	 * 
	 * @author <a href="mailto:hartmut.beckschulze@novabit.de">hartmut.beckschulze</a>
	 * @version 01.00.00
	 */
	private class WYSIWYGInitialFocusComponentDialog extends JDialog implements SaveAndCancelButtonPanelControllable {
		
		private JComboBox comboBoxEntity;
		private JComboBox comboBoxEntityField;
		
		/** the layoutdefinition for the Dialog */
		private double[][] layout = new double[][]{
				{
					InterfaceGuidelines.MARGIN_LEFT,
					TableLayout.PREFERRED,
					InterfaceGuidelines.MARGIN_BETWEEN,
					TableLayout.FILL,
					InterfaceGuidelines.MARGIN_RIGHT
				},
				{
					InterfaceGuidelines.MARGIN_TOP,
					TableLayout.PREFERRED,
					InterfaceGuidelines.MARGIN_BETWEEN,
					TableLayout.PREFERRED,
					InterfaceGuidelines.MARGIN_BETWEEN,
					TableLayout.PREFERRED,
					InterfaceGuidelines.MARGIN_BOTTOM
				}
			};
		
		private int width = 400;
		private int height = 150;
		
		/**
		 * Constructor
		 */
		private WYSIWYGInitialFocusComponentDialog() {
			this.setIconImage(NuclosIcons.getInstance().getScaledDialogIcon(48).getImage());
		
			this.setTitle(INITIAL_FOCUS_EDITOR.TITLE_INITIAL_FOCUS_EDITOR);
			
			this.setLayout(new TableLayout(layout));
			
			JLabel lblAttribute = new JLabel(INITIAL_FOCUS_EDITOR.LABEL_ATTRIBUTE);
			this.add(lblAttribute, "1,3");
			
			this.comboBoxEntityField = new JComboBox();
			this.add(comboBoxEntityField, "3,3");
			
			this.comboBoxEntity = new JComboBox();
			
			/** handling the change of the entity to find subentity (case Subform/ Subform Column */
			this.comboBoxEntity.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					try {
						if (e.getStateChange() == ItemEvent.SELECTED) {
							if (comboBoxEntity.getSelectedItem() == null
									|| LangUtils.equal(comboBoxEntity.getSelectedItem(), EntityMeta.NULL)) {
								comboBoxEntityField.removeAllItems();
								
								final UID entityUid = WYSIWYGInitialFocusComponentEditor.this.editorPanel.getController().getLayoutCollectController().getEntityName();
								final List<FieldMeta<?>> fields
									= new ArrayList<FieldMeta<?>>(WYSIWYGInitialFocusComponentEditor.this.editorPanel.getCollectableComponents());
								Collections.sort(fields, EntityUtils.getMetaComparator(FieldMeta.class));
								
								for (FieldMeta<?> field : fields) {
									if (!SF.isEOField(entityUid, field.getUID()))
										comboBoxEntityField.addItem(EntityUtils.wrapMetaData(field));
								}
							}
							else {
								comboBoxEntityField.removeAllItems();
								
								final UID entityUid = ((EntityMeta<?>)comboBoxEntity.getSelectedItem()).getUID();
								final List<FieldMeta<?>> fields
									= new ArrayList<FieldMeta<?>>(WYSIWYGInitialFocusComponentEditor.this.editorPanel.getMetaInformation().getSubFormColumns(entityUid));
								Collections.sort(fields, EntityUtils.getMetaComparator(FieldMeta.class));
								
								for (FieldMeta<?> field : fields) {
									if (!SF.isEOField(entityUid, field.getUID()))
										comboBoxEntityField.addItem(EntityUtils.wrapMetaData(field));
								}
							}
						}
					} catch (Exception ex) {
						LOG.warn("error getting focusable components.", ex);
					}
				}
			});
			
			this.comboBoxEntity.addItem(EntityUtils.wrapMetaData(EntityMeta.NULL));
			final List<UID> subformUIDs = WYSIWYGInitialFocusComponentEditor.this.editorPanel.getSubFormEntityUIDs();
			if (subformUIDs.size() > 0) {
				final List<EntityMeta<?>> subformEntities = CollectionUtils.transform(subformUIDs, new Transformer<UID, EntityMeta<?>>() {
					@Override
					public EntityMeta<?> transform(UID i) {
						return MetaProvider.getInstance().getEntity(i);
					}
				});
				Collections.sort(subformEntities, EntityUtils.getMetaComparator(EntityMeta.class));
				for (EntityMeta<?> entity : subformEntities) {
					comboBoxEntity.addItem(EntityUtils.wrapMetaData(entity));	
				}
			}
			
			JLabel lblEntity  = new JLabel(INITIAL_FOCUS_EDITOR.LABEL_ENTITY);
			this.add(lblEntity , "1,1");
			this.add(comboBoxEntity, "3,1");
			
			/** restoring values that may be set */
			if (WYSIWYGInitialFocusComponentEditor.this.editorPanel.getInitialFocusComponent() != null) {
				this.comboBoxEntity.setSelectedItem(
						WYSIWYGInitialFocusComponentEditor.this.editorPanel.getInitialFocusComponent().getEntityUID() == null ? EntityMeta.NULL : 
							MetaProvider.getInstance().getEntity(WYSIWYGInitialFocusComponentEditor.this.editorPanel.getInitialFocusComponent().getEntityUID()));
				
				FieldMeta<?> entityField = null;
				try {
					entityField = MetaProvider.getInstance().getEntityField(WYSIWYGInitialFocusComponentEditor.this.editorPanel.getInitialFocusComponent().getEntityFieldUID());
				} catch(CommonFatalException e) {
					LOG.error("Unable to get initial focus component. Maybe the field was deleted.");
				}
				
				this.comboBoxEntityField.setSelectedItem(WYSIWYGInitialFocusComponentEditor.this.editorPanel.getInitialFocusComponent().getEntityFieldUID() == null ? FieldMeta.NULL : entityField);
			}
			
			JButton remove = new JButton(INITIAL_FOCUS_EDITOR.LABEL_BUTTON_REMOVE_INITIAL_FOCUS);
			remove.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					WYSIWYGInitialFocusComponentEditor.this.editorPanel.setInitialFocusComponent(null);
					WYSIWYGInitialFocusComponentDialog.this.setVisible(false);
				}
			});
			
			ArrayList<AbstractButton> additional = new ArrayList<AbstractButton>();
			additional.add(remove);
			
			this.add(new SaveAndCancelButtonPanel(this.getBackground(), this, null, additional), new TableLayoutConstraints(0,5,4,5));
			
			Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			int x = (screenSize.width - width) / 2;
			int y = (screenSize.height - height) / 2;
			this.setBounds(x, y, width, height);
			this.setResizable(true);
			this.setModal(true);
			this.setVisible(true);
		}
		
		/*
		 * (non-Javadoc)
		 * @see org.nuclos.client.layout.wysiwyg.editor.ui.panels.elementalcomponents.SaveAndCancelButtonPanel.SaveAndCancelButtonPanelControllable#performCancelAction()
		 */
		@Override
		public void performCancelAction() {
			this.setVisible(false);
		}

		/*
		 * (non-Javadoc)
		 * @see org.nuclos.client.layout.wysiwyg.editor.ui.panels.elementalcomponents.SaveAndCancelButtonPanel.SaveAndCancelButtonPanelControllable#performSaveAction()
		 */
		@Override
		public void performSaveAction() {
			WYSIWYGInitialFocusComponent initFocus = new WYSIWYGInitialFocusComponent(
					(!(comboBoxEntity.getSelectedItem() instanceof EntityMeta<?>)) ? null : ((EntityMeta<?>)comboBoxEntity.getSelectedItem()).getUID(),
							(!(comboBoxEntityField.getSelectedItem() instanceof FieldMeta<?>)) ? null : ((FieldMeta<?>)comboBoxEntityField.getSelectedItem()).getUID());
			WYSIWYGInitialFocusComponentEditor.this.editorPanel.setInitialFocusComponent(initFocus);
			this.setVisible(false);
		}
	}

}
