//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.layout.wysiwyg.editor.ui.panels.editor;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import javax.swing.AbstractButton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.basic.BasicComboBoxRenderer;

import org.apache.log4j.Logger;
import org.nuclos.client.NuclosIcons;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.datasource.DatasourceDelegate;
import org.nuclos.client.layout.wysiwyg.WYSIWYGMetaInformation;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels.VALUELIST_PROVIDER_EDITOR;
import org.nuclos.client.layout.wysiwyg.component.AbstractWYSIWYGTableColumn;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableComponent;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyValueString;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.elementalcomponents.AddRemoveRowsFromPanel;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.elementalcomponents.AddRemoveRowsFromPanel.AddRemoveButtonControllable;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.elementalcomponents.SaveAndCancelButtonPanel;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.elementalcomponents.SaveAndCancelButtonPanel.SaveAndCancelButtonPanelControllable;
import org.nuclos.client.layout.wysiwyg.editor.util.InterfaceGuidelines;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.WYSIWYGParameter;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.WYSIWYGValuelistProvider;
import org.nuclos.client.ui.Errors;
import org.nuclos.common.E;
import org.nuclos.common.NuclosVLPException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.querybuilder.DatasourceUtils;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.attribute.valueobject.AttributeCVO;

import info.clearthought.layout.TableLayout;
import info.clearthought.layout.TableLayoutConstraints;

/**
 *
 *
 *
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author <a href="mailto:hartmut.beckschulze@novabit.de">hartmut.beckschulze</a>
 * @version 01.00.00
 */
public class ValueListProviderEditor extends JDialog implements SaveAndCancelButtonPanelControllable {

	private static final Logger LOG = Logger.getLogger(ValueListProviderEditor.class);

	private final int HEIGHT = 300;
	private final int WIDTH = 650;

	private final JLabel lblType = new JLabel(VALUELIST_PROVIDER_EDITOR.LABEL_VALUELIST_PROVIDER_NAME);
	private final JComboBox cbxType = new JComboBox();
	private final JLabel lblIdField = new JLabel(VALUELIST_PROVIDER_EDITOR.LABEL_PARAMETER_ID);
	private final JComboBox cbxIdField = new JComboBox();
	private final JLabel lblNameField = new JLabel(VALUELIST_PROVIDER_EDITOR.LABEL_PARAMETER_NAME);
	private final JComboBox cbxNameField = new JComboBox();
	private final JLabel lblDefaultMarkerField = new JLabel(VALUELIST_PROVIDER_EDITOR.LABEL_PARAMETER_DEFAULTMARKER);
	private final JComboBox cbxDefaultMarkerField = new JComboBox();

	private final JLabel lblEntity = new JLabel(VALUELIST_PROVIDER_EDITOR.LABEL_ENTITY);
	private final JTextField tfEntity = new JTextField();
	private final JLabel lblField = new JLabel(VALUELIST_PROVIDER_EDITOR.LABEL_FIELD);
	private final JTextField tfField = new JTextField();

	private final JPanel parameterContainer = new JPanel();;
	private final JPanel valuecontainer = new JPanel();

	private WYSIWYGValuelistProvider wysiwygStaticValuelistProvider;

	private WYSIWYGValuelistProvider backupWYSIWYGStaticValuelistProvider;

	public static WYSIWYGValuelistProvider returnValuelistProvider;

	private final JButton btnAddParameter = new JButton(VALUELIST_PROVIDER_EDITOR.BUTTON_ADD_PARAMETER);

	/**
	 * Introduced with 1b6543e7 https://support.novabit.de/browse/RSWORGA-169 (tp)
	 */
	private final UID uidField;

	/**
	 * ATTENTION:
	 * <code>WYSIWYGComponent c</code> might be null, see 
	 * {@link #showEditor(WYSIWYGValuelistProvider)}. (tp)
	 * 
	 * @param c
	 * @param wysiwygStaticValuelistProvider
	 */
	private ValueListProviderEditor(final WYSIWYGComponent c, WYSIWYGValuelistProvider wysiwygStaticValuelistProvider) {
		if (c != null &&
				c.getProperties().getProperty(WYSIWYGComponent.PROPERTY_UID) != null &&
				c.getProperties().getProperty(WYSIWYGComponent.PROPERTY_UID).getValue() != null) {
			this.uidField = UID.parseUID(c.getProperties().getProperty(WYSIWYGComponent.PROPERTY_UID).getValue().toString());
		} else {
			this.uidField = null;
		}
		
		parameterContainer.setLayout(new TableLayout(new double[][]{{TableLayout.FILL}, {}}));

		this.setIconImage(NuclosIcons.getInstance().getScaledDialogIcon(48).getImage());
		//NUCLEUSINT-312
		setTitle(VALUELIST_PROVIDER_EDITOR.TITLE_VALUELIST_PROVIDER_EDITOR);
		//TODO align relative to parent Component
		this.wysiwygStaticValuelistProvider = wysiwygStaticValuelistProvider;
		double[][] layout = {{InterfaceGuidelines.MARGIN_LEFT, TableLayout.FILL, InterfaceGuidelines.MARGIN_RIGHT}, {InterfaceGuidelines.MARGIN_TOP, TableLayout.PREFERRED, InterfaceGuidelines.MARGIN_BETWEEN, TableLayout.FILL, TableLayout.PREFERRED}};
		this.setLayout(new TableLayout(layout));

		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

		cbxType.setPreferredSize(new Dimension(250, 20));
		cbxType.setEditable(true);
		cbxType.setRenderer(new BasicComboBoxRenderer(){

			@Override
			public Component getListCellRendererComponent(JList list,
				Object value, int index, boolean isSelected, boolean cellHasFocus) {
				Component c = super.getListCellRendererComponent(list, value, index, isSelected,
					cellHasFocus);
				if ((VALUELIST_PROVIDER_EDITOR.SEPARATOR_DATASOURCES.equals(((JLabel)c).getText())) ||
						 VALUELIST_PROVIDER_EDITOR.SEPARATOR_SYSTEM.equals(((JLabel)c).getText())) {
					c.setEnabled(false);
					c.setBackground(list.getBackground());
				} else {
					c.setEnabled(true);
				}
				return c;
			}
		});
		cbxType.setModel(new DefaultComboBoxModel(){

			@Override
			public void setSelectedItem(Object anObject) {
				if (VALUELIST_PROVIDER_EDITOR.SEPARATOR_DATASOURCES.equals(anObject) ||
						VALUELIST_PROVIDER_EDITOR.SEPARATOR_SYSTEM.equals(anObject)){
					return;
				}
				super.setSelectedItem(anObject);
			}
		});

		initTypes();
		if (this.wysiwygStaticValuelistProvider.getType() != null) {
			// Find input in list and select it...
			for (int i  = 0; i < cbxType.getItemCount(); i++) {
				final String sValue = this.wysiwygStaticValuelistProvider.getValue();
				final ValuelistProviderVO.Type type = this.wysiwygStaticValuelistProvider.getType();
				if (LangUtils.equal(type,  ValuelistProviderVO.Type.DS)) {
					if (cbxType.getItemAt(i) instanceof ValuelistProviderVO 
							&& UID.parseUID(sValue).equals(((ValuelistProviderVO)cbxType.getItemAt(i)).getId())) {
						cbxType.setSelectedIndex(i);
					}
				} else {
					if (sValue.equals(cbxType.getItemAt(i).toString())) {
						cbxType.setSelectedIndex(i);
					}
				}
			}
		}

		((JTextField)cbxType.getEditor().getEditorComponent()).getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {changeValueForType();}
			@Override
			public void insertUpdate(DocumentEvent e) {changeValueForType();}
			@Override
			public void changedUpdate(DocumentEvent e) {changeValueForType();}
		});
		cbxType.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {changeValueForType();}
		});

		/**
		 * the valuelist provider type panel
		 */
		valuecontainer.setLayout(new TableLayout(new double[][]{{TableLayout.PREFERRED, InterfaceGuidelines.MARGIN_BETWEEN, TableLayout.PREFERRED, TableLayout.FILL, TableLayout.PREFERRED},
				{TableLayout.PREFERRED, InterfaceGuidelines.MARGIN_BETWEEN, TableLayout.PREFERRED, InterfaceGuidelines.MARGIN_BETWEEN, TableLayout.PREFERRED, InterfaceGuidelines.MARGIN_BETWEEN, TableLayout.PREFERRED, InterfaceGuidelines.MARGIN_BETWEEN, TableLayout.PREFERRED, InterfaceGuidelines.MARGIN_BETWEEN, TableLayout.PREFERRED}}));
		valuecontainer.add(lblType, "0,0");
		TableLayoutConstraints constraint = new TableLayoutConstraints(2, 0, 2, 0, TableLayout.FULL, TableLayout.CENTER);
		valuecontainer.add(cbxType, constraint);
		cbxIdField.setPreferredSize(new Dimension(250, 20));
		cbxNameField.setPreferredSize(new Dimension(250, 20));
		cbxDefaultMarkerField.setPreferredSize(new Dimension(250, 20));
		tfEntity.setPreferredSize(new Dimension(250, 20));
		tfField.setPreferredSize(new Dimension(250, 20));

		btnAddParameter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addParameterPanelIntoPanel(null, true, true);
			}
		});
		valuecontainer.add(btnAddParameter, "4,0");

		initFields();
		this.add(valuecontainer, "1,1");

		/**
		 * the parameters
		 */
		JScrollPane scrollbar = new JScrollPane(parameterContainer);
		scrollbar.getVerticalScrollBar().setVisible(true);
		scrollbar.getVerticalScrollBar().setUnitIncrement(20);
		this.add(scrollbar, "1,3");

		/**
		 * save and cancel
		 */

		JButton deleteValueList = new JButton(VALUELIST_PROVIDER_EDITOR.BUTTON_DELETE_VALUELIST_PROVIDER);
		deleteValueList.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				clearValuelistProviderForComponent();
				
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						try {
							c.getParentEditor().getController().safePendingPropertyChanges(true);		
						} catch (Exception e) {
							// ignore. if something went wrong it will be shown in safePendingPropertyChanges
						}	
					}
				});
			}

		});

		constraint = new TableLayoutConstraints(0, 4, 2, 4);
		ArrayList<AbstractButton> buttons = new ArrayList<AbstractButton>(1);
		buttons.add(deleteValueList);
		this.add(new SaveAndCancelButtonPanel(parameterContainer.getBackground(), this, c, buttons), constraint);

		try {
			backupWYSIWYGStaticValuelistProvider = (WYSIWYGValuelistProvider) wysiwygStaticValuelistProvider.clone();
		} catch (CloneNotSupportedException e1) {
			/** nothing to do, does support clone() */
		}
		
		Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		int x = (screenSize.width - WIDTH) / 2;
		int y = (screenSize.height - HEIGHT) / 2;

		this.setBounds(x, y, WIDTH, HEIGHT);
		// get the editor to where it belongs, not on the other screen...
		if (c != null)
			setLocationRelativeTo(c.getParentEditor().getMainEditorPanel().getTopLevelAncestor());
		this.setModal(true);
		this.setVisible(true);
	}

	private void initTypes() {
		//NUCLEUSINT-1043
		cbxType.addItem("");
		if (wysiwygStaticValuelistProvider.getName() != null &&
				!(wysiwygStaticValuelistProvider.getName().equals("status")
					|| wysiwygStaticValuelistProvider.getName().equals("parameter")
					|| ValuelistProviderVO.Type.DS.equals(wysiwygStaticValuelistProvider.getType())))
		cbxType.addItem(wysiwygStaticValuelistProvider.getName());
		cbxType.addItem(VALUELIST_PROVIDER_EDITOR.SEPARATOR_SYSTEM);
		cbxType.addItem("status");
		cbxType.addItem("parameters");
		try {
			Collection<ValuelistProviderVO> vpVOs = DatasourceDelegate.getInstance().getAllValuelistProvider();
			List<ValuelistProviderVO> sortedVPVOs = new ArrayList<ValuelistProviderVO>(vpVOs);
			Collections.sort(sortedVPVOs, new Comparator<ValuelistProviderVO>() {
				@Override
				public int compare(ValuelistProviderVO o1, ValuelistProviderVO o2) {
					return o1.getName().compareTo(o2.getName());
				}});
			if (!sortedVPVOs.isEmpty()) {
				cbxType.addItem(VALUELIST_PROVIDER_EDITOR.SEPARATOR_DATASOURCES);
			}
			for (ValuelistProviderVO vpVO : sortedVPVOs) {
				if (vpVO.getValid() != null && vpVO.getValid())
					cbxType.addItem(vpVO);
			}
		}
		catch(CommonPermissionException e) {
			Errors.getInstance().showExceptionDialog(this, e);
		}
	}

	private boolean isSelectedTypeFromDatasource() {
		return cbxType.getEditor().getItem() != null &&
			cbxType.getEditor().getItem() instanceof ValuelistProviderVO;
	}

	private void initFields() {
		boolean showNameField = false;
		boolean showIdField = false;
		boolean showDefaultMarkerField = false;

		//remove parameter
		parameterContainer.removeAll();

		if (isSelectedTypeFromDatasource()) {
			showIdField = true;
			showNameField = true;
			showDefaultMarkerField = true;

			btnAddParameter.setEnabled(false);

			//remove id fields and name fields
			cbxIdField.removeAllItems();
			cbxNameField.removeAllItems();
			cbxDefaultMarkerField.removeAllItems();
			cbxDefaultMarkerField.addItem("");

			tfEntity.setText("");
			tfField.setText("");

			if (wysiwygStaticValuelistProvider.isEntityAndFieldAvailable()) {
				tfEntity.setText(wysiwygStaticValuelistProvider.getEntity()==null?null:wysiwygStaticValuelistProvider.getEntity().getString());
				tfField.setText(wysiwygStaticValuelistProvider.getField()==null?null:wysiwygStaticValuelistProvider.getField().getString());
			}

			ValuelistProviderVO vpVO = (ValuelistProviderVO) cbxType.getEditor().getItem();
			try {
				// add empty id field (for valuelist provider that should generate plain value fields)
				cbxIdField.addItem("");
				// add id fields and name fields

				// only provide columns with valid datatype
				final boolean system = uidField==null?false:(E.isNuclosEntity(MetaProvider.getInstance().getEntityField(uidField).getForeignEntity()));
				final Class<?> clazzPK = system ? String.class : Number.class;
				final List<String> lstIdColumns =  new ArrayList<String>();
				lstIdColumns.addAll(DatasourceDelegate.getInstance().getColumnsFromVLP(vpVO.getId(), clazzPK));
				for (String sColumn : DatasourceUtils.getColumnsWithoutQuotes(lstIdColumns)) {
					cbxIdField.addItem(sColumn);
				}

				cbxNameField.addItem("");
				for (String sColumn : DatasourceUtils.getColumnsWithoutQuotes(
					DatasourceDelegate.getInstance().getColumnsFromVLP(vpVO.getId(), String.class))) {
					cbxNameField.addItem(sColumn);
				}
				
				final List<String> lstDefaultColumns = new ArrayList<String>();

				lstDefaultColumns.addAll(DatasourceDelegate.getInstance().getColumnsFromVLP(vpVO.getId(), Boolean.class));
				// NUCLOS-3321 especially for datasources in PostgreSQL boolean columns pretend to be numeric 
				lstDefaultColumns.addAll(DatasourceDelegate.getInstance().getColumnsFromVLP(vpVO.getId(), Number.class));
				
				for (final String sColumn : DatasourceUtils.getColumnsWithoutQuotes(lstDefaultColumns)) {
					cbxDefaultMarkerField.addItem(sColumn);
				}

				// sync parameters
				List<DatasourceParameterVO> lstParameterVOs = DatasourceDelegate.getInstance().getParametersFromXML(vpVO.getSource());
				Collections.sort(lstParameterVOs, new Comparator<DatasourceParameterVO>() {
					@Override
					public int compare(DatasourceParameterVO o1, DatasourceParameterVO o2) {
						return o2.getParameter().compareToIgnoreCase(o1.getParameter());
					}
				});
				List<WYSIWYGParameter> lstWYSIYWYGParameterToRemove = new ArrayList<WYSIWYGParameter>();
				for (WYSIWYGParameter wysiwygParameter : wysiwygStaticValuelistProvider.getAllWYSIYWYGParameter()) {
					if (ValuelistProviderVO.DATASOURCE_IDFIELD.equals(wysiwygParameter.getParameterName())) {
						cbxIdField.setSelectedItem(extractFieldName(wysiwygParameter.getParameterValue()));
					} else if (ValuelistProviderVO.DATASOURCE_NAMEFIELD.equals(wysiwygParameter.getParameterName())) {
						cbxNameField.setSelectedItem(extractFieldName(wysiwygParameter.getParameterValue()));
					} else if (ValuelistProviderVO.DATASOURCE_DEFAULTMARKERFIELD.equals(wysiwygParameter.getParameterName())) {
						cbxDefaultMarkerField.setSelectedItem(extractFieldName(wysiwygParameter.getParameterValue()));
					} else if (ValuelistProviderVO.DATASOURCE_VALUELISTPROVIDER.equals(wysiwygParameter.getParameterName())) {
						/* do not show in editor */
					} else {
						boolean wysiwygParamFound = false;
						for (DatasourceParameterVO p : lstParameterVOs) {
							if (p.getParameter().equals(wysiwygParameter.getParameterName())) {
								wysiwygParamFound = true;
							}
						}
						if (!wysiwygParamFound) {
							lstWYSIYWYGParameterToRemove.add(wysiwygParameter);
						}
					}
				}
				for (WYSIWYGParameter wysiwygParameterToRemove : lstWYSIYWYGParameterToRemove) {
					wysiwygStaticValuelistProvider.removeWYSIYWYGParameter(wysiwygParameterToRemove);
				}
				for (DatasourceParameterVO paramVO : lstParameterVOs) {
					WYSIWYGParameter wysiwygParameter = null;
					for (WYSIWYGParameter p : wysiwygStaticValuelistProvider.getAllWYSIYWYGParameterSorted(true)) {
						if (paramVO.getParameter().equals(p.getParameterName())) {
							wysiwygParameter = p;
						}
					}
					if (wysiwygParameter == null) {
						wysiwygParameter = new WYSIWYGParameter();
						wysiwygParameter.setParameterName(paramVO.getParameter());
						wysiwygStaticValuelistProvider.addWYSIYWYGParameter(wysiwygParameter);
					}
					addParameterPanelIntoPanel(wysiwygParameter, false, false);
				}

				// if id- and name fields not set via datasources parameters...
				if (wysiwygStaticValuelistProvider.getAllWYSIYWYGParameter().isEmpty()) {
					for (int i = 0; i < cbxIdField.getItemCount(); i++) {
						String  sColumn = (String)cbxIdField.getItemAt(i);
						if (StringUtils.equalsIgnoreCase(sColumn, "intid")) {
							cbxIdField.setSelectedIndex(i);
							break;
						}
					}
					for (int i = 0; i < cbxNameField.getItemCount(); i++) {
						String  sColumn = (String)cbxNameField.getItemAt(i);
						if (!StringUtils.equalsIgnoreCase(sColumn, "intid")) {
							cbxNameField.setSelectedIndex(i);
							break;
						}
					}
				}
			} catch(CommonBusinessException e) {
				Errors.getInstance().showExceptionDialog(this, e);
			}
		} else {
			// no ValuelistProvierVO seletected
			btnAddParameter.setEnabled(true);
			for (WYSIWYGParameter wysiwygParameter : wysiwygStaticValuelistProvider.getAllWYSIYWYGParameterSorted(true)) {
				addParameterPanelIntoPanel(wysiwygParameter, true, true);
			}
		}

		if (showIdField) {
			valuecontainer.add(lblIdField, "0,2");
			valuecontainer.add(cbxIdField, "2,2,l,c");
		} else {
			valuecontainer.remove(lblIdField);
			valuecontainer.remove(cbxIdField);
		}

		if (showNameField) {
			valuecontainer.add(lblNameField, "0,4");
			valuecontainer.add(cbxNameField, "2,4,l,c");
		} else {
			valuecontainer.remove(lblNameField);
			valuecontainer.remove(cbxNameField);
		}

		if (showDefaultMarkerField) {
			valuecontainer.add(lblDefaultMarkerField, "0,6");
			valuecontainer.add(cbxDefaultMarkerField, "2,6,l,c");
		} else {
			valuecontainer.remove(lblDefaultMarkerField);
			valuecontainer.remove(cbxDefaultMarkerField);
		}

		if (wysiwygStaticValuelistProvider.isEntityAndFieldAvailable()) {
			valuecontainer.add(lblEntity, "0,8");
			valuecontainer.add(tfEntity, "2,8,l,c");
			valuecontainer.add(lblField, "0,10");
			valuecontainer.add(tfField, "2,10,l,c");
		}

		parameterContainer.updateUI();
		valuecontainer.updateUI();
	}
	
	private static String extractFieldName(String value) {
		// extract label if no alias is set. we strip something like T1."strname" @see NUCLOS-645
		if (value != null) {
			int idxDot = value.indexOf(".");
			if (idxDot != -1)
				value = value.substring(idxDot + 1);
			value = value.replaceAll("\"", "");				
		}
		return value;
	}

	/**
	 * This Method shows the Editor. <br>
	 * Works like {@link JOptionPane#showInputDialog(Object)}
	 *
	 * @param wysiwygStaticValuelistProvider the Value
	 * @return
	 */
	public static WYSIWYGValuelistProvider showEditor(WYSIWYGValuelistProvider wysiwygStaticValuelistProvider) {
		return showEditor(null, wysiwygStaticValuelistProvider);
	}

	public static WYSIWYGValuelistProvider showEditor(WYSIWYGComponent component, WYSIWYGValuelistProvider wysiwygStaticValuelistProvider) {
		new ValueListProviderEditor(component, wysiwygStaticValuelistProvider);
		//NUCLEUSINT-811
		if (component != null) {
			//validation only within wysiwyg editor
			try {
				validateValuelistProvider(component, returnValuelistProvider);
			} catch (CommonBusinessException e) {
				// Display the Validation Message and redisplay the Valuelistprovidereditor
				Errors.getInstance().showExceptionDialog(null, e);
				new ValueListProviderEditor(component, wysiwygStaticValuelistProvider);
			}
		}
		return returnValuelistProvider;
	}

	/**
	 * This Method validates the {@link AttributeCVO} against the Values set for the Valuelistprovider.
	 *
	 * @param component the {@link WYSIWYGComponent} for comparison
	 * @param valuelistProvider the {@link WYSIWYGValuelistProvider} to check
	 * @throws CommonBusinessException if validation is not successful
	 * NUCLEUSINT-811
	 */
	private static void validateValuelistProvider(WYSIWYGComponent component, WYSIWYGValuelistProvider valuelistProvider) throws NuclosVLPException {
		if(valuelistProvider != null && "parameters".equals(valuelistProvider.getType())) {
			Vector<WYSIWYGParameter> parameters = valuelistProvider.getAllWYSIYWYGParameter();
			boolean showClassFound = false;
			WYSIWYGMetaInformation metainf = component.getParentEditor().getMetaInformation();
			Class<?> javaclass = null;
			if (component instanceof AbstractWYSIWYGTableColumn) {
				CollectableEntityField field = ((AbstractWYSIWYGTableColumn) component).getEntityField();
				javaclass = field.getJavaClass();
			} 
			else {
				String name = ((PropertyValueString) component.getProperties().getProperty(
					WYSIWYGCollectableComponent.PROPERTY_NAME)).getValue();
				javaclass = metainf.getDatatypeForAttribute(name);
			}

			for (WYSIWYGParameter parameter : parameters) {
				if("showClass".equals(parameter.getParameterName())) {
					if(!javaclass.getName().equals(parameter.getParameterValue())) {
						String exception = WYSIWYGStringsAndLabels.partedString(
							WYSIWYGStringsAndLabels.VALUELIST_PROVIDER_EDITOR.VALIDATIONEXCEPTION_PARAMETERS_1,valuelistProvider.getValue(), javaclass.getName(),javaclass.getName(),javaclass.getName());
						throw new NuclosVLPException(exception);
					}
					showClassFound = true;
				}
			}

			if (!showClassFound) {
				if (!javaclass.getName().equals("java.lang.String")) {
					String exception = WYSIWYGStringsAndLabels.partedString(
							WYSIWYGStringsAndLabels.VALUELIST_PROVIDER_EDITOR.VALIDATIONEXCEPTION_PARAMETERS_2,valuelistProvider.getValue(), javaclass.getName(),javaclass.getName());
					throw new NuclosVLPException(exception);
				}
			}
		}
	}

	/**
	 * Remove the {@link WYSIWYGValuelistProvider}.
	 */
	private final void clearValuelistProviderForComponent() {
		boolean isEntityAndFieldAvailable = wysiwygStaticValuelistProvider.isEntityAndFieldAvailable();
		wysiwygStaticValuelistProvider = null;
		backupWYSIWYGStaticValuelistProvider = null;
		// NUCLEUSINT-669
		returnValuelistProvider = new WYSIWYGValuelistProvider(isEntityAndFieldAvailable);
		this.dispose();
	}

	/**
	 * Sets the Type for the {@link WYSIWYGValuelistProvider}
	 */
	private final void changeValueForType() {
		if (cbxType.getEditor().getItem() != null) {
			if (isSelectedTypeFromDatasource()){
				wysiwygStaticValuelistProvider.setName(
						cbxType.getEditor().getItem().toString() + "." + ValuelistProviderVO.Type.DS.getType());
				wysiwygStaticValuelistProvider.setType(ValuelistProviderVO.Type.DS);
				wysiwygStaticValuelistProvider.setValue(
						((ValuelistProviderVO)cbxType.getEditor().getItem()).getId().getStringifiedDefinitionWithEntity(E.VALUELISTPROVIDER));
			} else {
				wysiwygStaticValuelistProvider.setName(cbxType.getEditor().getItem().toString());
				wysiwygStaticValuelistProvider.setType(ValuelistProviderVO.Type.NAMED);
				wysiwygStaticValuelistProvider.setValue(cbxType.getEditor().getItem().toString());
			}
		} else {
			wysiwygStaticValuelistProvider.setName("");
			wysiwygStaticValuelistProvider.setType(null);
			wysiwygStaticValuelistProvider.setValue("");
		}
		for(WYSIWYGParameter param : new Vector<WYSIWYGParameter>(wysiwygStaticValuelistProvider.getAllWYSIYWYGParameter())) {
			wysiwygStaticValuelistProvider.removeWYSIYWYGParameter(param);
		}
		initFields();
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.editor.ui.panels.elementalcomponents.SaveAndCancelButtonPanel.SaveAndCancelButtonPanelControllable#performCancelAction()
	 */
	@Override
	public void performCancelAction() {
		wysiwygStaticValuelistProvider = null;
		wysiwygStaticValuelistProvider = backupWYSIWYGStaticValuelistProvider;
		returnValuelistProvider = backupWYSIWYGStaticValuelistProvider;
		this.dispose();
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.editor.ui.panels.elementalcomponents.SaveAndCancelButtonPanel.SaveAndCancelButtonPanelControllable#performSaveAction()
	 */
	@Override
	public void performSaveAction() {
		if (wysiwygStaticValuelistProvider.isEntityAndFieldAvailable()) {
			if (StringUtils.looksEmpty(tfEntity.getText()) || StringUtils.looksEmpty(tfField.getText())) {
				JOptionPane.showMessageDialog(this, VALUELIST_PROVIDER_EDITOR.MESSAGE_ENTITYAND_FIELD_NOT_NULL);
				return;
			}
			wysiwygStaticValuelistProvider.setEntity(new UID(tfEntity.getText()));
			wysiwygStaticValuelistProvider.setField(new UID(tfField.getText()));
		}

		if (isSelectedTypeFromDatasource()) {
			WYSIWYGParameter paramValuelistProviderDatasource = null;
			WYSIWYGParameter parameterIdField = null;
			WYSIWYGParameter parameterNameField = null;
			WYSIWYGParameter parameterDefaultMarkerField = null;

			for (WYSIWYGParameter wysiywygParameter : wysiwygStaticValuelistProvider.getAllWYSIYWYGParameter()) {
				if (ValuelistProviderVO.DATASOURCE_VALUELISTPROVIDER.equals(wysiywygParameter.getParameterName())) {
					paramValuelistProviderDatasource = wysiywygParameter;
				} else if (ValuelistProviderVO.DATASOURCE_IDFIELD.equals(wysiywygParameter.getParameterName())) {
					parameterIdField = wysiywygParameter;
				} else if (ValuelistProviderVO.DATASOURCE_NAMEFIELD.equals(wysiywygParameter.getParameterName())) {
					parameterNameField = wysiywygParameter;
				} else if (ValuelistProviderVO.DATASOURCE_DEFAULTMARKERFIELD.equals(wysiywygParameter.getParameterName())) {
					parameterDefaultMarkerField = wysiywygParameter;
				}
			}

			if (parameterIdField != null) {
				parameterIdField.setParameterValue(cbxIdField.getSelectedItem().toString());
			} else {
				parameterIdField = new WYSIWYGParameter();
				parameterIdField.setParameterName(ValuelistProviderVO.DATASOURCE_IDFIELD);
				parameterIdField.setParameterValue(cbxIdField.getSelectedItem().toString());
				wysiwygStaticValuelistProvider.addWYSIYWYGParameter(parameterIdField);
			}

			if (cbxNameField.getSelectedItem() != null) {
				if (parameterNameField != null) {
					parameterNameField.setParameterValue(cbxNameField.getSelectedItem().toString());
				} else {
					parameterNameField = new WYSIWYGParameter();
					parameterNameField.setParameterName(ValuelistProviderVO.DATASOURCE_NAMEFIELD);
					parameterNameField.setParameterValue(cbxNameField.getSelectedItem().toString());
					wysiwygStaticValuelistProvider.addWYSIYWYGParameter(parameterNameField);
				}
			}

			if (!"".equals(cbxDefaultMarkerField.getSelectedItem())) {
				if (parameterDefaultMarkerField != null) {
					parameterDefaultMarkerField.setParameterValue(cbxDefaultMarkerField.getSelectedItem().toString());
				} else {
					parameterDefaultMarkerField = new WYSIWYGParameter();
					parameterDefaultMarkerField.setParameterName(ValuelistProviderVO.DATASOURCE_DEFAULTMARKERFIELD);
					parameterDefaultMarkerField.setParameterValue(cbxDefaultMarkerField.getSelectedItem().toString());
					wysiwygStaticValuelistProvider.addWYSIYWYGParameter(parameterDefaultMarkerField);
				}
			}
			else if (parameterDefaultMarkerField != null) {
				wysiwygStaticValuelistProvider.removeWYSIYWYGParameter(parameterDefaultMarkerField);
			}

			String sDatasource = wysiwygStaticValuelistProvider.getValue();
			if (paramValuelistProviderDatasource != null) {
				paramValuelistProviderDatasource.setParameterValue(sDatasource);
			} else {
				paramValuelistProviderDatasource = new WYSIWYGParameter();
				paramValuelistProviderDatasource.setParameterName(ValuelistProviderVO.DATASOURCE_VALUELISTPROVIDER);
				paramValuelistProviderDatasource.setParameterValue(sDatasource);
				wysiwygStaticValuelistProvider.addWYSIYWYGParameter(paramValuelistProviderDatasource);
			}
		}
		returnValuelistProvider = wysiwygStaticValuelistProvider;
		if (returnValuelistProvider.getType().equals(""))
			performCancelAction();
		else
			this.dispose();
	}

	/**
	 * This Method removes a {@link ParameterPanel} from the {@link ValueListProviderEditor}.
	 * @param parameterPanel to be removed
	 */
	public void removeParameterFromPanel(ParameterPanel parameterPanel) {
		this.wysiwygStaticValuelistProvider.removeWYSIYWYGParameter(parameterPanel.getWYSIWYGParameter());
		TableLayout tablelayout = (TableLayout) parameterContainer.getLayout();
		TableLayoutConstraints constraint = tablelayout.getConstraints(parameterPanel);
		int row = constraint.row1;
		if (row - 1 < 0)
			row = 0;
		else
			row = row - 1;

		parameterContainer.remove(parameterPanel);
		removeOneRow(row);
		parameterContainer.updateUI();
	}

	/**
	 * Add a new Parameter to the {@link ValueListProviderEditor}.
	 * @param wysiwygParameter the {@link WYSIWYGParameter} which will be added (and wrapped in a {@link ParameterPanel})
	 */
	public void addParameterPanelIntoPanel(WYSIWYGParameter wysiwygParameter, boolean showButtons, boolean enableName) {
		WYSIWYGParameter newParameter = wysiwygParameter;
		if (newParameter == null) {
			newParameter = new WYSIWYGParameter();
			wysiwygStaticValuelistProvider.addWYSIYWYGParameter(newParameter);
		}
		ParameterPanel newPanel = new ParameterPanel(newParameter, showButtons, enableName);

		expandLayout();
		parameterContainer.add(newPanel, "0,0");
		parameterContainer.updateUI();
	}

	/**
	 * Small Helpermethod for expanding the Layout to be capable to add another {@link ParameterPanel}<br>
	 * Called by {@link #addParameterPanelIntoPanel(WYSIWYGParameter)}
	 */
	private void expandLayout() {
		TableLayout tablelayout = (TableLayout) parameterContainer.getLayout();
		tablelayout.insertRow(0, InterfaceGuidelines.MARGIN_BETWEEN);
		tablelayout.insertRow(0, TableLayout.PREFERRED);
	}

	/**
	 * Removes a row from the Layout<br>
	 * Called by {@link #removeParameterFromPanel(ParameterPanel)}
	 * @param row
	 */
	private void removeOneRow(int row) {
		TableLayout tablelayout = (TableLayout) parameterContainer.getLayout();
		tablelayout.deleteRow(row);
		tablelayout.deleteRow(row);
	}

	/**
	 * This Class wraps a {@link WYSIWYGParameter}.
	 *
	 * <br>
	 * Created by Novabit Informationssysteme GmbH <br>
	 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
	 *
	 * @author <a href="mailto:hartmut.beckschulze@novabit.de">hartmut.beckschulze</a>
	 * @version 01.00.00
	 */
	class ParameterPanel extends JPanel implements AddRemoveButtonControllable {

		WYSIWYGParameter wysiwygParameter = null;
		private JTextField txtName = null;
		private JTextField txtValue = null;

		/**
		 * @param wysiwygParameter the {@link WYSIWYGParameter} to be represented by {@link ParameterPanel}
		 */
		public ParameterPanel(WYSIWYGParameter wysiwygParameter, boolean showButtons, boolean enableName) {
			this.wysiwygParameter = wysiwygParameter;
			double[][] layout = {{InterfaceGuidelines.MARGIN_LEFT, TableLayout.PREFERRED, InterfaceGuidelines.MARGIN_BETWEEN, TableLayout.PREFERRED, InterfaceGuidelines.MARGIN_BETWEEN, TableLayout.PREFERRED, InterfaceGuidelines.MARGIN_BETWEEN, TableLayout.PREFERRED, InterfaceGuidelines.MARGIN_BETWEEN, TableLayout.PREFERRED}, {InterfaceGuidelines.MARGIN_BETWEEN, TableLayout.PREFERRED}};

			this.setLayout(new TableLayout(layout));
			JLabel lblName = new JLabel("Parameter Name:");
			JLabel lblValue = new JLabel("Parameter Value:");

			txtName = new JTextField(15);
			txtName.setEnabled(enableName);
			txtName.setText(wysiwygParameter.getParameterName());
			txtName.addFocusListener(new FocusListener() {
				@Override
				public void focusGained(FocusEvent e) {
				}

				@Override
				public void focusLost(FocusEvent e) {
					changeValueForName();
				}
			});

			txtValue = new JTextField(15);
			txtValue.setText(wysiwygParameter.getParameterValue());
			txtValue.addFocusListener(new FocusListener() {
				@Override
				public void focusGained(FocusEvent e) {
				}

				@Override
				public void focusLost(FocusEvent e) {
					changeValueForValue();
				}
			});

			this.add(lblName, "1,1");
			TableLayoutConstraints constraint = new TableLayoutConstraints(3, 1, 3, 1, TableLayout.FULL, TableLayout.CENTER);
			this.add(txtName, constraint);
			this.add(lblValue, "5,1");
			constraint = new TableLayoutConstraints(7, 1, 7, 1, TableLayout.FULL, TableLayout.CENTER);
			this.add(txtValue, constraint);

			if (showButtons){
				this.add(new AddRemoveRowsFromPanel(this.getBackground(), this), "9,1");
			}
		}

		/**
		 * This method changes the Parametervalue from the Textfield
		 */
		protected void changeValueForValue() {
			this.wysiwygParameter.setParameterValue(this.txtValue.getText());
			LOG.debug(this.wysiwygParameter.getParameterValue());
		}

		/**
		 * This method changes the Parametername from the Textfield
		 */
		protected void changeValueForName() {
			this.wysiwygParameter.setParameterName(this.txtName.getText());
			LOG.debug(this.wysiwygParameter.getParameterName());
		}

		/*
		 * (non-Javadoc)
		 * @see org.nuclos.client.layout.wysiwyg.editor.ui.panels.elementalcomponents.AddRemoveRowsFromPanel.AddRemoveButtonControllable#performAddAction()
		 */
		@Override
		public void performAddAction() {
			ValueListProviderEditor.this.addParameterPanelIntoPanel(null, true, true);
		}

		/*
		 * (non-Javadoc)
		 * @see org.nuclos.client.layout.wysiwyg.editor.ui.panels.elementalcomponents.AddRemoveRowsFromPanel.AddRemoveButtonControllable#performRemoveAction()
		 */
		@Override
		public void performRemoveAction() {
			ValueListProviderEditor.this.removeParameterFromPanel(this);
		}

		/**
		 * @return the {@link WYSIWYGParameter} attached to this {@link ParameterPanel}
		 */
		public WYSIWYGParameter getWYSIWYGParameter() {
			return this.wysiwygParameter;
		}
	}
}
