//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.layout.wysiwyg.editor.util.mouselistener;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;

import org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGStaticButton;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGStaticComboBox;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGStaticLabel;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGStaticSeparator;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGStaticTextfield;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGStaticTitledSeparator;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGUniversalComponent;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertiesPanel;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyValueInteger;
import org.nuclos.client.layout.wysiwyg.editor.util.DnDUtil;
import org.nuclos.client.layout.wysiwyg.editor.util.InterfaceGuidelines;
import org.nuclos.client.layout.wysiwyg.editor.util.TableLayoutUtil;
import org.nuclos.client.layout.wysiwyg.editor.util.mouselistener.AbstractControlledListener.ActionToPerform;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.LayoutCell;

/**
 * Small {@link MouseListener} class adding opening {@link PropertiesPanel} on double click on a {@link WYSIWYGComponent}
 * 
 * NUCLEUSINT-556
 * @author hartmut.beckschulze
 *
 */
public class PropertiesDisplayMouseListener implements MouseListener, MouseMotionListener {
	
	private WYSIWYGComponent component;
	private TableLayoutUtil tableLayoutUtil;
	
	private PropertiesPanel propertiesPanel; 
	
	private LayoutCell cellToResize;
	
	private static boolean blnIsPerfomingAction = false;
	
	private static boolean blnMouseDragged = false;
	
	public static boolean isPerfomingAction() {
		return blnIsPerfomingAction;
	}
	
	public PropertiesDisplayMouseListener(WYSIWYGComponent component, TableLayoutUtil tableLayoutUtil) {
		this.component = component;
		this.tableLayoutUtil = tableLayoutUtil;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if(e.getClickCount() == 1 && 
				(e.getButton() == MouseEvent.BUTTON1 || e.getButton() == MouseEvent.BUTTON3)) {
			// NUCLOSINT-681
			propertiesPanel = PropertiesPanel.checkIfAlreadyShowingForComponent(component);
			if (propertiesPanel == null) {
				propertiesPanel = PropertiesPanel.showPropertiesForComponent(component, tableLayoutUtil);
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		if (isPerfomingAction())
			return;

		Component c = e.getComponent();
		if (component.isSelected()) {
			c.removeMouseMotionListener(this);
			c.addMouseMotionListener(this);
		}
	}

	@Override
	public void mouseExited(MouseEvent e) {
		if (!blnMouseDragged) {
			Component c = e.getComponent();
			if (!component.isSelected()) {
				c.removeMouseMotionListener(this);
			}

			setActionToPerform(ActionToPerform.NOTHING_TO_DO);
			
			cellToResize = null;

			blnIsPerfomingAction = false;
			e.getComponent().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}

	}

	@Override
	public void mousePressed(MouseEvent e) {
		 // NUCLEUSINT-992
		((JComponent)component).requestFocusInWindow();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		Component c = e.getComponent();

		if(e.getClickCount() == 1 && 
				(e.getButton() == MouseEvent.BUTTON1 || e.getButton() == MouseEvent.BUTTON3)) {
			// NUCLOSINT-681
			propertiesPanel = PropertiesPanel.checkIfAlreadyShowingForComponent(component);
			if (propertiesPanel == null) {
				propertiesPanel = PropertiesPanel.showPropertiesForComponent(component, tableLayoutUtil);
			}
		}

		setActionToPerform(ActionToPerform.NOTHING_TO_DO);
		
		blnIsPerfomingAction = false;
		blnMouseDragged = false;

		cellToResize = null;
		
		c.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		Component c = e.getComponent();
		if (!component.isSelected()) {
			;//c.removeMouseMotionListener(this);
			return;
		}
		
		setActionToPerform(getActionToPerformForJPanel(e));
		
		propertiesPanel = PropertiesPanel.checkIfAlreadyShowingForComponent(component);
		if (propertiesPanel == null) {
			propertiesPanel = PropertiesPanel.showPropertiesForComponent(component, tableLayoutUtil);
		}
		
		switch (getActionToPerform()) {
			case PANEL_ALTER_RIGHT :
				blnIsPerfomingAction = true;
				c.setCursor(Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR));
				break;
			case PANEL_ALTER_LEFT :
				blnIsPerfomingAction = true;
				c.setCursor(Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR));
				break;
			case PANEL_ALTER_TOP :
				blnIsPerfomingAction = true;
				c.setCursor(Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR));
				break;
			case PANEL_ALTER_BOTTOM :
				blnIsPerfomingAction = true;
				c.setCursor(Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR));
				break;
			case PANEL_ALTER_NE :
				blnIsPerfomingAction = true;
				c.setCursor(Cursor.getPredefinedCursor(Cursor.NE_RESIZE_CURSOR));
				break;
			case PANEL_ALTER_NW :
				blnIsPerfomingAction = true;
				c.setCursor(Cursor.getPredefinedCursor(Cursor.NW_RESIZE_CURSOR));
				break;
			case PANEL_ALTER_SW :
				blnIsPerfomingAction = true;
				c.setCursor(Cursor.getPredefinedCursor(Cursor.SW_RESIZE_CURSOR));
				break;
			case PANEL_ALTER_SE :
				blnIsPerfomingAction = true;
				c.setCursor(Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR));
				break;
			default :
				blnIsPerfomingAction = false;
				c.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				break;
		}
		
		if (e.getPoint() != null) {
			tableLayoutUtil.setCellForEditing(e.getPoint());
			cellToResize = tableLayoutUtil.getCellForEditing();
		} else {
			cellToResize = null;
		}
	}

	private ActionToPerform actionToPerform;

	/**
	 * Setting the {@link ActionToPerform}
	 * 
	 * @param actionToPerform
	 */
	public void setActionToPerform(ActionToPerform actionToPerform) {
		this.actionToPerform = actionToPerform;
	}

	/**
	 * @return {@link ActionToPerform} that was set
	 */
	public ActionToPerform getActionToPerform() {
		return actionToPerform;
	}

	/**
	 * This Method controls what action has do be performed
	 * @param e
	 * @return {@link ActionToPerform} with the action found
	 */
	public ActionToPerform getActionToPerformForJPanel(MouseEvent e) {
		if (e.getPoint() == null)
			return ActionToPerform.NOTHING_TO_DO;
		
		if (DnDUtil.isPerfomingAction())
			return ActionToPerform.NOTHING_TO_DO;
		
		if (component instanceof WYSIWYGUniversalComponent)
			return ActionToPerform.NOTHING_TO_DO;

		/** prevent resizing if mouse is outside layout */
		if (e.getPoint().x > tableLayoutUtil.getCalculatedLayoutWidth() + InterfaceGuidelines.SENSITIVITY)
			return ActionToPerform.NOTHING_TO_DO;
		if (e.getPoint().y > tableLayoutUtil.getCalculatedLayoutHeight() + InterfaceGuidelines.SENSITIVITY)
			return ActionToPerform.NOTHING_TO_DO;

		Component current = e.getComponent();
		/*if (current instanceof WYSIWYGTabbedPane) {
			for (int i = 0; i < ((WYSIWYGTabbedPane)current).getComponentCount(); i++) {
				if (((WYSIWYGTabbedPane)current).getComponent(i).getClass().getName().endsWith("TabContainer")) {
					current = ((WYSIWYGTabbedPane)current).getComponent(i);
					break;
				}
			}
		}*/
		int leftSide = current.getX();
		int top = current.getY();
		int rightSide = current.getX() + current.getWidth();
		int bottom = current.getY() + current.getHeight();

		Point p = e.getPoint();
		if (component instanceof WYSIWYGStaticSeparator
				|| component instanceof WYSIWYGStaticTitledSeparator
				//|| component instanceof WYSIWYGStaticComboBox
				|| component instanceof WYSIWYGStaticTextfield
				|| component instanceof WYSIWYGStaticButton
				|| component instanceof WYSIWYGStaticLabel)
			p = SwingUtilities.convertPoint(e.getComponent(), p, ((WYSIWYGComponent)component).getParentEditor().getTableLayoutUtil().getContainer());
		if (component instanceof WYSIWYGStaticComboBox && e.getComponent() instanceof AbstractButton)
			p = SwingUtilities.convertPoint(e.getComponent(), p, ((Component)component));

		if (isInRangeOf(InterfaceGuidelines.SENSITIVITY, leftSide, p.x) && isInRangeOf(InterfaceGuidelines.SENSITIVITY, top, p.y)) {
			if (e.getX() < InterfaceGuidelines.MARGIN_LEFT && e.getY() < InterfaceGuidelines.MARGIN_TOP) {
				return ActionToPerform.PANEL_ALTER_NW;
			}
		}
		if (isInRangeOf(InterfaceGuidelines.SENSITIVITY, leftSide, p.x) && isInRangeOf(InterfaceGuidelines.SENSITIVITY, bottom, p.y)) {
			if (e.getX() < InterfaceGuidelines.MARGIN_LEFT && e.getY() > InterfaceGuidelines.MARGIN_BOTTOM) {
				return ActionToPerform.PANEL_ALTER_SW;
			}
		}
		if (isInRangeOf(InterfaceGuidelines.SENSITIVITY, rightSide, p.x) && isInRangeOf(InterfaceGuidelines.SENSITIVITY, top, p.y)) {
			if (e.getX() > InterfaceGuidelines.MARGIN_RIGHT && e.getY() < InterfaceGuidelines.MARGIN_TOP) {
				return ActionToPerform.PANEL_ALTER_NE;
			}
		}
		if (isInRangeOf(InterfaceGuidelines.SENSITIVITY, rightSide, p.x) && isInRangeOf(InterfaceGuidelines.SENSITIVITY, bottom, p.y)) {
			if (e.getX() > InterfaceGuidelines.MARGIN_RIGHT && e.getY() > InterfaceGuidelines.MARGIN_TOP) {
				return ActionToPerform.PANEL_ALTER_SE;
			}
		}

		if (isInRangeOf(InterfaceGuidelines.SENSITIVITY, leftSide, p.x)) {
			if (e.getX() < InterfaceGuidelines.MARGIN_LEFT)
				return ActionToPerform.PANEL_ALTER_LEFT;
		} else if (isInRangeOf(InterfaceGuidelines.SENSITIVITY, bottom, p.y)) {
			if (e.getY() > InterfaceGuidelines.MARGIN_BOTTOM) {
				return ActionToPerform.PANEL_ALTER_BOTTOM;
			}
		}
		
		if (isInRangeOf(InterfaceGuidelines.SENSITIVITY, rightSide, p.x)) {
			if (e.getX() > InterfaceGuidelines.MARGIN_RIGHT)
//				System.out.println(ActionToPerform.PANEL_ALTER_RIGHT + " top=" + top + " y=" + p.y + " right=" + rightSide + " x="+ p.x);
				return ActionToPerform.PANEL_ALTER_RIGHT;
		} else if (isInRangeOf(InterfaceGuidelines.SENSITIVITY, top, p.y)) {
			if (e.getY() < InterfaceGuidelines.MARGIN_TOP) {
//				System.out.println(ActionToPerform.PANEL_ALTER_TOP + " top=" + top + " y=" + p.y + " right=" + rightSide + " x="+ p.x);
				return ActionToPerform.PANEL_ALTER_TOP;
			}
		}
		return ActionToPerform.NOTHING_TO_DO;
	}	
	
	/**
	 * This Method checks if the MouseCursor is in the Range of something
	 * @param RANGE the Sensitivity to use {@link InterfaceGuidelines#SENSITIVITY}
	 * @param align_to the Point that should be checked
	 * @param align_what normally the MousePosition
	 * @return true if its in the near of the Point
	 */
	public boolean isInRangeOf(final int RANGE, int align_to, int align_what) {
		if (align_to >= align_what - RANGE && align_to <= align_what + RANGE)
			return true;
		else
			return false;
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		blnMouseDragged = true;
		tableLayoutUtil.getContainer().setCursor(e.getComponent().getCursor());
		
		Component o = e.getComponent();
		WYSIWYGComponent c = null;
		while (c == null || o == null)
		{
			if (o instanceof WYSIWYGComponent)
				c = (WYSIWYGComponent)o;
			if (o != null)
				o = o.getParent();
		}
		
		component = c;
		
		Point p = e.getPoint();
		final LayoutCell cell;
		p = SwingUtilities.convertPoint(e.getComponent(), p, tableLayoutUtil.getContainer());
		cell = tableLayoutUtil.getLayoutCell(p);
		
		if (cellToResize != null && cell != null && propertiesPanel != null) {
			try {
				boolean bChanged = false;
				switch (getActionToPerform()) {
					case PANEL_ALTER_RIGHT :
						Integer col1 = (Integer)propertiesPanel.getPropertyValue(WYSIWYGComponent.CONSTRAINT_COL1).getValue();
						if (cell.getCellX()-col1 >= 0 && cellToResize.getCellX() != cell.getCellX()) {
							propertiesPanel.setPropertyValue(WYSIWYGComponent.CONSTRAINT_COL2, new PropertyValueInteger(cell.getCellX()));
							bChanged = true;
						}
						break;
					case PANEL_ALTER_LEFT :
						col1 = (Integer)propertiesPanel.getPropertyValue(WYSIWYGComponent.CONSTRAINT_COL2).getValue();
						if (col1-cell.getCellX() >= 0 && cellToResize.getCellX() != cell.getCellX()) {
							propertiesPanel.setPropertyValue(WYSIWYGComponent.CONSTRAINT_COL1, new PropertyValueInteger(cell.getCellX()));
							bChanged = true;
						}
						break;
					case PANEL_ALTER_TOP :
						Integer row1 = (Integer)propertiesPanel.getPropertyValue(WYSIWYGComponent.CONSTRAINT_ROW2).getValue();
						if (row1-cell.getCellY() >= 0 && cellToResize.getCellY() != cell.getCellY()) {
							propertiesPanel.setPropertyValue(WYSIWYGComponent.CONSTRAINT_ROW1, new PropertyValueInteger(cell.getCellY()));
							bChanged = true;
						}
						break;
					case PANEL_ALTER_BOTTOM :
						row1 = (Integer)propertiesPanel.getPropertyValue(WYSIWYGComponent.CONSTRAINT_ROW1).getValue();
						if (cell.getCellY()-row1 >= 0 && cellToResize.getCellY() != cell.getCellY()) {
							propertiesPanel.setPropertyValue(WYSIWYGComponent.CONSTRAINT_ROW2, new PropertyValueInteger(cell.getCellY()));
							bChanged = true;
						}
						break;
					case PANEL_ALTER_NE :
						col1 = (Integer)propertiesPanel.getPropertyValue(WYSIWYGComponent.CONSTRAINT_COL1).getValue();
						if (cell.getCellX()-col1 >= 0 && cellToResize.getCellX() != cell.getCellX()) {
							propertiesPanel.setPropertyValue(WYSIWYGComponent.CONSTRAINT_COL2, new PropertyValueInteger(cell.getCellX()));
							bChanged = true;
						}
						row1 = (Integer)propertiesPanel.getPropertyValue(WYSIWYGComponent.CONSTRAINT_ROW2).getValue();
						if (row1-cell.getCellY() >= 0 && cellToResize.getCellY() != cell.getCellY()) {
							propertiesPanel.setPropertyValue(WYSIWYGComponent.CONSTRAINT_ROW1, new PropertyValueInteger(cell.getCellY()));
							bChanged = true;
						}
						break;
					case PANEL_ALTER_NW :
						col1 = (Integer)propertiesPanel.getPropertyValue(WYSIWYGComponent.CONSTRAINT_COL2).getValue();
						if (col1-cell.getCellX() >= 0 && cellToResize.getCellX() != cell.getCellX()) {
							propertiesPanel.setPropertyValue(WYSIWYGComponent.CONSTRAINT_COL1, new PropertyValueInteger(cell.getCellX()));
							bChanged = true;
						}
						row1 = (Integer)propertiesPanel.getPropertyValue(WYSIWYGComponent.CONSTRAINT_ROW2).getValue();
						if (row1-cell.getCellY() >= 0 && cellToResize.getCellY() != cell.getCellY()) {
							propertiesPanel.setPropertyValue(WYSIWYGComponent.CONSTRAINT_ROW1, new PropertyValueInteger(cell.getCellY()));
							bChanged = true;
						}
						break;
					case PANEL_ALTER_SE :
						col1 = (Integer)propertiesPanel.getPropertyValue(WYSIWYGComponent.CONSTRAINT_COL1).getValue();
						if (cell.getCellX()-col1 >= 0 && cellToResize.getCellX() != cell.getCellX()) {
							propertiesPanel.setPropertyValue(WYSIWYGComponent.CONSTRAINT_COL2, new PropertyValueInteger(cell.getCellX()));
							bChanged = true;
						}
						row1 = (Integer)propertiesPanel.getPropertyValue(WYSIWYGComponent.CONSTRAINT_ROW1).getValue();
						if (cell.getCellY()-row1 >= 0 && cellToResize.getCellY() != cell.getCellY()) {
							propertiesPanel.setPropertyValue(WYSIWYGComponent.CONSTRAINT_ROW2, new PropertyValueInteger(cell.getCellY()));
							bChanged = true;
						}
						break;
					case PANEL_ALTER_SW :
						col1 = (Integer)propertiesPanel.getPropertyValue(WYSIWYGComponent.CONSTRAINT_COL2).getValue();
						if (col1-cell.getCellX() >= 0 && cellToResize.getCellX() != cell.getCellX()) {
							propertiesPanel.setPropertyValue(WYSIWYGComponent.CONSTRAINT_COL1, new PropertyValueInteger(cell.getCellX()));
							bChanged = true;
						}
						row1 = (Integer)propertiesPanel.getPropertyValue(WYSIWYGComponent.CONSTRAINT_ROW1).getValue();
						if (cell.getCellY()-row1 >= 0 && cellToResize.getCellY() != cell.getCellY()) {
							propertiesPanel.setPropertyValue(WYSIWYGComponent.CONSTRAINT_ROW2, new PropertyValueInteger(cell.getCellY()));
							bChanged = true;
						}
						break;
					default :
						break;
				}
				if (bChanged) {
					if (component instanceof WYSIWYGStaticSeparator
							|| component instanceof WYSIWYGStaticTitledSeparator)
						;
					else {
						propertiesPanel.setPropertyValue(WYSIWYGComponent.CONSTRAINT_HALIGN, new PropertyValueInteger(2));
						propertiesPanel.setPropertyValue(WYSIWYGComponent.CONSTRAINT_VALIGN, new PropertyValueInteger(2));
					}
					
					cellToResize = tableLayoutUtil.getLayoutCell(p);
					component.getParentEditor().getController().safePendingPropertyChanges(true);
				}
				//else
					//tableLayoutUtil.getContainer().setCursor(DragSource.DefaultMoveNoDrop);
			} catch (Exception e2) {
				// ignore.
			}
		}
	}
}
