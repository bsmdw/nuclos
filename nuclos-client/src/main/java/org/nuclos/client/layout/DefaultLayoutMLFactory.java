//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.layout;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.masterdata.CollectableMasterData;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.masterdata.CollectableMasterDataEntity;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeRemote;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

public class DefaultLayoutMLFactory extends AbstractLayoutMLFactory {

	private final UID layoutUID;

	private final Map<UID, String> attributeGroups;
	
	// former Spring injection
	
	private SpringLocaleDelegate localeDelegate;
	
	private MasterDataFacadeRemote masterDataFacadeRemote;
	
	// end of former Spring injection
	
	public DefaultLayoutMLFactory(UID layoutUID, Map<UID, String> attributeGroups) {
		this.layoutUID = layoutUID;
		this.attributeGroups = attributeGroups;
		
		setSpringLocaleDelegate(SpringApplicationContextHolder.getBean(SpringLocaleDelegate.class));
		setMasterDataFacadeRemote(SpringApplicationContextHolder.getBean(MasterDataFacadeRemote.class));
	}
	
	final void setSpringLocaleDelegate(SpringLocaleDelegate cld) {
		this.localeDelegate = cld;
	}
	
	final SpringLocaleDelegate getSpringLocaleDelegate() {
		return localeDelegate;
	}
	
	final void setMasterDataFacadeRemote(MasterDataFacadeRemote masterDataFacadeRemote) {
		this.masterDataFacadeRemote = masterDataFacadeRemote;
	}

	final MasterDataFacadeRemote getMasterDataFacadeRemote() {
		return masterDataFacadeRemote;
	}

	@Override
	public String getResourceText(String resourceId) {
		return getSpringLocaleDelegate().getResource(resourceId, "");
	}

	@Override
	public Collection<EntityMeta<?>> getEntityMetaData() {
		return MetaProvider.getInstance().getAllEntities();
	}

	/*
	@Override
	public Collection<FieldMeta<?>> getEntityFieldMetaData(UID entityUid) {
		return MetaProvider.getInstance().getAllEntityFieldsByEntity(entityUid).values();
	}
	 */

	@Override
	public Map<UID, String> getAttributeGroups() {
		return attributeGroups;
	}

	@Override
	public UID getLayoutUID() {
		return layoutUID;
	}

	public void createLayout(UID entity, List<FieldMeta<?>> fields, boolean groupAttributes, boolean withSubforms, boolean withEditFields) throws CommonBusinessException {
		final String layoutml = generateLayout(entity, fields, groupAttributes, withSubforms, withEditFields);
		
		final EntityMeta<?> eMeta = MasterDataDelegate.getInstance().getMetaData(entity);
		final EntityMeta<?> masterVO = MasterDataDelegate.getInstance().getMetaData(E.ENTITY.getUID());
		final CollectableMasterDataEntity masterDataEntity = new CollectableMasterDataEntity(masterVO);

		final EntityMeta<?> metaLayout = MasterDataDelegate.getInstance().getMetaData(E.LAYOUT.getUID());
		final CollectableMasterDataEntity masterDataLayout = new CollectableMasterDataEntity(metaLayout);
		final CollectableMasterData<UID> masterDataLayouml = new CollectableMasterData<UID>(
				masterDataEntity, new MasterDataVO<UID>(masterDataLayout.getMeta(), false));
		final MasterDataVO<UID> mdLayout = masterDataLayouml.getMasterDataCVO();
		mdLayout.setFieldValue(E.LAYOUT.name, eMeta.getEntityName());
		mdLayout.setFieldValue(E.LAYOUT.description, eMeta.getEntityName());
		mdLayout.setFieldValue(E.LAYOUT.layoutML, layoutml);
		mdLayout.setFieldUid(E.LAYOUT.nuclet, eMeta.getNuclet());

		EntityMeta<?> metaLayoutUsage = MasterDataDelegate.getInstance().getMetaData(E.LAYOUTUSAGE.getUID());
		CollectableMasterDataEntity masterDataLayoutUsage = new CollectableMasterDataEntity(metaLayoutUsage);
		CollectableMasterData<UID> masterDataLayoumlUsage = new CollectableMasterData<UID>(
				masterDataEntity, new MasterDataVO<UID>(masterDataLayoutUsage.getMeta(), false));
		MasterDataVO<UID> mdLayoutUsage = masterDataLayoumlUsage.getMasterDataCVO();
		DependentDataMap dependMapLayoutUsage = new DependentDataMap();

		mdLayoutUsage.setFieldUid(E.LAYOUTUSAGE.entity, entity);

		mdLayoutUsage.setFieldValue(E.LAYOUTUSAGE.searchScreen, Boolean.FALSE);
		dependMapLayoutUsage.addData(E.LAYOUTUSAGE.layout, mdLayoutUsage.getEntityObject());

		metaLayoutUsage = MasterDataDelegate.getInstance().getMetaData(E.LAYOUTUSAGE.getUID());
		masterDataLayoutUsage = new CollectableMasterDataEntity(metaLayoutUsage);
		masterDataLayoumlUsage = new CollectableMasterData<UID>(
				masterDataEntity, new MasterDataVO<UID>(masterDataLayoutUsage.getMeta(), false));
		mdLayoutUsage = masterDataLayoumlUsage.getMasterDataCVO();

		mdLayoutUsage.setFieldUid(E.LAYOUTUSAGE.entity, entity);

		mdLayoutUsage.setFieldValue(E.LAYOUTUSAGE.searchScreen, Boolean.TRUE);
		dependMapLayoutUsage.addData(E.LAYOUTUSAGE.layout, mdLayoutUsage.getEntityObject());
		
		CollectableComparison compare = SearchConditionUtils.newComparison(E.LAYOUTUSAGE.entity, ComparisonOperator.EQUAL, entity);
		Collection<MasterDataVO<UID>> colLayout = getMasterDataFacadeRemote().getMasterDataWithCheck(E.LAYOUTUSAGE.getUID(), compare, true);

		if(colLayout.size() > 0) {
			final MasterDataVO<UID> voLayoutUsage = colLayout.iterator().next();
			final MasterDataVO<UID> voLayout = MasterDataDelegate.getInstance().get(E.LAYOUT.getUID(), voLayoutUsage.getFieldUid(E.LAYOUTUSAGE.layout));
			
			voLayout.setFieldValue(E.LAYOUT.layoutML.getUID(), layoutml);
			MasterDataDelegate.getInstance().update(E.LAYOUT.getUID(), voLayout, voLayout.getDependents(), 
					ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY), false);
		}
		else {
			MasterDataDelegate.getInstance().create(E.LAYOUT.getUID(), mdLayout, dependMapLayoutUsage, 
					ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
		}
	}

}
