//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.fileimport;

import java.io.Serializable;
import java.util.Map;

import javax.swing.JTable;
import javax.swing.table.TableColumnModel;

import org.nuclos.client.common.DependantCollectableMasterDataMap;
import org.nuclos.client.common.DetailsSubFormController;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.ui.collect.CollectStateAdapter;
import org.nuclos.client.ui.collect.CollectStateEvent;
import org.nuclos.client.ui.collect.component.CollectableCheckBox;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.LabeledCollectableComponentWithVLP;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelAdapter;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelEvent;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.Utils;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.fileimport.ImportMode;
import org.nuclos.common2.KeyEnum;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.fileimport.ejb3.CsvImportFacadeRemote;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * Controller for creation of structure definition for module object import.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Configurable
public class CsvImportStructureCollectController extends AbstractImportStructureCollectController {

	private CollectableComponentModelAdapter modeListener = new CollectableComponentModelAdapter() {
		@Override
		public void collectableFieldChangedInModel(CollectableComponentModelEvent ev) {
			CollectableField field = ev.getNewValue();
			setMode(KeyEnum.Utils.findEnum(ImportMode.class, (String) field.getValue()), true);
		}
	};

	// Spring injection
	
	@Autowired
	CsvImportFacadeRemote importFacadeRemote;
	
	// end of Spring injection

	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton} 
	 * to get an instance.
	 * 
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	public CsvImportStructureCollectController(MainFrameTab tabIfAny) {
		super(E.IMPORT.getUID(), tabIfAny);
		this.getCollectStateModel().addCollectStateListener(new CollectStateAdapter(){
			@Override
            public void detailsModeEntered(CollectStateEvent ev) throws CommonBusinessException {
	            super.detailsModeEntered(ev);
	            if (!SecurityCache.getInstance().isSuperUser()) {
		            for (CollectableComponent c : getDetailCollectableComponentsFor(E.IMPORT.mode.getUID())) {
		            	if (c instanceof LabeledCollectableComponentWithVLP) {
		            		c.setEnabled(false);
		            	}
		            }
	            }
	            getDetailsComponentModel(E.IMPORT.mode.getUID()).addCollectableComponentModelListener(CsvImportStructureCollectController.this, modeListener);
	            setMode(KeyEnum.Utils.findEnum(ImportMode.class, 
	            		(String) getDetailsComponentModel(E.IMPORT.mode.getUID()).getField().getValue()), false);
            }

			@Override
			public void detailsModeLeft(CollectStateEvent ev) throws CommonBusinessException {
				getDetailsComponentModel(E.IMPORT.mode.getUID()).removeCollectableComponentModelListener(modeListener);
			}
		});
	}
	
	/**
	 * set the structures import mode (after selecting an import mode or on entering details view)
	 *
	 * @param mode the new import mode
	 * @param setDelete if the delete option should be set to false (not on entering details mode as the model would be changed)
	 */
	void setMode(ImportMode mode, boolean setDelete) {
		final JTable tbl = getSubFormController(E.IMPORTATTRIBUTE.getUID()).getSubForm().getJTable();
		final TableColumnModel columnmodel = tbl.getColumnModel();

		if (mode != null && ImportMode.DBIMPORT.equals(mode)) {
			for (CollectableComponent c : getDetailCollectableComponentsFor(E.IMPORT.delete.getUID())) {
            	if (c instanceof CollectableCheckBox) {
            		c.setEnabled(false);
            	}
            }
			if (setDelete) {
				getDetailsComponentModel(E.IMPORT.delete.getUID()).setField(new CollectableValueField(Boolean.valueOf(false)));
			}
			if (tablecolumnPreserve == null) {
				boolean found = false;
				for (int index = 0; index < columnmodel.getColumnCount(); index++) {
					if (columnmodel.getColumn(index).getIdentifier().equals(E.IMPORTATTRIBUTE.preserve.getUID())) {
						found = true;
						break;
					}
				}
				if (found) {
					tablecolumnPreserve = tbl.getColumn(E.IMPORTATTRIBUTE.preserve.getUID());
					columnmodel.removeColumn(tablecolumnPreserve);
				}
			}
		}
		else {
			for (CollectableComponent c : getDetailCollectableComponentsFor(E.IMPORT.delete.getUID())) {
            	if (c instanceof CollectableCheckBox) {
            		c.setEnabled(true);
            	}
            }
			if (tablecolumnPreserve != null) {
				columnmodel.addColumn(tablecolumnPreserve);
				tablecolumnPreserve = null;
			}
		}
	}

	@Override
    protected CollectableMasterDataWithDependants<UID> newCollectableWithDefaultValues(boolean forInsert) {
		final CollectableMasterDataWithDependants<UID> result = super.newCollectableWithDefaultValues(forInsert);
		result.setField(E.IMPORT.mode.getUID(), new CollectableValueField(ImportMode.NUCLOSIMPORT.getValue()));
		return result;
    }

	@Override
	public CollectableMasterDataWithDependants<UID> insertCollectable(CollectableMasterDataWithDependants<UID> clctNew) throws CommonBusinessException {
		if (clctNew.getId() != null) {
			throw new IllegalArgumentException("clctNew");
		}

		// We have to clear the ids for cloned objects:
		/**
		 * @todo eliminate this workaround - this is the wrong place. The right
		 *       place is the Clone action!
		 */

		final IDependentDataMap mpmdvoDependants = Utils.clearIds(getAllSubFormData(null).toDependentDataMap());
		final MasterDataVO<UID> mdvoInserted = importFacadeRemote.createImportStructure(new MasterDataVO<UID>(clctNew.getMasterDataCVO(), mpmdvoDependants));
		
		CollectableMasterDataWithDependants<UID> retVal =  new CollectableMasterDataWithDependants<UID>(clctNew.getCollectableEntity(),
				new MasterDataVO<UID>(mdvoInserted, readDependants(mdvoInserted.getId())));
		
		return retVal;

	}

	@Override
	protected CollectableMasterDataWithDependants<UID> updateCollectable(CollectableMasterDataWithDependants<UID> clct, Object oAdditionalData, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		final DependantCollectableMasterDataMap mpclctDependants = (DependantCollectableMasterDataMap)oAdditionalData;

		final UID oId = (UID) importFacadeRemote.modifyImportStructure(
				new MasterDataVO<UID>(clct.getMasterDataCVO(), mpclctDependants.toDependentDataMap()));

		final MasterDataVO<UID> mdvoUpdated = mddelegate.get(getEntityUid(), oId);
		CollectableMasterDataWithDependants<UID> retVal =  new CollectableMasterDataWithDependants<UID>(clct.getCollectableEntity(), 
				new MasterDataVO<UID>(mdvoUpdated, this.readDependants(mdvoUpdated.getId())));
		
		return retVal;
	}

	@Override
	protected void deleteCollectable(CollectableMasterDataWithDependants<UID> clct, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		importFacadeRemote.removeImportStructure(clct.getMasterDataCVO());
	}

	/**
	 * Show all removed columns again, so they can be saved with their width, and are restored when opened again.
	 */
	@Override
	public void close() {
		DetailsSubFormController subFormController = getSubFormController(E.IMPORTATTRIBUTE.getUID());
		if (subFormController != null) {
			final JTable tbl = subFormController.getSubForm().getJTable();
			final TableColumnModel columnmodel = tbl.getColumnModel();
			if (tablecolumnPreserve != null) {
				columnmodel.addColumn(tablecolumnPreserve);
				tablecolumnPreserve = null;
			}
		}
		super.close();
	}
	
}
