//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.client.i18n.language.data;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.border.Border;

import org.nuclos.api.Preferences;
import org.nuclos.api.Property;
import org.nuclos.api.context.LayoutComponentContext;
import org.nuclos.api.ui.layout.LayoutComponent;
import org.nuclos.api.ui.layout.LayoutComponentListener;
import org.nuclos.api.ui.layout.TabController;
import org.nuclos.client.ui.Errors;
import org.nuclos.common.UID;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.server.i18n.language.data.DataLanguageVO;

public class DataLanguageController implements LayoutComponent<UID>, DataLanguageViewListener {

	private LayoutComponentContext context;
	private String sName;
	private Preferences prefs;
	private Collection<LayoutComponentListener<UID>> listener;
	private TabController tabController;
	private DataLanguageView view;
	
	private List<DataLanguageVO> availDataLanguages;
	private List<DataLanguageVO> selectDataLanguages;
	
	public DataLanguageController(LayoutComponentContext context) {
		super();
		this.context = context;
		this.view = createView();
	}

	private DataLanguageView createView() {
		availDataLanguages = getCompleteCopy(DataLanguageDelegate.getInstance().getAllDataLanguages());
		selectDataLanguages = getCompleteCopy(DataLanguageDelegate.getInstance().getSelectedLocales());
		
		this.view = new DataLanguageView(this);
		
		return view;
	}

	@Override
	public JComponent getComponent(
			org.nuclos.api.ui.layout.LayoutComponent.LayoutComponentType type) {
		return this.view;
	}

	@Override
	public EnumSet<LayoutComponentType> getSupportedTypes() {
		return EnumSet.of(LayoutComponentType.DETAIL,
				LayoutComponentType.DESIGN);
	}

	@Override
	public void setPreferences(Preferences prefs) {
		this.prefs = prefs;
	}

	@Override
	public void setName(String name) {
		this.sName = name;
	}

	@Override
	public String getName() {
		return this.sName;
	}

	@Override
	public void setBorder(Border border) {
		this.view.setBorder(border);
	}

	@Override
	public void setPreferredSize(Dimension preferredSize) {
		this.view.setPreferredSize(preferredSize);
	}

	@Override
	public void setProperty(String name, Object value) {
		this.setProperty(name, value);
	}

	@Override
	public void setTabController(TabController tabController) {
		this.tabController = tabController;
	}

	@Override
	public Property[] getComponentProperties() {
		return null;
	}

	@Override
	public String[] getComponentPropertyLabels() {
		return null;
	}

	@Override
	public LayoutComponentContext getContext() {
		return this.context;
	}

	@Override
	public Collection<LayoutComponentListener<UID>> getLayoutComponentListeners() {
		return this.listener;
	}

	@Override
	public void addLayoutComponentListener(LayoutComponentListener<UID> listener) {
		this.listener.add(listener);
	}

	@Override
	public void removeLayoutComponentListener(
			LayoutComponentListener<UID> listener) {
		this.listener.remove(listener);
	}

	@Override
	public List<DataLanguageVO> getAvailables() {
		
		List<DataLanguageVO> retVal = new ArrayList<DataLanguageVO>();
		
		for (DataLanguageVO availDL : this.availDataLanguages) {
			if (!this.selectDataLanguages.contains(availDL)) {
				retVal.add(availDL);
			}
		}
		
		Collections.sort(retVal, new Comparator<DataLanguageVO>() {
			@Override
			public int compare(DataLanguageVO o1, DataLanguageVO o2) {
				return o1.toString().compareTo(o2.toString());
			}
		});
		
		return retVal;
	}
	
	@Override
	public List<DataLanguageVO> getSelected() {
		List<DataLanguageVO> retVal = new ArrayList<DataLanguageVO>();
		for (DataLanguageVO availDL : this.selectDataLanguages) {
			retVal.add(availDL);
		}
		Collections.sort(retVal, new Comparator<DataLanguageVO>() {
			@Override
			public int compare(DataLanguageVO o1, DataLanguageVO o2) {
				return o1.getOrder().compareTo(o2.getOrder());
			}
		});
		return retVal;
	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		this.view.setIsRefreshEnabled(Boolean.TRUE);
		this.view.setIsSaveEnabled(Boolean.TRUE);
	}

	@Override
	public void setPrimaryDataLanguage(DataLanguageVO item) {
		
		this.view.setIsRefreshEnabled(Boolean.TRUE);
		this.view.setIsSaveEnabled(Boolean.TRUE);
		
	}

	@Override
	public void addDataLanguages(List<DataLanguageVO> elemnts) {
		
		this.availDataLanguages.removeAll(elemnts);
		
		boolean markFirstAsPrimary = false;
				
		if (this.selectDataLanguages.size() == 0) {
			markFirstAsPrimary = true;
		}
		
		for (DataLanguageVO dlvo : elemnts) {
			dlvo.setOrder(this.selectDataLanguages.size() + 1);
			this.selectDataLanguages.add(dlvo);
			
			if (markFirstAsPrimary == true) {
				dlvo.setIsPrimary(Boolean.TRUE);
				markFirstAsPrimary = false;
			}
		}
		

		this.view.refreshView();
		
		this.view.setIsRefreshEnabled(Boolean.TRUE);
		this.view.setIsSaveEnabled(Boolean.TRUE);
	}

	@Override
	public void removeDataLanguages(List<DataLanguageVO> elemnts) {

		this.selectDataLanguages.removeAll(elemnts);
		this.availDataLanguages.addAll(elemnts);
		
		for (int idx=0; idx < this.selectDataLanguages.size(); idx++) {
			DataLanguageVO dataLanguageVO = this.selectDataLanguages.get(idx);
			dataLanguageVO.setOrder(idx + 1);
		}
		
		this.view.refreshView();		
		this.view.setIsRefreshEnabled(Boolean.TRUE);
		this.view.setIsSaveEnabled(Boolean.TRUE);
	}

	@Override
	public void save() {
		try {
			
			for (DataLanguageVO dlvo : this.selectDataLanguages) {
				if (dlvo.isPrimary() && !dlvo.getPrimaryKey().equals(
						DataLanguageDelegate.getInstance().getPrimaryDataLanguage()) && 
						DataLanguageDelegate.getInstance().getPrimaryDataLanguage() != null) {
					
					final String sMessage = SpringLocaleDelegate.getInstance().getMessage(
							"DataLanguageController.2", 
							"Wenn Sie die Primärsprache wechseln, werden die lokalisierten Werte aus den Sprachtabellen in die Hauptentitäten übernommen.\nBestehende Werte werden überschrieben.");
					final String sTitle = SpringLocaleDelegate.getInstance().getMessage("DataLanguageController.1", "Setzen der Primärsprache");

					int dropEntity = JOptionPane.showConfirmDialog(this.view, sMessage, sTitle, JOptionPane.OK_CANCEL_OPTION);
					switch(dropEntity) {
						case JOptionPane.CANCEL_OPTION:
							return;
					}
				}	
			}
			
			DataLanguageDelegate.getInstance().save(this.selectDataLanguages);
			DataLanguageDelegate.getInstance().invalidate();
			DataLanguageContext.setDataSystemLanguage(
					DataLanguageDelegate.getInstance().getPrimaryDataLanguage());
			
			refresh();	
		} catch (Exception e) {
			Errors.getInstance().showExceptionDialog(view, e);
		} 
	}

	@Override
	public void refresh() {
		availDataLanguages = getCompleteCopy(DataLanguageDelegate.getInstance().getAllDataLanguages());
		selectDataLanguages = getCompleteCopy(DataLanguageDelegate.getInstance().getSelectedLocales());
		this.view.refreshView();		
		this.view.setIsRefreshEnabled(Boolean.FALSE);
		this.view.setIsSaveEnabled(Boolean.FALSE);
	}

	private List<DataLanguageVO> getCompleteCopy(Collection<DataLanguageVO> source) {
		List<DataLanguageVO> retVal = new ArrayList<DataLanguageVO>();
		
		for (DataLanguageVO dlvo : source) {
			retVal.add(dlvo.copy());
		}
		
		Collections.sort(retVal, new Comparator<DataLanguageVO>() {
			@Override
			public int compare(DataLanguageVO o1, DataLanguageVO o2) {
				return o1.getOrder().compareTo(o2.getOrder());
			}
		});
		
		return retVal;
	}
	@Override
	public void moveUpDataLanguages(DataLanguageVO dlToMoveUp) {		
		if (dlToMoveUp != null) {			
			
			for (DataLanguageVO dlvo : getSelected()) {
				if (dlvo.getOrder() + 1 == dlToMoveUp.getOrder()) {
					int val = dlvo.getOrder();
					dlvo.setOrder(dlToMoveUp.getOrder());
					dlToMoveUp.setOrder(val);
					break;
				}
			}
			
			Collections.sort(selectDataLanguages, new Comparator<DataLanguageVO>() {

				@Override
				public int compare(DataLanguageVO o1, DataLanguageVO o2) {
					return o1.getOrder().compareTo(o2.getOrder());
				}
			});
			
			
			this.view.refreshView();		
			this.view.setIsRefreshEnabled(Boolean.TRUE);
			this.view.setIsSaveEnabled(Boolean.TRUE);
		}		
	}

	
	@Override
	public void moveDownDataLanguages(DataLanguageVO dlToMoveDown) {		
		if (dlToMoveDown != null) {			
			
			for (DataLanguageVO dlvo : getSelected()) {
				if (dlvo.getOrder() - 1 == dlToMoveDown.getOrder()) {
					int val = dlvo.getOrder();
					dlvo.setOrder(dlToMoveDown.getOrder());
					dlToMoveDown.setOrder(val);
					break;
				}
			}
			

			Collections.sort(selectDataLanguages, new Comparator<DataLanguageVO>() {

				@Override
				public int compare(DataLanguageVO o1, DataLanguageVO o2) {
					return o1.getOrder().compareTo(o2.getOrder());
				}
			});
			
			this.view.refreshView();		
			this.view.setIsRefreshEnabled(Boolean.TRUE);
			this.view.setIsSaveEnabled(Boolean.TRUE);
		}
	}

	@Override
	public void switchSave() {
		this.view.setIsRefreshEnabled(Boolean.TRUE);
		this.view.setIsSaveEnabled(Boolean.TRUE);
	}
	
}
