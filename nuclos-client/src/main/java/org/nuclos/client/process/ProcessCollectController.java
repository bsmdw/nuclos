package org.nuclos.client.process;

import java.io.Serializable;
import java.util.Map;

import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.MasterDataCollectController;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonBusinessException;



public class ProcessCollectController extends MasterDataCollectController<UID> {

	public ProcessCollectController(UID entity, MainFrameTab tabIfAny) {
		super(entity, tabIfAny, null);
	}
	
	@Override
	protected void deleteCollectable(final CollectableMasterDataWithDependants<UID> clct, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		super.deleteCollectable(clct, applyMultiEditContext);
	}
	
	@Override
	protected CollectableMasterDataWithDependants<UID> insertCollectable(final CollectableMasterDataWithDependants<UID> clctNew) throws CommonBusinessException {
		CollectableMasterDataWithDependants<UID> retVal = super.insertCollectable(clctNew);
		return retVal;
	}
	
	@Override
	protected CollectableMasterDataWithDependants<UID> updateCollectable(final CollectableMasterDataWithDependants<UID> clct, Object oAdditionalData, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		CollectableMasterDataWithDependants<UID> retVal = super.updateCollectable(clct, oAdditionalData, applyMultiEditContext);
		return retVal;
	}
}
