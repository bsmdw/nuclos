//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.common;

import java.lang.reflect.InvocationTargetException;
import java.util.NoSuchElementException;
import java.util.prefs.Preferences;

import javax.swing.JComponent;
import javax.swing.JOptionPane;

import org.apache.commons.lang.NullArgumentException;
import org.nuclos.client.common.controller.NuclosCollectControllerCommonState;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.genericobject.DynamicEntitySubFormController;
import org.nuclos.client.genericobject.GenericObjectCollectController;
import org.nuclos.client.genericobject.GenericObjectSplitViewCollectController;
import org.nuclos.client.genericobject.Modules;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.MainController;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.MasterDataCollectController;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.masterdata.MasterDataLayoutHelper;
import org.nuclos.client.masterdata.MasterDataSplitViewCollectController;
import org.nuclos.client.masterdata.MasterDataSubFormController;
import org.nuclos.client.statemodel.StateDelegate;
import org.nuclos.client.ui.collect.CollectControllerFactorySingleton;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelProvider;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.client.valuelistprovider.cache.CollectableFieldsProviderCache;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.E;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.WorkspaceDescription2;
import org.nuclos.common.WorkspaceDescription2.EntityPreferences;
import org.nuclos.common.statemodel.Statemodel;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonPermissionException;

/**
 * Factory that creates any <code>NuclosCollectController</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class NuclosCollectControllerFactory {
	
	private static NuclosCollectControllerFactory singleton;

	protected NuclosCollectControllerFactory() {
	}

	public static synchronized NuclosCollectControllerFactory getInstance() {
		if (singleton == null) {
			singleton = newFactory();
		}
		return singleton;
	}

	private static NuclosCollectControllerFactory newFactory() {
		try {
			final String sClassName = LangUtils.defaultIfNull(
					ApplicationProperties.getInstance().getCollectControllerFactoryClassName(), NuclosCollectControllerFactory.class.getName());
			return (NuclosCollectControllerFactory) LangUtils.getClassLoaderThatWorksForWebStart().loadClass(sClassName).newInstance();
		}
		catch (Exception ex) {
			throw new CommonFatalException("CollectControllerFactory could not be created.", ex);
		}
	}

	/**
	 * All specialized <code>CollectController</code>s that do not extend
	 * <code>MasterDataCollectController</code> or <code>GenericObjectCollectController</code>
	 * must be taken care of here.
	 * 
	 * §postcondition result != null
	 */	
	public NuclosCollectController<?,?> newCollectController(UID entityUid, MainFrameTab tabIfAny, String customUsage) throws NuclosBusinessException, CommonPermissionException, CommonFatalException {
		return newCollectController(entityUid, null, tabIfAny, customUsage, false);
	}
	
	/**
	 * All specialized <code>CollectController</code>s that do not extend
	 * <code>MasterDataCollectController</code> or <code>GenericObjectCollectController</code>
	 * must be taken care of here.
	 * 
	 * §postcondition result != null
	 */	
	public NuclosCollectController<?,?> newCollectController(UID entityUid, UID iProcessId, MainFrameTab tabIfAny, String customUsage, boolean bNoSplitViews) throws NuclosBusinessException, CommonPermissionException, CommonFatalException {
		if (entityUid == null) {
			throw new NullArgumentException("sEntityName");
		}

		// check if the entity is a generic object entity:
		UID iModuleId = null;
		try {
			iModuleId = Modules.getInstance().getModule(entityUid).getUID();
		}
		catch (NoSuchElementException ex) {
			iModuleId = null;
		}

		if (iModuleId != null) {
			Statemodel sm = StateDelegate.getInstance().getStatemodel(new UsageCriteria(iModuleId, iProcessId, null, null));
			if(sm == null) {
				JOptionPane.showMessageDialog(
					Main.getInstance().getMainFrame(),
					SpringLocaleDelegate.getInstance().getMessage(
							"NuclosCollectControllerFactory.2", "Es ist kein Statusmodell definiert. Das Modul kann nicht geöffnet werden.\nDie Statusmodellverwaltung finden Sie im Menü Konfiguration."),
					MetaProvider.getInstance().getEntity(entityUid).getEntityName(),
					JOptionPane.WARNING_MESSAGE);
				return null;
			}
			
			Boolean bSplitView = bNoSplitViews ? false : MetaProvider.getInstance().getEntity(entityUid).isResultdetailssplitview();
			if(bSplitView != null && bSplitView) {	
				return newGenericObjectCollectControllerSplitView(iModuleId, tabIfAny, customUsage);
			} else {
				return newGenericObjectCollectController(iModuleId, iProcessId, tabIfAny, customUsage);
			}
		} else {
			if (SecurityCache.getInstance().isReadAllowedForMasterData(entityUid)) {
				MasterDataLayoutHelper.checkLayoutMLExistence(entityUid, MasterDataDelegate.getInstance().getMetaData(entityUid).isSearchable() || MainController.isOverrideSearchableForEntity(entityUid));
				final CollectControllerFactorySingleton factory = CollectControllerFactorySingleton.getInstance();
				if (E.DATASOURCE.checkEntityUID(entityUid)) {
					return factory.newDatasourceCollectController(tabIfAny);
				} else if (E.DYNAMICENTITY.checkEntityUID(entityUid)) {
					return factory.newDynamicEntityCollectController(tabIfAny);
				} else if (E.VALUELISTPROVIDER.checkEntityUID(entityUid)) {
					return factory.newValuelistProviderCollectController(tabIfAny);
				} else if (E.RECORDGRANT.checkEntityUID(entityUid)) {
					return factory.newRecordGrantCollectController(tabIfAny);
				} else if (E.DYNAMICTASKLIST.checkEntityUID(entityUid)) {
					return factory.newDynamicTasklistCollectController(tabIfAny);
				} else if (E.CHART.checkEntityUID(entityUid)) {
					return factory.newChartCollectController(tabIfAny);
				} else if (E.CALCATTRIBUTE.checkEntityUID(entityUid)) {
					return factory.newCalcAttributeCollectController(tabIfAny);
				} else if (E.STATEMODEL.checkEntityUID(entityUid)) {
					return factory.newStateModelCollectController(tabIfAny, new NuclosCollectControllerCommonState());
				} else if (E.ENTITYRELATION.checkEntityUID(entityUid)) {
					return factory.newEntityRelationShipCollectController(Main.getInstance().getMainFrame(), tabIfAny, new NuclosCollectControllerCommonState());
				}

				Boolean bSplitView = bNoSplitViews ? false : MetaProvider.getInstance().getEntity(entityUid).isResultdetailssplitview();
				if(bSplitView != null && bSplitView) {					
					return newMasterDataCollectControllerSplitView(entityUid, tabIfAny, customUsage);
				}
				else {				
					// try general masterdata factory method:
					// if there is no MasterDataCollectController for this entity, an exception will be thrown here.
					return newMasterDataCollectController(entityUid, tabIfAny, customUsage);
				}

			} else {
				throw new CommonPermissionException(SpringLocaleDelegate.getInstance().getMessage(
						"NuclosCollectControllerFactory.1", "Sie haben kein Recht in der Entit\u00e4t ''{0}'' zu lesen.", 
						SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(
								MasterDataDelegate.getInstance().getMetaData(entityUid))));
			}
		}
	}

	/**
	 * §postcondition result != null
	 * NUCLOS-5380 Do not remove the final here
	 * 
	 * @param moduleUid may be <code>null</code>
	 * @return a new GenericObjectCollectController for the given module id.
	 */
	public final GenericObjectCollectController newGenericObjectCollectController(UID moduleUid, MainFrameTab tabIfAny, String customUsage) {
		UID processId = null;
		return newGenericObjectCollectController(moduleUid, processId, tabIfAny, customUsage);
	}

	/**
	 * §postcondition result != null
	 * 
	 * @param moduleUid may be <code>null</code>
	 * @return a new GenericObjectCollectController for the given module id.
	 */
	public GenericObjectCollectController newGenericObjectCollectController(UID moduleUid, UID processId, MainFrameTab tabIfAny, String customUsage) {
		final GenericObjectCollectController result;
		final CollectControllerFactorySingleton factory = CollectControllerFactorySingleton.getInstance();
		result = factory.newGenericObjectCollectController(moduleUid, processId, true, tabIfAny, customUsage);

		assert result != null;
		return result;
	}
	
	
	public GenericObjectSplitViewCollectController newGenericObjectCollectControllerSplitView(UID moduleUid, MainFrameTab tabIfAny, String customUsage) {
		final GenericObjectSplitViewCollectController result;
		final CollectControllerFactorySingleton factory = CollectControllerFactorySingleton.getInstance();
		result = factory.newGenericObjectCollectControllerSplitView(moduleUid, true, tabIfAny, customUsage);

		assert result != null;
		return result;
	}


	/**
	 * §postcondition result != null
	 * 
	 * @param entityUid the masterdata entity to collect
	 * @return a new MasterDataCollectController for the given entity.
	 */
	public MasterDataCollectController newMasterDataCollectController(UID entityUid, MainFrameTab tabIfAny, String customUsage) {
		final CollectControllerFactorySingleton factory = CollectControllerFactorySingleton.getInstance();
		if (E.GENERATION.checkEntityUID(entityUid)) {
			return factory.newGenerationCollectController(tabIfAny);	
		} else if (E.LAYOUT.checkEntityUID(entityUid)) {
			return factory.newGenericObjectLayoutCollectController(tabIfAny);			
		} else if (E.RELATIONTYPE.checkEntityUID(entityUid)) {
			return factory.newRelationTypeCollectController(tabIfAny);
		} else if (E.REPORT.checkEntityUID(entityUid)) {
			return factory.newReportCollectController(entityUid, tabIfAny);
		} else if (E.FORM.checkEntityUID(entityUid)) {
			return factory.newReportCollectController(entityUid, tabIfAny);
		} else if (E.REPORTEXECUTION.checkEntityUID(entityUid)) {
			return factory.newReportExecutionCollectController(tabIfAny);
		} else if (E.USER.checkEntityUID(entityUid)) {
			return factory.newUserCollectController(tabIfAny, new NuclosCollectControllerCommonState());
		} else if (E.ROLE.checkEntityUID(entityUid)) {
			return factory.newRoleCollectController(tabIfAny);
		} else if (E.RESOURCE.checkEntityUID(entityUid)) {
			return factory.newResourceCollectController(tabIfAny);
		} else if (E.IMPORTEXPORT.checkEntityUID(entityUid)) {
			return factory.newExportImportCollectController(tabIfAny);
		} else if (E.SEARCHFILTER.checkEntityUID(entityUid)) {
			return factory.newSearchFilterCollectController(tabIfAny, new NuclosCollectControllerCommonState());
		} else if (E.DATATYPE.checkEntityUID(entityUid)) {
			return factory.newDataTypeCollectController(tabIfAny);
		} else if (E.IMPORT.checkEntityUID(entityUid)) {
			return factory.newCsvImportStructureCollectController(tabIfAny);
		} else if (E.IMPORTFILE.checkEntityUID(entityUid)) {
			return factory.newCsvImportCollectController(tabIfAny);
		} else if (E.XML_IMPORT.checkEntityUID(entityUid)) {
			return factory.newXmlImportStructureCollectController(tabIfAny);
		} else if (E.XMLIMPORTFILE.checkEntityUID(entityUid)) {
			return factory.newXmlFileImportCollectController(tabIfAny);
		} else if (E.JOBCONTROLLER.checkEntityUID(entityUid)) {
			return factory.newJobControlCollectController(tabIfAny);
		} else if (E.LOCALE.checkEntityUID(entityUid)) {
			return factory.newLocaleCollectController(tabIfAny);
		} else if (E.NUCLET.checkEntityUID(entityUid)) {
			return factory.newNucletCollectController(tabIfAny);
		} else if (E.DBOBJECT.checkEntityUID(entityUid)) {
			return factory.newDbObjectCollectController(tabIfAny);
		} else if (E.DBSOURCE.checkEntityUID(entityUid)) {
			return factory.newDbSourceCollectController(tabIfAny);
		} else if (E.LDAPSERVER.checkEntityUID(entityUid)) {
			return factory.newLdapServerCollectController(tabIfAny);
		} else if (E.SERVERCODE.checkEntityUID(entityUid)) {
			return factory.newServerCodeCollectController(E.SERVERCODE.getUID(), tabIfAny);
		} else if (E.CLIENTCODE.checkEntityUID(entityUid)) {
				return factory.newClientCodeCollectController(E.CLIENTCODE.getUID(), tabIfAny);
		} else if (E.PROCESS.checkEntityUID(entityUid)) {
				return factory.newProcessCollectController(E.PROCESS.getUID(), tabIfAny);
		} else if (E.PARAMETER.checkEntityUID(entityUid)) {
			return factory.newParameterCollectController(tabIfAny);
		} else if (E.MANDATOR.checkEntityUID(entityUid)) {
			return factory.newMandatorCollectController(tabIfAny);
		} else if (E.MANDATOR_LEVEL.checkEntityUID(entityUid)) {
			return factory.newMandatorLevelCollectController(tabIfAny);
		} else if (E.PRINTSERVICE.checkEntityUID(entityUid)) {
			return factory.newPrintServiceCollectController(tabIfAny);
		} else if (E.COMMUNICATION_PORT.checkEntityUID(entityUid)) {
			return factory.newCommunicationPortCollectController(tabIfAny);
		} else if (E.NUCLET_INTEGRATION_POINT.checkEntityUID(entityUid)) {
			return factory.newNucletIntegrationPointCollectController(tabIfAny);
		} else {
			return factory.newMasterDataCollectController(entityUid, tabIfAny, customUsage);			
		}		
	}
	
	public MasterDataSplitViewCollectController newMasterDataCollectControllerSplitView(UID entityUid, MainFrameTab tabIfAny, String customUsage) {
		final CollectControllerFactorySingleton factory = CollectControllerFactorySingleton.getInstance();
		return factory.newMasterDataCollectControllerSplitView(entityUid, tabIfAny, customUsage);
	}

	/**
	 * §postcondition result != null
	 * @return a new DetailsSubFormController for use in an NuclosCollectController
	 */
	public <PK> MasterDataSubFormController<PK> newDetailsSubFormController(SubForm subform,
			UID parentEntityUid, CollectableComponentModelProvider clctcompmodelprovider,
			MainFrameTab tab, JComponent compDetails, Preferences prefs,
			EntityPreferences entityPrefs, CollectableFieldsProviderCache valueListProviderCache,
			NuclosCollectControllerCommonState state, EntityCollectController<?, ?> eoController) {

		final MasterDataSubFormController<PK> result;

		final String sControllerType = subform.getControllerType();
		if (sControllerType == null || sControllerType.equals("default")) {
			if (MetaProvider.getInstance().getEntity(subform.getEntityUID()).isDynamic()) {
				result = (MasterDataSubFormController<PK>) new DynamicEntitySubFormController(
						tab, clctcompmodelprovider, parentEntityUid, subform, 
						prefs, entityPrefs, valueListProviderCache,
						new NuclosCollectControllerCommonState());
			} else {
				result = new MasterDataSubFormController(
						tab, clctcompmodelprovider, parentEntityUid, 
						subform, prefs, entityPrefs, valueListProviderCache,
						new NuclosCollectControllerCommonState(), eoController);
			}
		} else {
			try {
				Class<?> controllerClass = Class.forName(sControllerType);
				Object controller = controllerClass.getConstructor(MainFrameTab.class, CollectableComponentModelProvider.class, UID.class, SubForm.class, Preferences.class, WorkspaceDescription2.EntityPreferences.class, CollectableFieldsProviderCache.class, NuclosCollectControllerCommonState.class, EntityCollectController.class)
						.newInstance(tab, clctcompmodelprovider, parentEntityUid,
								subform, prefs, entityPrefs, valueListProviderCache,
								new NuclosCollectControllerCommonState(), eoController);
				if (controller instanceof MasterDataSubFormController) {
					return ((MasterDataSubFormController) controller);
				} else {
					throw new NuclosFatalException("Unknown Controllertype for subform:" + sControllerType);
				}
			} catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
				throw new NuclosFatalException("Unknown Controllertype for subform:" + sControllerType);
			}
		}
		
		assert result != null;
		return result;
	}

}	// class NuclosCollectControllerFactory
