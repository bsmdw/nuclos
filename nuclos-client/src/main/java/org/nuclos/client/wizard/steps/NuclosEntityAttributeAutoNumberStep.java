package org.nuclos.client.wizard.steps;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.nuclos.client.common.EntityUtils;
import org.nuclos.client.wizard.NuclosEntityWizardStaticModel;
import org.nuclos.client.wizard.model.Attribute;
import org.nuclos.client.wizard.model.DataTyp;
import org.nuclos.common.EntityMeta;
import org.nuclos.common2.SpringLocaleDelegate;
import org.pietschy.wizard.InvalidStateException;

import info.clearthought.layout.TableLayout;

public class NuclosEntityAttributeAutoNumberStep extends NuclosEntityAttributeAbstractStep {

	private static final Logger LOG = Logger.getLogger(NuclosEntityAttributeAutoNumberStep.class);

	private JLabel lbEntity;
	private JLabel lbExplain;
	private JComboBox cbxEntity;


	private NuclosEntityWizardStaticModel parentWizardModel;
	private List<Attribute> lstAttributes;

	public NuclosEntityAttributeAutoNumberStep(String name, String summary) {
		super(name, summary);
		initComponents();
	}

	@Override
	protected void initComponents() {
		double size [][] = {{TableLayout.PREFERRED,40, TableLayout.FILL}, {20,20,20,20,100,20,TableLayout.PREFERRED, TableLayout.FILL}};

		TableLayout layout = new TableLayout(size);
		layout.setVGap(3);
		layout.setHGap(5);
		this.setLayout(layout);

		lbEntity = new JLabel(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeautonumbership.1", "Nummerierung nach Entit\u00e4t")+": ");
		lbExplain= new JLabel(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeautonumbership.2", "Die Nummerierung erfolgt in Abhängigkeit der hier ausgewählten Entität. " +
				"Dies ermöglicht es, eigene Layouts für Unterformulardaten anzulegen."));
		cbxEntity = new JComboBox();
		cbxEntity.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeautonumbership.3", "Bitte wählen Sie ein Businessobjekt aus, auf das sich die Autonummer beziehen soll."));
		this.add(lbEntity, "0,0, 1,0");
		this.add(cbxEntity, "2,0");
		this.add(lbExplain, "0,0,2,3");

		cbxEntity.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					final Object obj = e.getItem();
					if (!(obj instanceof EntityMeta)) {
						NuclosEntityAttributeAutoNumberStep.this.model.getAttribute().setAutoNumberMetaVO(null);
					} else {
						NuclosEntityAttributeAutoNumberStep.this.model.getAttribute().setAutoNumberMetaVO((EntityMeta<?>)obj);
					}
				}
			}

		});
	}

	@Override
	public void prepare() {
		super.prepare();
		
		fillEntityCombobox();

		if (null != this.model.getAttribute().getAutoNumberMetaVO()) {
			cbxEntity.setSelectedItem(this.model.getAttribute().getAutoNumberMetaVO());
		}
		if (this.model.isAutoNumberType()) {
			NuclosEntityAttributeAutoNumberStep.this.setComplete(true);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					cbxEntity.requestFocusInWindow();
				}
			});
		}
	}

	public void setParentWizardModel(NuclosEntityWizardStaticModel model) {
		this.parentWizardModel = model;
	}

	public void setAttributeList(List<Attribute> lst) {
		this.lstAttributes = lst;
	}

	@Override
	public void close() {
		lbEntity = null;
		cbxEntity = null;

		parentWizardModel = null;

		super.close();
	}


	private final void fillEntityCombobox() {
		final ItemListener ilArray[] = cbxEntity.getItemListeners();
		for(ItemListener il : ilArray) {
			cbxEntity.removeItemListener(il);
		}

		cbxEntity.removeAllItems();


		final List<EntityMeta<?>> lstEntities = new ArrayList<EntityMeta<?>>();

		final EntityMeta<?> metaVOAutoNumber = this.model.getAttribute().getAutoNumberMetaVO();
		EntityMeta<?> selectedItem = null;
		for (final Attribute attribute : lstAttributes) {
			// reference must be mandatory
			if (!attribute.isMandatory()) {
				continue;
			}

			final DataTyp dt = attribute.getDatatyp();
			if (dt.isRefenceTyp()) {
				final EntityMeta<?> mvo = attribute.getMetaVO();
				if (mvo == null) {
					continue;
				}
				if(null != metaVOAutoNumber && mvo.getUID().equals(metaVOAutoNumber.getUID())) {
					selectedItem = mvo;
				}
				lstEntities.add(mvo);
			}
		}

		cbxEntity.addItem(EntityUtils.wrapMetaData(EntityMeta.NULL));

		Collections.sort(lstEntities, EntityUtils.getMetaComparator(EntityMeta.class));
		
		for(EntityMeta<?> entity : lstEntities) {
			cbxEntity.addItem(EntityUtils.wrapMetaData(entity));
		}

		if (null != selectedItem) {
			cbxEntity.setSelectedItem(selectedItem);
		}

		for(ItemListener il : ilArray) {
			cbxEntity.addItemListener(il);
		}
	}

	@Override
	public void applyState() throws InvalidStateException {
		model.nextStep();

		super.applyState();
	}
}
