//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.wizard.steps;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;

import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;
import org.nuclos.client.common.EntityUtils;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.datasource.DatasourceDelegate;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.util.ITableLayoutBuilder;
import org.nuclos.client.ui.util.TableLayoutBuilder;
import org.nuclos.client.wizard.NuclosEntityWizardStaticModel;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NucletEntityMeta;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.schema.meta.entity.EntityFieldVlpConfig;
import org.nuclos.schema.meta.entity.EntityFieldVlpContext;
import org.nuclos.schema.meta.entity.EntityFieldVlpParam;
import org.pietschy.wizard.InvalidStateException;

import info.clearthought.layout.TableLayout;

public class NuclosEntityAttributeVlpStep extends NuclosEntityAttributeAbstractStep {

	private static final Logger LOG = Logger.getLogger(NuclosEntityAttributeVlpStep.class);

	private boolean errorsDuringPrepare = false;

	private boolean vlpItemListenerEnabled = true;
	private boolean fieldItemListenerEnabled = true;

	private JComboBox cbxValueListProvider;
	private JComboBox cbxValueField;
	private JComboBox cbxIdField;

	private JPanel jpnParameter;
	private JScrollPane scrollParameter;

	private JLabel lbInfo;

	private TableLayoutBuilder tblParams;

	private final List<UID> lstContextId = new ArrayList<>();
	private final Map<UID, EntityFieldVlpContext> mapContext = new HashMap<>();
	private final Map<UID, JComboBox> mapContextEntity = new HashMap<>();
	private final Map<UID, JCheckBox> mapContextSearch = new HashMap<>();
	private final Map<UID, JCheckBox> mapContextInput = new HashMap<>();
	private final Map<UID, List<JParameterView>> mapParameterViews = new HashMap<>();

	private static final List<EntityMeta<?>> lstEntities = new ArrayList<>();
	private final List<ValuelistProviderVO> allValuelistProvider = new ArrayList<>();
	private ValuelistProviderVO vlpVo = null;
	private final List<String> lstValueFields = new ArrayList<>();
	private final List<String> lstIdFields = new ArrayList<>();
	private final List<DatasourceParameterVO> lstParams = new ArrayList<>();
	private EntityFieldVlpConfig vlpConfig = null;

	private NuclosEntityWizardStaticModel parentWizardModel;

	public NuclosEntityAttributeVlpStep(String name, String summary) {
		super(name, summary);
		initComponents();
	}

	private static String msg(String resId, Object ... params) {
		return SpringLocaleDelegate.getInstance().getMessage(resId, "", params);
	}

	@Override
	protected void initComponents() {
		TableLayoutBuilder tbllay = new TableLayoutBuilder(this).columns(TableLayout.PREFERRED,40, TableLayout.FILL, TableLayout.FILL);
		tbllay.getTableLayout().setVGap(3);
		tbllay.getTableLayout().setHGap(12);

		cbxValueListProvider = new JComboBox();
		cbxValueField = new JComboBox();
		cbxIdField = new JComboBox();
		tbllay.addLocalizedLabel("wizard.step.attributevlp.1").skip().add(cbxValueListProvider);
		tbllay.newRow().addLocalizedLabel("wizard.step.attributevlp.13").skip().add(cbxValueField);
		tbllay.newRow().addLocalizedLabel("wizard.step.attributevlp.14").skip().add(cbxIdField);

		jpnParameter = new JPanel();
		jpnParameter.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		scrollParameter = new JScrollPane(jpnParameter, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		tbllay.newRow(TableLayout.FILL).addFullSpan(scrollParameter);

		lbInfo = new JLabel();
		lbInfo.setForeground(Color.RED);
		tbllay.newRow().addFullSpan(lbInfo);

		cbxValueListProvider.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(final ItemEvent e) {
				if(vlpItemListenerEnabled && e.getStateChange() == ItemEvent.SELECTED) {
					Object item = e.getItem();
					if (item instanceof ValuelistProviderVO) {
						setValuelistProviderVo((ValuelistProviderVO) item, false);
					} else {
						setValuelistProviderVo(null, false);
					}
					fillFieldComboboxes();
					setupVlpParameterPanel();
				}
			}
		});
		cbxValueField.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(final ItemEvent e) {
				if (fieldItemListenerEnabled && e.getStateChange() == ItemEvent.SELECTED) {
					validateFields();
				}
			}
		});
		cbxIdField.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(final ItemEvent e) {
				if (fieldItemListenerEnabled && e.getStateChange() == ItemEvent.SELECTED) {
					validateFields();
				}
			}
		});
	}

	@Override
	public void prepare() {
		super.prepare();

		allValuelistProvider.clear();
		try {
			allValuelistProvider.addAll(DatasourceDelegate.getInstance().getAllValuelistProvider());
			Collections.sort(allValuelistProvider);
		} catch (CommonPermissionException e) {
			errorsDuringPrepare = true;
			Errors.getInstance().showDetailedExceptionDialog(this, e);
		}

		lstEntities.clear();
		lstEntities.addAll(MetaProvider.getInstance().getAllEntities());
		final Iterator<EntityMeta<?>> iterator = lstEntities.iterator();
		while (iterator.hasNext()) {
			final EntityMeta<?> eMeta = iterator.next();
			if (eMeta.isGeneric()) {
				iterator.remove();
				continue;
			}
			if (NucletEntityMeta.isEntityLanguageUID(eMeta.getUID())) {
				iterator.remove();
				continue;
			}
			if (E.isNuclosEntity(eMeta.getUID())) {
				iterator.remove();
				continue;
			}
		}
		Collections.sort(lstEntities, EntityUtils.getMetaComparator(EntityMeta.class));

		vlpConfig = this.model.getAttribute().getVlpConfig();
		if (vlpConfig == null) {
			vlpConfig = new EntityFieldVlpConfig();
		}

		UID vlpId = UID.parseUID(vlpConfig.getVlpId());
		Optional<ValuelistProviderVO> foundVlp = allValuelistProvider.stream().filter(vlp -> vlp.getId().equals(vlpId)).findFirst();
		setValuelistProviderVo(
				foundVlp.isPresent() ? foundVlp.get() : null,
				true);

		fillVlpCombobox();
		fillFieldComboboxes();
		setupVlpParameterPanel();

		if (this.model.isReferenceType()) {
			validateStep();
		}

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				cbxValueListProvider.requestFocusInWindow();
			}
		});
	}

	private void fillVlpCombobox() {
		vlpItemListenerEnabled = false;
		try {
			cbxValueListProvider.removeAllItems();
			cbxValueListProvider.addItem("");
			allValuelistProvider.stream().forEach(vlp -> cbxValueListProvider.addItem(vlp));
			if (vlpVo != null) {
				cbxValueListProvider.setSelectedItem(vlpVo);
			}
		} finally {
			vlpItemListenerEnabled = true;
		}
	}

	private void fillFieldComboboxes() {
		fieldItemListenerEnabled = false;
		try {
			cbxValueField.removeAllItems();
			cbxIdField.removeAllItems();
			lstValueFields.stream().forEach(f -> cbxValueField.addItem(f));
			lstIdFields.stream().forEach(f -> cbxIdField.addItem(f));
			if (vlpConfig.getValueField() != null) {
				cbxValueField.setSelectedItem(vlpConfig.getValueField());
			}
			if (vlpConfig.getIdField() != null) {
				cbxIdField.setSelectedItem(vlpConfig.getIdField());
			}
			validateFields();
		} finally {
			fieldItemListenerEnabled = true;
		}
	}

	private void setupVlpParameterPanel() {
		jpnParameter.removeAll();
		lstContextId.clear();
		mapContext.clear();
		mapContextEntity.clear();
		mapContextSearch.clear();
		mapContextInput.clear();
		mapParameterViews.clear();

		if (!lstParams.isEmpty()) {
			List<Double> lstColumns = new ArrayList<>();
			lstColumns.add(TableLayout.PREFERRED); // remove
			lstColumns.add(TableLayout.PREFERRED); // context BO
			lstColumns.add(TableLayout.PREFERRED); // context search
			// lstColumns.add(TableLayout.PREFERRED); // context input // noch nicht verfügbar
			lstColumns.add(40d);
			lstParams.stream().forEach(p -> lstColumns.add(TableLayout.PREFERRED)); // a column for each parameter

			tblParams = new TableLayoutBuilder(jpnParameter).columns(ArrayUtils.toPrimitive(lstColumns.toArray(new Double[]{})));
			tblParams.getTableLayout().setVGap(3);
			tblParams.getTableLayout().setHGap(5);

			tblParams.addLabel("").addLabel("").addLabel("");
			// tblParams.addLabel(""); // input noch nicht verfügbar
			JLabel lbParameter = new JLabel(msg("wizard.step.attributevlp.9"));
			tblParams.skip().addFullSpan(lbParameter);
			tblParams.newRow().addLabel("");
			tblParams.addLocalizedLabel("wizard.step.attributevlp.2", "wizard.step.attributevlp.3");
			tblParams.addLocalizedLabel("wizard.step.attributevlp.4", "wizard.step.attributevlp.5");
			// tblParams.addLocalizedLabel("wizard.step.attributevlp.6", "wizard.step.attributevlp.7"); // input noch nicht verfügbar
			tblParams.skip();
			lstParams.stream().forEach(p -> tblParams.addLabel("<html><b>" + p.getParameter() + "</b></html>")); // a column for each parameter

			if (vlpConfig.getVlpContexts().isEmpty()) {
				vlpConfig.getVlpContexts().add(EntityFieldVlpContext.builder().withSearch(true).build());
			}

			tblParams.newRow(5);
			tblParams.newRow(5);
			tblParams.newRow().skip().add(newAddNewContextRowButton());
			tblParams.newRow(5);

			vlpConfig.getVlpContexts().stream().forEach(vlpContext -> {
				addNewContextRow(vlpContext);
			});
		}

		jpnParameter.invalidate();
		jpnParameter.revalidate();
		jpnParameter.repaint();
	}

	private JComponent newRemoveContextRowButton(final UID contextId) {
		JLabel removeIcon = new JLabel(Icons.getInstance().getIconMinus16());
		removeIcon.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent e) {
				if (SwingUtilities.isLeftMouseButton(e)) {
					int rowNumber = lstContextId.indexOf(contextId) + 1;
					if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(NuclosEntityAttributeVlpStep.this,
							msg("wizard.step.attributevlp.11", rowNumber),
							null,
							JOptionPane.YES_NO_OPTION)) {
						removeContextRow(contextId);
					}
					e.consume();
				}
			}
		});
		return removeIcon;
	}

	private void removeContextRow(final UID contextId) {
		JComboBox entityComboBox = mapContextEntity.get(contextId);
		int row = tblParams.getMinRow(entityComboBox);
		int index = lstContextId.indexOf(contextId);
		vlpConfig.getVlpContexts().remove(index);
		tblParams.deleteRow(row);
		lstContextId.remove(contextId);
		mapContext.remove(contextId);
		mapContextEntity.remove(contextId);
		mapContextSearch.remove(contextId);
		mapContextInput.remove(contextId);
		mapParameterViews.remove(contextId);
		validateContexts();
		repaintStep(this);
	}

	private JComponent newAddNewContextRowButton() {
		JButton btnNewContext = new JButton(msg("wizard.step.attributevlp.8"), Icons.getInstance().getIconPlus16());
		btnNewContext.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				EntityFieldVlpContext newVlpContext = EntityFieldVlpContext.builder().withSearch(true).build();
				addNewContextRow(newVlpContext);
				vlpConfig.getVlpContexts().add(newVlpContext);
				validateContexts();
			}
		});
		return btnNewContext;
	}

	private void addNewContextRow(EntityFieldVlpContext vlpContext) {
		UID contextId = new UID();
		lstContextId.add(contextId);
		mapContext.put(contextId, vlpContext);
		syncVlpParams(vlpContext);
		List<JParameterView> parameterViews = vlpContext.getVlpParams().stream().map(p ->
				new JParameterView(NuclosEntityAttributeVlpStep.this, p, this.parentWizardModel.getUID())
		).collect(Collectors.toList());

		ITableLayoutBuilder newRow = tblParams.insertRowAt(tblParams.getRowSize() - 3)
				.add(newRemoveContextRowButton(contextId))
				.add(newEntityComboBox(contextId))
				.add(newCheckBox(contextId, true, false))
				// .add(newCheckBox(vlpContext, false, true)) // input noch nicht verfügbar
				.skip();
		mapContextSearch.get(contextId).setEnabled(false); // so lange es nur search gibt!
		parameterViews.stream().forEach(pv -> newRow.add(pv));
		mapParameterViews.put(contextId, parameterViews);
	}

	private void syncVlpParams(EntityFieldVlpContext context) {
		final List<EntityFieldVlpParam> oldVlpParams = new ArrayList<>(context.getVlpParams());
		Iterator<EntityFieldVlpParam> itParam = oldVlpParams.iterator();
		while (itParam.hasNext()) {
			EntityFieldVlpParam vlpParam = itParam.next();
			if (lstParams.stream().noneMatch(p -> p.getParameter().equals(vlpParam.getName()))) {
				// Parameter ist nicht mehr im VLP vorhanden:
				itParam.remove();
			}
		}

		final List<EntityFieldVlpParam> newVlpParams = new ArrayList<>();
		lstParams.stream().forEach(p -> {
			Optional<EntityFieldVlpParam> foundParam = oldVlpParams.stream().filter(vlpParam -> p.getParameter().equals(vlpParam.getName())).findFirst();
			EntityFieldVlpParam vp;
			if (foundParam.isPresent()) {
				vp = foundParam.get();
			} else {
				vp = EntityFieldVlpParam.builder().withName(p.getParameter()).build();
			}
			newVlpParams.add(vp);
		});

		context.getVlpParams().clear();
		context.getVlpParams().addAll(newVlpParams);
	}

	private JCheckBox newCheckBox(final UID contextId, boolean bSearch, boolean bInput) {
		final EntityFieldVlpContext vlpContext = mapContext.get(contextId);
		final JCheckBox cb = new JCheckBox();
		if (bSearch && vlpContext.isSearch()) {
			cb.setSelected(true);
		}
		if (bInput && vlpContext.isInput()) {
			cb.setSelected(true);
		}
		if (bSearch) {
			mapContextSearch.put(contextId, cb);
		}
		if (bInput) {
			mapContextInput.put(contextId, cb);
		}
		cb.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(final ItemEvent e) {
				boolean bSelected = e.getStateChange() == ItemEvent.SELECTED;
				if (bSearch) {
					vlpContext.setSearch(bSelected);
				}
				if (bInput) {
					vlpContext.setInput(bSelected);
				}
				validateContexts();
			}
		});
		return cb;
	}

	private JComboBox newEntityComboBox(final UID contextId) {
		final EntityFieldVlpContext vlpContext = mapContext.get(contextId);
		final UID selectedEntityId = UID.parseUID(vlpContext.getEntityClassId());
		final JComboBox cbxEntity = newEntityComboBox();
		if (selectedEntityId != null) {
			EntityMeta<?> selectedEntityMeta = lstEntities.stream().filter(eMeta -> eMeta.getUID().equals(selectedEntityId)).findFirst().get();
			cbxEntity.setSelectedItem(selectedEntityMeta);
		}
		cbxEntity.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(final ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					EntityMeta<?> eMeta = (EntityMeta<?>) e.getItem();
					vlpContext.setEntityClassId(eMeta.getUID().getStringifiedDefinitionWithEntity(E.ENTITY));
					validateContexts();
				}
			}
		});
		mapContextEntity.put(contextId, cbxEntity);
		return cbxEntity;
	}

	private static JComboBox newEntityComboBox() {
		final JComboBox cbxEntity = new JComboBox();
		cbxEntity.addItem(EntityUtils.wrapMetaData(EntityMeta.NULL));
		lstEntities.stream().forEach(eMeta -> cbxEntity.addItem(eMeta));
		return cbxEntity;
	}

	private void setValuelistProviderVo(ValuelistProviderVO newVlpVo, boolean bPrepare) {
		UID oldVlpId = vlpVo != null ? vlpVo.getId() : null;
		UID newVlpId = null;
		lstValueFields.clear();
		lstIdFields.clear();
		lstParams.clear();
		if (newVlpVo != null) {
			vlpVo = newVlpVo;
			newVlpId = vlpVo.getId();
			vlpConfig.setVlpId(newVlpId.getString());
			try {
				lstValueFields.addAll(DatasourceDelegate.getInstance().getColumnsFromVLP(vlpVo.getId(), String.class));
				EntityMeta<?> refEntityMeta = this.model.getAttribute().getMetaVO();
				final boolean bIsUidReference = refEntityMeta == null ? false : refEntityMeta.isUidEntity();
				if (bIsUidReference) {
					lstIdFields.addAll(lstValueFields);
				} else {
					lstIdFields.addAll(DatasourceDelegate.getInstance().getColumnsFromVLP(vlpVo.getId(), Number.class));
				}
				lstParams.addAll(DatasourceDelegate.getInstance().getParametersFromXML(vlpVo.getSource()));
				Collections.sort(lstParams);
			} catch (CommonBusinessException ex) {
				Errors.getInstance().showDetailedExceptionDialog(this, ex);
			}
		} else {
			this.vlpVo = null;
			vlpConfig.setVlpId(null);
		}
		if (!bPrepare && !RigidUtils.equal(oldVlpId, newVlpId)) {
			vlpConfig.getVlpContexts().clear();
		}
	}

	public void setParentWizardModel(NuclosEntityWizardStaticModel model) {
		this.parentWizardModel = model;
	}

	@Override
	public void close() {
		parentWizardModel = null;

		cbxValueListProvider = null;
		cbxValueField = null;
		cbxIdField = null;

		scrollParameter = null;
		jpnParameter = null;

		lbInfo = null;

		tblParams = null;
		lstContextId.clear();
		mapContext.clear();
		mapContextEntity.clear();
		mapContextSearch.clear();
		mapContextInput.clear();
		mapParameterViews.clear();

		super.close();
	}

	private boolean validateStep() {
		boolean bValid = true;
		if (!validateFields() || !validateContexts()) {
			bValid = false;
		}
		setComplete(bValid);
		return bValid;
	}

	private boolean validateFields() {
		if (vlpVo == null) {
			vlpConfig.setValueField(null);
			vlpConfig.setIdField(null);
		} else {
			if (!RigidUtils.equal(vlpConfig.getValueField(), cbxValueField.getSelectedItem())) {
				vlpConfig.setValueField(cbxValueField.getSelectedItem() != null ? cbxValueField.getSelectedItem().toString() : null);
			}
			if (!RigidUtils.equal(vlpConfig.getIdField(), cbxIdField.getSelectedItem())) {
				vlpConfig.setIdField(cbxIdField.getSelectedItem() != null ? cbxIdField.getSelectedItem().toString() : null);
			}
		}
		// idField could be optional in the future, but currently the search works only with ID
		if (vlpVo != null && (vlpConfig.getValueField() == null || vlpConfig.getIdField() == null)) {
			lbInfo.setText(msg("wizard.step.attributevlp.15"));
			return false;
		}
		lbInfo.setText("");
		return true;
	}

	private boolean validateContexts() {
		EntityFieldVlpConfig cloned = (EntityFieldVlpConfig) vlpConfig.clone();
		cloned.getVlpContexts().stream().forEach(c -> c.getVlpParams().clear());
		boolean areAllUnique = cloned.getVlpContexts().stream().allMatch(new HashSet<>()::add);
		lbInfo.setText(areAllUnique ? "" : msg("wizard.step.attributevlp.12"));
		return areAllUnique;
	}

	@Override
	public void applyState() throws InvalidStateException {
		if (!errorsDuringPrepare) {
			if (vlpVo != null) {
				if (!validateStep()) {
					return;
				}
				orderContexts(vlpConfig);
				this.model.getAttribute().setVlpConfig(vlpConfig);
			} else {
				this.model.getAttribute().setVlpConfig(null);
			}
		}
		super.applyState();
	}

	private static void orderContexts(EntityFieldVlpConfig vlpConfig) {
		Collections.sort(vlpConfig.getVlpContexts(), new Comparator<EntityFieldVlpContext>() {
			@Override
			public int compare(final EntityFieldVlpContext o1, final EntityFieldVlpContext o2) {
				String contextEntity1 = o1.getEntityClassId() != null ? MetaProvider.getInstance().getEntity(UID.parseUID(o1.getEntityClassId())).getEntityName() : "";
				String contextEntity2 = o2.getEntityClassId() != null ? MetaProvider.getInstance().getEntity(UID.parseUID(o2.getEntityClassId())).getEntityName() : "";
				contextEntity1 += o1.isSearch() ? "0" : "1";
				contextEntity2 += o1.isSearch() ? "0" : "1";
				contextEntity1 += o1.isInput() ? "0" : "1";
				contextEntity2 += o1.isInput() ? "0" : "1";
				return StringUtils.compareIgnoreCase(contextEntity1, contextEntity2);
			}
		});
	}

	private static void repaintStep(NuclosEntityAttributeVlpStep vlpStep) {
		if (vlpStep != null) {
			vlpStep.jpnParameter.invalidate();
			vlpStep.jpnParameter.revalidate();
			vlpStep.jpnParameter.repaint();
			vlpStep.invalidate();
			vlpStep.revalidate();
			vlpStep.repaint();
		}
	}

	private static class JParameterView extends JPanel {

		private final JLabel textLabel = new JLabel();
		private final JLabel editIcon = new JLabel();

		private final EntityFieldVlpParam vlpParam;
		private final UID editingEntityId;

		private final WeakReference<NuclosEntityAttributeVlpStep> parentComponent;

		public JParameterView(NuclosEntityAttributeVlpStep parentComponent, EntityFieldVlpParam vlpParam, UID editingEntityId) {
			if (vlpParam == null) {
				throw new IllegalArgumentException("vlpParam must not be null");
			}
			this.editingEntityId = editingEntityId;
			this.parentComponent = new WeakReference<>(parentComponent);
			this.vlpParam = vlpParam;
			this.setBackground(Color.WHITE);
			this.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));
			TableLayoutBuilder tbllay = new TableLayoutBuilder(this).columns(TableLayout.FILL, 5, TableLayout.PREFERRED);
			tbllay.newRow(TableLayout.FILL).add(textLabel).skip().add(editIcon, 1, TableLayout.CENTER, TableLayout.CENTER);
			editIcon.setIcon(Icons.getInstance().getIconTextFieldButtonHyperlink());
			editIcon.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(final MouseEvent e) {
					if (SwingUtilities.isLeftMouseButton(e)) {
						showParameterEditor();
						e.consume();
					}
				}
			});
			updateText();
		}

		private void updateText() {
			StringBuilder s = new StringBuilder();
			if (vlpParam.getValue() != null) {
				s.append('"');
				s.append(vlpParam.getValue());
				s.append('"');
			}
			if (vlpParam.getValueFromEntityFieldId() != null) {
				if (s.length() > 0) {
					s.append(" / ");
				}
				UID fieldId = UID.parseUID(vlpParam.getValueFromEntityFieldId());
				if (MetaProvider.getInstance().checkEntityField(fieldId)) {
					FieldMeta<?> fMeta = MetaProvider.getInstance().getEntityField(fieldId);
					if (!editingEntityId.equals(fMeta.getEntity())) {
						EntityMeta<?> eMeta = MetaProvider.getInstance().getEntity(fMeta.getEntity());
						s.append(eMeta.getEntityName());
						s.append('.');
					}
					s.append(fMeta.getFieldName());
				} else {
					vlpParam.setValueFromEntityFieldId(null);
				}
			}
			textLabel.setText(s.toString());
			textLabel.setToolTipText(s.toString());
			repaintStep(parentComponent.get());
		}

		private void showParameterEditor() {
			final JPanel jParameterEditor = new JPanel();
			final TableLayoutBuilder tbllay = new TableLayoutBuilder(jParameterEditor).columns(TableLayout.PREFERRED, 300);
			tbllay.getTableLayout().setVGap(3);
			tbllay.getTableLayout().setHGap(5);

			final JTextField tfValue = new JTextField();
			tfValue.setText(vlpParam.getValue());
			tbllay.addLocalizedLabel("wizard.step.attributevlp.10");
			tbllay.add(tfValue);
			tbllay.newRow().addLocalizedLabel("nuclos.entityfield.entity");
			final JComboBox cbxEntity = newEntityComboBox();
			tbllay.add(cbxEntity);
			tbllay.newRow().addLocalizedLabel("nuclos.entityfield.field");
			final JComboBox cbxField = new JComboBox();
			tbllay.add(cbxField);

			cbxEntity.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(final ItemEvent e) {
					if(e.getStateChange() == ItemEvent.SELECTED) {
						EntityMeta<?> eMeta = (EntityMeta<?>) e.getItem();
						cbxField.removeAllItems();
						UID entityId = eMeta.getUID();
						if (entityId != null) {
							List<FieldMeta<?>> lstFields = new ArrayList<>(MetaProvider.getInstance().getAllEntityFieldsByEntity(entityId).values());
							// Compared by order ;(
							// Collections.sort(lstFields, EntityUtils.getMetaComparator(FieldMeta.class));
							Collections.sort(lstFields, new Comparator<FieldMeta<?>>() {
								@Override
								public int compare(final FieldMeta<?> o1, final FieldMeta<?> o2) {
									if (o1.isSystemField() && !o2.isSystemField()) {
										return 1;
									}
									if (!o1.isSystemField() && o2.isSystemField()) {
										return -1;
									}
									return StringUtils.compareIgnoreCase(o1.getFieldName(), o2.getFieldName());
								}
							});
							cbxField.addItem(EntityUtils.wrapMetaData(FieldMeta.NULL));
							lstFields.stream().forEach(fMeta -> cbxField.addItem(fMeta));
						}
					}
				}
			});

			UID fieldId = UID.parseUID(vlpParam.getValueFromEntityFieldId());
			UID selectEntityId = editingEntityId;
			if (fieldId != null) {
				selectEntityId = MetaProvider.getInstance().getEntityField(fieldId).getEntity();
			}
			for (int i = 0; i < cbxEntity.getItemCount(); i++) {
				if (selectEntityId.equals(((EntityMeta<?>) cbxEntity.getItemAt(i)).getUID())) {
					cbxEntity.setSelectedIndex(i);
					break;
				}
			}
			if (fieldId != null) {
				for (int i = 0; i < cbxField.getItemCount(); i++) {
					if (fieldId.equals(((FieldMeta<?>) cbxField.getItemAt(i)).getUID())) {
						cbxField.setSelectedIndex(i);
						break;
					}
				}
			}

			if (JOptionPane.OK_OPTION ==
					JOptionPane.showConfirmDialog(parentComponent.get(), jParameterEditor, vlpParam.getName(), JOptionPane.OK_CANCEL_OPTION)) {
				vlpParam.setValue(StringUtils.nullIfEmpty(tfValue.getText()));
				FieldMeta<?> fMeta = (FieldMeta<?>) cbxField.getSelectedItem();
				vlpParam.setValueFromEntityFieldId(fMeta.getUID() != null ? fMeta.getUID().getStringifiedDefinitionWithEntity(E.ENTITYFIELD) : null);
				updateText();
			}
		}

		public EntityFieldVlpParam getVlpParam() {
			return vlpParam;
		}

	}

}
