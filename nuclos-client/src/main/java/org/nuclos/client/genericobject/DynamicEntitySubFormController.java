//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.prefs.Preferences;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.NuclosCollectControllerFactory;
import org.nuclos.client.common.Utils;
import org.nuclos.client.common.controller.NuclosCollectControllerCommonState;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.datasource.DatasourceDelegate;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.MasterDataCollectController;
import org.nuclos.client.masterdata.MasterDataLayoutHelper;
import org.nuclos.client.masterdata.MasterDataSubFormController;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelProvider;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.client.ui.popupmenu.DefaultJPopupMenuListener;
import org.nuclos.client.ui.popupmenu.JPopupMenuFactory;
import org.nuclos.client.ui.popupmenu.JPopupMenuListener;
import org.nuclos.client.ui.table.TableUtils;
import org.nuclos.client.ui.table.TableUtils.CopyCellContentMenuItem;
import org.nuclos.client.valuelistprovider.cache.CollectableFieldsProviderCache;
import org.nuclos.common.E;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.common.WorkspaceDescription2.EntityPreferences;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.SearchConditionUtils;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.report.valueobject.DynamicEntityVO;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.genericobject.searchcondition.CollectableGenericObjectSearchExpression;
import org.nuclos.server.genericobject.valueobject.GenericObjectVO;

/**
 * <code>MasterDataSubFormController</code> for dynamic entities.
 * This is a read-only subform, where every line represents a set of attributes from a leased object,
 * which can be opened via context menu.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:uwe.allner@novabit.de">Uwe.Allner</a>
 * @version 01.00.00
 */

public class DynamicEntitySubFormController extends MasterDataSubFormController<Long> {

	private static final Logger LOG = Logger.getLogger(DynamicEntitySubFormController.class);
	
	private JMenuItem miDetails = new JMenuItem(getSpringLocaleDelegate().getMessage(
			"AbstractCollectableComponent.7","Details anzeigen..."));
	private JMenuItem miDefineAsNewSearchResult = new JMenuItem(getSpringLocaleDelegate().getMessage(
			"DynamicEntitySubFormController.1", "In Liste anzeigen"));

	public DynamicEntitySubFormController(MainFrameTab tab,
			CollectableComponentModelProvider clctcompmodelprovider, UID sParentEntityName, SubForm subform,
			Preferences prefsUserParent, EntityPreferences entityPrefs, CollectableFieldsProviderCache valueListProviderCache,
			NuclosCollectControllerCommonState state) {
		super(tab, clctcompmodelprovider, sParentEntityName, subform, prefsUserParent, 
				entityPrefs, valueListProviderCache, state, null);

		setupDetailsStuff();
	}

	private void setupDetailsStuff() {
		final JTable dynamicTable = this.getSubForm().getJTable();
		final JTable fixedTable = this.getSubForm().getSubformRowHeader().getHeaderTable();

		dynamicTable.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		fixedTable.setEnabled(false);
		fixedTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		this.getSubForm().setEnabled(false);

		// double click:
		MouseAdapter mouseAdapter = new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent ev) {
				if (SwingUtilities.isLeftMouseButton(ev) && ev.getClickCount() == 2) {
					if (getSelectedCollectable().getId() != null
							&& getSelectedCollectable().getId() > 0l){
						UID entityUID = getEntityForDynamicLookup(null);
						if (entityUID == null) {
							try {
								final GenericObjectVO govo = GenericObjectDelegate.getInstance().get(getSelectedCollectable().getId());
								entityUID = govo.getModule();
							} catch (Exception e) {
								LOG.warn("error get entity for dynamic lookup. " + e.getMessage());
							}
						}
						if(entityUID != null && MasterDataLayoutHelper.isLayoutMLAvailable(entityUID, false)) {
							cmdShowDetails();
						}
					}
				}
			}
		};
		dynamicTable.addMouseListener(mouseAdapter);
		fixedTable.addMouseListener(mouseAdapter);
		
		final CopyCellContentMenuItem copyCellContentMenuItem = TableUtils.getCopyCellContentMenuItem(dynamicTable);

		// context menu:
		final JPopupMenuFactory factory = new JPopupMenuFactory() {
			@Override
            public JPopupMenu newJPopupMenu() {
				final JPopupMenu result = new JPopupMenu();
				miDetails.addActionListener(new ActionListener() {
					@Override
                    public void actionPerformed(ActionEvent ev) {
						cmdShowDetails();
					}
				});
				miDefineAsNewSearchResult.addActionListener(new ActionListener() {
					@Override
                    public void actionPerformed(ActionEvent ev) {
						cmdDefineSelectedCollectablesAsNewSearchResult();
					}
				});
				result.add(miDetails);
				result.addSeparator();
				result.add(miDefineAsNewSearchResult);
				result.addSeparator();
				result.add(copyCellContentMenuItem);
				
				return result;
			}
		};
		JPopupMenuListener popupMenuListener = new DefaultJPopupMenuListener(factory, true) {
			@Override
			public void mousePressed(MouseEvent ev) {
				if (ev.getClickCount() == 1) {
					// select current row before opening the menu:
					/** @todo factor out this default selection behavior if possible */
					if (ev.getSource() instanceof JTable) {
						final int iRow = ((JTable)ev.getSource()).rowAtPoint(ev.getPoint());
						if (iRow >= 0) {
//							 Nur, wenn nicht selektiert, selektieren:
							if (!((JTable)ev.getSource()).getSelectionModel().isSelectedIndex(iRow)) {
								if ((ev.getModifiers() & MouseEvent.CTRL_MASK) != 0) {
									// Control gedr\u00fcckt:
									// Zeile zur Selektion hinzuf\u00fcgen:
									((JTable)ev.getSource()).getSelectionModel().addSelectionInterval(iRow, iRow);
								} else {
									// Sonst nur diese Zeile selektieren:
									((JTable)ev.getSource()).getSelectionModel().setSelectionInterval(iRow, iRow);
								}
							}  // if
						}
						
						final int iSelectedRowCount = ((JTable)ev.getSource()).getSelectedRowCount();

						UID entityUID = getEntityForDynamicLookup(null);
						if (entityUID == null) {
							if (getSelectedCollectable()!=null && getSelectedCollectable().getId() != null
									&& getSelectedCollectable().getId() > 0l) {
								try {
									final GenericObjectVO govo = GenericObjectDelegate.getInstance().get(getSelectedCollectable().getId());
									entityUID = govo.getModule();	
								} catch (Exception e) {
									LOG.warn("error get entity for dynamic lookup. " + e.getMessage());
								}
							}
						}
						
						boolean bShowDetailsEnabled;
						try {
							bShowDetailsEnabled = iSelectedRowCount == 1 && entityUID != null
								&& getSelectedCollectable().getId() != null && getSelectedCollectable().getId() > 0l
									&& MasterDataLayoutHelper.isLayoutMLAvailable(entityUID, false);
						}
						catch (Exception ex) {
							bShowDetailsEnabled = false;
						}
						if (bShowDetailsEnabled) {
							bShowDetailsEnabled = SecurityCache.getInstance().isReadAllowedForEntity(entityUID);
						}
						miDetails.setEnabled(bShowDetailsEnabled);
						
						boolean bDefineAsNewSearchResultEnabled;
						try {
							bDefineAsNewSearchResultEnabled = iSelectedRowCount > 1
									&& entityUID != null && MasterDataLayoutHelper.isLayoutMLAvailable(entityUID, true);
						}
						catch (Exception ex) {
							bDefineAsNewSearchResultEnabled = false;
						}
						if (bDefineAsNewSearchResultEnabled) {
							bDefineAsNewSearchResultEnabled = SecurityCache.getInstance().isReadAllowedForEntity(entityUID);
						}
						miDefineAsNewSearchResult.setEnabled(bDefineAsNewSearchResultEnabled);
						
						copyCellContentMenuItem.update(ev.getPoint());
					}
				}
				super.mousePressed(ev);
			}
		};
		dynamicTable.addMouseListener(popupMenuListener);
		fixedTable.addMouseListener(popupMenuListener);
	}

	private void cmdShowDetails() {
		UIUtils.runCommandForTabbedPane(getMainFrameTabbedPane(), new CommonRunnable() {
			@Override
            public void run() throws CommonBusinessException {
				final Collectable<Long> clct = getSelectedCollectable();
				assert clct != null;
				UID entityUID = getEntityForDynamicLookup(null);
				if (entityUID == null) {
					try {
						final GenericObjectVO govo = GenericObjectDelegate.getInstance().get(getSelectedCollectable().getId());
						entityUID = govo.getModule();	
					} catch (Exception e) {
						LOG.warn("error get entity for dynamic lookup. " + e.getMessage());
					}
				}
				if (entityUID != null) {
					showDetails(entityUID, clct.getId());
				}
			}
		});
	}

	@Override
	protected boolean getDefaultForOpenDetailsWithTabRecycling() {
		return true;
	}

	private void cmdDefineSelectedCollectablesAsNewSearchResult() {
		UIUtils.runCommandForTabbedPane(getMainFrameTabbedPane(), new CommonRunnable(){
			@Override
            public void run() throws CommonBusinessException {
				final Collection<Collectable> collclct = CollectionUtils.typecheck(getSelectedCollectables(), Collectable.class);

				assert CollectionUtils.isNonEmpty(collclct);
				final CollectableSearchCondition cond = getCollectableSearchCondition(collclct);

				final UID entityUID = getEntityForDynamicLookup(null);
				if(entityUID != null) {
					if(MetaProvider.getInstance().getEntity(entityUID).isStateModel()) {
						showGenericobjectInResult(collclct, cond, entityUID);
					} else {
						showMasterDataInResult(cond, entityUID);
					}
				} else {
					showGenericobjectInResult(collclct, cond, entityUID);
				}
			}

			private void showMasterDataInResult(final CollectableSearchCondition cond, UID entityUID) throws CommonBusinessException {
				final MasterDataCollectController<?> ctlMasterdata
					= NuclosCollectControllerFactory.getInstance().newMasterDataCollectController(
							entityUID, null, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
				ctlMasterdata.runViewResults(cond);
			}

			private void showGenericobjectInResult(final Collection<Collectable> collclct, final CollectableSearchCondition cond, UID entityUID) throws CommonPermissionException, CommonBusinessException {
				if (entityUID == null) {
					entityUID = getCommonModuleUid(collclct);
				}
				final GenericObjectCollectController ctlGenericObject = NuclosCollectControllerFactory.getInstance().
						newGenericObjectCollectController(entityUID, null, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
				ctlGenericObject.setSearchDeleted(CollectableGenericObjectSearchExpression.SEARCH_BOTH);
				ctlGenericObject.runViewResults(cond);
			}
		});
	}

	private UID getEntityForDynamicLookup(UID entityUid) {
		final CollectableEntity colEntity = DynamicEntitySubFormController.this.getCollectableEntity();
		final UID dynEntityMeta = colEntity.getUID();
		if(dynEntityMeta != null) {
			final String entityUidPrefix = E.DYNAMICENTITY.getUID().getString()+"-";
			final UID dynEntityDS = new UID(dynEntityMeta.getString().replaceFirst("^"+entityUidPrefix, ""));
			try {
				final DynamicEntityVO voDyn = DatasourceDelegate.getInstance().getDynamicEntity(dynEntityDS);
				if (voDyn == null) {
					throw new NuclosFatalException("Dynamic entity with UID " + dynEntityDS + " does not exist");
				}
				entityUid = voDyn.getEntityUID();
			} catch (Exception e) {
				LOG.warn("error get entity for dynamic lookup. " + e.getMessage());
			}
		}
		return entityUid;
	}

	/**
	 * §precondition !CollectionUtils.isNullOrEmpty(collclct)
	 */
	private static CollectableSearchCondition getCollectableSearchCondition(Collection<Collectable> collclct) {
		final Collection<Object> collIds = CollectionUtils.transform(collclct, new Transformer<Collectable, Object>() {
			@Override
            public Object transform(Collectable clct) {
				return clct.getId();
			}
		});

		return SearchConditionUtils.getCollectableSearchConditionForIds(collIds);
	}

	/**
	 * @return the module id shared by all collectables, if any.
	 */
	private static UID getCommonModuleUid(Collection<Collectable> collclct) throws CommonPermissionException{
		return Utils.getCommonObject(CollectionUtils.transform(collclct, new Transformer<Collectable, UID>() {
			@Override
            public UID transform(Collectable clct) {
				try {
					return GenericObjectDelegate.getInstance().get((Long)clct.getId()).getModule();
				} catch (CommonBusinessException e) {
					LOG.warn("getCommonModuleId failed: " + e);
					return null;
				}
			}
		}));
	}

}	// class DynamicEntitySubFormController
