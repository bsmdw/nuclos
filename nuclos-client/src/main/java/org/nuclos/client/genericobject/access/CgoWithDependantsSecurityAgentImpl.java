//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject.access;

import org.nuclos.client.attribute.AttributeCache;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.genericobject.CollectableGenericObject;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.access.CefSecurityAgent;
import org.nuclos.common.security.IPermission;

public class CgoWithDependantsSecurityAgentImpl<PK> implements CefSecurityAgent<PK> {

	private CollectableGenericObject cgo;

	private final CollectableEntityField field;

	private final boolean fieldBelongsToSubEntity;
	
	private final SecurityCache securityCache;

	public CgoWithDependantsSecurityAgentImpl(CollectableGenericObject cgo, CollectableEntityField field, boolean fieldBelongsToSubEntity) {
		this.cgo = cgo;
		this.field = field;
		this.fieldBelongsToSubEntity = fieldBelongsToSubEntity;
		this.securityCache = SecurityCache.getInstance();
	}

	/**
	 * sets the <code>Collectable</code> for the <code>CollectableEntityField</code>
	 */
	public void setCollectable(Collectable<PK> clct) {
		if (!(clct instanceof CollectableGenericObject)) {
			throw new IllegalArgumentException("clct not instanceof CollectableGenericObject");			
		}
		
		this.cgo = (CollectableGenericObject)clct;
	}

	private CollectableGenericObject getCollectable() {
		return cgo;
	}

	@Override
	public boolean isReadable() {
		final UID entityUid = getCollectable().getEntityUID();
		final UID stateFieldUid = SF.STATE.getUID(entityUid);
		
		final IPermission permission;
		final UID stateUid;
				
		if (!AttributeCache.getInstance().contains(stateFieldUid)) {
			stateUid = null;			
		}
		else {
			stateUid = (UID)getCollectable().getField(stateFieldUid).getValueId();			
		}

		// check subform data
		if (fieldBelongsToSubEntity) {
			permission = securityCache.getSubFormPermission(field.getEntityUID(), stateUid);
		}
		// check attribute data
		else {
			permission = securityCache.getAttributePermission(entityUid, field.getUID(), stateUid);
		}
		
		return permission != null ? permission.includesReading() : false;
	}

	@Override
	public boolean isWritable() {
		return false;
	}

	@Override
	public boolean isRemovable() {
		return false;
	}

}
