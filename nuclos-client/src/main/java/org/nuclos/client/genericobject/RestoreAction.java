package org.nuclos.client.genericobject;

import java.io.Serializable;
import java.util.Map;

import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.client.ui.multiaction.MultiCollectablesActionController.Action;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;

/**
 * Restore Action for {@link Collectable}
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 * @param <PK> generic collectable
 */
public class RestoreAction<PK>  implements Action<Collectable<PK>, Object> {
	private final CollectController<PK,Collectable<PK>> ctl;

	public RestoreAction(CollectController<PK,Collectable<PK>> ctl) {
		this.ctl = ctl;
	}

	@Override
	public Object perform(Collectable<PK> clct, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		ctl.checkedRestoreCollectable(clct, applyMultiEditContext);
		ctl.getSearchStrategy().search();
		return null;
	}

	@Override
	public String getText(Collectable<PK> clct) {
		return SpringLocaleDelegate.getInstance().getMessage(
				"RestoreSelectedCollectablesController.1", "Datensatz {0} wird wiederhergestellt...", SpringLocaleDelegate.getInstance().getIdentifierLabel(clct, ctl.getEntityUid(), MetaProvider.getInstance()));
	}

	@Override
	public String getSuccessfulMessage(Collectable<PK> clct, Object oResult) {
		return SpringLocaleDelegate.getInstance().getMessage(
				"RestoreSelectedCollectablesController.2", "Datensatz {0} erfolgreich wiederhergestellt.", SpringLocaleDelegate.getInstance().getIdentifierLabel(clct, ctl.getEntityUid(), MetaProvider.getInstance()));
	}

	@Override
	public String getConfirmStopMessage() {
		return SpringLocaleDelegate.getInstance().getMessage(
				"RestoreSelectedCollectablesController.3", "Wollen Sie die Wiederherstellung der Datens\u00e4tze an dieser Stelle beenden?\n(Die bisher wiederhergestellten Datens\u00e4tze bleiben in jedem Fall erhalten.)");
	}

	@Override
	public String getExceptionMessage(Collectable<PK> clct, Exception ex) {
		return SpringLocaleDelegate.getInstance().getMessage(
				"RestoreSelectedCollectablesController.4", "Datensatz {0} konnte nicht wiederhergestellt werden.", SpringLocaleDelegate.getInstance().getIdentifierLabel(clct, ctl.getEntityUid(), MetaProvider.getInstance())) + " " + ex.getMessage();
	}

	@Override
	public void executeFinalAction() throws CommonBusinessException {
		// do nothing
	}
}