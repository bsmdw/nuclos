package org.nuclos.client.genericobject.controller;

import java.awt.Component;

import javax.swing.Icon;

/**
 * Created by Oliver Brausch on 18.07.17.
 */
public class RemovedTab {
	private final int idx;
	private final Component c;
	private final String title;
	private final String tooltip;
	private final Icon icon;

	public RemovedTab(int idx, Component c, String title, String tooltip,  Icon icon) {
		this.idx = idx;
		this.c = c;
		this.title = title;
		this.tooltip = tooltip;
		this.icon = icon;
	}

	public int getIdx() {
		return idx;
	}

	public Component getC() {
		return c;
	}

	public String getTitle() {
		return title;
	}

	public String getTooltip() {
		return tooltip;
	}

	public Icon getIcon() {
		return icon;
	}

}
