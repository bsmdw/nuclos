//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import java.util.Date;

import org.nuclos.api.UID;
import org.nuclos.api.businessobject.attribute.Attribute;
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute;
import org.nuclos.api.businessobject.attribute.NumericAttribute;
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute;
import org.nuclos.api.businessobject.attribute.StringAttribute;
import org.nuclos.api.common.NuclosUser;
import org.nuclos.common.E;
import org.nuclos.server.nbo.AbstractBusinessObject;

/**
 * @Deprecated For backward compatibility of rules only (via nuclos-ccce.jar bundle)
 * Use org.nuclos.system.User
 *
 * BusinessObject: nuclos_user
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_MD_USER
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@Deprecated
public class nuclosuser extends AbstractBusinessObject<UID> implements NuclosUser {
private static final long serialVersionUID = 1L;



/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<UID> Id =
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "dRxj", "dRxj0", UID.class);


/**
 * Attribute: superuser
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: BLNSUPERUSER
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<Boolean> Superuser =
	new Attribute<>("Superuser", "org.nuclos.businessentity", "dRxj", "dRxjh", Boolean.class);


/**
 * Attribute: locked
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: BLNLOCKED
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<Boolean> Locked =
	new Attribute<>("Locked", "org.nuclos.businessentity", "dRxj", "dRxji", Boolean.class);


/**
 * Attribute: passwordchanged
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: DATPASSWORDCHANGED
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> Passwordchanged =
	new NumericAttribute<>("Passwordchanged", "org.nuclos.businessentity", "dRxj", "dRxjj", Date.class);


/**
 * Attribute: expirationdate
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: DATEXPIRATIONDATE
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> Expirationdate =
	new NumericAttribute<>("Expirationdate", "org.nuclos.businessentity", "dRxj", "dRxjk", Date.class);


/**
 * Attribute: requirepasswordchange
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: BLNREQUIREPASSWORDCHANGE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<Boolean> Requirepasswordchange =
	new Attribute<>("Requirepasswordchange", "org.nuclos.businessentity", "dRxj", "dRxjl", Boolean.class);


/**
 * Attribute: lastlogin
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: DATLASTLOGIN
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> Lastlogin =
	new NumericAttribute<>("Lastlogin", "org.nuclos.businessentity", "dRxj", "dRxjm", Date.class);


/**
 * Attribute: loginattempts
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: INTLOGINATTEMPTS
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public static final NumericAttribute<Integer> Loginattempts =
	new NumericAttribute<>("Loginattempts", "org.nuclos.businessentity", "dRxj", "dRxjn", Integer.class);


/**
 * Attribute: communicationAccountPhone
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRUID_COMPORT_PHONE
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<UID> CommunicationAccountPhoneId =
	new ForeignKeyAttribute<>("CommunicationAccountPhoneId", "org.nuclos.businessentity", "dRxj", "dRxjo", UID.class);


/**
 * Attribute: name
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRUSER
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 30
 *<br>Precision: null
**/
public static final StringAttribute<String> Name = new StringAttribute<>("Name", "org.nuclos.businessentity", "dRxj", "dRxja", String.class);


/**
 * Attribute: email
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STREMAIL
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> Email = new StringAttribute<>("Email", "org.nuclos.businessentity", "dRxj", "dRxjb", String.class);


/**
 * Attribute: lastname
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRLASTNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> Lastname = new StringAttribute<>("Lastname", "org.nuclos.businessentity", "dRxj", "dRxjc", String.class);


/**
 * Attribute: firstname
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRFIRSTNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> Firstname = new StringAttribute<>("Firstname", "org.nuclos.businessentity", "dRxj", "dRxjd", String.class);


/**
 * Attribute: preferences
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: OBJPREFERENCES
 *<br>Data type: [B
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<byte[]> Preferences = 
	new Attribute<>("Preferences", "org.nuclos.businessentity", "dRxj", "dRxjf", byte[].class);


/**
 * Attribute: password
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRPASSWORD
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> Password = new StringAttribute<>("Password", "org.nuclos.businessentity", "dRxj", "dRxjg", String.class);


/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final Attribute<UID> PrimaryKey =
	new Attribute<>("PrimaryKey", "org.nuclos.businessentity", "dRxj", "dRxj0", UID.class);


/**
 * Attribute: activationcode
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRACTIVATIONCODE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> Activationcode = new StringAttribute<>("Activationcode", "org.nuclos.businessentity", "dRxj", "dRxjp", String.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "dRxj", "dRxj1", Date.class);


/**
 * Attribute: privacyconsent
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: BLNPRIVACYCONSENT
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<Boolean> Privacyconsent =
	new Attribute<>("Privacyconsent", "org.nuclos.businessentity", "dRxj", "dRxjq", Boolean.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "dRxj", "dRxj2", String.class);


/**
 * Attribute: passwordresetcode
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRPASSWORDRESETCODE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> Passwordresetcode = new StringAttribute<>("Passwordresetcode", "org.nuclos.businessentity", "dRxj", "dRxjr", String.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "dRxj", "dRxj3", Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "dRxj", "dRxj4", String.class);


public nuclosuser() {
		super("dRxj");
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public UID getEntityUid() {
		return new org.nuclos.common.UID("dRxj");
}


/**
 * Getter-Method for attribute: superuser
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: BLNSUPERUSER
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Boolean getSuperuser() {
		return getField("dRxjh", Boolean.class);
}


	/**
 * Getter-Method for attribute: locked
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: BLNLOCKED
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Boolean getLocked() {
		return getField("dRxji", Boolean.class);
}


	/**
 * Getter-Method for attribute: passwordchanged
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: DATPASSWORDCHANGED
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getPasswordchanged() {
		return getField("dRxjj", Date.class);
}


/**
 * Getter-Method for attribute: expirationdate
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: DATEXPIRATIONDATE
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getExpirationdate() {
		return getField("dRxjk", Date.class);
}


/**
 * Getter-Method for attribute: requirepasswordchange
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: BLNREQUIREPASSWORDCHANGE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Boolean getRequirepasswordchange() {
		return getField("dRxjl", Boolean.class);
}


/**
 * Getter-Method for attribute: lastlogin
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: DATLASTLOGIN
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getLastlogin() {
		return getField("dRxjm", Date.class);
}


/**
 * Getter-Method for attribute: loginattempts
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: INTLOGINATTEMPTS
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public Integer getLoginattempts() {
		return getField("dRxjn", Integer.class);
}


/**
 * Getter-Method for attribute: communicationAccountPhone
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRUID_COMPORT_PHONE
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_userCommunicationAccount
 *<br>Reference field: communicationPort (account)
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public UID getCommunicationAccountPhoneId() {
		return getFieldUid("dRxjo");
}


	/**
 * Getter-Method for attribute: name
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRUSER
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 30
 *<br>Precision: null
**/
public String getName() {
		return getField("dRxja", String.class);
}


/**
 * Getter-Method for attribute: email
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STREMAIL
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getEmail() {
		return getField("dRxjb", String.class);
}


	/**
 * Getter-Method for attribute: lastname
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRLASTNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getLastname() {
		return getField("dRxjc", String.class);
}


	/**
 * Getter-Method for attribute: firstname
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRFIRSTNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getFirstname() {
		return getField("dRxjd", String.class);
}


/**
 * Getter-Method for attribute: preferences
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: OBJPREFERENCES
 *<br>Data type: [B
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public byte[] getPreferences() {
		return getField("dRxjf", byte[].class); 
}


	/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public org.nuclos.common.UID getPrimaryKey() {
		return getField("dRxj0", org.nuclos.common.UID.class); 
}


/**
 * Getter-Method for attribute: activationcode
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRACTIVATIONCODE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getActivationcode() {
		return getField("dRxjp", String.class);
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getCreatedAt() {
		return getField("dRxj1", Date.class);
}


/**
 * Getter-Method for attribute: privacyconsent
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: BLNPRIVACYCONSENT
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Boolean getPrivacyconsent() {
		return getField("dRxjq", Boolean.class);
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getCreatedBy() {
		return getField("dRxj2", String.class);
}


/**
 * Getter-Method for attribute: passwordresetcode
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRPASSWORDRESETCODE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getPasswordresetcode() {
		return getField("dRxjr", String.class);
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getChangedAt() {
		return getField("dRxj3", Date.class);
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getChangedBy() {
		return getField("dRxj4", String.class);
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save() throws org.nuclos.api.exception.BusinessException {
		super.save();
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(UID id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("dRxj"), id);
}
/**
* Static Get by Id
*/
public static nuclosuser get(UID id) {
		return get(nuclosuser.class, id);
}


	@Override
	public String getUsername() {
		return getName();
	}


	@Override
	public Boolean getLoginWithEmailAllowed() {
		return getField(E.USER.loginWithEmailAllowed.getUID().getString(), Boolean.class);
	}

	@Override
	public Boolean getPasswordChangeRequired() {
		return getRequirepasswordchange();
	}

	@Override
	public void setUsername(final String username) {
		setField(E.USER.username.getUID().getString(), username);
	}

	@Override
	public void setFirstname(final String firstname) {
		setField(E.USER.firstname.getUID().getString(), firstname);
	}

	@Override
	public void setLastname(final String lastname) {
		setField(E.USER.lastname.getUID().getString(), lastname);
	}

	@Override
	public void setEmail(final String email) {
		setField(E.USER.email.getUID().getString(), email);
	}

	@Override
	public void setLoginWithEmailAllowed(final Boolean loginWithEmailAllowed) {
		setField(E.USER.loginWithEmailAllowed.getUID().getString(), loginWithEmailAllowed);
	}

	@Override
	public void setPasswordChangeRequired(final Boolean passwordChangeRequired) {
		setField(E.USER.passwordChangeRequired.getUID().getString(), passwordChangeRequired);
	}

	@Override
	public String getPasswordResetCode() {
		return getPasswordresetcode();
	}

	@Override
	public void setPasswordResetCode(final String passwordResetCode) {
		setField(E.USER.passwordResetCode.getUID().getString(), passwordResetCode);
	}

	@Override
	public void setLocked(final Boolean locked) {
		setField(E.USER.locked.getUID().getString(), locked);
	}

	@Override
	public Date getLastPasswordChange() {
		return getPasswordchanged();
	}

	@Override
	public Date getExpirationDate() {
		return getExpirationdate();
	}

	@Override
	public void setExpirationDate(final Date expirationDate) {
		setField(E.USER.expirationDate.getUID().getString(), expirationDate);
	}

	@Override
	public Date getLastLogin() {
		return getLastlogin();
	}

	@Override
	public Integer getLoginAttempts() {
		return getLoginattempts();
	}

	@Override
	public Boolean getPrivacyConsentAccepted() {
		return getPrivacyconsent();
	}

	@Override
	public void setPrivacyConsentAccepted(final Boolean privacyConsentAccepted) {
		setField(E.USER.privacyConsentAccepted.getUID().getString(), privacyConsentAccepted);
	}

	@Override
	public String getActivationCode() {
		return getActivationcode();
	}

	@Override
	public void setActivationCode(final String activationCode) {
		setField(E.USER.activationCode.getUID().getString(), activationCode);
	}

	@Override
	public void setSuperuser(final Boolean superuser) {
		setField(E.USER.superuser.getUID().getString(), superuser);
	}

}

