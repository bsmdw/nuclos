//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import java.util.Date;

import org.nuclos.api.UID;
import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.businessobject.Dependent;
import org.nuclos.api.businessobject.attribute.Attribute;
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute;
import org.nuclos.api.businessobject.attribute.NumericAttribute;
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute;
import org.nuclos.api.businessobject.attribute.StringAttribute;
import org.nuclos.server.nbo.AbstractBusinessObject;

/**
 * @Deprecated For backward compatibility of rules only (via nuclos-ccce.jar bundle)
 * Use org.nuclos.system.RoleUser
 *
 * BusinessObject: nuclos_roleuser
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_MD_ROLE_USER
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@Deprecated
public class nuclosroleuser extends AbstractBusinessObject<UID> {
private static final long serialVersionUID = 1L;



/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_roleuser
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<UID> Id =
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "dYdq", "dYdq0", UID.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_roleuser
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "dYdq", "dYdq1", Date.class);


/**
 * Attribute: role
 *<br>
 *<br>Entity: nuclos_roleuser
 *<br>DB-Name: STRUID_T_MD_ROLE
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<UID> RoleId =
	new ForeignKeyAttribute<>("RoleId", "org.nuclos.businessentity", "dYdq", "dYdqa", UID.class);


/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_roleuser
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final Attribute<UID> PrimaryKey =
	new Attribute<>("PrimaryKey", "org.nuclos.businessentity", "dYdq", "dYdq0", UID.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_roleuser
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "dYdq", "dYdq4", String.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_roleuser
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "dYdq", "dYdq3", Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_roleuser
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "dYdq", "dYdq2", String.class);


/**
 * Attribute: user
 *<br>
 *<br>Entity: nuclos_roleuser
 *<br>DB-Name: STRUID_T_MD_USER
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<UID> UserId =
	new ForeignKeyAttribute<>("UserId", "org.nuclos.businessentity", "dYdq", "dYdqb", UID.class);


public nuclosroleuser() {
		super("dYdq");
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public UID getEntityUid() {
		return new org.nuclos.common.UID("dYdq");
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_roleuser
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getCreatedAt() {
		return getField("dYdq1", Date.class);
}


/**
 * Getter-Method for attribute: role
 *<br>
 *<br>Entity: nuclos_roleuser
 *<br>DB-Name: STRUID_T_MD_ROLE
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_role
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public UID getRoleId() {
		return getFieldUid("dYdqa");
}


/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_roleuser
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public org.nuclos.common.UID getPrimaryKey() {
		return getField("dYdq0", org.nuclos.common.UID.class); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_roleuser
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getChangedBy() {
		return getField("dYdq4", String.class);
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_roleuser
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getChangedAt() {
		return getField("dYdq3", Date.class);
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_roleuser
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getCreatedBy() {
		return getField("dYdq2", String.class);
}


/**
 * Getter-Method for attribute: user
 *<br>
 *<br>Entity: nuclos_roleuser
 *<br>DB-Name: STRUID_T_MD_USER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_user
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public UID getUserId() {
		return getFieldUid("dYdqb");
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save() throws org.nuclos.api.exception.BusinessException {
		super.save();
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(UID id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("dYdq"), id);
}
/**
* Static Get by Id
*/
public static nuclosroleuser get(UID id) {
		return get(nuclosroleuser.class, id);
}

	@Override
	protected <DPK, T extends BusinessObject<DPK>> void deleteDependent(final Dependent<T> depAttr, final T dep) {
		super.deleteDependent(depAttr, dep);
	}
}
