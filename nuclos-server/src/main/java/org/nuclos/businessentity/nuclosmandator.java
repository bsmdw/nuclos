//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import java.util.Date;

import org.nuclos.api.UID;
import org.nuclos.api.businessobject.attribute.Attribute;
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute;
import org.nuclos.api.businessobject.attribute.NumericAttribute;
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute;
import org.nuclos.api.businessobject.attribute.StringAttribute;
import org.nuclos.api.common.NuclosMandator;
import org.nuclos.server.nbo.AbstractBusinessObject;

/**
 * @Deprecated For backward compatibility of rules only (via nuclos-ccce.jar bundle)
 * Use org.nuclos.system.Mandator
 *
 * BusinessObject: nuclos_mandator
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_MD_MANDATOR
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@Deprecated
public class nuclosmandator extends AbstractBusinessObject<UID> implements NuclosMandator {
private static final long serialVersionUID = 1L;



/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_mandator
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<UID> Id =
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "AExm", "AExm0", UID.class);


/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_mandator
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final Attribute<UID> PrimaryKey =
	new Attribute<>("PrimaryKey", "org.nuclos.businessentity", "AExm", "AExm0", UID.class);


/**
 * Attribute: color
 *<br>
 *<br>Entity: nuclos_mandator
 *<br>DB-Name: STRCOLOR
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 10
 *<br>Precision: null
**/
public static final StringAttribute<String> Color = new StringAttribute<>("Color", "org.nuclos.businessentity", "AExm", "AExme", String.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_mandator
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "AExm", "AExm2", String.class);


/**
 * Attribute: level
 *<br>
 *<br>Entity: nuclos_mandator
 *<br>DB-Name: STRUID_T_MD_MANDATOR_LEVEL
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<UID> LevelId =
	new ForeignKeyAttribute<>("LevelId", "org.nuclos.businessentity", "AExm", "AExmb", UID.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_mandator
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "AExm", "AExm1", Date.class);


/**
 * Attribute: name
 *<br>
 *<br>Entity: nuclos_mandator
 *<br>DB-Name: STRNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> Name = new StringAttribute<>("Name", "org.nuclos.businessentity", "AExm", "AExma", String.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_mandator
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "AExm", "AExm4", String.class);


/**
 * Attribute: path
 *<br>
 *<br>Entity: nuclos_mandator
 *<br>DB-Name: STRPATH
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> Path = new StringAttribute<>("Path", "org.nuclos.businessentity", "AExm", "AExmd", String.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_mandator
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "AExm", "AExm3", Date.class);


/**
 * Attribute: parentMandator
 *<br>
 *<br>Entity: nuclos_mandator
 *<br>DB-Name: STRUID_PARENTMANDATOR
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<UID> ParentMandatorId =
	new ForeignKeyAttribute<>("ParentMandatorId", "org.nuclos.businessentity", "AExm", "AExmc", UID.class);


public nuclosmandator() {
		super("AExm");
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public UID getEntityUid() {
		return new org.nuclos.common.UID("AExm");
}


/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_mandator
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public org.nuclos.common.UID getPrimaryKey() {
		return getField("AExm0", org.nuclos.common.UID.class); 
}


/**
 * Getter-Method for attribute: color
 *<br>
 *<br>Entity: nuclos_mandator
 *<br>DB-Name: STRCOLOR
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 10
 *<br>Precision: null
**/
public String getColor() {
		return getField("AExme", String.class);
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_mandator
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getCreatedBy() {
		return getField("AExm2", String.class);
}


/**
 * Getter-Method for attribute: level
 *<br>
 *<br>Entity: nuclos_mandator
 *<br>DB-Name: STRUID_T_MD_MANDATOR_LEVEL
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_mandatorLevel
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public UID getLevelId() {
		return getFieldUid("AExmb");
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_mandator
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getCreatedAt() {
		return getField("AExm1", Date.class);
}


/**
 * Getter-Method for attribute: name
 *<br>
 *<br>Entity: nuclos_mandator
 *<br>DB-Name: STRNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getName() {
		return getField("AExma", String.class);
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_mandator
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getChangedBy() {
		return getField("AExm4", String.class);
}


/**
 * Getter-Method for attribute: path
 *<br>
 *<br>Entity: nuclos_mandator
 *<br>DB-Name: STRPATH
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getPath() {
		return getField("AExmd", String.class);
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_mandator
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getChangedAt() {
		return getField("AExm3", Date.class);
}


/**
 * Getter-Method for attribute: parentMandator
 *<br>
 *<br>Entity: nuclos_mandator
 *<br>DB-Name: STRUID_PARENTMANDATOR
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_mandator
 *<br>Reference field: path
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public UID getParentMandatorId() {
		return getFieldUid("AExmc");
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save() throws org.nuclos.api.exception.BusinessException {
		super.save();
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(UID id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("AExm"), id);
}
/**
* Static Get by Id
*/
public static nuclosmandator get(UID id) {
		return get(nuclosmandator.class, id);
}
 }
