//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import java.util.Date;

import org.nuclos.api.UID;
import org.nuclos.api.businessobject.attribute.Attribute;
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute;
import org.nuclos.api.businessobject.attribute.NumericAttribute;
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute;
import org.nuclos.api.businessobject.attribute.StringAttribute;
import org.nuclos.server.nbo.AbstractBusinessObject;

/**
 * @Deprecated For backward compatibility of rules only (via nuclos-ccce.jar bundle)
 * Use org.nuclos.system.Process
 *
 * BusinessObject: nuclos_process
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_MD_PROCESS
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@Deprecated
public class nuclosprocess extends AbstractBusinessObject<UID> {
private static final long serialVersionUID = 1L;



/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_process
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<UID> Id =
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "ui9l", "ui9l0", UID.class);


/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_process
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final Attribute<UID> PrimaryKey =
	new Attribute<>("PrimaryKey", "org.nuclos.businessentity", "ui9l", "ui9l0", UID.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_process
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "ui9l", "ui9l2", String.class);


/**
 * Attribute: name
 *<br>
 *<br>Entity: nuclos_process
 *<br>DB-Name: STRPROCESS
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> Name = new StringAttribute<>("Name", "org.nuclos.businessentity", "ui9l", "ui9lb", String.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_process
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "ui9l", "ui9l1", Date.class);


/**
 * Attribute: module
 *<br>
 *<br>Entity: nuclos_process
 *<br>DB-Name: STRUID_T_MD_MODULE
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<UID> ModuleId =
	new ForeignKeyAttribute<>("ModuleId", "org.nuclos.businessentity", "ui9l", "ui9la", UID.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_process
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "ui9l", "ui9l4", String.class);


/**
 * Attribute: validFrom
 *<br>
 *<br>Entity: nuclos_process
 *<br>DB-Name: DATVALIDFROM
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> ValidFrom =
	new NumericAttribute<>("ValidFrom", "org.nuclos.businessentity", "ui9l", "ui9ld", Date.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_process
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "ui9l", "ui9l3", Date.class);


/**
 * Attribute: description
 *<br>
 *<br>Entity: nuclos_process
 *<br>DB-Name: STRDESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public static final StringAttribute<String> Description = new StringAttribute<>("Description", "org.nuclos.businessentity", "ui9l", "ui9lc", String.class);


/**
 * Attribute: nuclet
 *<br>
 *<br>Entity: nuclos_process
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<UID> NucletId =
	new ForeignKeyAttribute<>("NucletId", "org.nuclos.businessentity", "ui9l", "ui9lf", UID.class);


/**
 * Attribute: validUntil
 *<br>
 *<br>Entity: nuclos_process
 *<br>DB-Name: DATVALIDUNTIL
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> ValidUntil =
	new NumericAttribute<>("ValidUntil", "org.nuclos.businessentity", "ui9l", "ui9le", Date.class);


public nuclosprocess() {
		super("ui9l");
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public UID getEntityUid() {
		return new org.nuclos.common.UID("ui9l");
}


/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_process
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public org.nuclos.common.UID getPrimaryKey() {
		return getField("ui9l0", org.nuclos.common.UID.class); 
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_process
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getCreatedBy() {
		return getField("ui9l2", String.class);
}


/**
 * Getter-Method for attribute: name
 *<br>
 *<br>Entity: nuclos_process
 *<br>DB-Name: STRPROCESS
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getName() {
		return getField("ui9lb", String.class);
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_process
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getCreatedAt() {
		return getField("ui9l1", Date.class);
}


/**
 * Getter-Method for attribute: module
 *<br>
 *<br>Entity: nuclos_process
 *<br>DB-Name: STRUID_T_MD_MODULE
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.UID getModuleId() {
		return getFieldUid("ui9la");
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_process
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getChangedBy() {
		return getField("ui9l4", String.class);
}


/**
 * Getter-Method for attribute: validFrom
 *<br>
 *<br>Entity: nuclos_process
 *<br>DB-Name: DATVALIDFROM
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getValidFrom() {
		return getField("ui9ld", Date.class);
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_process
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getChangedAt() {
		return getField("ui9l3", Date.class);
}


/**
 * Getter-Method for attribute: description
 *<br>
 *<br>Entity: nuclos_process
 *<br>DB-Name: STRDESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public String getDescription() {
		return getField("ui9lc", String.class);
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_process
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.UID getNucletId() {
		return getFieldUid("ui9lf");
}


/**
 * Getter-Method for attribute: validUntil
 *<br>
 *<br>Entity: nuclos_process
 *<br>DB-Name: DATVALIDUNTIL
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getValidUntil() {
		return getField("ui9le", Date.class);
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save() throws org.nuclos.api.exception.BusinessException {
		super.save();
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(UID id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("ui9l"), id);
}
/**
* Static Get by Id
*/
public static nuclosprocess get(UID id) {
		return get(nuclosprocess.class, id);
}
 }
