//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import java.util.Date;

import org.nuclos.api.UID;
import org.nuclos.api.businessobject.attribute.Attribute;
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute;
import org.nuclos.api.businessobject.attribute.NumericAttribute;
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute;
import org.nuclos.api.businessobject.attribute.StringAttribute;
import org.nuclos.server.nbo.AbstractBusinessObject;

/**
 * @Deprecated For backward compatibility of rules only (via nuclos-ccce.jar bundle)
 * Use org.nuclos.system.State
 *
 * BusinessObject: nuclos_state
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_MD_STATE
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@Deprecated
public class nuclosstate extends AbstractBusinessObject<UID> {
private static final long serialVersionUID = 1L;



/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<UID> Id =
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "U89N", "U89N0", UID.class);


/**
 * Attribute: name
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRSTATE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> Name = new StringAttribute<>("Name", "org.nuclos.businessentity", "U89N", "U89Na", String.class);


/**
 * Attribute: model
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRUID_T_MD_STATEMODEL
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<UID> ModelId =
	new ForeignKeyAttribute<>("ModelId", "org.nuclos.businessentity", "U89N", "U89Nb", UID.class);


/**
 * Attribute: description
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRDESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public static final StringAttribute<String> Description = new StringAttribute<>("Description", "org.nuclos.businessentity", "U89N", "U89Nc", String.class);


/**
 * Attribute: numeral
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: INTNUMERAL
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 3
 *<br>Precision: null
**/
public static final NumericAttribute<Integer> Numeral =
	new NumericAttribute<>("Numeral", "org.nuclos.businessentity", "U89N", "U89Nd", Integer.class);


/**
 * Attribute: labelres
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STR_LOCALERESOURCE_L
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> Labelres = new StringAttribute<>("Labelres", "org.nuclos.businessentity", "U89N", "U89Ne", String.class);


/**
 * Attribute: descriptionres
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STR_LOCALERESOURCE_D
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> Descriptionres = new StringAttribute<>("Descriptionres", "org.nuclos.businessentity", "U89N", "U89Nf", String.class);


/**
 * Attribute: tab
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRTAB
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> Tab = new StringAttribute<>("Tab", "org.nuclos.businessentity", "U89N", "U89Ng", String.class);


/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final Attribute<UID> PrimaryKey =
	new Attribute<>("PrimaryKey", "org.nuclos.businessentity", "U89N", "U89N0", UID.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "U89N", "U89N1", Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "U89N", "U89N2", String.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "U89N", "U89N3", Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "U89N", "U89N4", String.class);


/**
 * Attribute: icon
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: OBJICON
 *<br>Data type: org.nuclos.common.NuclosImage
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final Attribute<org.nuclos.api.NuclosImage> Icon = 
	new Attribute<>("Icon", "org.nuclos.businessentity", "U89N", "U89Nh", org.nuclos.api.NuclosImage.class);


/**
 * Attribute: buttonIcon
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRUID_BUTTONICON
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<UID> ButtonIconId =
	new ForeignKeyAttribute<>("ButtonIconId", "org.nuclos.businessentity", "U89N", "U89Ni", UID.class);


/**
 * Attribute: color
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRCOLOR
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 10
 *<br>Precision: null
**/
public static final StringAttribute<String> Color = new StringAttribute<>("Color", "org.nuclos.businessentity", "U89N", "U89Nj", String.class);


/**
 * Attribute: buttonRes
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STR_LOCALERESOURCE_BUT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> ButtonRes = new StringAttribute<>("ButtonRes", "org.nuclos.businessentity", "U89N", "U89Nk", String.class);


public nuclosstate() {
		super("U89N");
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public UID getEntityUid() {
		return new org.nuclos.common.UID("U89N");
}


/**
 * Getter-Method for attribute: name
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRSTATE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getName() {
		return getField("U89Na", String.class);
}


/**
 * Getter-Method for attribute: model
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRUID_T_MD_STATEMODEL
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_statemodel
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public UID getModelId() {
		return getFieldUid("U89Nb");
}


/**
 * Getter-Method for attribute: description
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRDESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public String getDescription() {
		return getField("U89Nc", String.class);
}


/**
 * Getter-Method for attribute: numeral
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: INTNUMERAL
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 3
 *<br>Precision: null
**/
public Integer getNumeral() {
		return getField("U89Nd", Integer.class);
}


/**
 * Getter-Method for attribute: labelres
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STR_LOCALERESOURCE_L
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getLabelres() {
		return getField("U89Ne", String.class);
}


/**
 * Getter-Method for attribute: descriptionres
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STR_LOCALERESOURCE_D
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getDescriptionres() {
		return getField("U89Nf", String.class);
}


/**
 * Getter-Method for attribute: tab
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRTAB
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getTab() {
		return getField("U89Ng", String.class);
}


/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public org.nuclos.common.UID getPrimaryKey() {
		return getField("U89N0", org.nuclos.common.UID.class); 
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getCreatedAt() {
		return getField("U89N1", Date.class);
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getCreatedBy() {
		return getField("U89N2", String.class);
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getChangedAt() {
		return getField("U89N3", Date.class);
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getChangedBy() {
		return getField("U89N4", String.class);
}


/**
 * Getter-Method for attribute: icon
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: OBJICON
 *<br>Data type: org.nuclos.common.NuclosImage
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.api.NuclosImage getIcon() {
		return getField("U89Nh", org.nuclos.api.NuclosImage.class); 
}


/**
 * Getter-Method for attribute: buttonIcon
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRUID_BUTTONICON
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_resource
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public UID getButtonIconId() {
		return getFieldUid("U89Ni");
}


/**
 * Getter-Method for attribute: color
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRCOLOR
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 10
 *<br>Precision: null
**/
public String getColor() {
		return getField("U89Nj", String.class);
}


/**
 * Getter-Method for attribute: buttonRes
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STR_LOCALERESOURCE_BUT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getButtonRes() {
		return getField("U89Nk", String.class);
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save() throws org.nuclos.api.exception.BusinessException {
		super.save();
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(UID id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("U89N"), id);
}
/**
* Static Get by Id
*/
public static nuclosstate get(UID id) {
		return get(nuclosstate.class, id);
}
 }
