//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.businessentity.rule;

import java.util.List;

import org.jsoup.Jsoup;
import org.nuclos.api.annotation.Rule;
import org.nuclos.api.businessobject.Query;
import org.nuclos.api.context.InputContext;
import org.nuclos.api.context.InputRequiredException;
import org.nuclos.api.context.InputSpecification;
import org.nuclos.api.context.InsertContext;
import org.nuclos.api.context.LogContext;
import org.nuclos.api.context.UpdateContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.api.rule.InsertFinalRule;
import org.nuclos.api.rule.InsertRule;
import org.nuclos.api.rule.UpdateFinalRule;
import org.nuclos.api.rule.UpdateRule;
import org.nuclos.businessentity.News;
import org.nuclos.businessentity.rule.annotation.SystemRuleUsage;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.news.NewsService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Rule(
		name = "NewsSaveRule",
		description = "")
@SystemRuleUsage(
		boClass = News.class,
		order = 1
)
@Configurable
@SuppressWarnings("unused") // system internal usage
public class NewsSaveRule implements InsertRule, InsertFinalRule, UpdateRule, UpdateFinalRule {

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(NewsSaveRule.class);

	@Autowired
	private NewsService newsService;

	@Override
	public void insert(final InsertContext context) throws BusinessException {
		News news = context.getBusinessObject(News.class);

		news.setRevision(findNextRevision(news.getName()));

		News predecessor = findPredecessor(news);
		if (predecessor != null) {
			news.setPredecessorId(predecessor.getId());
		}

		this.save(news, context);
	}

	@Override
	public void update(final UpdateContext context) throws BusinessException {
		News news = context.getBusinessObject(News.class);
		News oldNews = News.get(news.getId());

		this.save(news, context);

		checkContentChanges(news, oldNews);
	}

	private void checkContentChanges(final News news, final News oldNews) throws BusinessException {
		String newText = Jsoup.parse(news.getContent()).text();
		String oldText = Jsoup.parse(oldNews.getContent()).text();

		if (!StringUtils.equals(oldText, newText)) {
			if (confirm("The content of this news has been modified - Should all user confirmations/views be deleted for this news?")) {
				BusinessObjectProvider.deleteAll(news.getNewsConfirmed());
				BusinessObjectProvider.deleteAll(news.getNewsViewed());
				newsService.deletePrivacyConsents();
			}
		}
	}

	private void save(final News news, LogContext log) throws BusinessException {
		if (Boolean.TRUE.equals(news.getConfirmationRequired())) {
			news.setShowAtStartup(true);
		}
	}

	private News findPredecessor(final News news) {
		final News result;

		Query<News> query = QueryProvider.create(News.class);
		query.where(News.Name.eq(news.getName()))
				.and(News.Revision.Lt(news.getRevision()))
				.orderBy(News.Revision, false);
		List<News> list = QueryProvider.execute(query);

		if (list.isEmpty()) {
			result = null;
		} else {
			result = list.get(0);
		}

		return result;
	}

	private int findNextRevision(final String newsName) {
		int result = 0;

		Query<News> query = QueryProvider.create(News.class);
		query.where(News.Name.eq(newsName)).orderBy(News.Revision, false);
		List<News> list = QueryProvider.execute(query);

		if (!list.isEmpty()) {
			result = list.get(0).getRevision() + 1;
		}

		return result;
	}

	private boolean confirm(String message) throws BusinessException {
		if (InputContext.isSupported()) {
			Integer result = (Integer) InputContext.get(message);
			if (result == null) {
				InputSpecification spec = new InputSpecification(InputSpecification.CONFIRM_YES_NO, message, message);
				throw new InputRequiredException(spec);
			} else {
				return result == InputSpecification.YES;
			}
		} else {
			throw new BusinessException("InputContext not supported.");
		}
	}

	@Override
	public void insertFinal(final InsertContext context) throws BusinessException {
		newsService.updatePrivacyConsent();
	}

	@Override
	public void updateFinal(final UpdateContext context) throws BusinessException {
		newsService.updatePrivacyConsent();
	}
}
