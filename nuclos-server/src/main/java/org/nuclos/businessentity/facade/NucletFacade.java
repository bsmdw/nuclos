//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.businessentity.facade;

import static org.nuclos.server.common.NuclosSystemParameters.getClientExtensionPaths;
import static org.nuclos.server.web.NucletExtensionLoader.EXTENSION_FILE_IDENTIFIER;
import static org.nuclos.server.web.NucletExtensionLoader.getNucletExtensionPathEntries;
import static org.nuclos.server.web.NucletExtensionLoader.getNucletExtensionsHomePath;
import static org.nuclos.server.web.NucletExtensionLoader.getNucletExtensionsServerHomePath;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.nuclos.api.businessobject.Query;
import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.businessentity.NucletExtension;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.Priority;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.RuleNotification;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.nuclos.server.jnlp.JnlpServlet;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class NucletFacade {

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(NucletFacade.class);

	private enum ExtensionType {
		CLIENT("client"), SERVER("server");
		private final String name;
		ExtensionType(String name) {
			this.name = name;
		}
		public String toString() {
			return this.name;
		}
	}

	@Autowired
	private SpringDataBaseHelper dbHelper;

	@Autowired
	private WebAddonFacade webAddonFacade;

	@PostConstruct
	final void init() {
		SpringApplicationContextHolder.addSpringReadyListener(new SpringApplicationContextHolder.SpringReadyListener() {
			@Override
			public void springIsReady() {
				Thread t = new Thread(new Runnable() {
					@Override
					public void run() {
						// wait for webAddon facade to complete...
						while (webAddonFacade.isOutputAndCompileAllRunning()) {
							try {
								Thread.sleep(2000);
							} catch (InterruptedException ex) {
								LOG.error("Unexpected Thread ERROR: {}", ex, ex);
							}
						}
						updateExtensionsIfNecessary();
					}
				}, "Nuclet Extension Updater");
				t.start();
			}
			@Override
			public int getMinReadyState() {
				return 3;
			}
		});
	}

	@Transactional
	private List<NucletExtension> getAllNucletExtensions() {
		List<NucletExtension> result = new ArrayList<>();
		// load all files transactional...
		Query<NucletExtension> q = QueryProvider.create(NucletExtension.class);
		q.where(NucletExtension.DependencyAddAsProjectToPom.eq(Boolean.FALSE));
		for (NucletExtension nExt : QueryProvider.execute(q)) {
			try {
				nExt.getFile();
				result.add(nExt);
			} catch (Exception ex) {
				LOG.error("Unexpected IO ERROR: {} on {}.{}", ex, NucletExtension.class.getCanonicalName(), nExt.getId(), ex);
			}
		}
		return result;
	}

	public void updateExtensionsAfterNucletImport() {
		updateExtensionsIfNecessary(getAllNucletExtensions(), true);
	}

	public void updateExtensionsIfNecessary() {
		updateExtensionsIfNecessary(getAllNucletExtensions(), false);
	}

	private void updateExtensionsIfNecessary(List<NucletExtension> nucletExtensions, boolean bAfterNucletImport) {
		List<NucletExtension> clientExtensions = new ArrayList<>();
		List<NucletExtension> serverExtensions = new ArrayList<>();
		splitIntoClientAndServerExtensions(nucletExtensions, clientExtensions, serverExtensions);

		// update Client extensions if necessary
		if (!isExtensionsUpToDate(ExtensionType.CLIENT, clientExtensions)) {
			final String clientHashValues = getExtensionHashValuesFromHome(ExtensionType.CLIENT);
			for (Path clientExtensionPath : getClientExtensionPaths()) {
				updateExtensionDirectory(clientExtensionPath, clientHashValues, clientExtensions);
			}
			writeExtensionHashListFile(ExtensionType.CLIENT, clientExtensions);

			// update client extensions caches
			JnlpServlet.invalidateCaches();
		}

		// update Server extensions if necessary
		if (!isExtensionsUpToDate(ExtensionType.SERVER, serverExtensions)) {
			final String serverHashValues = getExtensionHashValuesFromHome(ExtensionType.SERVER);
			final Path homeExtensionsServer = getNucletExtensionsServerHomePath();
			if (homeExtensionsServer == null) {
				throw new NuclosFatalException("Directory for Nuclet extensions could not be determined: [NUCLOS_HOME]/nuclet-extensions/server!\nCheck your server.properies (nuclos.home)");
			}
			if (!homeExtensionsServer.toFile().exists()) {
				homeExtensionsServer.toFile().mkdirs();
			}
			updateExtensionDirectory(homeExtensionsServer, serverHashValues, serverExtensions);
			writeExtensionHashListFile(ExtensionType.SERVER, serverExtensions);
			SpringApplicationContextHolder.resetSpringReadyState(); // for server status update (not ready!)
			Thread t = new Thread("NucletServerExtensionChangedInfoThread") {
				@Override
				public void run() {
					while (true) {
						LOG.info("\n!!!!!!!! --------------------------------------------------- !!!!!!!!" +
								 "\n!!!!!!!! Nuclet server extension changed, please RESTART ... !!!!!!!!" +
								 "\n!!!!!!!! --------------------------------------------------- !!!!!!!!");
						try {
							Thread.sleep(10000);
						} catch (InterruptedException e) {
							LOG.error(e.getMessage(), e);
						}
					}
				}
			};
			t.start();
			if (!bAfterNucletImport) {
				// No JMS Message after nuclet import.
				// Tomcat throws java.io.FileNotFoundException when JAR file is removed and transaction synchronization in use...
				try {
					NuclosJMSUtils.sendObjectMessageAfterCommit(new RuleNotification(Priority.HIGH, "Nuclet server extension changed, please RESTART the server ...", "Nuclos Server"),
							JMSConstants.TOPICNAME_RULENOTIFICATION, SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
				} catch (Exception ex) {
					// Ignore: Not logged in during server start etc.
				}
			}
		}
	}

	public boolean isServerExtensionsUpToDate() {
		final Query<NucletExtension> q = QueryProvider.create(NucletExtension.class);
		q.where(NucletExtension.ServerExtension.eq(true));
		List<NucletExtension> serverExtensions = QueryProvider.execute(q);

		return isExtensionsUpToDate(ExtensionType.SERVER, serverExtensions);
	}

	private boolean isExtensionsUpToDate(ExtensionType extType, List<NucletExtension> nucletExtensions) {
		switch (extType) {
			case CLIENT:
				return getExtensionHashValuesFromNucletExtensions(nucletExtensions).equals(getExtensionHashValuesFromHome(ExtensionType.CLIENT));
			case SERVER:
				return getExtensionHashValuesFromNucletExtensions(nucletExtensions).equals(getExtensionHashValuesFromHome(ExtensionType.SERVER));
			default:
				return false;
		}
	}

	private void splitIntoClientAndServerExtensions(List<NucletExtension> nucletExtensions, List<NucletExtension> clientExtensions, List<NucletExtension> serverExtensions) {
		for (NucletExtension nExt : nucletExtensions) {
			if (Boolean.TRUE.equals(nExt.getClientExtension())) {
				clientExtensions.add(nExt);
			}
			if (Boolean.TRUE.equals(nExt.getServerExtension())) {
				serverExtensions.add(nExt);
			}
		}
	}

	private void updateExtensionDirectory(Path extensionPath, String oldHashListFileContent, List<NucletExtension> newNucletExtension) {
		try {
			final Map<UID, String> oldFileHashMap = new HashMap<>();
			final Map<UID, String> newFileHashMap = new HashMap<>();
			for (NucletExtension nExt : newNucletExtension) {
				newFileHashMap.put(nExt.getId(), nExt.getHash());
			}
			for (String line : oldHashListFileContent.split("\\n")) {
				String[] fileUidAndHashValue = line.split(":");
				if (fileUidAndHashValue.length == 2) {
					oldFileHashMap.put(UID.parseUID(fileUidAndHashValue[0]), fileUidAndHashValue[1]);
				} else {
					if (!RigidUtils.looksEmpty(line)) {
						LOG.warn("Unexpected line {} in hash list file for path {} ", line, extensionPath);
					}
				}
			}
			final List<Path> files = getNucletExtensionPathEntries(extensionPath);
			for (Path pfile : files) {
				final String fileName = pfile.toFile().getName();
				boolean bDeleteFile = true;
				for (UID extUID : newFileHashMap.keySet()) {
					if (fileName.contains(extUID.getString())) {
						final String oldHashValue = oldFileHashMap.get(extUID);
						final String newHashValue = newFileHashMap.get(extUID);
						if (RigidUtils.equal(oldHashValue, newHashValue)) {
							bDeleteFile = false;
						}
						break;
					}
				}
				if (bDeleteFile) {
					// Nuclet Extension removed or file hash changed. Remove file from Extension path...
					Files.deleteIfExists(pfile);
				}
			}
			for (NucletExtension nExt : newNucletExtension) {
				final UID extUID = nExt.getId();
				final String oldHashValue = oldFileHashMap.get(extUID);
				final String newHashValue = newFileHashMap.get(extUID);
				if (!RigidUtils.equal(oldHashValue, newHashValue)) {
					final NuclosFile nucFile = nExt.getFile();
					final String fileName = EXTENSION_FILE_IDENTIFIER + "-" +
							extUID.getString() + "-" +
							nucFile.getName().replaceAll(Pattern.quote("[^a-zA-Z0-9_-.]+"), "");
					FileUtils.writeByteArrayToFile(extensionPath.resolve(fileName).toFile(), nucFile.getContent());
				}
			}

		} catch (IOException ex) {
			LOG.error("Unexpected IO ERROR: {} on {}", ex, extensionPath, ex);
		}
	}

	public String getHash(NucletExtension nExt) {
		try {
			final byte[] content = nExt.getFile().getContent();
			final MessageDigest digest = MessageDigest.getInstance("SHA-256");
			digest.update(content);
			final byte[] hash = digest.digest();
			return Base64.getEncoder().encodeToString(hash);
		} catch (NoSuchAlgorithmException e) {
			throw new NuclosFatalException(e);
		}
	}

	private void writeExtensionHashListFile(ExtensionType extType, List<NucletExtension> nucletExtensions) {
		final File hashListFile = getExtensionHashListFileFromHome(extType, true);
		if (hashListFile != null) {
			LOG.info("Writing hash values to {}", hashListFile);
			try {
				Files.deleteIfExists(hashListFile.toPath());
				final String fileContent = getExtensionHashValuesFromNucletExtensions(nucletExtensions);
				FileUtils.writeStringToFile(hashListFile, fileContent);
			} catch (IOException ex) {
				LOG.error("Unexpected IO ERROR: {} on {}", ex, hashListFile, ex);
			}
		}
	}

	private String getExtensionHashValuesFromNucletExtensions(List<NucletExtension> nucletExtensions) {
		final SortedMap<UID, String> sortedFileHashes = new TreeMap<>();
		for (NucletExtension nucletExtension : nucletExtensions) {
			sortedFileHashes.put(nucletExtension.getId(), StringUtils.defaultIfEmpty(nucletExtension.getHash(), "null"));
		}
		final StringBuilder result = new StringBuilder();
		for (UID extUID : sortedFileHashes.keySet()) {
			result.append(extUID.getString() + ":" + sortedFileHashes.get(extUID) + "\n");
		}
		return result.toString();
	}

	private File getExtensionHashListFileFromHome(ExtensionType extType, boolean write) {
		final Path extensionsHome = getNucletExtensionsHomePath();
		if (extensionsHome == null) {
			return null;
		}
		if (!extensionsHome.toFile().exists()) {
			if (write) {
				extensionsHome.toFile().mkdirs();
			} else {
				return null;
			}
		}
		final File hashListFile = extensionsHome.resolve(extType + ".hashlist").toFile();
		return hashListFile;
	}

	private String getExtensionHashValuesFromHome(ExtensionType extType) {
		final File hashListFile = getExtensionHashListFileFromHome(extType, false);
		try {
			if (hashListFile != null && hashListFile.exists()) {
				final byte[] fileBytes = Files.readAllBytes(hashListFile.toPath());
				final String result = new String(fileBytes);
				return result;
			}
		} catch (IOException ex) {
			LOG.error("Unexpected IO ERROR: {} on {}", ex, hashListFile, ex);
		}
		return "";
	}

}
