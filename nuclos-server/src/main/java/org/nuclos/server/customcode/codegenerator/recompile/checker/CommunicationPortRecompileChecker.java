package org.nuclos.server.customcode.codegenerator.recompile.checker;

import org.nuclos.common.E;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.CompositeRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.FieldRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.PKRecompileChecker;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.stereotype.Component;

/**
 * Checks if a modification to a communication port must cause a recompile.
 */
@Component
public class CommunicationPortRecompileChecker extends CompositeRecompileChecker {

    public CommunicationPortRecompileChecker() {
        super(new PKRecompileChecker(),
              new FieldRecompileChecker<>(E.COMMUNICATION_PORT.portName));
    }

    @Override
    public boolean isRecompileRequiredOnInsertOrDelete(final MasterDataVO<?> mdvo) {
        // new or removed ports must force recompile:
        return true;
    }

}
