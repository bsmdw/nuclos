//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.customcode.codegenerator;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

import javax.inject.Inject;

import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.SpringApplicationHelper;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.security.MessageDigest;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.customcode.valueobject.CodeVO;
import org.nuclos.server.eventsupport.ejb3.SourceCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.ApplicationContext;

/**
 * CodeGenerator used for CodeVOs (aka 'new rules' and old library rules).
 *
 * @author Thomas Pasch (javadoc)
 * @see CodeVO
 */
@Configurable
public class PlainCodeGenerator implements CodeGenerator {

	// Spring injection

	@Autowired
	private NuclosJavaCompilerComponent nuclosJavaCompilerComponent;

	// End of Spring injection

	private CodeVO codeVO;

	private JavaSourceAsString src;

	private PlainCodeGenerator() {
	}

	private PlainCodeGenerator init(CodeVO codeVO) {
		this.codeVO = codeVO;
		this.src = new JavaSourceAsString(
				codeVO.getName(),
				codeVO.getSource(),
				getProperties(),
				codeVO.getId()
		);
		return this;
	}

	/**
	 * Use this method for spring injection to work
	 * @param codeVO
	 * @return
	 */
	public static PlainCodeGenerator newInstance(ApplicationContext applicationContext, CodeVO codeVO) {
		return SpringApplicationHelper.newInstanceAutowiredAndInitialized(applicationContext, PlainCodeGenerator.class).init(codeVO);
	}

	@Override
	public boolean isRecompileNecessary() {
		return true;
	}

	@Override
	public Map<String, String> getProperties() {
		Map<String, String> result = new TreeMap<>();

		if (NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_DEVELOPMENT)) {
			result.put("class", PlainCodeGenerator.class.getCanonicalName());
			result.put("type", CodeVO.class.getCanonicalName());
			result.put("entity", codeVO.getEntity().getEntityName());
			result.put("name", codeVO.getName());
			result.put("uid", codeVO.getId() != null ? codeVO.getId().toString() : "");
		}

		return result;
	}

	@Override
	public Path getJavaSrcFile(JavaSourceAsString srcobject) throws IOException {
		return Paths.get(nuclosJavaCompilerComponent.getSourceOutputPath().getCanonicalPath(), srcobject.getPath());
	}

	@Override
	public void writeSource(Writer writer, JavaSourceAsString src) throws IOException {
		writer.write(src.getSource());
		writer.write(src.getPostfix());
	}

	@Override
	public Iterable<? extends JavaSourceAsString> getSourceFiles() {
		return Collections.singletonList(src);
	}

	@Override
	public byte[] postCompile(String name, byte[] bytecode) {
		if (codeVO.isDebug() && !codeVO.getSource().contains("public interface")) {
			return ClassDebugAdapter.weaveDebugInterceptors(bytecode, 0);
		} else {
			return bytecode;
		}
	}

	public static String hashForManifest(ApplicationContext applicationContext, CodeVO codeVO) {
		return PlainCodeGenerator.newInstance(applicationContext, codeVO).hashForManifest();
	}

	@Override
	public String hashForManifest() {
		String sourceFileContent = src.getSource() + src.getPostfix();
		try {
			return hashForManifest(sourceFileContent.getBytes(NuclosCodegeneratorConstants.JAVA_SRC_ENCODING));
		} catch (Exception e) {
			throw new NuclosFatalException(e);
		}
	}

	public static String hashForManifest(byte[] content) {
		try {
			return MessageDigest.digestAsBase64(SourceCache.DIGEST, content);
		} catch (Exception e) {
			throw new NuclosFatalException(e);
		}
	}

	@Override
	public int hashCode() {
		return src.getName().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof CodeGenerator)) {
			return false;
		}
		final CodeGenerator other = (CodeGenerator) obj;
		final JavaSourceAsString firstOtherSrc;
		if (!other.isRecompileNecessary()) {
			return false;
		}
		firstOtherSrc = other.getSourceFiles().iterator().next();
		return LangUtils.equal(firstOtherSrc.getEntityUid(), src.getEntityUid());
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder("PlainCG[code=");
		result.append(codeVO.getName());
		result.append("]");
		return result.toString();
	}

}
