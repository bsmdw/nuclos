package org.nuclos.server.customcode.codegenerator.recompile.checker;

import org.nuclos.common.E;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.CompositeRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.PKRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.UIDFieldRecompileChecker;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.stereotype.Component;

/**
 * Checks if the impl field require a recompile.
 */
@Component
public class GenericFieldMappingRecompileChecker extends CompositeRecompileChecker {

    public GenericFieldMappingRecompileChecker() {
        super(new PKRecompileChecker(),
              new UIDFieldRecompileChecker(E.ENTITY_GENERIC_FIELDMAPPING.genericField),
              new UIDFieldRecompileChecker(E.ENTITY_GENERIC_FIELDMAPPING.implementingField));
    }

    @Override
    public boolean isRecompileRequiredOnInsertOrDelete(final MasterDataVO<?> mdvo) {
        // new or deleted force a recompile
        return true;
    }

}
