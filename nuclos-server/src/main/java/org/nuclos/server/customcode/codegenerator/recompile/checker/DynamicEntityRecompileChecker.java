//Copyright (C) 2018  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.customcode.codegenerator.recompile.checker;

import java.util.ArrayList;

import org.nuclos.common.E;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.CompositeRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.DatasourceMetaRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.FieldRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.PKRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.UIDFieldRecompileChecker;
import org.nuclos.server.datasource.DatasourceMetaParser;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Checks if a modification to a dynamic datasource must cause a recompile.
 */
@Component
public class DynamicEntityRecompileChecker extends CompositeRecompileChecker implements InitializingBean {

	@Autowired
	private DatasourceMetaParser dsMetaParser;

    public DynamicEntityRecompileChecker() {
        super(new ArrayList<>());
    }

	@Override
	public void afterPropertiesSet() throws Exception {
		super.addRecompileChecker(new PKRecompileChecker());
		super.addRecompileChecker(new FieldRecompileChecker<>(E.DYNAMICENTITY.name));
		super.addRecompileChecker(new UIDFieldRecompileChecker(E.DYNAMICENTITY.nuclet));
		super.addRecompileChecker(new FieldRecompileChecker<>(E.DYNAMICENTITY.description));
		super.addRecompileChecker(new DatasourceMetaRecompileChecker(dsMetaParser, E.DYNAMICENTITY.meta));
	}

}
