package org.nuclos.server.customcode.codegenerator.recompile.checker;

import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Checks if the generated code must be recompiled.
 * <p/>
 * Also load some required dependent entities of some entity to check.
 */
public interface IRecompileChecker {

    /**
     * Loads the dependents of the given entity that are required to perform the check.
     * @param mdvo The mdvo where the dependents are added.
     */
    void loadRequiredDependentsOf(MasterDataVO<?> mdvo);

    /**
     * Checks if the generated code must be recompiled if the entity is modified.
     *
     * @param oldMdvo     The old MDVO (before the update). Must not be {@code null}.
     * @param updatedMdvo the updated MDVO. Must not be {@code null}.
     * @return Returns true if the code must be recompiled.
     */
    boolean isRecompileRequiredOnUpdate(MasterDataVO<?> oldMdvo, MasterDataVO<?> updatedMdvo);

    /**
     * Checks if the generated code must be recompiled if some entity is inserted or deleted.
     *
     * @return Returns true if the code must be recompiled.
     */
    boolean isRecompileRequiredOnInsertOrDelete(MasterDataVO<?> mdvo);



}
