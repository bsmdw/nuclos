//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.customcode;

import static org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.BOJARFILE;
import static org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.CCCEJARFILE;
import static org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.COMMUNICATIONJARFILE;
import static org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.DATASOURCEREPORTJARFILE;
import static org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.GENERATIONJARFILE;
import static org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.IMPORTSTRUCTUREDEFSJARFILE;
import static org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.JARFILE;
import static org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.PARAMETERJARFILE;
import static org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.PRINTOUTJARFILE;
import static org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.REPORTJARFILE;
import static org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.STATEMODELJARFILE;
import static org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.USERROLEJARFILE;
import static org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.WEBSERVICEJARFILE;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicReference;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.servlet.ServletContext;

import org.nuclos.api.annotation.Function;
import org.nuclos.api.rule.CustomRule;
import org.nuclos.api.rule.DeleteFinalRule;
import org.nuclos.api.rule.DeleteRule;
import org.nuclos.api.rule.GenerateFinalRule;
import org.nuclos.api.rule.GenerateRule;
import org.nuclos.api.rule.InsertFinalRule;
import org.nuclos.api.rule.InsertRule;
import org.nuclos.api.rule.JobRule;
import org.nuclos.api.rule.StateChangeFinalRule;
import org.nuclos.api.rule.StateChangeRule;
import org.nuclos.api.rule.UpdateFinalRule;
import org.nuclos.api.rule.UpdateRule;
import org.nuclos.common.E;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.customcode.codegenerator.GeneratorClasspathComponent;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorUtils;
import org.nuclos.server.customcode.codegenerator.NuclosJarGeneratorManager;
import org.nuclos.server.customcode.codegenerator.NuclosJarGeneratorThread;
import org.nuclos.server.customcode.codegenerator.NuclosJavaCompilerComponent;
import org.nuclos.server.customcode.codegenerator.RuleClassLoader;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.AopInfrastructureBean;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

/**
 * Provides the classloader for dynamically loaded code (Rules, Wsdl).
 */
@Component
public class CustomCodeManager implements ApplicationContextAware, MessageListener {

	private static final Logger LOG = LoggerFactory.getLogger(CustomCodeManager.class);
	
	private static Set<String> ALLOWED_PACKAGE_STARTS;
	
	private static Set<String> DUBIOUS_PACKAGE_STARTS;
	
	static {
		Set<String> set = new HashSet<>();
		set.add("org.nuclet.");
		ALLOWED_PACKAGE_STARTS = Collections.unmodifiableSet(set);
		
		set = new HashSet<>();
		set.add("org");
		set.add("java");
		set.add("javax");
		DUBIOUS_PACKAGE_STARTS = Collections.unmodifiableSet(set);
	}

	// Spring injection

	@Autowired
	private NuclosJavaCompilerComponent nuclosJavaCompilerComponent;

	private ApplicationContext parent;

	@Autowired
	private ServletContext servletContext;
	
	/**
	 * This is a dependency-only injection used to ensure that CCCE.jar exists. (tp)
	 */
	@Autowired
	private GeneratorClasspathComponent generatorClasspathComponent;

	@Autowired
	private NuclosJarGeneratorManager generatorManager;

	// End of Spring injection
	
	private final ConcurrentMap<String, EventSupportSourceVO> executableEventSupportFiles = new ConcurrentHashMap<String, EventSupportSourceVO>();

	private final AtomicReference<RuleClassLoader> cl = new AtomicReference<RuleClassLoader>();

	private final AtomicReference<AnnotationConfigApplicationContext> context = new AtomicReference<AnnotationConfigApplicationContext>();

	private ConcurrentMap<String, BeanFunction> functions = new ConcurrentHashMap<String, BeanFunction>();

	private final Class<?>[] registeredEvenTypes = new Class[] {
			CustomRule.class, 
			DeleteFinalRule.class, DeleteRule.class,
			GenerateFinalRule.class, GenerateRule.class, 
			InsertFinalRule.class, InsertRule.class, 
			StateChangeFinalRule.class, StateChangeRule.class,
			UpdateFinalRule.class, UpdateRule.class,
			JobRule.class};
	
	CustomCodeManager() {
		
	}

	public NuclosJavaCompilerComponent getNuclosJavaCompilerComponent() {
		return this.nuclosJavaCompilerComponent;
	}
	
	public <T> T getInstance(String sClazz) throws NuclosCompileException {
		try {
			/*
			if (nuclosJavaCompilerComponent.validate()) {
				cl.set(null);
			}
			 */
			return _getInstance(sClazz);
		}
		catch(InstantiationException e) {
			throw new NuclosCompileException(e);
		}
		catch(IllegalAccessException e) {
			throw new NuclosCompileException(e);
		}
		catch(ClassNotFoundException e) {
			// throw new NuclosCompileException(e);
			
			// HACK for NUCLOS-2040:
			try {
				LOG.warn("retrying getInstance({}) because of {}", sClazz, e);
				nuclosJavaCompilerComponent.forceCompile();
				return _getInstance(sClazz);
			}
			catch (InstantiationException e1) {
				throw new NuclosCompileException(e);
			}
			catch (IllegalAccessException e1) {
				throw new NuclosCompileException(e);
			}
			catch (ClassNotFoundException e1) {
				throw new NuclosCompileException(e);
			}				
		}
	}
	
	private <T> T _getInstance(String sClazz) 
			throws NuclosCompileException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		
		return (T) getClassLoaderAndCompileIfNeeded().loadClass(sClazz).newInstance();
	}

	@Override
	public void onMessage(Message message) {
		if(message instanceof TextMessage) {
			try {
				String text = ((TextMessage) message).getText();
				if (StringUtils.isNullOrEmpty(text) || text.equals(E.NUCLET.getEntityName())) {
					LOG.info("Reload nuclet classloader and application context.");
					cl.set(null);
				}
			}
			catch(JMSException e) {
				LOG.error("{}.onMessage() failed: ", getClass().getName(), e);
			}
		}
	}

	/**
	 * Obtain an instance of the classloader for a given rule artifact.
	 *
	 * @return classloader or <code>null</code> if there are compile problems.
	 * @throws NuclosCompileException
	 */
	public ClassLoader getClassLoaderAndCompileIfNeeded() throws NuclosCompileException {
		return getClassLoaderAndCompileIfNeeded(true);
	}
	
	public ClassLoader getClassLoaderAndCompileIfNeeded(boolean saveSrc) throws NuclosCompileException {
		if (cl.get() == null) {
			_compileIfNoClassloader(saveSrc);
		} else if (nuclosJavaCompilerComponent.validate(saveSrc)) {
			cl.set(null);
			_compileIfNoClassloader(saveSrc);
		}
		// Be sure to really return the CURRENT classloader
		return cl.get();
	}
	
	/**
	 * Compile rule, make a new spring context and reinitialize <code>functions</code>.
	 */
	private void _compileIfNoClassloader(boolean saveSrc) throws NuclosCompileException {
		if (cl.get() != null) {
			return;
		}
		LOG.info("Rule classloader is not set, init a new one...");
		final RuleClassLoader localCl = new RuleClassLoader(Thread.currentThread().getContextClassLoader());
		try {
			if (!CCCEJARFILE.exists()) {
				throw new NuclosFatalException("Code-Compiler-Classpath-Extension removed! Restart Nuclos Server to generate a new one...\n("
							+ CCCEJARFILE.getAbsolutePath() + ")");
			}

			if (!(Thread.currentThread() instanceof NuclosJarGeneratorThread)) {
				// not the generator thread. Check if one generator thread is running and wait for it...
				generatorManager.isGeneratorRunningAndWaitFor();
			}

			// ATTENTION: if this is done before too early the resource can't be found. (tp)
			if (STATEMODELJARFILE.exists()) {
				localCl.addURL(STATEMODELJARFILE.toURI().toURL());
			}
			if (BOJARFILE.exists()) {
				localCl.addURL(BOJARFILE.toURI().toURL());
			}
			if (DATASOURCEREPORTJARFILE.exists()) {
				localCl.addURL(DATASOURCEREPORTJARFILE.toURI().toURL());
			}
			if (PARAMETERJARFILE.exists()) {
				localCl.addURL(PARAMETERJARFILE.toURI().toURL());
			}
			if (COMMUNICATIONJARFILE.exists()) {
				localCl.addURL(COMMUNICATIONJARFILE.toURI().toURL());
			}
			if (IMPORTSTRUCTUREDEFSJARFILE.exists()) {
				localCl.addURL(IMPORTSTRUCTUREDEFSJARFILE.toURI().toURL());
			}
			if (GENERATIONJARFILE.exists()) {
				localCl.addURL(GENERATIONJARFILE.toURI().toURL());
			}
			if (REPORTJARFILE.exists()) {
				localCl.addURL(REPORTJARFILE.toURI().toURL());
			}
			if (WEBSERVICEJARFILE.exists()) {
				localCl.addURL(WEBSERVICEJARFILE.toURI().toURL());
			}
			if (USERROLEJARFILE.exists()) {
				localCl.addURL(USERROLEJARFILE.toURI().toURL());
			}
			if (PRINTOUTJARFILE.exists()) {
				localCl.addURL(PRINTOUTJARFILE.toURI().toURL());
			}
			localCl.addJarsToClassPath(NuclosSystemParameters.getDirectory(NuclosSystemParameters.WSDL_GENERATOR_LIB_PATH));

			// see http://static.springsource.org/spring/docs/3.0.x/spring-framework-reference/html/beans.html#beans-java-instantiating-container
			final AnnotationConfigApplicationContext c = new AnnotationConfigApplicationContext();
			c.setParent(parent);
			c.setClassLoader(localCl);
			c.getBeanFactory().addBeanPostProcessor(new BeanFunctionPostProcessor());

			// add all nuclet packages to scan:
			Set<String> packages = new HashSet<>();
			for (Object nuclet : NucletDalProvider.getInstance().getEntityObjectProcessor(E.NUCLET).getAll()) {
				String p = ((EntityObjectVO<UID>)nuclet).getFieldValue(E.NUCLET.packagefield);
				if (!StringUtils.isNullOrEmpty(p)
						// NUCLOS-7842
					&& !p.startsWith("org.nuclos")
					&& !p.equals("org")) {
					packages.add(p);
				}
			}
			if (packages.size() > 0) {
				c.scan(packages.toArray(new String[packages.size()]));

				PACKAGES:
				for (String pkg: packages) {
					if (pkg == null) {
						continue;
					}
					if (DUBIOUS_PACKAGE_STARTS.contains(pkg)) {
						LOG.error("Scanning for groovy-callable @Function in complete {} is not allowed, "
								  + "see http://support.nuclos.de/browse/NUCLOS-3155", pkg);
					} else {
						for (String allowed: ALLOWED_PACKAGE_STARTS) {
							if (pkg.startsWith(allowed)) {
								continue PACKAGES;
							}
						}
						for (String illegal: DUBIOUS_PACKAGE_STARTS) {
							if (pkg.startsWith(illegal + ".")) {
								LOG.warn("Scanning for groovy-callable @Function in {}"
										+ " is discouraged because it is a sub package of {}"
										+ ", see http://support.nuclos.de/browse/NUCLOS-3155",
										 pkg, illegal);
							}
						}
					}
				}
			}
			// Ensure that Nuclet.jar exists before looking for executableEventSupportFiles
			nuclosJavaCompilerComponent.validate(saveSrc);
			// only after it exists, add the JAR to classloader
			// ATTENTION: if this is done before validate the resource can't be found. (tp)
			localCl.addURL(JARFILE.toURI().toURL());
			// Evil. ! we have to to a scan here. otherwise ruleclasses from nuclet.jar are not processed by BeanFunctionPostProcessor.
			// @see NUCLOS-2082
			if (packages.size() > 0) {
				LOG.info("_compileIfNoClassloader: scan for @Function annotations "
						 + "in packages: {}", packages);
				c.scan(packages.toArray(new String[packages.size()]));
			}
			cl.set(localCl);

			functions.clear();
			c.refresh();
			c.start();
			context.set(c);
			LOG.debug("_compileIfNoClassloader: End");
		}
		catch (IOException ex) {
			throw new NuclosFatalException(ex);
		}
	}

	/**
	 * Return the current classloader or <code>null</code>.
	 */
	public ClassLoader getCurrentClassLoader() {
		return cl.get();
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.parent = applicationContext;
	}

	public void deleteEventSupportSource(String essName) {
		if (essName != null) {
			/*
			File directory = NuclosSystemParameters.getDirectory(NuclosSystemParameters.GENERATOR_OUTPUT_PATH);
			File f = new File(directory, "/src/" + essName.replace(".", "/") + ".java");
			 */
			File f = NuclosCodegeneratorUtils.eventSupportSource(essName);
			if (f.exists()) {
				final boolean success = f.delete();
				if (!success) {
					LOG.warn("Unable to delete {}", f);
				}
				else {
					File jar = JARFILE;
					if(jar.exists()) {
						jar.delete();
					}
				}
			}
		}
	}
	
	public Object invokeFunction(String functionname, Object[] args) {
		try {
			getClassLoaderAndCompileIfNeeded();
		}
		catch (NuclosCompileException e) {
			throw new NuclosFatalException(e);
		}

		BeanFunction bf = functions.get(functionname);
		if (bf != null) {
			try {
				return bf.getMethod().invoke(bf.getBean(), args);
			}
			catch (IllegalArgumentException e) {
				LOG.warn("Function invoked with illegal arguments.", e);
				throw new NuclosFatalException(e);
			}
			catch (IllegalAccessException e) {
				LOG.warn("Function invoked with illegal access.", e);
				throw new NuclosFatalException(e);
			}
			catch (InvocationTargetException e) {
				LOG.warn("Invoked function threw an exception:", e.getTargetException());
				throw new NuclosFatalException(e.getTargetException());
			}
		}
		else {
			throw new NuclosFatalException("Unknown function:" + functionname);
		}
	}

	
	private class BeanFunctionPostProcessor implements BeanPostProcessor {

		@Override
		public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
			return bean;
		}

		@Override
		public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
			if (bean instanceof AopInfrastructureBean) {
				// Ignore AOP infrastructure such as scoped proxies.
				return bean;
			}
			Class<?> targetClass = AopUtils.getTargetClass(bean);
			for (Method m : targetClass.getMethods()) {
				if (m.isAnnotationPresent(Function.class)) {
					Function f = m.getAnnotation(Function.class);
					LOG.info("Processing Function {}.{}[name={}]",
					         bean.getClass(), m.getName(), f.value());
					functions.put(f.value(), new BeanFunction(bean, m));
				}
			}
			return bean;
		}

	}

	public static class BeanFunction {

		private final Object bean;
		private final Method method;

		public BeanFunction(Object bean, Method method) {
			super();
			this.bean = bean;
			this.method = method;
		}

		public Object getBean() {
			return bean;
		}

		public Method getMethod() {
			return method;
		}
	}
	
}
