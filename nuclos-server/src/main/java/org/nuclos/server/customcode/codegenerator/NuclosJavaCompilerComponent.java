//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.server.customcode.codegenerator;

import static org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.JARFILE;
import static org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.JARFILE_OLD;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.CodeSource;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.zip.ZipEntry;

import javax.inject.Inject;
import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.nuclos.common.E;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.SpringApplicationHelper;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common2.IOUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.common2.exception.NuclosSimultaneousCompileException;
import org.nuclos.server.customcode.CustomCodeManager;
import org.nuclos.server.customcode.codegenerator.CodeGenerator.JavaSourceAsString;
import org.nuclos.server.customcode.valueobject.CodeVO;
import org.nuclos.server.eventsupport.ejb3.SourceCache;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Role;
import org.springframework.stereotype.Component;

@Component
public class NuclosJavaCompilerComponent implements BeanFactoryAware {

	private static final Logger LOG = LoggerFactory.getLogger(NuclosJavaCompilerComponent.class);

	private static final String JAVAC_CLASSNAME = "com.sun.tools.javac.api.JavacTool";

	private volatile boolean currentlyWritingSources = false;

	private static Attributes.Name NUCLOS_CODE_NUCLET = new Attributes.Name("Nuclos-Code-Nuclet");
	private static Attributes.Name NUCLOS_CODE_HASH = new Attributes.Name("Nuclos-Code-Hash");

	// Cache:
	private Manifest manifest;
	
	
	private volatile boolean forceCompile = false;
	
	private volatile boolean automaticRuleCompilation = true;
	
	// Spring injection

	@Inject
	private ApplicationContext applicationContext;
	
	// BeanFactoryAware
	private BeanFactory beanFactory;

	/**
	 * Not @Autowired because of dependency cycle.
	 */
	// @Autowired
	private MasterDataFacadeLocal _masterDataFacadeLocal;
	
	/**
	 * Not @Autowired because of dependency cycle.
	 */
	// @Autowired
	private NuclosJarGeneratorManager _nuclosJarGeneratorManager;

	@Autowired
	private SourceScannerComponent sourceScannerComponent;
	
	@Autowired
	private SourceCache sourceCache;

	private static final String JAR_LOCK = "jarLock";

	private static final String MOVE_JAR_LOCK = "moveJarLock";
	
	// end of Spring injection

	NuclosJavaCompilerComponent() {

	}

	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		this.beanFactory = beanFactory;
	}
	
	private MasterDataFacadeLocal getMasterDataFacade() {
		if (_masterDataFacadeLocal == null) {
			_masterDataFacadeLocal = beanFactory.getBean(MasterDataFacadeLocal.class);
		}
		return _masterDataFacadeLocal;
	}
	
	private NuclosJarGeneratorManager getNuclosJarGenerationManager() {
		if (_nuclosJarGeneratorManager == null) {
			_nuclosJarGeneratorManager = beanFactory.getBean(NuclosJarGeneratorManager.class);
		}
		return _nuclosJarGeneratorManager;
	}
	
	public static JavaCompiler getJavaCompilerTool() {
		final JavaCompiler result = _getJavaCompilerTool();
		if (result != null && LOG.isDebugEnabled()) {
			final Class<?> clazz = result.getClass();
			final CodeSource cs = clazz.getProtectionDomain().getCodeSource();
			LOG.debug("JavaCompiler implementation is {} from {}", clazz.getName(), cs);
		}
		return result;
	}
	
	private static JavaCompiler _getJavaCompilerTool() {
		JavaCompiler tool = ToolProvider.getSystemJavaCompiler();
		if (tool != null) {
			return tool;
		}
		// No system Java compiler found, try to locate Javac ourself
		// (maybe we found a "bundled" Javac class on our classpath).
		try {
			Class<?> clazz = Class.forName(JAVAC_CLASSNAME);
			try {
				return (JavaCompiler) clazz.newInstance();
			} catch (Exception ex) {
				LOG.error("Unable to create java compiler instance:", ex);
			}
		} catch(ClassNotFoundException e) {
			LOG.warn("getJavaCompilerTool failed: {}", e);
		}
		return null;
	}
	
	public void forceCompile() {
		forceCompile = true;
	}

	/** the output path where generated java and class files are stored */
	public final File getCodeGeneratorOutputPath() {
		File dir = NuclosCodegeneratorConstants.GENERATOR_FOLDER;
		if (!dir.exists()) {
			dir.mkdirs();
		}
		return dir;
	}

	final File getSourceOutputPath() {
		File dir = NuclosCodegeneratorConstants.SOURCE_OUTPUT_PATH;
		if (!dir.exists()) {
			dir.mkdirs();
		}
		return dir;
	}

	final File getWsdlOutputPath() {
		File dir = NuclosCodegeneratorConstants.WSDL_OUTPUT_PATH;
		if (!dir.exists()) {
			dir.mkdirs();
		}
		return dir;
	}

	public final File getBuildOutputPath() {
		File dir = NuclosCodegeneratorConstants.BUILD_OUTPUT_PATH;
		if (!dir.exists()) {
			dir.mkdirs();
			// For MS Windows ???
			// Files.setAttribute(dir.toPath(), "dos:hidden", true, LinkOption.NOFOLLOW_LINKS);
		}
		return dir;
	}

	private boolean moveJarToOld() {
		getNuclosJarGenerationManager().isGeneratorRunningAndWaitFor();
		boolean oldExists = false;
		synchronized (MOVE_JAR_LOCK) {
			if (JARFILE.exists()) {
				JARFILE_OLD.delete();
				oldExists = JARFILE.renameTo(JARFILE_OLD);
				if (JARFILE.exists()) {
					try {
						IOUtils.copyFile(JARFILE, JARFILE_OLD);
						oldExists = true;
					}
					catch (IOException ex) {
						throw new IllegalStateException(ex);
					}
				}
			}
		}
		return oldExists;
	}

	private void jarThreadSafe(Map<String, byte[]> javacresult, List<CodeGenerator> generators) {
		if ((Thread.currentThread() instanceof NuclosJarGeneratorThread)) {
			jar(javacresult, generators);
		} else {
			// not the generator thread. Check if one generator thread is running and wait for it...
			if (getNuclosJarGenerationManager().isGeneratorRunningAndWaitFor()) {
				// generator finished. nothing more to do
			}
			synchronized (JAR_LOCK) {
				jar(javacresult, generators);
			}
		}
	}

	private void jar(Map<String, byte[]> javacresult, List<CodeGenerator> generators) {
		try {
			final boolean oldExists = moveJarToOld();
			if (javacresult.size() > 0) {
				final Set<String> entries = new HashSet<String>();
				final JarOutputStream jos = new JarOutputStream(
						new BufferedOutputStream(new FileOutputStream(JARFILE)), getManifest());

				try {
					for(final String key : javacresult.keySet()) {
						entries.add(key);
						byte[] bytecode = javacresult.get(key);

						// create entry for directory (required for classpath scanning)
						if (key.contains("/")) {
							String dir = key.substring(0, key.lastIndexOf('/') + 1);
							if (!entries.contains(dir)) {
								entries.add(dir);
								jos.putNextEntry(new JarEntry(dir));
								jos.closeEntry();
							}
						}

						// call postCompile() (weaving) on compiled sources
						for (CodeGenerator generator : generators) {
							if (!oldExists || generator.isRecompileNecessary()) {
								for(JavaSourceAsString src : generator.getSourceFiles()) {
									final String name = src.getFQName();
									if (key.startsWith(name.replaceAll("\\.", "/") + ".class")) {
										LOG.debug("postCompile (weaving) {}", key);
										try {
											bytecode = generator.postCompile(key, bytecode);
										} catch (LinkageError e) {
											LOG.warn("Can't post compile {}:{}", key, e, e);
										}
										// Can we break here???
										// break outer;
									}
								}
							}
						}
						jos.putNextEntry(new ZipEntry(key));
						LOG.debug("writing to {} to jar {}", key, JARFILE);
						jos.write(bytecode);
						jos.closeEntry();
					}

					if (oldExists) {
						final JarInputStream in = new JarInputStream(
								new BufferedInputStream(new FileInputStream(JARFILE_OLD)));
		                final byte[] buffer = new byte[2048];
						try {
			                int size;
							JarEntry entry;
							while ((entry = in.getNextJarEntry()) != null) {
								if (!entries.contains(entry.getName())) {
									jos.putNextEntry(entry);
									LOG.debug("copying {} from old jar {}",
									          entry.getName(), JARFILE_OLD);
									while ((size = in.read(buffer, 0, buffer.length)) != -1) {
										jos.write(buffer, 0, size);
									}
									jos.closeEntry();
								}
								in.closeEntry();
							}
						}
						finally {
							in.close();
						}
					}
				}
				finally {
					jos.close();
				}
			}
		}
		catch(IOException ex) {
			throw new NuclosFatalException(ex);
		}
	}

	/**
	 * @deprecated You should normally use {@link CustomCodeManager#getClassLoaderAndCompileIfNeeded()}.
	 */
	public void compile(boolean saveSrc) throws NuclosCompileException {
		if (automaticRuleCompilation) {
			compile(getAllCurrentGenerators(), saveSrc);
		}
	}

	private NuclosJavaCompiler compile(List<CodeGenerator> generators, boolean saveSrc) throws NuclosCompileException {
		final NuclosJavaCompiler c = SpringApplicationHelper.newInstanceAutowiredAndInitialized(applicationContext, NuclosJavaCompiler.class);
		try {
			Map<String, byte[]> generatedClasses = c.javacThreadSafe(generators, saveSrc);
			jarThreadSafe(generatedClasses, generators);
		} catch (NuclosSimultaneousCompileException e) {
			// Simultaneous generator run...  we have already been waiting for, skip jar creation.
			LOG.info("Simultaneous rule.jar compile prevented");
		}
		finally {
			try {
				c.close();
			}
			catch(IOException e) {
				LOG.warn("getFile failed: {}", e, e);
			}
		}
		return c;
	}

	public void check(CodeGenerator modified, boolean remove) throws NuclosCompileException {
		getNuclosJarGenerationManager().isGeneratorRunningAndWaitFor();
		checkSynchronized(modified, remove);
	}

	private synchronized void checkSynchronized(CodeGenerator modified, boolean remove) throws NuclosCompileException {
		final List<CodeGenerator> generators = getAllCurrentGenerators();
		int index = generators.indexOf(modified);
		if (index > -1) {
			if (remove) {
				generators.remove(index);
			}
			else {
				generators.set(index, modified);
			}
		}
		else if (!remove) {
			generators.add(modified);
		}
		
		final NuclosJavaCompiler c;
		if (automaticRuleCompilation) {
			c = check(generators);
		} else {
			c = SpringApplicationHelper.newInstanceAutowiredAndInitialized(applicationContext, NuclosJavaCompiler.class);
		}

		// If check was successful, update source on disk, or when automatic rule compilation is disabled (to fix errors ...)
		try {
			c.saveSrc(modified, remove);
		}
		catch (IOException e) {
			LOG.warn("Update source on disk failed: {}", e, e);
		}
	}

	private NuclosJavaCompiler check(List<CodeGenerator> generators) throws NuclosCompileException {
		final NuclosJavaCompiler c;
		try {
			checkBusinessObjects();
			if (JARFILE.exists()) {
				c = SpringApplicationHelper.newInstanceAutowiredAndInitialized(applicationContext, NuclosJavaCompiler.class);
				try {
					c.javacThreadSafe(generators, false);
				}
				finally {
					try {
						c.close();
					}
					catch(IOException e) {
						LOG.warn("check failed: {}", e, e);
					}
				}
			}
			else {
				c = compile(generators, false);
			}
		} catch (NuclosCompileException e1) {
			throw new NuclosCompileException(e1.getErrorMessages());
		} catch (Exception e2) {
			throw new NuclosCompileException(e2);
		}
		return c;
	}
	
	private void checkBusinessObjects() throws CommonFatalException,NuclosCompileException,CommonBusinessException {
		
		boolean rebuildBOs = false;

		if ( (!NuclosCodegeneratorConstants.BOJARFILE.exists() && getMasterDataFacade().getMasterDataIds(E.ENTITY.getUID()).size() > 0) || 
				 (!NuclosCodegeneratorConstants.GENERATIONJARFILE.exists() && getMasterDataFacade().getMasterDataIds(E.GENERATION.getUID()).size() > 0) || 
				 (!NuclosCodegeneratorConstants.PARAMETERJARFILE.exists() && getMasterDataFacade().getMasterDataIds(E.PARAMETER.getUID()).size() > 0) ||
				 (!NuclosCodegeneratorConstants.COMMUNICATIONJARFILE.exists() && getMasterDataFacade().getMasterDataIds(E.COMMUNICATION_PORT.getUID()).size() > 0) ||
				 (!NuclosCodegeneratorConstants.IMPORTSTRUCTUREDEFSJARFILE.exists() && getMasterDataFacade().getMasterDataIds(E.IMPORT.getUID()).size() > 0) ||
				 (!NuclosCodegeneratorConstants.DATASOURCEREPORTJARFILE.exists() && getMasterDataFacade().getMasterDataIds(E.DATASOURCE.getUID()).size() > 0) ||
				 (!NuclosCodegeneratorConstants.WEBSERVICEJARFILE.exists() && getMasterDataFacade().getMasterDataIds(E.WEBSERVICE.getUID()).size() > 0) ||
				 (!NuclosCodegeneratorConstants.REPORTJARFILE.exists() && getMasterDataFacade().getMasterDataIdsNoCheck(E.REPORT.getUID(),
						 new CollectableSearchExpression(SearchConditionUtils.and(
						 		SearchConditionUtils.newComparison(E.REPORT.type, ComparisonOperator.EQUAL, 0),
								SearchConditionUtils.newComparison(E.REPORT.withRuleClass, ComparisonOperator.EQUAL, true)))).size() > 0) ||
				 (!NuclosCodegeneratorConstants.PRINTOUTJARFILE.exists() && getMasterDataFacade().getMasterDataIdsNoCheck(E.REPORT.getUID(),
						 new CollectableSearchExpression(SearchConditionUtils.and(
						 		SearchConditionUtils.newComparison(E.REPORT.type, ComparisonOperator.EQUAL, 1),
								SearchConditionUtils.newComparison(E.REPORT.withRuleClass, ComparisonOperator.EQUAL, true)))).size() > 0) ||
				 (!NuclosCodegeneratorConstants.USERROLEJARFILE.exists() && getMasterDataFacade().getMasterDataIdsNoCheck(E.ROLE.getUID(),
						new CollectableSearchExpression(SearchConditionUtils.newComparison(E.ROLE.withRuleClass, ComparisonOperator.EQUAL, true))).size() > 0) ||
				 (!NuclosCodegeneratorConstants.STATEMODELJARFILE.exists() && getMasterDataFacade().getMasterDataIds(E.STATEMODEL.getUID()).size() > 0))

			rebuildBOs = true;
		
		if (rebuildBOs) {
			getNuclosJarGenerationManager().triggerCancelingOfCurrentGeneratorAndStartNewGenerator("NuclosJavaCompilerComponent.checkBusinessObjects");
		}
	}

	private List<CodeGenerator> getAllCurrentGenerators() {
		final List<CodeGenerator> result = new ArrayList<CodeGenerator>();
		for (MasterDataVO webserviceVO : getMasterDataFacade().getMasterData(E.WEBSERVICE, null)) {
			final CodeGenerator cg = WsdlCodeGenerator.newInstance(applicationContext, webserviceVO);
			if (cg.isRecompileNecessary()) {
				result.add(cg);
			}
		}
		
		for (MasterDataVO servercodeVO : getMasterDataFacade().getMasterData(E.SERVERCODE, null)) {
			final CodeVO code = MasterDataWrapper.getServerCodeVO(servercodeVO);
			if (code.isActive()) {
				final CodeGenerator cg = PlainCodeGenerator.newInstance(applicationContext, code);
				sourceCache.addOrUpdate(code);
				if (cg.isRecompileNecessary()) {
					result.add(cg);
				}
			}
		}
		
		return result;
	}

	private Manifest getManifest() {
		if (manifest == null) {
			manifest = new Manifest();
			HashCodeBuilder builder = new HashCodeBuilder(11, 17);
			for (CodeGenerator gen : getAllCurrentGenerators()) {
				builder.append(gen.hashForManifest());
			}
	
			Attributes mainAttributes = manifest.getMainAttributes();
			mainAttributes.put(Attributes.Name.MANIFEST_VERSION, "1.0");
			mainAttributes.put(NUCLOS_CODE_NUCLET, "default");
			mainAttributes.put(NUCLOS_CODE_HASH, String.valueOf(builder.toHashCode()));
		}
		return manifest;
	}
	
	public void invalidateManifest() {
		manifest = null;
	}

	/**
	 * Validate if Nuclet.jar (the JAR containing all rules) is up-to-date and recompile it
	 * if necessary.
	 * <p>
	 * If automatic rule compilation is disabled, this method does nothing.
	 * </p><p>
	 * If compilation is forced, this method recompiles unconditionally. The force
	 * flag is reset to false after that.
	 * </p><p>
	 * If there is no Nuclet.jar (or an IOException happens when accessing it), a recompile
	 * is triggered.
	 * </p>
	 * @param saveSrc 
	 * @return true if a compilation has happened
	 * @throws NuclosCompileException
	 */
	public boolean validate(boolean saveSrc) throws NuclosCompileException {
		if (!automaticRuleCompilation) {
			return false;
		}
		if (forceCompile) {
			try {
				compile(saveSrc);
			}
			finally {
				forceCompile = false;
			}
		}
		else if (JARFILE.exists()) {
			try {
				JarFile jar = new JarFile(JARFILE);
				if (!jar.getManifest().equals(getManifest())) {
					LOG.info("validate found an outdated {}, recompiling...",
					         JARFILE);
					compile(saveSrc);
				}
				else {
					return false;
				}
			}
			catch(IOException e) {
				LOG.info("rule.jar validation with error: {}", e.getMessage(), e);
				compile(saveSrc);
			}
		}
		else {
			compile(saveSrc);
		}
		return true;
	}

	public boolean isAutomaticRuleCompilation() {
		return automaticRuleCompilation;
	}
	
	public void setAutomaticRuleCompilation(boolean ruleCompilation) {
		this.automaticRuleCompilation = ruleCompilation;
	}

	public boolean isCurrentlyWritingSources() {
		return currentlyWritingSources;
	}

	public void setCurrentlyWritingSources(boolean currentlyWritingSources) {
		if (currentlyWritingSources) {
			sourceScannerComponent.cancel();
		} else {
			sourceScannerComponent.start();
		}
		this.currentlyWritingSources = currentlyWritingSources;
	}

	public synchronized void waitForSynchronization() {
		synchronized (JAR_LOCK) {
			// wait before start a new JarGeneratorThread.
		}
	}

}
