//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.customcode.codegenerator;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static org.nuclos.server.customcode.codegenerator.GeneratedFile.COMMENT_END;
import static org.nuclos.server.customcode.codegenerator.GeneratedFile.COMMENT_START;

import java.io.BufferedReader;
import java.io.CharArrayWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.ClosedWatchServiceException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.security.MessageDigestInputStream;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.customcode.ejb3.CodeFacadeLocal;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.eventsupport.ejb3.SourceCache;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.security.NuclosLocalServerSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * @author Thomas Pasch
 */
@Configurable
class SourceScannerTask {

	private static final Logger LOG = LoggerFactory.getLogger(SourceScannerTask.class);

	private static final Pattern PROP_PAT = Pattern.compile("^//\\s*(\\p{Alnum}+)=(.*)$");

	private static final String NUCLOS_WEBSERVICE_SRC_PATH = WsdlCodeGenerator.DEFAULT_PACKAGE_WEBSERVICES.replace('.', File.separatorChar);

	/**
	 * Not @Autowired because of dependency cycle.
	 */
	@Autowired
	private CodeFacadeLocal codeFacade;

	@Autowired
	private NucletDalProvider dalProvider;

	@Autowired
	private NuclosLocalServerSession nuclosLocalServerSession;

	@Autowired
	private SourceCache sourceCache;

	private WatchService watchService;

	private Thread t;

	SourceScannerTask() {
		LOG.debug("Scanner created");
	}

	public void run() {
		try {
			t = new Thread("SourceScannerTask") {
				@Override
				public void run() {
					LOG.info("Scanner started");
					startWatcher();
				}
			};
			t.start();
		} catch (Exception e) {
			LOG.warn("Scanner failed: ", e);
		}
	}

	public void cancel() {
		if (watchService != null) {
			try {
				watchService.close();
				LOG.info("Scanner canceled");
			} catch (IOException e) {
				LOG.error(e.getMessage(), e);
			}
		}
	}

	public boolean isRunning() {
		return t != null && t.isAlive() && !t.isInterrupted() && watchService != null;
	}

	private void startWatcher() {
		final File srcDir = NuclosCodegeneratorConstants.SOURCE_OUTPUT_PATH;

		if (!srcDir.exists()) {
			LOG.info("Source dir does not exist, scanner canceled");
			cancel();
			return;
		}

		// Watch for modifications on disk
		try {
			watchService = FileSystems.getDefault().newWatchService();
			registerPathAndAllSubdirectories(srcDir.toPath());

			WatchKey key;
			while ((key = watchService.take()) != null) {
				boolean bIgnoreThisWatchEvent = !SpringApplicationContextHolder.isSpringReady(true);
				if (bIgnoreThisWatchEvent) {
					key.reset();
					continue;
				}

				try {
					// wait a few ms before polling events and distinct should works as expected
					Thread.sleep(100);
				} catch (Exception e) {
					// ignore...
				}

				final Path watchPath = (Path) key.watchable();
				List<WatchEvent<?>> pollEvents = distinctPollEvents(key.pollEvents(), watchPath);
				for (WatchEvent<?> event : pollEvents) {

					if (event.context() instanceof Path) {
						final File f = getFile(event, watchPath);

						if (f.isDirectory()) {
							if (!event.kind().equals(ENTRY_DELETE)) {
								registerPathAndAllSubdirectories(f.toPath());
							}
							continue;
						}

						if (!f.getName().endsWith(".java")) {
							// Ignore JetBrains tmp files for example
							continue;
						}
						if (f.getCanonicalPath().startsWith(srcDir.getCanonicalPath() + File.separator + NUCLOS_WEBSERVICE_SRC_PATH)) {
							// ignore webservice sources completely
							continue;
						}
						try {
							if (event.kind().equals(ENTRY_CREATE)) {
								handleExistingFile(f, true);
							} else if (event.kind().equals(ENTRY_MODIFY)) {
								handleExistingFile(f, false);
							} else if (event.kind().equals(ENTRY_DELETE)) {
								handleDeletedFile(f);
							}

						} catch (IOException e) {
							LOG.warn("Can't parse file {}: ", f.getCanonicalPath(), e.getMessage(), e);
						} catch (NumberFormatException e) {
							LOG.warn("Can't parse file {}: ", f.getCanonicalPath(), e.getMessage(), e);
						} catch (CommonBusinessException e) {
							LOG.warn("Can't save modified/new file {}: {}", f.getCanonicalPath(), e.getMessage(), e);
						}
					}
				}

				// watch again...
				key.reset();
			}
		} catch (ClosedWatchServiceException e) {
			// Task canceled
			LOG.debug("Watch service closed: {} ", e.getMessage());
		} catch (InterruptedException e) {
			LOG.error(e.getMessage(), e);
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 * Some IDE's (IDEA) generate too many events (delete -> create -> edit), we are only interested in the last event.
	 * @param pollEvents
	 * @return
	 */
	private static List<WatchEvent<?>> distinctPollEvents(final List<WatchEvent<?>> pollEvents, final Path watchPath) throws IOException {
		final List<WatchEvent<?>> result = new ArrayList<>();
		final Set<String> alreadyProcessedFiles = new HashSet<>();
		for (WatchEvent<?> event : pollEvents) {
			final String canonicalPath = getCanonicalPath(event, watchPath);
			if (alreadyProcessedFiles.contains(canonicalPath)) {
				// remove previous event for the same file in the result
				Iterator<WatchEvent<?>> itResult = result.iterator();
				while (itResult.hasNext()) {
					WatchEvent<?> resultEvent = itResult.next();
					final String resultCanonicalPath = getCanonicalPath(resultEvent, watchPath);
					if (RigidUtils.equal(canonicalPath, resultCanonicalPath)) {
						itResult.remove();
					}
				}
			} else {
				alreadyProcessedFiles.add(canonicalPath);
			}
			result.add(event);
		}
		return result;
	}

	private static File getFile(WatchEvent<?> pollEvent, Path watchPath) {
		final Path contextPath = (Path) pollEvent.context();
		final Path fullPath = watchPath.resolve(contextPath);
		return fullPath.toFile();
	}

	private static String getCanonicalPath(WatchEvent<?> pollEvent, Path watchPath) throws IOException {
		return getFile(pollEvent, watchPath).getCanonicalPath();
	}

	private void registerPathAndAllSubdirectories(final Path start) throws IOException {
		if (watchService != null) {
			// register directory and sub-directories
			Files.walkFileTree(start, new SimpleFileVisitor<Path>() {

				@Override
				public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
						throws IOException {
					dir.register(watchService, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
					return FileVisitResult.CONTINUE;
				}

			});
		}
	}

	public void runOnce() {
		final File srcDir = NuclosCodegeneratorConstants.SOURCE_OUTPUT_PATH;

		// Find modified files on disk
		final List<File> javaSrc = new ArrayList<>();
		scanDir(javaSrc, srcDir, ".java");

		if (javaSrc.isEmpty()) {
			return;
		}

		final Collection<MasterDataVO<UID>> colVo = new ArrayList<>();
		for (File f : javaSrc) {
			GeneratedFile gf = null;
			try {
				// Parse files on disk (to get type and id)
				gf = parseFile(f, false);
				if (gf.getPrimaryKey() != null) {
					final String hashValue = sourceCache.getHashValue(gf.getName());
					if (hashValue == null) {
						// ignore...
					} else if (!hashValue.equals(gf.getHashValue())) {
						// Change data in DB based on file changes on disk
						if (gf.getEntity().checkEntityUID(E.SERVERCODE.getUID())) {
							try {
								if ("org.nuclos.server.customcode.valueobject.CodeVO".equals(gf.getType())) {
									colVo.add(getUpdatedVoAsMd(gf));
								}
							} catch (CommonBusinessException e) {
								LOG.warn("Can't get code vo for {}: {} ", gf.getFile().getCanonicalPath(), e.getMessage(), e);
							} finally {
								nuclosLocalServerSession.logout();
							}
						}
					}
				} else {
					colVo.add(createCodeVoAsMd(E.SERVERCODE, f, gf.getContent()));
				}
			} catch (IOException e) {
				LOG.warn("Can't parse file {}: {}", f, e.getMessage(), e);
			}
		}

		nuclosLocalServerSession.loginAsUser(ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_TIMELIMIT_RULE_USER));
		try {
			codeFacade.createOrModifyBatch(colVo);
		} catch (CommonBusinessException e) {
			LOG.warn("Collective processing resulted in error: {}", e.getMessage(), e);
		} finally {
			nuclosLocalServerSession.logout();
		}
	}

	private boolean handleDeletedFile(File file) throws IOException, CommonBusinessException {
		boolean bResult = false;
		if (file.getName().endsWith(".java")) {
			nuclosLocalServerSession.loginAsUser(ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_TIMELIMIT_RULE_USER));
			try {
				EntityObjectVO<UID> deleteEvo = searchForCode(file, false);
				if (deleteEvo != null) {
					codeFacade.remove(getCodeAsMd(E.SERVERCODE, deleteEvo.getPrimaryKey()), false);
					bResult = true;
				}
			} finally {
				nuclosLocalServerSession.logout();
			}
		}
		return bResult;
	}

	private EntityObjectVO<UID> searchForCode(File file, boolean bBusinessExceptionOnDuplicates) throws IOException, CommonBusinessException {
		Collection<EntityObjectVO<UID>> codesWithSameFileName = dalProvider.getEntityObjectProcessor(E.SERVERCODE).getBySearchExpression(
				new CollectableSearchExpression(SearchConditionUtils.newLikeCondition(E.SERVERCODE.name,
						"*" + file.getName().substring(0, file.getName().length()-".java".length()))));
		EntityObjectVO<UID> foundEvo = null;
		String foundName = null;
		for (EntityObjectVO<UID> codeEvo : codesWithSameFileName) {
			String codeClassName = codeEvo.getFieldValue(E.SERVERCODE.name);
			if (file.getCanonicalPath().endsWith(codeClassName.replace('.', File.separatorChar) + ".java")) {
				if (foundEvo == null) {
					foundEvo = codeEvo;
					foundName = codeClassName;
				} else {
					// more than one code matches the name... delete nothing
					foundEvo = null;
					String sDuplicateMessage = String.format("Code could not be unmistakable determined: File={}; FoundCodes=[{}, {}]", file.getCanonicalPath(), foundName, codeClassName);
					if (bBusinessExceptionOnDuplicates) {
						throw new NuclosBusinessException(sDuplicateMessage);
					}
					LOG.warn("Code could not be unmistakable determined: File={}; FoundCodes=[{}, {}]", file.getCanonicalPath(), foundName, codeClassName);
					break;
				}
			}
		}
		return foundEvo;
	}

	private boolean isUpdatableGenericFileAndHashIsDifferent(GeneratedFile gf) {
		boolean bUpdate = false;
		if (gf.getPrimaryKey() != null && gf.getName() != null && gf.getEntity() != null) {
			final String hashValue = sourceCache.getHashValue(gf.getName());
			if (hashValue == null) {
				// ignore...
			} else if (!hashValue.equals(gf.getHashValue())) {
				bUpdate = E.SERVERCODE.checkEntityUID(gf.getEntity().getUID()) && "org.nuclos.server.customcode.valueobject.CodeVO".equals(gf.getType());
			}
		}
		return bUpdate;
	}

	private boolean handleExistingFile(File file, boolean bJustCreated) throws IOException, CommonBusinessException {
		boolean bResult = false;
		boolean bNewCode = bJustCreated;

		GeneratedFile gf = null;
		try {
			gf = parseFile(file, false);
		} catch (IllegalStateException e) {
			// no content;
		}

		MasterDataVO<UID> vo = null;
		boolean bUpdate = false;

		if (!bNewCode && gf != null) {
			if (gf.getPrimaryKey() == null) {
				bNewCode = true;
			} else {
				bUpdate = isUpdatableGenericFileAndHashIsDifferent(gf);
				if (bUpdate) {
					vo = getUpdatedVoAsMd(gf);
				}
			}
		}

		if (bNewCode && gf != null) {
			// test if already exists
			EntityObjectVO<UID> evo = searchForCode(file, true);
			if (evo != null) {
				bNewCode = false;
				bUpdate = !gf.getHashValue().equals(sourceCache.getHashValue(evo.getFieldValue(E.SERVERCODE.name)));
				vo = new MasterDataVO<UID>(evo);
				vo.setFieldValue(E.SERVERCODE.source, new String(gf.getContent()));
			}
		}

		if (bNewCode && gf != null) {
			nuclosLocalServerSession.loginAsUser(ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_TIMELIMIT_RULE_USER));
			try {
				createCode(E.SERVERCODE, file, gf.getContent(), false);
				bResult = true;
			} catch (CommonBusinessException e) {
				LOG.error("Creating code {} not successfull: {}", file, e.getMessage(), e);
			} finally {
				nuclosLocalServerSession.logout();
			}
		} else if (bUpdate) {
			nuclosLocalServerSession.loginAsUser(ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_TIMELIMIT_RULE_USER));
			try {
				updateCode(vo, file, false);
				bResult = true;
			} finally {
				nuclosLocalServerSession.logout();
			}
		}

		return bResult;
	}

	private MasterDataVO<UID> getUpdatedVoAsMd(GeneratedFile gf) throws CommonBusinessException {
		final MasterDataVO<UID> vo = getCodeAsMd(gf.getEntity(), gf.getPrimaryKey());
		if (!gf.getName().equals(vo.getFieldValue(E.SERVERCODE.name))) {
			throw new IllegalStateException();
		}

		vo.setFieldValue(E.SERVERCODE.source, new String(gf.getContent()));
		return vo;
	}

	public void updateCode(GeneratedFile gf) throws CommonBusinessException {
		final MasterDataVO<UID> vo = getUpdatedVoAsMd(gf);
		updateCode(vo, gf.getFile(), true);
	}

	private void updateCode(MasterDataVO<UID> vo, File file, boolean bCompile) throws CommonBusinessException {
		LOG.info("Update code in nuclos: name={} uid={} from {}", vo.getFieldValue(E.SERVERCODE.name), vo.getPrimaryKey(), file);
		codeFacade.modify(vo, false);
	}

	private MasterDataVO<UID> createCodeVoAsMd(EntityMeta sourceEntityMeta, File sourceFile, char[] content) throws IOException {
		if (!E.SERVERCODE.equals(sourceEntityMeta)) {
			throw new IllegalStateException();
		}

		// build code name
		final File srcDir = NuclosCodegeneratorConstants.SOURCE_OUTPUT_PATH;
		final String codeName = sourceFile.getCanonicalPath().substring(srcDir.getCanonicalPath().length()+1).replace(File.separatorChar, '.').replace(".java", "");

		final MasterDataVO<UID> vo = new MasterDataVO<UID>(new EntityObjectVO<UID>(E.SERVERCODE.getUID()));
		vo.setFieldValue(E.SERVERCODE.source, new String(content));
		vo.setFieldValue(E.SERVERCODE.name, codeName);
		vo.setFieldValue(E.SERVERCODE.description, "Created from an external source");
		vo.setFieldValue(E.SERVERCODE.debug, false);
		vo.setFieldValue(E.SERVERCODE.active, true);

		// search for nuclet
		UID nucletUID = null;
		String foundNucletPackage = null;
		for (EntityObjectVO<UID> nucletVO : MetaProvider.getInstance().getNuclets()) {
			String sNucletPackage = nucletVO.getFieldValue(E.NUCLET.packagefield);
			String sNucletPackageAsPath = nucletVO.getFieldValue(E.NUCLET.packagefield).replace('.', File.separatorChar);
			if (sourceFile.getName().startsWith(sNucletPackage) ||
					sourceFile.getCanonicalPath().endsWith(sNucletPackageAsPath + File.separatorChar + sourceFile.getName())) {
				if (nucletUID == null) {
					nucletUID = nucletVO.getPrimaryKey();
					foundNucletPackage = sNucletPackage;
				} else {
					if (foundNucletPackage.length() < sNucletPackage.length()) {
						nucletUID = nucletVO.getPrimaryKey();
						foundNucletPackage = sNucletPackage;
					}
				}
			}
		}
		vo.setFieldUid(E.SERVERCODE.nuclet, nucletUID);

		return vo;
	}

	public EntityObjectVO<UID> createCode(GeneratedFile gf) throws IOException, CommonBusinessException {
		return createCode(gf.getEntity(), gf.getFile(), gf.getContent(), true);
	}

	public EntityObjectVO createCode(EntityMeta entityMeta, File file, char[] content, boolean bCompile) throws IOException, CommonBusinessException {
		final MasterDataVO<UID> vo = createCodeVoAsMd(entityMeta, file, content);
		LOG.info("Create code in nuclos: name={} from {}", vo.getFieldValue(E.SERVERCODE.name), file);
		codeFacade.create(vo, bCompile);
		return vo.getEntityObject();
	}

	private MasterDataVO<UID> getCodeAsMd(EntityMeta<?> entity, UID pk) throws CommonFinderException, CommonPermissionException {
		return codeFacade.get(pk);
	}

	private void scanDir(List<File> result, File dir, String ext) {
		final File[] files = dir.listFiles();
		for (File f : files) {
			if (f.isDirectory()) {
				scanDir(result, f, ext);
			} else if (f.isFile() && f.getName().endsWith(ext)) {
				result.add(f);
			}
		}
	}

	GeneratedFile parseFile(File file, boolean bThrowIllegalStateIfNoComment) throws IOException {
		final GeneratedFile result = new GeneratedFile(NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_DEVELOPMENT));
		result.setFile(file);
		final MessageDigestInputStream in = new MessageDigestInputStream(new FileInputStream(file), SourceCache.DIGEST);

		try (
				final BufferedReader reader = new BufferedReader(
						new InputStreamReader(in, NuclosCodegeneratorConstants.JAVA_SRC_ENCODING)
				);
				final CharArrayWriter out = new CharArrayWriter()
		) {
			Map<String, String> properties = null;
			String line;
			while ((line = reader.readLine()) != null) {
				if (line.equals(COMMENT_START)) {
					properties = readPropertiesComment(reader);
					properties.forEach((field, value) -> {
						if ("name".equals(field)) {
							result.setName(value);
						} else if ("classname".equals(field)) {
							result.setTargetClassName(value);
						} else if ("type".equals(field)) {
							result.setType(value);
						} else if ("entity".equals(field)) {
							result.setEntity(E.getByName(value));
						} else if ("class".equals(field)) {
							result.setGeneratorClass(value);
						} else if ("uid".equals(field)) {
							result.setPrimaryKey(new UID(value));
						} else {
							LOG.info("Unknown field '{}' with value '{}' in {}", field, value, file);
						}
					});
				} else {
					out.append(line);
					out.append("\n");
				}
			}
			if ((properties == null || properties.isEmpty()) && bThrowIllegalStateIfNoComment) {
				throw new IllegalStateException("Parse code: Can't find CodeGenerator properties in " + file);
			}

			if ("org.nuclos.server.ruleengine.valueobject.RuleVO".equals(result.getType())) {
				copyCode(reader, out);
			} else {
				copy(reader, out);
			}

			if (out.size() <= 0) {
				throw new IllegalStateException();
			}

			// Trim additional empty lines, leave only 1 line break at the end.
			final String trimmedContent = StringUtils.trimToEmpty(out.toString()) + "\n";

			result.setContent(trimmedContent.toCharArray());
		}
		result.setHashValue(in.digestAsBase64());
		return result;
	}

	private Map<String, String> readPropertiesComment(BufferedReader reader) throws IOException {
		Map<String, String> result = new TreeMap<>();

		String line = reader.readLine();
		while (StringUtils.startsWith(line, "//") && !StringUtils.startsWith(line, COMMENT_END)) {
			final Matcher m = PROP_PAT.matcher(line);
			if (m.matches()) {
				String field = m.group(1);
				String value = m.group(2);
				result.put(field, value);
			}
			line = reader.readLine();
		}

		return result;
	}

	private void copy(Reader r, Writer w) throws IOException {
		final char[] buffer = new char[4092];
		int size;
		while ((size = r.read(buffer)) >= 0) {
			w.write(buffer, 0, size);
		}
	}

	private void copyCode(BufferedReader r, Writer w) throws IOException {
		String line;
		boolean begin = false;
		// only copy the 'rule' part of the source
		while ((line = r.readLine()) != null) {
			if (line.trim().equals("// BEGIN RULE")) {
				begin = true;
				break;
			}
		}
		boolean end = false;
		while ((line = r.readLine()) != null) {
			if (line.trim().equals("// END RULE")) {
				end = true;
				break;
			}
			w.write(line, 0, line.length());
			w.write("\n");
		}
		if (!(begin && end)) {
			throw new IllegalStateException("Parse code: Can't find begin and end");
		}
	}

}
