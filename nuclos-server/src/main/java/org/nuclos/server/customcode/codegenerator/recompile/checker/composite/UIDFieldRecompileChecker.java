package org.nuclos.server.customcode.codegenerator.recompile.checker.composite;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.server.customcode.codegenerator.recompile.checker.IRecompileChecker;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Checks if the UID field was modified.
 */
public class UIDFieldRecompileChecker implements IRecompileChecker {

    private final FieldMeta<UID> fieldMeta;

    public UIDFieldRecompileChecker(FieldMeta<UID> fieldMeta) {
        this.fieldMeta = fieldMeta;
    }

    protected boolean isNucletEqual(
        MasterDataVO<?> oldEntity,
        MasterDataVO<?> updatedEntity) {

        UID oldUIDValue = oldEntity.getFieldUid(fieldMeta);
        UID newUIDValue = updatedEntity.getFieldUid(fieldMeta);

        if (oldUIDValue == newUIDValue) {
            return true;
        }
        if (oldUIDValue != null && newUIDValue != null) {
            if (!oldUIDValue.equals(newUIDValue)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void loadRequiredDependentsOf(MasterDataVO<?> mdvo) {
        // nop
    }

    @Override
    public boolean isRecompileRequiredOnUpdate(

        MasterDataVO<?> oldEntity,
        MasterDataVO<?> updatedEntity) {

        return !isNucletEqual(oldEntity, updatedEntity);
    }

    @Override
    public boolean isRecompileRequiredOnInsertOrDelete(final MasterDataVO<?> mdvo) {
        return false;
    }

    @Override
    public String toString() {
        return "UIDRecompileChecker[" + fieldMeta.getEntity() + "/" + fieldMeta.getFieldName() + "]";
    }

}
