package org.nuclos.server.customcode.codegenerator.recompile.checker;

import java.util.Collection;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.CompositeRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.FieldRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.PKRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.UIDFieldRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.helper.OutputFormatRecompileCheckerHelper;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeHelper;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * Checks if a modification to a form must cause a recompile.
 */
@Component
public class FormRecompileChecker implements IRecompileChecker {

    @Autowired
    private MasterDataFacadeHelper masterDataFacadeHelper;

    private final IRecompileChecker basicRecompileChecker;

    private final OutputFormatRecompileCheckerHelper outputFormatRecompileCheckerHelper;

    public FormRecompileChecker() {
        this.basicRecompileChecker = new CompositeRecompileChecker(
            new PKRecompileChecker(),
            new FieldRecompileChecker<>(E.FORM.name),
            new UIDFieldRecompileChecker(E.FORM.nuclet),
			new FieldRecompileChecker(E.FORM.withRuleClass)
        );
        outputFormatRecompileCheckerHelper = new OutputFormatRecompileCheckerHelper(
            E.FORMOUTPUT, E.FORMOUTPUT.description, E.FORMOUTPUT.parent);
    }


    @Override
    public void loadRequiredDependentsOf(MasterDataVO<?> mdvo) {
        String username =
            SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
        Collection<EntityObjectVO<UID>> formOutputEOs =
            masterDataFacadeHelper.getDependantMasterData(E.FORMOUTPUT.parent.getUID(),
                                                          username, null, (UID)mdvo.getPrimaryKey());
        mdvo.getDependents().addAllData(E.FORMOUTPUT.parent, formOutputEOs);
    }

    @Override
    public boolean isRecompileRequiredOnUpdate(MasterDataVO<?> oldReport,
                                               MasterDataVO<?> updatedReport) {
        return isRecompileRequired(oldReport, updatedReport);
    }

    @Override
    public boolean isRecompileRequiredOnInsertOrDelete(MasterDataVO<?> report) {
		if (report.getFieldValue(E.FORM.withRuleClass)) {
			return true;
		} else {
			return false;
		}
    }

    private boolean isRecompileRequired(
        MasterDataVO<?> oldReport,
        MasterDataVO<?> updatedReport) {
		if (!oldReport.getFieldValue(E.FORM.withRuleClass) && !updatedReport.getFieldValue(E.FORM.withRuleClass)) {
			return false;
		}
        if (basicRecompileChecker.isRecompileRequiredOnUpdate(oldReport, updatedReport)) {
            return true;
        }
        if (outputFormatRecompileCheckerHelper.isRecompileRequired(oldReport, updatedReport)) {
            return true;
        }
        return false;
    }
}
