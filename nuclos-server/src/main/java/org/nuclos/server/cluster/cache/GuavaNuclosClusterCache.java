//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.cluster.cache;

import org.nuclos.common.JMSConstants;
import org.nuclos.common.NotNullObject;
import org.nuclos.common.spring.GuavaCache;
import org.nuclos.server.cluster.jms.ClusterActionFactory;
import org.nuclos.server.cluster.jms.NuclosClusterAction;
import org.nuclos.server.jms.NuclosJMSUtils;

import com.google.common.cache.Cache;

/*
 * CLUSTERING
 * Spring configured cache that can say hello to the other cluster node's
 */
public class GuavaNuclosClusterCache<V, K> extends GuavaCache<K, V> implements NuclosSpringClusterCache {
	
	boolean notifyClients;	
	boolean notifyClusterCloud;

	public GuavaNuclosClusterCache(String name, Cache<K, V> wrapped) {
		super(name, wrapped);		
		resetParameterToDefault();
	}

	@Override
	public void evict(Object key) {
		super.evict(key);
		if(notifyClusterCloud) {
			notifyClusterCloud(key, Boolean.FALSE);
		}
		resetParameterToDefault();
	}

	@Override
	public void clear() {
		super.clear();
		if(notifyClusterCloud) {
			notifyClusterCloud(new NotNullObject(), Boolean.TRUE);
		}
		resetParameterToDefault();
	}	
	
	private void notifyClusterCloud(Object key, Boolean bClear) {
		NuclosClusterAction action = ClusterActionFactory.createClusterAction(NuclosClusterAction.SPRINGCACHE_ACTION, getName(), key, bClear);
		NuclosJMSUtils.sendObjectMessage(action, JMSConstants.TOPICNAME_CLUSTER, null);
	}

	@Override
	public void setParameterForClusterCloud(boolean notifyClients,	boolean notifyClusterCloud) {
		this.notifyClients = notifyClients;
		this.notifyClusterCloud = notifyClusterCloud;
	}	
	
	
	private void resetParameterToDefault() {
		notifyClusterCloud = true;
		notifyClients = false;
	}

}
