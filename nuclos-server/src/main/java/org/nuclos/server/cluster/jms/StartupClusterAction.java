//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.cluster.jms;

import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;

import org.apache.activemq.spring.ActiveMQConnectionFactory;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.server.cluster.NuclosClusterRegisterHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class StartupClusterAction implements NuclosClusterAction {

	public static final Logger LOG = LoggerFactory.getLogger(StartupClusterAction.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 7448893644349803294L;
	String sUrl;
	String sServer;
	
	public StartupClusterAction(String url, String fromServer) {
		this.sUrl = url;
		this.sServer = fromServer;
	}

	@Override
	public void doAction() {
		if(sServer.equals(NuclosClusterRegisterHelper.getServerName())) {
			LOG.info("Own action. Nothing to do!");
			return;
		}
		
		try {
			final Topic topic = (Topic) SpringApplicationContextHolder.getBean(JMSConstants.TOPICNAME_CLUSTER_CLIENT);
					
			ActiveMQConnectionFactory jmsFactory = new ActiveMQConnectionFactory();
			jmsFactory.setBrokerURL(sUrl);
			jmsFactory.setTrustAllPackages(true);
			TopicConnection topicConnection = jmsFactory.createTopicConnection();
			topicConnection.start();
			
			TopicSession session = topicConnection.createTopicSession(false, TopicSession.AUTO_ACKNOWLEDGE);
			TopicSubscriber subscriber = session.createSubscriber(topic);
			subscriber.setMessageListener(new ClusterMessageListener());
		}
		catch (Exception e) {
			
			LOG.warn("Unable to startup cluster action:", e);
		}
	
	}

	@Override
	public boolean doOnMaster() {
		return false;
	}

}
