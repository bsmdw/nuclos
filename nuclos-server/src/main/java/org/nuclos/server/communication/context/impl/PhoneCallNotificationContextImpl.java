//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.communication.context.impl;

import org.nuclos.api.UID;
import org.nuclos.api.communication.CommunicationPort;
import org.nuclos.api.context.communication.PhoneCallNotificationContext;

public class PhoneCallNotificationContextImpl extends AbstractCommunicationContext implements PhoneCallNotificationContext {
	
	private String id;
	
	private State state;
	
	private String fromNumber;

	private UID fromUserId;
	
	private String serviceNumber;
	
	private String toNumber;
	
	private UID toUserId;
	
	public PhoneCallNotificationContextImpl(CommunicationPort port, String logClassname) {
		super(port, logClassname);
	}
	
	/**
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * 
	 * @param state
	 */
	@Override
	public void setState(State state) {
		this.state = state;
	}
	
	/**
	 * @param fromNumber
	 * 			calling phone number,
	 * 			(could be null)
	 */
	@Override
	public void setFromNumber(String fromNumber) {
		this.fromNumber = fromNumber;
	}
	
	/**
	 * @param user
	 * 			if your interface could identify the calling Nuclos user.
	 * 			(could be null)
	 */
	@Override
	public void setFromUserId(UID fromUserId) {
		this.fromUserId = fromUserId;
	}

	/**
	 * @param serviceNumber
	 * 			the called service number
	 * 			(could be null)
	 */
	@Override
	public void setServiceNumber(String serviceNumber) {
		this.serviceNumber = serviceNumber;
	}
	
	/**
	 * 
	 * @param toNumber
	 * 			the called phone number, or in case of a service number,
	 * 			the number of the service team member 
	 */
	@Override
	public void setToNumber(String toNumber) {
		this.toNumber = toNumber;
	}
	
	/**
	 * @param user
	 * 			if your interface could identify the called Nuclos user.
	 * 			(could be null)
	 */
	@Override
	public void setToUserId(UID toUserId) {
		this.toUserId = toUserId;
	}

	/**
	 * @return
	 */
	@Override
	public String getId() {
		return id;
	}
	
	/**
	 * 
	 * @return
	 */
	@Override
	public State getState() {
		return state;
	}

	/**
	 * (could be null)
	 * @return
	 */
	@Override
	public String getFromNumber() {
		return fromNumber;
	}
	
	/**
	 * (could be null)
	 * @return
	 */
	@Override
	public UID getFromUserId() {
		return fromUserId;
	}

	/**
	 * (could be null)
	 * @return
	 */
	@Override
	public String getServiceNumber() {
		return serviceNumber;
	}
	
	/**
	 * 
	 * @return
	 */
	@Override
	public String getToNumber() {
		return toNumber;
	}

	/**
	 * (could be null)
	 * @return
	 */
	@Override
	public UID getToUserId() {
		return toUserId;
	}
	
	
	@Override
	public String toString() {
		return "PhoneCallNotificationContext["
				+ "id=" + id
				+ ", state=" + state
				+ ", fromNumber=" + fromNumber
				+ ", fromUserId=" + fromUserId
				+ ", serviceNumber=" + serviceNumber
				+ ", toNumber=" + toNumber
				+ ", toUserId=" + toUserId
				+ "]";
	}

}
