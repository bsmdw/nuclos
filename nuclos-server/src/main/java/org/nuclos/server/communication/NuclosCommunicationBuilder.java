package org.nuclos.server.communication;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ForkJoinPool;

import org.nuclos.api.communication.CommunicationPortKey;
import org.nuclos.common.E;
import org.nuclos.common.NuclosEntityValidator;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorUtils;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.nbo.AbstractNuclosObjectCompiler.NuclosBusinessJavaSource;
import org.nuclos.server.nbo.NuclosObjectBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("nuclosCommunicationObjectBuilder")
public class NuclosCommunicationBuilder extends NuclosObjectBuilder {

	private static final String DEFFAULT_PACKAGE_NUCLOS = "org.nuclos.communication";
	private static final String COMMUNICATION_PORT_CLASS_NAME = "NuclosCommunicationPort";

	@Autowired
	NuclosCommunicationObjectCompiler compiler;

	@Autowired
	private NucletDalProvider nucletDalProvider;

	protected String getNucletPackage() {
		return DEFFAULT_PACKAGE_NUCLOS;
	}

	public void createObjects(final ForkJoinPool builderThreadPool) throws CommonBusinessException, InterruptedException {
		List<NuclosBusinessJavaSource> javaSources = new ArrayList<>();
		javaSources.addAll(createCommunicationPorts());
		compiler.compileSourcesAndJar(builderThreadPool, javaSources);
	}

	private Collection<? extends NuclosBusinessJavaSource> createCommunicationPorts()
		throws InterruptedException {
		final List<NuclosBusinessJavaSource> busiJavaSource = new ArrayList<>();
		final String pkg = getNucletPackage();
		final String entity = COMMUNICATION_PORT_CLASS_NAME;
		final String qname = pkg + "." + entity;
		final String filename = NuclosCodegeneratorUtils.communicationSource(
			pkg, entity).toString();

		busiJavaSource.add(new NuclosBusinessJavaSource(
				qname, filename, createCommunicationPortJavaSource(), true));
		return busiJavaSource;
	}

	private String createCommunicationPortJavaSource() throws InterruptedException {

		List<EntityObjectVO<UID>> results =
			new ArrayList<>(nucletDalProvider.getEntityObjectProcessor(
				E.COMMUNICATION_PORT).getAll());
		Collections.sort(results, new Comparator<EntityObjectVO<UID>>() {
			@Override
			public int compare(EntityObjectVO<UID> o1, EntityObjectVO<UID> o2) {
				return StringUtils
					.compareIgnoreCase(o1.getFieldValue(E.COMMUNICATION_PORT.portName),
					                   o2.getFieldValue(E.COMMUNICATION_PORT.portName));
			}
		});

		StringBuilder builder = new StringBuilder();

		builder
			.append("package ").append(getNucletPackage()).append(";\n\n")
			.append("import ").append(CommunicationPortKey.class.getCanonicalName()).append(";\n\n")
			.append("public class ").append(COMMUNICATION_PORT_CLASS_NAME).append(" {\n\n");

		for (EntityObjectVO<UID> eo : results) {
			if (Thread.currentThread().isInterrupted()) {
				throw new InterruptedException();
			}
			String comPortName = NuclosEntityValidator.escapeJavaIdentifier(
				eo.getFieldValue(E.COMMUNICATION_PORT.portName), "COMPORT");
			builder
				.append("\tpublic static final CommunicationPortKey ")
				.append(comPortName)
				.append(" = new CommunicationPortKey(\"")
				.append(eo.getPrimaryKey().getString()).append("\");\n");
		}

		builder.append("\n}");

		return builder.toString();
	}

}
