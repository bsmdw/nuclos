package org.nuclos.server.statemodel;

import org.nuclos.api.ide.valueobject.SourceType;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants;
import org.nuclos.server.nbo.AbstractNuclosObjectCompiler;
import org.springframework.stereotype.Component;

@Component
public class StatemodelObjectCompiler extends AbstractNuclosObjectCompiler {

	public StatemodelObjectCompiler() {
		super(SourceType.STATEMODEL,
		      NuclosCodegeneratorConstants.STATEMODELJARFILE,
		      NuclosCodegeneratorConstants.STATEMODEL_SRC_DIR_NAME);
	}

}
