//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.i18n.language.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.nuclos.common.E;
import org.nuclos.common.EntityMetaVO;
import org.nuclos.common.EntityTreeViewVO;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NucletEntityMeta;
import org.nuclos.common.NucletFieldMeta;
import org.nuclos.common.NucletLanguageEntityMeta;
import org.nuclos.common.TranslationVO;
import org.nuclos.common.UID;
import org.nuclos.common.transport.vo.EntityMetaTransport;
import org.nuclos.server.database.SpringDataBaseHelper;

public class DataLanguageServerUtils extends DataLanguageUtils {

	public static NucletEntityMeta createEntityLanguageMeta(EntityMetaVO<?> boMeta, FieldMeta<?> entityFieldPK) {

	    NucletLanguageEntityMeta voEntity = new NucletLanguageEntityMeta();
	    EntityMetaTransport toEntity = new EntityMetaTransport();
	    
	    UID entityUID = NucletEntityMeta.getEntityLanguageUID(boMeta);
	    
	    voEntity.flagNew();
	    voEntity.setUID(entityUID);	    
	    voEntity.setSearchable(true);
	    voEntity.setCacheable(false);
	    voEntity.setEditable(true);
	    voEntity.setImportExport(true);
	    voEntity.setTreeRelation(false);
	    voEntity.setTreeGroup(false);
	    voEntity.setStateModel(false);
	    voEntity.setDynamic(false);
	    voEntity.setLogBookTracking(false);
	    voEntity.setFieldValueEntity(Boolean.TRUE);
	    toEntity.setEntityMetaVO(voEntity);
	    toEntity.setTranslation(new ArrayList<TranslationVO>());
	    toEntity.setTreeView(new ArrayList<EntityTreeViewVO>());
	    voEntity.setEntityName(NucletEntityMeta.getEntityLanguageName(boMeta));
	    voEntity.setDbTable(NucletEntityMeta.getEntityLanguageDBTablename(boMeta));
	   
	    List<FieldMeta<?>> lstFields = new ArrayList<FieldMeta<?>>();
	    
	    NucletFieldMeta voField = new NucletFieldMeta();
	    voField.setUID(extractDataLanguageReference(boMeta.getUID()));
	    buildValueListField("struid_t_md_data_language","STRUID_T_MD_DATA_LANGUAGE", "java.lang.String", 255, voField);
	    voField.setEntity(entityUID);
	    voField.setForeignEntity(E.DATA_LANGUAGE.getUID());
	    voField.setForeignEntityField("uid{AEyya}");	
	    voField.setState(0);
	    voField.setIndexed(true);
	    voField.setUnique(true);
	    lstFields.add(voField);
	    
	    voField = new NucletFieldMeta();
	    String dbRefFieldName = "INTID_" + boMeta.getDbTable().toUpperCase();
	    dbRefFieldName = SpringDataBaseHelper.getInstance().getDbAccess().generateName(null, dbRefFieldName);
	    buildValueListField("intid_" + boMeta.getDbTable().toLowerCase(), dbRefFieldName, "java.lang.Long", 20, voField);
	    voField.setUID(extractForeignEntityReference(boMeta.getUID()));
	    voField.setForeignEntity(boMeta.getUID());
	    voField.setEntity(entityUID);
	    voField.setForeignEntityField("uid{" + entityFieldPK.getUID().toString() + "}");	
	    voField.setState(0);
	    voField.setIndexed(true);
	    voField.setUnique(true);
	    lstFields.add(voField);
	    
	    lstFields.addAll(createLocalizedFieldsMeta(boMeta));
	    	    
	    voEntity.setFields(lstFields);
	    
	   return voEntity;
	
	}
	

	private static Collection<? extends FieldMeta<?>> createLocalizedFieldsMeta(
			EntityMetaVO<?> boMeta) {
		
		List<FieldMeta<?>> retVal = new ArrayList<FieldMeta<?>>();
		
		for (FieldMeta fmt : boMeta.getFields()) {
			if (fmt.isLocalized() && fmt.getCalcFunction() == null) {
				if (fmt instanceof NucletFieldMeta) {
					// the localized field itself
					retVal.add(extractFieldValue( (NucletFieldMeta) fmt, boMeta));
					// we add an additional field that indicares wether the value of the field has been set
					// initially or has been modified by the user
					retVal.add(extractFlaggedFieldValue( (NucletFieldMeta) fmt, boMeta));
				} else {
					// the localized field itself
					retVal.add(extractFieldValue( fmt, boMeta));
					// we add an additional field that indicares wether the value of the field has been set
					// initially or has been modified by the user
					retVal.add(extractFlaggedFieldValue(fmt, boMeta));
				}
			}
		}
		
		return retVal;
	}

	private static FieldMeta<?> extractFlaggedFieldValue(FieldMeta fmt,
			EntityMetaVO<?> boMeta) {
		
		NucletFieldMeta newFlagField = new NucletFieldMeta();
		
		String languageFieldDBColumName = 
				NucletFieldMeta.getLanguageFieldDBColumName(fmt, true);
		
	    buildValueListField(fmt.getFieldName() + "_modified", languageFieldDBColumName, "java.lang.Boolean", 5, newFlagField);
	    newFlagField.setUID(extractFieldUID(fmt.getUID(), true));
	    newFlagField.setEntity(NucletEntityMeta.getEntityLanguageUID(boMeta));
	       
	   return newFlagField;
	}

}
