//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.attribute.ejb3;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;

// @Local
public interface LayoutFacadeLocal {

	/**
	 * @return true, if detail layout is available for the given entity name, otherwise false
	 */
	@RolesAllowed("Login")
	boolean isDetailLayoutAvailable(UsageCriteria usage);

	/**
	 * @param sEntity
	 * @return the layout for the given entity name if any, otherwise null
	 */
	@RolesAllowed("Login")
	String getMasterDataLayout(UID sEntity,	boolean bSearchMode, String customUsage);

	/**
	 * returns the entity names of the subform entities along with their foreignkey field
	 * and the referenced parent entity name used in the given layout
	 * Note that this works only for genericobject entities
	 * @param iLayoutId
	 */
	@RolesAllowed("Login")
	Map<EntityAndField, UID> getSubFormEntityAndParentSubFormEntityNamesByLayoutId(UID iLayoutId);

	/**
	 * returns the names of the subform entities along with their foreignkey field
	 * and the referenced parent entity name used in the given entity
	 * @param entity
	 * @param id, id of MasterDataVO or GenericObjectVO
	 * @param forImportOrExport, true if it is used for import- or export-routines
	 * 
	 * @deprecated This method is not very effective for generic objects. Consider to 
	 * 		use {@link #getSubFormEntityAndParentSubFormEntityNamesByGO(UsageCriteria)}
	 * 		or {@link #getSubFormEntityAndParentSubFormEntityNamesMD(UsageCriteria, boolean)}
	 * 		directly.
	 */
	@RolesAllowed("Login")
	Map<EntityAndField, UID> getSubFormEntityAndParentSubFormEntities(UID entity, Object id, boolean forImportOrExport, String customUsage);

	@RolesAllowed("Login")
	Map<EntityAndField, UID> getSubFormEntityAndParentSubFormEntityNamesByGO(UsageCriteria usage)
		 throws CommonFinderException;

	@RolesAllowed("Login")
	Map<EntityAndField, UID> getSubFormEntityAndParentSubFormEntityNamesMD(UsageCriteria usage, boolean forImportOrExport);

	@RolesAllowed("Login")
	Map<EntityAndField, UID> getSubFormEntityAndParentSubFormEntityNamesById(UID iLayoutId);
	
	@RolesAllowed("Login")
	void evictCaches();	
	
	String getLayoutML(UID pk) throws CommonBusinessException;
	
	@RolesAllowed("Login")
	UID getNucletUID(UID pk) throws CommonBusinessException;
	
	UID getDetailLayoutIDForUsage(UsageCriteria usage);

	UID getDetailLayoutIDForUsage(UsageCriteria usage, boolean bSearchLayout);

	Collection<UID> getEntitiesAssignedToLayoutId(UID layoutUID) throws CommonBusinessException;
	
	Set<UID> getAllLayoutUidsForEntity(UID entityUid);

	IDependentKey getDependentKeyBetween(UID master, UID subform, String customUsage);
	
	IDependentKey getDependentKeyBetween(UsageCriteria usageOfMaster, UID subform);
}
