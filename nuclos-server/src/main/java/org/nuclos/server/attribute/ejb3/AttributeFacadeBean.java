//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.attribute.ejb3;

import java.io.StringReader;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.NullArgumentException;
import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosAttributeNotFoundException;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.layoutml.LayoutMLParser;
import org.nuclos.common2.layoutml.exception.LayoutMLException;
import org.nuclos.server.attribute.valueobject.AttributeCVO;
import org.nuclos.server.common.AttributeCache;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.genericobject.GenericObjectMetaDataCache;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.InputSource;

/**
 * Attribute facade bean encapsulating access functions for dynamic attributes.
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Transactional(noRollbackFor= {Exception.class})
public class AttributeFacadeBean extends NuclosFacadeBean implements AttributeFacadeLocal, AttributeFacadeRemote {
	
	@Autowired
	private AttributeCache attributeCache;
	
	@Autowired
	private SpringDataBaseHelper dataBaseHelper;
	
	public AttributeFacadeBean() {
	}
	
	/**
	 * @return a collection containing all dynamic attributes
	 */
	@RolesAllowed("Login")
	public Collection<AttributeCVO> getAttributes(UID iGroupId) {
		final Collection<AttributeCVO> result = new HashSet<AttributeCVO>();

		final SecurityCache securitycache = SecurityCache.getInstance();

		for (AttributeCVO attrcvo : attributeCache.getAttributes()) {
			if (iGroupId == null || iGroupId.equals(attrcvo.getAttributegroupUID())) {
				final AttributeCVO attrcvoClone = attrcvo.clone();
				final FieldMeta<?> fieldMeta = metaProvider.getEntityField(attrcvoClone.getId());				
				attrcvoClone.setPermissions(securitycache.getAttributeGroup(this.getCurrentUserName(), fieldMeta.getEntity(), attrcvoClone.getAttributegroupUID(), getCurrentMandatorUID()));
				result.add(attrcvoClone);
			}
		}
		return result;
	}

	/**
	 * §precondition iAttributeId != null
	 * 
	 * @param attributeUID id of attribute
	 * @return the attribute value object for the attribute with the given id
	 * @throws CommonPermissionException
	 */
	public AttributeCVO get(UID attributeUID) throws CommonFinderException, CommonPermissionException {
//		this.checkReadAllowed(NuclosEntity.ATTRIBUTE);
		if (attributeUID == null) {
			throw new NullArgumentException("attributeUID");
		}
		final AttributeCVO result;
		try {
			result = attributeCache.getAttribute(attributeUID);
		}
		catch (NuclosAttributeNotFoundException ex) {
			throw new CommonFinderException(ex);
		}
		return result;
	}

	/**
	 * invalidates the attribute cache (console function)
	 */
	@RolesAllowed("Login")
	public void invalidateCache() {
		AttributeCache.getInstance().invalidateCache(true, true);
	}

	/**
	 * @return the available functions for eg calculated attributes
	 */
	@RolesAllowed("Login")
	@Transactional(propagation=Propagation.NOT_SUPPORTED, noRollbackFor= {Exception.class})
	public Map<String, String> getCallableFunctions(final Class<?> result,final  Object... args) {
		final String dbTypeName = dataBaseHelper.getDbAccess().getDbType().name();
		CollectableSearchCondition cond = SearchConditionUtils.newComparison(E.DBOBJECT.dbobjecttype, ComparisonOperator.EQUAL, "function");
		CollectableSearchExpression expr = new CollectableSearchExpression(cond);
		Collection<EntityObjectVO<UID>> dbos = NucletDalProvider.getInstance().getEntityObjectProcessor(E.DBOBJECT).getBySearchExpression(expr);
		return CollectionUtils.transformIntoMap(dbos, new Transformer<EntityObjectVO<UID>, String>() {
			@Override
			public String transform(EntityObjectVO<UID> i) {
				return i.getFieldValue(E.DBOBJECT.name);
			}
		
		}, new Transformer<EntityObjectVO<UID>, String>() {

			@Override
			public String transform(EntityObjectVO<UID> i) {
				return i.getFieldValue(E.DBOBJECT.description);
			}
			
		});
	}

	/**
	 *
	 * @param sAttributeName
	 * @return the layouts that contained this attribute
	 */
	@RolesAllowed("Login")
	public Set<String> getAttributeLayouts(UID sAttributeName){
		Set<String> sLayoutsName = new HashSet<String>();
		LayoutMLParser parser = new LayoutMLParser();
		Map<UID, String> mLayoutMap = GenericObjectMetaDataCache.getLayoutMap();
		Iterator<UID> iLayoutsIds = mLayoutMap.keySet().iterator();
		while(iLayoutsIds.hasNext()){
			UID iId = iLayoutsIds.next();
			try{
				if (mLayoutMap.get(iId) != null) {
					Set<UID> set = parser.getCollectableFieldUids(new InputSource(new StringReader(mLayoutMap.get(iId))));
					if(set.contains(sAttributeName))
						sLayoutsName.add(GenericObjectMetaDataCache.getInstance().getLayoutML(iId));
				}
			}
			catch(LayoutMLException e){
				throw new CommonFatalException(e);
			}
		}
		return sLayoutsName;
	}
	
	@RolesAllowed("Login")
	public Set<String> getAttributeForModule(String sModuleId) {		
		throw new NotImplementedException("Method obsolete! Please solve different!");
	}
	
}
