package org.nuclos.server.test;

import java.util.Map;

public interface IntegrationTest {

	Map<String, Object> run(String sTestcase, Map<String, Object> mapParams) throws Exception;

}
