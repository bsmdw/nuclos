//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.searchfilter.ejb3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;

import org.apache.commons.lang.ArrayUtils;
import org.nuclos.common.E;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.MandatorVO;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.TranslationVO;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.searchfilter.EntitySearchFilter2;
import org.nuclos.common2.searchfilter.EntitySearchFilter2Support;
import org.nuclos.server.common.LocaleUtils;
import org.nuclos.server.common.NuclosUserDetailsContextHolder;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.SessionUtils;
import org.nuclos.server.common.ejb3.LocaleFacadeLocal;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbStatementUtils;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.nuclos.server.searchfilter.valueobject.SearchFilterUserVO;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.transaction.annotation.Transactional;

/**
 * Facade bean for searchfilter. <br>
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Transactional(noRollbackFor= {Exception.class})
public class SearchFilterFacadeBean implements SearchFilterFacadeLocal, SearchFilterFacadeRemote {
	
	private static final Logger LOG = LoggerFactory.getLogger(SearchFilterFacadeBean.class);

	@Autowired
	private MasterDataFacadeLocal masterDataFacade;

	@Autowired
	private SessionUtils sessionUtils;

	@Autowired
	private LocaleFacadeLocal localeFacade;
	
	@Autowired
	private SecurityCache securityCache;
	
	@Autowired
	private EntitySearchFilter2Support esfu;
	
	@Autowired
	private NuclosUserDetailsContextHolder userCtx;
	
	public SearchFilterFacadeBean() {
	}
	
	private final MasterDataFacadeLocal getMasterDataFacade() {
		return masterDataFacade;
	}
	
	private final LocaleFacadeLocal getLocaleFacade() {
		return localeFacade;
	}

	@Override
	public UID modify(MasterDataVO<UID> mdvo, IDependentDataMap mpDependants, List<TranslationVO> resources)
			throws CommonCreateException, CommonFinderException, CommonRemoveException, CommonStaleVersionException,
			CommonValidationException, CommonPermissionException, NuclosBusinessRuleException {

		if (mpDependants != null) {
			mdvo.setDependents(mpDependants);
		}
		UID uid = (UID) masterDataFacade.modify(mdvo,null);
		if (resources != null) {
			setResources(mdvo, resources);
		}

		Set<UID> usersToInvalidate = new HashSet<UID>();
		Set<UID> usersToNotify = new HashSet<UID>();
		
		// mdvo is E.SEARCHFILTER. (NUCLOS-3069)
		//usersToInvalidate.add(mdvo.getFieldUid(E.SEARCHFILTERUSER.user));
		
		Set<UID> assignedUsers = getAssignedSearchFilterUsers(uid);
		usersToInvalidate.addAll(assignedUsers);
		usersToNotify.addAll(assignedUsers);
		
		UID[] asUsers = new UID[usersToNotify.size()];
		notifyClients(mdvo.getPrimaryKey(), usersToNotify.toArray(asUsers));
		invalidateCaches(usersToInvalidate, null);
		for (MandatorVO mandatorVO : SecurityCache.getInstance().getAllMandators()) {
			invalidateCaches(usersToInvalidate, mandatorVO.getUID());
		}
		// Invalidate security cache (compulsory filters are security-related)
		SecurityCache.getInstance().invalidateCache(true, true);

		return uid;
	}
	
	private Set<UID> getAssignedSearchFilterUsers(UID uid) {
		Set<UID> users = new HashSet<UID>();
		
		final Collection<EntityObjectVO<UID>> colldep = masterDataFacade.getDependantMd4FieldMeta(
				E.SEARCHFILTERUSER.searchfilter, uid);
		
		for (EntityObjectVO<UID> mdvodep : colldep) {
			UID userUid = mdvodep.getFieldUid(E.SEARCHFILTERUSER.user);
			users.add(userUid);
		}
		return users;
	}

    @RolesAllowed("Login")
	public Collection<SearchFilterVO> getAllSearchFilterByCurrentUser() throws CommonFinderException, CommonPermissionException {
    	return getAllSearchFilterByUser(sessionUtils.getCurrentUserName(), userCtx.getMandatorUID());
    }
    
    public Collection<SearchFilterVO> getAllSearchFilterByUser(String sUser, UID mandator) throws CommonFinderException, CommonPermissionException {
    	LOG.debug("Getting all search filters for user: {}", sUser);
    	UID userUID = securityCache.getUserUid(sUser);
    	Set<UID> userRoles = securityCache.getUserRoles(sUser, mandator);
    	return getAllSearchFilterByUserUidAndRoles(userUID, userRoles, mandator==null?UID.UID_NULL:mandator);
    }

	@Cacheable(value="searchFilterByUserAndRoles", key="#p0.getString() + #p1.hashCode() + #p2.getString()")
	private Collection<SearchFilterVO> getAllSearchFilterByUserUidAndRoles(UID userUid, Set<UID> userRoles, UID mandator) throws CommonFinderException, CommonPermissionException {
		final Collection<SearchFilterVO> collSearchFilter = new ArrayList<SearchFilterVO>();

		// 1. get all searchfilteruser objects for given user
		final CollectableSearchCondition condSearchFilterUser = SearchConditionUtils.newUidComparison(
				E.SEARCHFILTERUSER.user, ComparisonOperator.EQUAL, userUid);

		for (MasterDataVO<UID> mdVO_searchFilteruser : getMasterDataFacade().getMasterData(
				E.SEARCHFILTERUSER, condSearchFilterUser)) {
			// 2. get corresponding searchfilter
			MasterDataVO<UID> mdVO_searchfilter = getMasterDataFacade().get(
					E.SEARCHFILTER.getUID(), mdVO_searchFilteruser.getFieldUid(E.SEARCHFILTERUSER.searchfilter.getUID()));

			// 3. transform MasterdataVOs to SearchFilterVO
			collSearchFilter.add(SearchFilterVO.transformToSearchFilter(mdVO_searchfilter, mdVO_searchFilteruser));
		}

		for (UID iRoleId : userRoles) {
			// 4. get all searchfilterrole objects for given user
			final CollectableSearchCondition condSearchFilterRole = SearchConditionUtils.newUidComparison(
					E.SEARCHFILTERROLE.role.getUID(), ComparisonOperator.EQUAL, iRoleId);

			for (MasterDataVO<UID> mdVOSearchFilterrole : getMasterDataFacade().getMasterData(
					E.SEARCHFILTERROLE, condSearchFilterRole)) {
				// 5. get corresponding searchfilter
				MasterDataVO<UID> mdVOSearchfilter = getMasterDataFacade().get(
						E.SEARCHFILTER, mdVOSearchFilterrole.getFieldUid(E.SEARCHFILTERROLE.searchfilter.getUID()));

				// 6. set user id and editable cause of transformToSearchFilter in 7. | Now this entry looks like an SEARCHFILTERUSER entry
				mdVOSearchFilterrole.setFieldValue(E.SEARCHFILTERROLE.role.getUID(), userUid);
				//TODO MULTINUCLET field does not exist 
				//mdVO_searchFilterrole.setFieldValue("editable", Boolean.FALSE);

				// 7. transform MasterdataVOs to SearchFilterVO and check if exists already in list
				SearchFilterVO sfRoleVO = SearchFilterVO.transformToSearchFilter(mdVOSearchfilter, mdVOSearchFilterrole);
				boolean sfUserVOFound = false;
				for (SearchFilterVO sfUserVO : collSearchFilter) {
					if (sfUserVO.getId().equals(sfRoleVO.getId())) {
						sfUserVOFound = true;						
					}
				}
				if (!sfUserVOFound) {
					collSearchFilter.add(sfRoleVO);					
				}
			}
		}

		return collSearchFilter;
	}

	@Caching(evict={
			@CacheEvict(value="searchFilterByUserAndRoles", allEntries=true),
			// NUCLOS-5611: Until a smoother solution, evict all entries
			//@CacheEvict(value="searchFilterByUserAndRoles", key="#p0.getString() + #p1.hashCode() + #p2?.getString()"),
	})	
	void invalidateCache(UID userUid, Set<UID> userRoles, UID mandator) {		
	}

	private void invalidateCaches(Set<UID> userUids, UID mandator) {
		if (userUids.isEmpty()) {
			invalidateCache(UID.UID_NULL, Collections.<UID>emptySet(), mandator);
		}

		for (UID userUid : userUids) {
			invalidateCache(userUid, securityCache.getUserRolesForUid(userUid, mandator), mandator);
		}
	}
	
	public SearchFilterVO getSearchFilterByPk(UID pk) throws CommonBusinessException {
		final Collection<SearchFilterVO> svos = getAllSearchFilterByUser(sessionUtils.getCurrentUserName(), userCtx.getMandatorUID());
		for (SearchFilterVO sf : svos) {
			if (sf.getPrimaryKey().equals(pk)) {
				return sf;
			}
		}
		return null;
	}

	public EntitySearchFilter2 createEntitySearchFilterFromVO(SearchFilterVO vo) {
		return esfu.createSearchFilter(vo);
	}

	/**
	 * creates the given search filter for the current user as owner
	 */
    @RolesAllowed("Login")
	public SearchFilterVO createSearchFilter(SearchFilterVO filterVO) throws NuclosBusinessRuleException, CommonCreateException, CommonPermissionException {
		final MasterDataVO<UID> mdVOSearchfilter = SearchFilterVO.transformToMasterData(filterVO);
		
		UID userUid = securityCache.getUserUid(sessionUtils.getCurrentUserName());

		filterVO.getSearchFilterUser().setEditable(true);
		filterVO.getSearchFilterUser().setUser(userUid);

		final MasterDataVO<UID> mdVOSearchfilteruser = SearchFilterUserVO.transformToMasterData(filterVO.getSearchFilterUser());

		final IDependentDataMap dmdp = new DependentDataMap();
		
		dmdp.addData(E.SEARCHFILTERUSER.searchfilter, mdVOSearchfilteruser.getEntityObject());

		mdVOSearchfilter.setDependents(dmdp);
		// E.SEARCHFILTER
		final MasterDataVO<UID> mdVO_searchfilter_new = getMasterDataFacade().create(mdVOSearchfilter, null);
		final Collection<EntityObjectVO<UID>> coll_searchfilteruser_new = getMasterDataFacade().getDependantMd4FieldMeta(
				E.SEARCHFILTERUSER.searchfilter, (UID) mdVO_searchfilter_new.getPrimaryKey());

		assert coll_searchfilteruser_new.size() == 1;

		Set<UID> userUids = new HashSet<UID>();
		userUids.add(userUid);
		invalidateCaches(userUids, userCtx.getMandatorUID());

		EntityObjectVO<UID> firstEO = coll_searchfilteruser_new.iterator().next();
		return SearchFilterVO.transformToSearchFilter(mdVO_searchfilter_new, new MasterDataVO<UID>(firstEO));
	}

	/**
	 * modifies the given searchfilter
	 * ATTENTION: this will not modify the searchfilteruser, only the searchfilter will be modified
	 */
    @RolesAllowed("Login")
	public SearchFilterVO modifySearchFilter(SearchFilterVO filterVO) 
			throws NuclosBusinessRuleException, CommonCreateException, CommonFinderException, 
			CommonRemoveException, CommonStaleVersionException, CommonValidationException, 
			CommonPermissionException {
    	// E.SEARCHFILTER
    	final UID oId = modify(SearchFilterVO.transformToMasterData(filterVO), null, null);
		return SearchFilterVO.transformToSearchFilter(getMasterDataFacade().get(E.SEARCHFILTER, oId),
				SearchFilterUserVO.transformToMasterData(filterVO.getSearchFilterUser()));
	}

	/**
	 * deletes the given searchfilter
	 */
    @RolesAllowed("Login")
	public void removeSearchFilter(SearchFilterVO filterVO) 
			throws NuclosBusinessRuleException, CommonCreateException, CommonFinderException, 
			CommonRemoveException, CommonStaleVersionException, CommonValidationException, 
			CommonPermissionException {
		// if the user is not the owner of the searchfilter, remove only the searchfilteruser record
    	Set<UID> usersToInvalidate = new HashSet<UID>();
		usersToInvalidate.add(filterVO.getSearchFilterUser().getUser());
		if (!filterVO.getOwner().equals(sessionUtils.getCurrentUserName())) {
			// E.SEARCHFILTERUSER
			getMasterDataFacade().remove(E.SEARCHFILTERUSER.getUID(), filterVO.getSearchFilterUser().getId(), false, null);
		}
		// if the user is the owner of the searchfilter, remove the searchfilter and the assigned searchfilteruser records
		else {
			Set<UID> assignedUsers = getAssignedSearchFilterUsers(filterVO.getId());
			getMasterDataFacade().remove(E.SEARCHFILTER.getUID(), filterVO.getId(), true, null);
			usersToInvalidate.addAll(assignedUsers);
		}

		UID[] asUsers = new UID[usersToInvalidate.size()];
		notifyClients(filterVO.getId(), usersToInvalidate.toArray(asUsers));
		invalidateCaches(usersToInvalidate, null);
		for (MandatorVO mandatorVO : SecurityCache.getInstance().getAllMandators()) {
			invalidateCaches(usersToInvalidate, mandatorVO.getUID());
		}

		// Invalidate security cache (compulsory filters are security-related)
		SecurityCache.getInstance().invalidateCache(true, true);
	}

	/**
	 * updates the createdBy field of the given searchfilter
	 * ATTENTION: this is only used by the migration process
	 */
    @RolesAllowed("Login")
	public void changeCreatedUser(final UID searchFilterUid, final String sUserName) throws NuclosBusinessRuleException, CommonCreateException, CommonFinderException, CommonRemoveException, CommonStaleVersionException, CommonValidationException, CommonPermissionException {
		SpringDataBaseHelper.getInstance().execute(DbStatementUtils.updateValues(E.SEARCHFILTER,
			SF.CREATEDBY.getMetaData(E.SEARCHFILTER), sUserName).where(SF.PK_UID.getMetaData(searchFilterUid), searchFilterUid));
	}

	private void notifyClients(final UID id, final UID[] asUsers) {
		LOG.info("JMS send notify clients: {}, id: {}, {}", Arrays.asList(asUsers), id, this);
		NuclosJMSUtils.sendObjectMessageAfterCommit(ArrayUtils.add(asUsers,0, id), JMSConstants.TOPICNAME_SEARCHFILTERCACHE, null);
	}

	public List<TranslationVO> getResources(UID id) throws CommonBusinessException {
		ArrayList<TranslationVO> result = new ArrayList<TranslationVO>();

		MasterDataVO<UID> sf = getMasterDataFacade().get(E.SEARCHFILTER, id);

		LocaleFacadeLocal service = localeFacade;

		String labelResourceId = sf.getFieldValue(E.SEARCHFILTER.labelres);
		String descriptionResourceId = sf.getFieldValue(E.SEARCHFILTER.descriptionres);

		for (LocaleInfo li : service.getAllLocales(false)) {
			Map<String, String> labels = new HashMap<String, String>();
			labels.put(TranslationVO.LABELS_FIELD[0], service.getResourceById(li, labelResourceId));
			labels.put(TranslationVO.LABELS_FIELD[1], service.getResourceById(li, descriptionResourceId));

			TranslationVO vo = new TranslationVO(li, labels);
			result.add(vo);
		}
		return result;
	}

	private void setResources(MasterDataVO<UID> sf, List<TranslationVO>  translations) {
		String labelResourceId = sf.getFieldValue(E.SEARCHFILTER.labelres);
		String descriptionResourceId = sf.getFieldValue(E.SEARCHFILTER.descriptionres);

		Map<UID, LocaleInfo> lis = CollectionUtils.transformIntoMap(getLocaleFacade().getAllLocales(false), new Transformer<LocaleInfo, UID>() {
				@Override
				public UID transform(LocaleInfo i) {
					return i.getLocale();
				}
			}, new Transformer<LocaleInfo, LocaleInfo>() {
				@Override
				public LocaleInfo transform(LocaleInfo i) {
					return i;
				}
			});

		for(TranslationVO vo : translations) {
			LocaleInfo li = lis.get(vo.getLocale());

			labelResourceId = getLocaleFacade().setResourceForLocale(labelResourceId, li, vo.getLabels().get(TranslationVO.LABELS_FIELD[0]));
			descriptionResourceId = getLocaleFacade().setResourceForLocale(descriptionResourceId, li, vo.getLabels().get(TranslationVO.LABELS_FIELD[1]));
		}
		LocaleUtils.setResourceIdForField(E.SEARCHFILTER, sf.getPrimaryKey(), E.SEARCHFILTER.labelres, labelResourceId);
		LocaleUtils.setResourceIdForField(E.SEARCHFILTER, sf.getPrimaryKey(), E.SEARCHFILTER.descriptionres, descriptionResourceId);
	}


}
