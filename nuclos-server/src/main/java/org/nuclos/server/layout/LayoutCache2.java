package org.nuclos.server.layout;

import java.util.Map;

import org.nuclos.common.E;
import org.nuclos.common.GenericObjectMetaDataProvider;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.common.dblayer.JoinType;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.layout.ILayoutCache2;
import org.nuclos.server.attribute.ejb3.LayoutFacadeLocal;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbJoin;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LayoutCache2 implements ILayoutCache2 {
	
	// Spring injection
	
	@Autowired
	private LayoutFacadeLocal layoutFacadeLocal;
	
	@Autowired
	private GenericObjectMetaDataProvider genericObjectMetaDataProvider;
	
	@Autowired
	private MetaProvider metaProvider;
	
	@Autowired
	private SpringDataBaseHelper springDataBaseHelper;
	
	// end of Spring injection
		
	LayoutCache2() {
	}

	@Override
	public UID getMasterDataLayoutId(UID entityUID, String customUsage, boolean searchMode) {
		assert !metaProvider.getEntity(entityUID).isStateModel();
		
		final DbQueryBuilder builder = springDataBaseHelper.getDbAccess().getQueryBuilder();
		final DbQuery<UID> query = builder.createQuery(UID.class);		
		final DbFrom<UID> from = query.from(E.LAYOUT);
		query.select(from.column(SystemFields.BASE_ALIAS, SF.PK_UID));
		
		final DbJoin<UID> join = from.join(E.LAYOUTUSAGE, JoinType.LEFT, "u");
		join.on(E.LAYOUTUSAGE.getPk(), E.LAYOUTUSAGE.layout);
		query.where(builder.and(
				builder.equal(from.column("u", E.LAYOUTUSAGE.entity), builder.literal(entityUID)),
				builder.equal(from.column("u", E.LAYOUTUSAGE.custom), builder.literal(customUsage)),
				builder.equal(from.column("u", E.LAYOUTUSAGE.searchScreen), builder.literal(searchMode))
		));
		return CollectionUtils.getSingleIfExist(springDataBaseHelper.getDbAccess().executeQuery(query));
	}

	@Override
	public UID getGenericObjectLayoutId(UsageCriteria usage, String customUsage, boolean searchMode) {
		try {
			return (genericObjectMetaDataProvider.getBestMatchingLayout(usage, searchMode));
		}
		catch (CommonFinderException e) {
			return null;
		}
	}

	@Override
	public String getLayoutXml(UID layoutId) {
		// ??? also works for MasterData?
		return genericObjectMetaDataProvider.getLayoutML(layoutId);
	}

	@Override
	public Map<EntityAndField, UID> getSubFormEntityAndParentSubFormEntityNamesById(UID layoutId) {
		return layoutFacadeLocal.getSubFormEntityAndParentSubFormEntityNamesById(layoutId);
	}

	@Override
	public boolean hasLayout(UID entityUID) {
		final DbQueryBuilder builder = springDataBaseHelper.getDbAccess().getQueryBuilder();
		final DbQuery query = builder.createQuery(UID.class);
		final DbFrom from = query.from(E.LAYOUTUSAGE);
		query.select(from.column(SystemFields.BASE_ALIAS, SF.PK_UID.getMetaData(E.LAYOUTUSAGE.getUID())));
		query.where(builder.equal(from.column(SystemFields.BASE_ALIAS, E.LAYOUTUSAGE.entity), builder.literal(entityUID)));
		return !springDataBaseHelper.getDbAccess().executeQuery(query).isEmpty();
	}

}
