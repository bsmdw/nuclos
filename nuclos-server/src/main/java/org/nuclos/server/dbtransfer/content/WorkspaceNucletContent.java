//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dbtransfer.content;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.nuclos.common.E;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.dal.DalCallResult;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dbtransfer.Transfer;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbStatementUtils;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;


public class WorkspaceNucletContent extends DefaultNucletContent {

	public WorkspaceNucletContent(List<INucletContent> contentTypes) {
		super(E.WORKSPACE, contentTypes);
	}
	
	@Override
	public CollectableSearchCondition cachingCondition() {
		return cachingConditionStatic();
	}
	
	public static CollectableSearchCondition cachingConditionStatic() {
		return SearchConditionUtils.and( 
				SearchConditionUtils.newIsNullCondition(E.WORKSPACE.user), 
				SearchConditionUtils.newComparison(E.WORKSPACE.maintenance, ComparisonOperator.EQUAL, Boolean.FALSE));
	}

	@Override
	public List<EntityObjectVO<UID>> getNcObjects(Set<UID> nucletUIDs) {
		List<EntityObjectVO<UID>> result = NucletDalProvider.getInstance().getEntityObjectProcessor(E.WORKSPACE).getBySearchExpression(
				new CollectableSearchExpression(SearchConditionUtils.newIsNullCondition(E.WORKSPACE.user)));
		
		Iterator<EntityObjectVO<UID>> it = result.iterator();
		while (it.hasNext()) {
			EntityObjectVO<UID> ncObject = it.next();
			UID nucletUID = ncObject.getFieldUid(E.WORKSPACE.nuclet);
			if (nucletUID != null) {
				if (!nucletUIDs.contains(nucletUID)) {
					it.remove();
				}
			} else {
				it.remove();
			}
		}

		return result;
	}

	@Override
	public void deleteNcObject(DalCallResult result, EntityObjectVO<UID> ncObject, boolean testOnly) {
		if (ncObject.getFieldUid(E.WORKSPACE.user) == null) { // is assignable workspace
			// Remove user assigned workspaces.
			SpringDataBaseHelper.getInstance().getDbAccess().execute(DbStatementUtils.deleteFrom(E.WORKSPACE, E.WORKSPACE.assignedWorkspace, ncObject.getPrimaryKey()));
		}
		super.deleteNcObject(result, ncObject, testOnly);
		invalidateWorkspaceCaches();
	}

	@Override
	public boolean insertOrUpdateNcObject(DalCallResult result, EntityObjectVO<UID> ncObject, boolean testOnly, final Transfer transfer, final LogEntry log) {
		boolean updated = super.insertOrUpdateNcObject(result, ncObject, testOnly, transfer, log);
		if (!testOnly && updated) {
			if (ncObject.getFieldUid(E.WORKSPACE.user) == null) { // is assignable workspace
				SpringDataBaseHelper.getInstance().getDbAccess().execute(DbStatementUtils.updateValues(
						E.WORKSPACE, E.WORKSPACE.name, ncObject.getFieldValue(E.WORKSPACE.name))
						.where(E.WORKSPACE.assignedWorkspace, ncObject.getPrimaryKey()));
			}
			invalidateWorkspaceCaches();
		}
		return updated;
	}
	
	private void invalidateWorkspaceCaches() {
		NucletDalProvider.getInstance().getWorkspaceProcessor().invalidateWorkspaceCaches();
	}
	
}
