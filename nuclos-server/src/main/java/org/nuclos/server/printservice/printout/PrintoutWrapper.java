//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.printservice.printout;

import org.nuclos.api.print.PrintProperties;
import org.nuclos.common.UID;
import org.nuclos.common.report.valueobject.OutputFormatTO;
import org.nuclos.common.report.valueobject.PrintPropertiesTO;
import org.nuclos.common.report.valueobject.ReportOutputVO;
import org.springframework.stereotype.Component;

/**
 * {@link PrintoutWrapper} wrapping VOs for printout
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
@Component
public class PrintoutWrapper {

	/**
	 * create {@link OutputFormatTO} from {@link ReportOutputVO}
	 * 
	 * @param outputVO {@link ReportOutputVO}
	 * @return {@link OutputFormatTO}
	 */
	public OutputFormatTO wrapTO(final ReportOutputVO outputVO) {
		final OutputFormatTO to = new OutputFormatTO((UID) outputVO.getId(), outputVO.getIsMandatory(), outputVO.getDestination(), outputVO.getParameter(), outputVO.getFormat());
		to.setDescription(outputVO.getDescription());
		to.setAttachDocument(outputVO.getAttachDocument());
		to.setProperties(wrapTO(outputVO.getPrintProperties()));
		return to;
	}
	
	/**
	 * create {@link PrintPropertiesTO} from {@link PrintProperties}
	 * 
	 * @param properties {@link PrintProperties}
	 * @return {@link PrintPropertiesTO}
	 */
	public PrintPropertiesTO wrapTO(final PrintProperties properties) {
		final PrintPropertiesTO to = new PrintPropertiesTO();
		to.setCopies(properties.getCopies());
		to.setDuplex(properties.isDuplex());
		to.setPrintServiceId(properties.getPrintServiceId());
		to.setTrayId(properties.getTrayId());
		return to;
	}

}
