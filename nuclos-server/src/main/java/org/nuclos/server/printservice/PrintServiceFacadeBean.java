//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.printservice;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.print.PrintService;

import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.print.PrintProperties;
import org.nuclos.api.printout.Printout;
import org.nuclos.api.rule.PrintFinalRule;
import org.nuclos.api.rule.PrintRule;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.printservice.PrintExecutionContext;
import org.nuclos.common.printservice.PrintServiceFacadeRemote;
import org.nuclos.common.printservice.PrintServiceLocator;
import org.nuclos.common.report.valueobject.OutputFormatTO;
import org.nuclos.common.report.valueobject.PrintPropertiesTO;
import org.nuclos.common.report.valueobject.PrintResultTO;
import org.nuclos.common.report.valueobject.PrintServiceTO;
import org.nuclos.common.report.valueobject.PrintoutTO;
import org.nuclos.common.report.valueobject.ReportOutputVO;
import org.nuclos.common.report.valueobject.ReportOutputVO.Destination;
import org.nuclos.common.report.valueobject.ReportOutputVO.Format;
import org.nuclos.common.report.valueobject.ReportVO.OutputType;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonPrintException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.common2.exception.PrintoutException;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ejb3.EntityObjectFacadeBean;
import org.nuclos.server.common.ejb3.EntityObjectFacadeLocal;
import org.nuclos.server.common.valueobject.DocumentFileBase;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeBean;
import org.nuclos.server.genericobject.ejb3.GenericObjectFacadeLocal;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.printservice.printout.PrintoutServiceProvider;
import org.nuclos.server.printservice.printout.PrintoutWrapper;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * {@link PrintServiceFacadeBean}
 * support for {@link Printout}
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
@Component
@Transactional(noRollbackFor = { Exception.class })
public class PrintServiceFacadeBean implements PrintServiceFacadeRemote {

	private final static Logger LOG = LoggerFactory.getLogger(PrintServiceFacadeBean.class);

	@Autowired
	private EntityObjectFacadeBean eoFacade;

	@Autowired
	private EventSupportFacadeBean esFacade;

	@Autowired
	private PrintoutWrapper wrapperPrintout;

	@Autowired
	private PrintoutServiceProvider psFacade;
	
	@Autowired 
	private PrintServiceRepository printServiceRepository;
	
	@Autowired
	private NucletDalProvider nucletDalProvider;
	
	@Autowired
	private PrintSpooler printSpooler;

	@RolesAllowed("Login")
	@Override
	public List<PrintoutTO> printoutsForBO(final UsageCriteria usagecriteria,
			Long pk) throws CommonPrintException {
		LOG.debug("lookup printouts for BO: {}", pk);
		
		if (pk == null) {
			// multiprint
			if (esFacade.existPrinoutEventSupportsForUsageCriteria(usagecriteria)) {
				throw new CommonPrintException("Error.print.rules.found.collective.print.not.possible");
			}
			try {
				return esFacade.findPrintoutTransportObjectsByUsageCriteriaOnly(usagecriteria);
			} catch (NuclosBusinessRuleException ex) {
				throw new CommonPrintException(ex);
			}
		}

		final List<PrintoutTO> result = new ArrayList<PrintoutTO>();
		try {
			final List<PrintoutTO> resultRuleStage = new ArrayList<PrintoutTO>();
			final EntityObjectVO<Long> eoVO = eoFacade.get(
					usagecriteria.getEntityUID(), pk);
			resultRuleStage.addAll(firePrintEventSupport(eoVO,
					PrintRule.class.getCanonicalName()));
			if (LOG.isDebugEnabled()) {
				LOG.debug("printRule stage provides {} printouts", resultRuleStage.size());
			}

			for (final PrintoutTO printoutVO : resultRuleStage) {
				if (printoutVO.getBusinessObjectId() == null) {
					printoutVO.setBusinessObjectId(pk);
				}
				result.add(printoutVO);
			}

		} catch (CommonPermissionException ex) {
			throw new CommonFatalException(ex);
		} finally {
			LOG.debug("printouts for BO: {}", result.size());
		}
		return result;
	}

	private List<PrintoutTO> firePrintEventSupport(
			EntityObjectVO<Long> eoVO, String sEventSupportType) throws CommonPrintException {
		
		try {
			return esFacade.firePrintEventSupport(eoVO, sEventSupportType);
		} catch (final NuclosBusinessRuleException | NuclosCompileException ex) {
			throw new CommonPrintException(ex);
		}
	}

	@RolesAllowed("Login")
	@Override
	public List<PrintServiceTO> printServices() {
		LOG.debug("lookup printservices");
		return printServiceRepository.printServices();
	}
	
	@RolesAllowed("Login")
	@Override
	public PrintServiceTO printService(final UID idPrintservice) {
		LOG.debug("lookup printservice {}", idPrintservice);
		return printServiceRepository.printService(idPrintservice);
	}

	@RolesAllowed("Login")
	public PrintResultTO executeOutputFormat(final OutputFormatTO outputFormat, final Long idBo) throws PrintoutException {
		LOG.debug("execute outputformat {}", outputFormat.getId());
		try {
			return new PrintResultTO(outputFormat, psFacade.run(outputFormat, idBo, outputFormat.getDatasourceParams()));
		} catch (final BusinessException ex) {
			throw new PrintoutException(null, outputFormat, ex);
		}
	}

	/**
	 * execute preview for {@link Printout}
	 * 
	 * @param printout {@link PrintoutTO}
	 * 
	 * @return list of {@link PrintResultTO} (printed files)
	 * @throws BusinessException
	 * @throws CommonPrintException
	 */
	@RolesAllowed("Login")
	protected List<PrintResultTO> executePreview(final PrintoutTO printout)
			throws PrintoutException {
		LOG.debug("execute printout {}", printout.getId());

		final List<PrintResultTO> result = new ArrayList<PrintResultTO>();
		if (printout.getOutputType() == OutputType.EXCEL) {
			// execute only the first outputformat...
			if (!printout.getOutputFormats().isEmpty()) {
				result.add(executeOutputFormat(printout.getOutputFormats().get(0), printout.getBusinessObjectId()));
			}
		} else {
			for (final OutputFormatTO of : printout.getOutputFormats()) {
				result.add(executeOutputFormat(of, printout.getBusinessObjectId()));
			}
		}

		return result;
	}

	@RolesAllowed("Login")
	public List<PrintResultTO> executePreview(final List<PrintoutTO> printouts)
			throws PrintoutException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("execute printout batch with printouts {}", printouts.size());
			for (final PrintoutTO printout : printouts) {
				LOG.debug("execute printout {}", printout.getId());
			}
		}

		final List<PrintResultTO> result = new ArrayList<PrintResultTO>();
		for (final PrintoutTO printout : printouts) {
			result.addAll(executePreview(printout));
		}

		return result;

	}
	
	public void printFile(final NuclosFile file, UID printServiceId) throws CommonPrintException {
		this.printFile(file, null, printServiceId);
	}
	
	public void printFile(final NuclosFile file, PrintService printService) throws CommonPrintException {		
		this.printFile(file, printService, null);
	}
	
	public void printFile(final NuclosFile file, PrintService printService, UID printServiceId) throws CommonPrintException {
		PrintPropertiesTO pp = new PrintPropertiesTO();
		pp.setPrintService(printService);
		pp.setPrintServiceId(printServiceId);
		pp.setCopies(1);
		this.printFile(file, pp);
	}
	
	public void printFile(final NuclosFile file, PrintProperties properties) throws CommonPrintException {
		final List<PrintResultTO> results = new ArrayList<PrintResultTO>();
		OutputFormatTO dummy = new OutputFormatTO(null, false, ReportOutputVO.Destination.PRINTER_SERVER, null, null) {
			@Override
			public String getDescription() {
				return file.getName();
			}
		};
		dummy.setProperties(wrapperPrintout.wrapTO(properties));
		
		results.add(new PrintResultTO(dummy, file));
		
		PrintSpoolerScheduleContext ctx = new PrintSpoolerScheduleContext() {
			// TODO
		};
		getPrintSpooler().schedule(results, ctx);
	}

	@RolesAllowed("Login")
	public List<PrintResultTO> executePrintout(final List<PrintoutTO> printouts, final PrintExecutionContext context) throws PrintoutException, NuclosBusinessRuleException {
		
		final List<PrintResultTO> results = executePreview(printouts);
		final List<PrintResultTO> result = new ArrayList<PrintResultTO>();
	
		try {
			final EntityObjectVO<Long> eoVO = eoFacade.get(
					context.getUsagecriteria().getEntityUID(),
					context.getPk());

			LOG.debug("firePrintFinalEventSupport");
			esFacade.firePrintFinalEventSupport(eoVO, PrintFinalRule.class.getCanonicalName(), results);

			PrintSpoolerScheduleContext ctx = new PrintSpoolerScheduleContext() {
				// TODO
			};
			getPrintSpooler().schedule(results, ctx);
			
			boolean attach = false;
			for (PrintResultTO print : results) {
				if (print.getOutputFormat().isAttachDocument()) {
					attach = true;
				}
				// TODO cache outputFormats
				final EntityObjectVO<UID> reportOutput = 
						nucletDalProvider.getEntityObjectProcessor(E.REPORTOUTPUT).getByPrimaryKey((UID) print.getOutputFormat().getId());
				final ReportOutputVO reportOutputVO = MasterDataWrapper.getReportOutputVO(new MasterDataVO<UID>(reportOutput, true));
				final Destination destination = reportOutputVO.getDestination();
				if (destination == Destination.DEFAULT_PRINTER_CLIENT ||
					destination == Destination.PRINTER_CLIENT ||
					destination == Destination.FILE ||
					destination == Destination.SCREEN) {
					if (reportOutputVO.getFormat() == Format.DOCX && (
							destination == Destination.DEFAULT_PRINTER_CLIENT ||
							destination == Destination.PRINTER_CLIENT)) {
						NuclosFile toPdf;
						try {
							toPdf = PrintUtils.toPdf(print.getOutput());
						} catch (CommonPrintException e) {
							throw new PrintoutException(null, print.getOutputFormat(), e);
						}
						result.add(new PrintResultTO(print.getOutputFormat(), toPdf));
					} else {
						result.add(print);
					}
				}
			}
			
			if (attach) {
				attachDocuments(context, results);
			}
			
			return result;
		} catch (final CommonPermissionException ex) {
			throw new NuclosFatalException(ex);
		} catch (final NuclosCompileException ex) {
			throw new NuclosFatalException(ex);
		}
	}
	
	private void attachDocuments(final PrintExecutionContext context, final List<PrintResultTO> prints) throws PrintoutException {
				
		EntityMeta<Long> eMeta = MetaProvider.getInstance().getEntity(context.getUsagecriteria().getEntityUID());
		if (!eMeta.isStateModel()) {
			return;
		}
		
		EntityMeta<Long> eMetaAttachment = null;
		FieldMeta<Long> parentAttribute = null;
		FieldMeta<String> commentAttribute = null;
		FieldMeta<UID> fileAttribute = null;
		FieldMeta<?> createdDate = null;
		FieldMeta<?> createdUser = null;
		if (context.getAttachmentSubEntityUID() != null && !context.getAttachmentSubEntityUID().equals(E.GENERALSEARCHDOCUMENT.getUID())) {
			// for backward compatibility
			
			final UID[] attachmentSubEntityAttributeUIDs = context.getAttachmentSubEntityAttributeUIDs();
			
			eMetaAttachment = MetaProvider.getInstance().getEntity(context.getAttachmentSubEntityUID());
			for (FieldMeta<?> fMeta : MetaProvider.getInstance().getAllEntityFieldsByEntity(eMetaAttachment.getUID()).values()) {
				if (fMeta.getFieldName().equals("genericObject")) {
					parentAttribute = (FieldMeta<Long>) fMeta;
				}
				if (fMeta.getFieldName().equals("comment")) {
					commentAttribute = (FieldMeta<String>) fMeta;
				}
				if (fMeta.getFieldName().equals("filename")) {
					fileAttribute = (FieldMeta<UID>) fMeta;
				}
				if (attachmentSubEntityAttributeUIDs != null && attachmentSubEntityAttributeUIDs.length >= 1) {
					// comment
					if (RigidUtils.equal(fMeta.getUID(), attachmentSubEntityAttributeUIDs[0])) {
						commentAttribute = (FieldMeta<String>) fMeta;
					}
				}
				if (attachmentSubEntityAttributeUIDs != null && attachmentSubEntityAttributeUIDs.length >= 2) {
					// createdDate
					if (RigidUtils.equal(fMeta.getUID(), attachmentSubEntityAttributeUIDs[1])) {
						createdDate = fMeta;
					}
				}
				if (attachmentSubEntityAttributeUIDs != null && attachmentSubEntityAttributeUIDs.length >= 3) {
					// createdUser
					if (RigidUtils.equal(fMeta.getUID(), attachmentSubEntityAttributeUIDs[2])) {
						createdUser = fMeta;
					}
				}
				if (attachmentSubEntityAttributeUIDs != null && attachmentSubEntityAttributeUIDs.length >= 4) {
					// filename
					if (RigidUtils.equal(fMeta.getUID(), attachmentSubEntityAttributeUIDs[3])) {
						fileAttribute = (FieldMeta<UID>) fMeta;
					}
				}				
			}
		}
		if (parentAttribute == null || fileAttribute == null) {
			eMetaAttachment = E.GENERALSEARCHDOCUMENT;
			parentAttribute = E.GENERALSEARCHDOCUMENT.genericObject;
			commentAttribute = E.GENERALSEARCHDOCUMENT.comment;
			fileAttribute = E.GENERALSEARCHDOCUMENT.documentfile;
			createdDate = null;
			createdUser = null;
		}
		
		final EntityObjectVO<Long> eo = nucletDalProvider.<Long>getEntityObjectProcessor(eMeta.getUID()).getByPrimaryKey(context.getPk());
		
		String genericObjectIdentifier = eo.getFieldValue(SF.SYSTEMIDENTIFIER);
		
		for (final PrintResultTO print : prints) {
			if (!print.getOutputFormat().isAttachDocument()) {
				continue;
			}
			try {
				final GenericObjectDocumentFile loFile = new GenericObjectDocumentFile(print.getOutput().getName(), DocumentFileBase.newFileUID(), print.getOutput().getContent());
				
				final MasterDataVO<Long> mdvo = new MasterDataVO<Long>(eMetaAttachment, false);
				mdvo.setFieldId(parentAttribute, eo.getPrimaryKey());
				mdvo.setFieldValue(fileAttribute.getUID(), loFile);
				mdvo.setFieldUid(fileAttribute.getUID(), (UID)loFile.getDocumentFilePk());
				if (commentAttribute != null) {
					mdvo.setFieldValue(commentAttribute.getUID(), SpringLocaleDelegate.getInstance().getMessage("ReportController.2", "Automatisch angef\u00fcgtes Dokument"));
				}
				if (createdDate != null) {
					mdvo.setFieldValue(createdDate.getUID(), new InternalTimestamp(System.currentTimeMillis()));
				}
				if (createdUser != null) {
					mdvo.setFieldValue(createdUser.getUID(), getCurrentUserName());
				}
				
				SpringApplicationContextHolder.getBean(GenericObjectFacadeLocal.class).attachDocumentToObject(mdvo, parentAttribute.getUID());
				
			}
			catch (Exception ex) {
				throw new PrintoutException(null, print.getOutputFormat(), SpringLocaleDelegate.getInstance().getMessage(
						"ReportController.1", "Anh\u00e4ngen der Datei \"{0}\" an GenericObject \"{1}\" fehlgeschlagen.", print.getOutput().getName(), genericObjectIdentifier), ex);
			}
		}
	}

	protected PrintSpooler getPrintSpooler() {
		return this.printSpooler;
	}
	
	@RolesAllowed("Login")
	public Integer autoSetupPrintServices() throws CommonPermissionException {
		SecurityCache.getInstance().isWriteAllowedForMasterData(getCurrentUserName(), E.PRINTSERVICE.getUID(), null);
		javax.print.PrintService[] srvs = PrintServiceLocator.lookupPrintServices();
		int result = 0;
		try {
			for (PrintService srv : srvs) {
				if (srv != null) {
					final String name = srv.getName();
					CollectableSearchCondition cond = SearchConditionUtils.newComparison(E.PRINTSERVICE.name, ComparisonOperator.EQUAL, name);
					List<EntityObjectVO<UID>> srvsFound = nucletDalProvider.getEntityObjectProcessor(E.PRINTSERVICE).getBySearchExpression(new CollectableSearchExpression(cond));
					if (srvsFound.isEmpty()) {
						LOG.info("Print service '{}' does not exist. Create it...", name);
						// create new print service
						EntityObjectVO<UID> eoSrv = new EntityObjectVO<UID>(E.PRINTSERVICE);
						eoSrv.setFieldValue(E.PRINTSERVICE.name, name);
						eoSrv.setFieldValue(E.PRINTSERVICE.useNativePdfSupport, false);
						
						for (PrintServiceTO.Tray tray : PrintServiceLocator.createTrayList(srv)) {
							EntityObjectVO<UID> eoTray = new EntityObjectVO<UID>(E.PRINTSERVICE_TRAY);
							eoTray.setFieldValue(E.PRINTSERVICE_TRAY.number, tray.getNumber());
							eoTray.setFieldValue(E.PRINTSERVICE_TRAY.description, tray.getDescription());
							eoSrv.getDependents().addData(E.PRINTSERVICE_TRAY.printService, eoTray);
						}
						SpringApplicationContextHolder.getBean(EntityObjectFacadeLocal.class).insert(eoSrv);
						result++;
					} else {
						LOG.info("Print service '{}' already exist. UID={}",
						         name, srvsFound.get(0).getPrimaryKey().getString());
					}
				}
			}
		} catch (Exception ex) {
			LOG.error(ex.getMessage(), ex);
			throw new NuclosFatalException(ex);
		} 
		return result;
	}
	
	public final String getCurrentUserName() {
		return SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
	}
}