//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.documentfile;

import org.nuclos.common.UID;

/**
 * Value object that contains meta information of a file
 * like documentUID, name, checksum and length 
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
public class DocumentFileChecksum {
	
	private UID documentUID;
	
	private String filename;
	
	private String checksum;
	
	private long filelength;

	public DocumentFileChecksum(UID documentUID, String filename,
			String checksum, long filelength) {
		super();
		this.documentUID = documentUID;
		this.filename = filename;
		this.checksum = checksum;
		this.filelength = filelength;
	}

	public UID getDocumentUID() {
		return documentUID;
	}

	public String getFilename() {
		return filename;
	}

	public String getChecksum() {
		return checksum;
	}

	public long getFilelength() {
		return filelength;
	}
}
