//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.navigation.ejb3;

import org.nuclos.server.navigation.treenode.GenericObjectTreeNode.RelationDirection;
import org.nuclos.server.navigation.treenode.GenericObjectTreeNode.SystemRelationType;

public class RelationContext {
	private final Integer iRelationId;
	private final SystemRelationType relationtype;
	private final RelationDirection direction;
	public RelationContext(Integer iRelationId,
			SystemRelationType relationtype, RelationDirection direction) {
		super();
		this.iRelationId = iRelationId;
		this.relationtype = relationtype;
		this.direction = direction;
	}
	public Integer getiRelationId() {
		return iRelationId;
	}
	public SystemRelationType getRelationtype() {
		return relationtype;
	}
	public RelationDirection getDirection() {
		return direction;
	}
}
