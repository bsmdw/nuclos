package org.nuclos.server.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import org.nuclos.server.common.NuclosSystemParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LauncherDownloadServlet extends FileServlet {

	private static final String BASE;
	private static final Logger LOG = LoggerFactory.getLogger(LauncherDownloadServlet.class);

	static {

		BASE = NuclosSystemParameters.getDirectory(NuclosSystemParameters.LAUNCHER_PATH).getAbsolutePath();

	}

	public LauncherDownloadServlet() {
	}

	@Override
	public void init() throws ServletException {
		super.init();
	}

	@Override
	protected String getBasePath() {
		return BASE;
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response, boolean content) throws IOException {
		final String pi = request.getPathInfo();

		final HttpServletRequestWrapper wrapper = new HttpServletRequestWrapper(request) {
			@Override
			public String getPathInfo() {

				return pi;
			}
		};
		super.processRequest(wrapper, response, content);
	}

}