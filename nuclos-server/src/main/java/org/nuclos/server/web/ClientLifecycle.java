//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Client lifecycle tracking as required by BMWFDM-563.
 * 
 * see https://support.novabit.de/browse/BMWFDM-563
 * @author Thomas Pasch
 * @since Nuclos 4.0.10
 */
//startup context: @Component does NOT work (tp)
public class ClientLifecycle {

	private static final Logger LOG = LoggerFactory.getLogger(ClientLifecycle.class);
	
	private static ClientLifecycle INSTANCE;
	
	// Spring injection
	
	@Autowired
	private Session session;
	
	@Autowired
	private Request request;
	
	// end of Spring injection
	
	ClientLifecycle() {
		INSTANCE = this;
	}
	
	static ClientLifecycle getInstance() {
		if (INSTANCE == null) {
			throw new IllegalStateException("too early");
		}
		return INSTANCE;
	}
	
	private void checkSession(HttpSession s) {
		if (s == null) return;
		String remoteAddr = null;
		try {
			remoteAddr = session.getRemoteAddress();
		} catch (BeanCreationException e) {
			// sometimes we are NOT in a session here, e.g. on startup 
			// with some obscure user '(superuser)'
			return;
		}
		if (remoteAddr == null) {
			try {
				session.setRemoteAddress(request.getHttpRequest().getRemoteAddr());
				session.setRemotePort(request.getHttpRequest().getRemotePort());
			} catch (BeanCreationException e) {
				// if the session is new, this is triggered by the session listener
				// which has NO access to the request
			}
		}
	}
	
	public void webStartRequest(HttpServletRequest request) {
		// We MUST create a http session here because
		// - we want to be able to track the user while downloading AND login
		// - we MUST invalidate our maps
		// (tp)
		boolean canAccessSession = true;
		HttpSession httpSession;
		try {
			httpSession = session.getHttpSession();
			checkSession(httpSession);
		} catch (BeanCreationException e) {
			canAccessSession = false;
			// currently NO session - create one
			httpSession = request.getSession(true);
		}
		if (canAccessSession) {
			session.setRemoteAddress(request.getRemoteAddr());
			session.setRemotePort(request.getRemotePort());
			LOG.info("downloading {}{} {}", request.getContentType(), request.getServletPath(),
			         session);
		} else {
			log(request, httpSession);
		}
	}
	
	private void log(HttpServletRequest request, HttpSession httpSession) {
		String id = "";
		String last = "";
		if (httpSession != null) {
			id = httpSession.getId();
			last = Session.getDateFormat().format(httpSession.getLastAccessedTime());
		}
		LOG.debug("downloading {}{} from {}:{} session: id {} last {}",
		         request.getContentType()==null?"":(request.getContentType()+" "), request.getServletPath(),
		         request.getRemoteAddr(), request.getRemotePort(), id, last);
	}
	
	public void newSession(HttpSession s) {
		checkSession(s);
		LOG.debug("created session {}", s.getId());
	}
	
	public void destroySession(HttpSession s) {
		checkSession(s);
		LOG.debug("destroyed session " + s.getId());
	}
	
	public void clientWaitingForLogin() {
		checkSession(session.getHttpSession());
		LOG.info("clientWaitingForLogin {}", session);
	}
	
	public void clientLogin(String username) {
		HttpSession httpSession = null;
		try {
			httpSession = session.getHttpSession();
			checkSession(httpSession);
			session.setUsername(username);
			LOG.info("clientLogin user={} {}", username, session);
		} catch (BeanCreationException e) {
			// sometimes we are NOT in a session here, e.g. on startup 
			// with some obscure user '(superuser)'
			LOG.debug("clientLogin user={} NOT IN SESSION SCOPE (scheduled job?)", username);
		}
	}
	
	public void clientReady() {
		checkSession(session.getHttpSession());
		LOG.info("clientReady {}", session);
	}
	
	public void clientLogout(String username) {
		checkSession(session.getHttpSession());
		session.setUsername(username);
		LOG.info("clientLogout {} {}", username, session);
	}
	
}
