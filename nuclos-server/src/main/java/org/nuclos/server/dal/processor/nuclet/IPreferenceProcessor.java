//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor.nuclet;

import java.util.List;
import java.util.Set;

import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.PreferenceVO;
import org.nuclos.server.dal.specification.IDalReadSpecification;
import org.nuclos.server.dal.specification.IDalWriteSpecification;

public interface IPreferenceProcessor extends 
	IDalWriteSpecification<PreferenceVO, UID>,
	IDalReadSpecification<PreferenceVO, UID> {

	PreferenceVO getByPrimaryKeyWithSelected(UID id, UID userUID);

	List<PreferenceVO> getByAppAndType(String app, String type);
	List<PreferenceVO> getByAppTypeAndUser(String app, String type, UID user);
	List<PreferenceVO> getByAppTypeUserAndMenuRelevance(String app, String type, UID user, boolean menuRelevant);
	List<PreferenceVO> getByAppTypeUserAndEntity(String app, String type, UID user, Set<UID> entity, UID layout, boolean orLayoutIsNull);
	List<PreferenceVO> getByAppAndNuclet(String app, UID nuclet);
	List<PreferenceVO> getByAppAndUser(String app, UID user);
	List<PreferenceVO> getByAppUserAndMenuRelevance(String app, UID user, boolean menuRelevant);
	List<PreferenceVO> getByAppUserAndEntity(String app, UID user, Set<UID> entity, UID layout, boolean orLayoutIsNull);

	PreferenceVO getBySharedKeyAndUser(UID sharedPreferenceKey, UID user);
	void deleteUserPreferences(UID user);
	/**
	 * reset user customizations, if any
	 * @param sharedPreference
	 */
	void batchResetUserCustomizations(PreferenceVO sharedPreference);
	/**
	 * delete customizations for users who lost their grant completely
	 * @param prefUID
	 * @param superUsersToo usually, super users customizations are not deleted. 
	 * 			Set it true handles a super user like a default one.
	 */
	void batchDeleteCustomizations(UID prefUID, boolean superUsersToo);
	/**
	 * delete all customized preferences of these types for the given user.
	 * @param userUID
	 * @param entity
	 * @param type
	 */
	void batchDeleteAllCustomizationsForUser(UID userUID, UID entity, String...type);
	/**
	 * delete customizations for the given user, but only if he or she lost their grant completely
	 * @param userUID
	 */
	void batchDeleteCustomizationsForUser(UID userUID);
	/**
	 * select the given preference for the user and layout. Overwrites a selection of the same type, entity and app (user dependent)
	 * @param prefUID
	 * @param userUID
	 * @param layoutUID (optional)
	 *                  layoutUID checked against a possible layoutUID in the pref. But only the the given layoutUID is used for selection!
	 */
	void selectPreferenceForUser(UID prefUID, UID userUID, final UID layoutUID);
	/**
	 * select the given preference for the user and layout. Overwrites a selection of the same type, entity and app (user dependent)
	 * @param pref
	 * @param userUID
	 * @param layoutUID (optional)
	 * 					layoutUID checked against a possible layoutUID in the pref. But only the the given layoutUID is used for selection!
	 * @return false: if selection is not possible, e.g. subform-table without layout declaration
	 */
	boolean selectPreferenceForUser(PreferenceVO pref, UID userUID, final UID layoutUID);
	/**
	 * de-select the given preference for the user.
	 * @param prefUID
	 * @param userUID
	 * @param layoutUID (optional)
	 */
	void deselectPreferenceForUser(UID prefUID, UID userUID, final UID layoutUID);
}
