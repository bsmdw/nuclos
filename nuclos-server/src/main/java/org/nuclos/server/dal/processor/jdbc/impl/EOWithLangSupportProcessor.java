package org.nuclos.server.dal.processor.jdbc.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.EntityMetaVO;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NucletEntityMeta;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collection.JdbcTransformerParams;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.DataLanguageMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.dal.processor.ColumnToLanguageFieldVOMapping;
import org.nuclos.server.dal.processor.IColumnToVOMapping;
import org.nuclos.server.dal.processor.IColumnWithMdToVOMapping;
import org.nuclos.server.dal.processor.ProcessorConfiguration;
import org.nuclos.server.dal.processor.ProcessorFactorySingleton;
import org.nuclos.server.dblayer.MetaDbHelper;
import org.nuclos.server.dblayer.query.DbCondition;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbLanguageJoin;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbSelection;
import org.nuclos.server.dblayer.structure.DbColumn;
import org.nuclos.server.dblayer.structure.DbColumnType;
import org.nuclos.server.dblayer.structure.DbNamedObject;
import org.nuclos.server.dblayer.structure.DbNullable;
import org.nuclos.server.i18n.language.data.DataLanguageCache;
import org.nuclos.server.i18n.language.data.DataLanguageLocalizedEntityEntry;
import org.nuclos.server.i18n.language.data.DataLanguageServerUtils;
import org.nuclos.server.i18n.language.data.DataLanguageVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class EOWithLangSupportProcessor<PK> extends EntityObjectProcessor<PK> {

	@Autowired
	private DataLanguageCache dataLangCache;

	public EOWithLangSupportProcessor(final ProcessorConfiguration<PK> config) {
		super(config);
	}

	@Override
	protected EOSearchExpressionUnparser createEOSearchExpressionUnparser(DbQuery<?> query) {
		EOSearchExpressionUnparser unparser = super.createEOSearchExpressionUnparser(query);
		unparser.setDataLanguageCache(dataLangCache);
		return unparser;
	}

	@Override
	protected final DbQuery<Object[]> createQuery(List<IColumnToVOMapping<?, PK>> columns, boolean bAllLanguages,
												  Collection<IColumnWithMdToVOMapping<?, PK>> referenceColumnsAsSubselect) {
		final DbQuery<Object[]> query = super.createQuery(columns, bAllLanguages, referenceColumnsAsSubselect);

		// join lang tables for localized fields of this entity
		if (getMetaData().IsLocalized()) {
			DbFrom<PK> from = query.getDbFrom();
			List<DbSelection<Object[]>> selections = query.getSelections();
			joinLocalizationTables(columns, from, selections, bAllLanguages);
		}

		return query;
	}

	/*
	 	moved from AbstractJdbcDalProcessor
	 */
	private void joinLocalizationTables(List<IColumnToVOMapping<?, PK>> columns, DbFrom<PK> from, List<DbSelection<Object[]>> selections, boolean bAllLanguages) {
		FieldMeta<?> primPkField =
				getMetaData().isUidEntity() ? SF.PK_UID.getMetaData(getMetaData()): SF.PK_ID.getMetaData(getMetaData());
		EntityMeta<?> langEntityMeta = MetaProvider.getInstance()
				.getEntity(NucletEntityMeta.getEntityLanguageUID(getMetaData()));

		UID refFieldUID = DataLanguageServerUtils.extractForeignEntityReference(getMetaData().getUID());
		UID refDataLangFieldUID = DataLanguageServerUtils.extractDataLanguageReference(getMetaData().getUID());

		FieldMeta<?> refField = langEntityMeta.getField(refFieldUID);
		FieldMeta<?> refDataLangField = langEntityMeta.getField(refDataLangFieldUID);

		DbNamedObject nmdObj = new DbNamedObject(langEntityMeta.getUID(), langEntityMeta.getDbTable());
		DbColumnType columnType = MetaDbHelper.createDbColumnType(refField);
		DbColumn col = new DbColumn(refField.getUID(), nmdObj,
				refField.getDbColumn(), columnType, DbNullable.NULL, refField.getDefaultValue(), refField.getOrder());

		Collection<DataLanguageVO> langsToUse = null;
		if (bAllLanguages) {
			langsToUse = dataLangCache.getDataLanguages();
		} else {
			langsToUse = new ArrayList<>();
			langsToUse.add(dataLangCache.getNuclosPrimaryDataLanguageObject());
			if (!dataLangCache.getNuclosPrimaryDataLanguage().equals(dataLangCache.getLanguageToUse())) {
				langsToUse.add(dataLangCache.getCurrentUserDataLanguageObject());
			}
		}


		for (DataLanguageVO dlvo : langsToUse) {

			DbLanguageJoin<?> newJoin;
			DbFrom parent = from;

			newJoin = parent.leftJoin(langEntityMeta, "LANG_" + dlvo.getPrimaryKey().toString(), dataLangCache.getLanguageToUse());

			DbCondition condMatchingPK =
					dataBaseHelper.getDbAccess().getQueryBuilder().equal(from.baseColumn(primPkField), newJoin.baseColumn(col));
			newJoin.onAnd(condMatchingPK,
					dataBaseHelper.getDbAccess().getQueryBuilder().<String>equalValue(
							newJoin.baseColumn(refDataLangField), dlvo.getPrimaryKey().toString()));

			if(selections != null) {
				selections.add(newJoin.baseColumn(SF.PK_ID.getMetaData(langEntityMeta), dlvo.getPrimaryKey()));
			}

			if (columns != null) {
				columns.add(ProcessorFactorySingleton.createLanguageFieldMapping(
						SystemFields.BASE_ALIAS, dlvo.getPrimaryKey(), SF.PK_ID.getMetaData(langEntityMeta), false, true));
			}

			for (Object langDataFieldObj : langEntityMeta.getFields()) {
				FieldMeta langDataField = (FieldMeta) langDataFieldObj;
				if (langDataField.getForeignEntity() == null) {
					if(selections != null) {
						selections.add(newJoin.baseColumn(langDataField, dlvo.getPrimaryKey()));
					}
					if (columns != null) {
						columns.add(ProcessorFactorySingleton.createLanguageFieldMapping(
								SystemFields.BASE_ALIAS, dlvo.getPrimaryKey(), langDataField, false, true));
					}
				}
			}
		}
	}

	@Override
	protected <S> Transformer<Object[], EntityObjectVO<PK>> getResultTransformer(final JdbcTransformerParams params, final IColumnToVOMapping<Object, PK>... columns) {
		final Transformer<Object[], EntityObjectVO<PK>> eoBaseTransformer = super.getResultTransformer(params, columns);
		return new Transformer<Object[], EntityObjectVO<PK>>() {
			@Override
			public EntityObjectVO<PK> transform(Object[] result) {
				final EntityObjectVO<PK> eo = eoBaseTransformer.transform(result);

				for (int i = 0, n = columns.length; i < n; i++) {
					final IColumnToVOMapping<Object, PK> column = columns[i];
					final Object value = result[i];
					if (column instanceof ColumnToLanguageFieldVOMapping) {
						ColumnToLanguageFieldVOMapping col = (ColumnToLanguageFieldVOMapping) column;
						if (col.isForDataMap()) {
							storeLocalizedValuesForField(eo, value, col);
						}
					}
				}

				return eo;
			}

			private void storeLocalizedValuesForField(EntityObjectVO<PK> eo,
													  Object value, ColumnToLanguageFieldVOMapping column) {

				if (MetaProvider.getInstance().hasEntity(eo.getDalEntity())) {
					EntityMeta entityMeta = MetaProvider.getInstance().getEntity(eo.getDalEntity());
					if(eo.getDataLanguageMap() == null) {
						eo.setDataLanguageMap(new DataLanguageMap());
					}

					if (eo.getDataLanguageMap().getDataLanguage(column.getLanguageUID()) == null) {
						Map<UID, Object> mpFieldValues = new HashMap<>();

						Long primKey = null;
						if(SF.PK_ID.getUID(column.getMeta().getEntity()).equals(column.getMeta().getUID())) {
							primKey = (Long) value;
						} else {
							mpFieldValues.put(column.getUID(),value);
						}
						DataLanguageLocalizedEntityEntry newEntry = new DataLanguageLocalizedEntityEntry(new EntityMetaVO<>(entityMeta, true),
								(Long) eo.getPrimaryKey(), column.getLanguageUID(), mpFieldValues);
						newEntry.reset();
						if(primKey != null) {
							newEntry.setPrimaryKey(primKey);
						}
						eo.getDataLanguageMap().setDataLanguage(column.getLanguageUID(), newEntry);
					} else {

						Long primKey = null;
						if(SF.PK_ID.getUID(column.getMeta().getEntity()).equals(column.getMeta().getUID())) {
							eo.getDataLanguageMap().getDataLanguage(column.getLanguageUID()).setPrimaryKey(
									value);
						} else {
							eo.getDataLanguageMap().getDataLanguage(column.getLanguageUID()).
									setFieldValue(column.getUID(), value);
						}

					}

				}
			}

		};
	}

}
