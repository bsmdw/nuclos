package org.nuclos.server.dal.processor.jdbc.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RecordGrantMeta;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collection.JdbcTransformerParams;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.server.common.StateCache;
import org.nuclos.server.dal.processor.ColumnToFieldIdVOMapping;
import org.nuclos.server.dal.processor.IColumnToVOMapping;
import org.nuclos.server.dal.processor.ProcessorConfiguration;
import org.nuclos.server.dblayer.query.DbExpression;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.i18n.LocaleContextHolder;

@Configurable
public class EOWithStateModelProcessor<PK> extends EOWithLangSupportProcessor<PK> {

	protected static final Logger LOG = LoggerFactory.getLogger(EOWithStateModelProcessor.class);

	private final List<IColumnToVOMapping<?, PK>> usageCriteriaColumns;

	@Autowired
	private StateCache stateCache;

	public EOWithStateModelProcessor(final ProcessorConfiguration<PK> config) {
		super(config);
		final EntityMeta<PK> eMeta = config.getMetaData();
		final UID entityUID = eMeta.getUID();
		final FieldMeta<?> processMeta = SF.PROCESS_UID.getMetaData(entityUID);
		final FieldMeta<?> stateMeta = SF.STATE_UID.getMetaData(entityUID);
		try {
			usageCriteriaColumns = Arrays.asList(
					new ColumnToFieldIdVOMapping(SystemFields.BASE_ALIAS, processMeta, true, config.getDataSourceCaseSensivity().isCaseSensitive(processMeta)),
					new ColumnToFieldIdVOMapping(SystemFields.BASE_ALIAS, stateMeta, true, config.getDataSourceCaseSensivity().isCaseSensitive(stateMeta))
			);
		} catch (ClassNotFoundException ex) {
			throw new NuclosFatalException(ex);
		}
	}

	protected <S> Transformer<Object[], EntityObjectVO<PK>> getResultTransformer(final JdbcTransformerParams params, final IColumnToVOMapping<Object, PK>... columns) {
		final Transformer<Object[], EntityObjectVO<PK>> eoBaseTransformer = super.getResultTransformer(params, columns);
		return new Transformer<Object[], EntityObjectVO<PK>>() {
			@Override
			public EntityObjectVO<PK> transform(Object[] result) {
				final EntityObjectVO<PK> eo = eoBaseTransformer.transform(result);
				boolean referencesState = false;
				for (int i = 0, n = columns.length; i < n; i++) {
					final IColumnToVOMapping<Object, PK> column = columns[i];
					// If we want to display the statename we have to merge with locale_resource
					// to retrieve localized values
					if (SF.STATE.getUID(eo.getDalEntity()).equals(column.getUID())){
						referencesState = true;
						break;
					}
				}
				if (referencesState){
					replaceLocalizedResourceValuesForField(eo);
				}
				return eo;
			}

			private void replaceLocalizedResourceValuesForField(EntityObjectVO<PK> eo) {
				final UID stateFieldUID = SF.STATE_UID.getUID(eo.getDalEntity());
				final UID stateId = eo.getFieldUid(stateFieldUID);
				if (stateId != null) {
					String stateLabel = null;
					try {
						StateVO state = stateCache.getStateById(stateId);
						stateLabel = state.getStatename(LocaleContextHolder.getLocale());
					} catch (CommonFinderException e) {
						LOG.error("State with ID {} not found", stateId, e);
						stateLabel = "Status[" + stateId.getString() + "]";
					}
					eo.setFieldValue(stateFieldUID, stateLabel);
				}
			}

		};
	}

	@Override
	public Set<UsageCriteria> getUsageCriteriaBySearchExpression(final CollectableSearchExpression clctexpr) {
		Set<UsageCriteria> result = new HashSet<>();

		final DbQuery<Object[]> query = createQueryImpl(Object[].class);
		final DbFrom<PK> from = query.from(getMetaData(), false);
		adjustFrom(from);

		final List<DbExpression<?>> selection = usageCriteriaColumns.stream().map(column -> column.getDbColumn(from)).collect(Collectors.toList());
		query.multiselect(selection);
		query.groupBy(selection);

		final RecordGrantMeta rgMeta = getAssignedRecordgrantMeta(null);
		addSearchExpressionAndGrantsToQuery(clctexpr, query, rgMeta, null);

		/* Sometimes ordering throws an exception
			SQL query failed with org.h2.jdbc.JdbcSQLException: Column "T.INTID" must be in the GROUP BY list; SQL statement:
				SELECT t.STRUID_NUCLOSPROCESS, t.STRUID_NUCLOSSTATE FROM V594_ORDER AS t WHERE (t.INTID IN (?, ?) AND t.BLNNUCLOSDELETED = ?)
				GROUP BY t.STRUID_NUCLOSPROCESS, t.STRUID_NUCLOSSTATE ORDER BY t.INTID DESC [90016-196]:
		 */
		// ... and ordering is unnecessary:
		query.orderBy(Collections.emptyList());

		result.addAll(dataBaseHelper.getDbAccess().executeQuery(query, new Transformer<Object[], UsageCriteria>() {
			@Override
			public UsageCriteria transform(final Object[] object) {
				return new UsageCriteria(
						getEntityUID(),
						(UID) object[0],
						(UID) object[1],
						null);
			}
		}));

		return result;
	}
}
