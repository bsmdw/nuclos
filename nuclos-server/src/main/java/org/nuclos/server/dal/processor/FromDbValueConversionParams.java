package org.nuclos.server.dal.processor;

/**
 * Created by Sebastian Debring on 4/1/2019.
 */
public class FromDbValueConversionParams {

	private final boolean bIncludeThumbnailsOnly;

	public FromDbValueConversionParams(final boolean bIncludeThumbnailsOnly) {
		this.bIncludeThumbnailsOnly = bIncludeThumbnailsOnly;
	}

	public boolean includeThumbnailsOnly() {
		return bIncludeThumbnailsOnly;
	}
}
