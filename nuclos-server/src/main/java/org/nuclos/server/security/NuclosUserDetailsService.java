//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.security;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.nuclos.common.E;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.NuclosUsernameNotFoundException;
import org.nuclos.server.autosync.XMLEntities;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbAccess;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.expression.DbCurrentDateTime;
import org.nuclos.server.dblayer.query.DbExpression;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.statements.DbMap;
import org.nuclos.server.dblayer.statements.DbUpdateStatement;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeHelper;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
public class NuclosUserDetailsService implements org.nuclos.server.security.UserDetailsService {

	private static final Logger LOG = LoggerFactory.getLogger(MasterDataFacadeHelper.class);

	private ParameterProvider paramprovider;
	
	private SpringDataBaseHelper dataBaseHelper;
	
	NuclosUserDetailsService() {
	}
	
	@Autowired
	void setDataBaseHelper(SpringDataBaseHelper dataBaseHelper) {
		this.dataBaseHelper = dataBaseHelper;
	}

	public void setParameterProvider(ParameterProvider paramprovider) {
		this.paramprovider = paramprovider;
	}

	public static List<DbTuple> loadUsersDataByLoginFromDb(String login, DbAccess dbAccess) {
		DbQueryBuilder builder = dbAccess.getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom<UID> t = query.from(E.USER);
		query.multiselect(
				t.basePk(),
				t.baseColumn(E.USER.password),
				t.baseColumn(E.USER.superuser),
				t.baseColumn(E.USER.lastLogin),
				t.baseColumn(E.USER.locked),
				t.baseColumn(E.USER.expirationDate),
				t.baseColumn(E.USER.lastPasswordChange),
				t.baseColumn(E.USER.passwordChangeRequired),
				t.baseColumn(E.USER.username));

		DbExpression<String> upperLogin = builder.upper(builder.literal(login));
		query.where(
				builder.or(
						builder.equal(builder.upper(t.baseColumn(E.USER.username)), upperLogin),
						builder.and(
								builder.equalValue(t.baseColumn(E.USER.loginWithEmailAllowed), true),
								builder.equal(builder.upper(t.baseColumn(E.USER.email)), upperLogin)
						)
				));

		return dbAccess.executeQuery(query);
	}

	public List<DbTuple> loadUsersDataByLoginFromDb(String login) {
		return loadUsersDataByLoginFromDb(login, dataBaseHelper.getDbAccess());
	}

	/**
	 * Load user including roles (actions) from nuclos database
	 * (T_MD_USER, T_MD_ROLE_USER, T_MD_ROLE, T_MD_ROLE_ACTION, T_AD_ACTION) and JSON.
	 */
	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException, DataAccessException {
		final DbTuple tuple = CollectionUtils.getFirst(loadUsersDataByLoginFromDb(login));
		final UID uid;
		final Boolean isSuperUser;
		final String password;
		final Date lastlogin;
		Boolean locked;
		final Date expiration;
		final Date passwordchanged;
		final Boolean requirechange;
		final String user;

		if (tuple != null) {
			uid = tuple.get(0, UID.class);
			password = tuple.get(1, String.class);
			isSuperUser = Boolean.TRUE.equals(tuple.get(2, Boolean.class));
			lastlogin = tuple.get(3, Date.class);
			locked = Boolean.TRUE.equals(tuple.get(4, Boolean.class));
			expiration = tuple.get(5, Date.class);
			passwordchanged = tuple.get(6, Date.class);
			requirechange = Boolean.TRUE.equals(tuple.get(7, Boolean.class));
			user = tuple.get(8, String.class);
		} else {
			throw new NuclosUsernameNotFoundException("User " + login + " not found");
		}

		Boolean expired = false;
		if (expiration != null) {
			Calendar c = Calendar.getInstance();
			c.setTime(expiration);
			expired = c.before(Calendar.getInstance());
		}

		String lockDays = paramprovider.getValue(ParameterProvider.KEY_SECURITY_LOCK_DAYS);
		if (!locked && lastlogin != null && !StringUtils.isNullOrEmpty(lockDays)) {
			try {
				Integer days = Integer.parseInt(lockDays);
				Calendar c = Calendar.getInstance();
				c.setTime(lastlogin);
				c.add(Calendar.DAY_OF_MONTH, days);
				if (Calendar.getInstance().after(c)) {
					lockUser(uid);
					locked = true;
				}
			}
			catch (NumberFormatException ex) {
				LOG.error("Cannot parse parameter value for key {}",
				          ParameterProvider.KEY_SECURITY_LOCK_DAYS, ex);
			}
		}

		Boolean credentialsExpired = false;
		String passwordInterval = paramprovider.getValue(ParameterProvider.KEY_SECURITY_PASSWORD_INTERVAL);
		if (!StringUtils.isNullOrEmpty(passwordInterval)) {
			if (passwordchanged != null) {
				try {
					Integer days = Integer.parseInt(passwordInterval);
					Calendar c = Calendar.getInstance();
					c.setTime(passwordchanged);
					c.add(Calendar.DAY_OF_MONTH, days);
					credentialsExpired = c.before(Calendar.getInstance());
				}
				catch (NumberFormatException ex) {
					LOG.error("Cannot parse parameter value for key {}",
					          ParameterProvider.KEY_SECURITY_PASSWORD_INTERVAL, ex);
				}
			}
			else {
				setPasswordChanged(uid);
			}
		}

		Set<String> actions;

		if (isSuperUser) {
			actions = new HashSet<>(getSuperUserActions());
		}
		else {
			actions = new HashSet<>(SecurityCache.getInstance().getAllowedActions(login, null));
		}
		actions.add("real.nuclos.username=" + user);

		List<GrantedAuthority> authorities = CollectionUtils.transform(actions, new Transformer<String, GrantedAuthority>() {
			@Override
			public GrantedAuthority transform(String i) {
				return new SimpleGrantedAuthority(i);
			}
		});

		return new User(user, password == null ? "" : password, true, !expired, !credentialsExpired && !requirechange, !locked, authorities);
	}

	@Override
	public void logAttempt(String login, boolean authenticated) {
		Integer maxattempts = 0;
		String sMaxattempts = paramprovider.getValue(ParameterProvider.KEY_SECURITY_LOCK_ATTEMPTS);
		if (!StringUtils.isNullOrEmpty(sMaxattempts)) {
			try {
				maxattempts = Integer.parseInt(sMaxattempts);
			}
			catch (NumberFormatException ex) {
				LOG.error("Cannot parse parameter value for key {}",
				          ParameterProvider.KEY_SECURITY_LOCK_ATTEMPTS, ex);
				return;
			}
		}

		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom<UID> t = query.from(E.USER);
		query.multiselect(t.basePk(), t.baseColumn(E.USER.loginAttempts));

		DbExpression<String> upperLogin = builder.upper(builder.literal(login));
		query.where(
				builder.or(
						builder.equal(builder.upper(t.baseColumn(E.USER.username)), upperLogin),
						builder.and(
								builder.equalValue(t.baseColumn(E.USER.loginWithEmailAllowed), true),
								builder.equal(builder.upper(t.baseColumn(E.USER.email)), upperLogin)
						)
				));

		List<DbTuple> tuples = dataBaseHelper.getDbAccess().executeQuery(query);

		if (tuples.isEmpty()) {
			throw new NuclosUsernameNotFoundException("User " + login + " not found");
		}

		DbTuple tuple = tuples.get(0);

		UID uid = tuple.get(0, UID.class);
		Integer loginattempts = tuple.get(1, Integer.class);

		final DbMap values = new DbMap(2);
		if (!authenticated) {
			if (loginattempts == null) {
				loginattempts = 1;
			}
			else {
				loginattempts++;
			}
			values.put(E.USER.loginAttempts, loginattempts);
			if (maxattempts > 0 && loginattempts.compareTo(maxattempts) >= 0) {
				values.put(E.USER.locked, true);
			}
		}
		else {
			values.put(E.USER.loginAttempts, 0);
			values.putUnsafe(E.USER.lastLogin, DbCurrentDateTime.CURRENT_DATETIME);
		}
		final DbMap conditions = new DbMap(1);
		// conditions.put("INTID", id);
		conditions.put(E.USER.getPk(), uid);
		dataBaseHelper.getDbAccess().execute(new DbUpdateStatement<UID>(E.USER, values, conditions));
	}

	static Set<String> getSuperUserActions() {
		DbQueryBuilder builder = SpringDataBaseHelper.getInstance().getDbAccess().getQueryBuilder();
		Set<String> actions = new HashSet<String>();
		DbQuery<String> rolesQuery = builder.createQuery(String.class);
		DbFrom<UID> action = rolesQuery.from(E.ACTION);
		rolesQuery.select(action.baseColumn(E.ACTION.name));
		actions.addAll(SpringDataBaseHelper.getInstance().getDbAccess().executeQuery(rolesQuery));

		for(MasterDataVO<UID> mdvo : XMLEntities.getData(E.ACTION).getAll()) {
			actions.add(mdvo.getFieldValue(E.ACTION.name));
		}

		return actions;
	}

	private void lockUser(UID uid) {
		final DbMap values = new DbMap(1);
		values.put(E.USER.locked, Boolean.TRUE);
		final DbMap conditions = new DbMap(1);
		// conditions.put("INTID", user);
		conditions.put(E.USER.getPk(), uid);
		dataBaseHelper.getDbAccess().execute(new DbUpdateStatement<UID>(E.USER, values, conditions));
	}

	private void setPasswordChanged(UID uid) {
		final DbMap values = new DbMap(1);
		values.put(E.USER.lastPasswordChange, Calendar.getInstance().getTime());
		final DbMap conditions = new DbMap(1);
		// conditions.put("INTID", user);
		conditions.put(E.USER.getPk(), uid);
		dataBaseHelper.getDbAccess().execute(new DbUpdateStatement<UID>(E.USER, values, conditions));
	}
}
