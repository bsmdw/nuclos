//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.resource.ejb3;

import java.io.File;
import java.util.Set;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.E;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.resource.ResourceCache;
import org.nuclos.server.resource.valueobject.ResourceFile;
import org.nuclos.server.resource.valueobject.ResourceVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Resource facade bean.
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Transactional(noRollbackFor= {Exception.class})
public class ResourceFacadeBean extends NuclosFacadeBean implements ResourceFacadeLocal, ResourceFacadeRemote {
	
	@Autowired
	private ResourceCache resourceCache;
	
	@Autowired
	private MasterDataFacadeLocal masterDataFacade;
	
	public ResourceFacadeBean() {
	}
	
    @RolesAllowed("Login")
	public ResourceVO getResource(UID uidResourceName) {
		return resourceCache.getResource(uidResourceName);
	}

    @RolesAllowed("Login")
	public Pair<ResourceVO, byte[]> getResourceBytes(UID resourceUid) {
		Pair<ResourceVO, byte[]> res = new Pair<ResourceVO, byte[]>();
		res.x = resourceCache.getResource(resourceUid);
		res.y = resourceCache.getResourceBytes(resourceUid);
		return res;
	}
	

	@Override
	public MasterDataVO<UID> create(UID sEntityName, MasterDataVO<UID> mdvo, IDependentDataMap mpDependants)
			throws CommonCreateException, CommonPermissionException, NuclosBusinessRuleException {

		final ResourceFile resourceFile = (ResourceFile)mdvo.getFieldValue(E.RESOURCE.file);
		if (resourceFile != null && resourceFile.getContents() != null ) {
			mdvo.setFieldValue(E.RESOURCE.content, resourceFile.getContents());
		}
		mdvo.setDependents(mpDependants);
		
		return this.masterDataFacade.create(mdvo, null);
	}

	@Override
	public Object modify(UID sEntityName, MasterDataVO<UID> mdvo, IDependentDataMap mpDependants)
			throws CommonCreateException, CommonFinderException, CommonRemoveException, CommonStaleVersionException,
				CommonValidationException, CommonPermissionException, NuclosBusinessRuleException {

		final ResourceFile resourceFile = (ResourceFile)mdvo.getFieldValue(E.RESOURCE.file);
		if (resourceFile != null && resourceFile.getContents() != null ) {
			mdvo.setFieldValue(E.RESOURCE.content, resourceFile.getContents());
		}
		mdvo.setDependents(mpDependants);
		
		return this.masterDataFacade.modify(mdvo, null);
	}

	@Override
    public void remove(UID sEntityName, MasterDataVO<UID> mdvo)throws CommonFinderException, CommonRemoveException,
			CommonStaleVersionException, CommonPermissionException, CommonCreateException, NuclosBusinessRuleException {
		
			masterDataFacade.remove(mdvo.getEntityObject().getDalEntity(), mdvo.getPrimaryKey(), true, null);
	}


	private static String getResourcePathName(ResourceFile resourceFile) {
		try {
			if(resourceFile == null) {
				throw new CommonFatalException("resource.error.invalid.file");//"Der Parameter resourceFile darf nicht null sein.");
			}
			// @todo introduce symbolic constant
			File resourceDir = NuclosSystemParameters.getDirectory(NuclosSystemParameters.RESOURCE_PATH);
			if (resourceDir == null) {
				throw new CommonFatalException("resource.error.invalid.path");//"Der Paramater 'Resource Path' darf nicht null sein");
			}

			java.io.File file = new java.io.File(resourceDir, resourceFile.getFilename());
			LOG.debug("Calculated path for resource: {}", file.getCanonicalPath());
			return file.getCanonicalPath();
		}
		catch (java.io.IOException e) {
			throw new NuclosFatalException(e);
		}
	}

	/**
	 * get the file content of a resource file
	 * @param resourceUID
	 * @return resource file content
	 * @throws CommonFinderException
	 */
    @RolesAllowed("Login")
	public byte[] loadResource(UID resourceUID) throws CommonFinderException {
		if (resourceUID == null) {
			throw new NuclosFatalException("resource.error.invalid.id");//"Die Id der Ressource darf nicht null sein");
		}
		
		try {
			MasterDataVO voResource = masterDataFacade.get(E.RESOURCE, resourceUID);
			if(voResource.getFieldValue(E.RESOURCE.content) != null) {
				return (byte[]) voResource.getFieldValue(E.RESOURCE.content);
			}
		} catch (CommonPermissionException e) {
			throw new CommonFatalException(e);
		}
		
	
		return ResourceCache.getInstance().getResourceBytes(resourceUID);
	}

	public Set<UID> getResourceUIDs() {
		return resourceCache.getResourceUIDs();
	}

}
