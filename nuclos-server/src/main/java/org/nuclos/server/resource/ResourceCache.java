//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.resource;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.nuclos.common.E;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RigidFile;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Pair;
import org.nuclos.common2.IOUtils;
import org.nuclos.server.cluster.cache.ClusterCache;
import org.nuclos.server.cluster.jms.ClusterActionFactory;
import org.nuclos.server.cluster.jms.NuclosClusterAction;
import org.nuclos.server.common.valueobject.DocumentFileBase;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.nuclos.server.resource.valueobject.ResourceVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * A (server) cache for resources.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:corina.mandoki@novabit.de">Corina Mandoki</a>
 * @version 00.01.000
 */
@Component
public class ResourceCache implements ClusterCache {
	
	private static final Logger LOG = LoggerFactory.getLogger(ResourceCache.class);
	
	private Map<UID, ResourceVO> mpResourcesByUid;
	
	private final Map<Pair<UID, String>, UID> mpResourceNucletFiles = new ConcurrentHashMap<Pair<UID, String>, UID>();
	
	private final Map<UID, File> mpResourceFiles = new ConcurrentHashMap<UID, File>();
	
	//private final ClientNotifier clientnotifier = new ClientNotifier(JMSConstants.TOPICNAME_RESOURCECACHE);
	
//	private static final File resourceDir = NuclosSystemParameters.getDirectory(NuclosSystemParameters.RESOURCE_PATH);
	
	private static ResourceCache INSTANCE;
	
	//
	
	private final Map<ResourceVO, byte[]> mpResources = new ConcurrentHashMap<ResourceVO, byte[]>();
	
	private final Object lock = new Object();
	
	private boolean loaded = false;
	
	private SpringDataBaseHelper dataBaseHelper;
	
	public static ResourceCache getInstance() {
		return INSTANCE;
	}
	
	ResourceCache() {
		INSTANCE = this;
	}
	
	@Autowired
	void setDataBaseHelper(SpringDataBaseHelper dataBaseHelper) {
		this.dataBaseHelper = dataBaseHelper;
	}
	
	@PostConstruct
	final void init() {
		synchronized (lock) {
			this.mpResourcesByUid = buildMap();
			loaded = true;
		}
	}
	
	/**
	 * init the map
	 */
	private Map<UID, ResourceVO> buildMap() {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom t = query.from(E.RESOURCE);
		query.multiselect(
			t.baseColumn(E.RESOURCE.getPk()),
			t.baseColumn(SF.CREATEDAT),
			t.baseColumn(SF.CREATEDBY),
			t.baseColumn(SF.CHANGEDAT),
			t.baseColumn(SF.CHANGEDBY),
			t.baseColumn(SF.VERSION),
			t.baseColumn(E.RESOURCE.name),
			t.baseColumn(E.RESOURCE.description),
			t.baseColumn(E.RESOURCE.file),
			t.baseColumn(E.RESOURCE.systemresource),
			t.baseColumn(E.RESOURCE.content));

		final Map<UID, ResourceVO> result = CollectionUtils.newHashMap();
		for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query)) {
			DocumentFileBase file = tuple.get(8, DocumentFileBase.class);
			ResourceVO resourcevo = new ResourceVO(
				tuple.get(0, UID.class),
				tuple.get(1, Date.class),
				tuple.get(2, String.class),
				tuple.get(3, Date.class),
				tuple.get(4, String.class),
				tuple.get(5, Integer.class),
				tuple.get(6, String.class),
				tuple.get(7, String.class),
				file.getFilename(),
				tuple.get(9, Boolean.class),
				tuple.get(10, byte[].class));
			result.put(resourcevo.getPrimaryKey(), resourcevo);
		}
		return result;
	}
	
	public ResourceVO getResource(UID resourceUid) {
		synchronized (lock) {
			if (!loaded) {
				mpResourcesByUid = buildMap();
				loaded = true;
			}
		}
		return mpResourcesByUid.get(resourceUid);
	}
	
	/**
	 * @param resourceUid
	 * @return the resource as <code>byte[]</code>
	 */
	public byte[] getResourceBytes(UID resourceUid) {
		synchronized (lock) {
			if (!loaded) {
				mpResourcesByUid = buildMap();
				loaded = true;
			}
		}
		ResourceVO resourcevo = mpResourcesByUid.get(resourceUid);
		if (resourcevo == null) {
			return null;
		}
		if (!mpResources.containsKey(resourcevo)) {
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<byte[]> query = builder.createQuery(byte[].class);
			DbFrom<UID> t = query.from(E.RESOURCE);
			query.select(t.baseColumn(E.RESOURCE.content));
			query.where(builder.equalValue(t.baseColumn(E.RESOURCE.getPk()), resourceUid));
			
			byte[] content = dataBaseHelper.getDbAccess().executeQuerySingleResult(query);
			mpResources.put(resourcevo, content);			
		}
		return mpResources.get(resourcevo);		
	}
	
	/**
	 * 
	 * @param resourceUid
	 * @return file from temp dir
	 */
	public File getResourceFile(UID resourceUid) throws IOException {
		if (resourceUid == null) {
			throw new NuclosFatalException("resourceUid must not be null");
		}
		File result = mpResourceFiles.get(resourceUid);
		
		if (result == null || !result.exists()) {
			byte[] content = getResourceBytes(resourceUid);
			ResourceVO resVO = getResource(resourceUid);
			
			if (resVO == null) {
				throw new NuclosFatalException("Resource not found: " + resourceUid.getString());
			}
			final String filename = resourceUid.getString() + "-" + resVO.getFileName();
			final File file = File.createTempFile(filename, "");
			IOUtils.writeToBinaryFile(file, content);
		
			file.deleteOnExit();	
			mpResourceFiles.put(resourceUid, file);
			result = file;
		}
		return result;
	}
	
	/**
	 * 
	 * @param nucletUID (could be null)
	 * @param filename
	 * @return UID from resource (could be null)
	 */
	public UID getResourceUID(UID nucletUID, String filename) {
		if (filename == null) {
			throw new NuclosFatalException("filename must not be null");
		}
		Pair<UID, String> key = new Pair<UID, String>(nucletUID, filename);
		UID result = mpResourceNucletFiles.get(key);
		
		if (result == null) {
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<UID> query = builder.createQuery(UID.class);
			DbFrom<UID> t = query.from(E.RESOURCE);
			query.select(t.baseColumn(E.RESOURCE));
			query.where(builder.equalValue(t.baseColumn(E.RESOURCE.file), new RigidFile(filename, null)));
			if (nucletUID == null) {
				query.addToWhereAsAnd(builder.isNull(t.baseColumn(E.RESOURCE.nuclet)));
			} else {
				query.addToWhereAsAnd(builder.equalValue(t.baseColumn(E.RESOURCE.nuclet), nucletUID));
			}
			
			UID uid = dataBaseHelper.getDbAccess().executeQuerySingleResult(query);
			mpResourceNucletFiles.put(key, uid);			
			result = uid;
		}
		
		return result;
	}
	
	/**
	 * 
	 * @param nucletUID (could be null)
	 * @param filename
	 * @return file from temp dir
	 * @throws IOException 
	 * @throws NuclosFatalException if resource not found
	 */
	public File getResourceFile(UID nucletUID, String filename) throws IOException {
		UID resourceUID = getResourceUID(nucletUID, filename);
		if (resourceUID == null) {
			throw new NuclosFatalException(String.format("Resource file \"%s\" in nuclet with UID %s does not exist", filename, nucletUID==null?"<NULL>":nucletUID.getString()));
		}
		
		return getResourceFile(resourceUID);
	}
	
	public Set<UID> getResourceUIDs() {
		synchronized (lock) {
			if (!loaded) {
				mpResourcesByUid = buildMap();
				loaded = true;
			}
		}
		return new HashSet<UID>(mpResourcesByUid.keySet());
	}
	
	private void notifyClients(boolean bAfterCommit) {
		LOG.info("JMS send: notify clients that resources changed: {}", this);

		if(bAfterCommit) {
			NuclosJMSUtils.sendOnceAfterCommitDelayed(null, JMSConstants.TOPICNAME_RESOURCECACHE);
		}
		else {
			NuclosJMSUtils.sendMessage(null, JMSConstants.TOPICNAME_RESOURCECACHE, null);
		}
	}

	@Override
	public void invalidateCache(boolean notifyClients, boolean notifyClusterCloud) {
		synchronized (lock) {
			this.mpResourcesByUid.clear();
			this.mpResources.clear();
			this.mpResourceNucletFiles.clear();
			for (File file : mpResourceFiles.values()) {
				if (file.exists()) {
					file.delete();
				}
			}
			mpResourceFiles.clear();
			loaded = false;
		}
		if(notifyClients) {
			this.notifyClients(notifyClusterCloud);
		}
		if(notifyClusterCloud) {
			this.notifyClusterCloud();
		}
	}

	@Override
	public void notifyClusterCloud() {
		NuclosClusterAction action = ClusterActionFactory.createClusterAction(NuclosClusterAction.RESOURCE_ACTION);
		NuclosJMSUtils.sendObjectMessage(action, JMSConstants.TOPICNAME_CLUSTER, null);		
	}

	@Override
	public void registerCache() {
	}

	@Override
	public void deregisterCache() {
	}

	@Override
	public String getName() {
		return null;
	}
}
