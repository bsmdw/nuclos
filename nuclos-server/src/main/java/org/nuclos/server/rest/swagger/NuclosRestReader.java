package org.nuclos.server.rest.swagger;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;

import org.apache.commons.lang.StringUtils;
import org.nuclos.api.rule.CustomRestRule;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.server.rest.NuclosRestApplication;
import org.nuclos.server.rest.services.CustomRestComponent;
import org.nuclos.server.rest.services.helper.RestServiceInfo;

import com.fasterxml.jackson.annotation.JsonView;

import io.swagger.v3.core.converter.AnnotatedType;
import io.swagger.v3.core.converter.ModelConverters;
import io.swagger.v3.core.converter.ResolvedSchema;
import io.swagger.v3.core.util.AnnotationsUtils;
import io.swagger.v3.core.util.ReflectionUtils;
import io.swagger.v3.jaxrs2.Reader;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.media.Content;
import io.swagger.v3.oas.models.media.MediaType;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.parameters.Parameter;
import io.swagger.v3.oas.models.parameters.RequestBody;
import io.swagger.v3.oas.models.responses.ApiResponses;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.servers.Server;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class NuclosRestReader extends Reader {

	@Override
	public OpenAPI read(final Set<Class<?>> classes) {
		OpenAPI api = super.read(classes);


		if (SpringApplicationContextHolder.isNuclosReady()) {
			CustomRestComponent customRest = SpringApplicationContextHolder.getBean(CustomRestComponent.class);
			Set<CustomRestComponent.EndpointInfo> customRestRules = customRest.getAllowedCustomRestRules();

			api = addOperationsManually(api, customRestRules);
		}

		return api;
	}

	/**
	 * Certain operation (CustomRestRules) must be added manually to the generated
	 * OpenAPI, because they are missing @Path annotations and will therefore not be
	 * scanned by Swagger.
	 */
	private OpenAPI addOperationsManually(
			OpenAPI api,
			final Set<CustomRestComponent.EndpointInfo> customRestRules
	) {
		for (CustomRestComponent.EndpointInfo info : customRestRules) {

			Class<? extends CustomRestRule> cls = info.getCustomRestClass();
			Method method = info.getCustomRestMethod();

			final String path = "/rest/execute/" + info.getCustomRestClass().getName() + "/" + method.getName();

			Operation operation = getOperation(cls, method);
			addResponses(method, operation);

			if (info.getMethodType() == GET.class) {
				api = api.path(path, new PathItem().get(operation));
			} else if (info.getMethodType() == PUT.class) {
				api = api.path(path, new PathItem().put(operation));
			} else if (info.getMethodType() == POST.class) {
				api = api.path(path, new PathItem().post(operation));
			} else if (info.getMethodType() == DELETE.class) {
				api = api.path(path, new PathItem().delete(operation));
			}
		}

		return api;
	}

	private Operation getOperation(final Class<? extends CustomRestRule> cls, final Method method) {
		Operation result = new Operation().operationId(cls.getName() + "." + method.getName());

		io.swagger.v3.oas.annotations.Operation operation = ReflectionUtils.getAnnotation(method, io.swagger.v3.oas.annotations.Operation.class);
		if (operation != null) {
			result.deprecated(operation.deprecated())
					.description(operation.description())
					.externalDocs(SwaggerAnnotationParser.parseExternalDocumentation(operation.externalDocs()))
					.parameters(SwaggerAnnotationParser.parseParameters(operation.parameters()))
					.requestBody(SwaggerAnnotationParser.parseRequestBody(operation.requestBody()))
					.responses(SwaggerAnnotationParser.parseResponses(operation.responses()))
					.security(SwaggerAnnotationParser.parseSecurity(operation.security()))
					.servers(SwaggerAnnotationParser.parseServers(operation.servers()))
					.summary(operation.summary())
					.tags(Arrays.asList(operation.tags()));
		}

		return result;
	}


	/**
	 * Adds ApiResponses based on the method return type.
	 * Code is mostly taken (with some adjustments) from io.swagger.v3.jaxrs2.Reader#parseMethod
	 */
	private void addResponses(final Method method, final Operation operation) {
		Type returnType = method.getGenericReturnType();

		if (returnType != null && !Void.TYPE.equals(returnType)) {
			ResolvedSchema resolvedSchema = ModelConverters.getInstance().resolveAsResolvedSchema(new AnnotatedType(returnType).resolveAsRef(true));
			if (resolvedSchema.schema != null) {
				Schema returnTypeSchema = resolvedSchema.schema;
				Content content = new Content();
				MediaType mediaType = new MediaType().schema(returnTypeSchema);
				AnnotationsUtils.applyTypes(new String[0], new String[0], content, mediaType);
				if (operation.getResponses() == null) {
					operation.responses(
							new ApiResponses()._default(
									new io.swagger.v3.oas.models.responses.ApiResponse().description(DEFAULT_DESCRIPTION)
											.content(content)
							)
					);
				}
				if (operation.getResponses().getDefault() != null &&
						org.apache.commons.lang3.StringUtils.isBlank(operation.getResponses().getDefault().get$ref())) {
					if (operation.getResponses().getDefault().getContent() == null) {
						operation.getResponses().getDefault().content(content);
					} else {
						for (String key : operation.getResponses().getDefault().getContent().keySet()) {
							if (operation.getResponses().getDefault().getContent().get(key).getSchema() == null) {
								operation.getResponses().getDefault().getContent().get(key).setSchema(returnTypeSchema);
							}
						}
					}
				}
			}
		}

		if (operation.getResponses() == null || operation.getResponses().isEmpty()) {
			Content content = new Content();
			MediaType mediaType = new MediaType();
			AnnotationsUtils.applyTypes(new String[0], new String[0], content, mediaType);

			io.swagger.v3.oas.models.responses.ApiResponse apiResponseObject = new io.swagger.v3.oas.models.responses.ApiResponse().description(DEFAULT_DESCRIPTION).content(content);
			operation.setResponses(new ApiResponses()._default(apiResponseObject));
		}
	}

	@Override
	public Operation parseMethod(
			final Method method,
			final List<Parameter> globalParameters,
			final Produces methodProduces,
			final Produces classProduces,
			final Consumes methodConsumes,
			final Consumes classConsumes,
			final List<SecurityRequirement> classSecurityRequirements,
			final Optional<ExternalDocumentation> classExternalDocs,
			final Set<String> classTags,
			final List<Server> classServers,
			final boolean isSubresource,
			final RequestBody parentRequestBody,
			final ApiResponses parentResponses,
			final JsonView jsonViewAnnotation,
			final ApiResponse[] classResponses
	) {
		final Operation result = super.parseMethod(
				method,
				globalParameters,
				methodProduces,
				classProduces,
				methodConsumes,
				classConsumes,
				classSecurityRequirements,
				classExternalDocs,
				classTags,
				classServers,
				isSubresource,
				parentRequestBody,
				parentResponses,
				jsonViewAnnotation,
				classResponses
		);

		if (StringUtils.isBlank(result.getSummary())) {
			final RestServiceInfo info = method.getAnnotation(RestServiceInfo.class);
			if (info != null) {
				final String description = info.description();
				if (StringUtils.isNotBlank(description)) {
					result.setSummary(description);
				}
			}
		}

		if (method.getReturnType() == void.class) {
			result.getResponses().getDefault().setContent(null);
		}

		if (method.getAnnotation(Deprecated.class) != null) {
			result.setDeprecated(true);
		}

		if (NuclosRestApplication.isSessionValidationEnabled(null, method)) {
			result.addSecurityItem(new SecurityRequirement().addList("cookieAuth"));

			result.getResponses().addApiResponse(
					"401",
					new io.swagger.v3.oas.models.responses.ApiResponse()
							.description("Unauthorized")
			);
			// TODO: The sessionId header does not work yet, see NUCLOS-7786
			//		Uncomment the following line, when it is fixed
//			result.addSecurityItem(new SecurityRequirement().addList("headerAuth"));
		}

		return result;
	}
}
