//Copyright (C) 2016  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.services;

import java.io.IOException;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.E;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.WorkspaceVO;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.resource.ResourceResolver;
import org.nuclos.common2.searchfilter.EntitySearchFilter2;
import org.nuclos.schema.rest.MenuEntry;
import org.nuclos.schema.rest.ProcessLayoutLinks;
import org.nuclos.schema.rest.RestSystemparameters;
import org.nuclos.schema.rest.SearchfilterEntityMeta;
import org.nuclos.schema.rest.SearchfilterInfo;
import org.nuclos.schema.rest.SideviewMenuSelector;
import org.nuclos.schema.rest.TaskEntityMeta;
import org.nuclos.schema.rest.TaskMenuEntry;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.StateCache;
import org.nuclos.server.common.ejb3.PreferencesFacadeLocal;
import org.nuclos.server.common.mail.NuclosMailServiceProvider;
import org.nuclos.server.rest.CacheableResponseBuilder;
import org.nuclos.server.rest.NuclosRestApplication;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.services.helper.DataServiceHelper;
import org.nuclos.server.rest.services.helper.MetaDataServiceHelper;
import org.nuclos.server.rest.services.helper.RestServiceInfo;
import org.nuclos.server.rest.services.helper.TableViewLayout;
import org.nuclos.server.rest.services.rvo.JsonFactory;
import org.nuclos.server.rest.services.rvo.MenuTreeNodeRVO;
import org.nuclos.server.rest.services.rvo.WebAddonUsageRVO;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.annotation.JsonGetter;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Path("/meta")
@Produces(MediaType.APPLICATION_JSON)
public class MetaDataService extends MetaDataServiceHelper {

	private static final Logger LOG = LoggerFactory.getLogger(MetaDataService.class);

	public static final int RESOURCE_ICON_MAX_AGE_IN_SECDONS = 60 * 60 * 1;

	private DataServiceHelper dataServiceHelper;

	@Autowired
	JsonFactory jsonFactory;

	@Autowired
	private NuclosMailServiceProvider mailServiceProvider;

	@Autowired
	private StateCache stateCache;

	@GET
	@Path("/menu")
	@RestServiceInfo(identifier = "menu", description = "List of the menu-items. Includes all Businessobject-metas")
	@Operation(
			responses = @ApiResponse(
					content = @Content(
							array = @ArraySchema(
									schema = @Schema(
											implementation = MenuEntry.class
									)
							)
					)
			)
	)
	public JsonArray menu() {
		return getMenu().build();
	}

	@GET
	@Path("/menustructure")
	@RestServiceInfo(identifier = "menustructure", description = "Menu-item structure. Includes all Businessobject-metas")
	@Operation(
			responses = @ApiResponse(
					content = @Content(
							array = @ArraySchema(
									schema = @Schema(
											implementation = MenuEntry.class
									)
							)
					)
			)
	)
	public MenuTreeNodeRVO menustructure() {
		return getMenuStructure();
	}

	@PUT
	@Path("/togglemenuitem/{menuitemname}")
	@RestServiceInfo(identifier = "togglemenuitem", description = "Toggle the menu items below given menu item name.")
	@Produces(MediaType.WILDCARD)	// Does not produce any content
	public void collapseMenuItem(
			@PathParam("menuitemname") String menuitemname,
			final boolean expand
	) {
		toggleMenu(menuitemname, expand);
	}

	@GET
	@Path("/layout/{layoutId}")
	@RestServiceInfo(identifier = "layout", description = "Parsed layout for the corresponding layout-id in JSON format")
	public JsonObject layout(@PathParam("layoutId") String layoutId) {
		return getLayout(layoutId).build();
	}

	//TODO: TO BE REFACTURED

	/**
	 * @return Tasks by menu path (not a flat list)
	 */
	@GET
	@Path("/tasks")
	@RestServiceInfo(identifier = "tasks", description = "Task-List from searchfilters and dynamic tasklists.")
	@Operation(
			responses = @ApiResponse(
					content = @Content(
							array = @ArraySchema(
									schema = @Schema(
											implementation = TaskMenuEntry.class
									)
							)
					)
			)
	)
	public JsonArray tasks() {
		return getTasks().build();
	}

	/**
	 * @return A flat list of all dynamic and searchfilter based task lists
	 *
	 * TODO: Result is not structured by menu path, the individual tasklists should therefore have a corresponding attribute
	 */
	@GET
	@Path("/tasklists")
	@RestServiceInfo(identifier = "tasklists", description = "List of all searchfilter based and dynamic task lists for the current user.")
	public List<TaskEntityMeta> tasklists() {
		return getTaskLists();
	}

	@GET
	@Path("/processlayout/{processId}/{boMetaId}")
	@RestServiceInfo(description = "Layout-Link for a particular process of a new BO.")
	@Operation(
			responses = @ApiResponse(
					content = @Content(
							schema = @Schema(
									implementation = ProcessLayoutLinks.class
							)
					)
			)
	)
	public JsonValue processlayout(
			@PathParam("processId") String processId,
			@PathParam("boMetaId") String boMetaId
	) {
		return getProcessLayout(processId, boMetaId, null).build();
	}

	@GET
	@Path("/processlayout/{processId}/{boMetaId}/{boId}")
	@RestServiceInfo(description = "Layout-Link for a particular process of an existing BO.")
	@Operation(
			responses = @ApiResponse(
					content = @Content(
							schema = @Schema(
									implementation = ProcessLayoutLinks.class
							)
					)
			)
	)
	public JsonValue processlayout(
			@PathParam("processId") String processId,
			@PathParam("boMetaId") String boMetaId,
			@PathParam("boId") String boId
	) {
		return getProcessLayout(processId, boMetaId, boId).build();
	}

	/**
	 * @deprecated column layout is stored in preferences
	 */
	@Deprecated
	@GET
	@Path("/tableviewlayout/{parentUid}/{dependenceUid}")
	@RestServiceInfo(description = "Layout information for the webclient table view.")
	public JsonValue tableviewlayout(@PathParam("parentUid") String parentUid, @PathParam("dependenceUid") String dependenceUid) {
		checkPermission(E.ENTITY, parentUid);

		TableViewLayout tableViewLayout = getDataServiceHelper().getTableViewLayout(Rest.translateFqn(E.ENTITY, parentUid), Rest.translateFqn(E.ENTITY, dependenceUid));
		return jsonFactory.buildJsonObject(tableViewLayout, this).build();
	}

	/**
	 * @deprecated column layout is stored in preferences
	 */
	@Deprecated
	@GET
	@Path("/tableviewlayout/{uid}")
	@RestServiceInfo(description = "Layout information for the webclient table view.")
	public JsonValue tableviewlayout(@PathParam("uid") String uid) {
		checkPermission(E.ENTITY, uid);

		TableViewLayout tableViewLayout = getDataServiceHelper().getTableViewLayout(Rest.translateFqn(E.ENTITY, uid));
		return jsonFactory.buildJsonObject(tableViewLayout, this).build();
	}

	/**
	 * @deprecated Replaced by preferences.
	 */
	@Deprecated
	@PUT
	@Path("/storetableviewlayout/{uid}")
	@Consumes({MediaType.APPLICATION_JSON})
	@RestServiceInfo(description = "Stores grid column order and column width for tableview.")
	public void storetableviewlayout(@PathParam("uid") String uid, JsonObject data) {

		checkPermission(E.ENTITY, uid);

		getDataServiceHelper().storeTableViewLayout(
				uid,
				null,
				data
		);
	}

	/**
	 * @deprecated Replaced by preferences.
	 */
	@Deprecated
	@PUT
	@Path("/storetableviewlayout/{uid}/{dependenceUid}")
	@Consumes({MediaType.APPLICATION_JSON})
	@RestServiceInfo(description = "Stores grid column order and column width for subforms.")
	public void storetableviewlayout(@PathParam("uid") String uid, @PathParam("dependenceUid") String dependenceUid, JsonObject data) {

		checkPermission(E.ENTITY, uid);

		getDataServiceHelper().storeTableViewLayout(
				uid,
				dependenceUid,
				data
		);
	}


	/**
	 * @deprecated Replaced by preferences.
	 */
	@Deprecated
	@POST
	@Path("/resettableviewlayout/{uid}")
	@Consumes({MediaType.APPLICATION_JSON})
	@RestServiceInfo(description = "Resets grid column order and column width for tableview.")
	public void resettableviewlayout(@PathParam("uid") String uid) {

		checkPermission(E.ENTITY, uid);

		getDataServiceHelper().resetTableViewLayout(
				uid,
				null
		);
	}


	/**
	 * @deprecated Replaced by preferences.
	 */
	@Deprecated
	@POST
	@Path("/resettableviewlayout/{uid}/{dependenceUid}")
	@Consumes({MediaType.APPLICATION_JSON})
	@RestServiceInfo(description = "Resets grid column order and column width for subforms.")
	public void resettableviewlayout(@PathParam("uid") String uid, @PathParam("dependenceUid") String dependenceUid) {

		checkPermission(E.ENTITY, uid);

		getDataServiceHelper().resetTableViewLayout(
				uid,
				dependenceUid
		);
	}


	@POST
	@Path("/resetworkspace")
	@Consumes({MediaType.APPLICATION_JSON})
	@RestServiceInfo(description = "Resets the users workspace to default.")
	public void resetworkspace() {
		PreferencesFacadeLocal preferencesFacade = SpringApplicationContextHolder.getBean(PreferencesFacadeLocal.class);
		try {
			WorkspaceVO wvo = Rest.getDefaultWorkspace();

			preferencesFacade.restoreWorkspace(wvo);
			wvo = Rest.getDefaultWorkspace();

			wvo.getWoDesc().setAlwaysOpenAtLogin(true);
			preferencesFacade.storeWorkspace(wvo);
		} catch (Exception e) {
			throw new NuclosWebException(e, null);
		}
	}


	@GET
	@Path("/searchfilter/{uid}")
	@RestServiceInfo(description = "Get searchfilter.", identifier = "searchfilter")
	@Operation(
			responses = @ApiResponse(
					content = @Content(
							schema = @Schema(
									implementation = SearchfilterEntityMeta.class
							)
					)
			)
	)
	public JsonObject searchfilter(@PathParam("uid") String uid) {
		try {
			EntitySearchFilter2 sf = Rest.facade().getSearchFilterByPk(Rest.translateFqn(E.SEARCHFILTER, uid));
			if (sf == null) {
				throw new WebApplicationException(Status.BAD_REQUEST);
			}
			checkPermission(E.SEARCHFILTER, Rest.translateUid(E.SEARCHFILTER, sf.getVO().getEntity()));
			//TODO: Avoid build() here
			JsonObject jsonEM = jsonFactory.buildJsonObject(getBOMeta(), this).build();
			JsonObjectBuilder b = Json.createObjectBuilder();
			for (Entry<String, JsonValue> entry : jsonEM.entrySet()) {
				b.add(entry.getKey(), entry.getValue());
			}
			b.add("searchfilter", Rest.translateUid(E.SEARCHFILTER, sf.getVO().getPrimaryKey()));
			return b.build();
		} catch (Exception ex) {
			throw new NuclosWebException(ex, Rest.translateFqn(E.SEARCHFILTER, uid));
		}
	}

	@GET
	@Path("/searchfilters/{uid}")
	@RestServiceInfo(description = "Get all searchfilters for a business object for the logged in user.")
	@Operation(
			responses = @ApiResponse(
					content = @Content(
							array = @ArraySchema(
									schema = @Schema(
											implementation = SearchfilterInfo.class
									)
							)
					)
			)
	)
	public JsonArray searchfilters(@PathParam("uid") String uid) {
		try {
			UID entityUID = Rest.translateFqn(E.ENTITY, uid);
			List<SearchFilterVO> searchFilters = Rest.facade().getSearchFilters();
			JsonArrayBuilder jsonArray = Json.createArrayBuilder();

			for (SearchFilterVO searchFilter : searchFilters) {
				if (entityUID.equals(searchFilter.getEntity()) && Boolean.TRUE.equals(searchFilter.getFastSelectInResult())) {
					jsonArray.add(
							Json.createObjectBuilder()
									.add("pk", "" + searchFilter.getPrimaryKey())
									.add("filterName", "" + searchFilter.getFilterName())
									.add("default", "" + searchFilter.getFastSelectDefault())
									.add("order", LangUtils.defaultIfNull(searchFilter.getOrder(), Integer.MAX_VALUE))
									.add("icon", searchFilter.getFastSelectIcon() != null ? searchFilter.getFastSelectIcon().toString() : "")
					);
				}
			}

			return jsonArray.build();

		} catch (Exception ex) {
			throw new NuclosWebException(ex, Rest.translateFqn(E.SEARCHFILTER, uid));
		}
	}

	@GET
	@Path("/systemparameters")
	@RestServiceInfo(validateSession = false)
	public RestSystemparameters systemparameters() {
		return RestSystemparameters.builder()
				.withENVIRONMENTDEVELOPMENT(NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_DEVELOPMENT))
				.withFULLTEXTSEARCHENABLED(!"off".equalsIgnoreCase(NuclosSystemParameters.getString(NuclosSystemParameters.INDEX_PATH)))
				.withANONYMOUSUSERACCESSENABLED(ServerParameterProvider.getInstance().isEnabled(ParameterProvider.KEY_ANONYMOUS_USER_ACCESS_ENABLED))
				.withUSERREGISTRATIONENABLED(StringUtils.isNotEmpty(ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_ROLE_FOR_SELF_REGISTERED_USERS)))
				.withWEBCLIENTCSS(ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_WEBCLIENT_CSS))
				.withFORGOTLOGINDETAILSENABLED(isForgotLoginDetailsEnabled())
				.withQUICKSEARCHDELAYTIME(ServerParameterProvider.getInstance().getIntValue(ParameterProvider.QUICKSEARCH_DELAY_TIME, 400))
				.withFUNCTIONBLOCKDEV(ApplicationProperties.getInstance().isFunctionBlockDev())
				.build();
	}

	/**
	 * Determines if the "forgot login details" feature is currently usable.
	 */
	private boolean isForgotLoginDetailsEnabled() {
		return mailServiceProvider.isConfiguredForSending()
			&& StringUtils.isNotEmpty(ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_RESET_PW_EMAIL_MESSAGE))
			&& StringUtils.isNotEmpty(ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_USERNAME_EMAIL_MESSAGE));
	}

	@PUT
	@Path("/systemparameters")
	@Transactional(rollbackFor = {Throwable.class})
	public void saveSystemparameters(Map<String, Object> parameters) {
		checkSuperUser();

		Map<String, String> stringParameters = new HashMap<>();
		for (String key : parameters.keySet()) {
			Object value = parameters.get(key);
			stringParameters.put(key, value != null ? value.toString() : null);
		}
		ServerParameterProvider.getInstance().updateSystemParameters(stringParameters);
	}


	/**
	 * @deprecated This service is obsolete since the REST explorer was replaced with Swagger-UI (NUCLOS-7794).
	 */
	@GET
	@Path("/restservices")
	@RestServiceInfo(isFinalized = true, description = "List of restservices.")
	@Deprecated
	public RestServiceDefinitionWrapper restservices() {

		List<RestServiceDefinition> restServiceDefinitions = new ArrayList<>();

		for (Class<?> restServiceClass : new NuclosRestApplication().getClasses()) {

			String pathValueFromClass = "";
			for (Annotation annotation : restServiceClass.getAnnotations()) {
				if (annotation instanceof Path) {
					Path path = (Path) annotation;
					pathValueFromClass = path.value();
				}
			}

			for (Method method : restServiceClass.getMethods()) {
				Annotation[] annotations = method.getDeclaredAnnotations();

				HTTPMethod httpMethod = getHttpMethod(method);
				if (httpMethod == null) {
					continue;
				}

				String examplePostData = getRestServiceExamplePostData(method);
				String description = getRestServiceDescription(method);
				boolean isFinalized = getRestServiceIsFinalized(method);
				boolean validateSession = getRestSericeValidateSession(method);

				Path path = null;
				for (Annotation annotation : annotations) {
					if (annotation instanceof Path) {
						path = (Path) annotation;
					}
				}

				RestServiceDefinition rsd = new RestServiceDefinition(httpMethod,
						pathValueFromClass + (path != null ? path.value() : ""),
						examplePostData,
						description,
						method.getReturnType(),
						isFinalized,
						validateSession);
				restServiceDefinitions.add(rsd);

			}
		}

		Collections.sort(restServiceDefinitions);
		return new RestServiceDefinitionWrapper(restServiceDefinitions);
	}

	class RestServiceDefinitionWrapper implements Serializable {

		private List<RestServiceDefinition> services;

		public List<RestServiceDefinition> getServices() {
			return services;
		}

		RestServiceDefinitionWrapper(List<RestServiceDefinition> restServiceDefinitions) {
			this.services = restServiceDefinitions;
		}
	}


	private static HTTPMethod getHttpMethod(Method method) {
		Annotation[] annotations = method.getDeclaredAnnotations();

		for (Annotation annotation : annotations) {
			if (annotation instanceof GET) {
				return HTTPMethod.GET;
			} else if (annotation instanceof POST) {
				return HTTPMethod.POST;
			} else if (annotation instanceof PUT) {
				return HTTPMethod.PUT;
			} else if (annotation instanceof DELETE) {
				return HTTPMethod.DELETE;
			}
		}
		return null;
	}

	private static String getRestServiceExamplePostData(Method method) {
		if (method.isAnnotationPresent(RestServiceInfo.class)) {
			return method.getAnnotation(RestServiceInfo.class).examplePostData();
		}
		return null;
	}

	private static String getRestServiceDescription(Method method) {
		if (method.isAnnotationPresent(RestServiceInfo.class)) {
			return method.getAnnotation(RestServiceInfo.class).description();
		}
		return null;
	}

	private static boolean getRestServiceIsFinalized(Method method) {
		if (method.isAnnotationPresent(RestServiceInfo.class)) {
			return method.getAnnotation(RestServiceInfo.class).isFinalized();
		}
		return false;
	}

	private static boolean getRestSericeValidateSession(Method method) {
		if (method.isAnnotationPresent(RestServiceInfo.class)) {
			return method.getAnnotation(RestServiceInfo.class).validateSession();
		}
		return false;
	}

	private enum HTTPMethod {
		GET, POST, PUT, DELETE
	}


	public class RestServiceDefinition implements Comparable<RestServiceDefinition>, Serializable {
		private HTTPMethod httpMethod;
		private String path;
		private String examplePostData;
		private String description;
		private Class<?> returnType;
		private boolean isFinalized;
		private boolean validateSession;

		@JsonGetter("method")
		public HTTPMethod getHttpMethod() {
			return httpMethod;
		}

		public String getPath() {
			return path;
		}

		public String getExamplePostData() {
			return examplePostData;
		}

		public String getDescription() {
			return description;
		}

		public Class<?> getReturnType() {
			return returnType;
		}

		public boolean isFinalized() {
			return isFinalized;
		}

		public boolean isValidateSession() {
			return validateSession;
		}


		RestServiceDefinition(HTTPMethod httpMethod, String path, String examplePostData, String description, Class<?> returnType, boolean isFinalized, boolean validateSession) {
			this.httpMethod = httpMethod;
			if (path != null) {
				this.path = path.replaceAll("//", "/");
			}
			this.examplePostData = examplePostData;
			this.description = description;
			this.returnType = returnType;
			this.isFinalized = isFinalized;
			this.validateSession = validateSession;
		}

		private int sortHttpMethod(HTTPMethod httpMethod) {
			if (httpMethod.toString().equals("GET"))
				return 0;
			if (httpMethod.toString().equals("POST"))
				return 1;
			if (httpMethod.toString().equals("PUT"))
				return 2;
			if (httpMethod.toString().equals("PATCH"))
				return 3;
			if (httpMethod.toString().equals("DELETE"))
				return 4;
			return 5;
		}

		@Override
		public int compareTo(RestServiceDefinition arg0) {
			int i = path.compareTo(arg0.path);
			if (i != 0) {
				return i;
			}
			return Integer.compare(sortHttpMethod(httpMethod), sortHttpMethod(arg0.httpMethod));
		}
	}

	@Deprecated
	@GET
	@Path("/statusicon/{uid}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	/**
	 * Deprecated: Use org.nuclos.server.rest.services.ResourceService#stateIcon(java.lang.String)
	 */
	public Response statusicon(@PathParam("uid") String uid) {
		return ResourceService.getStateIcon(uid, stateCache);
	}

	@GET
	@Path("/icon/{resourcename}")
	@RestServiceInfo(identifier = "resourceicon", description = "Resource-Icon")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response icon(
			@Context final Request request,
			@PathParam("resourcename") String resourcename
	) {
		try {
			ResourceResolver rr = new ResourceResolver();
			Resource resource = rr.resolveEntityIcon(resourcename);
			if (resource == null) {
				resource = rr.resolveHiddenEntityIcon(resourcename);
			}

			if (resource == null) {
				throw new NuclosWebException(Response.Status.NOT_FOUND, String.format("Resource (%s) not found", resourcename));
			}

			final Resource finalResource = resource;

			return new CacheableResponseBuilder(request) {
				@Override
				protected Date getLastModified() throws IOException {
					return new Date(finalResource.lastModified());
				}

				@Override
				protected Response.ResponseBuilder newResponseBuilder() throws IOException {
					byte[] imageBytes = IOUtils.toByteArray(finalResource.getInputStream());

					final Response.ResponseBuilder builder = Response.ok(imageBytes).type("image/gif");
					CacheControl cc = new CacheControl();
					cc.setMaxAge(RESOURCE_ICON_MAX_AGE_IN_SECDONS);
					builder.cacheControl(cc);
					return builder;
					// return Response.ok(imageBytes)
					//		.type("image/gif");
				}
			}.build();
		} catch (Exception e) {
			LOG.warn("Unable to get icon with resourcename: {}", resourcename, e);
			// throw new NuclosWebException(Response.Status.NOT_FOUND, String.format("Resource (%s) not found", resourcename));
			return Response.ok(Rest.EMPTY_IMAGE).type("image/gif").build();
		}
	}

	@GET
	@Path("/sideviewmenuselector")
	@RestServiceInfo(identifier = "sideviewmenu", description = "Sideview Menu Buttons. (List of 'neverClose' tabs.)")
	@Operation(
			responses = @ApiResponse(
					content = @Content(
							array = @ArraySchema(
									schema = @Schema(
											implementation = SideviewMenuSelector.class
									)
							)
					)
			)
	)
	public JsonArray sideviewMenuSelector() {
		return buildSideviewMenuSelector();
	}

	private DataServiceHelper getDataServiceHelper() {
		if (dataServiceHelper == null) {
			dataServiceHelper = new DataServiceHelper();
		}
		return dataServiceHelper;
	}

	@GET
	@Path("/addonusages")
	@Produces(MediaType.APPLICATION_JSON)
	@RestServiceInfo(identifier = "addonusages", description = "Web addon usages", validateSession=false)
	public WebAddonUsageRVO addonUsages() {
		return new WebAddonUsageRVO(true);
	}

}
