package org.nuclos.server.rest.services;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.schema.rest.LegalDisclaimer;
import org.nuclos.schema.rest.LocaleInfo;
import org.nuclos.schema.rest.LoginInfo;
import org.nuclos.schema.rest.LoginParams;
import org.nuclos.schema.rest.ServerStatus;
import org.nuclos.server.customcode.codegenerator.NuclosJarGeneratorManager;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.services.helper.LoginServiceHelper;
import org.nuclos.server.rest.services.helper.RestServiceInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Path("/")
public class LoginService extends LoginServiceHelper {

	@Lazy
	@Autowired
	private NuclosJarGeneratorManager nuclosJarGeneratorManager;

	@GET
	@Path("/login")
	@RestServiceInfo(identifier="login", description="Returns JsonObject with SessionID and link for BO-Metalist.")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	public LoginInfo login(@Context HttpServletRequest request) {
		return buildLoginInfo();
	}

	/**
	 * For backward compatibility only.
	 * Endpoints with same path as their parent can cause problems, see NUCLOS-7765.
	 */
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@Deprecated
	public LoginInfo deprecatedLogin(@Context HttpServletRequest request) {
		return login(request);
	}

	@POST
	@Path("/login")
	@RestServiceInfo(identifier="login", validateSession=false, isFinalized=true, description="User-Login. Returns JsonObject with SessionID and link for BO-Metalist.", examplePostData = "{\"username\": \"nuclos\", \"password\": \"\"}")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Operation(
			responses = @ApiResponse(
					content = @Content(
							schema = @Schema(
									implementation = LoginInfo.class
							)
					)
			)
	)
	public Response login(
			@Context HttpServletRequest request,
			final LoginParams data
	) {
		LoginResult result = doLogin(data, request);

		// TODO: A LoginInfo should be returned directly, instead of a generic Response.
		//  How to set cookies then?
		return Response.ok(result.getLoginInfo(), MediaType.APPLICATION_JSON)
				.cookie(result.getCookies())
				.build();
	}

	/**
	 * For backward compatibility only.
	 * Endpoints with same path as their parent can cause problems, see NUCLOS-7765.
	 */
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@RestServiceInfo(validateSession=false)
	@Operation(
			responses = @ApiResponse(
					content = @Content(
							schema = @Schema(
									implementation = LoginInfo.class
							)
					)
			)
	)
	public Response deprecatedLogin(
			@Context final HttpServletRequest request,
			final LoginParams data
	) {
		return login(request, data);
	}

	@GET
	@Path("locale")
	@RestServiceInfo(identifier="getLocale", description="Returns the server-side locale for the current session.")
	@Produces({MediaType.APPLICATION_JSON})
	public LocaleInfo doGetLocale() {
		final Locale locale = getLocale();

		return LocaleInfo.builder()
				.withLocale(locale.toString())
				.build();
	}

	@PUT
	@Path("locale")
	@RestServiceInfo(identifier="setLocale", description="Sets the server-side locale for the current session.", examplePostData = "{\"locale\": \"de\"}")
	@Consumes({MediaType.APPLICATION_JSON})
	public void setLocale(
			final LocaleInfo data
	) {
		final String localeString = data.getLocale();
		setLocale(localeString);
	}

	@POST
	@Path("chooseMandator/{mandatorId}")
	@RestServiceInfo(identifier="chooseMandator", isFinalized=true, description="Choose the mandator after a user logs in. The Result of the login contains a list of possible mandators to choose.")
	public void chooseMandator(@PathParam("mandatorId") String mandatorId) {
		doChooseMandator(mandatorId);
	}

	//FIXME: HATEOAS Standard dictates that @DELETE-Method must delete persistent object on the DB. See also BoService @Path("/{boMetaId}/query")
	@DELETE
	@Path("/login")
	@RestServiceInfo(identifier="logout", isFinalized=true, description="User-Logout. Returns HTTP-Status 200, if successful")
	public void logout(@Context HttpServletRequest request) {
		doLogout(request);
	}

	/**
	 * For backward compatibility only.
	 * Endpoints with same path as their parent can cause problems, see NUCLOS-7765.
	 */
	@DELETE
	@Deprecated
	public void deprecatedLogout(@Context HttpServletRequest request) {
		logout(request);
	}

	@GET
	@Path("serverstatus")
	@Produces(MediaType.APPLICATION_JSON)
	@RestServiceInfo(identifier="serverstatus", validateSession=false, isFinalized=true, description="Status of the nuclos server")
	public ServerStatus serverstatus() {
		boolean ready = SpringApplicationContextHolder.isNuclosReady();
		if (ready) {
			nuclosJarGeneratorManager.isGeneratorRunningAndWaitFor();
		}

		if (!ready) {
			throw new NuclosWebException(Response.Status.SERVICE_UNAVAILABLE);
		}

		return ServerStatus.builder()
				.withReady(ready)
				.build();
	}

	@GET
	@Path("version")
	@RestServiceInfo(identifier="version", validateSession=false, isFinalized=true, description="Version of the nuclos server")
	public String version() {
		return getVersion();
	}

	@GET
	@Path("version/db")
	@RestServiceInfo(identifier="dbversion", validateSession=false, isFinalized=true, description="Version of DB schema of the nuclos server. Actually accesses the DB.")
	public String dbversion() {
		return getDbVersion();
	}

	@GET
	@Path("version/timestamp")
	@RestServiceInfo(identifier="versiontimestamp", validateSession=false, isFinalized=true, description="Nuclos build timestamp.")
	public String versionDate() {
		return getVersionTimestamp();
	}

	@GET
	@Path("version/legaldisclaimer")
	@Produces(MediaType.APPLICATION_JSON)
	@RestServiceInfo(identifier = "legaldisclaimer", validateSession = false, description = "Legal disclaimer about Nuclos and this application.")
	public List<LegalDisclaimer> getLegalDisclaimers() {
		return super.getLegalDisclaimers();
	}
}
