package org.nuclos.server.rest.services;

import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

import org.nuclos.schema.rest.InputRequiredContext;
import org.nuclos.server.rest.services.helper.DataServiceHelper;
import org.nuclos.server.rest.services.helper.RestServiceInfo;

import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;

@Path("/boStateChanges")
public class BoStateService extends DataServiceHelper {

	@PUT
	@Path("/{boMetaId}/{boId}/{stateId}")
	@RestServiceInfo(identifier="boStateChange", description="Change status of BO")
	@Consumes(MediaType.APPLICATION_JSON)
	public void boStateChange(
			@PathParam("boMetaId") String boMetaId,
			@PathParam("boId") String boId,
			@PathParam("stateId") String statusIdOrNumeral,
			@Parameter(
					schema = @Schema(
							implementation = InputRequiredContext.class
					)
			) JsonObject data
	) {
		stateChange(boMetaId, boId, statusIdOrNumeral, data);
	}

}
