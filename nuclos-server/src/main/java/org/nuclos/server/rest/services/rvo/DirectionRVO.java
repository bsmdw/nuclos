//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.services.rvo;

import java.io.Serializable;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

import org.nuclos.api.Direction.AdditionalNavigation;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class DirectionRVO implements Serializable, CommandRVO {

	private static final long serialVersionUID = 1L;

	@JsonProperty("id")
	final private Long id;
	
	@JsonProperty("path")
	final private String path;
	
	@JsonProperty("newTab")
	final private Boolean newTab;

	@JsonProperty("additionalNavigation")
	final private AdditionalNavigation additionalNavigation;
	
	@JsonCreator
	public DirectionRVO(@JsonProperty("id") Long id, @JsonProperty("path") String path, @JsonProperty("newTab") Boolean newTab, AdditionalNavigation additionalNavigation) {
      this.id = id;
      this.path = path;
      this.newTab = newTab;
      this.additionalNavigation = additionalNavigation;
	}

	public Long getId() {
		return id;
	}

	public String getPath() {
		return path;
	}

	public Boolean getNewTab() {
		return newTab;
	}
	
	public AdditionalNavigation getAdditionalNavigation() {
		return additionalNavigation;
	}
	
	@Override
	public CommandType getCommandType() {
		return CommandType.NAVIGATE;
	}

	
	@Override
	public JsonObjectBuilder toJsonBuilderObject() {
		JsonObjectBuilder json = Json.createObjectBuilder();
		json.add("commandType", JsonFactory.buildJsonValue(getCommandType().name()));
		json.add("id", JsonFactory.buildJsonValue(id));
		json.add("path", JsonFactory.buildJsonValue(path));
		json.add("newTab", JsonFactory.buildJsonValue(newTab));
		if(additionalNavigation!=null) json.add("additionalNavigation", JsonFactory.buildJsonValue(additionalNavigation.name()));
		else json.add("additionalNavigation", JsonFactory.buildJsonValue(null));
		return json;
	}


}
