package org.nuclos.server.rest.services.helper;

import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

import javax.json.JsonObject;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.nuclos.api.User;
import org.nuclos.api.context.CustomRestContext;
import org.nuclos.api.context.CustomRestResponse;
import org.nuclos.api.context.CustomRestUploadContext;
import org.nuclos.api.rule.CustomRestRule;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.common.EventSupportCache;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.customcode.CustomCodeManager;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.i18n.language.data.DataLanguageCache;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.security.UserFacadeLocal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CustomRestServiceHelper extends DataServiceHelper {

	protected static final Logger LOG = LoggerFactory.getLogger(CustomRestServiceHelper.class);

	@Autowired
	UserFacadeLocal userFacade;

	@Autowired
	private DataLanguageCache dataLanguageCache;

	@Autowired
	private CustomCodeManager customCodeManager;

	@Autowired
	private SecurityCache securityCache;

	@Autowired
	private EventSupportCache esCache;

	protected <T> Response invokeRestRuleMethod(
			final String className,
			final String methodName,
			final Class methodType,
			final JsonObject data,
			final Class<T> returnType
	) throws NuclosWebException, IllegalAccessException {
		return invokeRestRuleMethod(className,
				methodName,
				methodType,
				data,
				null,
				null,
				returnType
		);
	}

	protected <T> Response invokeRestRuleMethod(
			final String className,
			final String methodName,
			final Class methodType,
			final JsonObject data,
			final InputStream inputStream,
			final FormDataContentDisposition formData,
			final Class<T> returnType
	) throws NuclosWebException, IllegalAccessException {
		try {

			final boolean isFileupLoadRequest = inputStream != null;

			final Map<UID, String> allowedCustomRestRules = securityCache.getAllowedCustomRestRules(this.getUser(), this.getMandatorUID());
			if (!Rest.facade().isSuperUser() && !allowedCustomRestRules.containsValue(className)) {
				throw new NuclosWebException(Response.Status.UNAUTHORIZED);
			}

			EventSupportSourceVO essV0 = esCache.getEventSupportSourceByClassname(className);
			if (essV0 == null || !essV0.isActive()) {
				throw new NuclosWebException(Response.Status.NOT_IMPLEMENTED);
			}

			final ClassLoader classLoader = this.customCodeManager.getClassLoaderAndCompileIfNeeded();
			final CustomRestRule rule = (CustomRestRule)classLoader.loadClass(className).newInstance();

			if (!(rule instanceof CustomRestRule)) {
				throw new NuclosWebException(Response.Status.FORBIDDEN);
			}

			final String language = dataLanguageCache.getLanguageToUse() != null ? dataLanguageCache.getLanguageToUse().toString() : null;
			final Class<? extends CustomRestRule> ruleClass = rule.getClass();
			final Method method = ruleClass.getMethod(methodName, isFileupLoadRequest ? CustomRestUploadContext.class : CustomRestContext.class);
			if (method.getAnnotation(methodType) == null) {
				throw new IllegalAccessException("'" + methodName + "' is not a " + methodType + " method.");
			}

			User user = null;
			try {
				user = CustomRestContextFactory.getUser(getUser());
			} catch (CommonBusinessException e) {
				LOG.error("Unable to get user");
			}

			final CustomRestContext customRestContext = isFileupLoadRequest ?
					new CustomRestUploadContextImpl(user, this, data, ruleClass, language, inputStream, formData)
					:
					new CustomRestContextImpl(user, this, data, ruleClass, language)
					;
			final Object methodReturnValue = method.invoke(rule, customRestContext);

			Response.ResponseBuilder builder;
			if (methodReturnValue instanceof CustomRestResponse) {
				CustomRestResponse  customRestResponseResult = (CustomRestResponse)methodReturnValue;
				builder = Response.ok(transformJson(customRestResponseResult.getResponseValue(), returnType));

				// set response headers
				customRestResponseResult.getHeaders().entrySet().stream().forEach(h -> builder.header(h.getKey(), h.getValue()));
			} else {
				builder = Response.ok(transformJson(methodReturnValue, returnType));
			}
			return  builder.build();
		} catch (NoSuchMethodException
				| InstantiationException
				| InvocationTargetException
				| JsonProcessingException
				| NuclosCompileException | ClassNotFoundException e) {
			throw new NuclosWebException(e, null);

		}
	}

	private <T> Object transformJson(final Object value, final Class<T> returnType) throws JsonProcessingException {
		if (returnType == Object.class) {
			return (T) value;
		} else {
			if (value instanceof String || value instanceof JsonObject) {
				return (T) value.toString();
			} else {
				return (T) new ObjectMapper().writeValueAsString(value);
			}
		}
	}

}
