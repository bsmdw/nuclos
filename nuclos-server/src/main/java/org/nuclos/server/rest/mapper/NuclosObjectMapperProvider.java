//Copyright (C) 2016  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.mapper;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import org.nuclos.cache.IFqnCache;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common2.jackson.MinimalTypeNameMixIn;
import org.nuclos.layout.transformation.UidToFqnJacksonSerializer;
import org.nuclos.schema.layout.rule.RuleAction;
import org.nuclos.schema.layout.web.WebComponent;
import org.nuclos.server.masterdata.ejb3.MasterDataRestFqnCache;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

@Provider
@Component
public class NuclosObjectMapperProvider implements ContextResolver<ObjectMapper> {

	final ObjectMapper defaultObjectMapper;

	/**
	 * A ObjectMapper that serializes only non-null properties.
	 */
	ObjectMapper nonNullObjectMapper;

	public NuclosObjectMapperProvider() {
		defaultObjectMapper = createDefaultMapper();
		nonNullObjectMapper = createNonEmptyMapper();
	}

	@Override
	public ObjectMapper getContext(Class<?> type) {
		if (!type.isArray() && type.getPackage().getName().startsWith("org.nuclos.schema.")) {
			return nonNullObjectMapper;
		}

		return defaultObjectMapper;
	}

	private static ObjectMapper createDefaultMapper() {
		final ObjectMapper result = new ObjectMapper();
		result.configure(MapperFeature.USE_WRAPPER_NAME_AS_PROPERTY_NAME, true);
		return result;
	}

	private ObjectMapper createNonEmptyMapper() {
		final ObjectMapper result = createDefaultMapper();
		result.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

		// TODO: Find away to define this directly at the underlying schema of these classes
		result.addMixIn(WebComponent.class, MinimalTypeNameMixIn.class);
		result.addMixIn(RuleAction.class, MinimalTypeNameMixIn.class);

		SimpleModule testModule = new SimpleModule("TestModule", new Version(1, 0, 0, null));
		testModule.addSerializer(new UidToFqnJacksonSerializer() {
			@Override
			public IFqnCache getFqnCache() {
				return SpringApplicationContextHolder.getBean(MasterDataRestFqnCache.class);
			}
		});
		result.registerModule(testModule);

		return result;
	}
}

