package org.nuclos.server.rest.swagger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.google.common.base.Enums;

import io.swagger.v3.oas.annotations.enums.Explode;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.enums.ParameterStyle;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.servers.ServerVariable;
import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.examples.Example;
import io.swagger.v3.oas.models.headers.Header;
import io.swagger.v3.oas.models.links.Link;
import io.swagger.v3.oas.models.media.Content;
import io.swagger.v3.oas.models.media.Encoding;
import io.swagger.v3.oas.models.media.MediaType;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.parameters.Parameter;
import io.swagger.v3.oas.models.parameters.RequestBody;
import io.swagger.v3.oas.models.responses.ApiResponses;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.servers.Server;
import io.swagger.v3.oas.models.servers.ServerVariables;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class SwaggerAnnotationParser {

	static ExternalDocumentation parseExternalDocumentation(final io.swagger.v3.oas.annotations.ExternalDocumentation externalDocs) {
		ExternalDocumentation result = null;

		if (externalDocs != null) {
			result = new ExternalDocumentation();
			result.description(externalDocs.description());
			result.url(externalDocs.url());
		}

		return result;
	}

	static List<Server> parseServers(final io.swagger.v3.oas.annotations.servers.Server[] servers) {
		List<Server> result = new ArrayList<>();

		if (servers != null) {
			Arrays.stream(servers).forEach(server ->
					result.add(parseServer(server))
			);
		}

		return result;
	}

	static Server parseServer(final io.swagger.v3.oas.annotations.servers.Server server) {
		Server result = null;

		if (server != null) {
			result = new Server()
					.description(server.description())
					.url(server.url())
					.variables(parseVariables(server.variables()));
		}

		return result;
	}

	static ServerVariables parseVariables(final ServerVariable[] variables) {
		ServerVariables result;

		if (variables != null) {
			result = new ServerVariables();
			Arrays.stream(variables).forEach(var ->
					result.addServerVariable(
							var.name(),
							new io.swagger.v3.oas.models.servers.ServerVariable()
									.description(var.description())
									._default(var.defaultValue())
									._enum(Arrays.asList(var.allowableValues()))
					)
			);
		} else {
			result = null;
		}

		return result;
	}

	static List<SecurityRequirement> parseSecurity(final io.swagger.v3.oas.annotations.security.SecurityRequirement[] security) {
		List<SecurityRequirement> result = new ArrayList<>();

		if (security != null) {
			for (io.swagger.v3.oas.annotations.security.SecurityRequirement item : security) {
				result.add(new SecurityRequirement()
						.addList(item.name(), Arrays.asList(item.scopes()))
				);
			}
		}

		return result;
	}

	static ApiResponses parseResponses(final ApiResponse[] responses) {
		ApiResponses result;

		if (responses != null) {
			result = new ApiResponses();
			Arrays.stream(responses).forEach(it ->
			{
				io.swagger.v3.oas.models.responses.ApiResponse response = new io.swagger.v3.oas.models.responses.ApiResponse()
						.content(parseContent(it.content()))
						.description(it.description())
						.headers(parseHeaders(it.headers()));

				Arrays.stream(it.links()).forEach(link -> response.link(
						link.name(),
						parseLink(link)
				));

				result.addApiResponse(
						it.responseCode(),
						response
				);
			});
		} else {
			result = null;
		}

		return result;
	}

	static Link parseLink(final io.swagger.v3.oas.annotations.links.Link link) {
		final Link result;

		if (link != null) {
			result = new Link()
					.description(link.description())
					.operationId(StringUtils.defaultIfBlank(link.operationId(), null))
					.operationRef(StringUtils.defaultIfBlank(link.operationRef(), null));

			Arrays.stream(link.parameters()).forEach(
					it -> result.parameters(
							it.name(),
							it.expression()
					)
			);

			result.requestBody(link.requestBody())
					.server(parseServer(link.server()));
		} else {
			result = null;
		}

		return result;
	}

	static Map<String, Header> parseHeaders(final io.swagger.v3.oas.annotations.headers.Header[] headers) {
		Map<String, Header> result = new HashMap<>();

		if (headers != null) {
			Arrays.stream(headers).forEach(it ->
					result.put(it.name(), parseHeader(it))
			);
		}

		return result;
	}

	static Header parseHeader(final io.swagger.v3.oas.annotations.headers.Header header) {
		Header result = null;

		if (header != null) {
			result = new Header()
					.deprecated(header.deprecated())
					.description(header.description())
					.required(header.required())
					.schema(parseSchema(header.schema()));
		}

		return result;
	}

	static RequestBody parseRequestBody(final io.swagger.v3.oas.annotations.parameters.RequestBody requestBody) {
		RequestBody result = null;

		if (requestBody != null) {
			result = new RequestBody()
					.content(parseContent(requestBody.content()))
					.description(requestBody.description())
					.required(requestBody.required());
		}

		return result;
	}

	static List<Parameter> parseParameters(final io.swagger.v3.oas.annotations.Parameter[] parameters) {
		List<Parameter> result = new ArrayList<>();

		for (io.swagger.v3.oas.annotations.Parameter param : parameters) {
			result.add(new Parameter()
					.allowEmptyValue(param.allowEmptyValue())
					.allowReserved(param.allowReserved())
					.content(parseContent(param.content()))
					.deprecated(param.deprecated())
					.description(param.description())
					.example(param.example())
					.examples(parseExamples(param.examples()))
					.explode(param.explode() == Explode.TRUE)
					.in(parseIn(param.in()))
					.name(param.name())
					.required(param.required())
					.schema(parseSchema(param.schema()))
					.style(parseStyle(param.style()))
			);
		}

		return result;
	}

	static Parameter.StyleEnum parseStyle(final ParameterStyle style) {
		Parameter.StyleEnum result = null;

		if (style != null) {
			result = Parameter.StyleEnum.valueOf(style.name());
		}

		return result;
	}

	static Schema parseSchema(final io.swagger.v3.oas.annotations.media.Schema schema) {
		Schema result = null;

		if (schema != null) {
			result = new Schema()
					.deprecated(schema.deprecated())
					.description(schema.description())
//					.discriminator(parseDiscriminator(schema.discriminatorMapping()))
					.example(schema.example())
					.exclusiveMaximum(schema.exclusiveMaximum())
					.exclusiveMinimum(schema.exclusiveMinimum())
					.externalDocs(parseExternalDocumentation(schema.externalDocs()))
					.format(schema.format())
					.maximum(StringUtils.isBlank(schema.maximum()) ? null : new BigDecimal(schema.maximum()))
//					.maxItems(schema.maxLength())
					.maxLength(schema.maxLength())
					.maxProperties(schema.maxProperties())
					.minimum(StringUtils.isBlank(schema.minimum()) ? null : new BigDecimal(schema.minimum()))
//					.minItems()
					.minLength(schema.minLength())
					.minProperties(schema.minProperties())
					.name(schema.name())
//					.not(schema.not())
					.nullable(schema.nullable())
					.pattern(schema.pattern())
//					.properties()
					.readOnly(schema.readOnly())
//					.required(schema.requiredProperties())
					.title(schema.title())
					.type(schema.type())
//					.uniqueItems()
					.writeOnly(schema.writeOnly())
//					.xml()
			;

			// Set only if > 0
			BigDecimal multipleOf = BigDecimal.valueOf(schema.multipleOf());
			if (multipleOf.compareTo(BigDecimal.ZERO) > 0) {
				result.multipleOf(multipleOf);
			}
		}

		return result;
	}

	static String parseIn(final ParameterIn in) {
		if (in != null) {
			return in.toString();
		}

		return ParameterIn.DEFAULT.toString();
	}

	static Map<String, Example> parseExamples(final ExampleObject[] examples) {
		Map<String, Example> result = new HashMap<>();

		if (examples != null) {
			Arrays.stream(examples).forEach(it ->
					result.put(
							it.name(),
							new Example()
									.description(it.name())
									.externalValue(it.externalValue())
									.summary(it.summary())
									.value(it.value())
					)
			);
		}

		return result;
	}

	static Content parseContent(final io.swagger.v3.oas.annotations.media.Content[] contents) {
		Content result = null;

		if (contents != null) {
			result = new Content();
			for (io.swagger.v3.oas.annotations.media.Content content : contents) {

				MediaType mediaType = new MediaType();
				mediaType.schema(parseSchema(content.schema()));
				for (io.swagger.v3.oas.annotations.media.Encoding encoding : content.encoding()) {
					mediaType.addEncoding(encoding.name(), parseEncoding(encoding));
				}
				mediaType.examples(parseExamples(content.examples()));
				result.addMediaType(
						content.mediaType(),
						mediaType
				);
			}
		}

		return result;
	}

	static Encoding parseEncoding(final io.swagger.v3.oas.annotations.media.Encoding encoding) {
		Encoding result = null;

		if (encoding != null) {
			result = new Encoding()
					.allowReserved(encoding.allowReserved())
					.contentType(encoding.contentType())
					.explode(encoding.explode())
					.headers(parseHeaders(encoding.headers()))
					.style(parseStyle(encoding));
		}

		return result;
	}

	static Encoding.StyleEnum parseStyle(final io.swagger.v3.oas.annotations.media.Encoding encoding) {
		Encoding.StyleEnum result = null;

		if (encoding != null) {
			result = Enums.getIfPresent(
					Encoding.StyleEnum.class,
					encoding.style()
			).orNull();
		}

		return result;
	}
}
