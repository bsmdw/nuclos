package org.nuclos.server.rest.services.helper;

import org.nuclos.api.User;
import org.nuclos.api.context.CustomRestContext;
import org.nuclos.api.locale.NuclosLocale;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.security.UserVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.i18n.language.data.DataLanguageCache;
import org.nuclos.server.security.UserFacadeLocal;
import org.nuclos.server.security.UserImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomRestContextFactory {

	private final static Logger LOG = LoggerFactory.getLogger(CustomRestContextFactory.class);


	public static CustomRestContext createCustomRestContext(final String userName) {

		final DataLanguageCache dataLanguageCache = SpringApplicationContextHolder.getBean(DataLanguageCache.class);
		final String language = dataLanguageCache.getLanguageToUse() != null ? dataLanguageCache.getLanguageToUse().toString() : null;

		User user = null;
		try {
			if (userName != null) {
				user = getUser(userName);
			}
		} catch (CommonBusinessException e) {
			LOG.error("Unable to get user");
		}
		return new CustomRestContextImpl(
				user,
				null,
				null,
				CustomRestContextImpl.class,
				language
		);
	}

	public static User getUser(final String userName) throws CommonBusinessException {
		final DataLanguageCache dataLanguageCache = SpringApplicationContextHolder.getBean(DataLanguageCache.class);
		final String language = dataLanguageCache.getLanguageToUse() != null ? dataLanguageCache.getLanguageToUse().toString() : null;

		final UserVO userVo = SpringApplicationContextHolder.getBean(UserFacadeLocal.class).getByUserName(userName);
		return new UserImpl(userVo.getPrimaryKey(),
				userVo.getUsername(), userVo.getLastname(), userVo.getFirstname(), userVo.getEmail(),
				userVo.getLocked(), userVo.getSuperuser(), userVo.getLastPasswordChange(),
				userVo.getExpirationDate(), userVo.getPasswordChangeRequired(),
				language != null ? NuclosLocale.valueOf(language.toUpperCase()) : null
		);
	}
}
