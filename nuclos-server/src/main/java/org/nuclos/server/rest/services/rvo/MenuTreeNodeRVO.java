package org.nuclos.server.rest.services.rvo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.nuclos.server.rest.misc.IWebContext;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MenuTreeNodeRVO implements Comparable<MenuTreeNodeRVO>, Serializable {

	private static final long serialVersionUID = 1L;
	public static final String ROOT_MENU_ID = "ROOT_MENU";
	private static final String PATH_SEPARATOR = "\\";
	private static final String PATH_SEPARATOR_FOR_REGEX = "\\\\";
	
	private String id;
	private String parentId;
	private List<MenuTreeNodeRVO> entries = new ArrayList<MenuTreeNodeRVO>();

	private String boMetaId;
	private String name;
	private String icon;
	private boolean hidden;
	private boolean createNew;
	private String processMetaId;
	
	private IWebContext webContext;

	/**
	 * create root node
	 */
	private MenuTreeNodeRVO() {
		this.id = ROOT_MENU_ID;
		this.name = ROOT_MENU_ID;
	}
	
	public MenuTreeNodeRVO(String boMetaId, String path, boolean hidden, String icon, boolean createNew, String processMetaId, IWebContext webContext) {
		String [] pathArray = path.split(PATH_SEPARATOR_FOR_REGEX);
		if (pathArray.length > 1) {
			this.name = pathArray[pathArray.length - 1];
			this.id = StringUtils.join(pathArray, PATH_SEPARATOR);
			this.parentId = StringUtils.join(pathArray, PATH_SEPARATOR, 0, pathArray.length - 1);
		} else if (pathArray.length == 1) {
			this.name = path;
			this.id = path;
			this.parentId = ROOT_MENU_ID;
		}
		
		this.boMetaId = boMetaId;
		this.hidden = hidden;
		this.icon = icon;
		this.createNew = createNew;
		this.processMetaId = processMetaId;

		this.webContext = webContext;
	}
	
	private MenuTreeNodeRVO findTreeNode(String id) {
		if (this.id.equals(id)) {
			return this;
		}
		for (MenuTreeNodeRVO tn : entries) {
			MenuTreeNodeRVO node = tn.findTreeNode(id);
			if (node != null) {
				return node;
			}
		}
		return null;
	}
	
	private MenuTreeNodeRVO addTreeNode(MenuTreeNodeRVO treeNode) {
		if (this.id.equals(treeNode.parentId)) {
			this.entries.add(treeNode);
			return this;
		}
		for (MenuTreeNodeRVO tn : entries) {
			return tn.addTreeNode(treeNode);
		}
		return null;
	}
	
	/**
	 * for debugging
	 */
	public void print() {
		print(0);
	}
	private void print(int level) {
		for(int i=0;i<level;i++) {System.out.print("  ");}
		System.out.println(" > " + name + "      [" + ((parentId!=null)?parentId:"-") +"]");
		if (entries != null) {
			for (MenuTreeNodeRVO childNode : entries) {
				childNode.print(level+1);
			}			
		}
	}

	public static class TreeNodeBuilder {
		
		private IWebContext webContext;
		
		public TreeNodeBuilder(IWebContext webContext) {
			this.webContext = webContext;
		}
		
		List<MenuTreeNodeRVO> list = new ArrayList<MenuTreeNodeRVO>();
		
		public MenuTreeNodeRVO add(String boMetaId, String path, boolean hidden, String icon, boolean createNew, String processMetaId) {
			MenuTreeNodeRVO tn = new MenuTreeNodeRVO(boMetaId, path, hidden, icon, createNew, processMetaId, webContext);
			list.add(tn);
			addMissingParentNodes(tn);
			return tn;
		}

		private void addMissingParentNodes(MenuTreeNodeRVO treeNode) {
			for (MenuTreeNodeRVO tn : list) {
				if (tn.id.equals(treeNode.parentId)) {
					return;
				}
			}
			
			if (!treeNode.parentId.equals(ROOT_MENU_ID)) {
				MenuTreeNodeRVO newNode = add(null, treeNode.parentId, false, null, false, null);
				if (newNode.parentId != null) {
					addMissingParentNodes(newNode);
				}
			}
		}

		public void sort() {
			Collections.sort(this.list);
		}

		public MenuTreeNodeRVO build() {
			this.sort();
			
			MenuTreeNodeRVO root = new MenuTreeNodeRVO();
			list.add(0, root);		
			for(MenuTreeNodeRVO tn : list) {
				MenuTreeNodeRVO parentNode = root.findTreeNode(tn.parentId);
				if (parentNode != null) {
					parentNode.entries.add(tn);
				} else {
					root.addTreeNode(tn);
				}
			}
			return root;
		}
	}
	
	public static class RestLinks {
		private RestLink bos;
		private RestLink resourceicon;

		@JsonInclude(Include.NON_NULL)
		public RestLink getResourceicon() {
			return resourceicon;
		}

		@JsonInclude(Include.NON_NULL)
		public RestLink getBos() {
			return bos;
		}
	}

	public static class RestLink {
		private String href;
		
		public RestLink(String href) {
			super();
			this.href = href;
		}

		public String getHref() {
			return href;
		}
	}
	
	public RestLinks getLinks() {
		RestLinks restLinks = new RestLinks();
		if (icon != null) {
			restLinks.resourceicon = new RestLink(webContext.getRestURI("resourceicon", "") + icon);
		}
		return restLinks;
	}
	
	
	@Override
	public int compareTo(MenuTreeNodeRVO treeNode) {
		if (this.id == null || treeNode.id == null) {
			return 0;
		}
		return this.id.compareTo(treeNode.id);
	}


	@JsonInclude(Include.NON_NULL)
	public String getBoMetaId() {
		return boMetaId;
	}

	public String getName() {
		return name;
	}

	@JsonInclude(Include.NON_NULL)
	public String getIcon() {
		return icon;
	}

	@JsonInclude(Include.NON_EMPTY)
	public List<MenuTreeNodeRVO> getEntries() {
		return entries;
	}
	
	@JsonInclude(Include.NON_NULL)
	public String getProcessMetaId() {
		return processMetaId;
	}
	
	public boolean getCreateNew() {
		return createNew;
	}


	/* for testing
	public static void main(String[] args) {
		TreeNodeBuilder treeNodeList = new TreeNodeBuilder(null);
		treeNodeList.add("de_nuclos_example_X-Achse", "Matrix\\Submenu\\X-Achse", false, null);
		treeNodeList.add("de_nuclos_example_Y-Achse", "Matrix\\Submenu\\Y-Achse", false, null);

		treeNodeList.add(null, "Example", false, null);
		treeNodeList.add("de_nuclos_example_Article", "Example\\Article", false, null);
		treeNodeList.add("de_nuclos_example_Category", "Example\\Category", false, null);
		treeNodeList.add("de_nuclos_example_Customer", "Example\\Customer", false, null);

		MenuTreeNode root = treeNodeList.build();
		
		System.out.println("menu:");
		root.print();
		
		System.out.println("menu (JSON):");
		System.out.println(root.buildJson().build());
	}
	*/

	
}
