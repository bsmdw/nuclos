package org.nuclos.server.rest.services.helper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.nuclos.common.UID;
import org.nuclos.schema.rest.CollectiveProcessingObjectInfo;
import org.nuclos.schema.rest.ObjectId;

public class CollectiveProcessingExecutionEntry {

	private final String executionId;

	private final Map<Long, CollectiveProcessingObjectInfo> oInfoStore = new HashMap<>();

	private int totalCount = 0;

	private int countProcessed = 0;

	private String user;

	private long lastUpdate = System.currentTimeMillis();

	public static class ProgressInfoSinceLastRequest {
		private final long progressInPercent;
		private final List<CollectiveProcessingObjectInfo> objectInfos;
		public ProgressInfoSinceLastRequest(final long progressInPercent, final List<CollectiveProcessingObjectInfo> objectInfos) {
			this.progressInPercent = progressInPercent;
			this.objectInfos = objectInfos;
		}
		public long getProgressInPercent() {
			return progressInPercent;
		}
		public List<CollectiveProcessingObjectInfo> getObjectInfos() {
			return objectInfos;
		}
	}

	protected CollectiveProcessingExecutionEntry(@NotNull final String executionId) {
		if (executionId == null) {
			throw new IllegalArgumentException("executionId must not be null");
		}
		this.executionId = executionId;
	}

	public String getExecutionId() {
		return executionId;
	}

	public String getUser() {
		return user;
	}

	public void setUser(final String user) {
		this.user = user;
	}

	public void setTotalCount(final int totalCount) {
		synchronized (executionId) {
			this.totalCount = totalCount;
		}
	}

	public static CollectiveProcessingObjectInfo newObjectInfo(@NotNull String entityFQN, @NotNull Object id, String name, @NotNull Long infoRowNumber) {
		final ObjectId oId = new ObjectId();
		if (id instanceof UID) {
			oId.setString(((UID) id).getString());
		} else if (id instanceof Long) {
			oId.setLong((Long) id);
		} else {
			throw new IllegalArgumentException("Id class not supported: " + id.getClass().getCanonicalName());
		}
		final CollectiveProcessingObjectInfo oInfo = CollectiveProcessingObjectInfo.builder()
				.withBoMetaId(entityFQN)
				.withId(oId)
				.withInfoRowNumber(infoRowNumber)
				.withName(name)
				.build();
		return oInfo;
	}

	/**
	 * clones the CollectiveProcessingObjectInfo and stores internal for later progress requests
	 * @param oInfo
	 */
	public void storeObjectInfo(@NotNull CollectiveProcessingObjectInfo oInfo) {
		CollectiveProcessingObjectInfo oInfoToStore = (CollectiveProcessingObjectInfo) oInfo.clone();
		synchronized (executionId) {
			lastUpdate = System.currentTimeMillis();
			oInfoStore.put(oInfo.getInfoRowNumber(), oInfoToStore);
			if (!oInfo.isInProgress()) {
				countProcessed++;
			}
		}
	}

	/**
	 * the stock will be cleared afterwards
	 * @return sorted list of stored object infos
	 */
	public ProgressInfoSinceLastRequest getProgressInfoSinceLastRequest() {
		List<CollectiveProcessingObjectInfo> objectInfos;
		long progressInPercent;
		synchronized (executionId) {
			progressInPercent = totalCount == 0l ? 0L : 100 * countProcessed / totalCount;
			objectInfos = new ArrayList<>(oInfoStore.values());
			oInfoStore.clear();
		}
		objectInfos.sort(new Comparator<CollectiveProcessingObjectInfo>() {
			@Override
			public int compare(final CollectiveProcessingObjectInfo o1, final CollectiveProcessingObjectInfo o2) {
				return Long.compare(o1.getInfoRowNumber(), o2.getInfoRowNumber());
			}
		});
		// 100% == collective processing finished ... Not? => 99%!
		if (progressInPercent >= 100l && countProcessed < totalCount) {
			progressInPercent = 99l;
		}
		return new ProgressInfoSinceLastRequest(progressInPercent, objectInfos);
	}

	public long getLastUpdate() {
		return lastUpdate;
	}

}
