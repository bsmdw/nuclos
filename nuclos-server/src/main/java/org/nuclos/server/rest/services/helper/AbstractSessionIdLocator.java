package org.nuclos.server.rest.services.helper;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public abstract class AbstractSessionIdLocator {

	public static final String SESSION_COOKIE_NAME = "JSESSIONID";

	@Context protected HttpHeaders headers;
	@Context protected UriInfo uriInfo;
	@Context protected HttpServletRequest httpRequest;
	
	protected final String getSessionId() {
		String result = getSessionId("sessionId");
		if (result != null) {
			// not used by the webclient but by other clients
			return result;
		}
		Cookie cookie = headers.getCookies().get(SESSION_COOKIE_NAME);
		if (cookie != null) {
			return cookie.getValue();
		}
		return null;
	}

	private final String getSessionId(final String key) {
		List<String> sids;
		sids = getQueryParameters(key);
		if (sids != null && !sids.isEmpty()) {
			return sids.get(0);
		}
		sids = headers.getRequestHeader(key);
		if (sids != null && !sids.isEmpty()) {
			return sids.get(0);
		}
		return null;
	}

	public Set<String> getQueryParameterKeys() {
		return getQueryParameters().keySet();
	}

	protected MultivaluedMap<String, String> getQueryParameters() {
		return uriInfo.getQueryParameters();
	}

	protected List<String> getQueryParameters(String key) {
		return getQueryParameters().get(key);
	}

	private String restPath;

	/**
	 * TODO: Wrong place for this method - it has nothing to do with "SessionIdLocator"!
	 */
	private String buildRestPath() {
		if (restPath == null) {
			// Generate a protocol-relative URL, because using a specific protocol
			// does not work in situations where the connection is made via different
			// protocols because of a (reverse) proxy.
			// See NUCLOS-7818
			restPath = "//" + httpRequest.getServerName();

			// Add only non-standard ports to the URL
			int serverPort = httpRequest.getServerPort();
			if (serverPort != 80 && serverPort != 443) {
				restPath += ":" + serverPort;
			}

			restPath += httpRequest.getContextPath() + httpRequest.getServletPath();
		}
		return restPath;
	}

	/**
	 * TODO: Wrong place for this method - it has nothing to do with "SessionIdLocator"!
	 */
	protected final String restURL(String relativeUrl) {
		return buildRestPath() + relativeUrl;
	}

}
