package org.nuclos.server.rest.services;

import java.io.IOException;
import java.net.URLConnection;
import java.util.Date;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.ImageUtils;
import org.nuclos.common.NuclosFile;
import org.nuclos.common.UID;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.rest.CacheableResponseBuilder;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.misc.SessionEntityContext;
import org.nuclos.server.rest.services.helper.DataServiceHelper;
import org.nuclos.server.rest.services.helper.RestServiceInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/boImages")
@Produces(MediaType.APPLICATION_JSON)
public class BoImageService extends DataServiceHelper {

	private static final Logger LOG = LoggerFactory.getLogger(BoImageService.class);

	@GET
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Path("/{boMetaId}/{boId}/images/{imgAttrId}/{version}")
	@RestServiceInfo(identifier = "boImageValue", isFinalized = true, description = "Image file of BO attribute")
	public Response boImageValue(
			@Context final Request request,
			@PathParam("boMetaId") final String boMetaId,
			@PathParam("boId") final String boId,
			@PathParam("imgAttrId") final String imgAttrId,
			@PathParam("version") final String version
	) {
		final SessionEntityContext info = checkPermission(E.ENTITY, boMetaId, boId);
		final InternalTimestamp changedAt = getChangedAt(info.getMasterMeta(), Long.parseLong(boId));

		try {
			return new CacheableResponseBuilder(request) {
				@Override
				protected Date getLastModified() throws IOException {
					return changedAt;
				}

				@Override
				protected Response.ResponseBuilder newResponseBuilder() throws IOException {
					NuclosFile nf = byteAttributeValueWithValidation(boMetaId, boId, imgAttrId);
					return boImageResponseBuilder(nf);
				}
			}.build();
		} catch (IOException e) {
			throw new NuclosWebException(e, info.getEntity());
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Path("/temp/{token}")
	@RestServiceInfo(identifier = "boTempImageValue", isFinalized = true, description = "Temporarily uploaded image file")
	public Response boImageValue(@Context final Request request, @PathParam("token") final String token) {
		return boImageResponseBuilder(BoDocumentService.getUploadedFile(token, false)).build();
	}


	@GET
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Path("/{boMetaId}/{boId}/subBos/{refAttrId}/{subBoId}/images/{imgAttrId}/{version}")
	@RestServiceInfo(identifier = "dependence_ImageValue", isFinalized = true, description = "Image file of SubBO attribute")
	public Response boImageValue(
			@Context final Request request,
			@PathParam("boMetaId") final String boMetaId,
			@PathParam("boId") final String boId,
			@PathParam("refAttrId") final String refAttrId,
			@PathParam("subBoId") final String subBoId,
			@PathParam("imgAttrId") final String imgAttrId,
			@PathParam("version") final String version
	) {
		final SessionEntityContext info = checkReadAllowed(boMetaId, boId);
		final String subBoMetaId = getSubBoMetaId(refAttrId);
		final UID subBoUid = Rest.translateFqn(E.ENTITY, subBoMetaId);
		final EntityMeta<?> subEntityMeta = Rest.getEntity(subBoUid);

		final InternalTimestamp changedAt = getChangedAt(subEntityMeta, Long.parseLong(subBoId));

		try {
			return new CacheableResponseBuilder(request) {
				@Override
				protected Date getLastModified() throws IOException {
					return changedAt;
				}

				@Override
				protected Response.ResponseBuilder newResponseBuilder() throws IOException {
					final NuclosFile nf = byteAttributeValue(subEntityMeta, subBoId, imgAttrId, false);
					return boImageResponseBuilder(nf);
				}
			}.build();
		} catch (IOException e) {
			throw new NuclosWebException(e, info.getEntity());
		}
	}

	private Response.ResponseBuilder boImageResponseBuilder(NuclosFile nf) {
		if (nf == null) {
			return Response.noContent();
		}

		String filename = nf.getName();
		String type = URLConnection.guessContentTypeFromName(filename); //Type by Filename

		if (StringUtils.isNullOrEmpty(filename)) {
			filename = "image";
		}

		if (type == null) {
			//FALLBACK MAGIC NUMBER
			String magicString = null;
			if (nf.getContent().length > 8) {
				StringBuffer magicBuffer = new StringBuffer();
				for (int i = 0; i < 8; i++) {
					magicBuffer.append(String.format("%02x ", nf.getContent()[i]));
				}
				magicString = magicBuffer.toString();
			}
			if (magicString == null) {

			} else if (magicString.startsWith("89 50 4e 47 0d 0a 1a 0a")) {
				type = "image/png";
				filename += ".png";
			} else if (magicString.startsWith("ff d8")) {
				type = "image/jpg";
				filename += ".jpg";
			} else if (magicString.startsWith("47 49 46")) {
				type = "image/gif";
				filename += ".gif";
			} else if (magicString.startsWith("42 4d")) {
				type = "image/bmp";
				filename += ".bmp";
			} else if (magicString.startsWith("3c 3f 78 6d 6c")) {
				type = "image/svg+xml";
				filename += ".svg";
			} else {
				LOG.warn("DONT KNOW FILE-TYPE FOR MAGIC NUMBER: " + magicString);
			}
		}

		return Response.ok(scaleDownIfRequestedAndPossible(nf.getContent())).type(type);
	}

	private byte[] scaleDownIfRequestedAndPossible(byte[] imageContent) {
		try {
			MultivaluedMap<String, String> queryParameters = getQueryParameters();
			int reqHeight = 0;
			if (queryParameters.containsKey("height") && queryParameters.get("height") != null && queryParameters.get("height").size() > 0) {
				reqHeight = Integer.parseInt(queryParameters.get("height").get(0));
			}
			int reqWidth = 0;
			if (queryParameters.containsKey("width") && queryParameters.get("width") != null && queryParameters.get("width").size() > 0) {
				reqWidth = Integer.parseInt(queryParameters.get("width").get(0));
			}
			if (reqHeight > 0 || reqWidth > 0) {
				return ImageUtils.scaleImage(imageContent, reqWidth, reqHeight);
			}

		} catch (Exception ex) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Exception during production of scaled image: {}", ex.getMessage(), ex);
			} else {
				LOG.warn("Exception during production of scaled image: {}", ex.getMessage());
			}
		}
		return imageContent;
	}


	private SessionEntityContext checkReadAllowed(String boMetaId, String boId) {
		SessionEntityContext info = checkPermission(E.ENTITY, boMetaId, boId);

		try {
			Rest.facade().get(info.getEntity(), new Long(boId), true);
		} catch (Exception ex) {
			throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITY, boMetaId));
		}

		return info;
	}
}