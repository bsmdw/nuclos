package org.nuclos.server.rest.misc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

import org.nuclos.server.rest.services.rvo.JsonFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @deprecated Use schema classes from org.nuclos.schema.rest!
 */
@Deprecated
public class RestLinks {
	
	private static final Logger LOG = LoggerFactory.getLogger(RestLinks.class);

	private JsonObjectBuilder parentObject;
	
	private Map<String, RestLink> mpLinks = new HashMap<>();
	private List<String> keys = new ArrayList<String>();
	
	public RestLinks() {
		this(null);
	}

	public RestLinks(JsonObjectBuilder parentObject) {
		this.parentObject = parentObject;
	}

	public void buildJson(IWebContext context) {
		if (isEmpty()) {
			return;
		}
		
		JsonObjectBuilder links = Json.createObjectBuilder();
		for (String rel: keys) {
			RestLink link = mpLinks.get(rel);
			JsonObjectBuilder linkObject = Json.createObjectBuilder();
			
			String href = context.getRestURI(link.getHref(), link.getParams());
			if(href!=null) {
				linkObject.add("href",href);
			} else {
				LOG.error("Unable to build link for: {}", link.getHref());
			}
			linkObject.add("methods", JsonFactory.buildJsonArrayOfString(link.getVerbs().toArray()));
			links.add(rel, linkObject);
		}
		
		parentObject.add("links", links);
	}
	
	public RestLink addLink(String rel, Verbs verbs, String... params) {
		return this.addLinkHref(rel, rel, verbs, params);
	}
	
	public RestLink addLinkHref(String rel, String href, Verbs verbs, Object... params) {
		if (!mpLinks.containsKey(rel)) {
			RestLink link = new RestLink(href, verbs, params);
			mpLinks.put(rel, link);
			keys.add(rel);
			return link;
		} else {
			//The reason for all those "addings" is an unclear using of BoLinksFactory in ResultRVO for multiple bos.
			//TOOD: This has to be done consistently.
			//LOG.warn("Link: '" + href + "' already added: " + rel + "': " + mpLinks.get(rel).getHref());
			throw new IllegalArgumentException();
		}
	}
	
	public Map<String, RestLink> getLinks() {
		return mpLinks;
	}
	
	public void setParent (JsonObjectBuilder parent) {
		this.parentObject = parent;
	}
	
	public int count() {
		return mpLinks.size();
	}
	
	public boolean isEmpty() {
		return mpLinks.isEmpty();
	}

	/**
	 * TODO: Refactor to schema classes.
	 *
	 * @deprecated Strange construct.
	 */
	@Deprecated
	public enum Verbs {
			GET(new String[]{"GET"}),
			GET_PUT(new String[]{"GET","PUT"}), 
			GET_POST(new String[]{"GET","POST"}), 
			GET_DELETE(new String[]{"GET","DELETE"}), 
			GET_PUT_POST(new String[]{"GET","PUT","POST"}), 
			GET_PUT_DELETE(new String[]{"GET","PUT","DELETE"}), 
			GET_POST_DELETE(new String[]{"GET","POST","DELETE"}), 
			GET_PUT_POST_DELETE(new String[]{"GET","PUT","POST","DELETE"}),
			POST(new String[]{"POST"}),
			DELETE(new String[]{"DELETE"});
		
		private final String[] array;
		
		private Verbs(String[] array) {
			this.array = array;  
		}
		
		public String[] toArray() {
			return array;
		}
	}
	
	public static class RestLink {
		private final String href;
		private final Object[] params;
		private Verbs verbs;
		private boolean protectVerbs = false;
		
		private RestLink(String href, Verbs verbs, Object... params) {
			if (verbs == null) {
				throw new IllegalArgumentException("verbs must not be null");
			}
			this.href = href;
			this.params = params;
			this.verbs = verbs;
		}
		
		public String getHref() {
			return href;
		}
		
		public Object[] getParams() {
			return params;
		}

		public Verbs getVerbs() {
			return verbs;
		}
		
		public void setVerbPut() {
			if (protectVerbs) {
				return;
			}
			switch (verbs) {
			case GET:
				verbs = Verbs.GET_PUT;
				break;
			case GET_POST:
				verbs = Verbs.GET_PUT_POST;
				break;
			case GET_DELETE:
				verbs = Verbs.GET_PUT_DELETE;
				break;
			case GET_POST_DELETE:
				verbs = Verbs.GET_PUT_POST_DELETE;
				break;
			default:
				break;
			}
		}
		
		public void setVerbPost() {
			if (protectVerbs) {
				return;
			}
			switch (verbs) {
			case GET:
				verbs = Verbs.GET_POST;
				break;
			case GET_PUT:
				verbs = Verbs.GET_PUT_POST;
				break;
			case GET_DELETE:
				verbs = Verbs.GET_POST_DELETE;
				break;
			case GET_PUT_DELETE:
				verbs = Verbs.GET_PUT_POST_DELETE;
				break;
			default:
				break;
			}
		}

		public void setVerbDelete() {
			if (protectVerbs) {
				return;
			}
			switch (verbs) {
			case GET:
				verbs = Verbs.GET_DELETE;
				break;
			case GET_PUT:
				verbs = Verbs.GET_PUT_DELETE;
				break;
			case GET_POST:
				verbs = Verbs.GET_POST_DELETE;
				break;
			case GET_PUT_POST:
				verbs = Verbs.GET_PUT_POST_DELETE;
				break;
			default:
				break;
			}
		}

		public boolean isProtectVerbs() {
			return protectVerbs;
		}

		public RestLink protectVerbs() {
			this.protectVerbs = true;
			return this;
		}
		
	}

	public void clear() {
		mpLinks.clear();
		keys.clear();
	}

}
