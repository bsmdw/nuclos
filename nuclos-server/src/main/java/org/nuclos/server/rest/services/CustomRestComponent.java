package org.nuclos.server.rest.services;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;

import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.common.EventSupportCache;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.customcode.CustomCodeManager;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.rest.ejb3.Rest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.google.common.collect.ImmutableSet;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Component
public class CustomRestComponent {
	private static final Logger LOG = LoggerFactory.getLogger(CustomRestComponent.class);

	private final SecurityCache securityCache;
	private final EventSupportCache eventSupportCache;
	private final CustomCodeManager customCodeManager;

	private static final Set<Class<?>> METHOD_CLASSES = ImmutableSet.of(
			GET.class,
			POST.class,
			PUT.class,
			DELETE.class,
			HEAD.class,
			OPTIONS.class,
			PATCH.class
	);

	public CustomRestComponent(
			final SecurityCache securityCache,
			final EventSupportCache eventSupportCache,
			final CustomCodeManager customCodeManager
	) {
		this.securityCache = securityCache;
		this.eventSupportCache = eventSupportCache;
		this.customCodeManager = customCodeManager;
	}

	public Set<EndpointInfo> getAllowedCustomRestRules() {
		Set<EndpointInfo> result = new HashSet<>();

		securityCache.getAllowedCustomRestRules(
				Rest.facade().getCurrentUserName(),
				Rest.facade().getCurrentMandatorUID()
		).values().stream().filter(className -> {
			EventSupportSourceVO essV0 = eventSupportCache.getEventSupportSourceByClassname(className);
			return essV0 != null && essV0.isActive();
		}).map(className -> {
			final ClassLoader classLoader;
			try {
				classLoader = this.customCodeManager.getClassLoaderAndCompileIfNeeded();
				return classLoader.loadClass(className);
			} catch (NuclosCompileException | ClassNotFoundException e) {
				LOG.error("Could not load CustomRestRule {}", className, e);
			}
			return null;
		}).filter(Objects::nonNull)
				.forEach(cls -> {
					for (Method method : cls.getDeclaredMethods()) {
						for (Annotation annotation : method.getAnnotations()) {
							for (Class<?> methodClass: METHOD_CLASSES) {
								if (methodClass.isAssignableFrom(annotation.getClass())) {
									result.add(new EndpointInfo(methodClass, cls, method));
								}
							}
						}
					}
				});
		return result;
	}

	public static class EndpointInfo {
		private final Class methodType;
		private final Class customRestClass;
		private final Method customRestMethod;

		private EndpointInfo(
				final Class methodType,
				final Class customRestClass,
				final Method customRestMethod
		) {
			this.methodType = methodType;
			this.customRestClass = customRestClass;
			this.customRestMethod = customRestMethod;
		}

		public Class getMethodType() {
			return methodType;
		}

		public Class getCustomRestClass() {
			return customRestClass;
		}

		public Method getCustomRestMethod() {
			return customRestMethod;
		}
	}
}
