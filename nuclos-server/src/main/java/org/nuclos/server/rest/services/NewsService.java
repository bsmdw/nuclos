package org.nuclos.server.rest.services;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.nuclos.api.exception.BusinessException;
import org.nuclos.businessentity.News;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.services.helper.RestServiceInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.web.bind.annotation.RestController;

/**
 * Provides some extra functionality and workarounds around the News BO.
 */
@Path("/news")
@RestController
@Configurable
public class NewsService {

	@Autowired
	private org.nuclos.server.news.NewsService newsService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@RestServiceInfo(identifier = "news", validateSession = false, description = "Returns all currently valid news.")
	public List<org.nuclos.schema.rest.News> getCurrentNews() {
		return newsService.getCurrentNews()
				.stream()
				.map(this::toRestNews)
				.collect(Collectors.toCollection(LinkedList::new));
	}

	@GET
	@Path("/confirmationRequired")
	@Produces(MediaType.APPLICATION_JSON)
	@RestServiceInfo(identifier = "newsConfirmationRequired", validateSession = false, description = "Returns all news whose confirmation is required at login.")
	public List<org.nuclos.schema.rest.News> getNewsConfirmationRequired() {
		return newsService.getNewsConfirmationRequired()
				.stream()
				.map(this::toRestNews)
				.collect(Collectors.toCollection(LinkedList::new));
	}

	@GET
	@Path("/unconfirmed")
	@Produces(MediaType.APPLICATION_JSON)
	@RestServiceInfo(identifier = "newsConfirmationRequired", description = "Returns News that require confirmation but are not confirmed by the current user yet.")
	public List<org.nuclos.schema.rest.News> getUnconfirmedNewsForCurrentUser() {
		return newsService.getUnconfirmedNewsForCurrentUser()
				.stream()
				.map(this::toRestNews)
				.collect(Collectors.toCollection(LinkedList::new));
	}

	@GET
	@Path("/unread")
	@Produces(MediaType.APPLICATION_JSON)
	@RestServiceInfo(identifier = "newsUnread", description = "Unread News for the current user that should be shown at startup.")
	public List<org.nuclos.schema.rest.News> getUnreadNewsForCurrentUser() {
		return newsService.getUnreadNewsForCurrentUser()
				.stream()
				.map(this::toRestNews)
				.collect(Collectors.toCollection(LinkedList::new));
	}

	@GET
	@Path("/{news}")
	@Produces(MediaType.APPLICATION_JSON)
	@RestServiceInfo(identifier = "singleNews", validateSession = false, description = "Fetches the News with the given FQN.")
	public org.nuclos.schema.rest.News getSingleNews(@PathParam("news") final String newsFqn) {
		UID newsId = Rest.translateFqn(E.NEWS, newsFqn);

		return toRestNews(News.get(newsId));
	}

	@PUT
	@Path("/{news}/confirmed")
	@Produces(MediaType.APPLICATION_JSON)
	@RestServiceInfo(identifier = "newsConfirmed", description = "Confirms the given News for the current user.")
	public void newsConfirmed(@PathParam("news") final String newsFqn) {
		UID newsId = Rest.translateFqn(E.NEWS, newsFqn);

		try {
			newsService.newsConfirmed(newsId);
		} catch (BusinessException e) {
			throw NuclosWebException.wrap(e);
		}
	}

	@PUT
	@Path("/{news}/viewed")
	@Produces(MediaType.APPLICATION_JSON)
	@RestServiceInfo(identifier = "newsViewed", description = "Marks the given News as viewed for the current user.")
	public void newsViewed(@PathParam("news") final String newsFqn) {
		UID newsId = Rest.translateFqn(E.NEWS, newsFqn);

		try {
			newsService.newsViewed(newsId);
		} catch (BusinessException e) {
			throw NuclosWebException.wrap(e);
		}
	}

	/**
	 * TODO: Create some central entity for Server and Client, and get rid of this conversion.
	 */
	private org.nuclos.schema.rest.News toRestNews(org.nuclos.businessentity.News news) {
		org.nuclos.schema.rest.News result = new org.nuclos.schema.rest.News();

		try {
			result.setActive(Boolean.TRUE.equals(news.getActive()));
			result.setConfirmationRequired(Boolean.TRUE.equals(news.getConfirmationRequired()));
			result.setContent(news.getContent());
			result.setId(news.getId().toString());
			result.setName(news.getName());
			result.setRevision(news.getRevision());
			result.setShowAtStartup(Boolean.TRUE.equals(news.getShowAtStartup()));
			result.setTitle(news.getTitle());
			result.setValidFrom(toXMLGregorian(news.getValidFrom()));
			result.setValidUntil(toXMLGregorian(news.getValidUntil()));
			result.setPrivacyPolicy(Boolean.TRUE.equals(news.getPrivacyPolicy()));
		} catch (DatatypeConfigurationException e) {
			throw NuclosWebException.wrap(e);
		}

		return result;
	}


	private XMLGregorianCalendar toXMLGregorian(Date date) throws DatatypeConfigurationException {
		if (date == null) {
			return null;
		}

		GregorianCalendar c = new GregorianCalendar();
		c.setTime(date);
		return DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
	}
}
