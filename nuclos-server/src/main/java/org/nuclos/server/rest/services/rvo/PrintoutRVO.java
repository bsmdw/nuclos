package org.nuclos.server.rest.services.rvo;

import java.util.ArrayList;
import java.util.List;

import javax.json.JsonObjectBuilder;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.report.valueobject.DefaultReportVO;

public class PrintoutRVO extends AbstractJsonRVO<UID> {
	
	private final DefaultReportVO reportVO;
	private List<JsonRVO> outputFormats;
	
	private final String boId;

	public PrintoutRVO(String boId, DefaultReportVO reportVO) {
		super(E.REPORT);
		this.reportVO = reportVO;
		this.boId = boId;
	}

	public void addOutputFormat(OutputFormatRVO outputformat) {
		if (outputFormats == null) {
			 outputFormats = new ArrayList<JsonRVO>();
		}
		outputFormats.add(outputformat);
		outputformat.setBoId(boId);
	}
	
	public List<OutputFormatRVO> getOutputFormats() {
		List<OutputFormatRVO> result = new ArrayList<OutputFormatRVO>();
		if (outputFormats != null) {
			for (JsonRVO format : outputFormats) {
				result.add((OutputFormatRVO) format);
			}
		}
		return result;
	}
	
	@Override
	public JsonObjectBuilder getJSONObjectBuilder() {
		NuclosJsonObjectBuilder json = new NuclosJsonObjectBuilder();
		
		Object pk = getPKForJson((UID)reportVO.getId());
		json.add("printoutId", pk);
		json.add("name", reportVO.getName());
		
		addArray(json, "outputFormats", outputFormats);
		
		return json;
	}
	
}
