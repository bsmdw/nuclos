package org.nuclos.server.parameter;

import org.nuclos.api.ide.valueobject.SourceType;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants;
import org.nuclos.server.nbo.AbstractNuclosObjectCompiler;
import org.springframework.stereotype.Component;

@Component
public class NuclosParameterObjectCompiler extends AbstractNuclosObjectCompiler {

	public NuclosParameterObjectCompiler() {
		super(SourceType.PARAMETER,
		      NuclosCodegeneratorConstants.PARAMETERJARFILE,
		      NuclosCodegeneratorConstants.PARAMETER_SRC_DIR_NAME);
	}

}
