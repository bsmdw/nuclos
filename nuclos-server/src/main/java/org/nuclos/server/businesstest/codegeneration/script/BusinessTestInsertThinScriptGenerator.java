package org.nuclos.server.businesstest.codegeneration.script;

import java.util.Collection;
import java.util.Comparator;
import java.util.stream.Collectors;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.server.businesstest.IBusinessTestLogger;
import org.nuclos.server.businesstest.codegeneration.BusinessTestGenerationContext;
import org.nuclos.server.businesstest.codegeneration.IBusinessTestNucletCache;
import org.nuclos.server.businesstest.codegeneration.IBusinessTestSampleDataProvider;

/**
 * Generates insert tests that insert thin records (only mandatory attributes filled).
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class BusinessTestInsertThinScriptGenerator extends BusinessTestInsertScriptGenerator {

	BusinessTestInsertThinScriptGenerator(
			final EntityMeta<?> meta,
			final IBusinessTestNucletCache nucletCache,
			final IBusinessTestLogger logger,
			final BusinessTestGenerationContext context,
			final IBusinessTestSampleDataProvider sampleDataProvider
	) {
		super(meta, nucletCache, logger, context, sampleDataProvider);
	}

	public String getTestName() {
		return "INSERT THIN " + meta.getEntityName();
	}


	@Override
	Collection<FieldMeta<?>> getFields() {
		return getMeta().getFields().stream()
				.filter(it -> !it.isSystemField() && !it.isNullable())
				.sorted(Comparator.comparing(FieldMeta::getFieldName))
				.collect(Collectors.toList());
	}
}
