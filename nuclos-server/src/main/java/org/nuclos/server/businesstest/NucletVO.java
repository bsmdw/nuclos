package org.nuclos.server.businesstest;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class NucletVO {
	private final UID uid;
	private final String pkg;

	public NucletVO(MasterDataVO<UID> mdvo) {
		this(mdvo.getPrimaryKey(), mdvo.getFieldValue(E.NUCLET.packagefield));
	}

	public NucletVO(final UID uid, final String pkg) {
		this.uid = uid;
		this.pkg = pkg;
	}

	public UID getUid() {
		return uid;
	}

	public String getPackage() {
		return pkg;
	}
}
