package org.nuclos.server.businesstest.sampledata;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class SampleDataGeneratorFactory {
	private static final Map<Class<?>, Class<? extends SampleDataGenerator>> registry = new HashMap<>();

	static {
		SampleDataGeneratorFactory.register(String.class, SampleTextDataGenerator.class);
		SampleDataGeneratorFactory.register(Boolean.class, SampleBooleanDataGenerator.class);
		SampleDataGeneratorFactory.register(Date.class, SampleDateDataGenerator.class);
		SampleDataGeneratorFactory.register(Integer.class, SampleIntegerDataGenerator.class);
		SampleDataGeneratorFactory.register(Double.class, SampleDoubleDataGenerator.class);
		SampleDataGeneratorFactory.register(BigDecimal.class, SampleBigDecimalDataGenerator.class);
	}

	private static <T> void register(Class<T> cls, Class<? extends SampleDataGenerator<T>> generatorClass) {
		registry.put(cls, generatorClass);
	}

	public static <T> SampleDataGenerator<T> newDataGeneratorFor(Class<T> cls) throws IllegalAccessException, InstantiationException {
		if (!registry.containsKey(cls)) {
			return null;
		}

		Class<? extends SampleDataGenerator<T>> generatorClass = (Class<? extends SampleDataGenerator<T>>) lookup(cls);
		SampleDataGenerator<T> generator = generatorClass.newInstance();

		return generator;
	}

	private static Class<? extends SampleDataGenerator> lookup(Class<?> cls) {
		for (Map.Entry<Class<?>, Class<? extends SampleDataGenerator>> entry : registry.entrySet()) {
			if (entry.getKey().isAssignableFrom(cls)) {
				return entry.getValue();
			}
		}
		return null;
	}
}
