package org.nuclos.server.businesstest.codegeneration;

import java.util.List;

import org.nuclos.common.FieldMeta;

/**
 * Provides sample data for a given entity field.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public interface IBusinessTestSampleDataProvider {

	/**
	 * Returns a new sample value for the given field.
	 */
	<T> T getSampleData(FieldMeta<T> field);

	/**
	 * Returns "count" new sample values for the given field.
	 */
	<T> List<T> getSampleData(FieldMeta<T> field, int count);
}
