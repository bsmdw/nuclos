package org.nuclos.server.businesstest.codegeneration.script;

import org.nuclos.common.EntityMeta;
import org.nuclos.server.businesstest.IBusinessTestLogger;
import org.nuclos.server.businesstest.codegeneration.BusinessTestGenerationContext;
import org.nuclos.server.businesstest.codegeneration.IBusinessTestNucletCache;
import org.nuclos.server.businesstest.codegeneration.source.BusinessTestScriptSource;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class BusinessTestUpdateScriptGenerator extends AbstractBusinessTestScriptGenerator {
	BusinessTestUpdateScriptGenerator(
			final EntityMeta<?> meta,
			final IBusinessTestNucletCache nucletCache,
			final IBusinessTestLogger logger,
			final BusinessTestGenerationContext context) {
		super(meta, nucletCache, logger, context);
	}

	public String getTestName() {
		return "UPDATE " + meta.getEntityName();
	}

	@Override
	public BusinessTestScriptSource generateScriptSource() {
		final String className = getClassName();
		final BusinessTestScriptSource script = createDefaultScript();

		script.addScriptLine("List<" + className + "> bos = " + className + ".list(10)");
		script.addScriptLine("");
		script.addScriptLine("if (bos.empty) {");
		script.addScriptLine("	fail('Could not run test: no records available')");
		script.addScriptLine("}");
		script.addScriptLine("");
		script.addScriptLine("bos.each { " + className + " bo ->");
		// TODO: Change some field values
		script.addScriptLine("	attempt {");
		script.addScriptLine("		bo.save()");
		script.addScriptLine("	}");
		script.addScriptLine("}");

		return script;
	}
}
