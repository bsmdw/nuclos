package org.nuclos.server.businesstest.codegeneration;

import org.nuclos.common.UID;
import org.nuclos.server.businesstest.NucletVO;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public interface IBusinessTestNucletCache {
	NucletVO getNuclet(UID nucletUID);

	NucletVO getNucletByPackage(String pkg);
}
