package org.nuclos.server.businesstest.execution;

import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.lang.NotImplementedException;
import org.nuclos.api.businessobject.Flag;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentKey;

/**
 * Dependents of a BusinessTestEO.
 *
 * @author Andreas Lämmlein <andreas@laemm-line.de>
 */
public class BusinessTestEODependents<T extends BusinessTestEO> implements Collection<T> {

	private final BusinessTestEO<? extends BusinessTestEO> parentEO;
	private final UID dependentUID;
	private final UID dependentFieldUID;
	protected final Class<T> cls;
	protected final Flag[] flags;

	public BusinessTestEODependents(
			final BusinessTestEO parentEO,
			final UID dependentUID,
			final UID dependentFieldUID,
			final Class<T> cls,
			final Flag[] flags) {
		this.parentEO = parentEO;
		this.dependentUID = dependentUID;
		this.dependentFieldUID = dependentFieldUID;
		this.cls = cls;
		this.flags = flags;
	}

	private Collection<EntityObjectVO<?>> getDependentEOs() {
		return parentEO.getDependentEOs(dependentUID, dependentFieldUID, flags);
	}

	public void leftShift(T dependent) {
		add(dependent);
	}

	public boolean add(T dependent) {
		if (!dependent.getEntityUID().equals(dependentUID)) {
			throw new IllegalArgumentException("Can not add dependent of type " + dependent.getEntityUID() + "" +
					"to dependent collection of type " + dependentUID);
		}
		IDependentKey dependentKey = DependentDataMap.createDependentKey(dependentFieldUID);
		parentEO.addDependent(dependentKey, dependent.getWrappedEO());
		return true;    // FIXME: Check if the dependent could really be added
	}

	public T getById(final Long id) {
		for (EntityObjectVO<?> eo : getDependentEOs()) {
			if (id.equals(eo.getPrimaryKey())) {
				try {
					BusinessTestEO.from((EntityObjectVO<Long>) eo, cls);
				} catch (Exception e) {
					throw new RuntimeException("Could not get dependent of type " + dependentUID + " with id " + id, e);
				}
			}
		}

		return null;
	}

	public T get(final int index) {
		int i = 0;
		for (EntityObjectVO<?> eo : getDependentEOs()) {
			if (i == index) {
				try {
					return BusinessTestEO.from((EntityObjectVO<Long>) eo, cls);
				} catch (Exception e) {
					throw new RuntimeException("Could not get dependent of type " + dependentUID + " at index " + index, e);
				}
			}
		}
		return null;
	}

	@Override
	public boolean remove(final Object o) {
		((T)o).getWrappedEO().flagRemove();
		return true;    // FIXME: Check if the dependent could really be removed
	}

	@Override
	public boolean containsAll(final Collection<?> c) {
		throw new NotImplementedException();
	}

	@Override
	public boolean addAll(final Collection<? extends T> c) {
		throw new NotImplementedException();
	}

	@Override
	public boolean removeAll(final Collection<?> c) {
		throw new NotImplementedException();
	}

	@Override
	public boolean retainAll(final Collection<?> c) {
		throw new NotImplementedException();
	}

	@Override
	public void clear() {
		throw new NotImplementedException();
	}

	@Override
	public int size() {
		return getDependentEOs().size();
	}

	@Override
	public boolean isEmpty() {
		return getDependentEOs().isEmpty();
	}

	@Override
	public boolean contains(final Object o) {
		throw new NotImplementedException();
	}

	@Override
	public Iterator<T> iterator() {
		final Iterator iter = getDependentEOs().iterator();
		return new Iterator<T>() {
			@Override
			public boolean hasNext() {
				return iter.hasNext();
			}

			@Override
			public T next() {
				try {
					return BusinessTestEO.<T>from((EntityObjectVO<Long>) iter.next(), cls);
				} catch (Exception e) {
					throw new RuntimeException("Could not get dependent of type " + dependentUID, e);
				}
			}

			@Override
			public void remove() {
				throw new NotImplementedException();
			}
		};
	}

	@Override
	public Object[] toArray() {
		throw new NotImplementedException();
	}

	@Override
	public <T1> T1[] toArray(final T1[] a) {
		throw new NotImplementedException();
	}
}
