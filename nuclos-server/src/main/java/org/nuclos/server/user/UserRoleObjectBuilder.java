package org.nuclos.server.user;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;

import org.nuclos.api.common.NuclosRole;
import org.nuclos.common.E;
import org.nuclos.common.NuclosEntityValidator;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.common.INucletCache;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorUtils;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.nbo.AbstractNuclosObjectCompiler.NuclosBusinessJavaSource;
import org.nuclos.server.nbo.NuclosObjectBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("nuclosUserRoleObjectBuilder")
public class UserRoleObjectBuilder extends NuclosObjectBuilder {

	public static final String DEFFAULT_PACKAGE_NUCLET = "org.nuclet.userrole";
	private static final String DEFFAULT_ENTITY_PREFIX = "UR";
	private static final String DEFFAULT_ENTITY_POSTFIX = "UR";

	@Autowired
	private UserRoleObjectCompiler compiler;

	@Autowired
	private MetaProvider provider;

	@Autowired
	private NucletDalProvider nucletDalProvider;

	public UserRoleObjectBuilder() {}
	
	public static String getNameForFqn(String sName) {
		return formatMethodName(NuclosEntityValidator.escapeJavaIdentifier(sName, DEFFAULT_ENTITY_PREFIX)) + DEFFAULT_ENTITY_POSTFIX;
	}
	
	public static String getNucletPackageStatic(UID nucletUID, INucletCache nucletCache) {
		String retVal = DEFFAULT_PACKAGE_NUCLET;

		if (nucletUID != null) {
			String sFqn = nucletCache.getFullQualifiedNucletName(nucletUID);
			if (!StringUtils.looksEmpty(sFqn)) {
				retVal = sFqn;
			}
		}

		return retVal;
	}
	
	public void createObjects(final ForkJoinPool builderThreadPool) throws CommonBusinessException, InterruptedException {
		List<NuclosBusinessJavaSource> retVal = new ArrayList<>();
			
		// get all roles
		CollectableSearchExpression clctExpr = new CollectableSearchExpression(SearchConditionUtils.newComparison(E.ROLE.withRuleClass, ComparisonOperator.EQUAL, Boolean.TRUE));
		List<EntityObjectVO<UID>> allRoles = nucletDalProvider.getEntityObjectProcessor(E.ROLE).getBySearchExpression(clctExpr);
				
		if (allRoles != null && allRoles.size() > 0) {
			for (EntityObjectVO<UID> eoVO : allRoles) {
				if (Thread.currentThread().isInterrupted()) {
					throw new InterruptedException();
				}
				retVal.add(createJavaSourceFile(eoVO));
			}
		}
		
		if (retVal.size() > 0) {
			compiler.compileSourcesAndJar(builderThreadPool, retVal);
		}
		
	}

	private NuclosBusinessJavaSource createJavaSourceFile(EntityObjectVO<UID> eoVO) {
		
		final String sPackage = getNucletPackageStatic(eoVO.getFieldUid(E.ROLE.nuclet), provider);
		final String unformatedEntity = NuclosEntityValidator.escapeJavaIdentifier(eoVO.getFieldValue(E.ROLE.name), DEFFAULT_ENTITY_PREFIX);
		final String formatEntity = unformatedEntity.substring(0, 1).toUpperCase() + unformatedEntity.substring(1) + DEFFAULT_ENTITY_POSTFIX;
		final String qname = sPackage + "." + formatEntity;
		final String filename = NuclosCodegeneratorUtils.userRoleSource(sPackage, formatEntity).toString();
		
		final StringBuilder builder = new StringBuilder();
		builder.append("package ").append(sPackage).append(";\n\n");
		builder.append("import org.nuclos.common.UID;\n\n");
		builder.append("public class ").append(formatEntity).append(" implements ")
			.append(NuclosRole.class.getCanonicalName()).append(" {");
		builder.append("\n\npublic org.nuclos.api.UID getId() {\n\treturn UID.parseUID(\"")
			.append(eoVO.getPrimaryKey().getString()).append("\"); \n}\n");
		builder.append("\n}");
		
		return new NuclosBusinessJavaSource(qname, filename, builder.toString(), false);
	}
	
}
