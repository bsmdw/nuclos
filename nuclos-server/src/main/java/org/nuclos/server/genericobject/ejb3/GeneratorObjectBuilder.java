package org.nuclos.server.genericobject.ejb3;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;

import org.nuclos.api.generation.Generation;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.NuclosEntityValidator;
import org.nuclos.common.UID;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.common.GenerationCache;
import org.nuclos.server.common.INucletCache;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorUtils;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.nbo.AbstractNuclosObjectCompiler.NuclosBusinessJavaSource;
import org.nuclos.server.nbo.NuclosBusinessObjectBuilder;
import org.nuclos.server.nbo.NuclosObjectBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("nuclosGeneratorObjectBuilder")
public class GeneratorObjectBuilder extends NuclosObjectBuilder {

	private static final Logger LOG = LoggerFactory.getLogger(GeneratorObjectBuilder.class);

	private static final String DEFFAULT_PACKAGE_NUCLET = "org.nuclet.generation";
	private static String DEFAULT_PREFIX = "Gen";
	private static String DEFAULT_POSTFIX = "GEN";

	@Autowired
	private MetaProvider provider;
	
	@Autowired
	private GenerationCache genCache;

	@Autowired
	private MasterDataFacadeLocal masterDataFacade;

	@Autowired
	private GeneratorObjectCompiler compiler;

	/**
	 * This method extracts a list of all user and system entities for which Nuclos Business Objects (NBOs)
	 * must be created. Elements in the list are compiled and stored in the Classpath
	 * 
	 * @throws CommonBusinessException
	 * @param builderThreadPool
	 */
	public void createObjects(final ForkJoinPool builderThreadPool) throws CommonBusinessException, InterruptedException {
		
		// get all statemodels
		List<GeneratorActionVO> allModels = genCache.getGeneratorActions();
		
		List<NuclosBusinessJavaSource> busiJavaSource = new ArrayList<>();
		
		if (allModels != null && allModels.size() > 0) {
			// get all state for a statemodell and create class
			for (GeneratorActionVO eoVO : allModels) {
				if (Thread.currentThread().isInterrupted()) {
					throw new InterruptedException();
				}
				busiJavaSource.add(createJavaSourceFile(eoVO));
			}

			compiler.compileSourcesAndJar(builderThreadPool, busiJavaSource);
		}	
	}

	private NuclosBusinessJavaSource createJavaSourceFile(GeneratorActionVO generatorActionVO) {
		final String sPackage = getNucletPackageStatic(generatorActionVO.getNuclet(), provider);
		final String entity = createFilename(generatorActionVO);
		final String qname = sPackage + "." + entity;
		final String filename = NuclosCodegeneratorUtils.generatorSource(
				sPackage, entity).toString();
		final String filecontent = createFilecontent(generatorActionVO);

		return new NuclosBusinessJavaSource(
				qname, filename, filecontent, true);
	}

	private String createFilename(GeneratorActionVO generatorActionVO) {
		return getNameForFqn(generatorActionVO.getName());
	}
	
	public static String getNameForFqn(String sName) {
		return formatMethodName(NuclosEntityValidator.escapeJavaIdentifier(sName, DEFAULT_PREFIX)) + DEFAULT_POSTFIX;
	}
	

	private String createFilecontent(GeneratorActionVO generatorActionVO) {
		StringBuilder fileContent = new StringBuilder();
		
		EntityMeta<?> sourceEntity = MetaProvider.getInstance().getEntity(generatorActionVO.getSourceModule());
		String sourceEntityName = NuclosBusinessObjectBuilder.getNameForFqn(sourceEntity.getEntityName());
		
		EntityMeta<?> targetEntity = MetaProvider.getInstance().getEntity(generatorActionVO.getTargetModule());
		String targetEntityName = NuclosBusinessObjectBuilder.getNameForFqn(targetEntity.getEntityName());
		
		String packageSource = getEntityPackage(sourceEntity.getUID());
		String packageTarget = getEntityPackage(targetEntity.getUID());

		// package
		fileContent.append("package ")
			.append(getNucletPackageStatic(generatorActionVO.getNuclet(), provider))
			.append("; \n\n");
		
		// imports
		fileContent.append("import ").append(Generation.class.getCanonicalName()).append("; \n");
		fileContent.append("import ").append(UID.class.getCanonicalName()).append(";\n");
		fileContent.append("import ").append(packageSource).append(".").append(sourceEntityName)
			.append("; \n\n");
		if (!sourceEntity.getUID().equals(targetEntity.getUID()))
			fileContent.append("import ").append(packageTarget).append(".").append(targetEntityName)
				.append("; \n\n");

		// class
		fileContent.append("public class ").append(createFilename(generatorActionVO))
			.append(" implements Generation<").append(packageSource).append(".")
			.append(sourceEntityName).append(", ").append(packageTarget).append(".")
			.append(targetEntityName).append("> { \n");
		
		fileContent.append("public org.nuclos.api.UID getId() {\n\treturn UID.parseUID(\"")
			.append(generatorActionVO.getId().getString()).append("\");\n}\n");
		
		fileContent.append("public Class<").append(packageSource).append(".")
			.append(sourceEntityName).append("> getSourceModule() {\n return ")
			.append(packageSource).append(".").append(sourceEntityName).append(".class;\n}\n");
		fileContent.append("public Class<").append(packageTarget).append(".")
			.append(targetEntityName).append("> getTargetModule() {\n return ")
			.append(packageTarget).append(".").append(targetEntityName).append(".class;\n}\n");
		
		fileContent.append("\n}");
		
		return fileContent.toString();
	}

	private String getEntityPackage(UID entityId) {
		
		EntityMeta<?> emdVO = MetaProvider.getInstance().getEntity(entityId);
		
		String retVal = E.isNuclosEntity(emdVO.getUID())? 
				NuclosBusinessObjectBuilder.DEFFAULT_PACKAGE_NUCLOS_SYSTEM_FOR_API :
				        NuclosBusinessObjectBuilder.DEFFAULT_PACKAGE_NUCLET;
		
		UID nucletUID = emdVO.getNuclet();
		if (nucletUID != null) {
			String sNucletPackage =  provider.getNuclet(nucletUID).getFieldValue(E.NUCLET.packagefield);
			if (!StringUtils.looksEmpty(sNucletPackage)) {
				retVal = sNucletPackage;
			}
		}

		return retVal;
	}
	
	public static String getNucletPackageStatic(UID nucletUID, INucletCache nucletCache) {
		String retVal = DEFFAULT_PACKAGE_NUCLET;

		if (nucletUID != null) {
			String sFqn = nucletCache.getFullQualifiedNucletName(nucletUID);
			if (!StringUtils.looksEmpty(sFqn)) {
				retVal = sFqn;
			}
		}

		return retVal;
	}
	
}
