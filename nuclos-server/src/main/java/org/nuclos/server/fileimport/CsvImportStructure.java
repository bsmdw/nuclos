//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.fileimport;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.NuclosScript;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.genericobject.GenericObjectImportUtils;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.fileimport.parser.CsvFileImportParserFactory;
import org.nuclos.server.attribute.valueobject.AttributeCVO;
import org.nuclos.server.common.AttributeCache;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.ServerServiceLocator;
import org.nuclos.server.genericobject.Modules;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * Class representing an invoice structure definition.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:ramin.goettlich@novabit.de">ramin.goettlich</a>
 * @version	00.01.000
 */
@Configurable
public class CsvImportStructure extends AbstractImportStructure {

	private final String sDelimiter;
	private final Integer iHeaderLineCount;
	
	// Spring injection
	
	@Autowired
	CsvFileImportParserFactory parserfactory;
	
	@Autowired
	private MasterDataFacadeLocal masterDataFacade;
	
	@Autowired
	private AttributeCache attributeCache;
	
	@Autowired
	private MetaProvider metaProvider;
	
	@Autowired
	Modules modules;
	
	// end of Spring injection
	
	public static CsvImportStructure newCsvImportStructure(final UID importStructureUid) {
		try {
			final MasterDataFacadeLocal mdfacade = ServerServiceLocator.getInstance().getFacade(
					MasterDataFacadeLocal.class);

			final MasterDataVO<UID> mdcvoStructure = mdfacade.get(E.IMPORT, importStructureUid);

			final String name = (String) mdcvoStructure.getFieldValue(E.IMPORT.name);
			final String sDelimiter = (String) mdcvoStructure.getFieldValue(E.IMPORT.delimiter);
			final String sEncoding = (String) mdcvoStructure.getFieldValue(E.IMPORT.encoding);
			final Integer iHeaderLineCount = (Integer) mdcvoStructure.getFieldValue(E.IMPORT.headerlines);
			final Boolean bInsert = (Boolean) mdcvoStructure.getFieldValue(E.IMPORT.insert);
			final Boolean bUpdate = (Boolean) mdcvoStructure.getFieldValue(E.IMPORT.update);
			final Boolean bDelete = (Boolean) mdcvoStructure.getFieldValue(E.IMPORT.delete);
			final String mode = (String) mdcvoStructure.getFieldValue(E.IMPORT.mode);

			final UID entityUid = mdcvoStructure.getFieldUid(E.IMPORT.entity);

			final CsvImportStructure result = new CsvImportStructure(
					importStructureUid, entityUid, name, mode,
					sEncoding,bInsert, bUpdate, bDelete,
					iHeaderLineCount, sDelimiter);
			return result;
		}
		catch (CommonPermissionException e) {
			throw new NuclosFatalException(e);
		}
		catch (CommonFinderException e) {
			throw new NuclosFatalException(e);
		}
	}

	public CsvImportStructure(UID importStructureUid, 
			UID entityUid, String name, String mode, String encoding,
			boolean insert, boolean update, boolean delete,
			Integer iHeaderLineCount, String sDelimiter) 
						throws CommonPermissionException {

		super(importStructureUid, entityUid, name, mode, encoding, insert, update, delete);
		this.iHeaderLineCount = iHeaderLineCount;
		this.sDelimiter = sDelimiter;
	}
	
	@PostConstruct
	void init() {
		final UID entityUid = getEntityUid();
		final UID importStructureUid = getImportStructureUid();
		
		final Collection<EntityObjectVO<UID>> collAttributes = masterDataFacade.getDependantMd4FieldMeta(
				E.IMPORTATTRIBUTE.importfield, importStructureUid);

		final boolean isModule = modules.isModule(entityUid);

		for (final EntityObjectVO<UID> mdcvo : collAttributes) {
			final Integer iIndex = mdcvo.getFieldValue(E.IMPORTATTRIBUTE.fieldcolumn);
			final UID attributeUid = mdcvo.getFieldUid(E.IMPORTATTRIBUTE.attribute);
			final String format = mdcvo.getFieldValue(E.IMPORTATTRIBUTE.parsestring);
			final boolean preserve = mdcvo.getFieldValue(E.IMPORTATTRIBUTE.preserve);
			final NuclosScript script = mdcvo.getFieldValue(E.IMPORTATTRIBUTE.script);

			final Class<?> clazz;

			UID foreignEntityUid = null;
			if (isModule) {
				final AttributeCVO meta = attributeCache.getAttribute(attributeUid);
				clazz = meta.getJavaClass();
				foreignEntityUid = meta.getExternalEntity();
			}
			else {
				final FieldMeta<?> meta = metaProvider.getEntityField(attributeUid);
				try {
					clazz = Class.forName(meta.getDataType());
				}
				catch (final ClassNotFoundException ex) {
					throw new NuclosFatalException(ex);
				}
				foreignEntityUid = meta.getForeignEntity();
			}

			final Set<ForeignEntityIdentifier> foreignentityattributes = new HashSet<ForeignEntityIdentifier>();

			if (foreignEntityUid != null
					&& !attributeUid.equals(SF.PROCESS.getMetaData(entityUid).getUID())
					&& !attributeUid.equals(SF.STATE.getMetaData(entityUid).getUID())) {
				final boolean isFeModule = modules.isModule(foreignEntityUid);

				final Collection<EntityObjectVO<UID>> collForeignEntityIdentifiers = masterDataFacade.getDependantMd4FieldMeta(
						E.IMPORTFEIDENTIFIER.importattribute, mdcvo.getPrimaryKey());
				for (final EntityObjectVO<UID> mdvo : collForeignEntityIdentifiers) {
					final Integer iColumn = mdvo.getFieldValue(E.IMPORTFEIDENTIFIER.fieldcolumn);
					final UID feattributeUid = mdvo.getFieldUid(E.IMPORTFEIDENTIFIER.attribute);
					final String feformat = mdvo.getFieldValue(E.IMPORTFEIDENTIFIER.parsestring);
					final Class<?> feclazz;

					if (isFeModule) {
						AttributeCVO meta = attributeCache.getAttribute(feattributeUid);
						feclazz = meta.getJavaClass();
					}
					else {
						FieldMeta<?> meta = metaProvider.getEntityField(feattributeUid);
						try {
							feclazz = Class.forName(meta.getDataType());
						}
						catch (final ClassNotFoundException ex) {
							throw new NuclosFatalException(ex);
						}
					}
					foreignentityattributes.add(new CsvForeignEntityIdentifierImpl(iColumn, foreignEntityUid, feattributeUid,
							feclazz, feformat));
				}
			}
			else {
				foreignEntityUid = null;
			}
			getItems().put(attributeUid, (CsvItem)
					new CsvItemImpl(iIndex, entityUid, attributeUid, clazz, format, preserve,
							foreignEntityUid, foreignentityattributes, script));
		}

		final Collection<EntityObjectVO<UID>> collIdentifiers = masterDataFacade.getDependantMd4FieldMeta(
				E.IMPORTIDENTIFIER.importfield, importStructureUid);
		
		for (final EntityObjectVO<UID> mdcvo : collIdentifiers) {
			getIdentifiers().add(mdcvo.getFieldUid(E.IMPORTIDENTIFIER.attribute));
		}
	}

	public String getDelimiter() {
		return this.sDelimiter;
	}

	public Integer getHeaderLineCount() {
		return this.iHeaderLineCount;
	}

	public Set<String> getForbiddenAttributeNames(ParameterProvider paramProvider) {
		return GenericObjectImportUtils.getForbiddenAttributeNames(paramProvider, isUpdate());
	}

	@Configurable
	static class CsvItemImpl extends AbstractItem implements CsvItem {
		
		private final Integer iColumn;

		CsvItemImpl(final Integer iColumn, final UID entityUid, 
				final UID fieldUid, final Class<?> cls, final String sFormat, 
				boolean bPreserve, final UID foreignentityUid, 
				final Set<ForeignEntityIdentifier> feIdentifier, final NuclosScript script) {
			
			super(entityUid, fieldUid, cls, sFormat, bPreserve, foreignentityUid, feIdentifier, script);
			this.iColumn = iColumn;
		}

		@Override
		public Integer getColumn() {
			return this.iColumn;
		}

	}	// inner class CsvItemImpl

	@Configurable
	static class CsvForeignEntityIdentifierImpl extends ForeignEntityIdentifierImpl implements CsvForeignEntityIdentifier {
		
		private final Integer iColumn;
		
		CsvForeignEntityIdentifierImpl(Integer iColumn, final UID entityUid, final UID fieldUid, 
				Class<?> cls, String sFormat) {
			
			super(entityUid, fieldUid, cls, sFormat);
			this.iColumn = iColumn;
		}

		@Override
		public Integer getColumn() {
			return this.iColumn;
		}

	} // class CsvForeignEntityIdentifierImpl
	
}  // class CsvImportStructure
