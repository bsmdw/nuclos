//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.fileimport;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.nuclos.common.NuclosScript;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.genericobject.GenericObjectImportUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.fileimport.CommonParseException;
import org.nuclos.common2.fileimport.NuclosFileImportException;
import org.nuclos.common2.fileimport.parser.CsvFileImportParserFactory;
import org.nuclos.server.common.MetaProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * Class representing an invoice structure definition.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:ramin.goettlich@novabit.de">ramin.goettlich</a>
 * @version	00.01.000
 */
abstract class AbstractImportStructure implements ImportStructure {

	public static final int INSERT = 1;
	public static final int UPDATE = 2;
	public static final int DELETE = 4;

	private final String name;
	private final String sEncoding;
	private final boolean bInsert;
	private final boolean bUpdate;
	private final boolean bDelete;
	private final UID entityUid;
	private final UID importStructureUid;
	private final String  mode;
	
	private final Map<UID,Item> items;

	private final Set<UID> stIdentifiers = new HashSet<UID>();

	AbstractImportStructure(UID importStructureUid, UID entityUid, String name, String mode, String encoding,
			boolean insert, boolean update, boolean delete) {
		
		this.entityUid = entityUid;
		this.importStructureUid = importStructureUid;
		this.name = name;
		this.mode = mode;
		this.sEncoding = encoding;
		this.bInsert = insert;
		this.bUpdate = update;
		this.bDelete = delete;
		
		this.items = new HashMap<UID, Item>();
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getEncoding() {
		return sEncoding;
	}

	@Override
	public Map<UID,Item> getItems() {
		return items;
	}

	@Override
	public Set<UID> getIdentifiers() {
		return stIdentifiers;
	}

	@Override
	public boolean isInsert() {
		return bInsert;
	}

	@Override
	public boolean isUpdate() {
		return bUpdate;
	}

	@Override
	public boolean isDelete() {
		return bDelete;
	}

	@Override
	public UID getEntityUid() {
		return entityUid;
	}
	
	@Override 
	public UID getImportStructureUid() {
		return importStructureUid;
	}

	@Override
	public Set<String> getForbiddenAttributeNames(ParameterProvider paramProvider) {
		return GenericObjectImportUtils.getForbiddenAttributeNames(paramProvider, isUpdate());
	}

	@Override
	public int getImportSettings() {
		int result = 0;
		if (isInsert()) result |= INSERT;
		if (isUpdate()) result |= UPDATE;
		if (isDelete()) result |= DELETE;
		return result;
	}

	@Override
	public String getMode() {
		return this.mode;
	}
	
	@Configurable
	abstract static class AbstractItem implements Item {

		@Autowired
		private MetaProvider metaProvider = SpringApplicationContextHolder.getBean(MetaProvider.class);

		private final UID entityUid;
		private final UID fieldUid;
		private final Class<?> cls;
		private final String sFormat;
		private final boolean bPreserve;
		private final UID foreignentityUid;
		private final Set<ForeignEntityIdentifier> feIdentifier;
		private final NuclosScript script;
				
		// Spring injection
		
		@Autowired
		private CsvFileImportParserFactory parserFactory;
		
		// end of Spring injection

		AbstractItem(final UID entityUid, 
				final UID fieldUid, final Class<?> cls, final String sFormat, 
				boolean bPreserve, final UID foreignentityUid, 
				final Set<ForeignEntityIdentifier> feIdentifier, NuclosScript script) {
			
			this.fieldUid = fieldUid;
			this.entityUid = entityUid;
			this.cls = cls;
			this.sFormat = sFormat;
			this.bPreserve = bPreserve;
			this.foreignentityUid = foreignentityUid;
			this.feIdentifier = feIdentifier;
			this.script = script;
		}

		@Override
		public Class<?> getCls() { return this.cls; }

		@Override
		public UID getFieldUID() {
			return this.fieldUid;
		}

		@Override
		public UID getEntityUID() {
			return this.entityUid;
		}

		@Override
		public boolean isPreserve() {
			return this.bPreserve;
		}

		@Override
		public boolean isReferencing() {
			return foreignentityUid != null;
		}

		@Override
		public UID getForeignEntityUID() {
			return this.foreignentityUid;
		}

		@Override
		public Set<ForeignEntityIdentifier> getForeignEntityIdentifiers() {
			return this.feIdentifier;
		}

		@Override
		public NuclosScript getScript() {
			return script;
		}

		@Override
		public Object parse(String sValue) throws NuclosFileImportException {
			try {
				if (!StringUtils.looksEmpty(sValue)) {
					return parserFactory.parse(cls, sValue, sFormat);
				}
				else {
					return null;
				}
			}
			catch (CommonParseException ex) {
				throw new NuclosFileImportException(StringUtils.getParameterizedExceptionMessage("import.structure.exception", metaProvider.getEntityField(this.getFieldUID()).getFieldName()), ex);
			}
		}
		
	}	// inner class ItemImpl

	@Configurable
	abstract static class ForeignEntityIdentifierImpl implements ForeignEntityIdentifier {

		@Autowired
		private MetaProvider metaProvider = SpringApplicationContextHolder.getBean(MetaProvider.class);

		private final UID entityUid;
		private final UID fieldUid;
		private final Class<?> cls;
		private final String sFormat;

		// Spring injection
		
		@Autowired
		private CsvFileImportParserFactory parserFactory;
		
		// end of Spring injection

		ForeignEntityIdentifierImpl(final UID entityUid, final UID fieldUid, Class<?> cls, String sFormat) {
			this.fieldUid = fieldUid;
			this.entityUid = entityUid;
			this.cls = cls;
			this.sFormat = sFormat;
		}

		@Override
		public UID getFieldUID() {
			return this.fieldUid;
		}

		@Override
		public UID getEntityUID() {
			return this.entityUid;
		}

		@Override
		public Object parse(String sValue) throws NuclosFileImportException {
			try {
				if (!StringUtils.looksEmpty(sValue)) {
					return parserFactory.parse(this.cls, sValue, this.sFormat);
				}
				else {
					return null;
				}
			}
			catch (CommonParseException ex) {
				throw new NuclosFileImportException(StringUtils.getParameterizedExceptionMessage(
						"import.structure.exception", metaProvider.getEntityField(this.getFieldUID()).getFieldName()), ex);
			}
		}
	} // class ForeignEntityIdentifierImpl
	
}  
