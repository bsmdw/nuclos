package org.nuclos.server.report;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.locale.NuclosLocale;
import org.nuclos.api.report.OutputFormat;
import org.nuclos.api.service.ReportService;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.report.valueobject.ReportOutputVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.common.ServerServiceLocator;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.i18n.language.data.DataLanguageCache;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.report.ejb3.ReportFacadeLocal;
import org.nuclos.server.ruleengine.NuclosFatalRuleException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("reportServiceProvider")
public class ReportServiceProvider implements ReportService {

	@Autowired
	DataLanguageCache dataLangCache;
	
	@Override
	public NuclosFile run(OutputFormat format) throws BusinessException {
		return this.run(format, null, null);
	}

	@Override
	public NuclosFile run(OutputFormat format,
			Map<String, Object> params) throws BusinessException{
		return run(format, params, null);
	}
	
	private static class ReportExecutionException {
		
		private Exception exc;
		
		public ReportExecutionException() {
			exc = null;
		}
		public void setException(Exception e) {
			this.exc = e;
		}
		
		public boolean hasException() {
			return this.exc != null;
		}
		
		public Exception getException() {
			return this.exc;
		}
	}

	@Override
	public NuclosFile run(OutputFormat format, NuclosLocale locale)
			throws BusinessException {
		return run(format, null, locale);
	}

	@Override
	public NuclosFile run(OutputFormat format, Map<String, Object> params,
			NuclosLocale locale) throws BusinessException {
		return run(format, params, locale, NucletDalProvider.getInstance().getAccessibleMandatorsOrigin());
	}
	
	@Override
	public NuclosFile run(org.nuclos.api.UID formatUID, Map<String, Object> params,
			NuclosLocale locale) throws BusinessException {
		return run((UID)formatUID, params, locale, NucletDalProvider.getInstance().getAccessibleMandatorsOrigin());
	}
	
	public NuclosFile run(OutputFormat format, Map<String, Object> params,
			NuclosLocale locale, final UID mandatorUID) throws BusinessException {
		
		if (format == null) {
			throw new NuclosFatalRuleException("ReportOutputFormat must not be null");
		}
		return run((UID)format.getId(), params, locale, mandatorUID);
	}
	
	public NuclosFile run(UID formatUID, Map<String, Object> params,
			NuclosLocale locale, final UID mandatorUID) throws BusinessException {
		
		final UID foundlanguage = locale != null ? 
				new UID(locale.toString()) : dataLangCache.getLanguageToUse();
				
		if (formatUID == null) {
			throw new NuclosFatalRuleException("ReportOutputFormat-UID must not be null");
		}

		final Map<String, Object> RepoParam = params != null ? params : new HashMap<String, Object>();
		
		final List<NuclosFile> retVal = new ArrayList<NuclosFile>();
		
			
		final ReportFacadeLocal reportFacade = 
				ServerServiceLocator.getInstance().getFacade(ReportFacadeLocal.class);		
		
		final EntityObjectVO<UID> reportOutput = 
				NucletDalProvider.getInstance().getEntityObjectProcessor(E.REPORTOUTPUT).getByPrimaryKey((UID)formatUID);
		final ReportOutputVO reportOutputVO = MasterDataWrapper.getReportOutputVO(new MasterDataVO(reportOutput, true));
		
		final EntityObjectVO<UID> report = 
				NucletDalProvider.getInstance().getEntityObjectProcessor(
						E.REPORT).getByPrimaryKey(reportOutputVO.getReportUID());
		
		final UID formatId = (UID)formatUID;
		final DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss", Locale.getDefault());
		final String fileName = report.getFieldValue(E.REPORT.name) + "_" + dateformat.format(Calendar.getInstance(Locale.getDefault()).getTime());
		
		final ReportExecutionException exceptionExecution = new ReportExecutionException();

		try {
			NuclosFile file = reportFacade.prepareReport(formatId, RepoParam, null, foundlanguage, mandatorUID);
			retVal.add(new org.nuclos.common.NuclosFile(fileName + reportOutputVO.getFormat().getExtension(), file.getContent()));
		}
		catch (CommonBusinessException e) {
			exceptionExecution.setException(e);
		}

		if (exceptionExecution.hasException())
			throw new BusinessException(exceptionExecution.getException());
		
		if (retVal.size() == 0)
			throw new BusinessException("No files found for report " + report.getFieldValue(E.REPORT.name));
		
		
		return retVal.get(0);
	}
}
 
