//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.report.ejb3;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;
import javax.print.DocFlavor;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.NuclosFile;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.attribute.DynamicAttributeVO;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.ToHumanReadablePresentationVisitor;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common.dblayer.JoinType;
import org.nuclos.common.genericobject.CollectableGenericObjectEntityField;
import org.nuclos.common.genericobject.GenericObjectUtils;
import org.nuclos.common.report.NuclosReportException;
import org.nuclos.common.report.NuclosReportPrintJob;
import org.nuclos.common.report.NuclosReportRemotePrintService;
import org.nuclos.common.report.ReportFieldDefinition;
import org.nuclos.common.report.ReportFieldDefinitionFactory;
import org.nuclos.common.report.ejb3.ReportFacadeRemote;
import org.nuclos.common.report.valueobject.DefaultReportOutputVO;
import org.nuclos.common.report.valueobject.DefaultReportVO;
import org.nuclos.common.report.valueobject.ReportOutputVO;
import org.nuclos.common.report.valueobject.ReportOutputVO.Format;
import org.nuclos.common.report.valueobject.ReportOutputVO.PageOrientation;
import org.nuclos.common.report.valueobject.ReportVO.ReportType;
import org.nuclos.common.report.valueobject.ResultColumnVO;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common.report.valueobject.SubreportVO;
import org.nuclos.common.security.Permission;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.server.attribute.ejb3.LayoutFacadeLocal;
import org.nuclos.server.common.AttributeCache;
import org.nuclos.server.common.DatasourceCache;
import org.nuclos.server.common.MandatorUtils;
import org.nuclos.server.common.ejb3.EntityObjectFacadeLocal;
import org.nuclos.server.common.ejb3.LocaleFacadeLocal;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.query.DbColumnExpression;
import org.nuclos.server.dblayer.query.DbCondition;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeLocal;
import org.nuclos.server.genericobject.ejb3.GenericObjectFacadeRemote;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeRemote;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.printservice.printout.PrintoutObjectBuilder;
import org.nuclos.server.printservice.printout.PrintoutWrapper;
import org.nuclos.server.report.Export;
import org.nuclos.server.report.ReportExecutionEvent;
import org.nuclos.server.report.ReportExecutionListener;
import org.nuclos.server.report.ReportExportFactory;
import org.nuclos.server.report.ReportObjectBuilder;
import org.nuclos.server.report.export.CsvExport;
import org.nuclos.server.report.export.ExcelExport;
import org.nuclos.server.report.export.JasperExport;
import org.nuclos.server.report.export.WordExport;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.transaction.UnexpectedRollbackException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Report facade encapsulating report management. <br>
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Transactional(noRollbackFor = { Exception.class })
public class ReportFacadeBean extends NuclosFacadeBean implements ReportFacadeLocal, ReportFacadeRemote {

	private final static Logger LOG = LoggerFactory.getLogger(ReportFacadeBean.class);

	// Spring injection
	
	@Autowired
	private MasterDataFacadeLocal masterDataFacade;

	@Autowired
	private GenericObjectFacadeRemote genericObjectFacade;
	
	@Autowired
	private ParameterProvider parameterProvider;
	
	@Autowired
	private ReportObjectBuilder reportObjectBuilder;
	
	@Autowired
	private PrintoutObjectBuilder printoutObjectBuilder;
	
	@Autowired
	private SpringDataBaseHelper dataBaseHelper;
	
	@Autowired
	private MandatorUtils mandatorUtils;
	
	@Autowired
	private EventSupportFacadeLocal eventSupportFacade;
	
	@Autowired
	private EntityObjectFacadeLocal eoFacade;
	
	@Autowired
	private PrintoutWrapper wrapperPrintout;

	@Autowired
	private SpringLocaleDelegate localeDelegate;
	
	// end of Spring injection
	
	/**
	 * Classpath for compiling JasperReport templates
	 */
	private String jrClasspath;
	
	private List<ReportExecutionListener> reportExecutionListeners;
	
	public ReportFacadeBean() {
	}

	@PostConstruct
	@RolesAllowed("Login")
	public void postConstruct() {		
		// Determine classpath dynamically
		jrClasspath = parameterProvider.getJRClassPath();
		LOG.info("Set JasperReports compile classpath to {}", jrClasspath);
		System.setProperty("jasper.reports.compile.class.path", jrClasspath);
		SpringApplicationContextHolder.addSpringReadyListener(new SpringApplicationContextHolder.SpringReadyListener() {
			@Override
			public void springIsReady() {
				try {
					preloadReports();
				} catch (UnexpectedRollbackException e) {
					LOG.warn("Failed to preload reports: {}", e, e);
				}
			}
			@Override
			public int getMinReadyState() {
				return 3;
			}
		});
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor=Exception.class)
	void preloadReports() {
		// just to cache classes etc- 
		// @todo definitely a hack to load classes to improve performance generating 
		// a report for the first time after server restarts-
		Collection<MasterDataVO<UID>> repoCollection = masterDataFacade.getMasterData(E.REPORTOUTPUT, null);
		for (int i = 0; i < ReportOutputVO.Format.values().length; i++) {
			final ReportOutputVO.Format format = ReportOutputVO.Format.values()[i];
			for (MasterDataVO<UID> mdVO : repoCollection) {
				try {
					if (mdVO.getFieldValue(E.REPORTOUTPUT.format).equals(format.name())) {
						DefaultReportOutputVO reportoutput = MasterDataWrapper.getReportOutputVO(mdVO);
						testReport(reportoutput);
						break;
					}
				} catch (Exception e) {
					LOG.warn("Unable to preload report from master data {} format: {}: {}", mdVO, format, e);
				}
			}
		}
	}

	/**
	 * @return all reports
	 * @throws CommonPermissionException
	 */
	public Collection<DefaultReportVO> getReports() throws CommonPermissionException {
		this.checkReadAllowed(E.REPORT);

		final Collection<DefaultReportVO> collreport = new ArrayList<DefaultReportVO>();
		for (MasterDataVO<UID> mdVO : masterDataFacade.getMasterData(E.REPORT, null)) {
			final Collection<UID> readableReports = securityCache.getReadableReports(getCurrentUserName(), getCurrentMandatorUID()).get(ReportType.REPORT);
			if (readableReports == null) {
				return collreport;
			}
			if (readableReports.contains(mdVO.getPrimaryKey()))
				collreport.add(MasterDataWrapper.getReportVO(mdVO, getCurrentUserName(), getCurrentMandatorUID()));
		}

		return collreport;
	}

	/**
	 * Get all reports which have outputs containing the given datasourceId and
	 * have the given type (report, form or template). We go a little
	 * indirection so that we can use the security mechanism of the ReportBean.
	 * 
	 * @param dataSourceUID
	 * @param type
	 * @return set of reports
	 * @throws CommonPermissionException
	 */
	public Collection<DefaultReportVO> getReportsForDatasourceId(UID dataSourceUID, final ReportType type) throws CommonPermissionException {
		this.checkReadAllowed(E.DATASOURCE);
		
		final Collection<DefaultReportVO> collreport = new ArrayList<DefaultReportVO>();

		final DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		final DbQuery<UID> query = builder.createQuery(UID.class);
		final DbFrom<UID> r = query.from(E.REPORT, "r");
		final DbFrom<UID> o = r.join(E.REPORTOUTPUT, JoinType.INNER, "o").on(E.REPORT.getPk(), E.REPORTOUTPUT.parent);
		query.select(r.baseColumn(E.REPORT.getPk()));
		query.where(builder.and(builder.equalValue(r.baseColumn(E.REPORT.type), type.getValue()), builder.equalValue(o.baseColumn(E.REPORTOUTPUT.datasource), dataSourceUID)));

		for (final UID uid : dataBaseHelper.getDbAccess().executeQuery(query)) {
			try {
				final MasterDataVO<UID> mdVO = masterDataFacade.get(type == ReportType.REPORT ? E.REPORT : E.FORM, uid);
				Collection<UID> readableReports = securityCache.getReadableReports(getCurrentUserName(), getCurrentMandatorUID()).get(type);
				if (mdVO != null && readableReports.contains(mdVO.getPrimaryKey())) {
					collreport.add(MasterDataWrapper.getReportVO(mdVO, getCurrentUserName(), getCurrentMandatorUID()));					
				}
			}
			catch (CommonPermissionException ex) {
				throw new NuclosFatalException(ex);
			}
			catch (CommonFinderException ex) {
				// nothing found, do nothing
			}
		}

		return collreport;
	}

	/**
	 * create new report
	 * 
	 * @param mdvo
	 *            value object
	 * @param mpDependants
	 * @return new report
	 */
	public MasterDataVO<UID> create(MasterDataVO<UID> mdvo, IDependentDataMap mpDependants) throws CommonCreateException, NuclosReportException, CommonPermissionException, NuclosBusinessRuleException {
		EntityMeta<UID> entity = E.REPORT;
		
		if (ReportType.FORM.getValue().equals(mdvo.getFieldValue(E.REPORT.type))) {
			entity = E.FORM;			
		}

		mdvo.setDependents(mpDependants);
		this.checkReadAllowed(entity);
		final MasterDataVO<UID> result = masterDataFacade.create(mdvo, null);
		compileAndSaveAllXML(entity, result);
		securityCache.invalidateCache(true, true);
		return result;
	}

	/**
	 * modify an existing report
	 */
	public UID modify(final MasterDataVO<UID> mdvo, final IDependentDataMap mpDependants) throws CommonBusinessException {
		EntityMeta<UID> entity = E.REPORT;
		if (ReportType.FORM.getValue().equals(mdvo.getFieldValue(E.REPORT.type))) {
			entity = E.FORM;			
		}

		this.checkReadAllowed(entity);
		mdvo.setDependents(mpDependants);
		final UID result = (UID) masterDataFacade.modify(mdvo, null);
		this.compileAndSaveAllXML(entity, mdvo);

		return result;
	}

	/**
	 * delete an existing report
	 */
	public <PK> void remove(MasterDataVO<PK> mdvo) 
			throws CommonFinderException, CommonRemoveException, CommonStaleVersionException, 
			CommonPermissionException, CommonCreateException, NuclosBusinessRuleException {
		this.checkReadAllowed(mdvo.getEntityObject().getDalEntity());
		masterDataFacade.remove(mdvo.getEntityObject().getDalEntity(), mdvo.getPrimaryKey(), true, null);
		securityCache.invalidateCache(true, true);
	}

	private void compileAndSaveAllXML(final EntityMeta<UID> entity, final MasterDataVO<UID> mdvo) throws NuclosReportException {
		for (DefaultReportOutputVO reportoutput : getReportOutputs(mdvo.getPrimaryKey())) {
			if ("PDF".equals(reportoutput.getFormat().getValue())) {
				if (reportoutput.getSourceFile() != null) {
					compileAndSaveXML(entity, reportoutput);

					// subreports are only allowed for forms and reports
					if (reportoutput.getFormat() != null) {
						for (SubreportVO subreport : getSubreports(entity, reportoutput.getId())) {
							compileAndSaveXML(entity, subreport);
						}
					}
				}
				else {
					// PDF must have a template
					throw new NuclosReportException("report.error.missing.template.1");// "F\u00fcr eine PDF-Ausgabe muss eine Vorlage angegeben werden.");
				}
			}
			else {
				reportoutput.setReportCLS(null);
			}
		}
	}

	private void compileAndSaveXML(final EntityMeta<UID> entity, final DefaultReportOutputVO reportOutput) throws NuclosReportException {
		reportOutput.setReportCLS(ReportCompiler.compileReport(reportOutput.getSourceFileContent()));

		final MasterDataVO<UID> mdvo = MasterDataWrapper.wrapReportOutputVO(entity, reportOutput);
		try {
			masterDataFacade.modify(mdvo, null);
		}
		catch (Exception e) {
			throw new NuclosReportException(e);
		}
	}

	private void compileAndSaveXML(final EntityMeta<UID> entity, final SubreportVO subreport) throws NuclosReportException {
		subreport.setReportCLS(ReportCompiler.compileReport(subreport.getSourcefileContent()));

		final MasterDataVO<UID> mdvo = MasterDataWrapper.wrapSubreportVO(entity, subreport);

		try {
			masterDataFacade.modify(mdvo, null);
		}
		catch (Exception e) {
			throw new NuclosReportException(e);
		}
	}

	/**
	 * get output formats for report
	 * 
	 * @param reportUID
	 *            UID of report
	 * @return collection of output formats
	 */
	public Collection<DefaultReportOutputVO> getReportOutputs(final UID reportUID) {
		final List<DefaultReportOutputVO> outputs = new ArrayList<DefaultReportOutputVO>();
		final CollectableComparison cond = SearchConditionUtils.newUidComparison(E.REPORTOUTPUT.parent.getUID(), ComparisonOperator.EQUAL, reportUID);
		Collection<MasterDataVO<UID>> mdOutputs = masterDataFacade.getMasterData(E.REPORTOUTPUT, cond);

		for (MasterDataVO<UID> mdVO : mdOutputs) {
			outputs.add(MasterDataWrapper.getReportOutputVO(mdVO));			
		}

		return outputs;
	}

	public Collection<SubreportVO> getSubreports(final EntityMeta<UID> entityTmp, final UID reportOutputUID) {
		final List<SubreportVO> subreports = new ArrayList<SubreportVO>();

		EntityMeta<UID> entity = E.SUBREPORT;
		FieldMeta<UID> entityField = E.SUBREPORT.reportoutput;
		if(entityTmp.equals(E.FORM)) {
			entity = E.SUBFORM;
			entityField = E.SUBFORM.formoutput;
		}
		final CollectableComparison cond = SearchConditionUtils.newUidComparison(entityField.getUID(), ComparisonOperator.EQUAL, reportOutputUID);
		final Collection<MasterDataVO<UID>> mdSubreports = masterDataFacade.getMasterData(entity, cond);

		for (MasterDataVO<UID> mdVO : mdSubreports) {
			subreports.add(new SubreportVO(mdVO));
		}

		return subreports;
	}

	/**
	 * get output format for reportoutput id
	 */
	public DefaultReportOutputVO getReportOutput(UID reportOutputUID) throws CommonFinderException, CommonPermissionException {
		return MasterDataWrapper.getReportOutputVO(masterDataFacade.get(E.REPORTOUTPUT, reportOutputUID));
	}
	
	@Cacheable(value = "reportUsagesByUsageCriteria", key = "#p0.mainHashCode()")
	public List<UID> getReportUsagesByUsageCriteria(final UsageCriteria usagecriteria) {
		
		final DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		final DbQuery<UID> query = builder.createQuery(UID.class);
		final DbFrom<UID> t = query.from(E.REPORTUSAGE);
		query.select(t.baseColumn(E.REPORTUSAGE.form));
		DbCondition cond = builder.equalValue(t.baseColumn(E.REPORTUSAGE.module), usagecriteria.getEntityUID());

		final DbColumnExpression<UID> cp = t.baseColumn(E.REPORTUSAGE.process);
		final UID processUID = usagecriteria.getProcessUID();
		if (processUID == null) {
			query.where(builder.and(cond, cp.isNull()));
		}
		else {
			query.where(builder.and(cond, builder.or(cp.isNull(), builder.equalValue(cp, processUID))));
		}
		
		final DbColumnExpression<UID> cs = t.baseColumn(E.REPORTUSAGE.state);

		final UID statusUID = usagecriteria.getStatusUID();
		if (statusUID == null) {
			query.addToWhereAsAnd(builder.and(cond, cs.isNull()));
		}
		else {
			query.addToWhereAsAnd(builder.and(cond, builder.or(cs.isNull(), builder.equalValue(cs, statusUID))));
		}

		return dataBaseHelper.getDbAccess().executeQuery(query);
	}
	
	@CacheEvict(value = "reportUsagesByUsageCriteria", key = "#p0.mainHashCode()")
	public void evictReportUsagesByUsageCriteria(final UsageCriteria usagecriteria) {
	}
	
	@Cacheable(value = "formMasterDataByReportUid", key = "#p0")
	public MasterDataVO<UID> getFormMasterDataByReportUid(final UID reportUID) throws CommonPermissionException, CommonFinderException {
		return masterDataFacade.get(E.FORM, reportUID);
	}
	
	@CacheEvict(value = "formMasterDataByReportUid", key = "#p0")
	public void evictFormMasterDataByReportUid(final UID reportUID) {
	}

	@Caching(evict = { 
			@CacheEvict(value = "reportUsagesByUsageCriteria", allEntries = true),
			@CacheEvict(value = "formMasterDataByReportUid", allEntries = true)
		})
	public void evictAllReportUsagesAndFormMasterData() {
	}

	/**
	 * finds reports (forms) by usage criteria
	 * 
	 * @param usagecriteria
	 * @return collection of reports (forms)
	 */
	@RolesAllowed("Login")
	public List<DefaultReportVO> findReportsByUsage(final UsageCriteria usagecriteria) {
		
		final List<DefaultReportVO> reports = new ArrayList<DefaultReportVO>();

		final List<UID> collUsableReportIds = getReportUsagesByUsageCriteria(usagecriteria);

		final Map<ReportType, Collection<UID>> readableReports = securityCache.getReadableReports(getCurrentUserName(), getCurrentMandatorUID());

		for (ReportType rt : readableReports.keySet()) {
			for (final UID reportUID : CollectionUtils.intersection(collUsableReportIds, readableReports.get(rt))) {
				try {
					reports.add(MasterDataWrapper.getReportVO(getFormMasterDataByReportUid(reportUID), getCurrentUserName(), getCurrentMandatorUID()));
				}
				catch (CommonPermissionException ex) {
					throw new CommonFatalException(ex);
				}
				catch (CommonFinderException ex) {
					throw new CommonFatalException(ex);
				}
			}
		}

		Collections.sort(reports, new Comparator<DefaultReportVO>() {

			@Override
			public int compare(DefaultReportVO o1, DefaultReportVO o2) {
				return StringUtils.emptyIfNull(o1.getName()).compareToIgnoreCase(StringUtils.emptyIfNull(o2.getName()));
			}
		});

		return reports;
	}

	private static Export getExportInstance(ReportOutputVO.Format format) {
		switch (format) {
		case PDF:
			return new JasperExport();
		case CSV:
			return new CsvExport();
		case TSV:
			return new CsvExport('\t', 0, ' ', false);
		case XLS:
		case XLSX:
			return new ExcelExport(format);
		case DOC:
		case DOCX:
			return new WordExport(format);
		default:
			throw new NuclosFatalException("Format " + format + " is not supported for server-side execution.");
		}
	}
	
	@Override
	public NuclosFile testReport(final UID reportOutputUID) throws NuclosReportException {
		try {
			DefaultReportOutputVO reportoutput = MasterDataWrapper.getReportOutputVO(masterDataFacade.get(E.REPORTOUTPUT, reportOutputUID));
			return testReport(reportoutput);
		}
		catch (CommonBusinessException e) {
			throw new NuclosReportException(e);
		}
	}
	
	public NuclosFile testReport(final DefaultReportOutputVO reportoutput) throws NuclosReportException {
		try {
			final Export export = getExportInstance(reportoutput.getFormat());
			return export.test(reportoutput, getCurrentMandatorUID());
		}
		catch (CommonBusinessException e) {
			throw new NuclosReportException(e);
		}
	}

	/**
	 * gets a report/form filled with data
	 * 
	 * @param reportOutputUID
	 * @param mpParams
	 *            parameters
	 * @return report/form filled with data
	 */
	public NuclosFile prepareReport(final UID reportOutputUID, final Map<String, Object> mpParams, final Integer iMaxRowCount, UID language) throws CommonFinderException, NuclosReportException, CommonPermissionException {
		return prepareReport(reportOutputUID, mpParams, iMaxRowCount, language, getCurrentMandatorUID());
	}
	
	public NuclosFile prepareReport(final UID reportOutputUID, final Map<String, Object> mpParams, final Integer iMaxRowCount, UID language, UID mandatorUID) throws CommonFinderException, NuclosReportException, CommonPermissionException {
		final DefaultReportOutputVO reportoutput = getReportOutput(reportOutputUID);
		LOG.info("preparing report (" + reportoutput.getFormat().getValue() + ") for datasource '" + DatasourceCache.getInstance().get(reportoutput.getDatasourceUID()).getName() + "'.");
		fireReportExecutionEvents(reportoutput.getReportUID());
		final Locale locale = getLocale(reportoutput.getLocale(), localeDelegate.getLocale());
		final Export export = getExportInstance(reportoutput.getFormat());
		return export.export(reportoutput, mpParams, locale, LangUtils.defaultIfNull(iMaxRowCount, -1), language, mandatorUID);
	}

	private Locale getLocale(final String locale, final Locale def) {
		if (!StringUtils.isNullOrEmpty(locale)) {
			for (LocaleInfo li : SpringApplicationContextHolder.getBean(LocaleFacadeLocal.class).getAllLocales(true)) {
				if (locale.equalsIgnoreCase(li.getName())) {
					return li.toLocale();
				}
			}
		}
		return def;
	}

	/**
	 * gets search result report filled with data
	 * <p>
	 * TODO: Don't serialize CollectableEntityField and/or CollectableEntity!
	 * (tp) Refer to
	 * {@link org.nuclos.common.CollectableEntityFieldWithEntity#readObject(ObjectInputStream)}
	 * for details.
	 * </p>
	 * 
	 * @param clctexpr
	 *            search expression
	 * @param bIncludeSubModules
	 *            Include submodules in search?
	 * @return search result report filled with data
	 */
	@RolesAllowed("Login")
	public NuclosFile prepareSearchResult(CollectableSearchExpression clctexpr, List<? extends CollectableEntityField> lstclctefweSelected, 
			List<Integer> selectedFieldWidth,
			UID entityUID, ReportOutputVO.Format format, String customUsage, PageOrientation orientation, boolean columnScaled) throws NuclosReportException {
		final UID entity = entityUID;
		final List<UID> lstAttributeIds = GenericObjectUtils.getAttributeUids(lstclctefweSelected, entity, AttributeCache.getInstance());
		final List<UID> lstFieldUids = new ArrayList<>();
		final Set<UID> subentities = new HashSet<UID>();
		for (CollectableEntityField cef : lstclctefweSelected) {
			if (!entity.equals(cef.getEntityUID())) {
				subentities.add(cef.getEntityUID());
			}
			lstFieldUids.add(cef.getUID());
		}
		
		ResultVO resultvo;
		List<ReportFieldDefinition> fields;
		
		// Append record grants here! (NUCLOS-3517)
		final EntityMeta<?> eMeta = metaProvider.getEntity(entityUID);
		clctexpr.setSearchCondition(mandatorUtils.append(clctexpr, eMeta));
		try {
			if (eMeta.isStateModel()) {
				// GO
				Collection<GenericObjectWithDependantsVO> lstlowdcvo = genericObjectFacade.getPrintableGenericObjectsWithDependants(
						entityUID, clctexpr, new HashSet<UID>(lstAttributeIds), subentities, customUsage);
				// BMWFDM-105
				List<CollectableEntityField> lstclctefweSelectedModified = modifyExportFields(entity, lstclctefweSelected);
				resultvo = convertGenericObjectListToResultVO(entity, lstclctefweSelectedModified, lstlowdcvo);
				fields = ReportFieldDefinitionFactory.getFieldDefinitions(lstclctefweSelectedModified);
			} else {
				// MD
				MasterDataFacadeRemote mdRemote = RigidUtils.uncheckedCast(SpringApplicationContextHolder.getBean("masterDataService"));
				Collection<MasterDataVO<Object>> lstmdvo = mdRemote.getMasterDataChunk(entity, clctexpr, new ResultParams(lstFieldUids));
				resultvo = convertMasterDataListToResultVO(entity, lstclctefweSelected, lstmdvo);
				fields = ReportFieldDefinitionFactory.getFieldDefinitions(lstclctefweSelected);
			}
		} catch (CommonPermissionException cbe) {
			throw new NuclosReportException(cbe);
		}
		
		if(selectedFieldWidth.size() == resultvo.getColumnCount()) {
			Iterator<Integer> selectedFieldWidthIterator = selectedFieldWidth.iterator();
			for(ResultColumnVO voColumn : resultvo.getColumns()) {
				voColumn.setColumnWidth(selectedFieldWidthIterator.next());
			}
		}
		
		String searchCondition = "";
		if (clctexpr.getSearchCondition() != null) {
			searchCondition = clctexpr.getSearchCondition().accept(new ToHumanReadablePresentationVisitor());			
		}
		final Export export = ReportExportFactory.getExport(searchCondition, format, orientation, columnScaled); // getExportInstance(format);
		return export.export(null, resultvo, fields);
	}
	
	private final List<CollectableEntityField> modifyExportFields(final UID entity, final List<? extends CollectableEntityField> lstclctefweSelectedOriginal) {
		// replace StateIcon field by state(name)
		final List<CollectableEntityField> lstclctefweSelected = new ArrayList<CollectableEntityField>();
		lstclctefweSelected.addAll(lstclctefweSelectedOriginal);
		
		final org.nuclos.common.collection.Predicate<CollectableEntityField> isStateIconContained = new org.nuclos.common.collection.Predicate<CollectableEntityField> (){

			@Override
			public boolean evaluate(CollectableEntityField t) {
				return LangUtils.equal(t.getUID(), SF.STATEICON.getUID(entity));
			}
		};
		
		final CollectableEntityField efStateIcon = CollectionUtils.findFirst(lstclctefweSelected, isStateIconContained);
		// is StateIcon field contained?
		if (null != efStateIcon) {
			int idxStateIcon = lstclctefweSelected.indexOf(efStateIcon);
			// remove StateIcon field
			lstclctefweSelected.remove(efStateIcon);
			
			final org.nuclos.common.collection.Predicate<CollectableEntityField> isStateContained = new org.nuclos.common.collection.Predicate<CollectableEntityField> (){
	
				@Override
				public boolean evaluate(CollectableEntityField t) {
					return LangUtils.equal(t.getUID(), SF.STATE.getUID(entity));
				}
				
			};
			final CollectableEntityField efState = CollectionUtils.findFirst(lstclctefweSelected, isStateContained);
			// is any State field available?
			if (null == efState) {
				
				final FieldMeta<?> efMeta = metaProvider.getEntityField(SF.STATE.getUID(entity));
				CollectableEntityField cefStateReplacement = null;
				Map<UID, Permission> mpPermissions = null;
				if (efMeta.isCalcAttributeAllowCustomization()) {
					mpPermissions = AttributeCache.getInstance().getAttribute(LangUtils.defaultIfNull(efMeta.getCalcBaseFieldUID(), efMeta.getUID())).getPermissions();
				} else {
					mpPermissions = AttributeCache.getInstance().getAttribute(efMeta.getUID()).getPermissions();
				}
				cefStateReplacement = new CollectableGenericObjectEntityField(mpPermissions, efMeta, entity);
				
				lstclctefweSelected.add(idxStateIcon,cefStateReplacement);
				
			}
		}
		// make it unmodifiable as before
		return Collections.unmodifiableList(lstclctefweSelected);
	}
	
	/**
	 * creates a new ResultVO object from a list of selected collectable entities.
	 *
	 * @param clcteMain main collectable entity
	 * @param lstclctefweSelected List<CollectableEntityFieldWithEntity> attributes, subform or parent columns which are part of the content
	 * @param lstlowdcvo List<GenericObjectWithDependantsVO> the data contained in the selected fields
	 */
	private ResultVO convertGenericObjectListToResultVO(UID entityUID, List<? extends CollectableEntityField> lstclctefweSelected,
			Collection<GenericObjectWithDependantsVO> lstlowdcvo) {

		final ResultVO result = new ResultVO();

		// fill the columns:
		for (CollectableEntityField clctefwe : lstclctefweSelected) {
			final FieldMeta<?> fieldMeta = metaProvider.getEntityField(clctefwe.getUID());
			final String fieldLabel = localeDelegate.getLabelFromMetaFieldDataVO(fieldMeta);
			final ResultColumnVO resultcolumnvo = new ResultColumnVO();
			resultcolumnvo.setColumnLabel(fieldLabel);
			resultcolumnvo.setColumnClassName(clctefwe.getJavaClass().getName());
			result.addColumn(resultcolumnvo);
		}

		// fill the rows:
		for (GenericObjectWithDependantsVO lowdcvo : lstlowdcvo) {
			final UsageCriteria usage = lowdcvo.getUsageCriteria(null);
			final Object[] aoData = new Object[lstclctefweSelected.size()];

			int iColumn = 0;
			for (CollectableEntityField clctefwe : lstclctefweSelected) {
				final UID sFieldUID = clctefwe.getUID();
				final UID sFieldEntityUID = clctefwe.getEntityUID();

				if (sFieldEntityUID.equals(entityUID)) {
					// own attribute:
					final DynamicAttributeVO davo = lowdcvo.getAttribute(sFieldUID);
					aoData[iColumn] = davo != null ? davo.getValue() : null;
				}
				else {
					// subform field:
					IDependentKey dependentKey = SpringApplicationContextHolder.getBean(LayoutFacadeLocal.class).getDependentKeyBetween(usage, sFieldEntityUID);
					final Collection<EntityObjectVO<?>> collmdvo = lowdcvo.getDependents().getData(dependentKey);
					final List<Object> values = CollectionUtils.transform(collmdvo, new Transformer<EntityObjectVO<?>, Object>() {
						@Override
						public Object transform(EntityObjectVO<?> i) {
							return i.getFieldValue(sFieldUID);
						}
					});
					aoData[iColumn] = values;
				}

				iColumn++;
			}
			result.addRow(aoData);
		}

		return result;
	}
	
	private ResultVO convertMasterDataListToResultVO(UID entityUID, List<? extends CollectableEntityField>  lstclctefweSelected, Collection<MasterDataVO<Object>> lstmdvo) {

		final ResultVO result = new ResultVO();

		// fill the columns:
		for (CollectableEntityField clctefwe : lstclctefweSelected) {
			final FieldMeta<?> fieldMeta = metaProvider.getEntityField(clctefwe.getUID());
			final String fieldLabel = localeDelegate.getLabelFromMetaFieldDataVO(fieldMeta);
			final ResultColumnVO resultcolumnvo = new ResultColumnVO();
			resultcolumnvo.setColumnLabel(fieldLabel);
			resultcolumnvo.setColumnClassName(clctefwe.getJavaClass().getName());
			result.addColumn(resultcolumnvo);
		}

		// fill the rows:
		for (MasterDataVO<Object> mdvo : lstmdvo) {
			final Object[] aoData = new Object[lstclctefweSelected.size()];

			int iColumn = 0;
			for (CollectableEntityField clctefwe : lstclctefweSelected) {
				final UID sFieldUID = clctefwe.getUID();
				final UID sFieldEntityUID = clctefwe.getEntityUID();

				if (sFieldEntityUID.equals(entityUID)) {
					// own attribute:
					final Object davo = mdvo.getFieldValue(sFieldUID);
					aoData[iColumn] = davo;
				}
				else {
					// subform field:
					IDependentKey dependentKey = SpringApplicationContextHolder.getBean(LayoutFacadeLocal.class).getDependentKeyBetween(entityUID, sFieldEntityUID, null);
					final Collection<EntityObjectVO<?>> collmdvo = mdvo.getDependents().getData(dependentKey);
					final List<Object> values = CollectionUtils.transform(collmdvo, new Transformer<EntityObjectVO<?>, Object>() {
						@Override
						public Object transform(EntityObjectVO<?> i) {
							return i.getFieldValue(sFieldUID);
						}
					});
					aoData[iColumn] = values;
				}

				iColumn++;
			}
			result.addRow(aoData);
		}

		return result;
	}

	@Override
	public NuclosFile prepareExport(ResultVO resultvo, Format format) throws NuclosReportException {
		final List<ReportFieldDefinition> fields = ReportFieldDefinitionFactory.getFieldDefinitions(resultvo);
		final Export export = getExportInstance(format);
		return export.export(null, resultvo, fields);
	}
	
	@Override
	public NuclosFile prepareExport(String searchCondition, ResultVO resultvo, Format format, PageOrientation orientation, boolean columnScaled) throws NuclosReportException {
		final List<ReportFieldDefinition> fields = ReportFieldDefinitionFactory.getFieldDefinitions(resultvo);
		final Export export = ReportExportFactory.getExport(searchCondition, format, orientation, columnScaled);
		return export.export(null, resultvo, fields);
	}

	/**
	 * @param reportUID
	 *            report/form id
	 * @return Is save allowed for the report/form with the given id?
	 */
	@RolesAllowed("Login")
	public boolean isSaveAllowed(final UID reportUID) {
		return securityCache.getWritableReportIds(getCurrentUserName(), getCurrentMandatorUID()).contains(reportUID);
	}

	/**
	 * finds reports readable for current user
	 * 
	 * @return collection of report UIDs
	 */
	public Collection<UID> getReadableReportUIDsForCurrentUser() {
		final Collection<UID> result = new HashSet<UID>();
		final Map<ReportType, Collection<UID>> readableReports = securityCache.getReadableReports(getCurrentUserName(), getCurrentMandatorUID());
		for (final ReportType rt : readableReports.keySet()) {
			result.addAll(readableReports.get(rt));			
		}
		return result;
	}

	public NuclosReportRemotePrintService lookupDefaultPrintService() throws NuclosReportException {
		PrintService ps = PrintServiceLookup.lookupDefaultPrintService();
		if (ps == null) {
			String defprinter = System.getProperty("javax.print.defaultPrinter");
			if (defprinter != null) {
				PrintService[] prservices = PrintServiceLookup.lookupPrintServices(null, null);
				if (null == prservices || 0 >= prservices.length) {
					throw new NuclosReportException("Es ist kein passender Print-Service installiert. " + defprinter); // @todo
				}
				
				for (int i = 0; i < prservices.length; i++) {
					PrintService printService = prservices[i];
					if (printService.getName().equals(defprinter)) {
						return new NuclosReportRemotePrintService(printService);
					}
				}
				throw new NuclosReportException("Es ist kein passender Default Print-Service installiert. " + defprinter); // @todo
			}
			throw new NuclosReportException("Es ist kein passender Default Print-Service installiert."); // @todo
		}
		return new NuclosReportRemotePrintService(ps);
	}

	public NuclosReportRemotePrintService[] lookupPrintServices(DocFlavor flavor, AttributeSet as) throws NuclosReportException {
		final PrintService prservDflt = PrintServiceLookup.lookupDefaultPrintService();
		PrintService[] prservices = PrintServiceLookup.lookupPrintServices(flavor, as);
		if (null == prservices || 0 >= prservices.length) {
			if (null != prservDflt) {
				prservices = new PrintService[] { prservDflt };
			}
			else {
				throw new NuclosReportException("Es ist kein passender Print-Service installiert."); // @todo
			}
		}

		final NuclosReportRemotePrintService[] rprservices = new NuclosReportRemotePrintService[prservices.length];
		for (int i = 0; i < prservices.length; i++) {
			rprservices[i] = new NuclosReportRemotePrintService(prservices[i]);
		}
		return rprservices;
	}

	public void printViaPrintService(NuclosReportRemotePrintService ps, NuclosReportPrintJob pj, PrintRequestAttributeSet aset, byte[] data) throws NuclosReportException {
		try {
			final File prntFile = getFileFromBytes(data);
			pj.print(ps, prntFile.getAbsolutePath(), aset);
		}
		catch (Exception e) {
			throw new NuclosReportException(e.getMessage());
		}
	}

	private static File getFileFromBytes(byte[] data) throws IOException {
		final File file = File.createTempFile("report_", ".tmp");
		file.deleteOnExit();

		final OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
		try {
			os.write(data);
		}
		finally {
			os.close();
		}
		return file;
	}
	
	public String getJRClasspath() {
		return jrClasspath;
	}
	
	@Override
	public UID getCurrentMandatorUID() {
		return super.getCurrentMandatorUID();
	}
	
	/**
	 * FOR USE BY EXTENSIONS
	 * Add a ReportExecutionListener that is called when a report is executed
	 * 
	 * @param reportExecutionListener ReportExecutionListener
	 */
	public void addReportExecutionListener(ReportExecutionListener reportExecutionListener) {
		if (reportExecutionListeners == null) {
			reportExecutionListeners = new ArrayList<ReportExecutionListener>();
		}
		reportExecutionListeners.add(reportExecutionListener);
	}
	
	/**
	 * FOR USE BY EXTENSIONS
	 * Remove a ReportExecutionListener
	 * 
	 * @param reportExecutionListener ReportExecutionListener
	 */
	public void removeReportExecutionListener(ReportExecutionListener reportExecutionListener) {
		if (reportExecutionListeners != null) {
			reportExecutionListeners.remove(reportExecutionListener);
		}
	}
	
	private void fireReportExecutionEvents(UID reportUID) {
		if (reportExecutionListeners != null) {
			for (ReportExecutionListener reportExecutionListener : reportExecutionListeners) {
				reportExecutionListener.reportExecuted(new ReportExecutionEvent(reportUID));
			}
		}
	}
	
}