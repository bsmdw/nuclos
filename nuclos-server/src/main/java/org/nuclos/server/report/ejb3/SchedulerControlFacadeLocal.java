//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.report.ejb3;

import java.util.Set;

import org.nuclos.common2.exception.CommonSchedulerException;
import org.nuclos.server.job.valueobject.JobVO;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Trigger;

// @Local
public interface SchedulerControlFacadeLocal {

	/**
	 * Create a quartz job.
	 *
	 * @param job Job to create
	 * @throws CommonBusinessException
	 */
	JobDetail addJob(JobVO job) throws CommonSchedulerException;

	/**
	 * Delete a quartz job including all existing triggers.
	 */
	void deleteJob(JobKey jobKey) throws CommonSchedulerException;

	/**
	 * Schedule the job with a cron expression.
	 *
	 * @param jobVO The job object containing jobname, cron expression and start time.
	 * @return Quartz trigger with calculated next fire time.
	 */
	Trigger scheduleJob(JobVO jobVO) throws CommonSchedulerException;

	/**
	 * Unschedule a job (remove all triggers).
	 */
	void unscheduleJob(JobVO jobVO) throws CommonSchedulerException;

	/**
	 * Unschedule a job by name (remove all triggers).
	 */
	void unscheduleJob(JobKey jobKey) throws CommonSchedulerException;

	/**
	 * @return the names of all scheduled jobs.
	 */
	Set<JobKey> _getJobKeys();

	/**
	 * Check if job is scheduled
	 */
	boolean isScheduled(JobKey jobKey);

	/**
	 * Trigger immediate job execution
	 */
	void triggerJob(JobVO jobVO) throws CommonSchedulerException;
}
