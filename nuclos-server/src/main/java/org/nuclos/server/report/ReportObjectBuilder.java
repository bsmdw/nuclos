package org.nuclos.server.report;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;

import org.nuclos.common.E;
import org.nuclos.common.NuclosEntityValidator;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInIdCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collection.multimap.MultiListHashMap;
import org.nuclos.common.collection.multimap.MultiListMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.report.valueobject.DefaultReportOutputVO;
import org.nuclos.common.report.valueobject.DefaultReportVO;
import org.nuclos.common.report.valueobject.ReportVO.ReportType;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.common.INucletCache;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorUtils;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.nbo.AbstractNuclosObjectCompiler.NuclosBusinessJavaSource;
import org.nuclos.server.nbo.NuclosObjectBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("reportObjectBuilder")
public class ReportObjectBuilder extends NuclosObjectBuilder {

	private static final String DEFFAULT_PACKAGE_NUCLET = "org.nuclet.report";
	private static final String DEFFAULT_ENTITY_PREFIX = "RE";
	private static final String DEFFAULT_ENTITY_POSTFIX = "RE";

	@Autowired
	private ReportObjectCompiler reportObjectCompiler;

	@Autowired
	private MetaProvider provider;

	@Autowired
	private NucletDalProvider nucletDalProvider;

	public void createObjects(final ForkJoinPool builderThreadPool) throws NuclosCompileException, InterruptedException {

		CollectableSearchExpression clctExpr = new CollectableSearchExpression(SearchConditionUtils.newComparison(E.REPORT.withRuleClass, ComparisonOperator.EQUAL, Boolean.TRUE));
		// get all reports and report outputs:
		List<EntityObjectVO<UID>> allReports = nucletDalProvider
			.getEntityObjectProcessor(E.REPORT).getBySearchExpression(clctExpr);
		List<UID> allReportIds = new ArrayList<>();
		for (EntityObjectVO<UID> report : allReports) {
			allReportIds.add(report.getPrimaryKey());
		}
		if (Thread.currentThread().isInterrupted()) {
			throw new InterruptedException();
		}
		CollectableSearchExpression clctExprOutputs = new CollectableSearchExpression(new CollectableInIdCondition<UID>(SearchConditionUtils.newEntityField(E.REPORTOUTPUT.parent), allReportIds));
		List<EntityObjectVO<UID>> allOutputs = nucletDalProvider
			.getEntityObjectProcessor(E.REPORTOUTPUT).getBySearchExpression(clctExprOutputs);
		if (Thread.currentThread().isInterrupted()) {
			throw new InterruptedException();
		}
		MultiListMap<UID, EntityObjectVO<UID>> reportOutputsByReportUID =
			new MultiListHashMap<>(allReports.size());
		for (EntityObjectVO<UID> output : allOutputs) {
			if (Thread.currentThread().isInterrupted()) {
				throw new InterruptedException();
			}
			reportOutputsByReportUID.addValue(output.getFieldUid(E.REPORTOUTPUT.parent), output);
		}

		List<NuclosBusinessJavaSource> reportSources = new ArrayList<>(allReports.size());
		for (EntityObjectVO<UID> reportEO : allReports) {
			if (Thread.currentThread().isInterrupted()) {
				throw new InterruptedException();
			}
			DefaultReportVO sourceVO = MasterDataWrapper.getReportVO(new MasterDataVO<>(
				reportEO, false), "INITIAL", null);
			if (ReportType.REPORT.equals(sourceVO.getType())) {
				reportSources.add(
					createJavaSourceFile(sourceVO, reportEO.getFieldUid(E.REPORT.nuclet),
					                     reportOutputsByReportUID));
			}
		}

		if (reportSources.size() > 0) {
			reportObjectCompiler.compileSourcesAndJar(builderThreadPool, reportSources);
		}

	}

	private NuclosBusinessJavaSource createJavaSourceFile(
		DefaultReportVO model,
		UID nucletUID,
		MultiListMap<UID, EntityObjectVO<UID>> reportOutputMap) throws InterruptedException {

		String sPackage = getNucletPackageStatic(nucletUID, provider);
		String unformatedEntity =
			NuclosEntityValidator.escapeJavaIdentifier(model.getName(), DEFFAULT_ENTITY_PREFIX);
		String formatEntity = unformatedEntity.substring(0, 1).toUpperCase()
		                            + unformatedEntity.substring(1) + DEFFAULT_ENTITY_POSTFIX;
		String qname = sPackage + "." + formatEntity;
		String filename = NuclosCodegeneratorUtils
			.reportSource(sPackage, formatEntity).toString();
		String content =
			createJavaSourceContent(sPackage, formatEntity, model, reportOutputMap);

		return new NuclosBusinessJavaSource(qname, filename, content, true);
	}

	private String createJavaSourceContent(
		String sPackage,
		String formatEntity,
		DefaultReportVO model,
		MultiListMap<UID, EntityObjectVO<UID>> reportOutputMap) throws InterruptedException {

		StringBuilder javaSourceCode = new StringBuilder(1024);
		javaSourceCode
			.append("package ").append(sPackage).append(";\n\n")
			.append("import org.nuclos.api.report.Report;\n")
			.append("import org.nuclos.common.UID;\n")
			.append("import org.nuclos.api.report.OutputFormat;\n\n")
			.append("import org.nuclos.server.nbo.AbstractOutputFormat;\n\n")

			.append("public class ").append(formatEntity).append(" implements Report {\n\n")

			.append(createJavaFileContent(model, reportOutputMap))

			.append("\n\n}");
		return javaSourceCode.toString();
	}

	private String createJavaFileContent(
		DefaultReportVO model,
		MultiListMap<UID, EntityObjectVO<UID>> reportOutputMap) throws InterruptedException {

		StringBuilder code = new StringBuilder();

		List<EntityObjectVO<UID>> allReportOutputs = reportOutputMap.getValues(model.getId());

		Map<String, Integer> lstUsedReportNames = new HashMap<>();

		for (EntityObjectVO<UID> reportOutputEO : allReportOutputs) {
			if (Thread.currentThread().isInterrupted()) {
				throw new InterruptedException();
			}
			DefaultReportOutputVO reportOutputVO =
				MasterDataWrapper.getReportOutputVO(new MasterDataVO<>(reportOutputEO, true));

			String formatName = validateOutputFormatName(reportOutputVO.getDescription());
			Integer formatNumber = null;
			
			if (StringUtils.looksEmpty(formatName)) {
				continue;
			}

			if (lstUsedReportNames.containsKey(formatName)) {
				formatNumber = lstUsedReportNames.get(formatName) + 1;
				lstUsedReportNames.put(formatName, formatNumber);
			} else {
				lstUsedReportNames.put(formatName, 1);
			}

			String formatNumberAsString = formatNumber != null ? "_" + formatNumber : "";
			code.append("public static final OutputFormat ")
				.append(formatName)
				.append(formatNumberAsString)
				.append(" = new AbstractOutputFormat(UID.parseUID(\"")
				.append(reportOutputVO.getId().getString()).append("\")){};\n");

		}

		code.append("\n\npublic org.nuclos.api.UID getId() {\n\treturn UID.parseUID(\"")
			.append(model.getId().getString()).append("\"); \n}\n");

		return code.toString();
	}

	public static String getNucletPackageStatic(UID nucletUID, INucletCache nucletCache) {
		String retVal = DEFFAULT_PACKAGE_NUCLET;

		if (nucletUID != null) {
			String sFqn = nucletCache.getFullQualifiedNucletName(nucletUID);
			if (!StringUtils.looksEmpty(sFqn)) {
				retVal = sFqn;
			}
		}

		return retVal;
	}

	protected static String validateOutputFormatName(String value) {
		String retVal = value;

		retVal = retVal.replace("ä", "ae");
		retVal = retVal.replace("ü", "ue");
		retVal = retVal.replace("ö", "oe");
		retVal = retVal.replace("Ä", "Ae");
		retVal = retVal.replace("Ü", "Ue");
		retVal = retVal.replace("Ö", "Oe");
		retVal = retVal.replace("ß", "ss");

		retVal = retVal.replaceAll("[^a-zA-Z ]+", "");
		retVal = retVal.replaceAll(" ", "_");

		return retVal;
	}

}
