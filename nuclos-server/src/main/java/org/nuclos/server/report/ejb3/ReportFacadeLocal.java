//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.report.ejb3;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.print.DocFlavor;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.NuclosFile;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.report.NuclosReportException;
import org.nuclos.common.report.NuclosReportPrintJob;
import org.nuclos.common.report.NuclosReportRemotePrintService;
import org.nuclos.common.report.valueobject.DefaultReportOutputVO;
import org.nuclos.common.report.valueobject.DefaultReportVO;
import org.nuclos.common.report.valueobject.ReportOutputVO;
import org.nuclos.common.report.valueobject.SubreportVO;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;

// @Local
public interface ReportFacadeLocal {

	/**
	 * @return all reports
	 * @throws CommonPermissionException
	 */
	Collection<DefaultReportVO> getReports()
		throws CommonPermissionException;

	/**
	 * get output formats for report
	 * @param reportUID id of report
	 * @return collection of output formats
	 */
	Collection<DefaultReportOutputVO> getReportOutputs(final UID reportUID);

	/**
	 * get output format for reportoutput id
	 * @param reportOutputUID
	 * @return reportoutput
	 */
	ReportOutputVO getReportOutput(final UID reportOutputUID)
		throws CommonFinderException, CommonPermissionException;

	/**
	 * get subreports for reportoutput
	 * 
	 * @return collection of subreports
	 */
	Collection<SubreportVO> getSubreports(final EntityMeta<UID> entity, final UID reportOutputUID);

	/**
	 * gets a report/form filled with data
	 * @param reportOutputUID
	 * @param mpParams parameters
	 * @return report/form filled with data
	 */
	NuclosFile prepareReport(final UID reportOutputUID,
		final Map<String, Object> mpParams, final Integer iMaxRowCount, UID language, UID mandatorUID)
		throws CommonFinderException, NuclosReportException,
		CommonPermissionException;

	NuclosReportRemotePrintService lookupDefaultPrintService() throws NuclosReportException;

	NuclosReportRemotePrintService[] lookupPrintServices(DocFlavor flavor, AttributeSet as) throws NuclosReportException;

	void printViaPrintService(NuclosReportRemotePrintService ps, NuclosReportPrintJob pj, PrintRequestAttributeSet aset, byte[] data) throws NuclosReportException;

	/**
	 * Get classpath for compiling JasperReport templates
	 */
	String getJRClasspath();
	

	public List<DefaultReportVO> findReportsByUsage(final UsageCriteria usagecriteria);
	
	UID getCurrentMandatorUID();
	
}
