package org.nuclos.server.common.mail.properties;

import java.util.Properties;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class POP3ConnectionProperties extends MailConnectionProperties {
	/**  */
	private static final long serialVersionUID = 1L;

	public POP3ConnectionProperties() {
		super("pop3");

		setSocketFactoryClass("javax.net.DefaultSocketFactory");
	}

	public boolean isUsingIMAP() {
		return false;
	}

	@Override
	public Properties toProperties() {
		Properties p = new Properties();

		p.put("mail.pop3.host", getHost());
		p.put("mail.pop3.port", getPort());
		p.put("mail.pop3.auth", "true");
		p.put("mail.pop3.socketFactory.class", getSocketFactoryClass());

		return p;
	}

	public void setUser(String user) {
		this.username = user;
	}

	@Override
	public String getFolderFrom() {
		return "INBOX";
	}

	@Override
	public String getFolderTo() {
		return null;
	}
}
