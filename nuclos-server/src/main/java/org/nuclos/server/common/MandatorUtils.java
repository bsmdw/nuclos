//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.FieldMeta.Valueable;
import org.nuclos.common.MandatorLevelVO;
import org.nuclos.common.MandatorVO;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInIdCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.entityobject.CollectableEOEntityField;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MandatorUtils {

	@Autowired
	private SessionUtils utils;
	
	@Autowired
	private NuclosUserDetailsContextHolder userCtx;
	
	// do not use Autowired!
	private SpringLocaleDelegate localeDelegate;
	
	@Autowired
	private SpringDataBaseHelper dbHelper;
	
	public UID getMandatorFromDb(Object pk, EntityMeta<?> entity) {
		if (!entity.isMandator()) {
			return null;
		}
		DbQueryBuilder builder = dbHelper.getDbAccess().getQueryBuilder();
		DbQuery<UID> query = builder.createQuery(UID.class);
		DbFrom t = query.from(entity);
		query.select(t.baseColumn(SF.MANDATOR_UID.getMetaData(entity)));
		query.where(builder.equalValue(t.basePk(), pk));
		UID mandatorUID = dbHelper.getDbAccess().executeQuerySingleResult(query);
		return mandatorUID;
	}
	
	public void checkWriteAllowedFromDb(Object pk, EntityMeta<?> entity) throws CommonPermissionException {
		if(!utils.isCalledRemotely()) {
			return;
		}
		if (!entity.isMandator()) {
			return;
		}
		UID mandatorUID = getMandatorFromDb(pk, entity);
		checkWriteAllowed(mandatorUID, entity);
	}
	
	public void checkWriteAllowed(EntityObjectVO<?> eo, EntityMeta<?> entity) throws CommonPermissionException {
		if (!entity.isMandator()) {
			return;
		}
		UID mandatorUID = eo.getFieldUid(SF.MANDATOR_UID);
		checkWriteAllowed(mandatorUID, entity);
	}
	
	public void checkWriteAllowed(UID mandatorUID, EntityMeta<?> entity) throws CommonPermissionException {
		if(!utils.isCalledRemotely()) {
			return;
		}
		if (!entity.isMandator()) {
			return;
		}

		if (mandatorUID != null) {
			UID mandatorLevelUid = entity.getMandatorLevel();
			List<MandatorVO>  mandatorVOS = SecurityCache.getInstance().getMandatorsByLevel(mandatorLevelUid);
			boolean found = false;
			for (MandatorVO mandatorVO : mandatorVOS) {
				if (mandatorVO.getUID().equals(mandatorUID)) {
					found = true;
					break;
				}
			}
			if (!found) {
				throw new IllegalArgumentException("Mandator belongs to wrong level.");
			}
		}

		if (userCtx.getMandatorUID()==null || mandatorUID==null) {
			SecurityCache.getInstance().checkMandatorLoginPermission(utils.getCurrentUserName(), mandatorUID);
		} else {
			if (!SecurityCache.getInstance().getAccessibleMandators(userCtx.getMandatorUID()).contains(mandatorUID)) {
				String sMandant = SecurityCache.getInstance().getMandator(mandatorUID).getPath();
				throw new CommonPermissionException(getLocaleDelegate().getMessage(
						"mandator.write.not.allowed", "Daten des Mandanten \"{0}\" können mit Ihrer aktuellen Anmeldung nicht schrieben werden.", sMandant));
			}
		}
	}
	
	public void checkMandatorChange(EntityObjectVO<?> eoFromDb, EntityObjectVO<?> eoUpdate, EntityMeta<?> entity) {
		if (!entity.isMandator()) {
			return;
		}
		UID mandatorUIDFromDb = eoFromDb.getFieldUid(SF.MANDATOR_UID);
		checkMandatorChange(mandatorUIDFromDb, eoUpdate, entity);
	}
	
	public void checkMandatorChange(EntityObjectVO<?> eoUpdate, EntityMeta<?> entity) {
		if (!entity.isMandator()) {
			return;
		}
		UID mandatorUIDFromDb = getMandatorFromDb(eoUpdate.getPrimaryKey(), entity);
		checkMandatorChange(mandatorUIDFromDb, eoUpdate, entity);
	}
	
	public void checkMandatorChange(UID mandatorUIDFromDb, EntityObjectVO<?> eoUpdate, EntityMeta<?> entity) {
		UID mandatorUIDFromUpdate = eoUpdate.getFieldUid(SF.MANDATOR_UID);
		Object pk = eoUpdate.getPrimaryKey();
		checkMandatorChange(mandatorUIDFromDb, mandatorUIDFromUpdate, pk, entity);
	}
	
	public void checkMandatorChange(UID mandatorUIDFromDb, UID mandatorUIDFromUpdate, Object pk, EntityMeta<?> entity) {
		if (!entity.isMandator()) {
			return;
		}
		if (!RigidUtils.equal(mandatorUIDFromUpdate, mandatorUIDFromDb)) {
			throw new IllegalArgumentException("Illegal argument: Mandator change on entity=" + entity.getEntityName() + 
					", pk=" + pk + 
					", mandatorFromDb=" + mandatorUIDFromDb.getString() + 
					", mandatorFromArgument=" + mandatorUIDFromUpdate.getString());
		}
	}
	
	public void checkReadAllowed(EntityObjectVO<?> eo, EntityMeta<?> entity) throws CommonPermissionException {
		if(!utils.isCalledRemotely()) {
			return;
		}
		if (!entity.isMandator()) {
			return;
		}
		UID mandatorUID = eo.getFieldUid(SF.MANDATOR_UID);
		checkReadAllowed(mandatorUID, entity);
	}
	
	public void checkReadAllowed(UID mandatorUID, EntityMeta<?> entity) throws CommonPermissionException {
		if(!utils.isCalledRemotely()) {
			return;
		}
		if (!entity.isMandator()) {
			return;
		}
		if (userCtx.getMandatorUID()==null || mandatorUID==null) {
			SecurityCache.getInstance().checkMandatorLoginPermission(utils.getCurrentUserName(), mandatorUID);
		} else {
			if (!SecurityCache.getInstance().getAccessibleMandators(userCtx.getMandatorUID()).contains(mandatorUID)) {
				String sMandant = SecurityCache.getInstance().getMandator(mandatorUID).getPath();
				throw new CommonPermissionException(getLocaleDelegate().getMessage(
						"mandator.read.not.allowed", "Daten des Mandanten \"{0}\" können mit Ihrer aktuellen Anmeldung nicht öffnet werden.", sMandant));
			}
		}
	}
	
	public CollectableSearchCondition append(
			CollectableSearchExpression expr, EntityMeta<?> entity) {
			return append(expr.getSearchCondition(), entity, expr.getMandator());
		}
	
	/**
	 * append mandator restriction to cond for given entity.
	 *
	 * @param cond
	 * @param entity
	 * @return new AND condition if any mandator restriction is set, otherwise cond
	 *         is returned.
	 */
	public CollectableSearchCondition append(
		CollectableSearchCondition cond, EntityMeta<?> entity) {
		return append(cond, entity, null);
	}
	
	public Set<UID> getAccessibleMandators(UID mandatorFromData) {
		return getAccessibleMandatorsByLogin(mandatorFromData, userCtx.getMandatorUID());
	}
	
	public static Set<UID> getAccessibleMandatorsByLogin(UID mandatorFromData, UID mandatorFromLogin) {
		Set<UID> mandators;
		if (mandatorFromData == null) {
			mandators = SecurityCache.getInstance().getAccessibleMandators(mandatorFromLogin);
		} else {
			if (mandatorFromLogin == null) {
				mandators = SecurityCache.getInstance().getAccessibleMandators(mandatorFromData);
			} else {
				if (mandatorFromData.equals(mandatorFromLogin)) {
					mandators = SecurityCache.getInstance().getAccessibleMandators(mandatorFromData);
				} else {
					mandators = CollectionUtils.intersection(
						SecurityCache.getInstance().getAccessibleMandators(mandatorFromLogin), 
						SecurityCache.getInstance().getAccessibleMandators(mandatorFromData));
				}
			}
		}
		return mandators;
	}
	
	/**
	 * append mandator restriction to cond for given entity.
	 *
	 * @param cond
	 * @param entity
	 * @param mandator (mandator of a given record)
	 * @return new AND condition if any mandator restriction is set, otherwise cond
	 *         is returned.
	 */
	public CollectableSearchCondition append(
		CollectableSearchCondition cond, EntityMeta<?> entity, UID mandator) {
		if (!utils.isCalledRemotely()) {
			return cond;
		}
		if (!entity.isMandator()) {
			if (!E.MANDATOR.checkEntityUID(entity.getUID())) {
				return cond;
			}
		}
		if (userCtx.getMandatorUID()==null && mandator==null) {
			return cond;
		}
		
		Set<UID> mandators = getAccessibleMandators(mandator);
		CompositeCollectableSearchCondition result = new CompositeCollectableSearchCondition(LogicalOperator.AND);
		if(cond != null) {
			result.addOperand(cond);
		}
		
		if (!E.MANDATOR.checkEntityUID(entity.getUID())) {
			result.addOperand(new CollectableInIdCondition<UID>(new CollectableEOEntityField(SF.MANDATOR_UID.getMetaData(entity)), new ArrayList<UID>(mandators)));
		} else {
			result.addOperand(new CollectableInCondition<UID>(new CollectableEOEntityField(E.MANDATOR.getPk().getMetaData(entity)), new ArrayList<UID>(mandators)));
		}

		return result;
	}
	
	private SpringLocaleDelegate getLocaleDelegate() {
		if (localeDelegate == null) {
			localeDelegate = SpringApplicationContextHolder.getBean(SpringLocaleDelegate.class);
			if (localeDelegate == null) {
				throw new IllegalStateException("too early");
			}
		}
		return localeDelegate;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T getByAccessibleMandators(List<EntityObjectVO<UID>> possibleValues, FieldMeta<T> valueField, FieldMeta<UID> mandatorField, Set<UID> accessibleMandators) {
		T retVal = null;
		
		if (!possibleValues.isEmpty()) {
			// Ordne die mandantenspezifischen Werte den unterschiedlichen Ebene zu.
			// Wenn eine Ebene hier nur aus einem Mandanten besteht (1),
			// und es exitiert ein spezifischer Wert für diesen (2),
			// und es gibt keine tiefere Ebene (3), so wird dieser Wert zurückgegeben.
			
			Map<UID, T> accessibleMandatorValues = new HashMap<UID, T>();
			for (EntityObjectVO<UID> valueVO : possibleValues) {
				UID manUID = valueVO.getFieldUid(mandatorField);
				if (accessibleMandators.contains(manUID)) {
					if (valueField instanceof Valueable) {
						accessibleMandatorValues.put(manUID, valueVO.getFieldValue((Valueable<T>)valueField));
					} else {
						accessibleMandatorValues.put(manUID, (T) valueVO.getFieldUid((FieldMeta<UID>) valueField));
					}
				}
			}
			
			if (!accessibleMandatorValues.isEmpty()) {
				
				T mandatorVal = null;
				
				// Die Liste der Ebenen ist sortiert (3)
				List<MandatorLevelVO> levels = SecurityCache.getInstance().getAllMandatorLevels();
				for (MandatorLevelVO level : levels) {
					int mandatorCount = 0;
					UID manUID = null;
					for (UID uid : accessibleMandators) {
						MandatorVO mandator = SecurityCache.getInstance().getMandator(uid);
						if (level.getUID().equals(mandator.getLevelUID())) {
							manUID = uid;
							mandatorCount++;
						}
					}
					if (mandatorCount == 1) {
						// (1) erfüllt
						if (accessibleMandatorValues.containsKey(manUID)) {
							// (2) erfüllt
							mandatorVal = accessibleMandatorValues.get(manUID);
						}
					}
				}
				
				if (mandatorVal != null) {
					retVal = mandatorVal;
				}
			}
		}
		
		return retVal;
	}

}
