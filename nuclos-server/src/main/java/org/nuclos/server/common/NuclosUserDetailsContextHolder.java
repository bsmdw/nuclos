//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common;

import java.util.TimeZone;
import java.util.UUID;

import javax.annotation.PreDestroy;

import org.nuclos.common.UID;
import org.nuclos.common2.InternalTimestamp;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Role;
import org.springframework.stereotype.Component;

@Component
public class NuclosUserDetailsContextHolder {

	private ThreadLocal<Object> threadLocalSP = new ThreadLocal<>();
	private ThreadLocal<InternalTimestamp> threadLocalSPTime = new ThreadLocal<>();
	private ThreadLocal<TimeZone> threadLocalTZ = new ThreadLocal<>();
	private ThreadLocal<UID> threadLocalMandator = new ThreadLocal<>();
	private ThreadLocal<UID> threadDataLocal = new ThreadLocal<>();

	public static class AllValues {
		private Object threadLocalSP;
		private InternalTimestamp threadLocalSPTime;
		private TimeZone threadLocalTZ;
		private UID threadLocalMandator;
		private UID threadDataLocal;
	}

	public NuclosUserDetailsContextHolder() {
	}

	public synchronized AllValues getAllValues() {
		final AllValues result = new AllValues();
		result.threadLocalSP = this.threadLocalSP.get();
		result.threadLocalSPTime = this.threadLocalSPTime.get();
		result.threadLocalTZ = this.threadLocalTZ.get();
		result.threadLocalMandator = this.threadLocalMandator.get();
		result.threadDataLocal = this.threadDataLocal.get();
		return result;
	}

	public synchronized void setAllValues(AllValues allValues) {
		this.threadLocalSP.set(allValues.threadLocalSP);
		this.threadLocalSPTime.set(allValues.threadLocalSPTime);
		this.threadLocalTZ.set(allValues.threadLocalTZ);
		this.threadLocalMandator.set(allValues.threadLocalMandator);
		this.threadDataLocal.set(allValues.threadDataLocal);
	}

	void setTimeZone(TimeZone tz) {
		threadLocalTZ.set(tz);
	}

	public TimeZone getTimeZone() {
		return threadLocalTZ.get();
	}

	public void createSavepoint() {
		setSavePoint(UUID.randomUUID().toString());
		setSavePointTime(new InternalTimestamp(System.currentTimeMillis()));
	}

	public Object getSavePoint() {
		return threadLocalSP.get();
	}

	private void setSavePoint(Object sp) {
		threadLocalSP.set(sp);
	}


	public InternalTimestamp getSavePointTime() {
		return threadLocalSPTime.get();
	}

	private void setSavePointTime(InternalTimestamp sp) {
		threadLocalSPTime.set(sp);
	}

	public UID getMandatorUID() {
		return threadLocalMandator.get();
	}

	public void setMandatorUID(UID mandator) {
		threadLocalMandator.set(mandator);
	}

	public void setDataLocal(UID dataLocale) {
		threadDataLocal.set(dataLocale);
	}

	public UID getDataLocal() {
		return threadDataLocal.get();
	}

	public synchronized void clear() {
		threadLocalSP.remove();
		threadLocalSPTime.remove();
		threadLocalTZ.remove();
		threadLocalMandator.remove();
		threadDataLocal.remove();
	}

	@PreDestroy
	public synchronized void destroy() {
		if (threadLocalSP != null) {
			threadLocalSP.remove();
			threadLocalSP = null;
		}
		if (threadLocalSPTime != null) {
			threadLocalSPTime.remove();
			threadLocalSPTime = null;
		}

		if (threadLocalTZ != null) {
			threadLocalTZ.remove();
			threadLocalTZ = null;
		}
		if (threadLocalMandator != null) {
			threadLocalMandator.remove();
			threadLocalMandator = null;
		}
		if (threadDataLocal != null) {
			threadDataLocal.remove();
			threadDataLocal = null;
		}
	}

}
