package org.nuclos.server.common;

import org.nuclos.common.UID;

public interface INucletCache {
	
	String getFullQualifiedNucletName(UID nucletUID);

}
