package org.nuclos.server.businesstest.sampledata;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class SampleBooleanDataGeneratorTest {

	@Test
	public void testDataGeneration() {
		SampleBooleanDataGenerator generator = new SampleBooleanDataGenerator();

		assert generator.newValue() == null;

		generator.addSampleValues(Arrays.asList(true));

		// Should always return true now
		assert generator.newValue();
		assert generator.newValue();
		assert generator.newValue();
		assert generator.newValue();
		assert generator.newValue();

		generator.addSampleValues(Arrays.asList(true, true));

		// Should always return true now
		assert generator.newValue();
		assert generator.newValue();
		assert generator.newValue();
		assert generator.newValue();
		assert generator.newValue();

		generator = new SampleBooleanDataGenerator();

		assert generator.newValue() == null;

		generator.addSampleValues(Arrays.asList(false));

		// Should always return false now
		assert !generator.newValue();
		assert !generator.newValue();
		assert !generator.newValue();
		assert !generator.newValue();
		assert !generator.newValue();

		generator.addSampleValues(Arrays.asList(true, null));
		final List<Boolean> values = new ArrayList(Arrays.asList(null, true, false));

		// true, false and null should occur now
		while (!values.isEmpty()) {
			values.remove(generator.newValue());
		}
	}
}