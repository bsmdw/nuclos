package org.nuclos.server.businesstest.codegeneration.script;

import java.io.IOException;

import org.junit.Test;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class BusinessTestDeleteScriptGeneratorTest extends AbstractBusinessTestScriptGeneratorTest {

	@Test
	public void testGenerator() throws CommonFinderException, IOException, CommonPermissionException {
		assert "DELETE B".equals(generator.getTestName());

		ScriptResult script = parseScript();

		assert script != null;
		assert script.getGroovySource().contains(".delete()");
	}

	@Override
	protected AbstractBusinessTestScriptGenerator newGenerator() {
		return getFactory(new B()).newDeleteScriptGenerator();
	}
}