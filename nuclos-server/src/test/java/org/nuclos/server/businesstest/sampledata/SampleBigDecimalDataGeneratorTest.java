package org.nuclos.server.businesstest.sampledata;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;

import org.junit.Test;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class SampleBigDecimalDataGeneratorTest {

	@Test
	public void testDataGeneration() {
		SampleBigDecimalDataGenerator generator = new SampleBigDecimalDataGenerator();

		BigDecimal sample1 = new BigDecimal(42.0).setScale(4);
		BigDecimal sample2 = new BigDecimal(45.0001).setScale(4, RoundingMode.HALF_UP);

		generator.addSampleValues(Arrays.asList(sample1));

		assert generator.newValue().compareTo(new BigDecimal(42.0)) == 0;

		generator.addSampleValues(Arrays.asList(sample2));

		for (int i=0; i<10; i++) {
			BigDecimal value = generator.newValue();
			assert value.compareTo(new BigDecimal(42.0)) >= 0;
			assert value.compareTo(new BigDecimal(45.0001)) <= 0;
			assert value.scale() == 4;
		}
	}
}