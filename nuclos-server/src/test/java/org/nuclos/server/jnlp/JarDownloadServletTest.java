package org.nuclos.server.jnlp;

import org.junit.Test;

public class JarDownloadServletTest {

	@Test
	public void testSplitJarNameVersion_nuclosJars() {
		testSplitJarNameVersion("nuclos-client-4.10.3-r4.10.2-38-ga47d45f-dirty.jar",
								"nuclos-client.jar",
								"4.10.3-r4.10.2-38-ga47d45f-dirty");
		testSplitJarNameVersion("nuclos-client-synthetica-4.10.3-r4.10.2-38-ga47d45f-dirty.jar",
								"nuclos-client-synthetica.jar",
								"4.10.3-r4.10.2-38-ga47d45f-dirty");
		testSplitJarNameVersion("nuclos-common-4.10.3-r4.10.2-38-ga47d45f-dirty.jar",
								"nuclos-common.jar",
								"4.10.3-r4.10.2-38-ga47d45f-dirty");
		testSplitJarNameVersion("nuclos-rigid-common-4.10.3-r4.10.2-38-ga47d45f-dirty.jar",
								"nuclos-rigid-common.jar",
								"4.10.3-r4.10.2-38-ga47d45f-dirty");
		testSplitJarNameVersion("nuclos-native-1.0.jar",
								"nuclos-native.jar",
								"1.0");
	}
	
	@Test
	public void testSplitJarNameVersion_jgitJar() {
		testSplitJarNameVersion("org.eclipse.jgit-3.5.0.201409260305-r.jar",
								"org.eclipse.jgit.jar",
								"3.5.0.201409260305-r");
	}
	
	@Test
	public void testSplitJarNameVersion_allInOneJar() {
		testSplitJarNameVersion("all-in-one-b6b7ef6d2cb7d6468d962d820f12646915ed4bf0.jar",
								"all-in-one.jar",
								"b6b7ef6d2cb7d6468d962d820f12646915ed4bf0");
	}
	
	@Test
	public void testSplitJarNameVersion_otherJars() {
		testSplitJarNameVersion("activemq-broker-5.11.1.jar",
								"activemq-broker.jar",
								"5.11.1");
		testSplitJarNameVersion("jfreechart-1.0.19.jar",
								"jfreechart.jar",
								"1.0.19");
		testSplitJarNameVersion("spring-web-4.1.9.RELEASE.jar",
								"spring-web.jar",
								"4.1.9.RELEASE");
	}
	
	@Test
	public void testSplitJarNameVersion_snapshot() {
		// theme etc.
		testSplitJarNameVersion("maik1-1.0-SNAPSHOT.jar",
				"maik1.jar",
				"1.0-SNAPSHOT");
	}
	
	private void testSplitJarNameVersion(String completeName, String assertName, String assertVersion) {
		String[] splitted = JnlpServlet.splitJarNameVersion(completeName);
		assert splitted.length == 2;
		assert assertName.equals(splitted[0]);
		assert assertVersion.equals(splitted[1]);
	}
	
}
