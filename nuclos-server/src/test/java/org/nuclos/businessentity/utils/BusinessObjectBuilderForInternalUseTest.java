package org.nuclos.businessentity.utils;

import org.junit.Test;

public class BusinessObjectBuilderForInternalUseTest {

	@Test
	public void testNoTemporaryExtension() {
		assert !BusinessObjectBuilderForInternalUse.isEnabled();
	}

}
