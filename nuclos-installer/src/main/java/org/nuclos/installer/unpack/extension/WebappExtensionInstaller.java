package org.nuclos.installer.unpack.extension;

import java.io.File;
import java.util.Arrays;
import java.util.List;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class WebappExtensionInstaller extends ExtensionInstaller {

	WebappExtensionInstaller(
			final InstallationContext context
	) {
		super(context);
	}

	@Override
	File getSourceDirectory() {
		return new File(context.getExtensionsDir(), DirectoryName.WEBAPP);
	}

	@Override
	List<File> getTargetDirectories() {
		return Arrays.asList(
				context.getWebappDir()
		);
	}
}
