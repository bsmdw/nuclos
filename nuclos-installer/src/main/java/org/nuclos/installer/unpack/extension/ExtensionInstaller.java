package org.nuclos.installer.unpack.extension;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.nuclos.installer.util.FileUtils;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
abstract class ExtensionInstaller {
	final InstallationContext context;

	ExtensionInstaller(
			final InstallationContext context
	) {
		this.context = context;
	}

	abstract File getSourceDirectory();
	abstract List<File> getTargetDirectories();

	/**
	 * @return List of files installed by this ExtensionInstaller
	 */
	public List<String> install() throws IOException {
		return copyAllFiles(
				getSourceDirectory(),
				getTargetDirectories()
		);
	}

	private List<String> copyAllFiles(final File sourceDir, final List<File> targetDirs) throws IOException {
		final List<String> files = new ArrayList<>();

		if (sourceDir.exists() && !FileUtils.isEmptyDir(sourceDir, true)) {
			files.addAll(FileUtils.getFiles(sourceDir));
			for (File targetDir : targetDirs) {
				FileUtils.forceMkdir(targetDir);
				files.addAll(FileUtils.copyDirectory(sourceDir, targetDir, context.getInstaller()));
			}
		}

		return files;
	}
}
