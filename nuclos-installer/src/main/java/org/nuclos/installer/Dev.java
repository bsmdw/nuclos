//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.installer;

import org.nuclos.installer.mode.GuiInstaller;
import org.nuclos.installer.mode.Installer;
import org.nuclos.installer.unpack.DevUnpacker;
import org.nuclos.installer.unpack.Unpacker;

public class Dev {

	private static Exception exception = null;

	public static void main(final String[] args) throws Exception {
		CheckJavaVersion.checkVersion(() -> {
			AbstractLauncher.initLogging();
			final Installer i = new GuiInstaller();
			final Unpacker u = new DevUnpacker();
			ConfigContext.getCurrentConfig().setDefaults(u);
			try {
				i.install(u);
			}
			catch (InstallException e) {
				exception = e;
			}
		});

		if (exception != null) {
			throw exception;
		}
	}
}