package org.nuclos.installer.util;

import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.Security;
import java.security.SignatureException;
import java.security.cert.CertPathBuilder;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertStore;
import java.security.cert.CertificateException;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.PKIXBuilderParameters;
import java.security.cert.PKIXCertPathBuilderResult;
import java.security.cert.TrustAnchor;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.apache.log4j.Logger;
import org.nuclos.installer.L10n;

public class CertificateVerifier {
	private static final Logger LOG = Logger.getLogger(CertificateVerifier.class);

	static {
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
	}

	public static void verify(X509Certificate certificate, Set<X509Certificate> intermediateCerts) throws CertificateVerificationException {
		try {
			certificate.checkValidity();
		} catch (CertificateException e) {
			throw new CertificateVerificationException(e);
		}

		checkTrust(certificate, intermediateCerts);
	}

	static void checkTrust(
			X509Certificate cert,
			Set<X509Certificate> intermediateCerts
	) throws CertificateVerificationException {
		try {
			// Check for self-signed certificate
			if (isSelfSigned(cert)) {
				throw new CertificateVerificationException(L10n.getMessage("validation.keystore.certificate.selfsigned"));
			}

			// Prepare a set of trusted root CA certificates
			// and a set of intermediate certificates
			Set<X509Certificate> trustedRootCerts = getRootCAs();

			// Defensive copy
			intermediateCerts = new HashSet<>(intermediateCerts);
			// Certificate itself must be added to intermediates for the verification to work
			intermediateCerts.add(cert);

			// Attempt to build the certification chain and verify it
			PKIXCertPathBuilderResult verifiedCertChain = checkTrust(cert, trustedRootCerts, intermediateCerts);

			// Check whether the certificate is revoked by the CRL
			// given in its CRL distribution point extension
//			CRLVerifier.verifyCertificateCRLs(cert);

			// The chain is built and verified. Return it as a result
//			return verifiedCertChain;
		} catch (CertPathBuilderException certPathEx) {
			throw new CertificateVerificationException(
					L10n.getMessage("validation.keystore.certificate.untrusted"),
					certPathEx
			);
		} catch (CertificateVerificationException cvex) {
			throw cvex;
		} catch (Exception ex) {
			throw new CertificateVerificationException(ex);
		}
	}

	/**
	 * Checks whether given X.509 certificate is self-signed.
	 */
	private static boolean isSelfSigned(X509Certificate cert)
			throws CertificateException, NoSuchAlgorithmException,
			NoSuchProviderException {
		try {
			// Try to verify certificate signature with its own public key
			PublicKey key = cert.getPublicKey();
			cert.verify(key);
			return true;
		} catch (SignatureException | InvalidKeyException sigEx) {
			// Invalid signature --> not self-signed
			return false;
		}
	}

	/**
	 * Attempts to build a certification chain for given certificate and to verify
	 * it. Relies on a set of root CA certificates (trust anchors) and a set of
	 * intermediate certificates (to be used as part of the chain).
	 * @param cert - certificate for validation
	 * @param trustedRootCerts - set of trusted root CA certificates
	 * @param intermediateCerts - set of intermediate certificates
	 * @return the certification chain (if verification is successful)
	 * @throws GeneralSecurityException - if the verification is not successful
	 *      (e.g. certification path cannot be built or some certificate in the
	 *      chain is expired)
	 */
	private static PKIXCertPathBuilderResult checkTrust(
			X509Certificate cert,
			Set<X509Certificate> trustedRootCerts,
			Set<X509Certificate> intermediateCerts
	) throws GeneralSecurityException {

		// Create the selector that specifies the starting certificate
		X509CertSelector selector = new X509CertSelector();
		selector.setCertificate(cert);

		// Create the trust anchors (set of root CA certificates)
		Set<TrustAnchor> trustAnchors = new HashSet<>();
		for (X509Certificate trustedRootCert : trustedRootCerts) {
			trustAnchors.add(new TrustAnchor(trustedRootCert, null));
		}

		// Configure the PKIX certificate builder algorithm parameters
		PKIXBuilderParameters pkixParams = new PKIXBuilderParameters(trustAnchors, selector);

		// Disable CRL checks (this is done manually as additional step)
		pkixParams.setRevocationEnabled(false);

		// Specify a list of intermediate certificates
		CertStore intermediateCertStore = CertStore.getInstance(
				"Collection",
				new CollectionCertStoreParameters(intermediateCerts),
				"BC"
		);
		pkixParams.addCertStore(intermediateCertStore);

		// Build and verify the certification chain
		CertPathBuilder builder = CertPathBuilder.getInstance("PKIX", "BC");
		PKIXCertPathBuilderResult result = (PKIXCertPathBuilderResult) builder.build(pkixParams);
		return result;
	}

	static Set<X509Certificate> getRootCAs() throws KeyStoreException, NoSuchAlgorithmException {
		TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		trustManagerFactory.init((KeyStore) null);

		Set<X509Certificate> rootCAs = new HashSet<>();

		for (TrustManager trustManager : trustManagerFactory.getTrustManagers()) {
			if (trustManager instanceof X509TrustManager) {
				X509TrustManager x509TrustManager = (X509TrustManager) trustManager;
				rootCAs.addAll(Arrays.asList(x509TrustManager.getAcceptedIssuers()));
			}
		}

		return rootCAs;
	}

}
