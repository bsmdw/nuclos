package org.nuclos.installer.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.nuclos.installer.L10n;

public class KeyStoreVerifier {
	private static final Logger LOG = Logger.getLogger(KeyStoreVerifier.class);


	public static void verify(File keystoreFile, String password) throws KeystoreVerificationException {
		try (final InputStream is = new FileInputStream(keystoreFile)) {
			verify(is, password);
		} catch (IOException e) {
			throw new KeystoreVerificationException(e);
		}
	}

	public static void verify(InputStream keystoreInputStream, String password) throws KeystoreVerificationException {
		try {
			KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
			keystore.load(keystoreInputStream, password.toCharArray());

			Enumeration<String> aliases = keystore.aliases();
			boolean foundValidAlias = false;

			StringBuilder sb = new StringBuilder();

			while (aliases.hasMoreElements()) {
				String alias = aliases.nextElement();
				try {
					Key key = getKey(keystore, alias, password);
					if (key instanceof PrivateKey) {
						X509Certificate cert;
						Set<X509Certificate> intermediateCertificates = new HashSet<>();

						// Get certificate of public key
						Certificate[] chain = keystore.getCertificateChain(alias);
						if (chain != null) {
							// The server certificate (which we want to verify) is the first in the chain
							cert = (X509Certificate) chain[0];

							// The remaining certificates are intermediates
							for (int i = 1; i < chain.length; i++) {
								intermediateCertificates.add((X509Certificate) chain[i]);
							}
						} else {
							// No chain means there is only the certificate itself in the keystore (no intermediates)
							cert = (X509Certificate) keystore.getCertificate(alias);
						}

						LOG.info("Verifying certificate: " + cert);
						CertificateVerifier.verify(cert, intermediateCertificates);
						foundValidAlias = true;
						break;
					}
				} catch (Exception e) {
					LOG.warn("Verification failed for alias " + alias, e);
					sb.append(alias + ": " + e.getMessage());
					sb.append("\n");
				}
			}

			if (!foundValidAlias) {
				String exceptionMessage = L10n.getMessage("validation.keystore.no.valid.alias");

				if (!StringUtils.isBlank(sb.toString())) {
					exceptionMessage += L10n.getMessage("validation.keystore.aliases", sb.toString());
				}

				throw new KeystoreVerificationException(exceptionMessage);
			}
		} catch (Exception e) {
			throw new KeystoreVerificationException(e);
		}
	}

	/**
	 * Tries to get the key with the given alias,
	 * first using no password, then using the given password.
	 */
	private static Key getKey(KeyStore keyStore, String alias, String password) {
		try {
			return keyStore.getKey(alias, "".toCharArray());
		} catch (Exception e) {
			// Maybe a key password is required, ignore
		}

		try {
			return keyStore.getKey(alias, password.toCharArray());
		} catch (Exception e) {
			LOG.warn("Failed to get key with alias " + alias);
		}

		return null;
	}
}
