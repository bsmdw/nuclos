package org.nuclos.installer.tomcat;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.xml.transform.TransformerException;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.nuclos.installer.InstallException;
import org.xml.sax.SAXException;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class CatalinaTest {

	private File tomcatHome;

	@Before
	public void setup() throws IOException {
		Path temp = Files.createTempDirectory(CatalinaTest.class.getSimpleName());
		tomcatHome = temp.toFile();

		FileUtils.copyInputStreamToFile(
				CatalinaTest.class.getResourceAsStream("web.xml"),
				new File(tomcatHome, "conf/web.xml")
		);
	}

	@Test
	public void configureWebXmlForRedirect() throws SAXException, TransformerException, InstallException, IOException {
		Catalina catalina = new Catalina(tomcatHome, false);

		catalina.configureWebXmlForRedirect();
	}
}