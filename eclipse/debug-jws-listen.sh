#!/bin/bash
# http://docs.oracle.com/javase/1.5.0/docs/guide/javaws/developersguide/troubleshooting.03.06.html
javaws -verbose -J-Xdebug -J-Xnoagent -J-Djava.compiler=NONE -J-Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5005 $*
