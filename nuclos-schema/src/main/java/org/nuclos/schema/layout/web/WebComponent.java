
package org.nuclos.schema.layout.web;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;


/**
 * Base type for any web component.
 * 
 * <p>Java class for web-component complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="web-component"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="advanced-properties" type="{urn:org.nuclos.schema.layout.web}web-advanced-property" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" /&gt;
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="column" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&gt;
 *       &lt;attribute name="row" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&gt;
 *       &lt;attribute name="colspan" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&gt;
 *       &lt;attribute name="rowspan" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&gt;
 *       &lt;attribute name="font-size" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="text-color" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="bold" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="italic" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="underline" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="next-focus-component" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="next-focus-field" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="alternative-tooltip" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "web-component", propOrder = {
    "advancedProperties"
})
@XmlSeeAlso({
    WebContainer.class,
    WebLabel.class,
    WebLabelStatic.class,
    WebSeparator.class,
    WebTitledSeparator.class,
    WebAddon.class,
    WebInputComponent.class
})
public abstract class WebComponent implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "advanced-properties")
    protected List<WebAdvancedProperty> advancedProperties;
    @XmlAttribute(name = "id")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;
    @XmlAttribute(name = "name", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String name;
    @XmlAttribute(name = "column")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger column;
    @XmlAttribute(name = "row")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger row;
    @XmlAttribute(name = "colspan")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger colspan;
    @XmlAttribute(name = "rowspan")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger rowspan;
    @XmlAttribute(name = "font-size")
    protected String fontSize;
    @XmlAttribute(name = "text-color")
    protected String textColor;
    @XmlAttribute(name = "bold")
    protected Boolean bold;
    @XmlAttribute(name = "italic")
    protected Boolean italic;
    @XmlAttribute(name = "underline")
    protected Boolean underline;
    @XmlAttribute(name = "next-focus-component")
    protected String nextFocusComponent;
    @XmlAttribute(name = "next-focus-field")
    protected String nextFocusField;
    @XmlAttribute(name = "alternative-tooltip")
    protected String alternativeTooltip;

    /**
     * Gets the value of the advancedProperties property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the advancedProperties property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdvancedProperties().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WebAdvancedProperty }
     * 
     * 
     */
    public List<WebAdvancedProperty> getAdvancedProperties() {
        if (advancedProperties == null) {
            advancedProperties = new ArrayList<WebAdvancedProperty>();
        }
        return this.advancedProperties;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the column property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getColumn() {
        return column;
    }

    /**
     * Sets the value of the column property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setColumn(BigInteger value) {
        this.column = value;
    }

    /**
     * Gets the value of the row property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRow() {
        return row;
    }

    /**
     * Sets the value of the row property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRow(BigInteger value) {
        this.row = value;
    }

    /**
     * Gets the value of the colspan property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getColspan() {
        return colspan;
    }

    /**
     * Sets the value of the colspan property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setColspan(BigInteger value) {
        this.colspan = value;
    }

    /**
     * Gets the value of the rowspan property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRowspan() {
        return rowspan;
    }

    /**
     * Sets the value of the rowspan property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRowspan(BigInteger value) {
        this.rowspan = value;
    }

    /**
     * Gets the value of the fontSize property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFontSize() {
        return fontSize;
    }

    /**
     * Sets the value of the fontSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFontSize(String value) {
        this.fontSize = value;
    }

    /**
     * Gets the value of the textColor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextColor() {
        return textColor;
    }

    /**
     * Sets the value of the textColor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextColor(String value) {
        this.textColor = value;
    }

    /**
     * Gets the value of the bold property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBold() {
        return bold;
    }

    /**
     * Sets the value of the bold property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBold(Boolean value) {
        this.bold = value;
    }

    /**
     * Gets the value of the italic property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isItalic() {
        return italic;
    }

    /**
     * Sets the value of the italic property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setItalic(Boolean value) {
        this.italic = value;
    }

    /**
     * Gets the value of the underline property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUnderline() {
        return underline;
    }

    /**
     * Sets the value of the underline property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUnderline(Boolean value) {
        this.underline = value;
    }

    /**
     * Gets the value of the nextFocusComponent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextFocusComponent() {
        return nextFocusComponent;
    }

    /**
     * Sets the value of the nextFocusComponent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextFocusComponent(String value) {
        this.nextFocusComponent = value;
    }

    /**
     * Gets the value of the nextFocusField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextFocusField() {
        return nextFocusField;
    }

    /**
     * Sets the value of the nextFocusField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextFocusField(String value) {
        this.nextFocusField = value;
    }

    /**
     * Gets the value of the alternativeTooltip property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlternativeTooltip() {
        return alternativeTooltip;
    }

    /**
     * Sets the value of the alternativeTooltip property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlternativeTooltip(String value) {
        this.alternativeTooltip = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<WebAdvancedProperty> theAdvancedProperties;
            theAdvancedProperties = (((this.advancedProperties!= null)&&(!this.advancedProperties.isEmpty()))?this.getAdvancedProperties():null);
            strategy.appendField(locator, this, "advancedProperties", buffer, theAdvancedProperties);
        }
        {
            String theId;
            theId = this.getId();
            strategy.appendField(locator, this, "id", buffer, theId);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            BigInteger theColumn;
            theColumn = this.getColumn();
            strategy.appendField(locator, this, "column", buffer, theColumn);
        }
        {
            BigInteger theRow;
            theRow = this.getRow();
            strategy.appendField(locator, this, "row", buffer, theRow);
        }
        {
            BigInteger theColspan;
            theColspan = this.getColspan();
            strategy.appendField(locator, this, "colspan", buffer, theColspan);
        }
        {
            BigInteger theRowspan;
            theRowspan = this.getRowspan();
            strategy.appendField(locator, this, "rowspan", buffer, theRowspan);
        }
        {
            String theFontSize;
            theFontSize = this.getFontSize();
            strategy.appendField(locator, this, "fontSize", buffer, theFontSize);
        }
        {
            String theTextColor;
            theTextColor = this.getTextColor();
            strategy.appendField(locator, this, "textColor", buffer, theTextColor);
        }
        {
            Boolean theBold;
            theBold = this.isBold();
            strategy.appendField(locator, this, "bold", buffer, theBold);
        }
        {
            Boolean theItalic;
            theItalic = this.isItalic();
            strategy.appendField(locator, this, "italic", buffer, theItalic);
        }
        {
            Boolean theUnderline;
            theUnderline = this.isUnderline();
            strategy.appendField(locator, this, "underline", buffer, theUnderline);
        }
        {
            String theNextFocusComponent;
            theNextFocusComponent = this.getNextFocusComponent();
            strategy.appendField(locator, this, "nextFocusComponent", buffer, theNextFocusComponent);
        }
        {
            String theNextFocusField;
            theNextFocusField = this.getNextFocusField();
            strategy.appendField(locator, this, "nextFocusField", buffer, theNextFocusField);
        }
        {
            String theAlternativeTooltip;
            theAlternativeTooltip = this.getAlternativeTooltip();
            strategy.appendField(locator, this, "alternativeTooltip", buffer, theAlternativeTooltip);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof WebComponent)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final WebComponent that = ((WebComponent) object);
        {
            List<WebAdvancedProperty> lhsAdvancedProperties;
            lhsAdvancedProperties = (((this.advancedProperties!= null)&&(!this.advancedProperties.isEmpty()))?this.getAdvancedProperties():null);
            List<WebAdvancedProperty> rhsAdvancedProperties;
            rhsAdvancedProperties = (((that.advancedProperties!= null)&&(!that.advancedProperties.isEmpty()))?that.getAdvancedProperties():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "advancedProperties", lhsAdvancedProperties), LocatorUtils.property(thatLocator, "advancedProperties", rhsAdvancedProperties), lhsAdvancedProperties, rhsAdvancedProperties)) {
                return false;
            }
        }
        {
            String lhsId;
            lhsId = this.getId();
            String rhsId;
            rhsId = that.getId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "id", lhsId), LocatorUtils.property(thatLocator, "id", rhsId), lhsId, rhsId)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            BigInteger lhsColumn;
            lhsColumn = this.getColumn();
            BigInteger rhsColumn;
            rhsColumn = that.getColumn();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "column", lhsColumn), LocatorUtils.property(thatLocator, "column", rhsColumn), lhsColumn, rhsColumn)) {
                return false;
            }
        }
        {
            BigInteger lhsRow;
            lhsRow = this.getRow();
            BigInteger rhsRow;
            rhsRow = that.getRow();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "row", lhsRow), LocatorUtils.property(thatLocator, "row", rhsRow), lhsRow, rhsRow)) {
                return false;
            }
        }
        {
            BigInteger lhsColspan;
            lhsColspan = this.getColspan();
            BigInteger rhsColspan;
            rhsColspan = that.getColspan();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "colspan", lhsColspan), LocatorUtils.property(thatLocator, "colspan", rhsColspan), lhsColspan, rhsColspan)) {
                return false;
            }
        }
        {
            BigInteger lhsRowspan;
            lhsRowspan = this.getRowspan();
            BigInteger rhsRowspan;
            rhsRowspan = that.getRowspan();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "rowspan", lhsRowspan), LocatorUtils.property(thatLocator, "rowspan", rhsRowspan), lhsRowspan, rhsRowspan)) {
                return false;
            }
        }
        {
            String lhsFontSize;
            lhsFontSize = this.getFontSize();
            String rhsFontSize;
            rhsFontSize = that.getFontSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fontSize", lhsFontSize), LocatorUtils.property(thatLocator, "fontSize", rhsFontSize), lhsFontSize, rhsFontSize)) {
                return false;
            }
        }
        {
            String lhsTextColor;
            lhsTextColor = this.getTextColor();
            String rhsTextColor;
            rhsTextColor = that.getTextColor();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "textColor", lhsTextColor), LocatorUtils.property(thatLocator, "textColor", rhsTextColor), lhsTextColor, rhsTextColor)) {
                return false;
            }
        }
        {
            Boolean lhsBold;
            lhsBold = this.isBold();
            Boolean rhsBold;
            rhsBold = that.isBold();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "bold", lhsBold), LocatorUtils.property(thatLocator, "bold", rhsBold), lhsBold, rhsBold)) {
                return false;
            }
        }
        {
            Boolean lhsItalic;
            lhsItalic = this.isItalic();
            Boolean rhsItalic;
            rhsItalic = that.isItalic();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "italic", lhsItalic), LocatorUtils.property(thatLocator, "italic", rhsItalic), lhsItalic, rhsItalic)) {
                return false;
            }
        }
        {
            Boolean lhsUnderline;
            lhsUnderline = this.isUnderline();
            Boolean rhsUnderline;
            rhsUnderline = that.isUnderline();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "underline", lhsUnderline), LocatorUtils.property(thatLocator, "underline", rhsUnderline), lhsUnderline, rhsUnderline)) {
                return false;
            }
        }
        {
            String lhsNextFocusComponent;
            lhsNextFocusComponent = this.getNextFocusComponent();
            String rhsNextFocusComponent;
            rhsNextFocusComponent = that.getNextFocusComponent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "nextFocusComponent", lhsNextFocusComponent), LocatorUtils.property(thatLocator, "nextFocusComponent", rhsNextFocusComponent), lhsNextFocusComponent, rhsNextFocusComponent)) {
                return false;
            }
        }
        {
            String lhsNextFocusField;
            lhsNextFocusField = this.getNextFocusField();
            String rhsNextFocusField;
            rhsNextFocusField = that.getNextFocusField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "nextFocusField", lhsNextFocusField), LocatorUtils.property(thatLocator, "nextFocusField", rhsNextFocusField), lhsNextFocusField, rhsNextFocusField)) {
                return false;
            }
        }
        {
            String lhsAlternativeTooltip;
            lhsAlternativeTooltip = this.getAlternativeTooltip();
            String rhsAlternativeTooltip;
            rhsAlternativeTooltip = that.getAlternativeTooltip();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "alternativeTooltip", lhsAlternativeTooltip), LocatorUtils.property(thatLocator, "alternativeTooltip", rhsAlternativeTooltip), lhsAlternativeTooltip, rhsAlternativeTooltip)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<WebAdvancedProperty> theAdvancedProperties;
            theAdvancedProperties = (((this.advancedProperties!= null)&&(!this.advancedProperties.isEmpty()))?this.getAdvancedProperties():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "advancedProperties", theAdvancedProperties), currentHashCode, theAdvancedProperties);
        }
        {
            String theId;
            theId = this.getId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "id", theId), currentHashCode, theId);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            BigInteger theColumn;
            theColumn = this.getColumn();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "column", theColumn), currentHashCode, theColumn);
        }
        {
            BigInteger theRow;
            theRow = this.getRow();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "row", theRow), currentHashCode, theRow);
        }
        {
            BigInteger theColspan;
            theColspan = this.getColspan();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "colspan", theColspan), currentHashCode, theColspan);
        }
        {
            BigInteger theRowspan;
            theRowspan = this.getRowspan();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "rowspan", theRowspan), currentHashCode, theRowspan);
        }
        {
            String theFontSize;
            theFontSize = this.getFontSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fontSize", theFontSize), currentHashCode, theFontSize);
        }
        {
            String theTextColor;
            theTextColor = this.getTextColor();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "textColor", theTextColor), currentHashCode, theTextColor);
        }
        {
            Boolean theBold;
            theBold = this.isBold();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "bold", theBold), currentHashCode, theBold);
        }
        {
            Boolean theItalic;
            theItalic = this.isItalic();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "italic", theItalic), currentHashCode, theItalic);
        }
        {
            Boolean theUnderline;
            theUnderline = this.isUnderline();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "underline", theUnderline), currentHashCode, theUnderline);
        }
        {
            String theNextFocusComponent;
            theNextFocusComponent = this.getNextFocusComponent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nextFocusComponent", theNextFocusComponent), currentHashCode, theNextFocusComponent);
        }
        {
            String theNextFocusField;
            theNextFocusField = this.getNextFocusField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nextFocusField", theNextFocusField), currentHashCode, theNextFocusField);
        }
        {
            String theAlternativeTooltip;
            theAlternativeTooltip = this.getAlternativeTooltip();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "alternativeTooltip", theAlternativeTooltip), currentHashCode, theAlternativeTooltip);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        if (null == target) {
            throw new IllegalArgumentException("Target argument must not be null for abstract copyable classes.");
        }
        if (target instanceof WebComponent) {
            final WebComponent copy = ((WebComponent) target);
            if ((this.advancedProperties!= null)&&(!this.advancedProperties.isEmpty())) {
                List<WebAdvancedProperty> sourceAdvancedProperties;
                sourceAdvancedProperties = (((this.advancedProperties!= null)&&(!this.advancedProperties.isEmpty()))?this.getAdvancedProperties():null);
                @SuppressWarnings("unchecked")
                List<WebAdvancedProperty> copyAdvancedProperties = ((List<WebAdvancedProperty> ) strategy.copy(LocatorUtils.property(locator, "advancedProperties", sourceAdvancedProperties), sourceAdvancedProperties));
                copy.advancedProperties = null;
                if (copyAdvancedProperties!= null) {
                    List<WebAdvancedProperty> uniqueAdvancedPropertiesl = copy.getAdvancedProperties();
                    uniqueAdvancedPropertiesl.addAll(copyAdvancedProperties);
                }
            } else {
                copy.advancedProperties = null;
            }
            if (this.id!= null) {
                String sourceId;
                sourceId = this.getId();
                String copyId = ((String) strategy.copy(LocatorUtils.property(locator, "id", sourceId), sourceId));
                copy.setId(copyId);
            } else {
                copy.id = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.column!= null) {
                BigInteger sourceColumn;
                sourceColumn = this.getColumn();
                BigInteger copyColumn = ((BigInteger) strategy.copy(LocatorUtils.property(locator, "column", sourceColumn), sourceColumn));
                copy.setColumn(copyColumn);
            } else {
                copy.column = null;
            }
            if (this.row!= null) {
                BigInteger sourceRow;
                sourceRow = this.getRow();
                BigInteger copyRow = ((BigInteger) strategy.copy(LocatorUtils.property(locator, "row", sourceRow), sourceRow));
                copy.setRow(copyRow);
            } else {
                copy.row = null;
            }
            if (this.colspan!= null) {
                BigInteger sourceColspan;
                sourceColspan = this.getColspan();
                BigInteger copyColspan = ((BigInteger) strategy.copy(LocatorUtils.property(locator, "colspan", sourceColspan), sourceColspan));
                copy.setColspan(copyColspan);
            } else {
                copy.colspan = null;
            }
            if (this.rowspan!= null) {
                BigInteger sourceRowspan;
                sourceRowspan = this.getRowspan();
                BigInteger copyRowspan = ((BigInteger) strategy.copy(LocatorUtils.property(locator, "rowspan", sourceRowspan), sourceRowspan));
                copy.setRowspan(copyRowspan);
            } else {
                copy.rowspan = null;
            }
            if (this.fontSize!= null) {
                String sourceFontSize;
                sourceFontSize = this.getFontSize();
                String copyFontSize = ((String) strategy.copy(LocatorUtils.property(locator, "fontSize", sourceFontSize), sourceFontSize));
                copy.setFontSize(copyFontSize);
            } else {
                copy.fontSize = null;
            }
            if (this.textColor!= null) {
                String sourceTextColor;
                sourceTextColor = this.getTextColor();
                String copyTextColor = ((String) strategy.copy(LocatorUtils.property(locator, "textColor", sourceTextColor), sourceTextColor));
                copy.setTextColor(copyTextColor);
            } else {
                copy.textColor = null;
            }
            if (this.bold!= null) {
                Boolean sourceBold;
                sourceBold = this.isBold();
                Boolean copyBold = ((Boolean) strategy.copy(LocatorUtils.property(locator, "bold", sourceBold), sourceBold));
                copy.setBold(copyBold);
            } else {
                copy.bold = null;
            }
            if (this.italic!= null) {
                Boolean sourceItalic;
                sourceItalic = this.isItalic();
                Boolean copyItalic = ((Boolean) strategy.copy(LocatorUtils.property(locator, "italic", sourceItalic), sourceItalic));
                copy.setItalic(copyItalic);
            } else {
                copy.italic = null;
            }
            if (this.underline!= null) {
                Boolean sourceUnderline;
                sourceUnderline = this.isUnderline();
                Boolean copyUnderline = ((Boolean) strategy.copy(LocatorUtils.property(locator, "underline", sourceUnderline), sourceUnderline));
                copy.setUnderline(copyUnderline);
            } else {
                copy.underline = null;
            }
            if (this.nextFocusComponent!= null) {
                String sourceNextFocusComponent;
                sourceNextFocusComponent = this.getNextFocusComponent();
                String copyNextFocusComponent = ((String) strategy.copy(LocatorUtils.property(locator, "nextFocusComponent", sourceNextFocusComponent), sourceNextFocusComponent));
                copy.setNextFocusComponent(copyNextFocusComponent);
            } else {
                copy.nextFocusComponent = null;
            }
            if (this.nextFocusField!= null) {
                String sourceNextFocusField;
                sourceNextFocusField = this.getNextFocusField();
                String copyNextFocusField = ((String) strategy.copy(LocatorUtils.property(locator, "nextFocusField", sourceNextFocusField), sourceNextFocusField));
                copy.setNextFocusField(copyNextFocusField);
            } else {
                copy.nextFocusField = null;
            }
            if (this.alternativeTooltip!= null) {
                String sourceAlternativeTooltip;
                sourceAlternativeTooltip = this.getAlternativeTooltip();
                String copyAlternativeTooltip = ((String) strategy.copy(LocatorUtils.property(locator, "alternativeTooltip", sourceAlternativeTooltip), sourceAlternativeTooltip));
                copy.setAlternativeTooltip(copyAlternativeTooltip);
            } else {
                copy.alternativeTooltip = null;
            }
        }
        return target;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebComponent.Builder<_B> _other) {
        if (this.advancedProperties == null) {
            _other.advancedProperties = null;
        } else {
            _other.advancedProperties = new ArrayList<WebAdvancedProperty.Builder<WebComponent.Builder<_B>>>();
            for (WebAdvancedProperty _item: this.advancedProperties) {
                _other.advancedProperties.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.id = this.id;
        _other.name = this.name;
        _other.column = this.column;
        _other.row = this.row;
        _other.colspan = this.colspan;
        _other.rowspan = this.rowspan;
        _other.fontSize = this.fontSize;
        _other.textColor = this.textColor;
        _other.bold = this.bold;
        _other.italic = this.italic;
        _other.underline = this.underline;
        _other.nextFocusComponent = this.nextFocusComponent;
        _other.nextFocusField = this.nextFocusField;
        _other.alternativeTooltip = this.alternativeTooltip;
    }

    public abstract<_B >WebComponent.Builder<_B> newCopyBuilder(final _B _parentBuilder);

    public abstract WebComponent.Builder<Void> newCopyBuilder();

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebComponent.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree advancedPropertiesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("advancedProperties"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(advancedPropertiesPropertyTree!= null):((advancedPropertiesPropertyTree == null)||(!advancedPropertiesPropertyTree.isLeaf())))) {
            if (this.advancedProperties == null) {
                _other.advancedProperties = null;
            } else {
                _other.advancedProperties = new ArrayList<WebAdvancedProperty.Builder<WebComponent.Builder<_B>>>();
                for (WebAdvancedProperty _item: this.advancedProperties) {
                    _other.advancedProperties.add(((_item == null)?null:_item.newCopyBuilder(_other, advancedPropertiesPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree idPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("id"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(idPropertyTree!= null):((idPropertyTree == null)||(!idPropertyTree.isLeaf())))) {
            _other.id = this.id;
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree columnPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("column"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(columnPropertyTree!= null):((columnPropertyTree == null)||(!columnPropertyTree.isLeaf())))) {
            _other.column = this.column;
        }
        final PropertyTree rowPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("row"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowPropertyTree!= null):((rowPropertyTree == null)||(!rowPropertyTree.isLeaf())))) {
            _other.row = this.row;
        }
        final PropertyTree colspanPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("colspan"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(colspanPropertyTree!= null):((colspanPropertyTree == null)||(!colspanPropertyTree.isLeaf())))) {
            _other.colspan = this.colspan;
        }
        final PropertyTree rowspanPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rowspan"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowspanPropertyTree!= null):((rowspanPropertyTree == null)||(!rowspanPropertyTree.isLeaf())))) {
            _other.rowspan = this.rowspan;
        }
        final PropertyTree fontSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fontSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fontSizePropertyTree!= null):((fontSizePropertyTree == null)||(!fontSizePropertyTree.isLeaf())))) {
            _other.fontSize = this.fontSize;
        }
        final PropertyTree textColorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("textColor"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(textColorPropertyTree!= null):((textColorPropertyTree == null)||(!textColorPropertyTree.isLeaf())))) {
            _other.textColor = this.textColor;
        }
        final PropertyTree boldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("bold"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boldPropertyTree!= null):((boldPropertyTree == null)||(!boldPropertyTree.isLeaf())))) {
            _other.bold = this.bold;
        }
        final PropertyTree italicPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("italic"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(italicPropertyTree!= null):((italicPropertyTree == null)||(!italicPropertyTree.isLeaf())))) {
            _other.italic = this.italic;
        }
        final PropertyTree underlinePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("underline"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(underlinePropertyTree!= null):((underlinePropertyTree == null)||(!underlinePropertyTree.isLeaf())))) {
            _other.underline = this.underline;
        }
        final PropertyTree nextFocusComponentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextFocusComponent"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextFocusComponentPropertyTree!= null):((nextFocusComponentPropertyTree == null)||(!nextFocusComponentPropertyTree.isLeaf())))) {
            _other.nextFocusComponent = this.nextFocusComponent;
        }
        final PropertyTree nextFocusFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextFocusField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextFocusFieldPropertyTree!= null):((nextFocusFieldPropertyTree == null)||(!nextFocusFieldPropertyTree.isLeaf())))) {
            _other.nextFocusField = this.nextFocusField;
        }
        final PropertyTree alternativeTooltipPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("alternativeTooltip"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(alternativeTooltipPropertyTree!= null):((alternativeTooltipPropertyTree == null)||(!alternativeTooltipPropertyTree.isLeaf())))) {
            _other.alternativeTooltip = this.alternativeTooltip;
        }
    }

    public abstract<_B >WebComponent.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse);

    public abstract WebComponent.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse);

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final WebComponent _storedValue;
        private List<WebAdvancedProperty.Builder<WebComponent.Builder<_B>>> advancedProperties;
        private String id;
        private String name;
        private BigInteger column;
        private BigInteger row;
        private BigInteger colspan;
        private BigInteger rowspan;
        private String fontSize;
        private String textColor;
        private Boolean bold;
        private Boolean italic;
        private Boolean underline;
        private String nextFocusComponent;
        private String nextFocusField;
        private String alternativeTooltip;

        public Builder(final _B _parentBuilder, final WebComponent _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.advancedProperties == null) {
                        this.advancedProperties = null;
                    } else {
                        this.advancedProperties = new ArrayList<WebAdvancedProperty.Builder<WebComponent.Builder<_B>>>();
                        for (WebAdvancedProperty _item: _other.advancedProperties) {
                            this.advancedProperties.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.id = _other.id;
                    this.name = _other.name;
                    this.column = _other.column;
                    this.row = _other.row;
                    this.colspan = _other.colspan;
                    this.rowspan = _other.rowspan;
                    this.fontSize = _other.fontSize;
                    this.textColor = _other.textColor;
                    this.bold = _other.bold;
                    this.italic = _other.italic;
                    this.underline = _other.underline;
                    this.nextFocusComponent = _other.nextFocusComponent;
                    this.nextFocusField = _other.nextFocusField;
                    this.alternativeTooltip = _other.alternativeTooltip;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final WebComponent _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree advancedPropertiesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("advancedProperties"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(advancedPropertiesPropertyTree!= null):((advancedPropertiesPropertyTree == null)||(!advancedPropertiesPropertyTree.isLeaf())))) {
                        if (_other.advancedProperties == null) {
                            this.advancedProperties = null;
                        } else {
                            this.advancedProperties = new ArrayList<WebAdvancedProperty.Builder<WebComponent.Builder<_B>>>();
                            for (WebAdvancedProperty _item: _other.advancedProperties) {
                                this.advancedProperties.add(((_item == null)?null:_item.newCopyBuilder(this, advancedPropertiesPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree idPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("id"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(idPropertyTree!= null):((idPropertyTree == null)||(!idPropertyTree.isLeaf())))) {
                        this.id = _other.id;
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree columnPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("column"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(columnPropertyTree!= null):((columnPropertyTree == null)||(!columnPropertyTree.isLeaf())))) {
                        this.column = _other.column;
                    }
                    final PropertyTree rowPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("row"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowPropertyTree!= null):((rowPropertyTree == null)||(!rowPropertyTree.isLeaf())))) {
                        this.row = _other.row;
                    }
                    final PropertyTree colspanPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("colspan"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(colspanPropertyTree!= null):((colspanPropertyTree == null)||(!colspanPropertyTree.isLeaf())))) {
                        this.colspan = _other.colspan;
                    }
                    final PropertyTree rowspanPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rowspan"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowspanPropertyTree!= null):((rowspanPropertyTree == null)||(!rowspanPropertyTree.isLeaf())))) {
                        this.rowspan = _other.rowspan;
                    }
                    final PropertyTree fontSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fontSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fontSizePropertyTree!= null):((fontSizePropertyTree == null)||(!fontSizePropertyTree.isLeaf())))) {
                        this.fontSize = _other.fontSize;
                    }
                    final PropertyTree textColorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("textColor"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(textColorPropertyTree!= null):((textColorPropertyTree == null)||(!textColorPropertyTree.isLeaf())))) {
                        this.textColor = _other.textColor;
                    }
                    final PropertyTree boldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("bold"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boldPropertyTree!= null):((boldPropertyTree == null)||(!boldPropertyTree.isLeaf())))) {
                        this.bold = _other.bold;
                    }
                    final PropertyTree italicPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("italic"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(italicPropertyTree!= null):((italicPropertyTree == null)||(!italicPropertyTree.isLeaf())))) {
                        this.italic = _other.italic;
                    }
                    final PropertyTree underlinePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("underline"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(underlinePropertyTree!= null):((underlinePropertyTree == null)||(!underlinePropertyTree.isLeaf())))) {
                        this.underline = _other.underline;
                    }
                    final PropertyTree nextFocusComponentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextFocusComponent"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextFocusComponentPropertyTree!= null):((nextFocusComponentPropertyTree == null)||(!nextFocusComponentPropertyTree.isLeaf())))) {
                        this.nextFocusComponent = _other.nextFocusComponent;
                    }
                    final PropertyTree nextFocusFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextFocusField"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextFocusFieldPropertyTree!= null):((nextFocusFieldPropertyTree == null)||(!nextFocusFieldPropertyTree.isLeaf())))) {
                        this.nextFocusField = _other.nextFocusField;
                    }
                    final PropertyTree alternativeTooltipPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("alternativeTooltip"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(alternativeTooltipPropertyTree!= null):((alternativeTooltipPropertyTree == null)||(!alternativeTooltipPropertyTree.isLeaf())))) {
                        this.alternativeTooltip = _other.alternativeTooltip;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends WebComponent >_P init(final _P _product) {
            if (this.advancedProperties!= null) {
                final List<WebAdvancedProperty> advancedProperties = new ArrayList<WebAdvancedProperty>(this.advancedProperties.size());
                for (WebAdvancedProperty.Builder<WebComponent.Builder<_B>> _item: this.advancedProperties) {
                    advancedProperties.add(_item.build());
                }
                _product.advancedProperties = advancedProperties;
            }
            _product.id = this.id;
            _product.name = this.name;
            _product.column = this.column;
            _product.row = this.row;
            _product.colspan = this.colspan;
            _product.rowspan = this.rowspan;
            _product.fontSize = this.fontSize;
            _product.textColor = this.textColor;
            _product.bold = this.bold;
            _product.italic = this.italic;
            _product.underline = this.underline;
            _product.nextFocusComponent = this.nextFocusComponent;
            _product.nextFocusField = this.nextFocusField;
            _product.alternativeTooltip = this.alternativeTooltip;
            return _product;
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        public WebComponent.Builder<_B> addAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            if (advancedProperties!= null) {
                if (this.advancedProperties == null) {
                    this.advancedProperties = new ArrayList<WebAdvancedProperty.Builder<WebComponent.Builder<_B>>>();
                }
                for (WebAdvancedProperty _item: advancedProperties) {
                    this.advancedProperties.add(new WebAdvancedProperty.Builder<WebComponent.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        public WebComponent.Builder<_B> withAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            if (this.advancedProperties!= null) {
                this.advancedProperties.clear();
            }
            return addAdvancedProperties(advancedProperties);
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        public WebComponent.Builder<_B> addAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            addAdvancedProperties(Arrays.asList(advancedProperties));
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        public WebComponent.Builder<_B> withAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            withAdvancedProperties(Arrays.asList(advancedProperties));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "AdvancedProperties" property.
         * Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "AdvancedProperties" property.
         *     Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         */
        public WebAdvancedProperty.Builder<? extends WebComponent.Builder<_B>> addAdvancedProperties() {
            if (this.advancedProperties == null) {
                this.advancedProperties = new ArrayList<WebAdvancedProperty.Builder<WebComponent.Builder<_B>>>();
            }
            final WebAdvancedProperty.Builder<WebComponent.Builder<_B>> advancedProperties_Builder = new WebAdvancedProperty.Builder<WebComponent.Builder<_B>>(this, null, false);
            this.advancedProperties.add(advancedProperties_Builder);
            return advancedProperties_Builder;
        }

        /**
         * Sets the new value of "id" (any previous value will be replaced)
         * 
         * @param id
         *     New value of the "id" property.
         */
        public WebComponent.Builder<_B> withId(final String id) {
            this.id = id;
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public WebComponent.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "column" (any previous value will be replaced)
         * 
         * @param column
         *     New value of the "column" property.
         */
        public WebComponent.Builder<_B> withColumn(final BigInteger column) {
            this.column = column;
            return this;
        }

        /**
         * Sets the new value of "row" (any previous value will be replaced)
         * 
         * @param row
         *     New value of the "row" property.
         */
        public WebComponent.Builder<_B> withRow(final BigInteger row) {
            this.row = row;
            return this;
        }

        /**
         * Sets the new value of "colspan" (any previous value will be replaced)
         * 
         * @param colspan
         *     New value of the "colspan" property.
         */
        public WebComponent.Builder<_B> withColspan(final BigInteger colspan) {
            this.colspan = colspan;
            return this;
        }

        /**
         * Sets the new value of "rowspan" (any previous value will be replaced)
         * 
         * @param rowspan
         *     New value of the "rowspan" property.
         */
        public WebComponent.Builder<_B> withRowspan(final BigInteger rowspan) {
            this.rowspan = rowspan;
            return this;
        }

        /**
         * Sets the new value of "fontSize" (any previous value will be replaced)
         * 
         * @param fontSize
         *     New value of the "fontSize" property.
         */
        public WebComponent.Builder<_B> withFontSize(final String fontSize) {
            this.fontSize = fontSize;
            return this;
        }

        /**
         * Sets the new value of "textColor" (any previous value will be replaced)
         * 
         * @param textColor
         *     New value of the "textColor" property.
         */
        public WebComponent.Builder<_B> withTextColor(final String textColor) {
            this.textColor = textColor;
            return this;
        }

        /**
         * Sets the new value of "bold" (any previous value will be replaced)
         * 
         * @param bold
         *     New value of the "bold" property.
         */
        public WebComponent.Builder<_B> withBold(final Boolean bold) {
            this.bold = bold;
            return this;
        }

        /**
         * Sets the new value of "italic" (any previous value will be replaced)
         * 
         * @param italic
         *     New value of the "italic" property.
         */
        public WebComponent.Builder<_B> withItalic(final Boolean italic) {
            this.italic = italic;
            return this;
        }

        /**
         * Sets the new value of "underline" (any previous value will be replaced)
         * 
         * @param underline
         *     New value of the "underline" property.
         */
        public WebComponent.Builder<_B> withUnderline(final Boolean underline) {
            this.underline = underline;
            return this;
        }

        /**
         * Sets the new value of "nextFocusComponent" (any previous value will be replaced)
         * 
         * @param nextFocusComponent
         *     New value of the "nextFocusComponent" property.
         */
        public WebComponent.Builder<_B> withNextFocusComponent(final String nextFocusComponent) {
            this.nextFocusComponent = nextFocusComponent;
            return this;
        }

        /**
         * Sets the new value of "nextFocusField" (any previous value will be replaced)
         * 
         * @param nextFocusField
         *     New value of the "nextFocusField" property.
         */
        public WebComponent.Builder<_B> withNextFocusField(final String nextFocusField) {
            this.nextFocusField = nextFocusField;
            return this;
        }

        /**
         * Sets the new value of "alternativeTooltip" (any previous value will be replaced)
         * 
         * @param alternativeTooltip
         *     New value of the "alternativeTooltip" property.
         */
        public WebComponent.Builder<_B> withAlternativeTooltip(final String alternativeTooltip) {
            this.alternativeTooltip = alternativeTooltip;
            return this;
        }

        @Override
        public WebComponent build() {
            return ((WebComponent) _storedValue);
        }

        public WebComponent.Builder<_B> copyOf(final WebComponent _other) {
            _other.copyTo(this);
            return this;
        }

        public WebComponent.Builder<_B> copyOf(final WebComponent.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends WebComponent.Selector<WebComponent.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static WebComponent.Select _root() {
            return new WebComponent.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private WebAdvancedProperty.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> advancedProperties = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> id = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> column = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> row = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> colspan = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> rowspan = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> fontSize = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> textColor = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> bold = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> italic = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> underline = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> nextFocusComponent = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> nextFocusField = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> alternativeTooltip = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.advancedProperties!= null) {
                products.put("advancedProperties", this.advancedProperties.init());
            }
            if (this.id!= null) {
                products.put("id", this.id.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.column!= null) {
                products.put("column", this.column.init());
            }
            if (this.row!= null) {
                products.put("row", this.row.init());
            }
            if (this.colspan!= null) {
                products.put("colspan", this.colspan.init());
            }
            if (this.rowspan!= null) {
                products.put("rowspan", this.rowspan.init());
            }
            if (this.fontSize!= null) {
                products.put("fontSize", this.fontSize.init());
            }
            if (this.textColor!= null) {
                products.put("textColor", this.textColor.init());
            }
            if (this.bold!= null) {
                products.put("bold", this.bold.init());
            }
            if (this.italic!= null) {
                products.put("italic", this.italic.init());
            }
            if (this.underline!= null) {
                products.put("underline", this.underline.init());
            }
            if (this.nextFocusComponent!= null) {
                products.put("nextFocusComponent", this.nextFocusComponent.init());
            }
            if (this.nextFocusField!= null) {
                products.put("nextFocusField", this.nextFocusField.init());
            }
            if (this.alternativeTooltip!= null) {
                products.put("alternativeTooltip", this.alternativeTooltip.init());
            }
            return products;
        }

        public WebAdvancedProperty.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> advancedProperties() {
            return ((this.advancedProperties == null)?this.advancedProperties = new WebAdvancedProperty.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "advancedProperties"):this.advancedProperties);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> id() {
            return ((this.id == null)?this.id = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "id"):this.id);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> column() {
            return ((this.column == null)?this.column = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "column"):this.column);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> row() {
            return ((this.row == null)?this.row = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "row"):this.row);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> colspan() {
            return ((this.colspan == null)?this.colspan = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "colspan"):this.colspan);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> rowspan() {
            return ((this.rowspan == null)?this.rowspan = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "rowspan"):this.rowspan);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> fontSize() {
            return ((this.fontSize == null)?this.fontSize = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "fontSize"):this.fontSize);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> textColor() {
            return ((this.textColor == null)?this.textColor = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "textColor"):this.textColor);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> bold() {
            return ((this.bold == null)?this.bold = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "bold"):this.bold);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> italic() {
            return ((this.italic == null)?this.italic = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "italic"):this.italic);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> underline() {
            return ((this.underline == null)?this.underline = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "underline"):this.underline);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> nextFocusComponent() {
            return ((this.nextFocusComponent == null)?this.nextFocusComponent = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "nextFocusComponent"):this.nextFocusComponent);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> nextFocusField() {
            return ((this.nextFocusField == null)?this.nextFocusField = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "nextFocusField"):this.nextFocusField);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> alternativeTooltip() {
            return ((this.alternativeTooltip == null)?this.alternativeTooltip = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "alternativeTooltip"):this.alternativeTooltip);
        }

    }

}
