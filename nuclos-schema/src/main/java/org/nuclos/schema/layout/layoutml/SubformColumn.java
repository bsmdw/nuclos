
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}translations" minOccurs="0"/&gt;
 *         &lt;element ref="{}valuelist-provider" minOccurs="0"/&gt;
 *         &lt;element ref="{}text-module" minOccurs="0"/&gt;
 *         &lt;element ref="{}property" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="label" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="controltype" type="{}controltype" /&gt;
 *       &lt;attribute name="controltypeclass" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="visible" type="{}boolean" /&gt;
 *       &lt;attribute name="enabled" type="{}boolean" /&gt;
 *       &lt;attribute name="notcloneable" type="{}boolean" /&gt;
 *       &lt;attribute name="insertable" type="{}boolean" /&gt;
 *       &lt;attribute name="lovsearch" type="{}boolean" /&gt;
 *       &lt;attribute name="rows" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="columns" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="resourceId" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="width" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="nextfocuscomponent" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="nextfocusfield" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="customusagesearch" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "translations",
    "valuelistProvider",
    "textModule",
    "property"
})
@XmlRootElement(name = "subform-column")
public class SubformColumn implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected Translations translations;
    @XmlElement(name = "valuelist-provider")
    protected ValuelistProvider valuelistProvider;
    @XmlElement(name = "text-module")
    protected TextModule textModule;
    protected List<Property> property;
    @XmlAttribute(name = "name", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String name;
    @XmlAttribute(name = "label")
    @XmlSchemaType(name = "anySimpleType")
    protected String label;
    @XmlAttribute(name = "controltype")
    protected Controltype controltype;
    @XmlAttribute(name = "controltypeclass")
    @XmlSchemaType(name = "anySimpleType")
    protected String controltypeclass;
    @XmlAttribute(name = "visible")
    protected Boolean visible;
    @XmlAttribute(name = "enabled")
    protected Boolean enabled;
    @XmlAttribute(name = "notcloneable")
    protected Boolean notcloneable;
    @XmlAttribute(name = "insertable")
    protected Boolean insertable;
    @XmlAttribute(name = "lovsearch")
    protected Boolean lovsearch;
    @XmlAttribute(name = "rows")
    @XmlSchemaType(name = "anySimpleType")
    protected String rows;
    @XmlAttribute(name = "columns")
    @XmlSchemaType(name = "anySimpleType")
    protected String columns;
    @XmlAttribute(name = "resourceId")
    @XmlSchemaType(name = "anySimpleType")
    protected String resourceId;
    @XmlAttribute(name = "width")
    @XmlSchemaType(name = "anySimpleType")
    protected String width;
    @XmlAttribute(name = "nextfocuscomponent")
    @XmlSchemaType(name = "anySimpleType")
    protected String nextfocuscomponent;
    @XmlAttribute(name = "nextfocusfield")
    @XmlSchemaType(name = "anySimpleType")
    protected String nextfocusfield;
    @XmlAttribute(name = "customusagesearch")
    @XmlSchemaType(name = "anySimpleType")
    protected String customusagesearch;

    /**
     * Gets the value of the translations property.
     * 
     * @return
     *     possible object is
     *     {@link Translations }
     *     
     */
    public Translations getTranslations() {
        return translations;
    }

    /**
     * Sets the value of the translations property.
     * 
     * @param value
     *     allowed object is
     *     {@link Translations }
     *     
     */
    public void setTranslations(Translations value) {
        this.translations = value;
    }

    /**
     * Gets the value of the valuelistProvider property.
     * 
     * @return
     *     possible object is
     *     {@link ValuelistProvider }
     *     
     */
    public ValuelistProvider getValuelistProvider() {
        return valuelistProvider;
    }

    /**
     * Sets the value of the valuelistProvider property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValuelistProvider }
     *     
     */
    public void setValuelistProvider(ValuelistProvider value) {
        this.valuelistProvider = value;
    }

    /**
     * Gets the value of the textModule property.
     * 
     * @return
     *     possible object is
     *     {@link TextModule }
     *     
     */
    public TextModule getTextModule() {
        return textModule;
    }

    /**
     * Sets the value of the textModule property.
     * 
     * @param value
     *     allowed object is
     *     {@link TextModule }
     *     
     */
    public void setTextModule(TextModule value) {
        this.textModule = value;
    }

    /**
     * Gets the value of the property property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the property property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProperty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Property }
     * 
     * 
     */
    public List<Property> getProperty() {
        if (property == null) {
            property = new ArrayList<Property>();
        }
        return this.property;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the controltype property.
     * 
     * @return
     *     possible object is
     *     {@link Controltype }
     *     
     */
    public Controltype getControltype() {
        return controltype;
    }

    /**
     * Sets the value of the controltype property.
     * 
     * @param value
     *     allowed object is
     *     {@link Controltype }
     *     
     */
    public void setControltype(Controltype value) {
        this.controltype = value;
    }

    /**
     * Gets the value of the controltypeclass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControltypeclass() {
        return controltypeclass;
    }

    /**
     * Sets the value of the controltypeclass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControltypeclass(String value) {
        this.controltypeclass = value;
    }

    /**
     * Gets the value of the visible property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getVisible() {
        return visible;
    }

    /**
     * Sets the value of the visible property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVisible(Boolean value) {
        this.visible = value;
    }

    /**
     * Gets the value of the enabled property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getEnabled() {
        return enabled;
    }

    /**
     * Sets the value of the enabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnabled(Boolean value) {
        this.enabled = value;
    }

    /**
     * Gets the value of the notcloneable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getNotcloneable() {
        return notcloneable;
    }

    /**
     * Sets the value of the notcloneable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNotcloneable(Boolean value) {
        this.notcloneable = value;
    }

    /**
     * Gets the value of the insertable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getInsertable() {
        return insertable;
    }

    /**
     * Sets the value of the insertable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInsertable(Boolean value) {
        this.insertable = value;
    }

    /**
     * Gets the value of the lovsearch property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getLovsearch() {
        return lovsearch;
    }

    /**
     * Sets the value of the lovsearch property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLovsearch(Boolean value) {
        this.lovsearch = value;
    }

    /**
     * Gets the value of the rows property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRows() {
        return rows;
    }

    /**
     * Sets the value of the rows property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRows(String value) {
        this.rows = value;
    }

    /**
     * Gets the value of the columns property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColumns() {
        return columns;
    }

    /**
     * Sets the value of the columns property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColumns(String value) {
        this.columns = value;
    }

    /**
     * Gets the value of the resourceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceId() {
        return resourceId;
    }

    /**
     * Sets the value of the resourceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceId(String value) {
        this.resourceId = value;
    }

    /**
     * Gets the value of the width property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWidth() {
        return width;
    }

    /**
     * Sets the value of the width property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWidth(String value) {
        this.width = value;
    }

    /**
     * Gets the value of the nextfocuscomponent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextfocuscomponent() {
        return nextfocuscomponent;
    }

    /**
     * Sets the value of the nextfocuscomponent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextfocuscomponent(String value) {
        this.nextfocuscomponent = value;
    }

    /**
     * Gets the value of the nextfocusfield property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextfocusfield() {
        return nextfocusfield;
    }

    /**
     * Sets the value of the nextfocusfield property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextfocusfield(String value) {
        this.nextfocusfield = value;
    }

    /**
     * Gets the value of the customusagesearch property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomusagesearch() {
        return customusagesearch;
    }

    /**
     * Sets the value of the customusagesearch property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomusagesearch(String value) {
        this.customusagesearch = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            Translations theTranslations;
            theTranslations = this.getTranslations();
            strategy.appendField(locator, this, "translations", buffer, theTranslations);
        }
        {
            ValuelistProvider theValuelistProvider;
            theValuelistProvider = this.getValuelistProvider();
            strategy.appendField(locator, this, "valuelistProvider", buffer, theValuelistProvider);
        }
        {
            TextModule theTextModule;
            theTextModule = this.getTextModule();
            strategy.appendField(locator, this, "textModule", buffer, theTextModule);
        }
        {
            List<Property> theProperty;
            theProperty = (((this.property!= null)&&(!this.property.isEmpty()))?this.getProperty():null);
            strategy.appendField(locator, this, "property", buffer, theProperty);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String theLabel;
            theLabel = this.getLabel();
            strategy.appendField(locator, this, "label", buffer, theLabel);
        }
        {
            Controltype theControltype;
            theControltype = this.getControltype();
            strategy.appendField(locator, this, "controltype", buffer, theControltype);
        }
        {
            String theControltypeclass;
            theControltypeclass = this.getControltypeclass();
            strategy.appendField(locator, this, "controltypeclass", buffer, theControltypeclass);
        }
        {
            Boolean theVisible;
            theVisible = this.getVisible();
            strategy.appendField(locator, this, "visible", buffer, theVisible);
        }
        {
            Boolean theEnabled;
            theEnabled = this.getEnabled();
            strategy.appendField(locator, this, "enabled", buffer, theEnabled);
        }
        {
            Boolean theNotcloneable;
            theNotcloneable = this.getNotcloneable();
            strategy.appendField(locator, this, "notcloneable", buffer, theNotcloneable);
        }
        {
            Boolean theInsertable;
            theInsertable = this.getInsertable();
            strategy.appendField(locator, this, "insertable", buffer, theInsertable);
        }
        {
            Boolean theLovsearch;
            theLovsearch = this.getLovsearch();
            strategy.appendField(locator, this, "lovsearch", buffer, theLovsearch);
        }
        {
            String theRows;
            theRows = this.getRows();
            strategy.appendField(locator, this, "rows", buffer, theRows);
        }
        {
            String theColumns;
            theColumns = this.getColumns();
            strategy.appendField(locator, this, "columns", buffer, theColumns);
        }
        {
            String theResourceId;
            theResourceId = this.getResourceId();
            strategy.appendField(locator, this, "resourceId", buffer, theResourceId);
        }
        {
            String theWidth;
            theWidth = this.getWidth();
            strategy.appendField(locator, this, "width", buffer, theWidth);
        }
        {
            String theNextfocuscomponent;
            theNextfocuscomponent = this.getNextfocuscomponent();
            strategy.appendField(locator, this, "nextfocuscomponent", buffer, theNextfocuscomponent);
        }
        {
            String theNextfocusfield;
            theNextfocusfield = this.getNextfocusfield();
            strategy.appendField(locator, this, "nextfocusfield", buffer, theNextfocusfield);
        }
        {
            String theCustomusagesearch;
            theCustomusagesearch = this.getCustomusagesearch();
            strategy.appendField(locator, this, "customusagesearch", buffer, theCustomusagesearch);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof SubformColumn)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final SubformColumn that = ((SubformColumn) object);
        {
            Translations lhsTranslations;
            lhsTranslations = this.getTranslations();
            Translations rhsTranslations;
            rhsTranslations = that.getTranslations();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "translations", lhsTranslations), LocatorUtils.property(thatLocator, "translations", rhsTranslations), lhsTranslations, rhsTranslations)) {
                return false;
            }
        }
        {
            ValuelistProvider lhsValuelistProvider;
            lhsValuelistProvider = this.getValuelistProvider();
            ValuelistProvider rhsValuelistProvider;
            rhsValuelistProvider = that.getValuelistProvider();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "valuelistProvider", lhsValuelistProvider), LocatorUtils.property(thatLocator, "valuelistProvider", rhsValuelistProvider), lhsValuelistProvider, rhsValuelistProvider)) {
                return false;
            }
        }
        {
            TextModule lhsTextModule;
            lhsTextModule = this.getTextModule();
            TextModule rhsTextModule;
            rhsTextModule = that.getTextModule();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "textModule", lhsTextModule), LocatorUtils.property(thatLocator, "textModule", rhsTextModule), lhsTextModule, rhsTextModule)) {
                return false;
            }
        }
        {
            List<Property> lhsProperty;
            lhsProperty = (((this.property!= null)&&(!this.property.isEmpty()))?this.getProperty():null);
            List<Property> rhsProperty;
            rhsProperty = (((that.property!= null)&&(!that.property.isEmpty()))?that.getProperty():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "property", lhsProperty), LocatorUtils.property(thatLocator, "property", rhsProperty), lhsProperty, rhsProperty)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsLabel;
            lhsLabel = this.getLabel();
            String rhsLabel;
            rhsLabel = that.getLabel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "label", lhsLabel), LocatorUtils.property(thatLocator, "label", rhsLabel), lhsLabel, rhsLabel)) {
                return false;
            }
        }
        {
            Controltype lhsControltype;
            lhsControltype = this.getControltype();
            Controltype rhsControltype;
            rhsControltype = that.getControltype();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "controltype", lhsControltype), LocatorUtils.property(thatLocator, "controltype", rhsControltype), lhsControltype, rhsControltype)) {
                return false;
            }
        }
        {
            String lhsControltypeclass;
            lhsControltypeclass = this.getControltypeclass();
            String rhsControltypeclass;
            rhsControltypeclass = that.getControltypeclass();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "controltypeclass", lhsControltypeclass), LocatorUtils.property(thatLocator, "controltypeclass", rhsControltypeclass), lhsControltypeclass, rhsControltypeclass)) {
                return false;
            }
        }
        {
            Boolean lhsVisible;
            lhsVisible = this.getVisible();
            Boolean rhsVisible;
            rhsVisible = that.getVisible();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "visible", lhsVisible), LocatorUtils.property(thatLocator, "visible", rhsVisible), lhsVisible, rhsVisible)) {
                return false;
            }
        }
        {
            Boolean lhsEnabled;
            lhsEnabled = this.getEnabled();
            Boolean rhsEnabled;
            rhsEnabled = that.getEnabled();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "enabled", lhsEnabled), LocatorUtils.property(thatLocator, "enabled", rhsEnabled), lhsEnabled, rhsEnabled)) {
                return false;
            }
        }
        {
            Boolean lhsNotcloneable;
            lhsNotcloneable = this.getNotcloneable();
            Boolean rhsNotcloneable;
            rhsNotcloneable = that.getNotcloneable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "notcloneable", lhsNotcloneable), LocatorUtils.property(thatLocator, "notcloneable", rhsNotcloneable), lhsNotcloneable, rhsNotcloneable)) {
                return false;
            }
        }
        {
            Boolean lhsInsertable;
            lhsInsertable = this.getInsertable();
            Boolean rhsInsertable;
            rhsInsertable = that.getInsertable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "insertable", lhsInsertable), LocatorUtils.property(thatLocator, "insertable", rhsInsertable), lhsInsertable, rhsInsertable)) {
                return false;
            }
        }
        {
            Boolean lhsLovsearch;
            lhsLovsearch = this.getLovsearch();
            Boolean rhsLovsearch;
            rhsLovsearch = that.getLovsearch();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "lovsearch", lhsLovsearch), LocatorUtils.property(thatLocator, "lovsearch", rhsLovsearch), lhsLovsearch, rhsLovsearch)) {
                return false;
            }
        }
        {
            String lhsRows;
            lhsRows = this.getRows();
            String rhsRows;
            rhsRows = that.getRows();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "rows", lhsRows), LocatorUtils.property(thatLocator, "rows", rhsRows), lhsRows, rhsRows)) {
                return false;
            }
        }
        {
            String lhsColumns;
            lhsColumns = this.getColumns();
            String rhsColumns;
            rhsColumns = that.getColumns();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "columns", lhsColumns), LocatorUtils.property(thatLocator, "columns", rhsColumns), lhsColumns, rhsColumns)) {
                return false;
            }
        }
        {
            String lhsResourceId;
            lhsResourceId = this.getResourceId();
            String rhsResourceId;
            rhsResourceId = that.getResourceId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "resourceId", lhsResourceId), LocatorUtils.property(thatLocator, "resourceId", rhsResourceId), lhsResourceId, rhsResourceId)) {
                return false;
            }
        }
        {
            String lhsWidth;
            lhsWidth = this.getWidth();
            String rhsWidth;
            rhsWidth = that.getWidth();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "width", lhsWidth), LocatorUtils.property(thatLocator, "width", rhsWidth), lhsWidth, rhsWidth)) {
                return false;
            }
        }
        {
            String lhsNextfocuscomponent;
            lhsNextfocuscomponent = this.getNextfocuscomponent();
            String rhsNextfocuscomponent;
            rhsNextfocuscomponent = that.getNextfocuscomponent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "nextfocuscomponent", lhsNextfocuscomponent), LocatorUtils.property(thatLocator, "nextfocuscomponent", rhsNextfocuscomponent), lhsNextfocuscomponent, rhsNextfocuscomponent)) {
                return false;
            }
        }
        {
            String lhsNextfocusfield;
            lhsNextfocusfield = this.getNextfocusfield();
            String rhsNextfocusfield;
            rhsNextfocusfield = that.getNextfocusfield();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "nextfocusfield", lhsNextfocusfield), LocatorUtils.property(thatLocator, "nextfocusfield", rhsNextfocusfield), lhsNextfocusfield, rhsNextfocusfield)) {
                return false;
            }
        }
        {
            String lhsCustomusagesearch;
            lhsCustomusagesearch = this.getCustomusagesearch();
            String rhsCustomusagesearch;
            rhsCustomusagesearch = that.getCustomusagesearch();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "customusagesearch", lhsCustomusagesearch), LocatorUtils.property(thatLocator, "customusagesearch", rhsCustomusagesearch), lhsCustomusagesearch, rhsCustomusagesearch)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            Translations theTranslations;
            theTranslations = this.getTranslations();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "translations", theTranslations), currentHashCode, theTranslations);
        }
        {
            ValuelistProvider theValuelistProvider;
            theValuelistProvider = this.getValuelistProvider();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "valuelistProvider", theValuelistProvider), currentHashCode, theValuelistProvider);
        }
        {
            TextModule theTextModule;
            theTextModule = this.getTextModule();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "textModule", theTextModule), currentHashCode, theTextModule);
        }
        {
            List<Property> theProperty;
            theProperty = (((this.property!= null)&&(!this.property.isEmpty()))?this.getProperty():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "property", theProperty), currentHashCode, theProperty);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String theLabel;
            theLabel = this.getLabel();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "label", theLabel), currentHashCode, theLabel);
        }
        {
            Controltype theControltype;
            theControltype = this.getControltype();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "controltype", theControltype), currentHashCode, theControltype);
        }
        {
            String theControltypeclass;
            theControltypeclass = this.getControltypeclass();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "controltypeclass", theControltypeclass), currentHashCode, theControltypeclass);
        }
        {
            Boolean theVisible;
            theVisible = this.getVisible();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "visible", theVisible), currentHashCode, theVisible);
        }
        {
            Boolean theEnabled;
            theEnabled = this.getEnabled();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "enabled", theEnabled), currentHashCode, theEnabled);
        }
        {
            Boolean theNotcloneable;
            theNotcloneable = this.getNotcloneable();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "notcloneable", theNotcloneable), currentHashCode, theNotcloneable);
        }
        {
            Boolean theInsertable;
            theInsertable = this.getInsertable();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "insertable", theInsertable), currentHashCode, theInsertable);
        }
        {
            Boolean theLovsearch;
            theLovsearch = this.getLovsearch();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "lovsearch", theLovsearch), currentHashCode, theLovsearch);
        }
        {
            String theRows;
            theRows = this.getRows();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "rows", theRows), currentHashCode, theRows);
        }
        {
            String theColumns;
            theColumns = this.getColumns();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "columns", theColumns), currentHashCode, theColumns);
        }
        {
            String theResourceId;
            theResourceId = this.getResourceId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "resourceId", theResourceId), currentHashCode, theResourceId);
        }
        {
            String theWidth;
            theWidth = this.getWidth();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "width", theWidth), currentHashCode, theWidth);
        }
        {
            String theNextfocuscomponent;
            theNextfocuscomponent = this.getNextfocuscomponent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nextfocuscomponent", theNextfocuscomponent), currentHashCode, theNextfocuscomponent);
        }
        {
            String theNextfocusfield;
            theNextfocusfield = this.getNextfocusfield();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nextfocusfield", theNextfocusfield), currentHashCode, theNextfocusfield);
        }
        {
            String theCustomusagesearch;
            theCustomusagesearch = this.getCustomusagesearch();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "customusagesearch", theCustomusagesearch), currentHashCode, theCustomusagesearch);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof SubformColumn) {
            final SubformColumn copy = ((SubformColumn) draftCopy);
            if (this.translations!= null) {
                Translations sourceTranslations;
                sourceTranslations = this.getTranslations();
                Translations copyTranslations = ((Translations) strategy.copy(LocatorUtils.property(locator, "translations", sourceTranslations), sourceTranslations));
                copy.setTranslations(copyTranslations);
            } else {
                copy.translations = null;
            }
            if (this.valuelistProvider!= null) {
                ValuelistProvider sourceValuelistProvider;
                sourceValuelistProvider = this.getValuelistProvider();
                ValuelistProvider copyValuelistProvider = ((ValuelistProvider) strategy.copy(LocatorUtils.property(locator, "valuelistProvider", sourceValuelistProvider), sourceValuelistProvider));
                copy.setValuelistProvider(copyValuelistProvider);
            } else {
                copy.valuelistProvider = null;
            }
            if (this.textModule!= null) {
                TextModule sourceTextModule;
                sourceTextModule = this.getTextModule();
                TextModule copyTextModule = ((TextModule) strategy.copy(LocatorUtils.property(locator, "textModule", sourceTextModule), sourceTextModule));
                copy.setTextModule(copyTextModule);
            } else {
                copy.textModule = null;
            }
            if ((this.property!= null)&&(!this.property.isEmpty())) {
                List<Property> sourceProperty;
                sourceProperty = (((this.property!= null)&&(!this.property.isEmpty()))?this.getProperty():null);
                @SuppressWarnings("unchecked")
                List<Property> copyProperty = ((List<Property> ) strategy.copy(LocatorUtils.property(locator, "property", sourceProperty), sourceProperty));
                copy.property = null;
                if (copyProperty!= null) {
                    List<Property> uniquePropertyl = copy.getProperty();
                    uniquePropertyl.addAll(copyProperty);
                }
            } else {
                copy.property = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.label!= null) {
                String sourceLabel;
                sourceLabel = this.getLabel();
                String copyLabel = ((String) strategy.copy(LocatorUtils.property(locator, "label", sourceLabel), sourceLabel));
                copy.setLabel(copyLabel);
            } else {
                copy.label = null;
            }
            if (this.controltype!= null) {
                Controltype sourceControltype;
                sourceControltype = this.getControltype();
                Controltype copyControltype = ((Controltype) strategy.copy(LocatorUtils.property(locator, "controltype", sourceControltype), sourceControltype));
                copy.setControltype(copyControltype);
            } else {
                copy.controltype = null;
            }
            if (this.controltypeclass!= null) {
                String sourceControltypeclass;
                sourceControltypeclass = this.getControltypeclass();
                String copyControltypeclass = ((String) strategy.copy(LocatorUtils.property(locator, "controltypeclass", sourceControltypeclass), sourceControltypeclass));
                copy.setControltypeclass(copyControltypeclass);
            } else {
                copy.controltypeclass = null;
            }
            if (this.visible!= null) {
                Boolean sourceVisible;
                sourceVisible = this.getVisible();
                Boolean copyVisible = ((Boolean) strategy.copy(LocatorUtils.property(locator, "visible", sourceVisible), sourceVisible));
                copy.setVisible(copyVisible);
            } else {
                copy.visible = null;
            }
            if (this.enabled!= null) {
                Boolean sourceEnabled;
                sourceEnabled = this.getEnabled();
                Boolean copyEnabled = ((Boolean) strategy.copy(LocatorUtils.property(locator, "enabled", sourceEnabled), sourceEnabled));
                copy.setEnabled(copyEnabled);
            } else {
                copy.enabled = null;
            }
            if (this.notcloneable!= null) {
                Boolean sourceNotcloneable;
                sourceNotcloneable = this.getNotcloneable();
                Boolean copyNotcloneable = ((Boolean) strategy.copy(LocatorUtils.property(locator, "notcloneable", sourceNotcloneable), sourceNotcloneable));
                copy.setNotcloneable(copyNotcloneable);
            } else {
                copy.notcloneable = null;
            }
            if (this.insertable!= null) {
                Boolean sourceInsertable;
                sourceInsertable = this.getInsertable();
                Boolean copyInsertable = ((Boolean) strategy.copy(LocatorUtils.property(locator, "insertable", sourceInsertable), sourceInsertable));
                copy.setInsertable(copyInsertable);
            } else {
                copy.insertable = null;
            }
            if (this.lovsearch!= null) {
                Boolean sourceLovsearch;
                sourceLovsearch = this.getLovsearch();
                Boolean copyLovsearch = ((Boolean) strategy.copy(LocatorUtils.property(locator, "lovsearch", sourceLovsearch), sourceLovsearch));
                copy.setLovsearch(copyLovsearch);
            } else {
                copy.lovsearch = null;
            }
            if (this.rows!= null) {
                String sourceRows;
                sourceRows = this.getRows();
                String copyRows = ((String) strategy.copy(LocatorUtils.property(locator, "rows", sourceRows), sourceRows));
                copy.setRows(copyRows);
            } else {
                copy.rows = null;
            }
            if (this.columns!= null) {
                String sourceColumns;
                sourceColumns = this.getColumns();
                String copyColumns = ((String) strategy.copy(LocatorUtils.property(locator, "columns", sourceColumns), sourceColumns));
                copy.setColumns(copyColumns);
            } else {
                copy.columns = null;
            }
            if (this.resourceId!= null) {
                String sourceResourceId;
                sourceResourceId = this.getResourceId();
                String copyResourceId = ((String) strategy.copy(LocatorUtils.property(locator, "resourceId", sourceResourceId), sourceResourceId));
                copy.setResourceId(copyResourceId);
            } else {
                copy.resourceId = null;
            }
            if (this.width!= null) {
                String sourceWidth;
                sourceWidth = this.getWidth();
                String copyWidth = ((String) strategy.copy(LocatorUtils.property(locator, "width", sourceWidth), sourceWidth));
                copy.setWidth(copyWidth);
            } else {
                copy.width = null;
            }
            if (this.nextfocuscomponent!= null) {
                String sourceNextfocuscomponent;
                sourceNextfocuscomponent = this.getNextfocuscomponent();
                String copyNextfocuscomponent = ((String) strategy.copy(LocatorUtils.property(locator, "nextfocuscomponent", sourceNextfocuscomponent), sourceNextfocuscomponent));
                copy.setNextfocuscomponent(copyNextfocuscomponent);
            } else {
                copy.nextfocuscomponent = null;
            }
            if (this.nextfocusfield!= null) {
                String sourceNextfocusfield;
                sourceNextfocusfield = this.getNextfocusfield();
                String copyNextfocusfield = ((String) strategy.copy(LocatorUtils.property(locator, "nextfocusfield", sourceNextfocusfield), sourceNextfocusfield));
                copy.setNextfocusfield(copyNextfocusfield);
            } else {
                copy.nextfocusfield = null;
            }
            if (this.customusagesearch!= null) {
                String sourceCustomusagesearch;
                sourceCustomusagesearch = this.getCustomusagesearch();
                String copyCustomusagesearch = ((String) strategy.copy(LocatorUtils.property(locator, "customusagesearch", sourceCustomusagesearch), sourceCustomusagesearch));
                copy.setCustomusagesearch(copyCustomusagesearch);
            } else {
                copy.customusagesearch = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new SubformColumn();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final SubformColumn.Builder<_B> _other) {
        _other.translations = ((this.translations == null)?null:this.translations.newCopyBuilder(_other));
        _other.valuelistProvider = ((this.valuelistProvider == null)?null:this.valuelistProvider.newCopyBuilder(_other));
        _other.textModule = ((this.textModule == null)?null:this.textModule.newCopyBuilder(_other));
        if (this.property == null) {
            _other.property = null;
        } else {
            _other.property = new ArrayList<Property.Builder<SubformColumn.Builder<_B>>>();
            for (Property _item: this.property) {
                _other.property.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.name = this.name;
        _other.label = this.label;
        _other.controltype = this.controltype;
        _other.controltypeclass = this.controltypeclass;
        _other.visible = this.visible;
        _other.enabled = this.enabled;
        _other.notcloneable = this.notcloneable;
        _other.insertable = this.insertable;
        _other.lovsearch = this.lovsearch;
        _other.rows = this.rows;
        _other.columns = this.columns;
        _other.resourceId = this.resourceId;
        _other.width = this.width;
        _other.nextfocuscomponent = this.nextfocuscomponent;
        _other.nextfocusfield = this.nextfocusfield;
        _other.customusagesearch = this.customusagesearch;
    }

    public<_B >SubformColumn.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new SubformColumn.Builder<_B>(_parentBuilder, this, true);
    }

    public SubformColumn.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static SubformColumn.Builder<Void> builder() {
        return new SubformColumn.Builder<Void>(null, null, false);
    }

    public static<_B >SubformColumn.Builder<_B> copyOf(final SubformColumn _other) {
        final SubformColumn.Builder<_B> _newBuilder = new SubformColumn.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final SubformColumn.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree translationsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("translations"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(translationsPropertyTree!= null):((translationsPropertyTree == null)||(!translationsPropertyTree.isLeaf())))) {
            _other.translations = ((this.translations == null)?null:this.translations.newCopyBuilder(_other, translationsPropertyTree, _propertyTreeUse));
        }
        final PropertyTree valuelistProviderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("valuelistProvider"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuelistProviderPropertyTree!= null):((valuelistProviderPropertyTree == null)||(!valuelistProviderPropertyTree.isLeaf())))) {
            _other.valuelistProvider = ((this.valuelistProvider == null)?null:this.valuelistProvider.newCopyBuilder(_other, valuelistProviderPropertyTree, _propertyTreeUse));
        }
        final PropertyTree textModulePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("textModule"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(textModulePropertyTree!= null):((textModulePropertyTree == null)||(!textModulePropertyTree.isLeaf())))) {
            _other.textModule = ((this.textModule == null)?null:this.textModule.newCopyBuilder(_other, textModulePropertyTree, _propertyTreeUse));
        }
        final PropertyTree propertyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("property"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(propertyPropertyTree!= null):((propertyPropertyTree == null)||(!propertyPropertyTree.isLeaf())))) {
            if (this.property == null) {
                _other.property = null;
            } else {
                _other.property = new ArrayList<Property.Builder<SubformColumn.Builder<_B>>>();
                for (Property _item: this.property) {
                    _other.property.add(((_item == null)?null:_item.newCopyBuilder(_other, propertyPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree labelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("label"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(labelPropertyTree!= null):((labelPropertyTree == null)||(!labelPropertyTree.isLeaf())))) {
            _other.label = this.label;
        }
        final PropertyTree controltypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("controltype"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(controltypePropertyTree!= null):((controltypePropertyTree == null)||(!controltypePropertyTree.isLeaf())))) {
            _other.controltype = this.controltype;
        }
        final PropertyTree controltypeclassPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("controltypeclass"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(controltypeclassPropertyTree!= null):((controltypeclassPropertyTree == null)||(!controltypeclassPropertyTree.isLeaf())))) {
            _other.controltypeclass = this.controltypeclass;
        }
        final PropertyTree visiblePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("visible"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(visiblePropertyTree!= null):((visiblePropertyTree == null)||(!visiblePropertyTree.isLeaf())))) {
            _other.visible = this.visible;
        }
        final PropertyTree enabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("enabled"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(enabledPropertyTree!= null):((enabledPropertyTree == null)||(!enabledPropertyTree.isLeaf())))) {
            _other.enabled = this.enabled;
        }
        final PropertyTree notcloneablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("notcloneable"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(notcloneablePropertyTree!= null):((notcloneablePropertyTree == null)||(!notcloneablePropertyTree.isLeaf())))) {
            _other.notcloneable = this.notcloneable;
        }
        final PropertyTree insertablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("insertable"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(insertablePropertyTree!= null):((insertablePropertyTree == null)||(!insertablePropertyTree.isLeaf())))) {
            _other.insertable = this.insertable;
        }
        final PropertyTree lovsearchPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("lovsearch"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(lovsearchPropertyTree!= null):((lovsearchPropertyTree == null)||(!lovsearchPropertyTree.isLeaf())))) {
            _other.lovsearch = this.lovsearch;
        }
        final PropertyTree rowsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rows"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowsPropertyTree!= null):((rowsPropertyTree == null)||(!rowsPropertyTree.isLeaf())))) {
            _other.rows = this.rows;
        }
        final PropertyTree columnsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("columns"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(columnsPropertyTree!= null):((columnsPropertyTree == null)||(!columnsPropertyTree.isLeaf())))) {
            _other.columns = this.columns;
        }
        final PropertyTree resourceIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resourceId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resourceIdPropertyTree!= null):((resourceIdPropertyTree == null)||(!resourceIdPropertyTree.isLeaf())))) {
            _other.resourceId = this.resourceId;
        }
        final PropertyTree widthPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("width"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(widthPropertyTree!= null):((widthPropertyTree == null)||(!widthPropertyTree.isLeaf())))) {
            _other.width = this.width;
        }
        final PropertyTree nextfocuscomponentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextfocuscomponent"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextfocuscomponentPropertyTree!= null):((nextfocuscomponentPropertyTree == null)||(!nextfocuscomponentPropertyTree.isLeaf())))) {
            _other.nextfocuscomponent = this.nextfocuscomponent;
        }
        final PropertyTree nextfocusfieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextfocusfield"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextfocusfieldPropertyTree!= null):((nextfocusfieldPropertyTree == null)||(!nextfocusfieldPropertyTree.isLeaf())))) {
            _other.nextfocusfield = this.nextfocusfield;
        }
        final PropertyTree customusagesearchPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("customusagesearch"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(customusagesearchPropertyTree!= null):((customusagesearchPropertyTree == null)||(!customusagesearchPropertyTree.isLeaf())))) {
            _other.customusagesearch = this.customusagesearch;
        }
    }

    public<_B >SubformColumn.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new SubformColumn.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public SubformColumn.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >SubformColumn.Builder<_B> copyOf(final SubformColumn _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final SubformColumn.Builder<_B> _newBuilder = new SubformColumn.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static SubformColumn.Builder<Void> copyExcept(final SubformColumn _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static SubformColumn.Builder<Void> copyOnly(final SubformColumn _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final SubformColumn _storedValue;
        private Translations.Builder<SubformColumn.Builder<_B>> translations;
        private ValuelistProvider.Builder<SubformColumn.Builder<_B>> valuelistProvider;
        private TextModule.Builder<SubformColumn.Builder<_B>> textModule;
        private List<Property.Builder<SubformColumn.Builder<_B>>> property;
        private String name;
        private String label;
        private Controltype controltype;
        private String controltypeclass;
        private Boolean visible;
        private Boolean enabled;
        private Boolean notcloneable;
        private Boolean insertable;
        private Boolean lovsearch;
        private String rows;
        private String columns;
        private String resourceId;
        private String width;
        private String nextfocuscomponent;
        private String nextfocusfield;
        private String customusagesearch;

        public Builder(final _B _parentBuilder, final SubformColumn _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.translations = ((_other.translations == null)?null:_other.translations.newCopyBuilder(this));
                    this.valuelistProvider = ((_other.valuelistProvider == null)?null:_other.valuelistProvider.newCopyBuilder(this));
                    this.textModule = ((_other.textModule == null)?null:_other.textModule.newCopyBuilder(this));
                    if (_other.property == null) {
                        this.property = null;
                    } else {
                        this.property = new ArrayList<Property.Builder<SubformColumn.Builder<_B>>>();
                        for (Property _item: _other.property) {
                            this.property.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.name = _other.name;
                    this.label = _other.label;
                    this.controltype = _other.controltype;
                    this.controltypeclass = _other.controltypeclass;
                    this.visible = _other.visible;
                    this.enabled = _other.enabled;
                    this.notcloneable = _other.notcloneable;
                    this.insertable = _other.insertable;
                    this.lovsearch = _other.lovsearch;
                    this.rows = _other.rows;
                    this.columns = _other.columns;
                    this.resourceId = _other.resourceId;
                    this.width = _other.width;
                    this.nextfocuscomponent = _other.nextfocuscomponent;
                    this.nextfocusfield = _other.nextfocusfield;
                    this.customusagesearch = _other.customusagesearch;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final SubformColumn _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree translationsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("translations"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(translationsPropertyTree!= null):((translationsPropertyTree == null)||(!translationsPropertyTree.isLeaf())))) {
                        this.translations = ((_other.translations == null)?null:_other.translations.newCopyBuilder(this, translationsPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree valuelistProviderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("valuelistProvider"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuelistProviderPropertyTree!= null):((valuelistProviderPropertyTree == null)||(!valuelistProviderPropertyTree.isLeaf())))) {
                        this.valuelistProvider = ((_other.valuelistProvider == null)?null:_other.valuelistProvider.newCopyBuilder(this, valuelistProviderPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree textModulePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("textModule"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(textModulePropertyTree!= null):((textModulePropertyTree == null)||(!textModulePropertyTree.isLeaf())))) {
                        this.textModule = ((_other.textModule == null)?null:_other.textModule.newCopyBuilder(this, textModulePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree propertyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("property"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(propertyPropertyTree!= null):((propertyPropertyTree == null)||(!propertyPropertyTree.isLeaf())))) {
                        if (_other.property == null) {
                            this.property = null;
                        } else {
                            this.property = new ArrayList<Property.Builder<SubformColumn.Builder<_B>>>();
                            for (Property _item: _other.property) {
                                this.property.add(((_item == null)?null:_item.newCopyBuilder(this, propertyPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree labelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("label"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(labelPropertyTree!= null):((labelPropertyTree == null)||(!labelPropertyTree.isLeaf())))) {
                        this.label = _other.label;
                    }
                    final PropertyTree controltypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("controltype"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(controltypePropertyTree!= null):((controltypePropertyTree == null)||(!controltypePropertyTree.isLeaf())))) {
                        this.controltype = _other.controltype;
                    }
                    final PropertyTree controltypeclassPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("controltypeclass"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(controltypeclassPropertyTree!= null):((controltypeclassPropertyTree == null)||(!controltypeclassPropertyTree.isLeaf())))) {
                        this.controltypeclass = _other.controltypeclass;
                    }
                    final PropertyTree visiblePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("visible"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(visiblePropertyTree!= null):((visiblePropertyTree == null)||(!visiblePropertyTree.isLeaf())))) {
                        this.visible = _other.visible;
                    }
                    final PropertyTree enabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("enabled"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(enabledPropertyTree!= null):((enabledPropertyTree == null)||(!enabledPropertyTree.isLeaf())))) {
                        this.enabled = _other.enabled;
                    }
                    final PropertyTree notcloneablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("notcloneable"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(notcloneablePropertyTree!= null):((notcloneablePropertyTree == null)||(!notcloneablePropertyTree.isLeaf())))) {
                        this.notcloneable = _other.notcloneable;
                    }
                    final PropertyTree insertablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("insertable"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(insertablePropertyTree!= null):((insertablePropertyTree == null)||(!insertablePropertyTree.isLeaf())))) {
                        this.insertable = _other.insertable;
                    }
                    final PropertyTree lovsearchPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("lovsearch"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(lovsearchPropertyTree!= null):((lovsearchPropertyTree == null)||(!lovsearchPropertyTree.isLeaf())))) {
                        this.lovsearch = _other.lovsearch;
                    }
                    final PropertyTree rowsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rows"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowsPropertyTree!= null):((rowsPropertyTree == null)||(!rowsPropertyTree.isLeaf())))) {
                        this.rows = _other.rows;
                    }
                    final PropertyTree columnsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("columns"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(columnsPropertyTree!= null):((columnsPropertyTree == null)||(!columnsPropertyTree.isLeaf())))) {
                        this.columns = _other.columns;
                    }
                    final PropertyTree resourceIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resourceId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resourceIdPropertyTree!= null):((resourceIdPropertyTree == null)||(!resourceIdPropertyTree.isLeaf())))) {
                        this.resourceId = _other.resourceId;
                    }
                    final PropertyTree widthPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("width"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(widthPropertyTree!= null):((widthPropertyTree == null)||(!widthPropertyTree.isLeaf())))) {
                        this.width = _other.width;
                    }
                    final PropertyTree nextfocuscomponentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextfocuscomponent"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextfocuscomponentPropertyTree!= null):((nextfocuscomponentPropertyTree == null)||(!nextfocuscomponentPropertyTree.isLeaf())))) {
                        this.nextfocuscomponent = _other.nextfocuscomponent;
                    }
                    final PropertyTree nextfocusfieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextfocusfield"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextfocusfieldPropertyTree!= null):((nextfocusfieldPropertyTree == null)||(!nextfocusfieldPropertyTree.isLeaf())))) {
                        this.nextfocusfield = _other.nextfocusfield;
                    }
                    final PropertyTree customusagesearchPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("customusagesearch"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(customusagesearchPropertyTree!= null):((customusagesearchPropertyTree == null)||(!customusagesearchPropertyTree.isLeaf())))) {
                        this.customusagesearch = _other.customusagesearch;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends SubformColumn >_P init(final _P _product) {
            _product.translations = ((this.translations == null)?null:this.translations.build());
            _product.valuelistProvider = ((this.valuelistProvider == null)?null:this.valuelistProvider.build());
            _product.textModule = ((this.textModule == null)?null:this.textModule.build());
            if (this.property!= null) {
                final List<Property> property = new ArrayList<Property>(this.property.size());
                for (Property.Builder<SubformColumn.Builder<_B>> _item: this.property) {
                    property.add(_item.build());
                }
                _product.property = property;
            }
            _product.name = this.name;
            _product.label = this.label;
            _product.controltype = this.controltype;
            _product.controltypeclass = this.controltypeclass;
            _product.visible = this.visible;
            _product.enabled = this.enabled;
            _product.notcloneable = this.notcloneable;
            _product.insertable = this.insertable;
            _product.lovsearch = this.lovsearch;
            _product.rows = this.rows;
            _product.columns = this.columns;
            _product.resourceId = this.resourceId;
            _product.width = this.width;
            _product.nextfocuscomponent = this.nextfocuscomponent;
            _product.nextfocusfield = this.nextfocusfield;
            _product.customusagesearch = this.customusagesearch;
            return _product;
        }

        /**
         * Sets the new value of "translations" (any previous value will be replaced)
         * 
         * @param translations
         *     New value of the "translations" property.
         */
        public SubformColumn.Builder<_B> withTranslations(final Translations translations) {
            this.translations = ((translations == null)?null:new Translations.Builder<SubformColumn.Builder<_B>>(this, translations, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "translations" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Translations.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "translations" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Translations.Builder#end()} to return to the current builder.
         */
        public Translations.Builder<? extends SubformColumn.Builder<_B>> withTranslations() {
            if (this.translations!= null) {
                return this.translations;
            }
            return this.translations = new Translations.Builder<SubformColumn.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "valuelistProvider" (any previous value will be replaced)
         * 
         * @param valuelistProvider
         *     New value of the "valuelistProvider" property.
         */
        public SubformColumn.Builder<_B> withValuelistProvider(final ValuelistProvider valuelistProvider) {
            this.valuelistProvider = ((valuelistProvider == null)?null:new ValuelistProvider.Builder<SubformColumn.Builder<_B>>(this, valuelistProvider, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "valuelistProvider" property.
         * Use {@link org.nuclos.schema.layout.layoutml.ValuelistProvider.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "valuelistProvider" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.ValuelistProvider.Builder#end()} to return to the current builder.
         */
        public ValuelistProvider.Builder<? extends SubformColumn.Builder<_B>> withValuelistProvider() {
            if (this.valuelistProvider!= null) {
                return this.valuelistProvider;
            }
            return this.valuelistProvider = new ValuelistProvider.Builder<SubformColumn.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "textModule" (any previous value will be replaced)
         * 
         * @param textModule
         *     New value of the "textModule" property.
         */
        public SubformColumn.Builder<_B> withTextModule(final TextModule textModule) {
            this.textModule = ((textModule == null)?null:new TextModule.Builder<SubformColumn.Builder<_B>>(this, textModule, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "textModule" property.
         * Use {@link org.nuclos.schema.layout.layoutml.TextModule.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "textModule" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.TextModule.Builder#end()} to return to the current builder.
         */
        public TextModule.Builder<? extends SubformColumn.Builder<_B>> withTextModule() {
            if (this.textModule!= null) {
                return this.textModule;
            }
            return this.textModule = new TextModule.Builder<SubformColumn.Builder<_B>>(this, null, false);
        }

        /**
         * Adds the given items to the value of "property"
         * 
         * @param property
         *     Items to add to the value of the "property" property
         */
        public SubformColumn.Builder<_B> addProperty(final Iterable<? extends Property> property) {
            if (property!= null) {
                if (this.property == null) {
                    this.property = new ArrayList<Property.Builder<SubformColumn.Builder<_B>>>();
                }
                for (Property _item: property) {
                    this.property.add(new Property.Builder<SubformColumn.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "property" (any previous value will be replaced)
         * 
         * @param property
         *     New value of the "property" property.
         */
        public SubformColumn.Builder<_B> withProperty(final Iterable<? extends Property> property) {
            if (this.property!= null) {
                this.property.clear();
            }
            return addProperty(property);
        }

        /**
         * Adds the given items to the value of "property"
         * 
         * @param property
         *     Items to add to the value of the "property" property
         */
        public SubformColumn.Builder<_B> addProperty(Property... property) {
            addProperty(Arrays.asList(property));
            return this;
        }

        /**
         * Sets the new value of "property" (any previous value will be replaced)
         * 
         * @param property
         *     New value of the "property" property.
         */
        public SubformColumn.Builder<_B> withProperty(Property... property) {
            withProperty(Arrays.asList(property));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Property" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Property.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Property" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Property.Builder#end()} to return to the current builder.
         */
        public Property.Builder<? extends SubformColumn.Builder<_B>> addProperty() {
            if (this.property == null) {
                this.property = new ArrayList<Property.Builder<SubformColumn.Builder<_B>>>();
            }
            final Property.Builder<SubformColumn.Builder<_B>> property_Builder = new Property.Builder<SubformColumn.Builder<_B>>(this, null, false);
            this.property.add(property_Builder);
            return property_Builder;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public SubformColumn.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "label" (any previous value will be replaced)
         * 
         * @param label
         *     New value of the "label" property.
         */
        public SubformColumn.Builder<_B> withLabel(final String label) {
            this.label = label;
            return this;
        }

        /**
         * Sets the new value of "controltype" (any previous value will be replaced)
         * 
         * @param controltype
         *     New value of the "controltype" property.
         */
        public SubformColumn.Builder<_B> withControltype(final Controltype controltype) {
            this.controltype = controltype;
            return this;
        }

        /**
         * Sets the new value of "controltypeclass" (any previous value will be replaced)
         * 
         * @param controltypeclass
         *     New value of the "controltypeclass" property.
         */
        public SubformColumn.Builder<_B> withControltypeclass(final String controltypeclass) {
            this.controltypeclass = controltypeclass;
            return this;
        }

        /**
         * Sets the new value of "visible" (any previous value will be replaced)
         * 
         * @param visible
         *     New value of the "visible" property.
         */
        public SubformColumn.Builder<_B> withVisible(final Boolean visible) {
            this.visible = visible;
            return this;
        }

        /**
         * Sets the new value of "enabled" (any previous value will be replaced)
         * 
         * @param enabled
         *     New value of the "enabled" property.
         */
        public SubformColumn.Builder<_B> withEnabled(final Boolean enabled) {
            this.enabled = enabled;
            return this;
        }

        /**
         * Sets the new value of "notcloneable" (any previous value will be replaced)
         * 
         * @param notcloneable
         *     New value of the "notcloneable" property.
         */
        public SubformColumn.Builder<_B> withNotcloneable(final Boolean notcloneable) {
            this.notcloneable = notcloneable;
            return this;
        }

        /**
         * Sets the new value of "insertable" (any previous value will be replaced)
         * 
         * @param insertable
         *     New value of the "insertable" property.
         */
        public SubformColumn.Builder<_B> withInsertable(final Boolean insertable) {
            this.insertable = insertable;
            return this;
        }

        /**
         * Sets the new value of "lovsearch" (any previous value will be replaced)
         * 
         * @param lovsearch
         *     New value of the "lovsearch" property.
         */
        public SubformColumn.Builder<_B> withLovsearch(final Boolean lovsearch) {
            this.lovsearch = lovsearch;
            return this;
        }

        /**
         * Sets the new value of "rows" (any previous value will be replaced)
         * 
         * @param rows
         *     New value of the "rows" property.
         */
        public SubformColumn.Builder<_B> withRows(final String rows) {
            this.rows = rows;
            return this;
        }

        /**
         * Sets the new value of "columns" (any previous value will be replaced)
         * 
         * @param columns
         *     New value of the "columns" property.
         */
        public SubformColumn.Builder<_B> withColumns(final String columns) {
            this.columns = columns;
            return this;
        }

        /**
         * Sets the new value of "resourceId" (any previous value will be replaced)
         * 
         * @param resourceId
         *     New value of the "resourceId" property.
         */
        public SubformColumn.Builder<_B> withResourceId(final String resourceId) {
            this.resourceId = resourceId;
            return this;
        }

        /**
         * Sets the new value of "width" (any previous value will be replaced)
         * 
         * @param width
         *     New value of the "width" property.
         */
        public SubformColumn.Builder<_B> withWidth(final String width) {
            this.width = width;
            return this;
        }

        /**
         * Sets the new value of "nextfocuscomponent" (any previous value will be replaced)
         * 
         * @param nextfocuscomponent
         *     New value of the "nextfocuscomponent" property.
         */
        public SubformColumn.Builder<_B> withNextfocuscomponent(final String nextfocuscomponent) {
            this.nextfocuscomponent = nextfocuscomponent;
            return this;
        }

        /**
         * Sets the new value of "nextfocusfield" (any previous value will be replaced)
         * 
         * @param nextfocusfield
         *     New value of the "nextfocusfield" property.
         */
        public SubformColumn.Builder<_B> withNextfocusfield(final String nextfocusfield) {
            this.nextfocusfield = nextfocusfield;
            return this;
        }

        /**
         * Sets the new value of "customusagesearch" (any previous value will be replaced)
         * 
         * @param customusagesearch
         *     New value of the "customusagesearch" property.
         */
        public SubformColumn.Builder<_B> withCustomusagesearch(final String customusagesearch) {
            this.customusagesearch = customusagesearch;
            return this;
        }

        @Override
        public SubformColumn build() {
            if (_storedValue == null) {
                return this.init(new SubformColumn());
            } else {
                return ((SubformColumn) _storedValue);
            }
        }

        public SubformColumn.Builder<_B> copyOf(final SubformColumn _other) {
            _other.copyTo(this);
            return this;
        }

        public SubformColumn.Builder<_B> copyOf(final SubformColumn.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends SubformColumn.Selector<SubformColumn.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static SubformColumn.Select _root() {
            return new SubformColumn.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private Translations.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> translations = null;
        private ValuelistProvider.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> valuelistProvider = null;
        private TextModule.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> textModule = null;
        private Property.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> property = null;
        private com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> label = null;
        private com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> controltype = null;
        private com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> controltypeclass = null;
        private com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> visible = null;
        private com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> enabled = null;
        private com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> notcloneable = null;
        private com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> insertable = null;
        private com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> lovsearch = null;
        private com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> rows = null;
        private com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> columns = null;
        private com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> resourceId = null;
        private com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> width = null;
        private com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> nextfocuscomponent = null;
        private com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> nextfocusfield = null;
        private com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> customusagesearch = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.translations!= null) {
                products.put("translations", this.translations.init());
            }
            if (this.valuelistProvider!= null) {
                products.put("valuelistProvider", this.valuelistProvider.init());
            }
            if (this.textModule!= null) {
                products.put("textModule", this.textModule.init());
            }
            if (this.property!= null) {
                products.put("property", this.property.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.label!= null) {
                products.put("label", this.label.init());
            }
            if (this.controltype!= null) {
                products.put("controltype", this.controltype.init());
            }
            if (this.controltypeclass!= null) {
                products.put("controltypeclass", this.controltypeclass.init());
            }
            if (this.visible!= null) {
                products.put("visible", this.visible.init());
            }
            if (this.enabled!= null) {
                products.put("enabled", this.enabled.init());
            }
            if (this.notcloneable!= null) {
                products.put("notcloneable", this.notcloneable.init());
            }
            if (this.insertable!= null) {
                products.put("insertable", this.insertable.init());
            }
            if (this.lovsearch!= null) {
                products.put("lovsearch", this.lovsearch.init());
            }
            if (this.rows!= null) {
                products.put("rows", this.rows.init());
            }
            if (this.columns!= null) {
                products.put("columns", this.columns.init());
            }
            if (this.resourceId!= null) {
                products.put("resourceId", this.resourceId.init());
            }
            if (this.width!= null) {
                products.put("width", this.width.init());
            }
            if (this.nextfocuscomponent!= null) {
                products.put("nextfocuscomponent", this.nextfocuscomponent.init());
            }
            if (this.nextfocusfield!= null) {
                products.put("nextfocusfield", this.nextfocusfield.init());
            }
            if (this.customusagesearch!= null) {
                products.put("customusagesearch", this.customusagesearch.init());
            }
            return products;
        }

        public Translations.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> translations() {
            return ((this.translations == null)?this.translations = new Translations.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>>(this._root, this, "translations"):this.translations);
        }

        public ValuelistProvider.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> valuelistProvider() {
            return ((this.valuelistProvider == null)?this.valuelistProvider = new ValuelistProvider.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>>(this._root, this, "valuelistProvider"):this.valuelistProvider);
        }

        public TextModule.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> textModule() {
            return ((this.textModule == null)?this.textModule = new TextModule.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>>(this._root, this, "textModule"):this.textModule);
        }

        public Property.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> property() {
            return ((this.property == null)?this.property = new Property.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>>(this._root, this, "property"):this.property);
        }

        public com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> label() {
            return ((this.label == null)?this.label = new com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>>(this._root, this, "label"):this.label);
        }

        public com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> controltype() {
            return ((this.controltype == null)?this.controltype = new com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>>(this._root, this, "controltype"):this.controltype);
        }

        public com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> controltypeclass() {
            return ((this.controltypeclass == null)?this.controltypeclass = new com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>>(this._root, this, "controltypeclass"):this.controltypeclass);
        }

        public com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> visible() {
            return ((this.visible == null)?this.visible = new com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>>(this._root, this, "visible"):this.visible);
        }

        public com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> enabled() {
            return ((this.enabled == null)?this.enabled = new com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>>(this._root, this, "enabled"):this.enabled);
        }

        public com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> notcloneable() {
            return ((this.notcloneable == null)?this.notcloneable = new com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>>(this._root, this, "notcloneable"):this.notcloneable);
        }

        public com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> insertable() {
            return ((this.insertable == null)?this.insertable = new com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>>(this._root, this, "insertable"):this.insertable);
        }

        public com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> lovsearch() {
            return ((this.lovsearch == null)?this.lovsearch = new com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>>(this._root, this, "lovsearch"):this.lovsearch);
        }

        public com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> rows() {
            return ((this.rows == null)?this.rows = new com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>>(this._root, this, "rows"):this.rows);
        }

        public com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> columns() {
            return ((this.columns == null)?this.columns = new com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>>(this._root, this, "columns"):this.columns);
        }

        public com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> resourceId() {
            return ((this.resourceId == null)?this.resourceId = new com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>>(this._root, this, "resourceId"):this.resourceId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> width() {
            return ((this.width == null)?this.width = new com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>>(this._root, this, "width"):this.width);
        }

        public com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> nextfocuscomponent() {
            return ((this.nextfocuscomponent == null)?this.nextfocuscomponent = new com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>>(this._root, this, "nextfocuscomponent"):this.nextfocuscomponent);
        }

        public com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> nextfocusfield() {
            return ((this.nextfocusfield == null)?this.nextfocusfield = new com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>>(this._root, this, "nextfocusfield"):this.nextfocusfield);
        }

        public com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>> customusagesearch() {
            return ((this.customusagesearch == null)?this.customusagesearch = new com.kscs.util.jaxb.Selector<TRoot, SubformColumn.Selector<TRoot, TParent>>(this._root, this, "customusagesearch"):this.customusagesearch);
        }

    }

}
