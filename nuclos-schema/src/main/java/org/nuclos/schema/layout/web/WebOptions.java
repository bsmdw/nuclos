
package org.nuclos.schema.layout.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Options of an optiongroup.
 * 
 * <p>Java class for web-options complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="web-options"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="option" type="{urn:org.nuclos.schema.layout.web}web-option" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="default" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="orientation" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "web-options", propOrder = {
    "option"
})
public class WebOptions implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected List<WebOption> option;
    @XmlAttribute(name = "name")
    protected String name;
    @XmlAttribute(name = "default")
    protected String _default;
    @XmlAttribute(name = "orientation")
    protected String orientation;

    /**
     * Gets the value of the option property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the option property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOption().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WebOption }
     * 
     * 
     */
    public List<WebOption> getOption() {
        if (option == null) {
            option = new ArrayList<WebOption>();
        }
        return this.option;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the default property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefault() {
        return _default;
    }

    /**
     * Sets the value of the default property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefault(String value) {
        this._default = value;
    }

    /**
     * Gets the value of the orientation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrientation() {
        return orientation;
    }

    /**
     * Sets the value of the orientation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrientation(String value) {
        this.orientation = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<WebOption> theOption;
            theOption = (((this.option!= null)&&(!this.option.isEmpty()))?this.getOption():null);
            strategy.appendField(locator, this, "option", buffer, theOption);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String theDefault;
            theDefault = this.getDefault();
            strategy.appendField(locator, this, "_default", buffer, theDefault);
        }
        {
            String theOrientation;
            theOrientation = this.getOrientation();
            strategy.appendField(locator, this, "orientation", buffer, theOrientation);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof WebOptions)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final WebOptions that = ((WebOptions) object);
        {
            List<WebOption> lhsOption;
            lhsOption = (((this.option!= null)&&(!this.option.isEmpty()))?this.getOption():null);
            List<WebOption> rhsOption;
            rhsOption = (((that.option!= null)&&(!that.option.isEmpty()))?that.getOption():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "option", lhsOption), LocatorUtils.property(thatLocator, "option", rhsOption), lhsOption, rhsOption)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsDefault;
            lhsDefault = this.getDefault();
            String rhsDefault;
            rhsDefault = that.getDefault();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "_default", lhsDefault), LocatorUtils.property(thatLocator, "_default", rhsDefault), lhsDefault, rhsDefault)) {
                return false;
            }
        }
        {
            String lhsOrientation;
            lhsOrientation = this.getOrientation();
            String rhsOrientation;
            rhsOrientation = that.getOrientation();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "orientation", lhsOrientation), LocatorUtils.property(thatLocator, "orientation", rhsOrientation), lhsOrientation, rhsOrientation)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<WebOption> theOption;
            theOption = (((this.option!= null)&&(!this.option.isEmpty()))?this.getOption():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "option", theOption), currentHashCode, theOption);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String theDefault;
            theDefault = this.getDefault();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "_default", theDefault), currentHashCode, theDefault);
        }
        {
            String theOrientation;
            theOrientation = this.getOrientation();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "orientation", theOrientation), currentHashCode, theOrientation);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof WebOptions) {
            final WebOptions copy = ((WebOptions) draftCopy);
            if ((this.option!= null)&&(!this.option.isEmpty())) {
                List<WebOption> sourceOption;
                sourceOption = (((this.option!= null)&&(!this.option.isEmpty()))?this.getOption():null);
                @SuppressWarnings("unchecked")
                List<WebOption> copyOption = ((List<WebOption> ) strategy.copy(LocatorUtils.property(locator, "option", sourceOption), sourceOption));
                copy.option = null;
                if (copyOption!= null) {
                    List<WebOption> uniqueOptionl = copy.getOption();
                    uniqueOptionl.addAll(copyOption);
                }
            } else {
                copy.option = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this._default!= null) {
                String sourceDefault;
                sourceDefault = this.getDefault();
                String copyDefault = ((String) strategy.copy(LocatorUtils.property(locator, "_default", sourceDefault), sourceDefault));
                copy.setDefault(copyDefault);
            } else {
                copy._default = null;
            }
            if (this.orientation!= null) {
                String sourceOrientation;
                sourceOrientation = this.getOrientation();
                String copyOrientation = ((String) strategy.copy(LocatorUtils.property(locator, "orientation", sourceOrientation), sourceOrientation));
                copy.setOrientation(copyOrientation);
            } else {
                copy.orientation = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new WebOptions();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebOptions.Builder<_B> _other) {
        if (this.option == null) {
            _other.option = null;
        } else {
            _other.option = new ArrayList<WebOption.Builder<WebOptions.Builder<_B>>>();
            for (WebOption _item: this.option) {
                _other.option.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.name = this.name;
        _other._default = this._default;
        _other.orientation = this.orientation;
    }

    public<_B >WebOptions.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new WebOptions.Builder<_B>(_parentBuilder, this, true);
    }

    public WebOptions.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static WebOptions.Builder<Void> builder() {
        return new WebOptions.Builder<Void>(null, null, false);
    }

    public static<_B >WebOptions.Builder<_B> copyOf(final WebOptions _other) {
        final WebOptions.Builder<_B> _newBuilder = new WebOptions.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebOptions.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree optionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("option"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(optionPropertyTree!= null):((optionPropertyTree == null)||(!optionPropertyTree.isLeaf())))) {
            if (this.option == null) {
                _other.option = null;
            } else {
                _other.option = new ArrayList<WebOption.Builder<WebOptions.Builder<_B>>>();
                for (WebOption _item: this.option) {
                    _other.option.add(((_item == null)?null:_item.newCopyBuilder(_other, optionPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree _defaultPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("_default"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(_defaultPropertyTree!= null):((_defaultPropertyTree == null)||(!_defaultPropertyTree.isLeaf())))) {
            _other._default = this._default;
        }
        final PropertyTree orientationPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("orientation"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(orientationPropertyTree!= null):((orientationPropertyTree == null)||(!orientationPropertyTree.isLeaf())))) {
            _other.orientation = this.orientation;
        }
    }

    public<_B >WebOptions.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new WebOptions.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public WebOptions.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >WebOptions.Builder<_B> copyOf(final WebOptions _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebOptions.Builder<_B> _newBuilder = new WebOptions.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static WebOptions.Builder<Void> copyExcept(final WebOptions _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebOptions.Builder<Void> copyOnly(final WebOptions _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final WebOptions _storedValue;
        private List<WebOption.Builder<WebOptions.Builder<_B>>> option;
        private String name;
        private String _default;
        private String orientation;

        public Builder(final _B _parentBuilder, final WebOptions _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.option == null) {
                        this.option = null;
                    } else {
                        this.option = new ArrayList<WebOption.Builder<WebOptions.Builder<_B>>>();
                        for (WebOption _item: _other.option) {
                            this.option.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.name = _other.name;
                    this._default = _other._default;
                    this.orientation = _other.orientation;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final WebOptions _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree optionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("option"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(optionPropertyTree!= null):((optionPropertyTree == null)||(!optionPropertyTree.isLeaf())))) {
                        if (_other.option == null) {
                            this.option = null;
                        } else {
                            this.option = new ArrayList<WebOption.Builder<WebOptions.Builder<_B>>>();
                            for (WebOption _item: _other.option) {
                                this.option.add(((_item == null)?null:_item.newCopyBuilder(this, optionPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree _defaultPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("_default"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(_defaultPropertyTree!= null):((_defaultPropertyTree == null)||(!_defaultPropertyTree.isLeaf())))) {
                        this._default = _other._default;
                    }
                    final PropertyTree orientationPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("orientation"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(orientationPropertyTree!= null):((orientationPropertyTree == null)||(!orientationPropertyTree.isLeaf())))) {
                        this.orientation = _other.orientation;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends WebOptions >_P init(final _P _product) {
            if (this.option!= null) {
                final List<WebOption> option = new ArrayList<WebOption>(this.option.size());
                for (WebOption.Builder<WebOptions.Builder<_B>> _item: this.option) {
                    option.add(_item.build());
                }
                _product.option = option;
            }
            _product.name = this.name;
            _product._default = this._default;
            _product.orientation = this.orientation;
            return _product;
        }

        /**
         * Adds the given items to the value of "option"
         * 
         * @param option
         *     Items to add to the value of the "option" property
         */
        public WebOptions.Builder<_B> addOption(final Iterable<? extends WebOption> option) {
            if (option!= null) {
                if (this.option == null) {
                    this.option = new ArrayList<WebOption.Builder<WebOptions.Builder<_B>>>();
                }
                for (WebOption _item: option) {
                    this.option.add(new WebOption.Builder<WebOptions.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "option" (any previous value will be replaced)
         * 
         * @param option
         *     New value of the "option" property.
         */
        public WebOptions.Builder<_B> withOption(final Iterable<? extends WebOption> option) {
            if (this.option!= null) {
                this.option.clear();
            }
            return addOption(option);
        }

        /**
         * Adds the given items to the value of "option"
         * 
         * @param option
         *     Items to add to the value of the "option" property
         */
        public WebOptions.Builder<_B> addOption(WebOption... option) {
            addOption(Arrays.asList(option));
            return this;
        }

        /**
         * Sets the new value of "option" (any previous value will be replaced)
         * 
         * @param option
         *     New value of the "option" property.
         */
        public WebOptions.Builder<_B> withOption(WebOption... option) {
            withOption(Arrays.asList(option));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Option" property.
         * Use {@link org.nuclos.schema.layout.web.WebOption.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Option" property.
         *     Use {@link org.nuclos.schema.layout.web.WebOption.Builder#end()} to return to the current builder.
         */
        public WebOption.Builder<? extends WebOptions.Builder<_B>> addOption() {
            if (this.option == null) {
                this.option = new ArrayList<WebOption.Builder<WebOptions.Builder<_B>>>();
            }
            final WebOption.Builder<WebOptions.Builder<_B>> option_Builder = new WebOption.Builder<WebOptions.Builder<_B>>(this, null, false);
            this.option.add(option_Builder);
            return option_Builder;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public WebOptions.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "_default" (any previous value will be replaced)
         * 
         * @param _default
         *     New value of the "_default" property.
         */
        public WebOptions.Builder<_B> withDefault(final String _default) {
            this._default = _default;
            return this;
        }

        /**
         * Sets the new value of "orientation" (any previous value will be replaced)
         * 
         * @param orientation
         *     New value of the "orientation" property.
         */
        public WebOptions.Builder<_B> withOrientation(final String orientation) {
            this.orientation = orientation;
            return this;
        }

        @Override
        public WebOptions build() {
            if (_storedValue == null) {
                return this.init(new WebOptions());
            } else {
                return ((WebOptions) _storedValue);
            }
        }

        public WebOptions.Builder<_B> copyOf(final WebOptions _other) {
            _other.copyTo(this);
            return this;
        }

        public WebOptions.Builder<_B> copyOf(final WebOptions.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends WebOptions.Selector<WebOptions.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static WebOptions.Select _root() {
            return new WebOptions.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private WebOption.Selector<TRoot, WebOptions.Selector<TRoot, TParent>> option = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebOptions.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebOptions.Selector<TRoot, TParent>> _default = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebOptions.Selector<TRoot, TParent>> orientation = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.option!= null) {
                products.put("option", this.option.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this._default!= null) {
                products.put("_default", this._default.init());
            }
            if (this.orientation!= null) {
                products.put("orientation", this.orientation.init());
            }
            return products;
        }

        public WebOption.Selector<TRoot, WebOptions.Selector<TRoot, TParent>> option() {
            return ((this.option == null)?this.option = new WebOption.Selector<TRoot, WebOptions.Selector<TRoot, TParent>>(this._root, this, "option"):this.option);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebOptions.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, WebOptions.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebOptions.Selector<TRoot, TParent>> _default() {
            return ((this._default == null)?this._default = new com.kscs.util.jaxb.Selector<TRoot, WebOptions.Selector<TRoot, TParent>>(this._root, this, "_default"):this._default);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebOptions.Selector<TRoot, TParent>> orientation() {
            return ((this.orientation == null)?this.orientation = new com.kscs.util.jaxb.Selector<TRoot, WebOptions.Selector<TRoot, TParent>>(this._root, this, "orientation"):this.orientation);
        }

    }

}
