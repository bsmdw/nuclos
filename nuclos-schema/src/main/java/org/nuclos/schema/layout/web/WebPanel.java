
package org.nuclos.schema.layout.web;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * A panel element with a title.
 * 
 * <p>Java class for web-panel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="web-panel"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:org.nuclos.schema.layout.web}web-container"&gt;
 *       &lt;attribute name="title" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="border-width" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="border-color" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "web-panel")
public class WebPanel
    extends WebContainer
    implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "title")
    protected String title;
    @XmlAttribute(name = "border-width")
    protected String borderWidth;
    @XmlAttribute(name = "border-color")
    protected String borderColor;

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the borderWidth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBorderWidth() {
        return borderWidth;
    }

    /**
     * Sets the value of the borderWidth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBorderWidth(String value) {
        this.borderWidth = value;
    }

    /**
     * Gets the value of the borderColor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBorderColor() {
        return borderColor;
    }

    /**
     * Sets the value of the borderColor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBorderColor(String value) {
        this.borderColor = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        super.appendFields(locator, buffer, strategy);
        {
            String theTitle;
            theTitle = this.getTitle();
            strategy.appendField(locator, this, "title", buffer, theTitle);
        }
        {
            String theBorderWidth;
            theBorderWidth = this.getBorderWidth();
            strategy.appendField(locator, this, "borderWidth", buffer, theBorderWidth);
        }
        {
            String theBorderColor;
            theBorderColor = this.getBorderColor();
            strategy.appendField(locator, this, "borderColor", buffer, theBorderColor);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof WebPanel)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final WebPanel that = ((WebPanel) object);
        {
            String lhsTitle;
            lhsTitle = this.getTitle();
            String rhsTitle;
            rhsTitle = that.getTitle();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "title", lhsTitle), LocatorUtils.property(thatLocator, "title", rhsTitle), lhsTitle, rhsTitle)) {
                return false;
            }
        }
        {
            String lhsBorderWidth;
            lhsBorderWidth = this.getBorderWidth();
            String rhsBorderWidth;
            rhsBorderWidth = that.getBorderWidth();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "borderWidth", lhsBorderWidth), LocatorUtils.property(thatLocator, "borderWidth", rhsBorderWidth), lhsBorderWidth, rhsBorderWidth)) {
                return false;
            }
        }
        {
            String lhsBorderColor;
            lhsBorderColor = this.getBorderColor();
            String rhsBorderColor;
            rhsBorderColor = that.getBorderColor();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "borderColor", lhsBorderColor), LocatorUtils.property(thatLocator, "borderColor", rhsBorderColor), lhsBorderColor, rhsBorderColor)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = super.hashCode(locator, strategy);
        {
            String theTitle;
            theTitle = this.getTitle();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "title", theTitle), currentHashCode, theTitle);
        }
        {
            String theBorderWidth;
            theBorderWidth = this.getBorderWidth();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "borderWidth", theBorderWidth), currentHashCode, theBorderWidth);
        }
        {
            String theBorderColor;
            theBorderColor = this.getBorderColor();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "borderColor", theBorderColor), currentHashCode, theBorderColor);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof WebPanel) {
            final WebPanel copy = ((WebPanel) draftCopy);
            if (this.title!= null) {
                String sourceTitle;
                sourceTitle = this.getTitle();
                String copyTitle = ((String) strategy.copy(LocatorUtils.property(locator, "title", sourceTitle), sourceTitle));
                copy.setTitle(copyTitle);
            } else {
                copy.title = null;
            }
            if (this.borderWidth!= null) {
                String sourceBorderWidth;
                sourceBorderWidth = this.getBorderWidth();
                String copyBorderWidth = ((String) strategy.copy(LocatorUtils.property(locator, "borderWidth", sourceBorderWidth), sourceBorderWidth));
                copy.setBorderWidth(copyBorderWidth);
            } else {
                copy.borderWidth = null;
            }
            if (this.borderColor!= null) {
                String sourceBorderColor;
                sourceBorderColor = this.getBorderColor();
                String copyBorderColor = ((String) strategy.copy(LocatorUtils.property(locator, "borderColor", sourceBorderColor), sourceBorderColor));
                copy.setBorderColor(copyBorderColor);
            } else {
                copy.borderColor = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new WebPanel();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebPanel.Builder<_B> _other) {
        super.copyTo(_other);
        _other.title = this.title;
        _other.borderWidth = this.borderWidth;
        _other.borderColor = this.borderColor;
    }

    @Override
    public<_B >WebPanel.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new WebPanel.Builder<_B>(_parentBuilder, this, true);
    }

    @Override
    public WebPanel.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static WebPanel.Builder<Void> builder() {
        return new WebPanel.Builder<Void>(null, null, false);
    }

    public static<_B >WebPanel.Builder<_B> copyOf(final WebComponent _other) {
        final WebPanel.Builder<_B> _newBuilder = new WebPanel.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >WebPanel.Builder<_B> copyOf(final WebContainer _other) {
        final WebPanel.Builder<_B> _newBuilder = new WebPanel.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >WebPanel.Builder<_B> copyOf(final WebPanel _other) {
        final WebPanel.Builder<_B> _newBuilder = new WebPanel.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebPanel.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        super.copyTo(_other, _propertyTree, _propertyTreeUse);
        final PropertyTree titlePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("title"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(titlePropertyTree!= null):((titlePropertyTree == null)||(!titlePropertyTree.isLeaf())))) {
            _other.title = this.title;
        }
        final PropertyTree borderWidthPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("borderWidth"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(borderWidthPropertyTree!= null):((borderWidthPropertyTree == null)||(!borderWidthPropertyTree.isLeaf())))) {
            _other.borderWidth = this.borderWidth;
        }
        final PropertyTree borderColorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("borderColor"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(borderColorPropertyTree!= null):((borderColorPropertyTree == null)||(!borderColorPropertyTree.isLeaf())))) {
            _other.borderColor = this.borderColor;
        }
    }

    @Override
    public<_B >WebPanel.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new WebPanel.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    @Override
    public WebPanel.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >WebPanel.Builder<_B> copyOf(final WebComponent _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebPanel.Builder<_B> _newBuilder = new WebPanel.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >WebPanel.Builder<_B> copyOf(final WebContainer _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebPanel.Builder<_B> _newBuilder = new WebPanel.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >WebPanel.Builder<_B> copyOf(final WebPanel _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebPanel.Builder<_B> _newBuilder = new WebPanel.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static WebPanel.Builder<Void> copyExcept(final WebComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebPanel.Builder<Void> copyExcept(final WebContainer _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebPanel.Builder<Void> copyExcept(final WebPanel _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebPanel.Builder<Void> copyOnly(final WebComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static WebPanel.Builder<Void> copyOnly(final WebContainer _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static WebPanel.Builder<Void> copyOnly(final WebPanel _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >
        extends WebContainer.Builder<_B>
        implements Buildable
    {

        private String title;
        private String borderWidth;
        private String borderColor;

        public Builder(final _B _parentBuilder, final WebPanel _other, final boolean _copy) {
            super(_parentBuilder, _other, _copy);
            if (_other!= null) {
                this.title = _other.title;
                this.borderWidth = _other.borderWidth;
                this.borderColor = _other.borderColor;
            }
        }

        public Builder(final _B _parentBuilder, final WebPanel _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            super(_parentBuilder, _other, _copy, _propertyTree, _propertyTreeUse);
            if (_other!= null) {
                final PropertyTree titlePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("title"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(titlePropertyTree!= null):((titlePropertyTree == null)||(!titlePropertyTree.isLeaf())))) {
                    this.title = _other.title;
                }
                final PropertyTree borderWidthPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("borderWidth"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(borderWidthPropertyTree!= null):((borderWidthPropertyTree == null)||(!borderWidthPropertyTree.isLeaf())))) {
                    this.borderWidth = _other.borderWidth;
                }
                final PropertyTree borderColorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("borderColor"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(borderColorPropertyTree!= null):((borderColorPropertyTree == null)||(!borderColorPropertyTree.isLeaf())))) {
                    this.borderColor = _other.borderColor;
                }
            }
        }

        protected<_P extends WebPanel >_P init(final _P _product) {
            _product.title = this.title;
            _product.borderWidth = this.borderWidth;
            _product.borderColor = this.borderColor;
            return super.init(_product);
        }

        /**
         * Sets the new value of "title" (any previous value will be replaced)
         * 
         * @param title
         *     New value of the "title" property.
         */
        public WebPanel.Builder<_B> withTitle(final String title) {
            this.title = title;
            return this;
        }

        /**
         * Sets the new value of "borderWidth" (any previous value will be replaced)
         * 
         * @param borderWidth
         *     New value of the "borderWidth" property.
         */
        public WebPanel.Builder<_B> withBorderWidth(final String borderWidth) {
            this.borderWidth = borderWidth;
            return this;
        }

        /**
         * Sets the new value of "borderColor" (any previous value will be replaced)
         * 
         * @param borderColor
         *     New value of the "borderColor" property.
         */
        public WebPanel.Builder<_B> withBorderColor(final String borderColor) {
            this.borderColor = borderColor;
            return this;
        }

        /**
         * Sets the new value of "grid" (any previous value will be replaced)
         * 
         * @param grid
         *     New value of the "grid" property.
         */
        @Override
        public WebPanel.Builder<_B> withGrid(final WebGrid grid) {
            super.withGrid(grid);
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "grid" property.
         * Use {@link org.nuclos.schema.layout.web.WebGrid.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "grid" property.
         *     Use {@link org.nuclos.schema.layout.web.WebGrid.Builder#end()} to return to the current builder.
         */
        public WebGrid.Builder<? extends WebPanel.Builder<_B>> withGrid() {
            return ((WebGrid.Builder<? extends WebPanel.Builder<_B>> ) super.withGrid());
        }

        /**
         * Sets the new value of "table" (any previous value will be replaced)
         * 
         * @param table
         *     New value of the "table" property.
         */
        @Override
        public WebPanel.Builder<_B> withTable(final WebTable table) {
            super.withTable(table);
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "table" property.
         * Use {@link org.nuclos.schema.layout.web.WebTable.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "table" property.
         *     Use {@link org.nuclos.schema.layout.web.WebTable.Builder#end()} to return to the current builder.
         */
        public WebTable.Builder<? extends WebPanel.Builder<_B>> withTable() {
            return ((WebTable.Builder<? extends WebPanel.Builder<_B>> ) super.withTable());
        }

        /**
         * Sets the new value of "calculated" (any previous value will be replaced)
         * 
         * @param calculated
         *     New value of the "calculated" property.
         */
        @Override
        public WebPanel.Builder<_B> withCalculated(final WebGridCalculated calculated) {
            super.withCalculated(calculated);
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "calculated" property.
         * Use {@link org.nuclos.schema.layout.web.WebGridCalculated.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "calculated" property.
         *     Use {@link org.nuclos.schema.layout.web.WebGridCalculated.Builder#end()} to return to the current builder.
         */
        public WebGridCalculated.Builder<? extends WebPanel.Builder<_B>> withCalculated() {
            return ((WebGridCalculated.Builder<? extends WebPanel.Builder<_B>> ) super.withCalculated());
        }

        /**
         * Adds the given items to the value of "components"
         * 
         * @param components
         *     Items to add to the value of the "components" property
         */
        @Override
        public WebPanel.Builder<_B> addComponents(final Iterable<? extends WebComponent> components) {
            super.addComponents(components);
            return this;
        }

        /**
         * Adds the given items to the value of "components"
         * 
         * @param components
         *     Items to add to the value of the "components" property
         */
        @Override
        public WebPanel.Builder<_B> addComponents(WebComponent... components) {
            super.addComponents(components);
            return this;
        }

        /**
         * Sets the new value of "components" (any previous value will be replaced)
         * 
         * @param components
         *     New value of the "components" property.
         */
        @Override
        public WebPanel.Builder<_B> withComponents(final Iterable<? extends WebComponent> components) {
            super.withComponents(components);
            return this;
        }

        /**
         * Sets the new value of "components" (any previous value will be replaced)
         * 
         * @param components
         *     New value of the "components" property.
         */
        @Override
        public WebPanel.Builder<_B> withComponents(WebComponent... components) {
            super.withComponents(components);
            return this;
        }

        /**
         * Sets the new value of "opaque" (any previous value will be replaced)
         * 
         * @param opaque
         *     New value of the "opaque" property.
         */
        @Override
        public WebPanel.Builder<_B> withOpaque(final Boolean opaque) {
            super.withOpaque(opaque);
            return this;
        }

        /**
         * Sets the new value of "backgroundColor" (any previous value will be replaced)
         * 
         * @param backgroundColor
         *     New value of the "backgroundColor" property.
         */
        @Override
        public WebPanel.Builder<_B> withBackgroundColor(final String backgroundColor) {
            super.withBackgroundColor(backgroundColor);
            return this;
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        @Override
        public WebPanel.Builder<_B> addAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            super.addAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        @Override
        public WebPanel.Builder<_B> addAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            super.addAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        @Override
        public WebPanel.Builder<_B> withAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            super.withAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        @Override
        public WebPanel.Builder<_B> withAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            super.withAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "advancedProperties" property.
         * Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "advancedProperties" property.
         *     Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         */
        @Override
        public WebAdvancedProperty.Builder<? extends WebPanel.Builder<_B>> addAdvancedProperties() {
            return ((WebAdvancedProperty.Builder<? extends WebPanel.Builder<_B>> ) super.addAdvancedProperties());
        }

        /**
         * Sets the new value of "id" (any previous value will be replaced)
         * 
         * @param id
         *     New value of the "id" property.
         */
        @Override
        public WebPanel.Builder<_B> withId(final String id) {
            super.withId(id);
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        @Override
        public WebPanel.Builder<_B> withName(final String name) {
            super.withName(name);
            return this;
        }

        /**
         * Sets the new value of "column" (any previous value will be replaced)
         * 
         * @param column
         *     New value of the "column" property.
         */
        @Override
        public WebPanel.Builder<_B> withColumn(final BigInteger column) {
            super.withColumn(column);
            return this;
        }

        /**
         * Sets the new value of "row" (any previous value will be replaced)
         * 
         * @param row
         *     New value of the "row" property.
         */
        @Override
        public WebPanel.Builder<_B> withRow(final BigInteger row) {
            super.withRow(row);
            return this;
        }

        /**
         * Sets the new value of "colspan" (any previous value will be replaced)
         * 
         * @param colspan
         *     New value of the "colspan" property.
         */
        @Override
        public WebPanel.Builder<_B> withColspan(final BigInteger colspan) {
            super.withColspan(colspan);
            return this;
        }

        /**
         * Sets the new value of "rowspan" (any previous value will be replaced)
         * 
         * @param rowspan
         *     New value of the "rowspan" property.
         */
        @Override
        public WebPanel.Builder<_B> withRowspan(final BigInteger rowspan) {
            super.withRowspan(rowspan);
            return this;
        }

        /**
         * Sets the new value of "fontSize" (any previous value will be replaced)
         * 
         * @param fontSize
         *     New value of the "fontSize" property.
         */
        @Override
        public WebPanel.Builder<_B> withFontSize(final String fontSize) {
            super.withFontSize(fontSize);
            return this;
        }

        /**
         * Sets the new value of "textColor" (any previous value will be replaced)
         * 
         * @param textColor
         *     New value of the "textColor" property.
         */
        @Override
        public WebPanel.Builder<_B> withTextColor(final String textColor) {
            super.withTextColor(textColor);
            return this;
        }

        /**
         * Sets the new value of "bold" (any previous value will be replaced)
         * 
         * @param bold
         *     New value of the "bold" property.
         */
        @Override
        public WebPanel.Builder<_B> withBold(final Boolean bold) {
            super.withBold(bold);
            return this;
        }

        /**
         * Sets the new value of "italic" (any previous value will be replaced)
         * 
         * @param italic
         *     New value of the "italic" property.
         */
        @Override
        public WebPanel.Builder<_B> withItalic(final Boolean italic) {
            super.withItalic(italic);
            return this;
        }

        /**
         * Sets the new value of "underline" (any previous value will be replaced)
         * 
         * @param underline
         *     New value of the "underline" property.
         */
        @Override
        public WebPanel.Builder<_B> withUnderline(final Boolean underline) {
            super.withUnderline(underline);
            return this;
        }

        /**
         * Sets the new value of "nextFocusComponent" (any previous value will be replaced)
         * 
         * @param nextFocusComponent
         *     New value of the "nextFocusComponent" property.
         */
        @Override
        public WebPanel.Builder<_B> withNextFocusComponent(final String nextFocusComponent) {
            super.withNextFocusComponent(nextFocusComponent);
            return this;
        }

        /**
         * Sets the new value of "nextFocusField" (any previous value will be replaced)
         * 
         * @param nextFocusField
         *     New value of the "nextFocusField" property.
         */
        @Override
        public WebPanel.Builder<_B> withNextFocusField(final String nextFocusField) {
            super.withNextFocusField(nextFocusField);
            return this;
        }

        /**
         * Sets the new value of "alternativeTooltip" (any previous value will be replaced)
         * 
         * @param alternativeTooltip
         *     New value of the "alternativeTooltip" property.
         */
        @Override
        public WebPanel.Builder<_B> withAlternativeTooltip(final String alternativeTooltip) {
            super.withAlternativeTooltip(alternativeTooltip);
            return this;
        }

        @Override
        public WebPanel build() {
            if (_storedValue == null) {
                return this.init(new WebPanel());
            } else {
                return ((WebPanel) _storedValue);
            }
        }

        public WebPanel.Builder<_B> copyOf(final WebPanel _other) {
            _other.copyTo(this);
            return this;
        }

        public WebPanel.Builder<_B> copyOf(final WebPanel.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends WebPanel.Selector<WebPanel.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static WebPanel.Select _root() {
            return new WebPanel.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends WebContainer.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, WebPanel.Selector<TRoot, TParent>> title = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebPanel.Selector<TRoot, TParent>> borderWidth = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebPanel.Selector<TRoot, TParent>> borderColor = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.title!= null) {
                products.put("title", this.title.init());
            }
            if (this.borderWidth!= null) {
                products.put("borderWidth", this.borderWidth.init());
            }
            if (this.borderColor!= null) {
                products.put("borderColor", this.borderColor.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebPanel.Selector<TRoot, TParent>> title() {
            return ((this.title == null)?this.title = new com.kscs.util.jaxb.Selector<TRoot, WebPanel.Selector<TRoot, TParent>>(this._root, this, "title"):this.title);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebPanel.Selector<TRoot, TParent>> borderWidth() {
            return ((this.borderWidth == null)?this.borderWidth = new com.kscs.util.jaxb.Selector<TRoot, WebPanel.Selector<TRoot, TParent>>(this._root, this, "borderWidth"):this.borderWidth);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebPanel.Selector<TRoot, TParent>> borderColor() {
            return ((this.borderColor == null)?this.borderColor = new com.kscs.util.jaxb.Selector<TRoot, WebPanel.Selector<TRoot, TParent>>(this._root, this, "borderColor"):this.borderColor);
        }

    }

}
