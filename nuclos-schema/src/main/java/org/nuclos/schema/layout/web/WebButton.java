
package org.nuclos.schema.layout.web;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * A button component.
 * 
 * <p>Java class for web-button complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="web-button"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:org.nuclos.schema.layout.web}web-input-component"&gt;
 *       &lt;attribute name="label" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="icon" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="disable-during-edit" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "web-button")
@XmlSeeAlso({
    WebButtonChangeState.class,
    WebButtonExecuteRule.class,
    WebButtonGenerateObject.class,
    WebButtonHyperlink.class,
    WebButtonAddon.class,
    WebButtonDummy.class
})
public abstract class WebButton
    extends WebInputComponent
    implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "label")
    protected String label;
    @XmlAttribute(name = "icon")
    protected String icon;
    @XmlAttribute(name = "disable-during-edit")
    protected Boolean disableDuringEdit;

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the icon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIcon() {
        return icon;
    }

    /**
     * Sets the value of the icon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIcon(String value) {
        this.icon = value;
    }

    /**
     * Gets the value of the disableDuringEdit property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisableDuringEdit() {
        return disableDuringEdit;
    }

    /**
     * Sets the value of the disableDuringEdit property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisableDuringEdit(Boolean value) {
        this.disableDuringEdit = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        super.appendFields(locator, buffer, strategy);
        {
            String theLabel;
            theLabel = this.getLabel();
            strategy.appendField(locator, this, "label", buffer, theLabel);
        }
        {
            String theIcon;
            theIcon = this.getIcon();
            strategy.appendField(locator, this, "icon", buffer, theIcon);
        }
        {
            Boolean theDisableDuringEdit;
            theDisableDuringEdit = this.isDisableDuringEdit();
            strategy.appendField(locator, this, "disableDuringEdit", buffer, theDisableDuringEdit);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof WebButton)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final WebButton that = ((WebButton) object);
        {
            String lhsLabel;
            lhsLabel = this.getLabel();
            String rhsLabel;
            rhsLabel = that.getLabel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "label", lhsLabel), LocatorUtils.property(thatLocator, "label", rhsLabel), lhsLabel, rhsLabel)) {
                return false;
            }
        }
        {
            String lhsIcon;
            lhsIcon = this.getIcon();
            String rhsIcon;
            rhsIcon = that.getIcon();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "icon", lhsIcon), LocatorUtils.property(thatLocator, "icon", rhsIcon), lhsIcon, rhsIcon)) {
                return false;
            }
        }
        {
            Boolean lhsDisableDuringEdit;
            lhsDisableDuringEdit = this.isDisableDuringEdit();
            Boolean rhsDisableDuringEdit;
            rhsDisableDuringEdit = that.isDisableDuringEdit();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "disableDuringEdit", lhsDisableDuringEdit), LocatorUtils.property(thatLocator, "disableDuringEdit", rhsDisableDuringEdit), lhsDisableDuringEdit, rhsDisableDuringEdit)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = super.hashCode(locator, strategy);
        {
            String theLabel;
            theLabel = this.getLabel();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "label", theLabel), currentHashCode, theLabel);
        }
        {
            String theIcon;
            theIcon = this.getIcon();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "icon", theIcon), currentHashCode, theIcon);
        }
        {
            Boolean theDisableDuringEdit;
            theDisableDuringEdit = this.isDisableDuringEdit();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "disableDuringEdit", theDisableDuringEdit), currentHashCode, theDisableDuringEdit);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        if (null == target) {
            throw new IllegalArgumentException("Target argument must not be null for abstract copyable classes.");
        }
        super.copyTo(locator, target, strategy);
        if (target instanceof WebButton) {
            final WebButton copy = ((WebButton) target);
            if (this.label!= null) {
                String sourceLabel;
                sourceLabel = this.getLabel();
                String copyLabel = ((String) strategy.copy(LocatorUtils.property(locator, "label", sourceLabel), sourceLabel));
                copy.setLabel(copyLabel);
            } else {
                copy.label = null;
            }
            if (this.icon!= null) {
                String sourceIcon;
                sourceIcon = this.getIcon();
                String copyIcon = ((String) strategy.copy(LocatorUtils.property(locator, "icon", sourceIcon), sourceIcon));
                copy.setIcon(copyIcon);
            } else {
                copy.icon = null;
            }
            if (this.disableDuringEdit!= null) {
                Boolean sourceDisableDuringEdit;
                sourceDisableDuringEdit = this.isDisableDuringEdit();
                Boolean copyDisableDuringEdit = ((Boolean) strategy.copy(LocatorUtils.property(locator, "disableDuringEdit", sourceDisableDuringEdit), sourceDisableDuringEdit));
                copy.setDisableDuringEdit(copyDisableDuringEdit);
            } else {
                copy.disableDuringEdit = null;
            }
        }
        return target;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebButton.Builder<_B> _other) {
        super.copyTo(_other);
        _other.label = this.label;
        _other.icon = this.icon;
        _other.disableDuringEdit = this.disableDuringEdit;
    }

    @Override
    public abstract<_B >WebButton.Builder<_B> newCopyBuilder(final _B _parentBuilder);

    @Override
    public abstract WebButton.Builder<Void> newCopyBuilder();

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebButton.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        super.copyTo(_other, _propertyTree, _propertyTreeUse);
        final PropertyTree labelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("label"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(labelPropertyTree!= null):((labelPropertyTree == null)||(!labelPropertyTree.isLeaf())))) {
            _other.label = this.label;
        }
        final PropertyTree iconPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("icon"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(iconPropertyTree!= null):((iconPropertyTree == null)||(!iconPropertyTree.isLeaf())))) {
            _other.icon = this.icon;
        }
        final PropertyTree disableDuringEditPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("disableDuringEdit"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(disableDuringEditPropertyTree!= null):((disableDuringEditPropertyTree == null)||(!disableDuringEditPropertyTree.isLeaf())))) {
            _other.disableDuringEdit = this.disableDuringEdit;
        }
    }

    @Override
    public abstract<_B >WebButton.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse);

    @Override
    public abstract WebButton.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse);

    public static class Builder<_B >
        extends WebInputComponent.Builder<_B>
        implements Buildable
    {

        private String label;
        private String icon;
        private Boolean disableDuringEdit;

        public Builder(final _B _parentBuilder, final WebButton _other, final boolean _copy) {
            super(_parentBuilder, _other, _copy);
            if (_other!= null) {
                this.label = _other.label;
                this.icon = _other.icon;
                this.disableDuringEdit = _other.disableDuringEdit;
            }
        }

        public Builder(final _B _parentBuilder, final WebButton _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            super(_parentBuilder, _other, _copy, _propertyTree, _propertyTreeUse);
            if (_other!= null) {
                final PropertyTree labelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("label"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(labelPropertyTree!= null):((labelPropertyTree == null)||(!labelPropertyTree.isLeaf())))) {
                    this.label = _other.label;
                }
                final PropertyTree iconPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("icon"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(iconPropertyTree!= null):((iconPropertyTree == null)||(!iconPropertyTree.isLeaf())))) {
                    this.icon = _other.icon;
                }
                final PropertyTree disableDuringEditPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("disableDuringEdit"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(disableDuringEditPropertyTree!= null):((disableDuringEditPropertyTree == null)||(!disableDuringEditPropertyTree.isLeaf())))) {
                    this.disableDuringEdit = _other.disableDuringEdit;
                }
            }
        }

        protected<_P extends WebButton >_P init(final _P _product) {
            _product.label = this.label;
            _product.icon = this.icon;
            _product.disableDuringEdit = this.disableDuringEdit;
            return super.init(_product);
        }

        /**
         * Sets the new value of "label" (any previous value will be replaced)
         * 
         * @param label
         *     New value of the "label" property.
         */
        public WebButton.Builder<_B> withLabel(final String label) {
            this.label = label;
            return this;
        }

        /**
         * Sets the new value of "icon" (any previous value will be replaced)
         * 
         * @param icon
         *     New value of the "icon" property.
         */
        public WebButton.Builder<_B> withIcon(final String icon) {
            this.icon = icon;
            return this;
        }

        /**
         * Sets the new value of "disableDuringEdit" (any previous value will be replaced)
         * 
         * @param disableDuringEdit
         *     New value of the "disableDuringEdit" property.
         */
        public WebButton.Builder<_B> withDisableDuringEdit(final Boolean disableDuringEdit) {
            this.disableDuringEdit = disableDuringEdit;
            return this;
        }

        /**
         * Sets the new value of "valuelistProvider" (any previous value will be replaced)
         * 
         * @param valuelistProvider
         *     New value of the "valuelistProvider" property.
         */
        @Override
        public WebButton.Builder<_B> withValuelistProvider(final WebValuelistProvider valuelistProvider) {
            super.withValuelistProvider(valuelistProvider);
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "valuelistProvider" property.
         * Use {@link org.nuclos.schema.layout.web.WebValuelistProvider.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "valuelistProvider" property.
         *     Use {@link org.nuclos.schema.layout.web.WebValuelistProvider.Builder#end()} to return to the current builder.
         */
        public WebValuelistProvider.Builder<? extends WebButton.Builder<_B>> withValuelistProvider() {
            return ((WebValuelistProvider.Builder<? extends WebButton.Builder<_B>> ) super.withValuelistProvider());
        }

        /**
         * Sets the new value of "enabled" (any previous value will be replaced)
         * 
         * @param enabled
         *     New value of the "enabled" property.
         */
        @Override
        public WebButton.Builder<_B> withEnabled(final Boolean enabled) {
            super.withEnabled(enabled);
            return this;
        }

        /**
         * Sets the new value of "editable" (any previous value will be replaced)
         * 
         * @param editable
         *     New value of the "editable" property.
         */
        @Override
        public WebButton.Builder<_B> withEditable(final Boolean editable) {
            super.withEditable(editable);
            return this;
        }

        /**
         * Sets the new value of "insertable" (any previous value will be replaced)
         * 
         * @param insertable
         *     New value of the "insertable" property.
         */
        @Override
        public WebButton.Builder<_B> withInsertable(final Boolean insertable) {
            super.withInsertable(insertable);
            return this;
        }

        /**
         * Sets the new value of "columns" (any previous value will be replaced)
         * 
         * @param columns
         *     New value of the "columns" property.
         */
        @Override
        public WebButton.Builder<_B> withColumns(final String columns) {
            super.withColumns(columns);
            return this;
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        @Override
        public WebButton.Builder<_B> addAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            super.addAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        @Override
        public WebButton.Builder<_B> addAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            super.addAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        @Override
        public WebButton.Builder<_B> withAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            super.withAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        @Override
        public WebButton.Builder<_B> withAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            super.withAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "advancedProperties" property.
         * Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "advancedProperties" property.
         *     Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         */
        @Override
        public WebAdvancedProperty.Builder<? extends WebButton.Builder<_B>> addAdvancedProperties() {
            return ((WebAdvancedProperty.Builder<? extends WebButton.Builder<_B>> ) super.addAdvancedProperties());
        }

        /**
         * Sets the new value of "id" (any previous value will be replaced)
         * 
         * @param id
         *     New value of the "id" property.
         */
        @Override
        public WebButton.Builder<_B> withId(final String id) {
            super.withId(id);
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        @Override
        public WebButton.Builder<_B> withName(final String name) {
            super.withName(name);
            return this;
        }

        /**
         * Sets the new value of "column" (any previous value will be replaced)
         * 
         * @param column
         *     New value of the "column" property.
         */
        @Override
        public WebButton.Builder<_B> withColumn(final BigInteger column) {
            super.withColumn(column);
            return this;
        }

        /**
         * Sets the new value of "row" (any previous value will be replaced)
         * 
         * @param row
         *     New value of the "row" property.
         */
        @Override
        public WebButton.Builder<_B> withRow(final BigInteger row) {
            super.withRow(row);
            return this;
        }

        /**
         * Sets the new value of "colspan" (any previous value will be replaced)
         * 
         * @param colspan
         *     New value of the "colspan" property.
         */
        @Override
        public WebButton.Builder<_B> withColspan(final BigInteger colspan) {
            super.withColspan(colspan);
            return this;
        }

        /**
         * Sets the new value of "rowspan" (any previous value will be replaced)
         * 
         * @param rowspan
         *     New value of the "rowspan" property.
         */
        @Override
        public WebButton.Builder<_B> withRowspan(final BigInteger rowspan) {
            super.withRowspan(rowspan);
            return this;
        }

        /**
         * Sets the new value of "fontSize" (any previous value will be replaced)
         * 
         * @param fontSize
         *     New value of the "fontSize" property.
         */
        @Override
        public WebButton.Builder<_B> withFontSize(final String fontSize) {
            super.withFontSize(fontSize);
            return this;
        }

        /**
         * Sets the new value of "textColor" (any previous value will be replaced)
         * 
         * @param textColor
         *     New value of the "textColor" property.
         */
        @Override
        public WebButton.Builder<_B> withTextColor(final String textColor) {
            super.withTextColor(textColor);
            return this;
        }

        /**
         * Sets the new value of "bold" (any previous value will be replaced)
         * 
         * @param bold
         *     New value of the "bold" property.
         */
        @Override
        public WebButton.Builder<_B> withBold(final Boolean bold) {
            super.withBold(bold);
            return this;
        }

        /**
         * Sets the new value of "italic" (any previous value will be replaced)
         * 
         * @param italic
         *     New value of the "italic" property.
         */
        @Override
        public WebButton.Builder<_B> withItalic(final Boolean italic) {
            super.withItalic(italic);
            return this;
        }

        /**
         * Sets the new value of "underline" (any previous value will be replaced)
         * 
         * @param underline
         *     New value of the "underline" property.
         */
        @Override
        public WebButton.Builder<_B> withUnderline(final Boolean underline) {
            super.withUnderline(underline);
            return this;
        }

        /**
         * Sets the new value of "nextFocusComponent" (any previous value will be replaced)
         * 
         * @param nextFocusComponent
         *     New value of the "nextFocusComponent" property.
         */
        @Override
        public WebButton.Builder<_B> withNextFocusComponent(final String nextFocusComponent) {
            super.withNextFocusComponent(nextFocusComponent);
            return this;
        }

        /**
         * Sets the new value of "nextFocusField" (any previous value will be replaced)
         * 
         * @param nextFocusField
         *     New value of the "nextFocusField" property.
         */
        @Override
        public WebButton.Builder<_B> withNextFocusField(final String nextFocusField) {
            super.withNextFocusField(nextFocusField);
            return this;
        }

        /**
         * Sets the new value of "alternativeTooltip" (any previous value will be replaced)
         * 
         * @param alternativeTooltip
         *     New value of the "alternativeTooltip" property.
         */
        @Override
        public WebButton.Builder<_B> withAlternativeTooltip(final String alternativeTooltip) {
            super.withAlternativeTooltip(alternativeTooltip);
            return this;
        }

        @Override
        public WebButton build() {
            return ((WebButton) _storedValue);
        }

        public WebButton.Builder<_B> copyOf(final WebButton _other) {
            _other.copyTo(this);
            return this;
        }

        public WebButton.Builder<_B> copyOf(final WebButton.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends WebButton.Selector<WebButton.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static WebButton.Select _root() {
            return new WebButton.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends WebInputComponent.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, WebButton.Selector<TRoot, TParent>> label = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebButton.Selector<TRoot, TParent>> icon = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebButton.Selector<TRoot, TParent>> disableDuringEdit = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.label!= null) {
                products.put("label", this.label.init());
            }
            if (this.icon!= null) {
                products.put("icon", this.icon.init());
            }
            if (this.disableDuringEdit!= null) {
                products.put("disableDuringEdit", this.disableDuringEdit.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebButton.Selector<TRoot, TParent>> label() {
            return ((this.label == null)?this.label = new com.kscs.util.jaxb.Selector<TRoot, WebButton.Selector<TRoot, TParent>>(this._root, this, "label"):this.label);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebButton.Selector<TRoot, TParent>> icon() {
            return ((this.icon == null)?this.icon = new com.kscs.util.jaxb.Selector<TRoot, WebButton.Selector<TRoot, TParent>>(this._root, this, "icon"):this.icon);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebButton.Selector<TRoot, TParent>> disableDuringEdit() {
            return ((this.disableDuringEdit == null)?this.disableDuringEdit = new com.kscs.util.jaxb.Selector<TRoot, WebButton.Selector<TRoot, TParent>>(this._root, this, "disableDuringEdit"):this.disableDuringEdit);
        }

    }

}
