
package org.nuclos.schema.layout.rule;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Enables a target component.
 * 
 * <p>Java class for rule-action-enable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rule-action-enable"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:org.nuclos.schema.layout.rule}rule-action"&gt;
 *       &lt;attribute name="invertable" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rule-action-enable")
public class RuleActionEnable
    extends RuleAction
    implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "invertable")
    protected Boolean invertable;

    /**
     * Gets the value of the invertable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isInvertable() {
        return invertable;
    }

    /**
     * Sets the value of the invertable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInvertable(Boolean value) {
        this.invertable = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        super.appendFields(locator, buffer, strategy);
        {
            Boolean theInvertable;
            theInvertable = this.isInvertable();
            strategy.appendField(locator, this, "invertable", buffer, theInvertable);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof RuleActionEnable)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final RuleActionEnable that = ((RuleActionEnable) object);
        {
            Boolean lhsInvertable;
            lhsInvertable = this.isInvertable();
            Boolean rhsInvertable;
            rhsInvertable = that.isInvertable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "invertable", lhsInvertable), LocatorUtils.property(thatLocator, "invertable", rhsInvertable), lhsInvertable, rhsInvertable)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = super.hashCode(locator, strategy);
        {
            Boolean theInvertable;
            theInvertable = this.isInvertable();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "invertable", theInvertable), currentHashCode, theInvertable);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof RuleActionEnable) {
            final RuleActionEnable copy = ((RuleActionEnable) draftCopy);
            if (this.invertable!= null) {
                Boolean sourceInvertable;
                sourceInvertable = this.isInvertable();
                Boolean copyInvertable = ((Boolean) strategy.copy(LocatorUtils.property(locator, "invertable", sourceInvertable), sourceInvertable));
                copy.setInvertable(copyInvertable);
            } else {
                copy.invertable = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new RuleActionEnable();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final RuleActionEnable.Builder<_B> _other) {
        super.copyTo(_other);
        _other.invertable = this.invertable;
    }

    @Override
    public<_B >RuleActionEnable.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new RuleActionEnable.Builder<_B>(_parentBuilder, this, true);
    }

    @Override
    public RuleActionEnable.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static RuleActionEnable.Builder<Void> builder() {
        return new RuleActionEnable.Builder<Void>(null, null, false);
    }

    public static<_B >RuleActionEnable.Builder<_B> copyOf(final RuleAction _other) {
        final RuleActionEnable.Builder<_B> _newBuilder = new RuleActionEnable.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >RuleActionEnable.Builder<_B> copyOf(final RuleActionEnable _other) {
        final RuleActionEnable.Builder<_B> _newBuilder = new RuleActionEnable.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final RuleActionEnable.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        super.copyTo(_other, _propertyTree, _propertyTreeUse);
        final PropertyTree invertablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("invertable"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(invertablePropertyTree!= null):((invertablePropertyTree == null)||(!invertablePropertyTree.isLeaf())))) {
            _other.invertable = this.invertable;
        }
    }

    @Override
    public<_B >RuleActionEnable.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new RuleActionEnable.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    @Override
    public RuleActionEnable.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >RuleActionEnable.Builder<_B> copyOf(final RuleAction _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final RuleActionEnable.Builder<_B> _newBuilder = new RuleActionEnable.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >RuleActionEnable.Builder<_B> copyOf(final RuleActionEnable _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final RuleActionEnable.Builder<_B> _newBuilder = new RuleActionEnable.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static RuleActionEnable.Builder<Void> copyExcept(final RuleAction _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static RuleActionEnable.Builder<Void> copyExcept(final RuleActionEnable _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static RuleActionEnable.Builder<Void> copyOnly(final RuleAction _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static RuleActionEnable.Builder<Void> copyOnly(final RuleActionEnable _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >
        extends RuleAction.Builder<_B>
        implements Buildable
    {

        private Boolean invertable;

        public Builder(final _B _parentBuilder, final RuleActionEnable _other, final boolean _copy) {
            super(_parentBuilder, _other, _copy);
            if (_other!= null) {
                this.invertable = _other.invertable;
            }
        }

        public Builder(final _B _parentBuilder, final RuleActionEnable _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            super(_parentBuilder, _other, _copy, _propertyTree, _propertyTreeUse);
            if (_other!= null) {
                final PropertyTree invertablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("invertable"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(invertablePropertyTree!= null):((invertablePropertyTree == null)||(!invertablePropertyTree.isLeaf())))) {
                    this.invertable = _other.invertable;
                }
            }
        }

        protected<_P extends RuleActionEnable >_P init(final _P _product) {
            _product.invertable = this.invertable;
            return super.init(_product);
        }

        /**
         * Sets the new value of "invertable" (any previous value will be replaced)
         * 
         * @param invertable
         *     New value of the "invertable" property.
         */
        public RuleActionEnable.Builder<_B> withInvertable(final Boolean invertable) {
            this.invertable = invertable;
            return this;
        }

        /**
         * Sets the new value of "targetcomponent" (any previous value will be replaced)
         * 
         * @param targetcomponent
         *     New value of the "targetcomponent" property.
         */
        @Override
        public RuleActionEnable.Builder<_B> withTargetcomponent(final String targetcomponent) {
            super.withTargetcomponent(targetcomponent);
            return this;
        }

        @Override
        public RuleActionEnable build() {
            if (_storedValue == null) {
                return this.init(new RuleActionEnable());
            } else {
                return ((RuleActionEnable) _storedValue);
            }
        }

        public RuleActionEnable.Builder<_B> copyOf(final RuleActionEnable _other) {
            _other.copyTo(this);
            return this;
        }

        public RuleActionEnable.Builder<_B> copyOf(final RuleActionEnable.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends RuleActionEnable.Selector<RuleActionEnable.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static RuleActionEnable.Select _root() {
            return new RuleActionEnable.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends RuleAction.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, RuleActionEnable.Selector<TRoot, TParent>> invertable = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.invertable!= null) {
                products.put("invertable", this.invertable.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, RuleActionEnable.Selector<TRoot, TParent>> invertable() {
            return ((this.invertable == null)?this.invertable = new com.kscs.util.jaxb.Selector<TRoot, RuleActionEnable.Selector<TRoot, TParent>>(this._root, this, "invertable"):this.invertable);
        }

    }

}
