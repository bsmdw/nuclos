
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="sourcefield" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="targetcomponent" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "transfer-lookedup-value")
public class TransferLookedupValue implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "sourcefield", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String sourcefield;
    @XmlAttribute(name = "targetcomponent", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String targetcomponent;

    /**
     * Gets the value of the sourcefield property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourcefield() {
        return sourcefield;
    }

    /**
     * Sets the value of the sourcefield property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourcefield(String value) {
        this.sourcefield = value;
    }

    /**
     * Gets the value of the targetcomponent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetcomponent() {
        return targetcomponent;
    }

    /**
     * Sets the value of the targetcomponent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetcomponent(String value) {
        this.targetcomponent = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theSourcefield;
            theSourcefield = this.getSourcefield();
            strategy.appendField(locator, this, "sourcefield", buffer, theSourcefield);
        }
        {
            String theTargetcomponent;
            theTargetcomponent = this.getTargetcomponent();
            strategy.appendField(locator, this, "targetcomponent", buffer, theTargetcomponent);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof TransferLookedupValue)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final TransferLookedupValue that = ((TransferLookedupValue) object);
        {
            String lhsSourcefield;
            lhsSourcefield = this.getSourcefield();
            String rhsSourcefield;
            rhsSourcefield = that.getSourcefield();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "sourcefield", lhsSourcefield), LocatorUtils.property(thatLocator, "sourcefield", rhsSourcefield), lhsSourcefield, rhsSourcefield)) {
                return false;
            }
        }
        {
            String lhsTargetcomponent;
            lhsTargetcomponent = this.getTargetcomponent();
            String rhsTargetcomponent;
            rhsTargetcomponent = that.getTargetcomponent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "targetcomponent", lhsTargetcomponent), LocatorUtils.property(thatLocator, "targetcomponent", rhsTargetcomponent), lhsTargetcomponent, rhsTargetcomponent)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theSourcefield;
            theSourcefield = this.getSourcefield();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "sourcefield", theSourcefield), currentHashCode, theSourcefield);
        }
        {
            String theTargetcomponent;
            theTargetcomponent = this.getTargetcomponent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "targetcomponent", theTargetcomponent), currentHashCode, theTargetcomponent);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof TransferLookedupValue) {
            final TransferLookedupValue copy = ((TransferLookedupValue) draftCopy);
            if (this.sourcefield!= null) {
                String sourceSourcefield;
                sourceSourcefield = this.getSourcefield();
                String copySourcefield = ((String) strategy.copy(LocatorUtils.property(locator, "sourcefield", sourceSourcefield), sourceSourcefield));
                copy.setSourcefield(copySourcefield);
            } else {
                copy.sourcefield = null;
            }
            if (this.targetcomponent!= null) {
                String sourceTargetcomponent;
                sourceTargetcomponent = this.getTargetcomponent();
                String copyTargetcomponent = ((String) strategy.copy(LocatorUtils.property(locator, "targetcomponent", sourceTargetcomponent), sourceTargetcomponent));
                copy.setTargetcomponent(copyTargetcomponent);
            } else {
                copy.targetcomponent = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new TransferLookedupValue();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final TransferLookedupValue.Builder<_B> _other) {
        _other.sourcefield = this.sourcefield;
        _other.targetcomponent = this.targetcomponent;
    }

    public<_B >TransferLookedupValue.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new TransferLookedupValue.Builder<_B>(_parentBuilder, this, true);
    }

    public TransferLookedupValue.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static TransferLookedupValue.Builder<Void> builder() {
        return new TransferLookedupValue.Builder<Void>(null, null, false);
    }

    public static<_B >TransferLookedupValue.Builder<_B> copyOf(final TransferLookedupValue _other) {
        final TransferLookedupValue.Builder<_B> _newBuilder = new TransferLookedupValue.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final TransferLookedupValue.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree sourcefieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("sourcefield"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(sourcefieldPropertyTree!= null):((sourcefieldPropertyTree == null)||(!sourcefieldPropertyTree.isLeaf())))) {
            _other.sourcefield = this.sourcefield;
        }
        final PropertyTree targetcomponentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("targetcomponent"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(targetcomponentPropertyTree!= null):((targetcomponentPropertyTree == null)||(!targetcomponentPropertyTree.isLeaf())))) {
            _other.targetcomponent = this.targetcomponent;
        }
    }

    public<_B >TransferLookedupValue.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new TransferLookedupValue.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public TransferLookedupValue.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >TransferLookedupValue.Builder<_B> copyOf(final TransferLookedupValue _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final TransferLookedupValue.Builder<_B> _newBuilder = new TransferLookedupValue.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static TransferLookedupValue.Builder<Void> copyExcept(final TransferLookedupValue _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static TransferLookedupValue.Builder<Void> copyOnly(final TransferLookedupValue _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final TransferLookedupValue _storedValue;
        private String sourcefield;
        private String targetcomponent;

        public Builder(final _B _parentBuilder, final TransferLookedupValue _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.sourcefield = _other.sourcefield;
                    this.targetcomponent = _other.targetcomponent;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final TransferLookedupValue _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree sourcefieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("sourcefield"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(sourcefieldPropertyTree!= null):((sourcefieldPropertyTree == null)||(!sourcefieldPropertyTree.isLeaf())))) {
                        this.sourcefield = _other.sourcefield;
                    }
                    final PropertyTree targetcomponentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("targetcomponent"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(targetcomponentPropertyTree!= null):((targetcomponentPropertyTree == null)||(!targetcomponentPropertyTree.isLeaf())))) {
                        this.targetcomponent = _other.targetcomponent;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends TransferLookedupValue >_P init(final _P _product) {
            _product.sourcefield = this.sourcefield;
            _product.targetcomponent = this.targetcomponent;
            return _product;
        }

        /**
         * Sets the new value of "sourcefield" (any previous value will be replaced)
         * 
         * @param sourcefield
         *     New value of the "sourcefield" property.
         */
        public TransferLookedupValue.Builder<_B> withSourcefield(final String sourcefield) {
            this.sourcefield = sourcefield;
            return this;
        }

        /**
         * Sets the new value of "targetcomponent" (any previous value will be replaced)
         * 
         * @param targetcomponent
         *     New value of the "targetcomponent" property.
         */
        public TransferLookedupValue.Builder<_B> withTargetcomponent(final String targetcomponent) {
            this.targetcomponent = targetcomponent;
            return this;
        }

        @Override
        public TransferLookedupValue build() {
            if (_storedValue == null) {
                return this.init(new TransferLookedupValue());
            } else {
                return ((TransferLookedupValue) _storedValue);
            }
        }

        public TransferLookedupValue.Builder<_B> copyOf(final TransferLookedupValue _other) {
            _other.copyTo(this);
            return this;
        }

        public TransferLookedupValue.Builder<_B> copyOf(final TransferLookedupValue.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends TransferLookedupValue.Selector<TransferLookedupValue.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static TransferLookedupValue.Select _root() {
            return new TransferLookedupValue.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, TransferLookedupValue.Selector<TRoot, TParent>> sourcefield = null;
        private com.kscs.util.jaxb.Selector<TRoot, TransferLookedupValue.Selector<TRoot, TParent>> targetcomponent = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.sourcefield!= null) {
                products.put("sourcefield", this.sourcefield.init());
            }
            if (this.targetcomponent!= null) {
                products.put("targetcomponent", this.targetcomponent.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, TransferLookedupValue.Selector<TRoot, TParent>> sourcefield() {
            return ((this.sourcefield == null)?this.sourcefield = new com.kscs.util.jaxb.Selector<TRoot, TransferLookedupValue.Selector<TRoot, TParent>>(this._root, this, "sourcefield"):this.sourcefield);
        }

        public com.kscs.util.jaxb.Selector<TRoot, TransferLookedupValue.Selector<TRoot, TParent>> targetcomponent() {
            return ((this.targetcomponent == null)?this.targetcomponent = new com.kscs.util.jaxb.Selector<TRoot, TransferLookedupValue.Selector<TRoot, TParent>>(this._root, this, "targetcomponent"):this.targetcomponent);
        }

    }

}
