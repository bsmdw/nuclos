
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}layoutconstraints" minOccurs="0"/&gt;
 *         &lt;element ref="{}property" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;group ref="{}borders"/&gt;
 *         &lt;group ref="{}sizes"/&gt;
 *         &lt;element ref="{}background" minOccurs="0"/&gt;
 *         &lt;element ref="{}font" minOccurs="0"/&gt;
 *         &lt;element ref="{}text-format" minOccurs="0"/&gt;
 *         &lt;element ref="{}description" minOccurs="0"/&gt;
 *         &lt;element ref="{}options" minOccurs="0"/&gt;
 *         &lt;element ref="{}translations" minOccurs="0"/&gt;
 *         &lt;element ref="{}valuelist-provider" minOccurs="0"/&gt;
 *         &lt;element ref="{}text-module" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="controltype" type="{}controltype" /&gt;
 *       &lt;attribute name="controltypeclass" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="enabled" type="{}boolean" /&gt;
 *       &lt;attribute name="insertable" type="{}boolean" /&gt;
 *       &lt;attribute name="multiselect" type="{}boolean" /&gt;
 *       &lt;attribute name="lovbtn" type="{}boolean" /&gt;
 *       &lt;attribute name="lovsearch" type="{}boolean" /&gt;
 *       &lt;attribute name="dropdownbtn" type="{}boolean" /&gt;
 *       &lt;attribute name="customusagesearch" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="visible" type="{}boolean" /&gt;
 *       &lt;attribute name="notcloneable" type="{}boolean" /&gt;
 *       &lt;attribute name="multieditable" type="{}boolean" /&gt;
 *       &lt;attribute name="scalable" type="{}boolean" /&gt;
 *       &lt;attribute name="show-only"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
 *             &lt;enumeration value="label"/&gt;
 *             &lt;enumeration value="control"/&gt;
 *             &lt;enumeration value="browsebutton"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="label" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="rows" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="columns" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="mnemonic" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="opaque" type="{}boolean" /&gt;
 *       &lt;attribute name="fill-control-horizontally" type="{}boolean" /&gt;
 *       &lt;attribute name="resourceId" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="nextfocuscomponent" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="nextfocusfield" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="aspectratio" type="{}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "layoutconstraints",
    "property",
    "clearBorder",
    "border",
    "minimumSize",
    "preferredSize",
    "strictSize",
    "background",
    "font",
    "textFormat",
    "description",
    "options",
    "translations",
    "valuelistProvider",
    "textModule"
})
@XmlRootElement(name = "collectable-component")
public class CollectableComponent implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "layoutconstraints", type = JAXBElement.class, required = false)
    protected JAXBElement<?> layoutconstraints;
    protected List<Property> property;
    @XmlElement(name = "clear-border")
    protected ClearBorder clearBorder;
    @XmlElementRef(name = "border", type = JAXBElement.class, required = false)
    protected List<JAXBElement<?>> border;
    @XmlElement(name = "minimum-size")
    protected MinimumSize minimumSize;
    @XmlElement(name = "preferred-size")
    protected PreferredSize preferredSize;
    @XmlElement(name = "strict-size")
    protected StrictSize strictSize;
    protected Background background;
    protected Font font;
    @XmlElement(name = "text-format")
    protected TextFormat textFormat;
    protected String description;
    protected Options options;
    protected Translations translations;
    @XmlElement(name = "valuelist-provider")
    protected ValuelistProvider valuelistProvider;
    @XmlElement(name = "text-module")
    protected TextModule textModule;
    @XmlAttribute(name = "name", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String name;
    @XmlAttribute(name = "controltype")
    protected Controltype controltype;
    @XmlAttribute(name = "controltypeclass")
    @XmlSchemaType(name = "anySimpleType")
    protected String controltypeclass;
    @XmlAttribute(name = "enabled")
    protected Boolean enabled;
    @XmlAttribute(name = "insertable")
    protected Boolean insertable;
    @XmlAttribute(name = "multiselect")
    protected Boolean multiselect;
    @XmlAttribute(name = "lovbtn")
    protected Boolean lovbtn;
    @XmlAttribute(name = "lovsearch")
    protected Boolean lovsearch;
    @XmlAttribute(name = "dropdownbtn")
    protected Boolean dropdownbtn;
    @XmlAttribute(name = "customusagesearch")
    @XmlSchemaType(name = "anySimpleType")
    protected String customusagesearch;
    @XmlAttribute(name = "visible")
    protected Boolean visible;
    @XmlAttribute(name = "notcloneable")
    protected Boolean notcloneable;
    @XmlAttribute(name = "multieditable")
    protected Boolean multieditable;
    @XmlAttribute(name = "scalable")
    protected Boolean scalable;
    @XmlAttribute(name = "show-only")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String showOnly;
    @XmlAttribute(name = "label")
    @XmlSchemaType(name = "anySimpleType")
    protected String label;
    @XmlAttribute(name = "rows")
    @XmlSchemaType(name = "anySimpleType")
    protected String rows;
    @XmlAttribute(name = "columns")
    @XmlSchemaType(name = "anySimpleType")
    protected String columns;
    @XmlAttribute(name = "mnemonic")
    @XmlSchemaType(name = "anySimpleType")
    protected String mnemonic;
    @XmlAttribute(name = "opaque")
    protected Boolean opaque;
    @XmlAttribute(name = "fill-control-horizontally")
    protected Boolean fillControlHorizontally;
    @XmlAttribute(name = "resourceId")
    @XmlSchemaType(name = "anySimpleType")
    protected String resourceId;
    @XmlAttribute(name = "nextfocuscomponent")
    @XmlSchemaType(name = "anySimpleType")
    protected String nextfocuscomponent;
    @XmlAttribute(name = "nextfocusfield")
    @XmlSchemaType(name = "anySimpleType")
    protected String nextfocusfield;
    @XmlAttribute(name = "aspectratio")
    protected Boolean aspectratio;

    /**
     * Gets the value of the layoutconstraints property.
     * 
     * @return
     *     possible object is
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     *     
     */
    public JAXBElement<?> getLayoutconstraints() {
        return layoutconstraints;
    }

    /**
     * Sets the value of the layoutconstraints property.
     * 
     * @param value
     *     allowed object is
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     *     
     */
    public void setLayoutconstraints(JAXBElement<?> value) {
        this.layoutconstraints = value;
    }

    /**
     * Gets the value of the property property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the property property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProperty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Property }
     * 
     * 
     */
    public List<Property> getProperty() {
        if (property == null) {
            property = new ArrayList<Property>();
        }
        return this.property;
    }

    /**
     * Gets the value of the clearBorder property.
     * 
     * @return
     *     possible object is
     *     {@link ClearBorder }
     *     
     */
    public ClearBorder getClearBorder() {
        return clearBorder;
    }

    /**
     * Sets the value of the clearBorder property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClearBorder }
     *     
     */
    public void setClearBorder(ClearBorder value) {
        this.clearBorder = value;
    }

    /**
     * Gets the value of the border property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the border property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBorder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     * 
     * 
     */
    public List<JAXBElement<?>> getBorder() {
        if (border == null) {
            border = new ArrayList<JAXBElement<?>>();
        }
        return this.border;
    }

    /**
     * Gets the value of the minimumSize property.
     * 
     * @return
     *     possible object is
     *     {@link MinimumSize }
     *     
     */
    public MinimumSize getMinimumSize() {
        return minimumSize;
    }

    /**
     * Sets the value of the minimumSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link MinimumSize }
     *     
     */
    public void setMinimumSize(MinimumSize value) {
        this.minimumSize = value;
    }

    /**
     * Gets the value of the preferredSize property.
     * 
     * @return
     *     possible object is
     *     {@link PreferredSize }
     *     
     */
    public PreferredSize getPreferredSize() {
        return preferredSize;
    }

    /**
     * Sets the value of the preferredSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link PreferredSize }
     *     
     */
    public void setPreferredSize(PreferredSize value) {
        this.preferredSize = value;
    }

    /**
     * Gets the value of the strictSize property.
     * 
     * @return
     *     possible object is
     *     {@link StrictSize }
     *     
     */
    public StrictSize getStrictSize() {
        return strictSize;
    }

    /**
     * Sets the value of the strictSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link StrictSize }
     *     
     */
    public void setStrictSize(StrictSize value) {
        this.strictSize = value;
    }

    /**
     * Gets the value of the background property.
     * 
     * @return
     *     possible object is
     *     {@link Background }
     *     
     */
    public Background getBackground() {
        return background;
    }

    /**
     * Sets the value of the background property.
     * 
     * @param value
     *     allowed object is
     *     {@link Background }
     *     
     */
    public void setBackground(Background value) {
        this.background = value;
    }

    /**
     * Gets the value of the font property.
     * 
     * @return
     *     possible object is
     *     {@link Font }
     *     
     */
    public Font getFont() {
        return font;
    }

    /**
     * Sets the value of the font property.
     * 
     * @param value
     *     allowed object is
     *     {@link Font }
     *     
     */
    public void setFont(Font value) {
        this.font = value;
    }

    /**
     * Gets the value of the textFormat property.
     * 
     * @return
     *     possible object is
     *     {@link TextFormat }
     *     
     */
    public TextFormat getTextFormat() {
        return textFormat;
    }

    /**
     * Sets the value of the textFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link TextFormat }
     *     
     */
    public void setTextFormat(TextFormat value) {
        this.textFormat = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the options property.
     * 
     * @return
     *     possible object is
     *     {@link Options }
     *     
     */
    public Options getOptions() {
        return options;
    }

    /**
     * Sets the value of the options property.
     * 
     * @param value
     *     allowed object is
     *     {@link Options }
     *     
     */
    public void setOptions(Options value) {
        this.options = value;
    }

    /**
     * Gets the value of the translations property.
     * 
     * @return
     *     possible object is
     *     {@link Translations }
     *     
     */
    public Translations getTranslations() {
        return translations;
    }

    /**
     * Sets the value of the translations property.
     * 
     * @param value
     *     allowed object is
     *     {@link Translations }
     *     
     */
    public void setTranslations(Translations value) {
        this.translations = value;
    }

    /**
     * Gets the value of the valuelistProvider property.
     * 
     * @return
     *     possible object is
     *     {@link ValuelistProvider }
     *     
     */
    public ValuelistProvider getValuelistProvider() {
        return valuelistProvider;
    }

    /**
     * Sets the value of the valuelistProvider property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValuelistProvider }
     *     
     */
    public void setValuelistProvider(ValuelistProvider value) {
        this.valuelistProvider = value;
    }

    /**
     * Gets the value of the textModule property.
     * 
     * @return
     *     possible object is
     *     {@link TextModule }
     *     
     */
    public TextModule getTextModule() {
        return textModule;
    }

    /**
     * Sets the value of the textModule property.
     * 
     * @param value
     *     allowed object is
     *     {@link TextModule }
     *     
     */
    public void setTextModule(TextModule value) {
        this.textModule = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the controltype property.
     * 
     * @return
     *     possible object is
     *     {@link Controltype }
     *     
     */
    public Controltype getControltype() {
        return controltype;
    }

    /**
     * Sets the value of the controltype property.
     * 
     * @param value
     *     allowed object is
     *     {@link Controltype }
     *     
     */
    public void setControltype(Controltype value) {
        this.controltype = value;
    }

    /**
     * Gets the value of the controltypeclass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControltypeclass() {
        return controltypeclass;
    }

    /**
     * Sets the value of the controltypeclass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControltypeclass(String value) {
        this.controltypeclass = value;
    }

    /**
     * Gets the value of the enabled property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getEnabled() {
        return enabled;
    }

    /**
     * Sets the value of the enabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnabled(Boolean value) {
        this.enabled = value;
    }

    /**
     * Gets the value of the insertable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getInsertable() {
        return insertable;
    }

    /**
     * Sets the value of the insertable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInsertable(Boolean value) {
        this.insertable = value;
    }

    /**
     * Gets the value of the multiselect property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getMultiselect() {
        return multiselect;
    }

    /**
     * Sets the value of the multiselect property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMultiselect(Boolean value) {
        this.multiselect = value;
    }

    /**
     * Gets the value of the lovbtn property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getLovbtn() {
        return lovbtn;
    }

    /**
     * Sets the value of the lovbtn property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLovbtn(Boolean value) {
        this.lovbtn = value;
    }

    /**
     * Gets the value of the lovsearch property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getLovsearch() {
        return lovsearch;
    }

    /**
     * Sets the value of the lovsearch property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLovsearch(Boolean value) {
        this.lovsearch = value;
    }

    /**
     * Gets the value of the dropdownbtn property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDropdownbtn() {
        return dropdownbtn;
    }

    /**
     * Sets the value of the dropdownbtn property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDropdownbtn(Boolean value) {
        this.dropdownbtn = value;
    }

    /**
     * Gets the value of the customusagesearch property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomusagesearch() {
        return customusagesearch;
    }

    /**
     * Sets the value of the customusagesearch property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomusagesearch(String value) {
        this.customusagesearch = value;
    }

    /**
     * Gets the value of the visible property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getVisible() {
        return visible;
    }

    /**
     * Sets the value of the visible property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVisible(Boolean value) {
        this.visible = value;
    }

    /**
     * Gets the value of the notcloneable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getNotcloneable() {
        return notcloneable;
    }

    /**
     * Sets the value of the notcloneable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNotcloneable(Boolean value) {
        this.notcloneable = value;
    }

    /**
     * Gets the value of the multieditable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getMultieditable() {
        return multieditable;
    }

    /**
     * Sets the value of the multieditable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMultieditable(Boolean value) {
        this.multieditable = value;
    }

    /**
     * Gets the value of the scalable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getScalable() {
        return scalable;
    }

    /**
     * Sets the value of the scalable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setScalable(Boolean value) {
        this.scalable = value;
    }

    /**
     * Gets the value of the showOnly property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShowOnly() {
        return showOnly;
    }

    /**
     * Sets the value of the showOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShowOnly(String value) {
        this.showOnly = value;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the rows property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRows() {
        return rows;
    }

    /**
     * Sets the value of the rows property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRows(String value) {
        this.rows = value;
    }

    /**
     * Gets the value of the columns property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColumns() {
        return columns;
    }

    /**
     * Sets the value of the columns property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColumns(String value) {
        this.columns = value;
    }

    /**
     * Gets the value of the mnemonic property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMnemonic() {
        return mnemonic;
    }

    /**
     * Sets the value of the mnemonic property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMnemonic(String value) {
        this.mnemonic = value;
    }

    /**
     * Gets the value of the opaque property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOpaque() {
        return opaque;
    }

    /**
     * Sets the value of the opaque property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOpaque(Boolean value) {
        this.opaque = value;
    }

    /**
     * Gets the value of the fillControlHorizontally property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getFillControlHorizontally() {
        return fillControlHorizontally;
    }

    /**
     * Sets the value of the fillControlHorizontally property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFillControlHorizontally(Boolean value) {
        this.fillControlHorizontally = value;
    }

    /**
     * Gets the value of the resourceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceId() {
        return resourceId;
    }

    /**
     * Sets the value of the resourceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceId(String value) {
        this.resourceId = value;
    }

    /**
     * Gets the value of the nextfocuscomponent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextfocuscomponent() {
        return nextfocuscomponent;
    }

    /**
     * Sets the value of the nextfocuscomponent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextfocuscomponent(String value) {
        this.nextfocuscomponent = value;
    }

    /**
     * Gets the value of the nextfocusfield property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextfocusfield() {
        return nextfocusfield;
    }

    /**
     * Sets the value of the nextfocusfield property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextfocusfield(String value) {
        this.nextfocusfield = value;
    }

    /**
     * Gets the value of the aspectratio property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getAspectratio() {
        return aspectratio;
    }

    /**
     * Sets the value of the aspectratio property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAspectratio(Boolean value) {
        this.aspectratio = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            JAXBElement<?> theLayoutconstraints;
            theLayoutconstraints = this.getLayoutconstraints();
            strategy.appendField(locator, this, "layoutconstraints", buffer, theLayoutconstraints);
        }
        {
            List<Property> theProperty;
            theProperty = (((this.property!= null)&&(!this.property.isEmpty()))?this.getProperty():null);
            strategy.appendField(locator, this, "property", buffer, theProperty);
        }
        {
            ClearBorder theClearBorder;
            theClearBorder = this.getClearBorder();
            strategy.appendField(locator, this, "clearBorder", buffer, theClearBorder);
        }
        {
            List<JAXBElement<?>> theBorder;
            theBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            strategy.appendField(locator, this, "border", buffer, theBorder);
        }
        {
            MinimumSize theMinimumSize;
            theMinimumSize = this.getMinimumSize();
            strategy.appendField(locator, this, "minimumSize", buffer, theMinimumSize);
        }
        {
            PreferredSize thePreferredSize;
            thePreferredSize = this.getPreferredSize();
            strategy.appendField(locator, this, "preferredSize", buffer, thePreferredSize);
        }
        {
            StrictSize theStrictSize;
            theStrictSize = this.getStrictSize();
            strategy.appendField(locator, this, "strictSize", buffer, theStrictSize);
        }
        {
            Background theBackground;
            theBackground = this.getBackground();
            strategy.appendField(locator, this, "background", buffer, theBackground);
        }
        {
            Font theFont;
            theFont = this.getFont();
            strategy.appendField(locator, this, "font", buffer, theFont);
        }
        {
            TextFormat theTextFormat;
            theTextFormat = this.getTextFormat();
            strategy.appendField(locator, this, "textFormat", buffer, theTextFormat);
        }
        {
            String theDescription;
            theDescription = this.getDescription();
            strategy.appendField(locator, this, "description", buffer, theDescription);
        }
        {
            Options theOptions;
            theOptions = this.getOptions();
            strategy.appendField(locator, this, "options", buffer, theOptions);
        }
        {
            Translations theTranslations;
            theTranslations = this.getTranslations();
            strategy.appendField(locator, this, "translations", buffer, theTranslations);
        }
        {
            ValuelistProvider theValuelistProvider;
            theValuelistProvider = this.getValuelistProvider();
            strategy.appendField(locator, this, "valuelistProvider", buffer, theValuelistProvider);
        }
        {
            TextModule theTextModule;
            theTextModule = this.getTextModule();
            strategy.appendField(locator, this, "textModule", buffer, theTextModule);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            Controltype theControltype;
            theControltype = this.getControltype();
            strategy.appendField(locator, this, "controltype", buffer, theControltype);
        }
        {
            String theControltypeclass;
            theControltypeclass = this.getControltypeclass();
            strategy.appendField(locator, this, "controltypeclass", buffer, theControltypeclass);
        }
        {
            Boolean theEnabled;
            theEnabled = this.getEnabled();
            strategy.appendField(locator, this, "enabled", buffer, theEnabled);
        }
        {
            Boolean theInsertable;
            theInsertable = this.getInsertable();
            strategy.appendField(locator, this, "insertable", buffer, theInsertable);
        }
        {
            Boolean theMultiselect;
            theMultiselect = this.getMultiselect();
            strategy.appendField(locator, this, "multiselect", buffer, theMultiselect);
        }
        {
            Boolean theLovbtn;
            theLovbtn = this.getLovbtn();
            strategy.appendField(locator, this, "lovbtn", buffer, theLovbtn);
        }
        {
            Boolean theLovsearch;
            theLovsearch = this.getLovsearch();
            strategy.appendField(locator, this, "lovsearch", buffer, theLovsearch);
        }
        {
            Boolean theDropdownbtn;
            theDropdownbtn = this.getDropdownbtn();
            strategy.appendField(locator, this, "dropdownbtn", buffer, theDropdownbtn);
        }
        {
            String theCustomusagesearch;
            theCustomusagesearch = this.getCustomusagesearch();
            strategy.appendField(locator, this, "customusagesearch", buffer, theCustomusagesearch);
        }
        {
            Boolean theVisible;
            theVisible = this.getVisible();
            strategy.appendField(locator, this, "visible", buffer, theVisible);
        }
        {
            Boolean theNotcloneable;
            theNotcloneable = this.getNotcloneable();
            strategy.appendField(locator, this, "notcloneable", buffer, theNotcloneable);
        }
        {
            Boolean theMultieditable;
            theMultieditable = this.getMultieditable();
            strategy.appendField(locator, this, "multieditable", buffer, theMultieditable);
        }
        {
            Boolean theScalable;
            theScalable = this.getScalable();
            strategy.appendField(locator, this, "scalable", buffer, theScalable);
        }
        {
            String theShowOnly;
            theShowOnly = this.getShowOnly();
            strategy.appendField(locator, this, "showOnly", buffer, theShowOnly);
        }
        {
            String theLabel;
            theLabel = this.getLabel();
            strategy.appendField(locator, this, "label", buffer, theLabel);
        }
        {
            String theRows;
            theRows = this.getRows();
            strategy.appendField(locator, this, "rows", buffer, theRows);
        }
        {
            String theColumns;
            theColumns = this.getColumns();
            strategy.appendField(locator, this, "columns", buffer, theColumns);
        }
        {
            String theMnemonic;
            theMnemonic = this.getMnemonic();
            strategy.appendField(locator, this, "mnemonic", buffer, theMnemonic);
        }
        {
            Boolean theOpaque;
            theOpaque = this.getOpaque();
            strategy.appendField(locator, this, "opaque", buffer, theOpaque);
        }
        {
            Boolean theFillControlHorizontally;
            theFillControlHorizontally = this.getFillControlHorizontally();
            strategy.appendField(locator, this, "fillControlHorizontally", buffer, theFillControlHorizontally);
        }
        {
            String theResourceId;
            theResourceId = this.getResourceId();
            strategy.appendField(locator, this, "resourceId", buffer, theResourceId);
        }
        {
            String theNextfocuscomponent;
            theNextfocuscomponent = this.getNextfocuscomponent();
            strategy.appendField(locator, this, "nextfocuscomponent", buffer, theNextfocuscomponent);
        }
        {
            String theNextfocusfield;
            theNextfocusfield = this.getNextfocusfield();
            strategy.appendField(locator, this, "nextfocusfield", buffer, theNextfocusfield);
        }
        {
            Boolean theAspectratio;
            theAspectratio = this.getAspectratio();
            strategy.appendField(locator, this, "aspectratio", buffer, theAspectratio);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof CollectableComponent)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final CollectableComponent that = ((CollectableComponent) object);
        {
            JAXBElement<?> lhsLayoutconstraints;
            lhsLayoutconstraints = this.getLayoutconstraints();
            JAXBElement<?> rhsLayoutconstraints;
            rhsLayoutconstraints = that.getLayoutconstraints();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "layoutconstraints", lhsLayoutconstraints), LocatorUtils.property(thatLocator, "layoutconstraints", rhsLayoutconstraints), lhsLayoutconstraints, rhsLayoutconstraints)) {
                return false;
            }
        }
        {
            List<Property> lhsProperty;
            lhsProperty = (((this.property!= null)&&(!this.property.isEmpty()))?this.getProperty():null);
            List<Property> rhsProperty;
            rhsProperty = (((that.property!= null)&&(!that.property.isEmpty()))?that.getProperty():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "property", lhsProperty), LocatorUtils.property(thatLocator, "property", rhsProperty), lhsProperty, rhsProperty)) {
                return false;
            }
        }
        {
            ClearBorder lhsClearBorder;
            lhsClearBorder = this.getClearBorder();
            ClearBorder rhsClearBorder;
            rhsClearBorder = that.getClearBorder();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "clearBorder", lhsClearBorder), LocatorUtils.property(thatLocator, "clearBorder", rhsClearBorder), lhsClearBorder, rhsClearBorder)) {
                return false;
            }
        }
        {
            List<JAXBElement<?>> lhsBorder;
            lhsBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            List<JAXBElement<?>> rhsBorder;
            rhsBorder = (((that.border!= null)&&(!that.border.isEmpty()))?that.getBorder():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "border", lhsBorder), LocatorUtils.property(thatLocator, "border", rhsBorder), lhsBorder, rhsBorder)) {
                return false;
            }
        }
        {
            MinimumSize lhsMinimumSize;
            lhsMinimumSize = this.getMinimumSize();
            MinimumSize rhsMinimumSize;
            rhsMinimumSize = that.getMinimumSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "minimumSize", lhsMinimumSize), LocatorUtils.property(thatLocator, "minimumSize", rhsMinimumSize), lhsMinimumSize, rhsMinimumSize)) {
                return false;
            }
        }
        {
            PreferredSize lhsPreferredSize;
            lhsPreferredSize = this.getPreferredSize();
            PreferredSize rhsPreferredSize;
            rhsPreferredSize = that.getPreferredSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "preferredSize", lhsPreferredSize), LocatorUtils.property(thatLocator, "preferredSize", rhsPreferredSize), lhsPreferredSize, rhsPreferredSize)) {
                return false;
            }
        }
        {
            StrictSize lhsStrictSize;
            lhsStrictSize = this.getStrictSize();
            StrictSize rhsStrictSize;
            rhsStrictSize = that.getStrictSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "strictSize", lhsStrictSize), LocatorUtils.property(thatLocator, "strictSize", rhsStrictSize), lhsStrictSize, rhsStrictSize)) {
                return false;
            }
        }
        {
            Background lhsBackground;
            lhsBackground = this.getBackground();
            Background rhsBackground;
            rhsBackground = that.getBackground();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "background", lhsBackground), LocatorUtils.property(thatLocator, "background", rhsBackground), lhsBackground, rhsBackground)) {
                return false;
            }
        }
        {
            Font lhsFont;
            lhsFont = this.getFont();
            Font rhsFont;
            rhsFont = that.getFont();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "font", lhsFont), LocatorUtils.property(thatLocator, "font", rhsFont), lhsFont, rhsFont)) {
                return false;
            }
        }
        {
            TextFormat lhsTextFormat;
            lhsTextFormat = this.getTextFormat();
            TextFormat rhsTextFormat;
            rhsTextFormat = that.getTextFormat();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "textFormat", lhsTextFormat), LocatorUtils.property(thatLocator, "textFormat", rhsTextFormat), lhsTextFormat, rhsTextFormat)) {
                return false;
            }
        }
        {
            String lhsDescription;
            lhsDescription = this.getDescription();
            String rhsDescription;
            rhsDescription = that.getDescription();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "description", lhsDescription), LocatorUtils.property(thatLocator, "description", rhsDescription), lhsDescription, rhsDescription)) {
                return false;
            }
        }
        {
            Options lhsOptions;
            lhsOptions = this.getOptions();
            Options rhsOptions;
            rhsOptions = that.getOptions();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "options", lhsOptions), LocatorUtils.property(thatLocator, "options", rhsOptions), lhsOptions, rhsOptions)) {
                return false;
            }
        }
        {
            Translations lhsTranslations;
            lhsTranslations = this.getTranslations();
            Translations rhsTranslations;
            rhsTranslations = that.getTranslations();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "translations", lhsTranslations), LocatorUtils.property(thatLocator, "translations", rhsTranslations), lhsTranslations, rhsTranslations)) {
                return false;
            }
        }
        {
            ValuelistProvider lhsValuelistProvider;
            lhsValuelistProvider = this.getValuelistProvider();
            ValuelistProvider rhsValuelistProvider;
            rhsValuelistProvider = that.getValuelistProvider();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "valuelistProvider", lhsValuelistProvider), LocatorUtils.property(thatLocator, "valuelistProvider", rhsValuelistProvider), lhsValuelistProvider, rhsValuelistProvider)) {
                return false;
            }
        }
        {
            TextModule lhsTextModule;
            lhsTextModule = this.getTextModule();
            TextModule rhsTextModule;
            rhsTextModule = that.getTextModule();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "textModule", lhsTextModule), LocatorUtils.property(thatLocator, "textModule", rhsTextModule), lhsTextModule, rhsTextModule)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            Controltype lhsControltype;
            lhsControltype = this.getControltype();
            Controltype rhsControltype;
            rhsControltype = that.getControltype();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "controltype", lhsControltype), LocatorUtils.property(thatLocator, "controltype", rhsControltype), lhsControltype, rhsControltype)) {
                return false;
            }
        }
        {
            String lhsControltypeclass;
            lhsControltypeclass = this.getControltypeclass();
            String rhsControltypeclass;
            rhsControltypeclass = that.getControltypeclass();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "controltypeclass", lhsControltypeclass), LocatorUtils.property(thatLocator, "controltypeclass", rhsControltypeclass), lhsControltypeclass, rhsControltypeclass)) {
                return false;
            }
        }
        {
            Boolean lhsEnabled;
            lhsEnabled = this.getEnabled();
            Boolean rhsEnabled;
            rhsEnabled = that.getEnabled();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "enabled", lhsEnabled), LocatorUtils.property(thatLocator, "enabled", rhsEnabled), lhsEnabled, rhsEnabled)) {
                return false;
            }
        }
        {
            Boolean lhsInsertable;
            lhsInsertable = this.getInsertable();
            Boolean rhsInsertable;
            rhsInsertable = that.getInsertable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "insertable", lhsInsertable), LocatorUtils.property(thatLocator, "insertable", rhsInsertable), lhsInsertable, rhsInsertable)) {
                return false;
            }
        }
        {
            Boolean lhsMultiselect;
            lhsMultiselect = this.getMultiselect();
            Boolean rhsMultiselect;
            rhsMultiselect = that.getMultiselect();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "multiselect", lhsMultiselect), LocatorUtils.property(thatLocator, "multiselect", rhsMultiselect), lhsMultiselect, rhsMultiselect)) {
                return false;
            }
        }
        {
            Boolean lhsLovbtn;
            lhsLovbtn = this.getLovbtn();
            Boolean rhsLovbtn;
            rhsLovbtn = that.getLovbtn();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "lovbtn", lhsLovbtn), LocatorUtils.property(thatLocator, "lovbtn", rhsLovbtn), lhsLovbtn, rhsLovbtn)) {
                return false;
            }
        }
        {
            Boolean lhsLovsearch;
            lhsLovsearch = this.getLovsearch();
            Boolean rhsLovsearch;
            rhsLovsearch = that.getLovsearch();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "lovsearch", lhsLovsearch), LocatorUtils.property(thatLocator, "lovsearch", rhsLovsearch), lhsLovsearch, rhsLovsearch)) {
                return false;
            }
        }
        {
            Boolean lhsDropdownbtn;
            lhsDropdownbtn = this.getDropdownbtn();
            Boolean rhsDropdownbtn;
            rhsDropdownbtn = that.getDropdownbtn();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dropdownbtn", lhsDropdownbtn), LocatorUtils.property(thatLocator, "dropdownbtn", rhsDropdownbtn), lhsDropdownbtn, rhsDropdownbtn)) {
                return false;
            }
        }
        {
            String lhsCustomusagesearch;
            lhsCustomusagesearch = this.getCustomusagesearch();
            String rhsCustomusagesearch;
            rhsCustomusagesearch = that.getCustomusagesearch();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "customusagesearch", lhsCustomusagesearch), LocatorUtils.property(thatLocator, "customusagesearch", rhsCustomusagesearch), lhsCustomusagesearch, rhsCustomusagesearch)) {
                return false;
            }
        }
        {
            Boolean lhsVisible;
            lhsVisible = this.getVisible();
            Boolean rhsVisible;
            rhsVisible = that.getVisible();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "visible", lhsVisible), LocatorUtils.property(thatLocator, "visible", rhsVisible), lhsVisible, rhsVisible)) {
                return false;
            }
        }
        {
            Boolean lhsNotcloneable;
            lhsNotcloneable = this.getNotcloneable();
            Boolean rhsNotcloneable;
            rhsNotcloneable = that.getNotcloneable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "notcloneable", lhsNotcloneable), LocatorUtils.property(thatLocator, "notcloneable", rhsNotcloneable), lhsNotcloneable, rhsNotcloneable)) {
                return false;
            }
        }
        {
            Boolean lhsMultieditable;
            lhsMultieditable = this.getMultieditable();
            Boolean rhsMultieditable;
            rhsMultieditable = that.getMultieditable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "multieditable", lhsMultieditable), LocatorUtils.property(thatLocator, "multieditable", rhsMultieditable), lhsMultieditable, rhsMultieditable)) {
                return false;
            }
        }
        {
            Boolean lhsScalable;
            lhsScalable = this.getScalable();
            Boolean rhsScalable;
            rhsScalable = that.getScalable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "scalable", lhsScalable), LocatorUtils.property(thatLocator, "scalable", rhsScalable), lhsScalable, rhsScalable)) {
                return false;
            }
        }
        {
            String lhsShowOnly;
            lhsShowOnly = this.getShowOnly();
            String rhsShowOnly;
            rhsShowOnly = that.getShowOnly();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "showOnly", lhsShowOnly), LocatorUtils.property(thatLocator, "showOnly", rhsShowOnly), lhsShowOnly, rhsShowOnly)) {
                return false;
            }
        }
        {
            String lhsLabel;
            lhsLabel = this.getLabel();
            String rhsLabel;
            rhsLabel = that.getLabel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "label", lhsLabel), LocatorUtils.property(thatLocator, "label", rhsLabel), lhsLabel, rhsLabel)) {
                return false;
            }
        }
        {
            String lhsRows;
            lhsRows = this.getRows();
            String rhsRows;
            rhsRows = that.getRows();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "rows", lhsRows), LocatorUtils.property(thatLocator, "rows", rhsRows), lhsRows, rhsRows)) {
                return false;
            }
        }
        {
            String lhsColumns;
            lhsColumns = this.getColumns();
            String rhsColumns;
            rhsColumns = that.getColumns();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "columns", lhsColumns), LocatorUtils.property(thatLocator, "columns", rhsColumns), lhsColumns, rhsColumns)) {
                return false;
            }
        }
        {
            String lhsMnemonic;
            lhsMnemonic = this.getMnemonic();
            String rhsMnemonic;
            rhsMnemonic = that.getMnemonic();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "mnemonic", lhsMnemonic), LocatorUtils.property(thatLocator, "mnemonic", rhsMnemonic), lhsMnemonic, rhsMnemonic)) {
                return false;
            }
        }
        {
            Boolean lhsOpaque;
            lhsOpaque = this.getOpaque();
            Boolean rhsOpaque;
            rhsOpaque = that.getOpaque();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "opaque", lhsOpaque), LocatorUtils.property(thatLocator, "opaque", rhsOpaque), lhsOpaque, rhsOpaque)) {
                return false;
            }
        }
        {
            Boolean lhsFillControlHorizontally;
            lhsFillControlHorizontally = this.getFillControlHorizontally();
            Boolean rhsFillControlHorizontally;
            rhsFillControlHorizontally = that.getFillControlHorizontally();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fillControlHorizontally", lhsFillControlHorizontally), LocatorUtils.property(thatLocator, "fillControlHorizontally", rhsFillControlHorizontally), lhsFillControlHorizontally, rhsFillControlHorizontally)) {
                return false;
            }
        }
        {
            String lhsResourceId;
            lhsResourceId = this.getResourceId();
            String rhsResourceId;
            rhsResourceId = that.getResourceId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "resourceId", lhsResourceId), LocatorUtils.property(thatLocator, "resourceId", rhsResourceId), lhsResourceId, rhsResourceId)) {
                return false;
            }
        }
        {
            String lhsNextfocuscomponent;
            lhsNextfocuscomponent = this.getNextfocuscomponent();
            String rhsNextfocuscomponent;
            rhsNextfocuscomponent = that.getNextfocuscomponent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "nextfocuscomponent", lhsNextfocuscomponent), LocatorUtils.property(thatLocator, "nextfocuscomponent", rhsNextfocuscomponent), lhsNextfocuscomponent, rhsNextfocuscomponent)) {
                return false;
            }
        }
        {
            String lhsNextfocusfield;
            lhsNextfocusfield = this.getNextfocusfield();
            String rhsNextfocusfield;
            rhsNextfocusfield = that.getNextfocusfield();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "nextfocusfield", lhsNextfocusfield), LocatorUtils.property(thatLocator, "nextfocusfield", rhsNextfocusfield), lhsNextfocusfield, rhsNextfocusfield)) {
                return false;
            }
        }
        {
            Boolean lhsAspectratio;
            lhsAspectratio = this.getAspectratio();
            Boolean rhsAspectratio;
            rhsAspectratio = that.getAspectratio();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "aspectratio", lhsAspectratio), LocatorUtils.property(thatLocator, "aspectratio", rhsAspectratio), lhsAspectratio, rhsAspectratio)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            JAXBElement<?> theLayoutconstraints;
            theLayoutconstraints = this.getLayoutconstraints();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "layoutconstraints", theLayoutconstraints), currentHashCode, theLayoutconstraints);
        }
        {
            List<Property> theProperty;
            theProperty = (((this.property!= null)&&(!this.property.isEmpty()))?this.getProperty():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "property", theProperty), currentHashCode, theProperty);
        }
        {
            ClearBorder theClearBorder;
            theClearBorder = this.getClearBorder();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "clearBorder", theClearBorder), currentHashCode, theClearBorder);
        }
        {
            List<JAXBElement<?>> theBorder;
            theBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "border", theBorder), currentHashCode, theBorder);
        }
        {
            MinimumSize theMinimumSize;
            theMinimumSize = this.getMinimumSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "minimumSize", theMinimumSize), currentHashCode, theMinimumSize);
        }
        {
            PreferredSize thePreferredSize;
            thePreferredSize = this.getPreferredSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "preferredSize", thePreferredSize), currentHashCode, thePreferredSize);
        }
        {
            StrictSize theStrictSize;
            theStrictSize = this.getStrictSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "strictSize", theStrictSize), currentHashCode, theStrictSize);
        }
        {
            Background theBackground;
            theBackground = this.getBackground();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "background", theBackground), currentHashCode, theBackground);
        }
        {
            Font theFont;
            theFont = this.getFont();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "font", theFont), currentHashCode, theFont);
        }
        {
            TextFormat theTextFormat;
            theTextFormat = this.getTextFormat();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "textFormat", theTextFormat), currentHashCode, theTextFormat);
        }
        {
            String theDescription;
            theDescription = this.getDescription();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "description", theDescription), currentHashCode, theDescription);
        }
        {
            Options theOptions;
            theOptions = this.getOptions();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "options", theOptions), currentHashCode, theOptions);
        }
        {
            Translations theTranslations;
            theTranslations = this.getTranslations();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "translations", theTranslations), currentHashCode, theTranslations);
        }
        {
            ValuelistProvider theValuelistProvider;
            theValuelistProvider = this.getValuelistProvider();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "valuelistProvider", theValuelistProvider), currentHashCode, theValuelistProvider);
        }
        {
            TextModule theTextModule;
            theTextModule = this.getTextModule();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "textModule", theTextModule), currentHashCode, theTextModule);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            Controltype theControltype;
            theControltype = this.getControltype();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "controltype", theControltype), currentHashCode, theControltype);
        }
        {
            String theControltypeclass;
            theControltypeclass = this.getControltypeclass();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "controltypeclass", theControltypeclass), currentHashCode, theControltypeclass);
        }
        {
            Boolean theEnabled;
            theEnabled = this.getEnabled();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "enabled", theEnabled), currentHashCode, theEnabled);
        }
        {
            Boolean theInsertable;
            theInsertable = this.getInsertable();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "insertable", theInsertable), currentHashCode, theInsertable);
        }
        {
            Boolean theMultiselect;
            theMultiselect = this.getMultiselect();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "multiselect", theMultiselect), currentHashCode, theMultiselect);
        }
        {
            Boolean theLovbtn;
            theLovbtn = this.getLovbtn();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "lovbtn", theLovbtn), currentHashCode, theLovbtn);
        }
        {
            Boolean theLovsearch;
            theLovsearch = this.getLovsearch();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "lovsearch", theLovsearch), currentHashCode, theLovsearch);
        }
        {
            Boolean theDropdownbtn;
            theDropdownbtn = this.getDropdownbtn();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dropdownbtn", theDropdownbtn), currentHashCode, theDropdownbtn);
        }
        {
            String theCustomusagesearch;
            theCustomusagesearch = this.getCustomusagesearch();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "customusagesearch", theCustomusagesearch), currentHashCode, theCustomusagesearch);
        }
        {
            Boolean theVisible;
            theVisible = this.getVisible();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "visible", theVisible), currentHashCode, theVisible);
        }
        {
            Boolean theNotcloneable;
            theNotcloneable = this.getNotcloneable();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "notcloneable", theNotcloneable), currentHashCode, theNotcloneable);
        }
        {
            Boolean theMultieditable;
            theMultieditable = this.getMultieditable();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "multieditable", theMultieditable), currentHashCode, theMultieditable);
        }
        {
            Boolean theScalable;
            theScalable = this.getScalable();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "scalable", theScalable), currentHashCode, theScalable);
        }
        {
            String theShowOnly;
            theShowOnly = this.getShowOnly();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "showOnly", theShowOnly), currentHashCode, theShowOnly);
        }
        {
            String theLabel;
            theLabel = this.getLabel();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "label", theLabel), currentHashCode, theLabel);
        }
        {
            String theRows;
            theRows = this.getRows();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "rows", theRows), currentHashCode, theRows);
        }
        {
            String theColumns;
            theColumns = this.getColumns();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "columns", theColumns), currentHashCode, theColumns);
        }
        {
            String theMnemonic;
            theMnemonic = this.getMnemonic();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "mnemonic", theMnemonic), currentHashCode, theMnemonic);
        }
        {
            Boolean theOpaque;
            theOpaque = this.getOpaque();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "opaque", theOpaque), currentHashCode, theOpaque);
        }
        {
            Boolean theFillControlHorizontally;
            theFillControlHorizontally = this.getFillControlHorizontally();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fillControlHorizontally", theFillControlHorizontally), currentHashCode, theFillControlHorizontally);
        }
        {
            String theResourceId;
            theResourceId = this.getResourceId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "resourceId", theResourceId), currentHashCode, theResourceId);
        }
        {
            String theNextfocuscomponent;
            theNextfocuscomponent = this.getNextfocuscomponent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nextfocuscomponent", theNextfocuscomponent), currentHashCode, theNextfocuscomponent);
        }
        {
            String theNextfocusfield;
            theNextfocusfield = this.getNextfocusfield();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nextfocusfield", theNextfocusfield), currentHashCode, theNextfocusfield);
        }
        {
            Boolean theAspectratio;
            theAspectratio = this.getAspectratio();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "aspectratio", theAspectratio), currentHashCode, theAspectratio);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof CollectableComponent) {
            final CollectableComponent copy = ((CollectableComponent) draftCopy);
            if (this.layoutconstraints!= null) {
                JAXBElement<?> sourceLayoutconstraints;
                sourceLayoutconstraints = this.getLayoutconstraints();
                @SuppressWarnings("unchecked")
                JAXBElement<?> copyLayoutconstraints = ((JAXBElement<?> ) strategy.copy(LocatorUtils.property(locator, "layoutconstraints", sourceLayoutconstraints), sourceLayoutconstraints));
                copy.setLayoutconstraints(copyLayoutconstraints);
            } else {
                copy.layoutconstraints = null;
            }
            if ((this.property!= null)&&(!this.property.isEmpty())) {
                List<Property> sourceProperty;
                sourceProperty = (((this.property!= null)&&(!this.property.isEmpty()))?this.getProperty():null);
                @SuppressWarnings("unchecked")
                List<Property> copyProperty = ((List<Property> ) strategy.copy(LocatorUtils.property(locator, "property", sourceProperty), sourceProperty));
                copy.property = null;
                if (copyProperty!= null) {
                    List<Property> uniquePropertyl = copy.getProperty();
                    uniquePropertyl.addAll(copyProperty);
                }
            } else {
                copy.property = null;
            }
            if (this.clearBorder!= null) {
                ClearBorder sourceClearBorder;
                sourceClearBorder = this.getClearBorder();
                ClearBorder copyClearBorder = ((ClearBorder) strategy.copy(LocatorUtils.property(locator, "clearBorder", sourceClearBorder), sourceClearBorder));
                copy.setClearBorder(copyClearBorder);
            } else {
                copy.clearBorder = null;
            }
            if ((this.border!= null)&&(!this.border.isEmpty())) {
                List<JAXBElement<?>> sourceBorder;
                sourceBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
                @SuppressWarnings("unchecked")
                List<JAXBElement<?>> copyBorder = ((List<JAXBElement<?>> ) strategy.copy(LocatorUtils.property(locator, "border", sourceBorder), sourceBorder));
                copy.border = null;
                if (copyBorder!= null) {
                    List<JAXBElement<?>> uniqueBorderl = copy.getBorder();
                    uniqueBorderl.addAll(copyBorder);
                }
            } else {
                copy.border = null;
            }
            if (this.minimumSize!= null) {
                MinimumSize sourceMinimumSize;
                sourceMinimumSize = this.getMinimumSize();
                MinimumSize copyMinimumSize = ((MinimumSize) strategy.copy(LocatorUtils.property(locator, "minimumSize", sourceMinimumSize), sourceMinimumSize));
                copy.setMinimumSize(copyMinimumSize);
            } else {
                copy.minimumSize = null;
            }
            if (this.preferredSize!= null) {
                PreferredSize sourcePreferredSize;
                sourcePreferredSize = this.getPreferredSize();
                PreferredSize copyPreferredSize = ((PreferredSize) strategy.copy(LocatorUtils.property(locator, "preferredSize", sourcePreferredSize), sourcePreferredSize));
                copy.setPreferredSize(copyPreferredSize);
            } else {
                copy.preferredSize = null;
            }
            if (this.strictSize!= null) {
                StrictSize sourceStrictSize;
                sourceStrictSize = this.getStrictSize();
                StrictSize copyStrictSize = ((StrictSize) strategy.copy(LocatorUtils.property(locator, "strictSize", sourceStrictSize), sourceStrictSize));
                copy.setStrictSize(copyStrictSize);
            } else {
                copy.strictSize = null;
            }
            if (this.background!= null) {
                Background sourceBackground;
                sourceBackground = this.getBackground();
                Background copyBackground = ((Background) strategy.copy(LocatorUtils.property(locator, "background", sourceBackground), sourceBackground));
                copy.setBackground(copyBackground);
            } else {
                copy.background = null;
            }
            if (this.font!= null) {
                Font sourceFont;
                sourceFont = this.getFont();
                Font copyFont = ((Font) strategy.copy(LocatorUtils.property(locator, "font", sourceFont), sourceFont));
                copy.setFont(copyFont);
            } else {
                copy.font = null;
            }
            if (this.textFormat!= null) {
                TextFormat sourceTextFormat;
                sourceTextFormat = this.getTextFormat();
                TextFormat copyTextFormat = ((TextFormat) strategy.copy(LocatorUtils.property(locator, "textFormat", sourceTextFormat), sourceTextFormat));
                copy.setTextFormat(copyTextFormat);
            } else {
                copy.textFormat = null;
            }
            if (this.description!= null) {
                String sourceDescription;
                sourceDescription = this.getDescription();
                String copyDescription = ((String) strategy.copy(LocatorUtils.property(locator, "description", sourceDescription), sourceDescription));
                copy.setDescription(copyDescription);
            } else {
                copy.description = null;
            }
            if (this.options!= null) {
                Options sourceOptions;
                sourceOptions = this.getOptions();
                Options copyOptions = ((Options) strategy.copy(LocatorUtils.property(locator, "options", sourceOptions), sourceOptions));
                copy.setOptions(copyOptions);
            } else {
                copy.options = null;
            }
            if (this.translations!= null) {
                Translations sourceTranslations;
                sourceTranslations = this.getTranslations();
                Translations copyTranslations = ((Translations) strategy.copy(LocatorUtils.property(locator, "translations", sourceTranslations), sourceTranslations));
                copy.setTranslations(copyTranslations);
            } else {
                copy.translations = null;
            }
            if (this.valuelistProvider!= null) {
                ValuelistProvider sourceValuelistProvider;
                sourceValuelistProvider = this.getValuelistProvider();
                ValuelistProvider copyValuelistProvider = ((ValuelistProvider) strategy.copy(LocatorUtils.property(locator, "valuelistProvider", sourceValuelistProvider), sourceValuelistProvider));
                copy.setValuelistProvider(copyValuelistProvider);
            } else {
                copy.valuelistProvider = null;
            }
            if (this.textModule!= null) {
                TextModule sourceTextModule;
                sourceTextModule = this.getTextModule();
                TextModule copyTextModule = ((TextModule) strategy.copy(LocatorUtils.property(locator, "textModule", sourceTextModule), sourceTextModule));
                copy.setTextModule(copyTextModule);
            } else {
                copy.textModule = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.controltype!= null) {
                Controltype sourceControltype;
                sourceControltype = this.getControltype();
                Controltype copyControltype = ((Controltype) strategy.copy(LocatorUtils.property(locator, "controltype", sourceControltype), sourceControltype));
                copy.setControltype(copyControltype);
            } else {
                copy.controltype = null;
            }
            if (this.controltypeclass!= null) {
                String sourceControltypeclass;
                sourceControltypeclass = this.getControltypeclass();
                String copyControltypeclass = ((String) strategy.copy(LocatorUtils.property(locator, "controltypeclass", sourceControltypeclass), sourceControltypeclass));
                copy.setControltypeclass(copyControltypeclass);
            } else {
                copy.controltypeclass = null;
            }
            if (this.enabled!= null) {
                Boolean sourceEnabled;
                sourceEnabled = this.getEnabled();
                Boolean copyEnabled = ((Boolean) strategy.copy(LocatorUtils.property(locator, "enabled", sourceEnabled), sourceEnabled));
                copy.setEnabled(copyEnabled);
            } else {
                copy.enabled = null;
            }
            if (this.insertable!= null) {
                Boolean sourceInsertable;
                sourceInsertable = this.getInsertable();
                Boolean copyInsertable = ((Boolean) strategy.copy(LocatorUtils.property(locator, "insertable", sourceInsertable), sourceInsertable));
                copy.setInsertable(copyInsertable);
            } else {
                copy.insertable = null;
            }
            if (this.multiselect!= null) {
                Boolean sourceMultiselect;
                sourceMultiselect = this.getMultiselect();
                Boolean copyMultiselect = ((Boolean) strategy.copy(LocatorUtils.property(locator, "multiselect", sourceMultiselect), sourceMultiselect));
                copy.setMultiselect(copyMultiselect);
            } else {
                copy.multiselect = null;
            }
            if (this.lovbtn!= null) {
                Boolean sourceLovbtn;
                sourceLovbtn = this.getLovbtn();
                Boolean copyLovbtn = ((Boolean) strategy.copy(LocatorUtils.property(locator, "lovbtn", sourceLovbtn), sourceLovbtn));
                copy.setLovbtn(copyLovbtn);
            } else {
                copy.lovbtn = null;
            }
            if (this.lovsearch!= null) {
                Boolean sourceLovsearch;
                sourceLovsearch = this.getLovsearch();
                Boolean copyLovsearch = ((Boolean) strategy.copy(LocatorUtils.property(locator, "lovsearch", sourceLovsearch), sourceLovsearch));
                copy.setLovsearch(copyLovsearch);
            } else {
                copy.lovsearch = null;
            }
            if (this.dropdownbtn!= null) {
                Boolean sourceDropdownbtn;
                sourceDropdownbtn = this.getDropdownbtn();
                Boolean copyDropdownbtn = ((Boolean) strategy.copy(LocatorUtils.property(locator, "dropdownbtn", sourceDropdownbtn), sourceDropdownbtn));
                copy.setDropdownbtn(copyDropdownbtn);
            } else {
                copy.dropdownbtn = null;
            }
            if (this.customusagesearch!= null) {
                String sourceCustomusagesearch;
                sourceCustomusagesearch = this.getCustomusagesearch();
                String copyCustomusagesearch = ((String) strategy.copy(LocatorUtils.property(locator, "customusagesearch", sourceCustomusagesearch), sourceCustomusagesearch));
                copy.setCustomusagesearch(copyCustomusagesearch);
            } else {
                copy.customusagesearch = null;
            }
            if (this.visible!= null) {
                Boolean sourceVisible;
                sourceVisible = this.getVisible();
                Boolean copyVisible = ((Boolean) strategy.copy(LocatorUtils.property(locator, "visible", sourceVisible), sourceVisible));
                copy.setVisible(copyVisible);
            } else {
                copy.visible = null;
            }
            if (this.notcloneable!= null) {
                Boolean sourceNotcloneable;
                sourceNotcloneable = this.getNotcloneable();
                Boolean copyNotcloneable = ((Boolean) strategy.copy(LocatorUtils.property(locator, "notcloneable", sourceNotcloneable), sourceNotcloneable));
                copy.setNotcloneable(copyNotcloneable);
            } else {
                copy.notcloneable = null;
            }
            if (this.multieditable!= null) {
                Boolean sourceMultieditable;
                sourceMultieditable = this.getMultieditable();
                Boolean copyMultieditable = ((Boolean) strategy.copy(LocatorUtils.property(locator, "multieditable", sourceMultieditable), sourceMultieditable));
                copy.setMultieditable(copyMultieditable);
            } else {
                copy.multieditable = null;
            }
            if (this.scalable!= null) {
                Boolean sourceScalable;
                sourceScalable = this.getScalable();
                Boolean copyScalable = ((Boolean) strategy.copy(LocatorUtils.property(locator, "scalable", sourceScalable), sourceScalable));
                copy.setScalable(copyScalable);
            } else {
                copy.scalable = null;
            }
            if (this.showOnly!= null) {
                String sourceShowOnly;
                sourceShowOnly = this.getShowOnly();
                String copyShowOnly = ((String) strategy.copy(LocatorUtils.property(locator, "showOnly", sourceShowOnly), sourceShowOnly));
                copy.setShowOnly(copyShowOnly);
            } else {
                copy.showOnly = null;
            }
            if (this.label!= null) {
                String sourceLabel;
                sourceLabel = this.getLabel();
                String copyLabel = ((String) strategy.copy(LocatorUtils.property(locator, "label", sourceLabel), sourceLabel));
                copy.setLabel(copyLabel);
            } else {
                copy.label = null;
            }
            if (this.rows!= null) {
                String sourceRows;
                sourceRows = this.getRows();
                String copyRows = ((String) strategy.copy(LocatorUtils.property(locator, "rows", sourceRows), sourceRows));
                copy.setRows(copyRows);
            } else {
                copy.rows = null;
            }
            if (this.columns!= null) {
                String sourceColumns;
                sourceColumns = this.getColumns();
                String copyColumns = ((String) strategy.copy(LocatorUtils.property(locator, "columns", sourceColumns), sourceColumns));
                copy.setColumns(copyColumns);
            } else {
                copy.columns = null;
            }
            if (this.mnemonic!= null) {
                String sourceMnemonic;
                sourceMnemonic = this.getMnemonic();
                String copyMnemonic = ((String) strategy.copy(LocatorUtils.property(locator, "mnemonic", sourceMnemonic), sourceMnemonic));
                copy.setMnemonic(copyMnemonic);
            } else {
                copy.mnemonic = null;
            }
            if (this.opaque!= null) {
                Boolean sourceOpaque;
                sourceOpaque = this.getOpaque();
                Boolean copyOpaque = ((Boolean) strategy.copy(LocatorUtils.property(locator, "opaque", sourceOpaque), sourceOpaque));
                copy.setOpaque(copyOpaque);
            } else {
                copy.opaque = null;
            }
            if (this.fillControlHorizontally!= null) {
                Boolean sourceFillControlHorizontally;
                sourceFillControlHorizontally = this.getFillControlHorizontally();
                Boolean copyFillControlHorizontally = ((Boolean) strategy.copy(LocatorUtils.property(locator, "fillControlHorizontally", sourceFillControlHorizontally), sourceFillControlHorizontally));
                copy.setFillControlHorizontally(copyFillControlHorizontally);
            } else {
                copy.fillControlHorizontally = null;
            }
            if (this.resourceId!= null) {
                String sourceResourceId;
                sourceResourceId = this.getResourceId();
                String copyResourceId = ((String) strategy.copy(LocatorUtils.property(locator, "resourceId", sourceResourceId), sourceResourceId));
                copy.setResourceId(copyResourceId);
            } else {
                copy.resourceId = null;
            }
            if (this.nextfocuscomponent!= null) {
                String sourceNextfocuscomponent;
                sourceNextfocuscomponent = this.getNextfocuscomponent();
                String copyNextfocuscomponent = ((String) strategy.copy(LocatorUtils.property(locator, "nextfocuscomponent", sourceNextfocuscomponent), sourceNextfocuscomponent));
                copy.setNextfocuscomponent(copyNextfocuscomponent);
            } else {
                copy.nextfocuscomponent = null;
            }
            if (this.nextfocusfield!= null) {
                String sourceNextfocusfield;
                sourceNextfocusfield = this.getNextfocusfield();
                String copyNextfocusfield = ((String) strategy.copy(LocatorUtils.property(locator, "nextfocusfield", sourceNextfocusfield), sourceNextfocusfield));
                copy.setNextfocusfield(copyNextfocusfield);
            } else {
                copy.nextfocusfield = null;
            }
            if (this.aspectratio!= null) {
                Boolean sourceAspectratio;
                sourceAspectratio = this.getAspectratio();
                Boolean copyAspectratio = ((Boolean) strategy.copy(LocatorUtils.property(locator, "aspectratio", sourceAspectratio), sourceAspectratio));
                copy.setAspectratio(copyAspectratio);
            } else {
                copy.aspectratio = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new CollectableComponent();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final CollectableComponent.Builder<_B> _other) {
        _other.layoutconstraints = this.layoutconstraints;
        if (this.property == null) {
            _other.property = null;
        } else {
            _other.property = new ArrayList<Property.Builder<CollectableComponent.Builder<_B>>>();
            for (Property _item: this.property) {
                _other.property.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.newCopyBuilder(_other));
        if (this.border == null) {
            _other.border = null;
        } else {
            _other.border = new ArrayList<Buildable>();
            for (JAXBElement<?> _item: this.border) {
                _other.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
            }
        }
        _other.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.newCopyBuilder(_other));
        _other.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.newCopyBuilder(_other));
        _other.strictSize = ((this.strictSize == null)?null:this.strictSize.newCopyBuilder(_other));
        _other.background = ((this.background == null)?null:this.background.newCopyBuilder(_other));
        _other.font = ((this.font == null)?null:this.font.newCopyBuilder(_other));
        _other.textFormat = ((this.textFormat == null)?null:this.textFormat.newCopyBuilder(_other));
        _other.description = this.description;
        _other.options = ((this.options == null)?null:this.options.newCopyBuilder(_other));
        _other.translations = ((this.translations == null)?null:this.translations.newCopyBuilder(_other));
        _other.valuelistProvider = ((this.valuelistProvider == null)?null:this.valuelistProvider.newCopyBuilder(_other));
        _other.textModule = ((this.textModule == null)?null:this.textModule.newCopyBuilder(_other));
        _other.name = this.name;
        _other.controltype = this.controltype;
        _other.controltypeclass = this.controltypeclass;
        _other.enabled = this.enabled;
        _other.insertable = this.insertable;
        _other.multiselect = this.multiselect;
        _other.lovbtn = this.lovbtn;
        _other.lovsearch = this.lovsearch;
        _other.dropdownbtn = this.dropdownbtn;
        _other.customusagesearch = this.customusagesearch;
        _other.visible = this.visible;
        _other.notcloneable = this.notcloneable;
        _other.multieditable = this.multieditable;
        _other.scalable = this.scalable;
        _other.showOnly = this.showOnly;
        _other.label = this.label;
        _other.rows = this.rows;
        _other.columns = this.columns;
        _other.mnemonic = this.mnemonic;
        _other.opaque = this.opaque;
        _other.fillControlHorizontally = this.fillControlHorizontally;
        _other.resourceId = this.resourceId;
        _other.nextfocuscomponent = this.nextfocuscomponent;
        _other.nextfocusfield = this.nextfocusfield;
        _other.aspectratio = this.aspectratio;
    }

    public<_B >CollectableComponent.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new CollectableComponent.Builder<_B>(_parentBuilder, this, true);
    }

    public CollectableComponent.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static CollectableComponent.Builder<Void> builder() {
        return new CollectableComponent.Builder<Void>(null, null, false);
    }

    public static<_B >CollectableComponent.Builder<_B> copyOf(final CollectableComponent _other) {
        final CollectableComponent.Builder<_B> _newBuilder = new CollectableComponent.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final CollectableComponent.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree layoutconstraintsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("layoutconstraints"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(layoutconstraintsPropertyTree!= null):((layoutconstraintsPropertyTree == null)||(!layoutconstraintsPropertyTree.isLeaf())))) {
            _other.layoutconstraints = this.layoutconstraints;
        }
        final PropertyTree propertyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("property"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(propertyPropertyTree!= null):((propertyPropertyTree == null)||(!propertyPropertyTree.isLeaf())))) {
            if (this.property == null) {
                _other.property = null;
            } else {
                _other.property = new ArrayList<Property.Builder<CollectableComponent.Builder<_B>>>();
                for (Property _item: this.property) {
                    _other.property.add(((_item == null)?null:_item.newCopyBuilder(_other, propertyPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree clearBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("clearBorder"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(clearBorderPropertyTree!= null):((clearBorderPropertyTree == null)||(!clearBorderPropertyTree.isLeaf())))) {
            _other.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.newCopyBuilder(_other, clearBorderPropertyTree, _propertyTreeUse));
        }
        final PropertyTree borderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("border"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(borderPropertyTree!= null):((borderPropertyTree == null)||(!borderPropertyTree.isLeaf())))) {
            if (this.border == null) {
                _other.border = null;
            } else {
                _other.border = new ArrayList<Buildable>();
                for (JAXBElement<?> _item: this.border) {
                    _other.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                }
            }
        }
        final PropertyTree minimumSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minimumSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minimumSizePropertyTree!= null):((minimumSizePropertyTree == null)||(!minimumSizePropertyTree.isLeaf())))) {
            _other.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.newCopyBuilder(_other, minimumSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree preferredSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("preferredSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(preferredSizePropertyTree!= null):((preferredSizePropertyTree == null)||(!preferredSizePropertyTree.isLeaf())))) {
            _other.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.newCopyBuilder(_other, preferredSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree strictSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("strictSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(strictSizePropertyTree!= null):((strictSizePropertyTree == null)||(!strictSizePropertyTree.isLeaf())))) {
            _other.strictSize = ((this.strictSize == null)?null:this.strictSize.newCopyBuilder(_other, strictSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree backgroundPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("background"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(backgroundPropertyTree!= null):((backgroundPropertyTree == null)||(!backgroundPropertyTree.isLeaf())))) {
            _other.background = ((this.background == null)?null:this.background.newCopyBuilder(_other, backgroundPropertyTree, _propertyTreeUse));
        }
        final PropertyTree fontPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("font"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fontPropertyTree!= null):((fontPropertyTree == null)||(!fontPropertyTree.isLeaf())))) {
            _other.font = ((this.font == null)?null:this.font.newCopyBuilder(_other, fontPropertyTree, _propertyTreeUse));
        }
        final PropertyTree textFormatPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("textFormat"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(textFormatPropertyTree!= null):((textFormatPropertyTree == null)||(!textFormatPropertyTree.isLeaf())))) {
            _other.textFormat = ((this.textFormat == null)?null:this.textFormat.newCopyBuilder(_other, textFormatPropertyTree, _propertyTreeUse));
        }
        final PropertyTree descriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("description"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(descriptionPropertyTree!= null):((descriptionPropertyTree == null)||(!descriptionPropertyTree.isLeaf())))) {
            _other.description = this.description;
        }
        final PropertyTree optionsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("options"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(optionsPropertyTree!= null):((optionsPropertyTree == null)||(!optionsPropertyTree.isLeaf())))) {
            _other.options = ((this.options == null)?null:this.options.newCopyBuilder(_other, optionsPropertyTree, _propertyTreeUse));
        }
        final PropertyTree translationsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("translations"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(translationsPropertyTree!= null):((translationsPropertyTree == null)||(!translationsPropertyTree.isLeaf())))) {
            _other.translations = ((this.translations == null)?null:this.translations.newCopyBuilder(_other, translationsPropertyTree, _propertyTreeUse));
        }
        final PropertyTree valuelistProviderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("valuelistProvider"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuelistProviderPropertyTree!= null):((valuelistProviderPropertyTree == null)||(!valuelistProviderPropertyTree.isLeaf())))) {
            _other.valuelistProvider = ((this.valuelistProvider == null)?null:this.valuelistProvider.newCopyBuilder(_other, valuelistProviderPropertyTree, _propertyTreeUse));
        }
        final PropertyTree textModulePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("textModule"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(textModulePropertyTree!= null):((textModulePropertyTree == null)||(!textModulePropertyTree.isLeaf())))) {
            _other.textModule = ((this.textModule == null)?null:this.textModule.newCopyBuilder(_other, textModulePropertyTree, _propertyTreeUse));
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree controltypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("controltype"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(controltypePropertyTree!= null):((controltypePropertyTree == null)||(!controltypePropertyTree.isLeaf())))) {
            _other.controltype = this.controltype;
        }
        final PropertyTree controltypeclassPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("controltypeclass"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(controltypeclassPropertyTree!= null):((controltypeclassPropertyTree == null)||(!controltypeclassPropertyTree.isLeaf())))) {
            _other.controltypeclass = this.controltypeclass;
        }
        final PropertyTree enabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("enabled"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(enabledPropertyTree!= null):((enabledPropertyTree == null)||(!enabledPropertyTree.isLeaf())))) {
            _other.enabled = this.enabled;
        }
        final PropertyTree insertablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("insertable"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(insertablePropertyTree!= null):((insertablePropertyTree == null)||(!insertablePropertyTree.isLeaf())))) {
            _other.insertable = this.insertable;
        }
        final PropertyTree multiselectPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("multiselect"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(multiselectPropertyTree!= null):((multiselectPropertyTree == null)||(!multiselectPropertyTree.isLeaf())))) {
            _other.multiselect = this.multiselect;
        }
        final PropertyTree lovbtnPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("lovbtn"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(lovbtnPropertyTree!= null):((lovbtnPropertyTree == null)||(!lovbtnPropertyTree.isLeaf())))) {
            _other.lovbtn = this.lovbtn;
        }
        final PropertyTree lovsearchPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("lovsearch"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(lovsearchPropertyTree!= null):((lovsearchPropertyTree == null)||(!lovsearchPropertyTree.isLeaf())))) {
            _other.lovsearch = this.lovsearch;
        }
        final PropertyTree dropdownbtnPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dropdownbtn"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dropdownbtnPropertyTree!= null):((dropdownbtnPropertyTree == null)||(!dropdownbtnPropertyTree.isLeaf())))) {
            _other.dropdownbtn = this.dropdownbtn;
        }
        final PropertyTree customusagesearchPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("customusagesearch"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(customusagesearchPropertyTree!= null):((customusagesearchPropertyTree == null)||(!customusagesearchPropertyTree.isLeaf())))) {
            _other.customusagesearch = this.customusagesearch;
        }
        final PropertyTree visiblePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("visible"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(visiblePropertyTree!= null):((visiblePropertyTree == null)||(!visiblePropertyTree.isLeaf())))) {
            _other.visible = this.visible;
        }
        final PropertyTree notcloneablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("notcloneable"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(notcloneablePropertyTree!= null):((notcloneablePropertyTree == null)||(!notcloneablePropertyTree.isLeaf())))) {
            _other.notcloneable = this.notcloneable;
        }
        final PropertyTree multieditablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("multieditable"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(multieditablePropertyTree!= null):((multieditablePropertyTree == null)||(!multieditablePropertyTree.isLeaf())))) {
            _other.multieditable = this.multieditable;
        }
        final PropertyTree scalablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("scalable"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(scalablePropertyTree!= null):((scalablePropertyTree == null)||(!scalablePropertyTree.isLeaf())))) {
            _other.scalable = this.scalable;
        }
        final PropertyTree showOnlyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("showOnly"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(showOnlyPropertyTree!= null):((showOnlyPropertyTree == null)||(!showOnlyPropertyTree.isLeaf())))) {
            _other.showOnly = this.showOnly;
        }
        final PropertyTree labelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("label"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(labelPropertyTree!= null):((labelPropertyTree == null)||(!labelPropertyTree.isLeaf())))) {
            _other.label = this.label;
        }
        final PropertyTree rowsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rows"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowsPropertyTree!= null):((rowsPropertyTree == null)||(!rowsPropertyTree.isLeaf())))) {
            _other.rows = this.rows;
        }
        final PropertyTree columnsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("columns"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(columnsPropertyTree!= null):((columnsPropertyTree == null)||(!columnsPropertyTree.isLeaf())))) {
            _other.columns = this.columns;
        }
        final PropertyTree mnemonicPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("mnemonic"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(mnemonicPropertyTree!= null):((mnemonicPropertyTree == null)||(!mnemonicPropertyTree.isLeaf())))) {
            _other.mnemonic = this.mnemonic;
        }
        final PropertyTree opaquePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("opaque"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(opaquePropertyTree!= null):((opaquePropertyTree == null)||(!opaquePropertyTree.isLeaf())))) {
            _other.opaque = this.opaque;
        }
        final PropertyTree fillControlHorizontallyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fillControlHorizontally"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fillControlHorizontallyPropertyTree!= null):((fillControlHorizontallyPropertyTree == null)||(!fillControlHorizontallyPropertyTree.isLeaf())))) {
            _other.fillControlHorizontally = this.fillControlHorizontally;
        }
        final PropertyTree resourceIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resourceId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resourceIdPropertyTree!= null):((resourceIdPropertyTree == null)||(!resourceIdPropertyTree.isLeaf())))) {
            _other.resourceId = this.resourceId;
        }
        final PropertyTree nextfocuscomponentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextfocuscomponent"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextfocuscomponentPropertyTree!= null):((nextfocuscomponentPropertyTree == null)||(!nextfocuscomponentPropertyTree.isLeaf())))) {
            _other.nextfocuscomponent = this.nextfocuscomponent;
        }
        final PropertyTree nextfocusfieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextfocusfield"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextfocusfieldPropertyTree!= null):((nextfocusfieldPropertyTree == null)||(!nextfocusfieldPropertyTree.isLeaf())))) {
            _other.nextfocusfield = this.nextfocusfield;
        }
        final PropertyTree aspectratioPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("aspectratio"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(aspectratioPropertyTree!= null):((aspectratioPropertyTree == null)||(!aspectratioPropertyTree.isLeaf())))) {
            _other.aspectratio = this.aspectratio;
        }
    }

    public<_B >CollectableComponent.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new CollectableComponent.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public CollectableComponent.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >CollectableComponent.Builder<_B> copyOf(final CollectableComponent _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final CollectableComponent.Builder<_B> _newBuilder = new CollectableComponent.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static CollectableComponent.Builder<Void> copyExcept(final CollectableComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static CollectableComponent.Builder<Void> copyOnly(final CollectableComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final CollectableComponent _storedValue;
        private JAXBElement<?> layoutconstraints;
        private List<Property.Builder<CollectableComponent.Builder<_B>>> property;
        private ClearBorder.Builder<CollectableComponent.Builder<_B>> clearBorder;
        private List<Buildable> border;
        private MinimumSize.Builder<CollectableComponent.Builder<_B>> minimumSize;
        private PreferredSize.Builder<CollectableComponent.Builder<_B>> preferredSize;
        private StrictSize.Builder<CollectableComponent.Builder<_B>> strictSize;
        private Background.Builder<CollectableComponent.Builder<_B>> background;
        private Font.Builder<CollectableComponent.Builder<_B>> font;
        private TextFormat.Builder<CollectableComponent.Builder<_B>> textFormat;
        private String description;
        private Options.Builder<CollectableComponent.Builder<_B>> options;
        private Translations.Builder<CollectableComponent.Builder<_B>> translations;
        private ValuelistProvider.Builder<CollectableComponent.Builder<_B>> valuelistProvider;
        private TextModule.Builder<CollectableComponent.Builder<_B>> textModule;
        private String name;
        private Controltype controltype;
        private String controltypeclass;
        private Boolean enabled;
        private Boolean insertable;
        private Boolean multiselect;
        private Boolean lovbtn;
        private Boolean lovsearch;
        private Boolean dropdownbtn;
        private String customusagesearch;
        private Boolean visible;
        private Boolean notcloneable;
        private Boolean multieditable;
        private Boolean scalable;
        private String showOnly;
        private String label;
        private String rows;
        private String columns;
        private String mnemonic;
        private Boolean opaque;
        private Boolean fillControlHorizontally;
        private String resourceId;
        private String nextfocuscomponent;
        private String nextfocusfield;
        private Boolean aspectratio;

        public Builder(final _B _parentBuilder, final CollectableComponent _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.layoutconstraints = _other.layoutconstraints;
                    if (_other.property == null) {
                        this.property = null;
                    } else {
                        this.property = new ArrayList<Property.Builder<CollectableComponent.Builder<_B>>>();
                        for (Property _item: _other.property) {
                            this.property.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.clearBorder = ((_other.clearBorder == null)?null:_other.clearBorder.newCopyBuilder(this));
                    if (_other.border == null) {
                        this.border = null;
                    } else {
                        this.border = new ArrayList<Buildable>();
                        for (JAXBElement<?> _item: _other.border) {
                            this.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                        }
                    }
                    this.minimumSize = ((_other.minimumSize == null)?null:_other.minimumSize.newCopyBuilder(this));
                    this.preferredSize = ((_other.preferredSize == null)?null:_other.preferredSize.newCopyBuilder(this));
                    this.strictSize = ((_other.strictSize == null)?null:_other.strictSize.newCopyBuilder(this));
                    this.background = ((_other.background == null)?null:_other.background.newCopyBuilder(this));
                    this.font = ((_other.font == null)?null:_other.font.newCopyBuilder(this));
                    this.textFormat = ((_other.textFormat == null)?null:_other.textFormat.newCopyBuilder(this));
                    this.description = _other.description;
                    this.options = ((_other.options == null)?null:_other.options.newCopyBuilder(this));
                    this.translations = ((_other.translations == null)?null:_other.translations.newCopyBuilder(this));
                    this.valuelistProvider = ((_other.valuelistProvider == null)?null:_other.valuelistProvider.newCopyBuilder(this));
                    this.textModule = ((_other.textModule == null)?null:_other.textModule.newCopyBuilder(this));
                    this.name = _other.name;
                    this.controltype = _other.controltype;
                    this.controltypeclass = _other.controltypeclass;
                    this.enabled = _other.enabled;
                    this.insertable = _other.insertable;
                    this.multiselect = _other.multiselect;
                    this.lovbtn = _other.lovbtn;
                    this.lovsearch = _other.lovsearch;
                    this.dropdownbtn = _other.dropdownbtn;
                    this.customusagesearch = _other.customusagesearch;
                    this.visible = _other.visible;
                    this.notcloneable = _other.notcloneable;
                    this.multieditable = _other.multieditable;
                    this.scalable = _other.scalable;
                    this.showOnly = _other.showOnly;
                    this.label = _other.label;
                    this.rows = _other.rows;
                    this.columns = _other.columns;
                    this.mnemonic = _other.mnemonic;
                    this.opaque = _other.opaque;
                    this.fillControlHorizontally = _other.fillControlHorizontally;
                    this.resourceId = _other.resourceId;
                    this.nextfocuscomponent = _other.nextfocuscomponent;
                    this.nextfocusfield = _other.nextfocusfield;
                    this.aspectratio = _other.aspectratio;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final CollectableComponent _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree layoutconstraintsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("layoutconstraints"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(layoutconstraintsPropertyTree!= null):((layoutconstraintsPropertyTree == null)||(!layoutconstraintsPropertyTree.isLeaf())))) {
                        this.layoutconstraints = _other.layoutconstraints;
                    }
                    final PropertyTree propertyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("property"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(propertyPropertyTree!= null):((propertyPropertyTree == null)||(!propertyPropertyTree.isLeaf())))) {
                        if (_other.property == null) {
                            this.property = null;
                        } else {
                            this.property = new ArrayList<Property.Builder<CollectableComponent.Builder<_B>>>();
                            for (Property _item: _other.property) {
                                this.property.add(((_item == null)?null:_item.newCopyBuilder(this, propertyPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree clearBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("clearBorder"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(clearBorderPropertyTree!= null):((clearBorderPropertyTree == null)||(!clearBorderPropertyTree.isLeaf())))) {
                        this.clearBorder = ((_other.clearBorder == null)?null:_other.clearBorder.newCopyBuilder(this, clearBorderPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree borderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("border"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(borderPropertyTree!= null):((borderPropertyTree == null)||(!borderPropertyTree.isLeaf())))) {
                        if (_other.border == null) {
                            this.border = null;
                        } else {
                            this.border = new ArrayList<Buildable>();
                            for (JAXBElement<?> _item: _other.border) {
                                this.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                            }
                        }
                    }
                    final PropertyTree minimumSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minimumSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minimumSizePropertyTree!= null):((minimumSizePropertyTree == null)||(!minimumSizePropertyTree.isLeaf())))) {
                        this.minimumSize = ((_other.minimumSize == null)?null:_other.minimumSize.newCopyBuilder(this, minimumSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree preferredSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("preferredSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(preferredSizePropertyTree!= null):((preferredSizePropertyTree == null)||(!preferredSizePropertyTree.isLeaf())))) {
                        this.preferredSize = ((_other.preferredSize == null)?null:_other.preferredSize.newCopyBuilder(this, preferredSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree strictSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("strictSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(strictSizePropertyTree!= null):((strictSizePropertyTree == null)||(!strictSizePropertyTree.isLeaf())))) {
                        this.strictSize = ((_other.strictSize == null)?null:_other.strictSize.newCopyBuilder(this, strictSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree backgroundPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("background"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(backgroundPropertyTree!= null):((backgroundPropertyTree == null)||(!backgroundPropertyTree.isLeaf())))) {
                        this.background = ((_other.background == null)?null:_other.background.newCopyBuilder(this, backgroundPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree fontPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("font"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fontPropertyTree!= null):((fontPropertyTree == null)||(!fontPropertyTree.isLeaf())))) {
                        this.font = ((_other.font == null)?null:_other.font.newCopyBuilder(this, fontPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree textFormatPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("textFormat"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(textFormatPropertyTree!= null):((textFormatPropertyTree == null)||(!textFormatPropertyTree.isLeaf())))) {
                        this.textFormat = ((_other.textFormat == null)?null:_other.textFormat.newCopyBuilder(this, textFormatPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree descriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("description"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(descriptionPropertyTree!= null):((descriptionPropertyTree == null)||(!descriptionPropertyTree.isLeaf())))) {
                        this.description = _other.description;
                    }
                    final PropertyTree optionsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("options"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(optionsPropertyTree!= null):((optionsPropertyTree == null)||(!optionsPropertyTree.isLeaf())))) {
                        this.options = ((_other.options == null)?null:_other.options.newCopyBuilder(this, optionsPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree translationsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("translations"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(translationsPropertyTree!= null):((translationsPropertyTree == null)||(!translationsPropertyTree.isLeaf())))) {
                        this.translations = ((_other.translations == null)?null:_other.translations.newCopyBuilder(this, translationsPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree valuelistProviderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("valuelistProvider"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuelistProviderPropertyTree!= null):((valuelistProviderPropertyTree == null)||(!valuelistProviderPropertyTree.isLeaf())))) {
                        this.valuelistProvider = ((_other.valuelistProvider == null)?null:_other.valuelistProvider.newCopyBuilder(this, valuelistProviderPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree textModulePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("textModule"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(textModulePropertyTree!= null):((textModulePropertyTree == null)||(!textModulePropertyTree.isLeaf())))) {
                        this.textModule = ((_other.textModule == null)?null:_other.textModule.newCopyBuilder(this, textModulePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree controltypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("controltype"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(controltypePropertyTree!= null):((controltypePropertyTree == null)||(!controltypePropertyTree.isLeaf())))) {
                        this.controltype = _other.controltype;
                    }
                    final PropertyTree controltypeclassPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("controltypeclass"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(controltypeclassPropertyTree!= null):((controltypeclassPropertyTree == null)||(!controltypeclassPropertyTree.isLeaf())))) {
                        this.controltypeclass = _other.controltypeclass;
                    }
                    final PropertyTree enabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("enabled"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(enabledPropertyTree!= null):((enabledPropertyTree == null)||(!enabledPropertyTree.isLeaf())))) {
                        this.enabled = _other.enabled;
                    }
                    final PropertyTree insertablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("insertable"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(insertablePropertyTree!= null):((insertablePropertyTree == null)||(!insertablePropertyTree.isLeaf())))) {
                        this.insertable = _other.insertable;
                    }
                    final PropertyTree multiselectPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("multiselect"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(multiselectPropertyTree!= null):((multiselectPropertyTree == null)||(!multiselectPropertyTree.isLeaf())))) {
                        this.multiselect = _other.multiselect;
                    }
                    final PropertyTree lovbtnPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("lovbtn"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(lovbtnPropertyTree!= null):((lovbtnPropertyTree == null)||(!lovbtnPropertyTree.isLeaf())))) {
                        this.lovbtn = _other.lovbtn;
                    }
                    final PropertyTree lovsearchPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("lovsearch"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(lovsearchPropertyTree!= null):((lovsearchPropertyTree == null)||(!lovsearchPropertyTree.isLeaf())))) {
                        this.lovsearch = _other.lovsearch;
                    }
                    final PropertyTree dropdownbtnPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dropdownbtn"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dropdownbtnPropertyTree!= null):((dropdownbtnPropertyTree == null)||(!dropdownbtnPropertyTree.isLeaf())))) {
                        this.dropdownbtn = _other.dropdownbtn;
                    }
                    final PropertyTree customusagesearchPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("customusagesearch"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(customusagesearchPropertyTree!= null):((customusagesearchPropertyTree == null)||(!customusagesearchPropertyTree.isLeaf())))) {
                        this.customusagesearch = _other.customusagesearch;
                    }
                    final PropertyTree visiblePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("visible"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(visiblePropertyTree!= null):((visiblePropertyTree == null)||(!visiblePropertyTree.isLeaf())))) {
                        this.visible = _other.visible;
                    }
                    final PropertyTree notcloneablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("notcloneable"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(notcloneablePropertyTree!= null):((notcloneablePropertyTree == null)||(!notcloneablePropertyTree.isLeaf())))) {
                        this.notcloneable = _other.notcloneable;
                    }
                    final PropertyTree multieditablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("multieditable"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(multieditablePropertyTree!= null):((multieditablePropertyTree == null)||(!multieditablePropertyTree.isLeaf())))) {
                        this.multieditable = _other.multieditable;
                    }
                    final PropertyTree scalablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("scalable"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(scalablePropertyTree!= null):((scalablePropertyTree == null)||(!scalablePropertyTree.isLeaf())))) {
                        this.scalable = _other.scalable;
                    }
                    final PropertyTree showOnlyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("showOnly"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(showOnlyPropertyTree!= null):((showOnlyPropertyTree == null)||(!showOnlyPropertyTree.isLeaf())))) {
                        this.showOnly = _other.showOnly;
                    }
                    final PropertyTree labelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("label"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(labelPropertyTree!= null):((labelPropertyTree == null)||(!labelPropertyTree.isLeaf())))) {
                        this.label = _other.label;
                    }
                    final PropertyTree rowsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rows"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowsPropertyTree!= null):((rowsPropertyTree == null)||(!rowsPropertyTree.isLeaf())))) {
                        this.rows = _other.rows;
                    }
                    final PropertyTree columnsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("columns"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(columnsPropertyTree!= null):((columnsPropertyTree == null)||(!columnsPropertyTree.isLeaf())))) {
                        this.columns = _other.columns;
                    }
                    final PropertyTree mnemonicPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("mnemonic"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(mnemonicPropertyTree!= null):((mnemonicPropertyTree == null)||(!mnemonicPropertyTree.isLeaf())))) {
                        this.mnemonic = _other.mnemonic;
                    }
                    final PropertyTree opaquePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("opaque"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(opaquePropertyTree!= null):((opaquePropertyTree == null)||(!opaquePropertyTree.isLeaf())))) {
                        this.opaque = _other.opaque;
                    }
                    final PropertyTree fillControlHorizontallyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fillControlHorizontally"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fillControlHorizontallyPropertyTree!= null):((fillControlHorizontallyPropertyTree == null)||(!fillControlHorizontallyPropertyTree.isLeaf())))) {
                        this.fillControlHorizontally = _other.fillControlHorizontally;
                    }
                    final PropertyTree resourceIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resourceId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resourceIdPropertyTree!= null):((resourceIdPropertyTree == null)||(!resourceIdPropertyTree.isLeaf())))) {
                        this.resourceId = _other.resourceId;
                    }
                    final PropertyTree nextfocuscomponentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextfocuscomponent"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextfocuscomponentPropertyTree!= null):((nextfocuscomponentPropertyTree == null)||(!nextfocuscomponentPropertyTree.isLeaf())))) {
                        this.nextfocuscomponent = _other.nextfocuscomponent;
                    }
                    final PropertyTree nextfocusfieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextfocusfield"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextfocusfieldPropertyTree!= null):((nextfocusfieldPropertyTree == null)||(!nextfocusfieldPropertyTree.isLeaf())))) {
                        this.nextfocusfield = _other.nextfocusfield;
                    }
                    final PropertyTree aspectratioPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("aspectratio"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(aspectratioPropertyTree!= null):((aspectratioPropertyTree == null)||(!aspectratioPropertyTree.isLeaf())))) {
                        this.aspectratio = _other.aspectratio;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends CollectableComponent >_P init(final _P _product) {
            _product.layoutconstraints = this.layoutconstraints;
            if (this.property!= null) {
                final List<Property> property = new ArrayList<Property>(this.property.size());
                for (Property.Builder<CollectableComponent.Builder<_B>> _item: this.property) {
                    property.add(_item.build());
                }
                _product.property = property;
            }
            _product.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.build());
            if (this.border!= null) {
                final List<JAXBElement<?>> border = new ArrayList<JAXBElement<?>>(this.border.size());
                for (Buildable _item: this.border) {
                    border.add(((JAXBElement<?> ) _item.build()));
                }
                _product.border = border;
            }
            _product.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.build());
            _product.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.build());
            _product.strictSize = ((this.strictSize == null)?null:this.strictSize.build());
            _product.background = ((this.background == null)?null:this.background.build());
            _product.font = ((this.font == null)?null:this.font.build());
            _product.textFormat = ((this.textFormat == null)?null:this.textFormat.build());
            _product.description = this.description;
            _product.options = ((this.options == null)?null:this.options.build());
            _product.translations = ((this.translations == null)?null:this.translations.build());
            _product.valuelistProvider = ((this.valuelistProvider == null)?null:this.valuelistProvider.build());
            _product.textModule = ((this.textModule == null)?null:this.textModule.build());
            _product.name = this.name;
            _product.controltype = this.controltype;
            _product.controltypeclass = this.controltypeclass;
            _product.enabled = this.enabled;
            _product.insertable = this.insertable;
            _product.multiselect = this.multiselect;
            _product.lovbtn = this.lovbtn;
            _product.lovsearch = this.lovsearch;
            _product.dropdownbtn = this.dropdownbtn;
            _product.customusagesearch = this.customusagesearch;
            _product.visible = this.visible;
            _product.notcloneable = this.notcloneable;
            _product.multieditable = this.multieditable;
            _product.scalable = this.scalable;
            _product.showOnly = this.showOnly;
            _product.label = this.label;
            _product.rows = this.rows;
            _product.columns = this.columns;
            _product.mnemonic = this.mnemonic;
            _product.opaque = this.opaque;
            _product.fillControlHorizontally = this.fillControlHorizontally;
            _product.resourceId = this.resourceId;
            _product.nextfocuscomponent = this.nextfocuscomponent;
            _product.nextfocusfield = this.nextfocusfield;
            _product.aspectratio = this.aspectratio;
            return _product;
        }

        /**
         * Sets the new value of "layoutconstraints" (any previous value will be replaced)
         * 
         * @param layoutconstraints
         *     New value of the "layoutconstraints" property.
         */
        public CollectableComponent.Builder<_B> withLayoutconstraints(final JAXBElement<?> layoutconstraints) {
            this.layoutconstraints = layoutconstraints;
            return this;
        }

        /**
         * Adds the given items to the value of "property"
         * 
         * @param property
         *     Items to add to the value of the "property" property
         */
        public CollectableComponent.Builder<_B> addProperty(final Iterable<? extends Property> property) {
            if (property!= null) {
                if (this.property == null) {
                    this.property = new ArrayList<Property.Builder<CollectableComponent.Builder<_B>>>();
                }
                for (Property _item: property) {
                    this.property.add(new Property.Builder<CollectableComponent.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "property" (any previous value will be replaced)
         * 
         * @param property
         *     New value of the "property" property.
         */
        public CollectableComponent.Builder<_B> withProperty(final Iterable<? extends Property> property) {
            if (this.property!= null) {
                this.property.clear();
            }
            return addProperty(property);
        }

        /**
         * Adds the given items to the value of "property"
         * 
         * @param property
         *     Items to add to the value of the "property" property
         */
        public CollectableComponent.Builder<_B> addProperty(Property... property) {
            addProperty(Arrays.asList(property));
            return this;
        }

        /**
         * Sets the new value of "property" (any previous value will be replaced)
         * 
         * @param property
         *     New value of the "property" property.
         */
        public CollectableComponent.Builder<_B> withProperty(Property... property) {
            withProperty(Arrays.asList(property));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Property" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Property.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Property" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Property.Builder#end()} to return to the current builder.
         */
        public Property.Builder<? extends CollectableComponent.Builder<_B>> addProperty() {
            if (this.property == null) {
                this.property = new ArrayList<Property.Builder<CollectableComponent.Builder<_B>>>();
            }
            final Property.Builder<CollectableComponent.Builder<_B>> property_Builder = new Property.Builder<CollectableComponent.Builder<_B>>(this, null, false);
            this.property.add(property_Builder);
            return property_Builder;
        }

        /**
         * Sets the new value of "clearBorder" (any previous value will be replaced)
         * 
         * @param clearBorder
         *     New value of the "clearBorder" property.
         */
        public CollectableComponent.Builder<_B> withClearBorder(final ClearBorder clearBorder) {
            this.clearBorder = ((clearBorder == null)?null:new ClearBorder.Builder<CollectableComponent.Builder<_B>>(this, clearBorder, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "clearBorder" property.
         * Use {@link org.nuclos.schema.layout.layoutml.ClearBorder.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "clearBorder" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.ClearBorder.Builder#end()} to return to the current builder.
         */
        public ClearBorder.Builder<? extends CollectableComponent.Builder<_B>> withClearBorder() {
            if (this.clearBorder!= null) {
                return this.clearBorder;
            }
            return this.clearBorder = new ClearBorder.Builder<CollectableComponent.Builder<_B>>(this, null, false);
        }

        /**
         * Adds the given items to the value of "border"
         * 
         * @param border
         *     Items to add to the value of the "border" property
         */
        public CollectableComponent.Builder<_B> addBorder(final Iterable<? extends JAXBElement<?>> border) {
            if (border!= null) {
                if (this.border == null) {
                    this.border = new ArrayList<Buildable>();
                }
                for (JAXBElement<?> _item: border) {
                    this.border.add(new Buildable.PrimitiveBuildable(_item));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "border" (any previous value will be replaced)
         * 
         * @param border
         *     New value of the "border" property.
         */
        public CollectableComponent.Builder<_B> withBorder(final Iterable<? extends JAXBElement<?>> border) {
            if (this.border!= null) {
                this.border.clear();
            }
            return addBorder(border);
        }

        /**
         * Adds the given items to the value of "border"
         * 
         * @param border
         *     Items to add to the value of the "border" property
         */
        public CollectableComponent.Builder<_B> addBorder(JAXBElement<?> ... border) {
            addBorder(Arrays.asList(border));
            return this;
        }

        /**
         * Sets the new value of "border" (any previous value will be replaced)
         * 
         * @param border
         *     New value of the "border" property.
         */
        public CollectableComponent.Builder<_B> withBorder(JAXBElement<?> ... border) {
            withBorder(Arrays.asList(border));
            return this;
        }

        /**
         * Sets the new value of "minimumSize" (any previous value will be replaced)
         * 
         * @param minimumSize
         *     New value of the "minimumSize" property.
         */
        public CollectableComponent.Builder<_B> withMinimumSize(final MinimumSize minimumSize) {
            this.minimumSize = ((minimumSize == null)?null:new MinimumSize.Builder<CollectableComponent.Builder<_B>>(this, minimumSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "minimumSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.MinimumSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "minimumSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.MinimumSize.Builder#end()} to return to the current builder.
         */
        public MinimumSize.Builder<? extends CollectableComponent.Builder<_B>> withMinimumSize() {
            if (this.minimumSize!= null) {
                return this.minimumSize;
            }
            return this.minimumSize = new MinimumSize.Builder<CollectableComponent.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "preferredSize" (any previous value will be replaced)
         * 
         * @param preferredSize
         *     New value of the "preferredSize" property.
         */
        public CollectableComponent.Builder<_B> withPreferredSize(final PreferredSize preferredSize) {
            this.preferredSize = ((preferredSize == null)?null:new PreferredSize.Builder<CollectableComponent.Builder<_B>>(this, preferredSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "preferredSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.PreferredSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "preferredSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.PreferredSize.Builder#end()} to return to the current builder.
         */
        public PreferredSize.Builder<? extends CollectableComponent.Builder<_B>> withPreferredSize() {
            if (this.preferredSize!= null) {
                return this.preferredSize;
            }
            return this.preferredSize = new PreferredSize.Builder<CollectableComponent.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "strictSize" (any previous value will be replaced)
         * 
         * @param strictSize
         *     New value of the "strictSize" property.
         */
        public CollectableComponent.Builder<_B> withStrictSize(final StrictSize strictSize) {
            this.strictSize = ((strictSize == null)?null:new StrictSize.Builder<CollectableComponent.Builder<_B>>(this, strictSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "strictSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.StrictSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "strictSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.StrictSize.Builder#end()} to return to the current builder.
         */
        public StrictSize.Builder<? extends CollectableComponent.Builder<_B>> withStrictSize() {
            if (this.strictSize!= null) {
                return this.strictSize;
            }
            return this.strictSize = new StrictSize.Builder<CollectableComponent.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "background" (any previous value will be replaced)
         * 
         * @param background
         *     New value of the "background" property.
         */
        public CollectableComponent.Builder<_B> withBackground(final Background background) {
            this.background = ((background == null)?null:new Background.Builder<CollectableComponent.Builder<_B>>(this, background, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "background" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Background.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "background" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Background.Builder#end()} to return to the current builder.
         */
        public Background.Builder<? extends CollectableComponent.Builder<_B>> withBackground() {
            if (this.background!= null) {
                return this.background;
            }
            return this.background = new Background.Builder<CollectableComponent.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "font" (any previous value will be replaced)
         * 
         * @param font
         *     New value of the "font" property.
         */
        public CollectableComponent.Builder<_B> withFont(final Font font) {
            this.font = ((font == null)?null:new Font.Builder<CollectableComponent.Builder<_B>>(this, font, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "font" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Font.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "font" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Font.Builder#end()} to return to the current builder.
         */
        public Font.Builder<? extends CollectableComponent.Builder<_B>> withFont() {
            if (this.font!= null) {
                return this.font;
            }
            return this.font = new Font.Builder<CollectableComponent.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "textFormat" (any previous value will be replaced)
         * 
         * @param textFormat
         *     New value of the "textFormat" property.
         */
        public CollectableComponent.Builder<_B> withTextFormat(final TextFormat textFormat) {
            this.textFormat = ((textFormat == null)?null:new TextFormat.Builder<CollectableComponent.Builder<_B>>(this, textFormat, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "textFormat" property.
         * Use {@link org.nuclos.schema.layout.layoutml.TextFormat.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "textFormat" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.TextFormat.Builder#end()} to return to the current builder.
         */
        public TextFormat.Builder<? extends CollectableComponent.Builder<_B>> withTextFormat() {
            if (this.textFormat!= null) {
                return this.textFormat;
            }
            return this.textFormat = new TextFormat.Builder<CollectableComponent.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "description" (any previous value will be replaced)
         * 
         * @param description
         *     New value of the "description" property.
         */
        public CollectableComponent.Builder<_B> withDescription(final String description) {
            this.description = description;
            return this;
        }

        /**
         * Sets the new value of "options" (any previous value will be replaced)
         * 
         * @param options
         *     New value of the "options" property.
         */
        public CollectableComponent.Builder<_B> withOptions(final Options options) {
            this.options = ((options == null)?null:new Options.Builder<CollectableComponent.Builder<_B>>(this, options, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "options" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Options.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "options" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Options.Builder#end()} to return to the current builder.
         */
        public Options.Builder<? extends CollectableComponent.Builder<_B>> withOptions() {
            if (this.options!= null) {
                return this.options;
            }
            return this.options = new Options.Builder<CollectableComponent.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "translations" (any previous value will be replaced)
         * 
         * @param translations
         *     New value of the "translations" property.
         */
        public CollectableComponent.Builder<_B> withTranslations(final Translations translations) {
            this.translations = ((translations == null)?null:new Translations.Builder<CollectableComponent.Builder<_B>>(this, translations, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "translations" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Translations.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "translations" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Translations.Builder#end()} to return to the current builder.
         */
        public Translations.Builder<? extends CollectableComponent.Builder<_B>> withTranslations() {
            if (this.translations!= null) {
                return this.translations;
            }
            return this.translations = new Translations.Builder<CollectableComponent.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "valuelistProvider" (any previous value will be replaced)
         * 
         * @param valuelistProvider
         *     New value of the "valuelistProvider" property.
         */
        public CollectableComponent.Builder<_B> withValuelistProvider(final ValuelistProvider valuelistProvider) {
            this.valuelistProvider = ((valuelistProvider == null)?null:new ValuelistProvider.Builder<CollectableComponent.Builder<_B>>(this, valuelistProvider, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "valuelistProvider" property.
         * Use {@link org.nuclos.schema.layout.layoutml.ValuelistProvider.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "valuelistProvider" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.ValuelistProvider.Builder#end()} to return to the current builder.
         */
        public ValuelistProvider.Builder<? extends CollectableComponent.Builder<_B>> withValuelistProvider() {
            if (this.valuelistProvider!= null) {
                return this.valuelistProvider;
            }
            return this.valuelistProvider = new ValuelistProvider.Builder<CollectableComponent.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "textModule" (any previous value will be replaced)
         * 
         * @param textModule
         *     New value of the "textModule" property.
         */
        public CollectableComponent.Builder<_B> withTextModule(final TextModule textModule) {
            this.textModule = ((textModule == null)?null:new TextModule.Builder<CollectableComponent.Builder<_B>>(this, textModule, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "textModule" property.
         * Use {@link org.nuclos.schema.layout.layoutml.TextModule.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "textModule" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.TextModule.Builder#end()} to return to the current builder.
         */
        public TextModule.Builder<? extends CollectableComponent.Builder<_B>> withTextModule() {
            if (this.textModule!= null) {
                return this.textModule;
            }
            return this.textModule = new TextModule.Builder<CollectableComponent.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public CollectableComponent.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "controltype" (any previous value will be replaced)
         * 
         * @param controltype
         *     New value of the "controltype" property.
         */
        public CollectableComponent.Builder<_B> withControltype(final Controltype controltype) {
            this.controltype = controltype;
            return this;
        }

        /**
         * Sets the new value of "controltypeclass" (any previous value will be replaced)
         * 
         * @param controltypeclass
         *     New value of the "controltypeclass" property.
         */
        public CollectableComponent.Builder<_B> withControltypeclass(final String controltypeclass) {
            this.controltypeclass = controltypeclass;
            return this;
        }

        /**
         * Sets the new value of "enabled" (any previous value will be replaced)
         * 
         * @param enabled
         *     New value of the "enabled" property.
         */
        public CollectableComponent.Builder<_B> withEnabled(final Boolean enabled) {
            this.enabled = enabled;
            return this;
        }

        /**
         * Sets the new value of "insertable" (any previous value will be replaced)
         * 
         * @param insertable
         *     New value of the "insertable" property.
         */
        public CollectableComponent.Builder<_B> withInsertable(final Boolean insertable) {
            this.insertable = insertable;
            return this;
        }

        /**
         * Sets the new value of "multiselect" (any previous value will be replaced)
         * 
         * @param multiselect
         *     New value of the "multiselect" property.
         */
        public CollectableComponent.Builder<_B> withMultiselect(final Boolean multiselect) {
            this.multiselect = multiselect;
            return this;
        }

        /**
         * Sets the new value of "lovbtn" (any previous value will be replaced)
         * 
         * @param lovbtn
         *     New value of the "lovbtn" property.
         */
        public CollectableComponent.Builder<_B> withLovbtn(final Boolean lovbtn) {
            this.lovbtn = lovbtn;
            return this;
        }

        /**
         * Sets the new value of "lovsearch" (any previous value will be replaced)
         * 
         * @param lovsearch
         *     New value of the "lovsearch" property.
         */
        public CollectableComponent.Builder<_B> withLovsearch(final Boolean lovsearch) {
            this.lovsearch = lovsearch;
            return this;
        }

        /**
         * Sets the new value of "dropdownbtn" (any previous value will be replaced)
         * 
         * @param dropdownbtn
         *     New value of the "dropdownbtn" property.
         */
        public CollectableComponent.Builder<_B> withDropdownbtn(final Boolean dropdownbtn) {
            this.dropdownbtn = dropdownbtn;
            return this;
        }

        /**
         * Sets the new value of "customusagesearch" (any previous value will be replaced)
         * 
         * @param customusagesearch
         *     New value of the "customusagesearch" property.
         */
        public CollectableComponent.Builder<_B> withCustomusagesearch(final String customusagesearch) {
            this.customusagesearch = customusagesearch;
            return this;
        }

        /**
         * Sets the new value of "visible" (any previous value will be replaced)
         * 
         * @param visible
         *     New value of the "visible" property.
         */
        public CollectableComponent.Builder<_B> withVisible(final Boolean visible) {
            this.visible = visible;
            return this;
        }

        /**
         * Sets the new value of "notcloneable" (any previous value will be replaced)
         * 
         * @param notcloneable
         *     New value of the "notcloneable" property.
         */
        public CollectableComponent.Builder<_B> withNotcloneable(final Boolean notcloneable) {
            this.notcloneable = notcloneable;
            return this;
        }

        /**
         * Sets the new value of "multieditable" (any previous value will be replaced)
         * 
         * @param multieditable
         *     New value of the "multieditable" property.
         */
        public CollectableComponent.Builder<_B> withMultieditable(final Boolean multieditable) {
            this.multieditable = multieditable;
            return this;
        }

        /**
         * Sets the new value of "scalable" (any previous value will be replaced)
         * 
         * @param scalable
         *     New value of the "scalable" property.
         */
        public CollectableComponent.Builder<_B> withScalable(final Boolean scalable) {
            this.scalable = scalable;
            return this;
        }

        /**
         * Sets the new value of "showOnly" (any previous value will be replaced)
         * 
         * @param showOnly
         *     New value of the "showOnly" property.
         */
        public CollectableComponent.Builder<_B> withShowOnly(final String showOnly) {
            this.showOnly = showOnly;
            return this;
        }

        /**
         * Sets the new value of "label" (any previous value will be replaced)
         * 
         * @param label
         *     New value of the "label" property.
         */
        public CollectableComponent.Builder<_B> withLabel(final String label) {
            this.label = label;
            return this;
        }

        /**
         * Sets the new value of "rows" (any previous value will be replaced)
         * 
         * @param rows
         *     New value of the "rows" property.
         */
        public CollectableComponent.Builder<_B> withRows(final String rows) {
            this.rows = rows;
            return this;
        }

        /**
         * Sets the new value of "columns" (any previous value will be replaced)
         * 
         * @param columns
         *     New value of the "columns" property.
         */
        public CollectableComponent.Builder<_B> withColumns(final String columns) {
            this.columns = columns;
            return this;
        }

        /**
         * Sets the new value of "mnemonic" (any previous value will be replaced)
         * 
         * @param mnemonic
         *     New value of the "mnemonic" property.
         */
        public CollectableComponent.Builder<_B> withMnemonic(final String mnemonic) {
            this.mnemonic = mnemonic;
            return this;
        }

        /**
         * Sets the new value of "opaque" (any previous value will be replaced)
         * 
         * @param opaque
         *     New value of the "opaque" property.
         */
        public CollectableComponent.Builder<_B> withOpaque(final Boolean opaque) {
            this.opaque = opaque;
            return this;
        }

        /**
         * Sets the new value of "fillControlHorizontally" (any previous value will be replaced)
         * 
         * @param fillControlHorizontally
         *     New value of the "fillControlHorizontally" property.
         */
        public CollectableComponent.Builder<_B> withFillControlHorizontally(final Boolean fillControlHorizontally) {
            this.fillControlHorizontally = fillControlHorizontally;
            return this;
        }

        /**
         * Sets the new value of "resourceId" (any previous value will be replaced)
         * 
         * @param resourceId
         *     New value of the "resourceId" property.
         */
        public CollectableComponent.Builder<_B> withResourceId(final String resourceId) {
            this.resourceId = resourceId;
            return this;
        }

        /**
         * Sets the new value of "nextfocuscomponent" (any previous value will be replaced)
         * 
         * @param nextfocuscomponent
         *     New value of the "nextfocuscomponent" property.
         */
        public CollectableComponent.Builder<_B> withNextfocuscomponent(final String nextfocuscomponent) {
            this.nextfocuscomponent = nextfocuscomponent;
            return this;
        }

        /**
         * Sets the new value of "nextfocusfield" (any previous value will be replaced)
         * 
         * @param nextfocusfield
         *     New value of the "nextfocusfield" property.
         */
        public CollectableComponent.Builder<_B> withNextfocusfield(final String nextfocusfield) {
            this.nextfocusfield = nextfocusfield;
            return this;
        }

        /**
         * Sets the new value of "aspectratio" (any previous value will be replaced)
         * 
         * @param aspectratio
         *     New value of the "aspectratio" property.
         */
        public CollectableComponent.Builder<_B> withAspectratio(final Boolean aspectratio) {
            this.aspectratio = aspectratio;
            return this;
        }

        @Override
        public CollectableComponent build() {
            if (_storedValue == null) {
                return this.init(new CollectableComponent());
            } else {
                return ((CollectableComponent) _storedValue);
            }
        }

        public CollectableComponent.Builder<_B> copyOf(final CollectableComponent _other) {
            _other.copyTo(this);
            return this;
        }

        public CollectableComponent.Builder<_B> copyOf(final CollectableComponent.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends CollectableComponent.Selector<CollectableComponent.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static CollectableComponent.Select _root() {
            return new CollectableComponent.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> layoutconstraints = null;
        private Property.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> property = null;
        private ClearBorder.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> clearBorder = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> border = null;
        private MinimumSize.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> minimumSize = null;
        private PreferredSize.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> preferredSize = null;
        private StrictSize.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> strictSize = null;
        private Background.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> background = null;
        private Font.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> font = null;
        private TextFormat.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> textFormat = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> description = null;
        private Options.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> options = null;
        private Translations.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> translations = null;
        private ValuelistProvider.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> valuelistProvider = null;
        private TextModule.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> textModule = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> controltype = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> controltypeclass = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> enabled = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> insertable = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> multiselect = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> lovbtn = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> lovsearch = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> dropdownbtn = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> customusagesearch = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> visible = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> notcloneable = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> multieditable = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> scalable = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> showOnly = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> label = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> rows = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> columns = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> mnemonic = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> opaque = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> fillControlHorizontally = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> resourceId = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> nextfocuscomponent = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> nextfocusfield = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> aspectratio = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.layoutconstraints!= null) {
                products.put("layoutconstraints", this.layoutconstraints.init());
            }
            if (this.property!= null) {
                products.put("property", this.property.init());
            }
            if (this.clearBorder!= null) {
                products.put("clearBorder", this.clearBorder.init());
            }
            if (this.border!= null) {
                products.put("border", this.border.init());
            }
            if (this.minimumSize!= null) {
                products.put("minimumSize", this.minimumSize.init());
            }
            if (this.preferredSize!= null) {
                products.put("preferredSize", this.preferredSize.init());
            }
            if (this.strictSize!= null) {
                products.put("strictSize", this.strictSize.init());
            }
            if (this.background!= null) {
                products.put("background", this.background.init());
            }
            if (this.font!= null) {
                products.put("font", this.font.init());
            }
            if (this.textFormat!= null) {
                products.put("textFormat", this.textFormat.init());
            }
            if (this.description!= null) {
                products.put("description", this.description.init());
            }
            if (this.options!= null) {
                products.put("options", this.options.init());
            }
            if (this.translations!= null) {
                products.put("translations", this.translations.init());
            }
            if (this.valuelistProvider!= null) {
                products.put("valuelistProvider", this.valuelistProvider.init());
            }
            if (this.textModule!= null) {
                products.put("textModule", this.textModule.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.controltype!= null) {
                products.put("controltype", this.controltype.init());
            }
            if (this.controltypeclass!= null) {
                products.put("controltypeclass", this.controltypeclass.init());
            }
            if (this.enabled!= null) {
                products.put("enabled", this.enabled.init());
            }
            if (this.insertable!= null) {
                products.put("insertable", this.insertable.init());
            }
            if (this.multiselect!= null) {
                products.put("multiselect", this.multiselect.init());
            }
            if (this.lovbtn!= null) {
                products.put("lovbtn", this.lovbtn.init());
            }
            if (this.lovsearch!= null) {
                products.put("lovsearch", this.lovsearch.init());
            }
            if (this.dropdownbtn!= null) {
                products.put("dropdownbtn", this.dropdownbtn.init());
            }
            if (this.customusagesearch!= null) {
                products.put("customusagesearch", this.customusagesearch.init());
            }
            if (this.visible!= null) {
                products.put("visible", this.visible.init());
            }
            if (this.notcloneable!= null) {
                products.put("notcloneable", this.notcloneable.init());
            }
            if (this.multieditable!= null) {
                products.put("multieditable", this.multieditable.init());
            }
            if (this.scalable!= null) {
                products.put("scalable", this.scalable.init());
            }
            if (this.showOnly!= null) {
                products.put("showOnly", this.showOnly.init());
            }
            if (this.label!= null) {
                products.put("label", this.label.init());
            }
            if (this.rows!= null) {
                products.put("rows", this.rows.init());
            }
            if (this.columns!= null) {
                products.put("columns", this.columns.init());
            }
            if (this.mnemonic!= null) {
                products.put("mnemonic", this.mnemonic.init());
            }
            if (this.opaque!= null) {
                products.put("opaque", this.opaque.init());
            }
            if (this.fillControlHorizontally!= null) {
                products.put("fillControlHorizontally", this.fillControlHorizontally.init());
            }
            if (this.resourceId!= null) {
                products.put("resourceId", this.resourceId.init());
            }
            if (this.nextfocuscomponent!= null) {
                products.put("nextfocuscomponent", this.nextfocuscomponent.init());
            }
            if (this.nextfocusfield!= null) {
                products.put("nextfocusfield", this.nextfocusfield.init());
            }
            if (this.aspectratio!= null) {
                products.put("aspectratio", this.aspectratio.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> layoutconstraints() {
            return ((this.layoutconstraints == null)?this.layoutconstraints = new com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "layoutconstraints"):this.layoutconstraints);
        }

        public Property.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> property() {
            return ((this.property == null)?this.property = new Property.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "property"):this.property);
        }

        public ClearBorder.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> clearBorder() {
            return ((this.clearBorder == null)?this.clearBorder = new ClearBorder.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "clearBorder"):this.clearBorder);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> border() {
            return ((this.border == null)?this.border = new com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "border"):this.border);
        }

        public MinimumSize.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> minimumSize() {
            return ((this.minimumSize == null)?this.minimumSize = new MinimumSize.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "minimumSize"):this.minimumSize);
        }

        public PreferredSize.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> preferredSize() {
            return ((this.preferredSize == null)?this.preferredSize = new PreferredSize.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "preferredSize"):this.preferredSize);
        }

        public StrictSize.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> strictSize() {
            return ((this.strictSize == null)?this.strictSize = new StrictSize.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "strictSize"):this.strictSize);
        }

        public Background.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> background() {
            return ((this.background == null)?this.background = new Background.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "background"):this.background);
        }

        public Font.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> font() {
            return ((this.font == null)?this.font = new Font.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "font"):this.font);
        }

        public TextFormat.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> textFormat() {
            return ((this.textFormat == null)?this.textFormat = new TextFormat.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "textFormat"):this.textFormat);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> description() {
            return ((this.description == null)?this.description = new com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "description"):this.description);
        }

        public Options.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> options() {
            return ((this.options == null)?this.options = new Options.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "options"):this.options);
        }

        public Translations.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> translations() {
            return ((this.translations == null)?this.translations = new Translations.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "translations"):this.translations);
        }

        public ValuelistProvider.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> valuelistProvider() {
            return ((this.valuelistProvider == null)?this.valuelistProvider = new ValuelistProvider.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "valuelistProvider"):this.valuelistProvider);
        }

        public TextModule.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> textModule() {
            return ((this.textModule == null)?this.textModule = new TextModule.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "textModule"):this.textModule);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> controltype() {
            return ((this.controltype == null)?this.controltype = new com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "controltype"):this.controltype);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> controltypeclass() {
            return ((this.controltypeclass == null)?this.controltypeclass = new com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "controltypeclass"):this.controltypeclass);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> enabled() {
            return ((this.enabled == null)?this.enabled = new com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "enabled"):this.enabled);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> insertable() {
            return ((this.insertable == null)?this.insertable = new com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "insertable"):this.insertable);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> multiselect() {
            return ((this.multiselect == null)?this.multiselect = new com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "multiselect"):this.multiselect);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> lovbtn() {
            return ((this.lovbtn == null)?this.lovbtn = new com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "lovbtn"):this.lovbtn);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> lovsearch() {
            return ((this.lovsearch == null)?this.lovsearch = new com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "lovsearch"):this.lovsearch);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> dropdownbtn() {
            return ((this.dropdownbtn == null)?this.dropdownbtn = new com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "dropdownbtn"):this.dropdownbtn);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> customusagesearch() {
            return ((this.customusagesearch == null)?this.customusagesearch = new com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "customusagesearch"):this.customusagesearch);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> visible() {
            return ((this.visible == null)?this.visible = new com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "visible"):this.visible);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> notcloneable() {
            return ((this.notcloneable == null)?this.notcloneable = new com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "notcloneable"):this.notcloneable);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> multieditable() {
            return ((this.multieditable == null)?this.multieditable = new com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "multieditable"):this.multieditable);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> scalable() {
            return ((this.scalable == null)?this.scalable = new com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "scalable"):this.scalable);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> showOnly() {
            return ((this.showOnly == null)?this.showOnly = new com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "showOnly"):this.showOnly);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> label() {
            return ((this.label == null)?this.label = new com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "label"):this.label);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> rows() {
            return ((this.rows == null)?this.rows = new com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "rows"):this.rows);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> columns() {
            return ((this.columns == null)?this.columns = new com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "columns"):this.columns);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> mnemonic() {
            return ((this.mnemonic == null)?this.mnemonic = new com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "mnemonic"):this.mnemonic);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> opaque() {
            return ((this.opaque == null)?this.opaque = new com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "opaque"):this.opaque);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> fillControlHorizontally() {
            return ((this.fillControlHorizontally == null)?this.fillControlHorizontally = new com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "fillControlHorizontally"):this.fillControlHorizontally);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> resourceId() {
            return ((this.resourceId == null)?this.resourceId = new com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "resourceId"):this.resourceId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> nextfocuscomponent() {
            return ((this.nextfocuscomponent == null)?this.nextfocuscomponent = new com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "nextfocuscomponent"):this.nextfocuscomponent);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> nextfocusfield() {
            return ((this.nextfocusfield == null)?this.nextfocusfield = new com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "nextfocusfield"):this.nextfocusfield);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>> aspectratio() {
            return ((this.aspectratio == null)?this.aspectratio = new com.kscs.util.jaxb.Selector<TRoot, CollectableComponent.Selector<TRoot, TParent>>(this._root, this, "aspectratio"):this.aspectratio);
        }

    }

}
