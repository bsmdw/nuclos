
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}layoutconstraints" minOccurs="0"/&gt;
 *         &lt;group ref="{}borders"/&gt;
 *         &lt;group ref="{}sizes"/&gt;
 *         &lt;element ref="{}font" minOccurs="0"/&gt;
 *         &lt;element ref="{}initial-sorting-order" minOccurs="0"/&gt;
 *         &lt;element ref="{}background" minOccurs="0"/&gt;
 *         &lt;element ref="{}description" minOccurs="0"/&gt;
 *         &lt;element ref="{}subform-column" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{}new-enabled" minOccurs="0"/&gt;
 *         &lt;element ref="{}edit-enabled" minOccurs="0"/&gt;
 *         &lt;element ref="{}delete-enabled" minOccurs="0"/&gt;
 *         &lt;element ref="{}clone-enabled" minOccurs="0"/&gt;
 *         &lt;element ref="{}dynamic-row-color" minOccurs="0"/&gt;
 *         &lt;element ref="{}property" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="entity" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="enabled" type="{}boolean" /&gt;
 *       &lt;attribute name="autonumbersorting" type="{}boolean" /&gt;
 *       &lt;attribute name="notcloneable" type="{}boolean" /&gt;
 *       &lt;attribute name="multieditable" type="{}boolean" /&gt;
 *       &lt;attribute name="toolbarorientation"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
 *             &lt;enumeration value="horizontal"/&gt;
 *             &lt;enumeration value="vertical"/&gt;
 *             &lt;enumeration value="hide"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="statusbar" type="{}boolean" /&gt;
 *       &lt;attribute name="opaque" type="{}boolean" /&gt;
 *       &lt;attribute name="open-details-with-tab-recycling" type="{}boolean" /&gt;
 *       &lt;attribute name="controllertype" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="unique-mastercolumn" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="foreignkeyfield-to-parent" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="parent-subform" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="dynamic-cell-heights-default" type="{}boolean" /&gt;
 *       &lt;attribute name="ignore-sub-layout" type="{}boolean" /&gt;
 *       &lt;attribute name="max-entries" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "layoutconstraints",
    "clearBorder",
    "border",
    "minimumSize",
    "preferredSize",
    "strictSize",
    "font",
    "initialSortingOrder",
    "background",
    "description",
    "subformColumn",
    "newEnabled",
    "editEnabled",
    "deleteEnabled",
    "cloneEnabled",
    "dynamicRowColor",
    "property"
})
public class Subform implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "layoutconstraints", type = JAXBElement.class, required = false)
    protected JAXBElement<?> layoutconstraints;
    @XmlElement(name = "clear-border")
    protected ClearBorder clearBorder;
    @XmlElementRef(name = "border", type = JAXBElement.class, required = false)
    protected List<JAXBElement<?>> border;
    @XmlElement(name = "minimum-size")
    protected MinimumSize minimumSize;
    @XmlElement(name = "preferred-size")
    protected PreferredSize preferredSize;
    @XmlElement(name = "strict-size")
    protected StrictSize strictSize;
    protected Font font;
    @XmlElement(name = "initial-sorting-order")
    protected InitialSortingOrder initialSortingOrder;
    protected Background background;
    protected String description;
    @XmlElement(name = "subform-column")
    protected List<SubformColumn> subformColumn;
    @XmlElement(name = "new-enabled")
    protected NewEnabled newEnabled;
    @XmlElement(name = "edit-enabled")
    protected EditEnabled editEnabled;
    @XmlElement(name = "delete-enabled")
    protected DeleteEnabled deleteEnabled;
    @XmlElement(name = "clone-enabled")
    protected CloneEnabled cloneEnabled;
    @XmlElement(name = "dynamic-row-color")
    protected DynamicRowColor dynamicRowColor;
    protected List<Property> property;
    @XmlAttribute(name = "name")
    @XmlSchemaType(name = "anySimpleType")
    protected String name;
    @XmlAttribute(name = "entity", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String entity;
    @XmlAttribute(name = "enabled")
    protected Boolean enabled;
    @XmlAttribute(name = "autonumbersorting")
    protected Boolean autonumbersorting;
    @XmlAttribute(name = "notcloneable")
    protected Boolean notcloneable;
    @XmlAttribute(name = "multieditable")
    protected Boolean multieditable;
    @XmlAttribute(name = "toolbarorientation")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String toolbarorientation;
    @XmlAttribute(name = "statusbar")
    protected Boolean statusbar;
    @XmlAttribute(name = "opaque")
    protected Boolean opaque;
    @XmlAttribute(name = "open-details-with-tab-recycling")
    protected Boolean openDetailsWithTabRecycling;
    @XmlAttribute(name = "controllertype")
    @XmlSchemaType(name = "anySimpleType")
    protected String controllertype;
    @XmlAttribute(name = "unique-mastercolumn")
    @XmlSchemaType(name = "anySimpleType")
    protected String uniqueMastercolumn;
    @XmlAttribute(name = "foreignkeyfield-to-parent")
    @XmlSchemaType(name = "anySimpleType")
    protected String foreignkeyfieldToParent;
    @XmlAttribute(name = "parent-subform")
    @XmlSchemaType(name = "anySimpleType")
    protected String parentSubform;
    @XmlAttribute(name = "dynamic-cell-heights-default")
    protected Boolean dynamicCellHeightsDefault;
    @XmlAttribute(name = "ignore-sub-layout")
    protected Boolean ignoreSubLayout;
    @XmlAttribute(name = "max-entries")
    @XmlSchemaType(name = "anySimpleType")
    protected String maxEntries;

    /**
     * Gets the value of the layoutconstraints property.
     * 
     * @return
     *     possible object is
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     *     
     */
    public JAXBElement<?> getLayoutconstraints() {
        return layoutconstraints;
    }

    /**
     * Sets the value of the layoutconstraints property.
     * 
     * @param value
     *     allowed object is
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     *     
     */
    public void setLayoutconstraints(JAXBElement<?> value) {
        this.layoutconstraints = value;
    }

    /**
     * Gets the value of the clearBorder property.
     * 
     * @return
     *     possible object is
     *     {@link ClearBorder }
     *     
     */
    public ClearBorder getClearBorder() {
        return clearBorder;
    }

    /**
     * Sets the value of the clearBorder property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClearBorder }
     *     
     */
    public void setClearBorder(ClearBorder value) {
        this.clearBorder = value;
    }

    /**
     * Gets the value of the border property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the border property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBorder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     * 
     * 
     */
    public List<JAXBElement<?>> getBorder() {
        if (border == null) {
            border = new ArrayList<JAXBElement<?>>();
        }
        return this.border;
    }

    /**
     * Gets the value of the minimumSize property.
     * 
     * @return
     *     possible object is
     *     {@link MinimumSize }
     *     
     */
    public MinimumSize getMinimumSize() {
        return minimumSize;
    }

    /**
     * Sets the value of the minimumSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link MinimumSize }
     *     
     */
    public void setMinimumSize(MinimumSize value) {
        this.minimumSize = value;
    }

    /**
     * Gets the value of the preferredSize property.
     * 
     * @return
     *     possible object is
     *     {@link PreferredSize }
     *     
     */
    public PreferredSize getPreferredSize() {
        return preferredSize;
    }

    /**
     * Sets the value of the preferredSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link PreferredSize }
     *     
     */
    public void setPreferredSize(PreferredSize value) {
        this.preferredSize = value;
    }

    /**
     * Gets the value of the strictSize property.
     * 
     * @return
     *     possible object is
     *     {@link StrictSize }
     *     
     */
    public StrictSize getStrictSize() {
        return strictSize;
    }

    /**
     * Sets the value of the strictSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link StrictSize }
     *     
     */
    public void setStrictSize(StrictSize value) {
        this.strictSize = value;
    }

    /**
     * Gets the value of the font property.
     * 
     * @return
     *     possible object is
     *     {@link Font }
     *     
     */
    public Font getFont() {
        return font;
    }

    /**
     * Sets the value of the font property.
     * 
     * @param value
     *     allowed object is
     *     {@link Font }
     *     
     */
    public void setFont(Font value) {
        this.font = value;
    }

    /**
     * Gets the value of the initialSortingOrder property.
     * 
     * @return
     *     possible object is
     *     {@link InitialSortingOrder }
     *     
     */
    public InitialSortingOrder getInitialSortingOrder() {
        return initialSortingOrder;
    }

    /**
     * Sets the value of the initialSortingOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link InitialSortingOrder }
     *     
     */
    public void setInitialSortingOrder(InitialSortingOrder value) {
        this.initialSortingOrder = value;
    }

    /**
     * Gets the value of the background property.
     * 
     * @return
     *     possible object is
     *     {@link Background }
     *     
     */
    public Background getBackground() {
        return background;
    }

    /**
     * Sets the value of the background property.
     * 
     * @param value
     *     allowed object is
     *     {@link Background }
     *     
     */
    public void setBackground(Background value) {
        this.background = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the subformColumn property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subformColumn property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubformColumn().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SubformColumn }
     * 
     * 
     */
    public List<SubformColumn> getSubformColumn() {
        if (subformColumn == null) {
            subformColumn = new ArrayList<SubformColumn>();
        }
        return this.subformColumn;
    }

    /**
     * Gets the value of the newEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link NewEnabled }
     *     
     */
    public NewEnabled getNewEnabled() {
        return newEnabled;
    }

    /**
     * Sets the value of the newEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link NewEnabled }
     *     
     */
    public void setNewEnabled(NewEnabled value) {
        this.newEnabled = value;
    }

    /**
     * Gets the value of the editEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link EditEnabled }
     *     
     */
    public EditEnabled getEditEnabled() {
        return editEnabled;
    }

    /**
     * Sets the value of the editEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link EditEnabled }
     *     
     */
    public void setEditEnabled(EditEnabled value) {
        this.editEnabled = value;
    }

    /**
     * Gets the value of the deleteEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link DeleteEnabled }
     *     
     */
    public DeleteEnabled getDeleteEnabled() {
        return deleteEnabled;
    }

    /**
     * Sets the value of the deleteEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeleteEnabled }
     *     
     */
    public void setDeleteEnabled(DeleteEnabled value) {
        this.deleteEnabled = value;
    }

    /**
     * Gets the value of the cloneEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link CloneEnabled }
     *     
     */
    public CloneEnabled getCloneEnabled() {
        return cloneEnabled;
    }

    /**
     * Sets the value of the cloneEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link CloneEnabled }
     *     
     */
    public void setCloneEnabled(CloneEnabled value) {
        this.cloneEnabled = value;
    }

    /**
     * Gets the value of the dynamicRowColor property.
     * 
     * @return
     *     possible object is
     *     {@link DynamicRowColor }
     *     
     */
    public DynamicRowColor getDynamicRowColor() {
        return dynamicRowColor;
    }

    /**
     * Sets the value of the dynamicRowColor property.
     * 
     * @param value
     *     allowed object is
     *     {@link DynamicRowColor }
     *     
     */
    public void setDynamicRowColor(DynamicRowColor value) {
        this.dynamicRowColor = value;
    }

    /**
     * Gets the value of the property property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the property property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProperty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Property }
     * 
     * 
     */
    public List<Property> getProperty() {
        if (property == null) {
            property = new ArrayList<Property>();
        }
        return this.property;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the entity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntity() {
        return entity;
    }

    /**
     * Sets the value of the entity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntity(String value) {
        this.entity = value;
    }

    /**
     * Gets the value of the enabled property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getEnabled() {
        return enabled;
    }

    /**
     * Sets the value of the enabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnabled(Boolean value) {
        this.enabled = value;
    }

    /**
     * Gets the value of the autonumbersorting property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getAutonumbersorting() {
        return autonumbersorting;
    }

    /**
     * Sets the value of the autonumbersorting property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutonumbersorting(Boolean value) {
        this.autonumbersorting = value;
    }

    /**
     * Gets the value of the notcloneable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getNotcloneable() {
        return notcloneable;
    }

    /**
     * Sets the value of the notcloneable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNotcloneable(Boolean value) {
        this.notcloneable = value;
    }

    /**
     * Gets the value of the multieditable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getMultieditable() {
        return multieditable;
    }

    /**
     * Sets the value of the multieditable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMultieditable(Boolean value) {
        this.multieditable = value;
    }

    /**
     * Gets the value of the toolbarorientation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToolbarorientation() {
        return toolbarorientation;
    }

    /**
     * Sets the value of the toolbarorientation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToolbarorientation(String value) {
        this.toolbarorientation = value;
    }

    /**
     * Gets the value of the statusbar property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getStatusbar() {
        return statusbar;
    }

    /**
     * Sets the value of the statusbar property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStatusbar(Boolean value) {
        this.statusbar = value;
    }

    /**
     * Gets the value of the opaque property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOpaque() {
        return opaque;
    }

    /**
     * Sets the value of the opaque property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOpaque(Boolean value) {
        this.opaque = value;
    }

    /**
     * Gets the value of the openDetailsWithTabRecycling property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOpenDetailsWithTabRecycling() {
        return openDetailsWithTabRecycling;
    }

    /**
     * Sets the value of the openDetailsWithTabRecycling property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOpenDetailsWithTabRecycling(Boolean value) {
        this.openDetailsWithTabRecycling = value;
    }

    /**
     * Gets the value of the controllertype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControllertype() {
        return controllertype;
    }

    /**
     * Sets the value of the controllertype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControllertype(String value) {
        this.controllertype = value;
    }

    /**
     * Gets the value of the uniqueMastercolumn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniqueMastercolumn() {
        return uniqueMastercolumn;
    }

    /**
     * Sets the value of the uniqueMastercolumn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniqueMastercolumn(String value) {
        this.uniqueMastercolumn = value;
    }

    /**
     * Gets the value of the foreignkeyfieldToParent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForeignkeyfieldToParent() {
        return foreignkeyfieldToParent;
    }

    /**
     * Sets the value of the foreignkeyfieldToParent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForeignkeyfieldToParent(String value) {
        this.foreignkeyfieldToParent = value;
    }

    /**
     * Gets the value of the parentSubform property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentSubform() {
        return parentSubform;
    }

    /**
     * Sets the value of the parentSubform property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentSubform(String value) {
        this.parentSubform = value;
    }

    /**
     * Gets the value of the dynamicCellHeightsDefault property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDynamicCellHeightsDefault() {
        return dynamicCellHeightsDefault;
    }

    /**
     * Sets the value of the dynamicCellHeightsDefault property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDynamicCellHeightsDefault(Boolean value) {
        this.dynamicCellHeightsDefault = value;
    }

    /**
     * Gets the value of the ignoreSubLayout property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIgnoreSubLayout() {
        return ignoreSubLayout;
    }

    /**
     * Sets the value of the ignoreSubLayout property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnoreSubLayout(Boolean value) {
        this.ignoreSubLayout = value;
    }

    /**
     * Gets the value of the maxEntries property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxEntries() {
        return maxEntries;
    }

    /**
     * Sets the value of the maxEntries property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxEntries(String value) {
        this.maxEntries = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            JAXBElement<?> theLayoutconstraints;
            theLayoutconstraints = this.getLayoutconstraints();
            strategy.appendField(locator, this, "layoutconstraints", buffer, theLayoutconstraints);
        }
        {
            ClearBorder theClearBorder;
            theClearBorder = this.getClearBorder();
            strategy.appendField(locator, this, "clearBorder", buffer, theClearBorder);
        }
        {
            List<JAXBElement<?>> theBorder;
            theBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            strategy.appendField(locator, this, "border", buffer, theBorder);
        }
        {
            MinimumSize theMinimumSize;
            theMinimumSize = this.getMinimumSize();
            strategy.appendField(locator, this, "minimumSize", buffer, theMinimumSize);
        }
        {
            PreferredSize thePreferredSize;
            thePreferredSize = this.getPreferredSize();
            strategy.appendField(locator, this, "preferredSize", buffer, thePreferredSize);
        }
        {
            StrictSize theStrictSize;
            theStrictSize = this.getStrictSize();
            strategy.appendField(locator, this, "strictSize", buffer, theStrictSize);
        }
        {
            Font theFont;
            theFont = this.getFont();
            strategy.appendField(locator, this, "font", buffer, theFont);
        }
        {
            InitialSortingOrder theInitialSortingOrder;
            theInitialSortingOrder = this.getInitialSortingOrder();
            strategy.appendField(locator, this, "initialSortingOrder", buffer, theInitialSortingOrder);
        }
        {
            Background theBackground;
            theBackground = this.getBackground();
            strategy.appendField(locator, this, "background", buffer, theBackground);
        }
        {
            String theDescription;
            theDescription = this.getDescription();
            strategy.appendField(locator, this, "description", buffer, theDescription);
        }
        {
            List<SubformColumn> theSubformColumn;
            theSubformColumn = (((this.subformColumn!= null)&&(!this.subformColumn.isEmpty()))?this.getSubformColumn():null);
            strategy.appendField(locator, this, "subformColumn", buffer, theSubformColumn);
        }
        {
            NewEnabled theNewEnabled;
            theNewEnabled = this.getNewEnabled();
            strategy.appendField(locator, this, "newEnabled", buffer, theNewEnabled);
        }
        {
            EditEnabled theEditEnabled;
            theEditEnabled = this.getEditEnabled();
            strategy.appendField(locator, this, "editEnabled", buffer, theEditEnabled);
        }
        {
            DeleteEnabled theDeleteEnabled;
            theDeleteEnabled = this.getDeleteEnabled();
            strategy.appendField(locator, this, "deleteEnabled", buffer, theDeleteEnabled);
        }
        {
            CloneEnabled theCloneEnabled;
            theCloneEnabled = this.getCloneEnabled();
            strategy.appendField(locator, this, "cloneEnabled", buffer, theCloneEnabled);
        }
        {
            DynamicRowColor theDynamicRowColor;
            theDynamicRowColor = this.getDynamicRowColor();
            strategy.appendField(locator, this, "dynamicRowColor", buffer, theDynamicRowColor);
        }
        {
            List<Property> theProperty;
            theProperty = (((this.property!= null)&&(!this.property.isEmpty()))?this.getProperty():null);
            strategy.appendField(locator, this, "property", buffer, theProperty);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String theEntity;
            theEntity = this.getEntity();
            strategy.appendField(locator, this, "entity", buffer, theEntity);
        }
        {
            Boolean theEnabled;
            theEnabled = this.getEnabled();
            strategy.appendField(locator, this, "enabled", buffer, theEnabled);
        }
        {
            Boolean theAutonumbersorting;
            theAutonumbersorting = this.getAutonumbersorting();
            strategy.appendField(locator, this, "autonumbersorting", buffer, theAutonumbersorting);
        }
        {
            Boolean theNotcloneable;
            theNotcloneable = this.getNotcloneable();
            strategy.appendField(locator, this, "notcloneable", buffer, theNotcloneable);
        }
        {
            Boolean theMultieditable;
            theMultieditable = this.getMultieditable();
            strategy.appendField(locator, this, "multieditable", buffer, theMultieditable);
        }
        {
            String theToolbarorientation;
            theToolbarorientation = this.getToolbarorientation();
            strategy.appendField(locator, this, "toolbarorientation", buffer, theToolbarorientation);
        }
        {
            Boolean theStatusbar;
            theStatusbar = this.getStatusbar();
            strategy.appendField(locator, this, "statusbar", buffer, theStatusbar);
        }
        {
            Boolean theOpaque;
            theOpaque = this.getOpaque();
            strategy.appendField(locator, this, "opaque", buffer, theOpaque);
        }
        {
            Boolean theOpenDetailsWithTabRecycling;
            theOpenDetailsWithTabRecycling = this.getOpenDetailsWithTabRecycling();
            strategy.appendField(locator, this, "openDetailsWithTabRecycling", buffer, theOpenDetailsWithTabRecycling);
        }
        {
            String theControllertype;
            theControllertype = this.getControllertype();
            strategy.appendField(locator, this, "controllertype", buffer, theControllertype);
        }
        {
            String theUniqueMastercolumn;
            theUniqueMastercolumn = this.getUniqueMastercolumn();
            strategy.appendField(locator, this, "uniqueMastercolumn", buffer, theUniqueMastercolumn);
        }
        {
            String theForeignkeyfieldToParent;
            theForeignkeyfieldToParent = this.getForeignkeyfieldToParent();
            strategy.appendField(locator, this, "foreignkeyfieldToParent", buffer, theForeignkeyfieldToParent);
        }
        {
            String theParentSubform;
            theParentSubform = this.getParentSubform();
            strategy.appendField(locator, this, "parentSubform", buffer, theParentSubform);
        }
        {
            Boolean theDynamicCellHeightsDefault;
            theDynamicCellHeightsDefault = this.getDynamicCellHeightsDefault();
            strategy.appendField(locator, this, "dynamicCellHeightsDefault", buffer, theDynamicCellHeightsDefault);
        }
        {
            Boolean theIgnoreSubLayout;
            theIgnoreSubLayout = this.getIgnoreSubLayout();
            strategy.appendField(locator, this, "ignoreSubLayout", buffer, theIgnoreSubLayout);
        }
        {
            String theMaxEntries;
            theMaxEntries = this.getMaxEntries();
            strategy.appendField(locator, this, "maxEntries", buffer, theMaxEntries);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Subform)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Subform that = ((Subform) object);
        {
            JAXBElement<?> lhsLayoutconstraints;
            lhsLayoutconstraints = this.getLayoutconstraints();
            JAXBElement<?> rhsLayoutconstraints;
            rhsLayoutconstraints = that.getLayoutconstraints();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "layoutconstraints", lhsLayoutconstraints), LocatorUtils.property(thatLocator, "layoutconstraints", rhsLayoutconstraints), lhsLayoutconstraints, rhsLayoutconstraints)) {
                return false;
            }
        }
        {
            ClearBorder lhsClearBorder;
            lhsClearBorder = this.getClearBorder();
            ClearBorder rhsClearBorder;
            rhsClearBorder = that.getClearBorder();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "clearBorder", lhsClearBorder), LocatorUtils.property(thatLocator, "clearBorder", rhsClearBorder), lhsClearBorder, rhsClearBorder)) {
                return false;
            }
        }
        {
            List<JAXBElement<?>> lhsBorder;
            lhsBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            List<JAXBElement<?>> rhsBorder;
            rhsBorder = (((that.border!= null)&&(!that.border.isEmpty()))?that.getBorder():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "border", lhsBorder), LocatorUtils.property(thatLocator, "border", rhsBorder), lhsBorder, rhsBorder)) {
                return false;
            }
        }
        {
            MinimumSize lhsMinimumSize;
            lhsMinimumSize = this.getMinimumSize();
            MinimumSize rhsMinimumSize;
            rhsMinimumSize = that.getMinimumSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "minimumSize", lhsMinimumSize), LocatorUtils.property(thatLocator, "minimumSize", rhsMinimumSize), lhsMinimumSize, rhsMinimumSize)) {
                return false;
            }
        }
        {
            PreferredSize lhsPreferredSize;
            lhsPreferredSize = this.getPreferredSize();
            PreferredSize rhsPreferredSize;
            rhsPreferredSize = that.getPreferredSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "preferredSize", lhsPreferredSize), LocatorUtils.property(thatLocator, "preferredSize", rhsPreferredSize), lhsPreferredSize, rhsPreferredSize)) {
                return false;
            }
        }
        {
            StrictSize lhsStrictSize;
            lhsStrictSize = this.getStrictSize();
            StrictSize rhsStrictSize;
            rhsStrictSize = that.getStrictSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "strictSize", lhsStrictSize), LocatorUtils.property(thatLocator, "strictSize", rhsStrictSize), lhsStrictSize, rhsStrictSize)) {
                return false;
            }
        }
        {
            Font lhsFont;
            lhsFont = this.getFont();
            Font rhsFont;
            rhsFont = that.getFont();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "font", lhsFont), LocatorUtils.property(thatLocator, "font", rhsFont), lhsFont, rhsFont)) {
                return false;
            }
        }
        {
            InitialSortingOrder lhsInitialSortingOrder;
            lhsInitialSortingOrder = this.getInitialSortingOrder();
            InitialSortingOrder rhsInitialSortingOrder;
            rhsInitialSortingOrder = that.getInitialSortingOrder();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "initialSortingOrder", lhsInitialSortingOrder), LocatorUtils.property(thatLocator, "initialSortingOrder", rhsInitialSortingOrder), lhsInitialSortingOrder, rhsInitialSortingOrder)) {
                return false;
            }
        }
        {
            Background lhsBackground;
            lhsBackground = this.getBackground();
            Background rhsBackground;
            rhsBackground = that.getBackground();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "background", lhsBackground), LocatorUtils.property(thatLocator, "background", rhsBackground), lhsBackground, rhsBackground)) {
                return false;
            }
        }
        {
            String lhsDescription;
            lhsDescription = this.getDescription();
            String rhsDescription;
            rhsDescription = that.getDescription();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "description", lhsDescription), LocatorUtils.property(thatLocator, "description", rhsDescription), lhsDescription, rhsDescription)) {
                return false;
            }
        }
        {
            List<SubformColumn> lhsSubformColumn;
            lhsSubformColumn = (((this.subformColumn!= null)&&(!this.subformColumn.isEmpty()))?this.getSubformColumn():null);
            List<SubformColumn> rhsSubformColumn;
            rhsSubformColumn = (((that.subformColumn!= null)&&(!that.subformColumn.isEmpty()))?that.getSubformColumn():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "subformColumn", lhsSubformColumn), LocatorUtils.property(thatLocator, "subformColumn", rhsSubformColumn), lhsSubformColumn, rhsSubformColumn)) {
                return false;
            }
        }
        {
            NewEnabled lhsNewEnabled;
            lhsNewEnabled = this.getNewEnabled();
            NewEnabled rhsNewEnabled;
            rhsNewEnabled = that.getNewEnabled();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "newEnabled", lhsNewEnabled), LocatorUtils.property(thatLocator, "newEnabled", rhsNewEnabled), lhsNewEnabled, rhsNewEnabled)) {
                return false;
            }
        }
        {
            EditEnabled lhsEditEnabled;
            lhsEditEnabled = this.getEditEnabled();
            EditEnabled rhsEditEnabled;
            rhsEditEnabled = that.getEditEnabled();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "editEnabled", lhsEditEnabled), LocatorUtils.property(thatLocator, "editEnabled", rhsEditEnabled), lhsEditEnabled, rhsEditEnabled)) {
                return false;
            }
        }
        {
            DeleteEnabled lhsDeleteEnabled;
            lhsDeleteEnabled = this.getDeleteEnabled();
            DeleteEnabled rhsDeleteEnabled;
            rhsDeleteEnabled = that.getDeleteEnabled();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "deleteEnabled", lhsDeleteEnabled), LocatorUtils.property(thatLocator, "deleteEnabled", rhsDeleteEnabled), lhsDeleteEnabled, rhsDeleteEnabled)) {
                return false;
            }
        }
        {
            CloneEnabled lhsCloneEnabled;
            lhsCloneEnabled = this.getCloneEnabled();
            CloneEnabled rhsCloneEnabled;
            rhsCloneEnabled = that.getCloneEnabled();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "cloneEnabled", lhsCloneEnabled), LocatorUtils.property(thatLocator, "cloneEnabled", rhsCloneEnabled), lhsCloneEnabled, rhsCloneEnabled)) {
                return false;
            }
        }
        {
            DynamicRowColor lhsDynamicRowColor;
            lhsDynamicRowColor = this.getDynamicRowColor();
            DynamicRowColor rhsDynamicRowColor;
            rhsDynamicRowColor = that.getDynamicRowColor();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dynamicRowColor", lhsDynamicRowColor), LocatorUtils.property(thatLocator, "dynamicRowColor", rhsDynamicRowColor), lhsDynamicRowColor, rhsDynamicRowColor)) {
                return false;
            }
        }
        {
            List<Property> lhsProperty;
            lhsProperty = (((this.property!= null)&&(!this.property.isEmpty()))?this.getProperty():null);
            List<Property> rhsProperty;
            rhsProperty = (((that.property!= null)&&(!that.property.isEmpty()))?that.getProperty():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "property", lhsProperty), LocatorUtils.property(thatLocator, "property", rhsProperty), lhsProperty, rhsProperty)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsEntity;
            lhsEntity = this.getEntity();
            String rhsEntity;
            rhsEntity = that.getEntity();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entity", lhsEntity), LocatorUtils.property(thatLocator, "entity", rhsEntity), lhsEntity, rhsEntity)) {
                return false;
            }
        }
        {
            Boolean lhsEnabled;
            lhsEnabled = this.getEnabled();
            Boolean rhsEnabled;
            rhsEnabled = that.getEnabled();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "enabled", lhsEnabled), LocatorUtils.property(thatLocator, "enabled", rhsEnabled), lhsEnabled, rhsEnabled)) {
                return false;
            }
        }
        {
            Boolean lhsAutonumbersorting;
            lhsAutonumbersorting = this.getAutonumbersorting();
            Boolean rhsAutonumbersorting;
            rhsAutonumbersorting = that.getAutonumbersorting();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "autonumbersorting", lhsAutonumbersorting), LocatorUtils.property(thatLocator, "autonumbersorting", rhsAutonumbersorting), lhsAutonumbersorting, rhsAutonumbersorting)) {
                return false;
            }
        }
        {
            Boolean lhsNotcloneable;
            lhsNotcloneable = this.getNotcloneable();
            Boolean rhsNotcloneable;
            rhsNotcloneable = that.getNotcloneable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "notcloneable", lhsNotcloneable), LocatorUtils.property(thatLocator, "notcloneable", rhsNotcloneable), lhsNotcloneable, rhsNotcloneable)) {
                return false;
            }
        }
        {
            Boolean lhsMultieditable;
            lhsMultieditable = this.getMultieditable();
            Boolean rhsMultieditable;
            rhsMultieditable = that.getMultieditable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "multieditable", lhsMultieditable), LocatorUtils.property(thatLocator, "multieditable", rhsMultieditable), lhsMultieditable, rhsMultieditable)) {
                return false;
            }
        }
        {
            String lhsToolbarorientation;
            lhsToolbarorientation = this.getToolbarorientation();
            String rhsToolbarorientation;
            rhsToolbarorientation = that.getToolbarorientation();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "toolbarorientation", lhsToolbarorientation), LocatorUtils.property(thatLocator, "toolbarorientation", rhsToolbarorientation), lhsToolbarorientation, rhsToolbarorientation)) {
                return false;
            }
        }
        {
            Boolean lhsStatusbar;
            lhsStatusbar = this.getStatusbar();
            Boolean rhsStatusbar;
            rhsStatusbar = that.getStatusbar();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "statusbar", lhsStatusbar), LocatorUtils.property(thatLocator, "statusbar", rhsStatusbar), lhsStatusbar, rhsStatusbar)) {
                return false;
            }
        }
        {
            Boolean lhsOpaque;
            lhsOpaque = this.getOpaque();
            Boolean rhsOpaque;
            rhsOpaque = that.getOpaque();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "opaque", lhsOpaque), LocatorUtils.property(thatLocator, "opaque", rhsOpaque), lhsOpaque, rhsOpaque)) {
                return false;
            }
        }
        {
            Boolean lhsOpenDetailsWithTabRecycling;
            lhsOpenDetailsWithTabRecycling = this.getOpenDetailsWithTabRecycling();
            Boolean rhsOpenDetailsWithTabRecycling;
            rhsOpenDetailsWithTabRecycling = that.getOpenDetailsWithTabRecycling();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "openDetailsWithTabRecycling", lhsOpenDetailsWithTabRecycling), LocatorUtils.property(thatLocator, "openDetailsWithTabRecycling", rhsOpenDetailsWithTabRecycling), lhsOpenDetailsWithTabRecycling, rhsOpenDetailsWithTabRecycling)) {
                return false;
            }
        }
        {
            String lhsControllertype;
            lhsControllertype = this.getControllertype();
            String rhsControllertype;
            rhsControllertype = that.getControllertype();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "controllertype", lhsControllertype), LocatorUtils.property(thatLocator, "controllertype", rhsControllertype), lhsControllertype, rhsControllertype)) {
                return false;
            }
        }
        {
            String lhsUniqueMastercolumn;
            lhsUniqueMastercolumn = this.getUniqueMastercolumn();
            String rhsUniqueMastercolumn;
            rhsUniqueMastercolumn = that.getUniqueMastercolumn();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "uniqueMastercolumn", lhsUniqueMastercolumn), LocatorUtils.property(thatLocator, "uniqueMastercolumn", rhsUniqueMastercolumn), lhsUniqueMastercolumn, rhsUniqueMastercolumn)) {
                return false;
            }
        }
        {
            String lhsForeignkeyfieldToParent;
            lhsForeignkeyfieldToParent = this.getForeignkeyfieldToParent();
            String rhsForeignkeyfieldToParent;
            rhsForeignkeyfieldToParent = that.getForeignkeyfieldToParent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "foreignkeyfieldToParent", lhsForeignkeyfieldToParent), LocatorUtils.property(thatLocator, "foreignkeyfieldToParent", rhsForeignkeyfieldToParent), lhsForeignkeyfieldToParent, rhsForeignkeyfieldToParent)) {
                return false;
            }
        }
        {
            String lhsParentSubform;
            lhsParentSubform = this.getParentSubform();
            String rhsParentSubform;
            rhsParentSubform = that.getParentSubform();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "parentSubform", lhsParentSubform), LocatorUtils.property(thatLocator, "parentSubform", rhsParentSubform), lhsParentSubform, rhsParentSubform)) {
                return false;
            }
        }
        {
            Boolean lhsDynamicCellHeightsDefault;
            lhsDynamicCellHeightsDefault = this.getDynamicCellHeightsDefault();
            Boolean rhsDynamicCellHeightsDefault;
            rhsDynamicCellHeightsDefault = that.getDynamicCellHeightsDefault();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dynamicCellHeightsDefault", lhsDynamicCellHeightsDefault), LocatorUtils.property(thatLocator, "dynamicCellHeightsDefault", rhsDynamicCellHeightsDefault), lhsDynamicCellHeightsDefault, rhsDynamicCellHeightsDefault)) {
                return false;
            }
        }
        {
            Boolean lhsIgnoreSubLayout;
            lhsIgnoreSubLayout = this.getIgnoreSubLayout();
            Boolean rhsIgnoreSubLayout;
            rhsIgnoreSubLayout = that.getIgnoreSubLayout();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ignoreSubLayout", lhsIgnoreSubLayout), LocatorUtils.property(thatLocator, "ignoreSubLayout", rhsIgnoreSubLayout), lhsIgnoreSubLayout, rhsIgnoreSubLayout)) {
                return false;
            }
        }
        {
            String lhsMaxEntries;
            lhsMaxEntries = this.getMaxEntries();
            String rhsMaxEntries;
            rhsMaxEntries = that.getMaxEntries();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "maxEntries", lhsMaxEntries), LocatorUtils.property(thatLocator, "maxEntries", rhsMaxEntries), lhsMaxEntries, rhsMaxEntries)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            JAXBElement<?> theLayoutconstraints;
            theLayoutconstraints = this.getLayoutconstraints();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "layoutconstraints", theLayoutconstraints), currentHashCode, theLayoutconstraints);
        }
        {
            ClearBorder theClearBorder;
            theClearBorder = this.getClearBorder();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "clearBorder", theClearBorder), currentHashCode, theClearBorder);
        }
        {
            List<JAXBElement<?>> theBorder;
            theBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "border", theBorder), currentHashCode, theBorder);
        }
        {
            MinimumSize theMinimumSize;
            theMinimumSize = this.getMinimumSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "minimumSize", theMinimumSize), currentHashCode, theMinimumSize);
        }
        {
            PreferredSize thePreferredSize;
            thePreferredSize = this.getPreferredSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "preferredSize", thePreferredSize), currentHashCode, thePreferredSize);
        }
        {
            StrictSize theStrictSize;
            theStrictSize = this.getStrictSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "strictSize", theStrictSize), currentHashCode, theStrictSize);
        }
        {
            Font theFont;
            theFont = this.getFont();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "font", theFont), currentHashCode, theFont);
        }
        {
            InitialSortingOrder theInitialSortingOrder;
            theInitialSortingOrder = this.getInitialSortingOrder();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "initialSortingOrder", theInitialSortingOrder), currentHashCode, theInitialSortingOrder);
        }
        {
            Background theBackground;
            theBackground = this.getBackground();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "background", theBackground), currentHashCode, theBackground);
        }
        {
            String theDescription;
            theDescription = this.getDescription();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "description", theDescription), currentHashCode, theDescription);
        }
        {
            List<SubformColumn> theSubformColumn;
            theSubformColumn = (((this.subformColumn!= null)&&(!this.subformColumn.isEmpty()))?this.getSubformColumn():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "subformColumn", theSubformColumn), currentHashCode, theSubformColumn);
        }
        {
            NewEnabled theNewEnabled;
            theNewEnabled = this.getNewEnabled();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "newEnabled", theNewEnabled), currentHashCode, theNewEnabled);
        }
        {
            EditEnabled theEditEnabled;
            theEditEnabled = this.getEditEnabled();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "editEnabled", theEditEnabled), currentHashCode, theEditEnabled);
        }
        {
            DeleteEnabled theDeleteEnabled;
            theDeleteEnabled = this.getDeleteEnabled();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "deleteEnabled", theDeleteEnabled), currentHashCode, theDeleteEnabled);
        }
        {
            CloneEnabled theCloneEnabled;
            theCloneEnabled = this.getCloneEnabled();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "cloneEnabled", theCloneEnabled), currentHashCode, theCloneEnabled);
        }
        {
            DynamicRowColor theDynamicRowColor;
            theDynamicRowColor = this.getDynamicRowColor();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dynamicRowColor", theDynamicRowColor), currentHashCode, theDynamicRowColor);
        }
        {
            List<Property> theProperty;
            theProperty = (((this.property!= null)&&(!this.property.isEmpty()))?this.getProperty():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "property", theProperty), currentHashCode, theProperty);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String theEntity;
            theEntity = this.getEntity();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entity", theEntity), currentHashCode, theEntity);
        }
        {
            Boolean theEnabled;
            theEnabled = this.getEnabled();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "enabled", theEnabled), currentHashCode, theEnabled);
        }
        {
            Boolean theAutonumbersorting;
            theAutonumbersorting = this.getAutonumbersorting();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "autonumbersorting", theAutonumbersorting), currentHashCode, theAutonumbersorting);
        }
        {
            Boolean theNotcloneable;
            theNotcloneable = this.getNotcloneable();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "notcloneable", theNotcloneable), currentHashCode, theNotcloneable);
        }
        {
            Boolean theMultieditable;
            theMultieditable = this.getMultieditable();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "multieditable", theMultieditable), currentHashCode, theMultieditable);
        }
        {
            String theToolbarorientation;
            theToolbarorientation = this.getToolbarorientation();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "toolbarorientation", theToolbarorientation), currentHashCode, theToolbarorientation);
        }
        {
            Boolean theStatusbar;
            theStatusbar = this.getStatusbar();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "statusbar", theStatusbar), currentHashCode, theStatusbar);
        }
        {
            Boolean theOpaque;
            theOpaque = this.getOpaque();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "opaque", theOpaque), currentHashCode, theOpaque);
        }
        {
            Boolean theOpenDetailsWithTabRecycling;
            theOpenDetailsWithTabRecycling = this.getOpenDetailsWithTabRecycling();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "openDetailsWithTabRecycling", theOpenDetailsWithTabRecycling), currentHashCode, theOpenDetailsWithTabRecycling);
        }
        {
            String theControllertype;
            theControllertype = this.getControllertype();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "controllertype", theControllertype), currentHashCode, theControllertype);
        }
        {
            String theUniqueMastercolumn;
            theUniqueMastercolumn = this.getUniqueMastercolumn();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "uniqueMastercolumn", theUniqueMastercolumn), currentHashCode, theUniqueMastercolumn);
        }
        {
            String theForeignkeyfieldToParent;
            theForeignkeyfieldToParent = this.getForeignkeyfieldToParent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "foreignkeyfieldToParent", theForeignkeyfieldToParent), currentHashCode, theForeignkeyfieldToParent);
        }
        {
            String theParentSubform;
            theParentSubform = this.getParentSubform();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "parentSubform", theParentSubform), currentHashCode, theParentSubform);
        }
        {
            Boolean theDynamicCellHeightsDefault;
            theDynamicCellHeightsDefault = this.getDynamicCellHeightsDefault();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dynamicCellHeightsDefault", theDynamicCellHeightsDefault), currentHashCode, theDynamicCellHeightsDefault);
        }
        {
            Boolean theIgnoreSubLayout;
            theIgnoreSubLayout = this.getIgnoreSubLayout();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ignoreSubLayout", theIgnoreSubLayout), currentHashCode, theIgnoreSubLayout);
        }
        {
            String theMaxEntries;
            theMaxEntries = this.getMaxEntries();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "maxEntries", theMaxEntries), currentHashCode, theMaxEntries);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Subform) {
            final Subform copy = ((Subform) draftCopy);
            if (this.layoutconstraints!= null) {
                JAXBElement<?> sourceLayoutconstraints;
                sourceLayoutconstraints = this.getLayoutconstraints();
                @SuppressWarnings("unchecked")
                JAXBElement<?> copyLayoutconstraints = ((JAXBElement<?> ) strategy.copy(LocatorUtils.property(locator, "layoutconstraints", sourceLayoutconstraints), sourceLayoutconstraints));
                copy.setLayoutconstraints(copyLayoutconstraints);
            } else {
                copy.layoutconstraints = null;
            }
            if (this.clearBorder!= null) {
                ClearBorder sourceClearBorder;
                sourceClearBorder = this.getClearBorder();
                ClearBorder copyClearBorder = ((ClearBorder) strategy.copy(LocatorUtils.property(locator, "clearBorder", sourceClearBorder), sourceClearBorder));
                copy.setClearBorder(copyClearBorder);
            } else {
                copy.clearBorder = null;
            }
            if ((this.border!= null)&&(!this.border.isEmpty())) {
                List<JAXBElement<?>> sourceBorder;
                sourceBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
                @SuppressWarnings("unchecked")
                List<JAXBElement<?>> copyBorder = ((List<JAXBElement<?>> ) strategy.copy(LocatorUtils.property(locator, "border", sourceBorder), sourceBorder));
                copy.border = null;
                if (copyBorder!= null) {
                    List<JAXBElement<?>> uniqueBorderl = copy.getBorder();
                    uniqueBorderl.addAll(copyBorder);
                }
            } else {
                copy.border = null;
            }
            if (this.minimumSize!= null) {
                MinimumSize sourceMinimumSize;
                sourceMinimumSize = this.getMinimumSize();
                MinimumSize copyMinimumSize = ((MinimumSize) strategy.copy(LocatorUtils.property(locator, "minimumSize", sourceMinimumSize), sourceMinimumSize));
                copy.setMinimumSize(copyMinimumSize);
            } else {
                copy.minimumSize = null;
            }
            if (this.preferredSize!= null) {
                PreferredSize sourcePreferredSize;
                sourcePreferredSize = this.getPreferredSize();
                PreferredSize copyPreferredSize = ((PreferredSize) strategy.copy(LocatorUtils.property(locator, "preferredSize", sourcePreferredSize), sourcePreferredSize));
                copy.setPreferredSize(copyPreferredSize);
            } else {
                copy.preferredSize = null;
            }
            if (this.strictSize!= null) {
                StrictSize sourceStrictSize;
                sourceStrictSize = this.getStrictSize();
                StrictSize copyStrictSize = ((StrictSize) strategy.copy(LocatorUtils.property(locator, "strictSize", sourceStrictSize), sourceStrictSize));
                copy.setStrictSize(copyStrictSize);
            } else {
                copy.strictSize = null;
            }
            if (this.font!= null) {
                Font sourceFont;
                sourceFont = this.getFont();
                Font copyFont = ((Font) strategy.copy(LocatorUtils.property(locator, "font", sourceFont), sourceFont));
                copy.setFont(copyFont);
            } else {
                copy.font = null;
            }
            if (this.initialSortingOrder!= null) {
                InitialSortingOrder sourceInitialSortingOrder;
                sourceInitialSortingOrder = this.getInitialSortingOrder();
                InitialSortingOrder copyInitialSortingOrder = ((InitialSortingOrder) strategy.copy(LocatorUtils.property(locator, "initialSortingOrder", sourceInitialSortingOrder), sourceInitialSortingOrder));
                copy.setInitialSortingOrder(copyInitialSortingOrder);
            } else {
                copy.initialSortingOrder = null;
            }
            if (this.background!= null) {
                Background sourceBackground;
                sourceBackground = this.getBackground();
                Background copyBackground = ((Background) strategy.copy(LocatorUtils.property(locator, "background", sourceBackground), sourceBackground));
                copy.setBackground(copyBackground);
            } else {
                copy.background = null;
            }
            if (this.description!= null) {
                String sourceDescription;
                sourceDescription = this.getDescription();
                String copyDescription = ((String) strategy.copy(LocatorUtils.property(locator, "description", sourceDescription), sourceDescription));
                copy.setDescription(copyDescription);
            } else {
                copy.description = null;
            }
            if ((this.subformColumn!= null)&&(!this.subformColumn.isEmpty())) {
                List<SubformColumn> sourceSubformColumn;
                sourceSubformColumn = (((this.subformColumn!= null)&&(!this.subformColumn.isEmpty()))?this.getSubformColumn():null);
                @SuppressWarnings("unchecked")
                List<SubformColumn> copySubformColumn = ((List<SubformColumn> ) strategy.copy(LocatorUtils.property(locator, "subformColumn", sourceSubformColumn), sourceSubformColumn));
                copy.subformColumn = null;
                if (copySubformColumn!= null) {
                    List<SubformColumn> uniqueSubformColumnl = copy.getSubformColumn();
                    uniqueSubformColumnl.addAll(copySubformColumn);
                }
            } else {
                copy.subformColumn = null;
            }
            if (this.newEnabled!= null) {
                NewEnabled sourceNewEnabled;
                sourceNewEnabled = this.getNewEnabled();
                NewEnabled copyNewEnabled = ((NewEnabled) strategy.copy(LocatorUtils.property(locator, "newEnabled", sourceNewEnabled), sourceNewEnabled));
                copy.setNewEnabled(copyNewEnabled);
            } else {
                copy.newEnabled = null;
            }
            if (this.editEnabled!= null) {
                EditEnabled sourceEditEnabled;
                sourceEditEnabled = this.getEditEnabled();
                EditEnabled copyEditEnabled = ((EditEnabled) strategy.copy(LocatorUtils.property(locator, "editEnabled", sourceEditEnabled), sourceEditEnabled));
                copy.setEditEnabled(copyEditEnabled);
            } else {
                copy.editEnabled = null;
            }
            if (this.deleteEnabled!= null) {
                DeleteEnabled sourceDeleteEnabled;
                sourceDeleteEnabled = this.getDeleteEnabled();
                DeleteEnabled copyDeleteEnabled = ((DeleteEnabled) strategy.copy(LocatorUtils.property(locator, "deleteEnabled", sourceDeleteEnabled), sourceDeleteEnabled));
                copy.setDeleteEnabled(copyDeleteEnabled);
            } else {
                copy.deleteEnabled = null;
            }
            if (this.cloneEnabled!= null) {
                CloneEnabled sourceCloneEnabled;
                sourceCloneEnabled = this.getCloneEnabled();
                CloneEnabled copyCloneEnabled = ((CloneEnabled) strategy.copy(LocatorUtils.property(locator, "cloneEnabled", sourceCloneEnabled), sourceCloneEnabled));
                copy.setCloneEnabled(copyCloneEnabled);
            } else {
                copy.cloneEnabled = null;
            }
            if (this.dynamicRowColor!= null) {
                DynamicRowColor sourceDynamicRowColor;
                sourceDynamicRowColor = this.getDynamicRowColor();
                DynamicRowColor copyDynamicRowColor = ((DynamicRowColor) strategy.copy(LocatorUtils.property(locator, "dynamicRowColor", sourceDynamicRowColor), sourceDynamicRowColor));
                copy.setDynamicRowColor(copyDynamicRowColor);
            } else {
                copy.dynamicRowColor = null;
            }
            if ((this.property!= null)&&(!this.property.isEmpty())) {
                List<Property> sourceProperty;
                sourceProperty = (((this.property!= null)&&(!this.property.isEmpty()))?this.getProperty():null);
                @SuppressWarnings("unchecked")
                List<Property> copyProperty = ((List<Property> ) strategy.copy(LocatorUtils.property(locator, "property", sourceProperty), sourceProperty));
                copy.property = null;
                if (copyProperty!= null) {
                    List<Property> uniquePropertyl = copy.getProperty();
                    uniquePropertyl.addAll(copyProperty);
                }
            } else {
                copy.property = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.entity!= null) {
                String sourceEntity;
                sourceEntity = this.getEntity();
                String copyEntity = ((String) strategy.copy(LocatorUtils.property(locator, "entity", sourceEntity), sourceEntity));
                copy.setEntity(copyEntity);
            } else {
                copy.entity = null;
            }
            if (this.enabled!= null) {
                Boolean sourceEnabled;
                sourceEnabled = this.getEnabled();
                Boolean copyEnabled = ((Boolean) strategy.copy(LocatorUtils.property(locator, "enabled", sourceEnabled), sourceEnabled));
                copy.setEnabled(copyEnabled);
            } else {
                copy.enabled = null;
            }
            if (this.autonumbersorting!= null) {
                Boolean sourceAutonumbersorting;
                sourceAutonumbersorting = this.getAutonumbersorting();
                Boolean copyAutonumbersorting = ((Boolean) strategy.copy(LocatorUtils.property(locator, "autonumbersorting", sourceAutonumbersorting), sourceAutonumbersorting));
                copy.setAutonumbersorting(copyAutonumbersorting);
            } else {
                copy.autonumbersorting = null;
            }
            if (this.notcloneable!= null) {
                Boolean sourceNotcloneable;
                sourceNotcloneable = this.getNotcloneable();
                Boolean copyNotcloneable = ((Boolean) strategy.copy(LocatorUtils.property(locator, "notcloneable", sourceNotcloneable), sourceNotcloneable));
                copy.setNotcloneable(copyNotcloneable);
            } else {
                copy.notcloneable = null;
            }
            if (this.multieditable!= null) {
                Boolean sourceMultieditable;
                sourceMultieditable = this.getMultieditable();
                Boolean copyMultieditable = ((Boolean) strategy.copy(LocatorUtils.property(locator, "multieditable", sourceMultieditable), sourceMultieditable));
                copy.setMultieditable(copyMultieditable);
            } else {
                copy.multieditable = null;
            }
            if (this.toolbarorientation!= null) {
                String sourceToolbarorientation;
                sourceToolbarorientation = this.getToolbarorientation();
                String copyToolbarorientation = ((String) strategy.copy(LocatorUtils.property(locator, "toolbarorientation", sourceToolbarorientation), sourceToolbarorientation));
                copy.setToolbarorientation(copyToolbarorientation);
            } else {
                copy.toolbarorientation = null;
            }
            if (this.statusbar!= null) {
                Boolean sourceStatusbar;
                sourceStatusbar = this.getStatusbar();
                Boolean copyStatusbar = ((Boolean) strategy.copy(LocatorUtils.property(locator, "statusbar", sourceStatusbar), sourceStatusbar));
                copy.setStatusbar(copyStatusbar);
            } else {
                copy.statusbar = null;
            }
            if (this.opaque!= null) {
                Boolean sourceOpaque;
                sourceOpaque = this.getOpaque();
                Boolean copyOpaque = ((Boolean) strategy.copy(LocatorUtils.property(locator, "opaque", sourceOpaque), sourceOpaque));
                copy.setOpaque(copyOpaque);
            } else {
                copy.opaque = null;
            }
            if (this.openDetailsWithTabRecycling!= null) {
                Boolean sourceOpenDetailsWithTabRecycling;
                sourceOpenDetailsWithTabRecycling = this.getOpenDetailsWithTabRecycling();
                Boolean copyOpenDetailsWithTabRecycling = ((Boolean) strategy.copy(LocatorUtils.property(locator, "openDetailsWithTabRecycling", sourceOpenDetailsWithTabRecycling), sourceOpenDetailsWithTabRecycling));
                copy.setOpenDetailsWithTabRecycling(copyOpenDetailsWithTabRecycling);
            } else {
                copy.openDetailsWithTabRecycling = null;
            }
            if (this.controllertype!= null) {
                String sourceControllertype;
                sourceControllertype = this.getControllertype();
                String copyControllertype = ((String) strategy.copy(LocatorUtils.property(locator, "controllertype", sourceControllertype), sourceControllertype));
                copy.setControllertype(copyControllertype);
            } else {
                copy.controllertype = null;
            }
            if (this.uniqueMastercolumn!= null) {
                String sourceUniqueMastercolumn;
                sourceUniqueMastercolumn = this.getUniqueMastercolumn();
                String copyUniqueMastercolumn = ((String) strategy.copy(LocatorUtils.property(locator, "uniqueMastercolumn", sourceUniqueMastercolumn), sourceUniqueMastercolumn));
                copy.setUniqueMastercolumn(copyUniqueMastercolumn);
            } else {
                copy.uniqueMastercolumn = null;
            }
            if (this.foreignkeyfieldToParent!= null) {
                String sourceForeignkeyfieldToParent;
                sourceForeignkeyfieldToParent = this.getForeignkeyfieldToParent();
                String copyForeignkeyfieldToParent = ((String) strategy.copy(LocatorUtils.property(locator, "foreignkeyfieldToParent", sourceForeignkeyfieldToParent), sourceForeignkeyfieldToParent));
                copy.setForeignkeyfieldToParent(copyForeignkeyfieldToParent);
            } else {
                copy.foreignkeyfieldToParent = null;
            }
            if (this.parentSubform!= null) {
                String sourceParentSubform;
                sourceParentSubform = this.getParentSubform();
                String copyParentSubform = ((String) strategy.copy(LocatorUtils.property(locator, "parentSubform", sourceParentSubform), sourceParentSubform));
                copy.setParentSubform(copyParentSubform);
            } else {
                copy.parentSubform = null;
            }
            if (this.dynamicCellHeightsDefault!= null) {
                Boolean sourceDynamicCellHeightsDefault;
                sourceDynamicCellHeightsDefault = this.getDynamicCellHeightsDefault();
                Boolean copyDynamicCellHeightsDefault = ((Boolean) strategy.copy(LocatorUtils.property(locator, "dynamicCellHeightsDefault", sourceDynamicCellHeightsDefault), sourceDynamicCellHeightsDefault));
                copy.setDynamicCellHeightsDefault(copyDynamicCellHeightsDefault);
            } else {
                copy.dynamicCellHeightsDefault = null;
            }
            if (this.ignoreSubLayout!= null) {
                Boolean sourceIgnoreSubLayout;
                sourceIgnoreSubLayout = this.getIgnoreSubLayout();
                Boolean copyIgnoreSubLayout = ((Boolean) strategy.copy(LocatorUtils.property(locator, "ignoreSubLayout", sourceIgnoreSubLayout), sourceIgnoreSubLayout));
                copy.setIgnoreSubLayout(copyIgnoreSubLayout);
            } else {
                copy.ignoreSubLayout = null;
            }
            if (this.maxEntries!= null) {
                String sourceMaxEntries;
                sourceMaxEntries = this.getMaxEntries();
                String copyMaxEntries = ((String) strategy.copy(LocatorUtils.property(locator, "maxEntries", sourceMaxEntries), sourceMaxEntries));
                copy.setMaxEntries(copyMaxEntries);
            } else {
                copy.maxEntries = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Subform();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Subform.Builder<_B> _other) {
        _other.layoutconstraints = this.layoutconstraints;
        _other.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.newCopyBuilder(_other));
        if (this.border == null) {
            _other.border = null;
        } else {
            _other.border = new ArrayList<Buildable>();
            for (JAXBElement<?> _item: this.border) {
                _other.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
            }
        }
        _other.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.newCopyBuilder(_other));
        _other.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.newCopyBuilder(_other));
        _other.strictSize = ((this.strictSize == null)?null:this.strictSize.newCopyBuilder(_other));
        _other.font = ((this.font == null)?null:this.font.newCopyBuilder(_other));
        _other.initialSortingOrder = ((this.initialSortingOrder == null)?null:this.initialSortingOrder.newCopyBuilder(_other));
        _other.background = ((this.background == null)?null:this.background.newCopyBuilder(_other));
        _other.description = this.description;
        if (this.subformColumn == null) {
            _other.subformColumn = null;
        } else {
            _other.subformColumn = new ArrayList<SubformColumn.Builder<Subform.Builder<_B>>>();
            for (SubformColumn _item: this.subformColumn) {
                _other.subformColumn.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.newEnabled = ((this.newEnabled == null)?null:this.newEnabled.newCopyBuilder(_other));
        _other.editEnabled = ((this.editEnabled == null)?null:this.editEnabled.newCopyBuilder(_other));
        _other.deleteEnabled = ((this.deleteEnabled == null)?null:this.deleteEnabled.newCopyBuilder(_other));
        _other.cloneEnabled = ((this.cloneEnabled == null)?null:this.cloneEnabled.newCopyBuilder(_other));
        _other.dynamicRowColor = ((this.dynamicRowColor == null)?null:this.dynamicRowColor.newCopyBuilder(_other));
        if (this.property == null) {
            _other.property = null;
        } else {
            _other.property = new ArrayList<Property.Builder<Subform.Builder<_B>>>();
            for (Property _item: this.property) {
                _other.property.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.name = this.name;
        _other.entity = this.entity;
        _other.enabled = this.enabled;
        _other.autonumbersorting = this.autonumbersorting;
        _other.notcloneable = this.notcloneable;
        _other.multieditable = this.multieditable;
        _other.toolbarorientation = this.toolbarorientation;
        _other.statusbar = this.statusbar;
        _other.opaque = this.opaque;
        _other.openDetailsWithTabRecycling = this.openDetailsWithTabRecycling;
        _other.controllertype = this.controllertype;
        _other.uniqueMastercolumn = this.uniqueMastercolumn;
        _other.foreignkeyfieldToParent = this.foreignkeyfieldToParent;
        _other.parentSubform = this.parentSubform;
        _other.dynamicCellHeightsDefault = this.dynamicCellHeightsDefault;
        _other.ignoreSubLayout = this.ignoreSubLayout;
        _other.maxEntries = this.maxEntries;
    }

    public<_B >Subform.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Subform.Builder<_B>(_parentBuilder, this, true);
    }

    public Subform.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Subform.Builder<Void> builder() {
        return new Subform.Builder<Void>(null, null, false);
    }

    public static<_B >Subform.Builder<_B> copyOf(final Subform _other) {
        final Subform.Builder<_B> _newBuilder = new Subform.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Subform.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree layoutconstraintsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("layoutconstraints"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(layoutconstraintsPropertyTree!= null):((layoutconstraintsPropertyTree == null)||(!layoutconstraintsPropertyTree.isLeaf())))) {
            _other.layoutconstraints = this.layoutconstraints;
        }
        final PropertyTree clearBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("clearBorder"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(clearBorderPropertyTree!= null):((clearBorderPropertyTree == null)||(!clearBorderPropertyTree.isLeaf())))) {
            _other.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.newCopyBuilder(_other, clearBorderPropertyTree, _propertyTreeUse));
        }
        final PropertyTree borderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("border"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(borderPropertyTree!= null):((borderPropertyTree == null)||(!borderPropertyTree.isLeaf())))) {
            if (this.border == null) {
                _other.border = null;
            } else {
                _other.border = new ArrayList<Buildable>();
                for (JAXBElement<?> _item: this.border) {
                    _other.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                }
            }
        }
        final PropertyTree minimumSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minimumSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minimumSizePropertyTree!= null):((minimumSizePropertyTree == null)||(!minimumSizePropertyTree.isLeaf())))) {
            _other.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.newCopyBuilder(_other, minimumSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree preferredSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("preferredSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(preferredSizePropertyTree!= null):((preferredSizePropertyTree == null)||(!preferredSizePropertyTree.isLeaf())))) {
            _other.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.newCopyBuilder(_other, preferredSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree strictSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("strictSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(strictSizePropertyTree!= null):((strictSizePropertyTree == null)||(!strictSizePropertyTree.isLeaf())))) {
            _other.strictSize = ((this.strictSize == null)?null:this.strictSize.newCopyBuilder(_other, strictSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree fontPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("font"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fontPropertyTree!= null):((fontPropertyTree == null)||(!fontPropertyTree.isLeaf())))) {
            _other.font = ((this.font == null)?null:this.font.newCopyBuilder(_other, fontPropertyTree, _propertyTreeUse));
        }
        final PropertyTree initialSortingOrderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("initialSortingOrder"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(initialSortingOrderPropertyTree!= null):((initialSortingOrderPropertyTree == null)||(!initialSortingOrderPropertyTree.isLeaf())))) {
            _other.initialSortingOrder = ((this.initialSortingOrder == null)?null:this.initialSortingOrder.newCopyBuilder(_other, initialSortingOrderPropertyTree, _propertyTreeUse));
        }
        final PropertyTree backgroundPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("background"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(backgroundPropertyTree!= null):((backgroundPropertyTree == null)||(!backgroundPropertyTree.isLeaf())))) {
            _other.background = ((this.background == null)?null:this.background.newCopyBuilder(_other, backgroundPropertyTree, _propertyTreeUse));
        }
        final PropertyTree descriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("description"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(descriptionPropertyTree!= null):((descriptionPropertyTree == null)||(!descriptionPropertyTree.isLeaf())))) {
            _other.description = this.description;
        }
        final PropertyTree subformColumnPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("subformColumn"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(subformColumnPropertyTree!= null):((subformColumnPropertyTree == null)||(!subformColumnPropertyTree.isLeaf())))) {
            if (this.subformColumn == null) {
                _other.subformColumn = null;
            } else {
                _other.subformColumn = new ArrayList<SubformColumn.Builder<Subform.Builder<_B>>>();
                for (SubformColumn _item: this.subformColumn) {
                    _other.subformColumn.add(((_item == null)?null:_item.newCopyBuilder(_other, subformColumnPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree newEnabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("newEnabled"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(newEnabledPropertyTree!= null):((newEnabledPropertyTree == null)||(!newEnabledPropertyTree.isLeaf())))) {
            _other.newEnabled = ((this.newEnabled == null)?null:this.newEnabled.newCopyBuilder(_other, newEnabledPropertyTree, _propertyTreeUse));
        }
        final PropertyTree editEnabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("editEnabled"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(editEnabledPropertyTree!= null):((editEnabledPropertyTree == null)||(!editEnabledPropertyTree.isLeaf())))) {
            _other.editEnabled = ((this.editEnabled == null)?null:this.editEnabled.newCopyBuilder(_other, editEnabledPropertyTree, _propertyTreeUse));
        }
        final PropertyTree deleteEnabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("deleteEnabled"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(deleteEnabledPropertyTree!= null):((deleteEnabledPropertyTree == null)||(!deleteEnabledPropertyTree.isLeaf())))) {
            _other.deleteEnabled = ((this.deleteEnabled == null)?null:this.deleteEnabled.newCopyBuilder(_other, deleteEnabledPropertyTree, _propertyTreeUse));
        }
        final PropertyTree cloneEnabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("cloneEnabled"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(cloneEnabledPropertyTree!= null):((cloneEnabledPropertyTree == null)||(!cloneEnabledPropertyTree.isLeaf())))) {
            _other.cloneEnabled = ((this.cloneEnabled == null)?null:this.cloneEnabled.newCopyBuilder(_other, cloneEnabledPropertyTree, _propertyTreeUse));
        }
        final PropertyTree dynamicRowColorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dynamicRowColor"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dynamicRowColorPropertyTree!= null):((dynamicRowColorPropertyTree == null)||(!dynamicRowColorPropertyTree.isLeaf())))) {
            _other.dynamicRowColor = ((this.dynamicRowColor == null)?null:this.dynamicRowColor.newCopyBuilder(_other, dynamicRowColorPropertyTree, _propertyTreeUse));
        }
        final PropertyTree propertyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("property"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(propertyPropertyTree!= null):((propertyPropertyTree == null)||(!propertyPropertyTree.isLeaf())))) {
            if (this.property == null) {
                _other.property = null;
            } else {
                _other.property = new ArrayList<Property.Builder<Subform.Builder<_B>>>();
                for (Property _item: this.property) {
                    _other.property.add(((_item == null)?null:_item.newCopyBuilder(_other, propertyPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree entityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entity"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityPropertyTree!= null):((entityPropertyTree == null)||(!entityPropertyTree.isLeaf())))) {
            _other.entity = this.entity;
        }
        final PropertyTree enabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("enabled"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(enabledPropertyTree!= null):((enabledPropertyTree == null)||(!enabledPropertyTree.isLeaf())))) {
            _other.enabled = this.enabled;
        }
        final PropertyTree autonumbersortingPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("autonumbersorting"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(autonumbersortingPropertyTree!= null):((autonumbersortingPropertyTree == null)||(!autonumbersortingPropertyTree.isLeaf())))) {
            _other.autonumbersorting = this.autonumbersorting;
        }
        final PropertyTree notcloneablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("notcloneable"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(notcloneablePropertyTree!= null):((notcloneablePropertyTree == null)||(!notcloneablePropertyTree.isLeaf())))) {
            _other.notcloneable = this.notcloneable;
        }
        final PropertyTree multieditablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("multieditable"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(multieditablePropertyTree!= null):((multieditablePropertyTree == null)||(!multieditablePropertyTree.isLeaf())))) {
            _other.multieditable = this.multieditable;
        }
        final PropertyTree toolbarorientationPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("toolbarorientation"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(toolbarorientationPropertyTree!= null):((toolbarorientationPropertyTree == null)||(!toolbarorientationPropertyTree.isLeaf())))) {
            _other.toolbarorientation = this.toolbarorientation;
        }
        final PropertyTree statusbarPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("statusbar"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(statusbarPropertyTree!= null):((statusbarPropertyTree == null)||(!statusbarPropertyTree.isLeaf())))) {
            _other.statusbar = this.statusbar;
        }
        final PropertyTree opaquePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("opaque"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(opaquePropertyTree!= null):((opaquePropertyTree == null)||(!opaquePropertyTree.isLeaf())))) {
            _other.opaque = this.opaque;
        }
        final PropertyTree openDetailsWithTabRecyclingPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("openDetailsWithTabRecycling"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(openDetailsWithTabRecyclingPropertyTree!= null):((openDetailsWithTabRecyclingPropertyTree == null)||(!openDetailsWithTabRecyclingPropertyTree.isLeaf())))) {
            _other.openDetailsWithTabRecycling = this.openDetailsWithTabRecycling;
        }
        final PropertyTree controllertypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("controllertype"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(controllertypePropertyTree!= null):((controllertypePropertyTree == null)||(!controllertypePropertyTree.isLeaf())))) {
            _other.controllertype = this.controllertype;
        }
        final PropertyTree uniqueMastercolumnPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("uniqueMastercolumn"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(uniqueMastercolumnPropertyTree!= null):((uniqueMastercolumnPropertyTree == null)||(!uniqueMastercolumnPropertyTree.isLeaf())))) {
            _other.uniqueMastercolumn = this.uniqueMastercolumn;
        }
        final PropertyTree foreignkeyfieldToParentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("foreignkeyfieldToParent"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(foreignkeyfieldToParentPropertyTree!= null):((foreignkeyfieldToParentPropertyTree == null)||(!foreignkeyfieldToParentPropertyTree.isLeaf())))) {
            _other.foreignkeyfieldToParent = this.foreignkeyfieldToParent;
        }
        final PropertyTree parentSubformPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("parentSubform"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(parentSubformPropertyTree!= null):((parentSubformPropertyTree == null)||(!parentSubformPropertyTree.isLeaf())))) {
            _other.parentSubform = this.parentSubform;
        }
        final PropertyTree dynamicCellHeightsDefaultPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dynamicCellHeightsDefault"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dynamicCellHeightsDefaultPropertyTree!= null):((dynamicCellHeightsDefaultPropertyTree == null)||(!dynamicCellHeightsDefaultPropertyTree.isLeaf())))) {
            _other.dynamicCellHeightsDefault = this.dynamicCellHeightsDefault;
        }
        final PropertyTree ignoreSubLayoutPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("ignoreSubLayout"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(ignoreSubLayoutPropertyTree!= null):((ignoreSubLayoutPropertyTree == null)||(!ignoreSubLayoutPropertyTree.isLeaf())))) {
            _other.ignoreSubLayout = this.ignoreSubLayout;
        }
        final PropertyTree maxEntriesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("maxEntries"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(maxEntriesPropertyTree!= null):((maxEntriesPropertyTree == null)||(!maxEntriesPropertyTree.isLeaf())))) {
            _other.maxEntries = this.maxEntries;
        }
    }

    public<_B >Subform.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Subform.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Subform.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Subform.Builder<_B> copyOf(final Subform _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Subform.Builder<_B> _newBuilder = new Subform.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Subform.Builder<Void> copyExcept(final Subform _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Subform.Builder<Void> copyOnly(final Subform _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Subform _storedValue;
        private JAXBElement<?> layoutconstraints;
        private ClearBorder.Builder<Subform.Builder<_B>> clearBorder;
        private List<Buildable> border;
        private MinimumSize.Builder<Subform.Builder<_B>> minimumSize;
        private PreferredSize.Builder<Subform.Builder<_B>> preferredSize;
        private StrictSize.Builder<Subform.Builder<_B>> strictSize;
        private Font.Builder<Subform.Builder<_B>> font;
        private InitialSortingOrder.Builder<Subform.Builder<_B>> initialSortingOrder;
        private Background.Builder<Subform.Builder<_B>> background;
        private String description;
        private List<SubformColumn.Builder<Subform.Builder<_B>>> subformColumn;
        private NewEnabled.Builder<Subform.Builder<_B>> newEnabled;
        private EditEnabled.Builder<Subform.Builder<_B>> editEnabled;
        private DeleteEnabled.Builder<Subform.Builder<_B>> deleteEnabled;
        private CloneEnabled.Builder<Subform.Builder<_B>> cloneEnabled;
        private DynamicRowColor.Builder<Subform.Builder<_B>> dynamicRowColor;
        private List<Property.Builder<Subform.Builder<_B>>> property;
        private String name;
        private String entity;
        private Boolean enabled;
        private Boolean autonumbersorting;
        private Boolean notcloneable;
        private Boolean multieditable;
        private String toolbarorientation;
        private Boolean statusbar;
        private Boolean opaque;
        private Boolean openDetailsWithTabRecycling;
        private String controllertype;
        private String uniqueMastercolumn;
        private String foreignkeyfieldToParent;
        private String parentSubform;
        private Boolean dynamicCellHeightsDefault;
        private Boolean ignoreSubLayout;
        private String maxEntries;

        public Builder(final _B _parentBuilder, final Subform _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.layoutconstraints = _other.layoutconstraints;
                    this.clearBorder = ((_other.clearBorder == null)?null:_other.clearBorder.newCopyBuilder(this));
                    if (_other.border == null) {
                        this.border = null;
                    } else {
                        this.border = new ArrayList<Buildable>();
                        for (JAXBElement<?> _item: _other.border) {
                            this.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                        }
                    }
                    this.minimumSize = ((_other.minimumSize == null)?null:_other.minimumSize.newCopyBuilder(this));
                    this.preferredSize = ((_other.preferredSize == null)?null:_other.preferredSize.newCopyBuilder(this));
                    this.strictSize = ((_other.strictSize == null)?null:_other.strictSize.newCopyBuilder(this));
                    this.font = ((_other.font == null)?null:_other.font.newCopyBuilder(this));
                    this.initialSortingOrder = ((_other.initialSortingOrder == null)?null:_other.initialSortingOrder.newCopyBuilder(this));
                    this.background = ((_other.background == null)?null:_other.background.newCopyBuilder(this));
                    this.description = _other.description;
                    if (_other.subformColumn == null) {
                        this.subformColumn = null;
                    } else {
                        this.subformColumn = new ArrayList<SubformColumn.Builder<Subform.Builder<_B>>>();
                        for (SubformColumn _item: _other.subformColumn) {
                            this.subformColumn.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.newEnabled = ((_other.newEnabled == null)?null:_other.newEnabled.newCopyBuilder(this));
                    this.editEnabled = ((_other.editEnabled == null)?null:_other.editEnabled.newCopyBuilder(this));
                    this.deleteEnabled = ((_other.deleteEnabled == null)?null:_other.deleteEnabled.newCopyBuilder(this));
                    this.cloneEnabled = ((_other.cloneEnabled == null)?null:_other.cloneEnabled.newCopyBuilder(this));
                    this.dynamicRowColor = ((_other.dynamicRowColor == null)?null:_other.dynamicRowColor.newCopyBuilder(this));
                    if (_other.property == null) {
                        this.property = null;
                    } else {
                        this.property = new ArrayList<Property.Builder<Subform.Builder<_B>>>();
                        for (Property _item: _other.property) {
                            this.property.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.name = _other.name;
                    this.entity = _other.entity;
                    this.enabled = _other.enabled;
                    this.autonumbersorting = _other.autonumbersorting;
                    this.notcloneable = _other.notcloneable;
                    this.multieditable = _other.multieditable;
                    this.toolbarorientation = _other.toolbarorientation;
                    this.statusbar = _other.statusbar;
                    this.opaque = _other.opaque;
                    this.openDetailsWithTabRecycling = _other.openDetailsWithTabRecycling;
                    this.controllertype = _other.controllertype;
                    this.uniqueMastercolumn = _other.uniqueMastercolumn;
                    this.foreignkeyfieldToParent = _other.foreignkeyfieldToParent;
                    this.parentSubform = _other.parentSubform;
                    this.dynamicCellHeightsDefault = _other.dynamicCellHeightsDefault;
                    this.ignoreSubLayout = _other.ignoreSubLayout;
                    this.maxEntries = _other.maxEntries;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Subform _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree layoutconstraintsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("layoutconstraints"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(layoutconstraintsPropertyTree!= null):((layoutconstraintsPropertyTree == null)||(!layoutconstraintsPropertyTree.isLeaf())))) {
                        this.layoutconstraints = _other.layoutconstraints;
                    }
                    final PropertyTree clearBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("clearBorder"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(clearBorderPropertyTree!= null):((clearBorderPropertyTree == null)||(!clearBorderPropertyTree.isLeaf())))) {
                        this.clearBorder = ((_other.clearBorder == null)?null:_other.clearBorder.newCopyBuilder(this, clearBorderPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree borderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("border"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(borderPropertyTree!= null):((borderPropertyTree == null)||(!borderPropertyTree.isLeaf())))) {
                        if (_other.border == null) {
                            this.border = null;
                        } else {
                            this.border = new ArrayList<Buildable>();
                            for (JAXBElement<?> _item: _other.border) {
                                this.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                            }
                        }
                    }
                    final PropertyTree minimumSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minimumSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minimumSizePropertyTree!= null):((minimumSizePropertyTree == null)||(!minimumSizePropertyTree.isLeaf())))) {
                        this.minimumSize = ((_other.minimumSize == null)?null:_other.minimumSize.newCopyBuilder(this, minimumSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree preferredSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("preferredSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(preferredSizePropertyTree!= null):((preferredSizePropertyTree == null)||(!preferredSizePropertyTree.isLeaf())))) {
                        this.preferredSize = ((_other.preferredSize == null)?null:_other.preferredSize.newCopyBuilder(this, preferredSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree strictSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("strictSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(strictSizePropertyTree!= null):((strictSizePropertyTree == null)||(!strictSizePropertyTree.isLeaf())))) {
                        this.strictSize = ((_other.strictSize == null)?null:_other.strictSize.newCopyBuilder(this, strictSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree fontPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("font"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fontPropertyTree!= null):((fontPropertyTree == null)||(!fontPropertyTree.isLeaf())))) {
                        this.font = ((_other.font == null)?null:_other.font.newCopyBuilder(this, fontPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree initialSortingOrderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("initialSortingOrder"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(initialSortingOrderPropertyTree!= null):((initialSortingOrderPropertyTree == null)||(!initialSortingOrderPropertyTree.isLeaf())))) {
                        this.initialSortingOrder = ((_other.initialSortingOrder == null)?null:_other.initialSortingOrder.newCopyBuilder(this, initialSortingOrderPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree backgroundPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("background"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(backgroundPropertyTree!= null):((backgroundPropertyTree == null)||(!backgroundPropertyTree.isLeaf())))) {
                        this.background = ((_other.background == null)?null:_other.background.newCopyBuilder(this, backgroundPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree descriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("description"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(descriptionPropertyTree!= null):((descriptionPropertyTree == null)||(!descriptionPropertyTree.isLeaf())))) {
                        this.description = _other.description;
                    }
                    final PropertyTree subformColumnPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("subformColumn"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(subformColumnPropertyTree!= null):((subformColumnPropertyTree == null)||(!subformColumnPropertyTree.isLeaf())))) {
                        if (_other.subformColumn == null) {
                            this.subformColumn = null;
                        } else {
                            this.subformColumn = new ArrayList<SubformColumn.Builder<Subform.Builder<_B>>>();
                            for (SubformColumn _item: _other.subformColumn) {
                                this.subformColumn.add(((_item == null)?null:_item.newCopyBuilder(this, subformColumnPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree newEnabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("newEnabled"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(newEnabledPropertyTree!= null):((newEnabledPropertyTree == null)||(!newEnabledPropertyTree.isLeaf())))) {
                        this.newEnabled = ((_other.newEnabled == null)?null:_other.newEnabled.newCopyBuilder(this, newEnabledPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree editEnabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("editEnabled"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(editEnabledPropertyTree!= null):((editEnabledPropertyTree == null)||(!editEnabledPropertyTree.isLeaf())))) {
                        this.editEnabled = ((_other.editEnabled == null)?null:_other.editEnabled.newCopyBuilder(this, editEnabledPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree deleteEnabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("deleteEnabled"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(deleteEnabledPropertyTree!= null):((deleteEnabledPropertyTree == null)||(!deleteEnabledPropertyTree.isLeaf())))) {
                        this.deleteEnabled = ((_other.deleteEnabled == null)?null:_other.deleteEnabled.newCopyBuilder(this, deleteEnabledPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree cloneEnabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("cloneEnabled"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(cloneEnabledPropertyTree!= null):((cloneEnabledPropertyTree == null)||(!cloneEnabledPropertyTree.isLeaf())))) {
                        this.cloneEnabled = ((_other.cloneEnabled == null)?null:_other.cloneEnabled.newCopyBuilder(this, cloneEnabledPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree dynamicRowColorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dynamicRowColor"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dynamicRowColorPropertyTree!= null):((dynamicRowColorPropertyTree == null)||(!dynamicRowColorPropertyTree.isLeaf())))) {
                        this.dynamicRowColor = ((_other.dynamicRowColor == null)?null:_other.dynamicRowColor.newCopyBuilder(this, dynamicRowColorPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree propertyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("property"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(propertyPropertyTree!= null):((propertyPropertyTree == null)||(!propertyPropertyTree.isLeaf())))) {
                        if (_other.property == null) {
                            this.property = null;
                        } else {
                            this.property = new ArrayList<Property.Builder<Subform.Builder<_B>>>();
                            for (Property _item: _other.property) {
                                this.property.add(((_item == null)?null:_item.newCopyBuilder(this, propertyPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree entityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entity"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityPropertyTree!= null):((entityPropertyTree == null)||(!entityPropertyTree.isLeaf())))) {
                        this.entity = _other.entity;
                    }
                    final PropertyTree enabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("enabled"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(enabledPropertyTree!= null):((enabledPropertyTree == null)||(!enabledPropertyTree.isLeaf())))) {
                        this.enabled = _other.enabled;
                    }
                    final PropertyTree autonumbersortingPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("autonumbersorting"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(autonumbersortingPropertyTree!= null):((autonumbersortingPropertyTree == null)||(!autonumbersortingPropertyTree.isLeaf())))) {
                        this.autonumbersorting = _other.autonumbersorting;
                    }
                    final PropertyTree notcloneablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("notcloneable"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(notcloneablePropertyTree!= null):((notcloneablePropertyTree == null)||(!notcloneablePropertyTree.isLeaf())))) {
                        this.notcloneable = _other.notcloneable;
                    }
                    final PropertyTree multieditablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("multieditable"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(multieditablePropertyTree!= null):((multieditablePropertyTree == null)||(!multieditablePropertyTree.isLeaf())))) {
                        this.multieditable = _other.multieditable;
                    }
                    final PropertyTree toolbarorientationPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("toolbarorientation"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(toolbarorientationPropertyTree!= null):((toolbarorientationPropertyTree == null)||(!toolbarorientationPropertyTree.isLeaf())))) {
                        this.toolbarorientation = _other.toolbarorientation;
                    }
                    final PropertyTree statusbarPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("statusbar"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(statusbarPropertyTree!= null):((statusbarPropertyTree == null)||(!statusbarPropertyTree.isLeaf())))) {
                        this.statusbar = _other.statusbar;
                    }
                    final PropertyTree opaquePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("opaque"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(opaquePropertyTree!= null):((opaquePropertyTree == null)||(!opaquePropertyTree.isLeaf())))) {
                        this.opaque = _other.opaque;
                    }
                    final PropertyTree openDetailsWithTabRecyclingPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("openDetailsWithTabRecycling"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(openDetailsWithTabRecyclingPropertyTree!= null):((openDetailsWithTabRecyclingPropertyTree == null)||(!openDetailsWithTabRecyclingPropertyTree.isLeaf())))) {
                        this.openDetailsWithTabRecycling = _other.openDetailsWithTabRecycling;
                    }
                    final PropertyTree controllertypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("controllertype"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(controllertypePropertyTree!= null):((controllertypePropertyTree == null)||(!controllertypePropertyTree.isLeaf())))) {
                        this.controllertype = _other.controllertype;
                    }
                    final PropertyTree uniqueMastercolumnPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("uniqueMastercolumn"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(uniqueMastercolumnPropertyTree!= null):((uniqueMastercolumnPropertyTree == null)||(!uniqueMastercolumnPropertyTree.isLeaf())))) {
                        this.uniqueMastercolumn = _other.uniqueMastercolumn;
                    }
                    final PropertyTree foreignkeyfieldToParentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("foreignkeyfieldToParent"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(foreignkeyfieldToParentPropertyTree!= null):((foreignkeyfieldToParentPropertyTree == null)||(!foreignkeyfieldToParentPropertyTree.isLeaf())))) {
                        this.foreignkeyfieldToParent = _other.foreignkeyfieldToParent;
                    }
                    final PropertyTree parentSubformPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("parentSubform"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(parentSubformPropertyTree!= null):((parentSubformPropertyTree == null)||(!parentSubformPropertyTree.isLeaf())))) {
                        this.parentSubform = _other.parentSubform;
                    }
                    final PropertyTree dynamicCellHeightsDefaultPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dynamicCellHeightsDefault"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dynamicCellHeightsDefaultPropertyTree!= null):((dynamicCellHeightsDefaultPropertyTree == null)||(!dynamicCellHeightsDefaultPropertyTree.isLeaf())))) {
                        this.dynamicCellHeightsDefault = _other.dynamicCellHeightsDefault;
                    }
                    final PropertyTree ignoreSubLayoutPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("ignoreSubLayout"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(ignoreSubLayoutPropertyTree!= null):((ignoreSubLayoutPropertyTree == null)||(!ignoreSubLayoutPropertyTree.isLeaf())))) {
                        this.ignoreSubLayout = _other.ignoreSubLayout;
                    }
                    final PropertyTree maxEntriesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("maxEntries"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(maxEntriesPropertyTree!= null):((maxEntriesPropertyTree == null)||(!maxEntriesPropertyTree.isLeaf())))) {
                        this.maxEntries = _other.maxEntries;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Subform >_P init(final _P _product) {
            _product.layoutconstraints = this.layoutconstraints;
            _product.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.build());
            if (this.border!= null) {
                final List<JAXBElement<?>> border = new ArrayList<JAXBElement<?>>(this.border.size());
                for (Buildable _item: this.border) {
                    border.add(((JAXBElement<?> ) _item.build()));
                }
                _product.border = border;
            }
            _product.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.build());
            _product.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.build());
            _product.strictSize = ((this.strictSize == null)?null:this.strictSize.build());
            _product.font = ((this.font == null)?null:this.font.build());
            _product.initialSortingOrder = ((this.initialSortingOrder == null)?null:this.initialSortingOrder.build());
            _product.background = ((this.background == null)?null:this.background.build());
            _product.description = this.description;
            if (this.subformColumn!= null) {
                final List<SubformColumn> subformColumn = new ArrayList<SubformColumn>(this.subformColumn.size());
                for (SubformColumn.Builder<Subform.Builder<_B>> _item: this.subformColumn) {
                    subformColumn.add(_item.build());
                }
                _product.subformColumn = subformColumn;
            }
            _product.newEnabled = ((this.newEnabled == null)?null:this.newEnabled.build());
            _product.editEnabled = ((this.editEnabled == null)?null:this.editEnabled.build());
            _product.deleteEnabled = ((this.deleteEnabled == null)?null:this.deleteEnabled.build());
            _product.cloneEnabled = ((this.cloneEnabled == null)?null:this.cloneEnabled.build());
            _product.dynamicRowColor = ((this.dynamicRowColor == null)?null:this.dynamicRowColor.build());
            if (this.property!= null) {
                final List<Property> property = new ArrayList<Property>(this.property.size());
                for (Property.Builder<Subform.Builder<_B>> _item: this.property) {
                    property.add(_item.build());
                }
                _product.property = property;
            }
            _product.name = this.name;
            _product.entity = this.entity;
            _product.enabled = this.enabled;
            _product.autonumbersorting = this.autonumbersorting;
            _product.notcloneable = this.notcloneable;
            _product.multieditable = this.multieditable;
            _product.toolbarorientation = this.toolbarorientation;
            _product.statusbar = this.statusbar;
            _product.opaque = this.opaque;
            _product.openDetailsWithTabRecycling = this.openDetailsWithTabRecycling;
            _product.controllertype = this.controllertype;
            _product.uniqueMastercolumn = this.uniqueMastercolumn;
            _product.foreignkeyfieldToParent = this.foreignkeyfieldToParent;
            _product.parentSubform = this.parentSubform;
            _product.dynamicCellHeightsDefault = this.dynamicCellHeightsDefault;
            _product.ignoreSubLayout = this.ignoreSubLayout;
            _product.maxEntries = this.maxEntries;
            return _product;
        }

        /**
         * Sets the new value of "layoutconstraints" (any previous value will be replaced)
         * 
         * @param layoutconstraints
         *     New value of the "layoutconstraints" property.
         */
        public Subform.Builder<_B> withLayoutconstraints(final JAXBElement<?> layoutconstraints) {
            this.layoutconstraints = layoutconstraints;
            return this;
        }

        /**
         * Sets the new value of "clearBorder" (any previous value will be replaced)
         * 
         * @param clearBorder
         *     New value of the "clearBorder" property.
         */
        public Subform.Builder<_B> withClearBorder(final ClearBorder clearBorder) {
            this.clearBorder = ((clearBorder == null)?null:new ClearBorder.Builder<Subform.Builder<_B>>(this, clearBorder, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "clearBorder" property.
         * Use {@link org.nuclos.schema.layout.layoutml.ClearBorder.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "clearBorder" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.ClearBorder.Builder#end()} to return to the current builder.
         */
        public ClearBorder.Builder<? extends Subform.Builder<_B>> withClearBorder() {
            if (this.clearBorder!= null) {
                return this.clearBorder;
            }
            return this.clearBorder = new ClearBorder.Builder<Subform.Builder<_B>>(this, null, false);
        }

        /**
         * Adds the given items to the value of "border"
         * 
         * @param border
         *     Items to add to the value of the "border" property
         */
        public Subform.Builder<_B> addBorder(final Iterable<? extends JAXBElement<?>> border) {
            if (border!= null) {
                if (this.border == null) {
                    this.border = new ArrayList<Buildable>();
                }
                for (JAXBElement<?> _item: border) {
                    this.border.add(new Buildable.PrimitiveBuildable(_item));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "border" (any previous value will be replaced)
         * 
         * @param border
         *     New value of the "border" property.
         */
        public Subform.Builder<_B> withBorder(final Iterable<? extends JAXBElement<?>> border) {
            if (this.border!= null) {
                this.border.clear();
            }
            return addBorder(border);
        }

        /**
         * Adds the given items to the value of "border"
         * 
         * @param border
         *     Items to add to the value of the "border" property
         */
        public Subform.Builder<_B> addBorder(JAXBElement<?> ... border) {
            addBorder(Arrays.asList(border));
            return this;
        }

        /**
         * Sets the new value of "border" (any previous value will be replaced)
         * 
         * @param border
         *     New value of the "border" property.
         */
        public Subform.Builder<_B> withBorder(JAXBElement<?> ... border) {
            withBorder(Arrays.asList(border));
            return this;
        }

        /**
         * Sets the new value of "minimumSize" (any previous value will be replaced)
         * 
         * @param minimumSize
         *     New value of the "minimumSize" property.
         */
        public Subform.Builder<_B> withMinimumSize(final MinimumSize minimumSize) {
            this.minimumSize = ((minimumSize == null)?null:new MinimumSize.Builder<Subform.Builder<_B>>(this, minimumSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "minimumSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.MinimumSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "minimumSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.MinimumSize.Builder#end()} to return to the current builder.
         */
        public MinimumSize.Builder<? extends Subform.Builder<_B>> withMinimumSize() {
            if (this.minimumSize!= null) {
                return this.minimumSize;
            }
            return this.minimumSize = new MinimumSize.Builder<Subform.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "preferredSize" (any previous value will be replaced)
         * 
         * @param preferredSize
         *     New value of the "preferredSize" property.
         */
        public Subform.Builder<_B> withPreferredSize(final PreferredSize preferredSize) {
            this.preferredSize = ((preferredSize == null)?null:new PreferredSize.Builder<Subform.Builder<_B>>(this, preferredSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "preferredSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.PreferredSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "preferredSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.PreferredSize.Builder#end()} to return to the current builder.
         */
        public PreferredSize.Builder<? extends Subform.Builder<_B>> withPreferredSize() {
            if (this.preferredSize!= null) {
                return this.preferredSize;
            }
            return this.preferredSize = new PreferredSize.Builder<Subform.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "strictSize" (any previous value will be replaced)
         * 
         * @param strictSize
         *     New value of the "strictSize" property.
         */
        public Subform.Builder<_B> withStrictSize(final StrictSize strictSize) {
            this.strictSize = ((strictSize == null)?null:new StrictSize.Builder<Subform.Builder<_B>>(this, strictSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "strictSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.StrictSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "strictSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.StrictSize.Builder#end()} to return to the current builder.
         */
        public StrictSize.Builder<? extends Subform.Builder<_B>> withStrictSize() {
            if (this.strictSize!= null) {
                return this.strictSize;
            }
            return this.strictSize = new StrictSize.Builder<Subform.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "font" (any previous value will be replaced)
         * 
         * @param font
         *     New value of the "font" property.
         */
        public Subform.Builder<_B> withFont(final Font font) {
            this.font = ((font == null)?null:new Font.Builder<Subform.Builder<_B>>(this, font, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "font" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Font.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "font" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Font.Builder#end()} to return to the current builder.
         */
        public Font.Builder<? extends Subform.Builder<_B>> withFont() {
            if (this.font!= null) {
                return this.font;
            }
            return this.font = new Font.Builder<Subform.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "initialSortingOrder" (any previous value will be replaced)
         * 
         * @param initialSortingOrder
         *     New value of the "initialSortingOrder" property.
         */
        public Subform.Builder<_B> withInitialSortingOrder(final InitialSortingOrder initialSortingOrder) {
            this.initialSortingOrder = ((initialSortingOrder == null)?null:new InitialSortingOrder.Builder<Subform.Builder<_B>>(this, initialSortingOrder, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "initialSortingOrder" property.
         * Use {@link org.nuclos.schema.layout.layoutml.InitialSortingOrder.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "initialSortingOrder" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.InitialSortingOrder.Builder#end()} to return to the current builder.
         */
        public InitialSortingOrder.Builder<? extends Subform.Builder<_B>> withInitialSortingOrder() {
            if (this.initialSortingOrder!= null) {
                return this.initialSortingOrder;
            }
            return this.initialSortingOrder = new InitialSortingOrder.Builder<Subform.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "background" (any previous value will be replaced)
         * 
         * @param background
         *     New value of the "background" property.
         */
        public Subform.Builder<_B> withBackground(final Background background) {
            this.background = ((background == null)?null:new Background.Builder<Subform.Builder<_B>>(this, background, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "background" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Background.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "background" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Background.Builder#end()} to return to the current builder.
         */
        public Background.Builder<? extends Subform.Builder<_B>> withBackground() {
            if (this.background!= null) {
                return this.background;
            }
            return this.background = new Background.Builder<Subform.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "description" (any previous value will be replaced)
         * 
         * @param description
         *     New value of the "description" property.
         */
        public Subform.Builder<_B> withDescription(final String description) {
            this.description = description;
            return this;
        }

        /**
         * Adds the given items to the value of "subformColumn"
         * 
         * @param subformColumn
         *     Items to add to the value of the "subformColumn" property
         */
        public Subform.Builder<_B> addSubformColumn(final Iterable<? extends SubformColumn> subformColumn) {
            if (subformColumn!= null) {
                if (this.subformColumn == null) {
                    this.subformColumn = new ArrayList<SubformColumn.Builder<Subform.Builder<_B>>>();
                }
                for (SubformColumn _item: subformColumn) {
                    this.subformColumn.add(new SubformColumn.Builder<Subform.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "subformColumn" (any previous value will be replaced)
         * 
         * @param subformColumn
         *     New value of the "subformColumn" property.
         */
        public Subform.Builder<_B> withSubformColumn(final Iterable<? extends SubformColumn> subformColumn) {
            if (this.subformColumn!= null) {
                this.subformColumn.clear();
            }
            return addSubformColumn(subformColumn);
        }

        /**
         * Adds the given items to the value of "subformColumn"
         * 
         * @param subformColumn
         *     Items to add to the value of the "subformColumn" property
         */
        public Subform.Builder<_B> addSubformColumn(SubformColumn... subformColumn) {
            addSubformColumn(Arrays.asList(subformColumn));
            return this;
        }

        /**
         * Sets the new value of "subformColumn" (any previous value will be replaced)
         * 
         * @param subformColumn
         *     New value of the "subformColumn" property.
         */
        public Subform.Builder<_B> withSubformColumn(SubformColumn... subformColumn) {
            withSubformColumn(Arrays.asList(subformColumn));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "SubformColumn" property.
         * Use {@link org.nuclos.schema.layout.layoutml.SubformColumn.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "SubformColumn" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.SubformColumn.Builder#end()} to return to the current builder.
         */
        public SubformColumn.Builder<? extends Subform.Builder<_B>> addSubformColumn() {
            if (this.subformColumn == null) {
                this.subformColumn = new ArrayList<SubformColumn.Builder<Subform.Builder<_B>>>();
            }
            final SubformColumn.Builder<Subform.Builder<_B>> subformColumn_Builder = new SubformColumn.Builder<Subform.Builder<_B>>(this, null, false);
            this.subformColumn.add(subformColumn_Builder);
            return subformColumn_Builder;
        }

        /**
         * Sets the new value of "newEnabled" (any previous value will be replaced)
         * 
         * @param newEnabled
         *     New value of the "newEnabled" property.
         */
        public Subform.Builder<_B> withNewEnabled(final NewEnabled newEnabled) {
            this.newEnabled = ((newEnabled == null)?null:new NewEnabled.Builder<Subform.Builder<_B>>(this, newEnabled, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "newEnabled" property.
         * Use {@link org.nuclos.schema.layout.layoutml.NewEnabled.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "newEnabled" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.NewEnabled.Builder#end()} to return to the current builder.
         */
        public NewEnabled.Builder<? extends Subform.Builder<_B>> withNewEnabled() {
            if (this.newEnabled!= null) {
                return this.newEnabled;
            }
            return this.newEnabled = new NewEnabled.Builder<Subform.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "editEnabled" (any previous value will be replaced)
         * 
         * @param editEnabled
         *     New value of the "editEnabled" property.
         */
        public Subform.Builder<_B> withEditEnabled(final EditEnabled editEnabled) {
            this.editEnabled = ((editEnabled == null)?null:new EditEnabled.Builder<Subform.Builder<_B>>(this, editEnabled, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "editEnabled" property.
         * Use {@link org.nuclos.schema.layout.layoutml.EditEnabled.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "editEnabled" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.EditEnabled.Builder#end()} to return to the current builder.
         */
        public EditEnabled.Builder<? extends Subform.Builder<_B>> withEditEnabled() {
            if (this.editEnabled!= null) {
                return this.editEnabled;
            }
            return this.editEnabled = new EditEnabled.Builder<Subform.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "deleteEnabled" (any previous value will be replaced)
         * 
         * @param deleteEnabled
         *     New value of the "deleteEnabled" property.
         */
        public Subform.Builder<_B> withDeleteEnabled(final DeleteEnabled deleteEnabled) {
            this.deleteEnabled = ((deleteEnabled == null)?null:new DeleteEnabled.Builder<Subform.Builder<_B>>(this, deleteEnabled, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "deleteEnabled" property.
         * Use {@link org.nuclos.schema.layout.layoutml.DeleteEnabled.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "deleteEnabled" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.DeleteEnabled.Builder#end()} to return to the current builder.
         */
        public DeleteEnabled.Builder<? extends Subform.Builder<_B>> withDeleteEnabled() {
            if (this.deleteEnabled!= null) {
                return this.deleteEnabled;
            }
            return this.deleteEnabled = new DeleteEnabled.Builder<Subform.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "cloneEnabled" (any previous value will be replaced)
         * 
         * @param cloneEnabled
         *     New value of the "cloneEnabled" property.
         */
        public Subform.Builder<_B> withCloneEnabled(final CloneEnabled cloneEnabled) {
            this.cloneEnabled = ((cloneEnabled == null)?null:new CloneEnabled.Builder<Subform.Builder<_B>>(this, cloneEnabled, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "cloneEnabled" property.
         * Use {@link org.nuclos.schema.layout.layoutml.CloneEnabled.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "cloneEnabled" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.CloneEnabled.Builder#end()} to return to the current builder.
         */
        public CloneEnabled.Builder<? extends Subform.Builder<_B>> withCloneEnabled() {
            if (this.cloneEnabled!= null) {
                return this.cloneEnabled;
            }
            return this.cloneEnabled = new CloneEnabled.Builder<Subform.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "dynamicRowColor" (any previous value will be replaced)
         * 
         * @param dynamicRowColor
         *     New value of the "dynamicRowColor" property.
         */
        public Subform.Builder<_B> withDynamicRowColor(final DynamicRowColor dynamicRowColor) {
            this.dynamicRowColor = ((dynamicRowColor == null)?null:new DynamicRowColor.Builder<Subform.Builder<_B>>(this, dynamicRowColor, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "dynamicRowColor" property.
         * Use {@link org.nuclos.schema.layout.layoutml.DynamicRowColor.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "dynamicRowColor" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.DynamicRowColor.Builder#end()} to return to the current builder.
         */
        public DynamicRowColor.Builder<? extends Subform.Builder<_B>> withDynamicRowColor() {
            if (this.dynamicRowColor!= null) {
                return this.dynamicRowColor;
            }
            return this.dynamicRowColor = new DynamicRowColor.Builder<Subform.Builder<_B>>(this, null, false);
        }

        /**
         * Adds the given items to the value of "property"
         * 
         * @param property
         *     Items to add to the value of the "property" property
         */
        public Subform.Builder<_B> addProperty(final Iterable<? extends Property> property) {
            if (property!= null) {
                if (this.property == null) {
                    this.property = new ArrayList<Property.Builder<Subform.Builder<_B>>>();
                }
                for (Property _item: property) {
                    this.property.add(new Property.Builder<Subform.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "property" (any previous value will be replaced)
         * 
         * @param property
         *     New value of the "property" property.
         */
        public Subform.Builder<_B> withProperty(final Iterable<? extends Property> property) {
            if (this.property!= null) {
                this.property.clear();
            }
            return addProperty(property);
        }

        /**
         * Adds the given items to the value of "property"
         * 
         * @param property
         *     Items to add to the value of the "property" property
         */
        public Subform.Builder<_B> addProperty(Property... property) {
            addProperty(Arrays.asList(property));
            return this;
        }

        /**
         * Sets the new value of "property" (any previous value will be replaced)
         * 
         * @param property
         *     New value of the "property" property.
         */
        public Subform.Builder<_B> withProperty(Property... property) {
            withProperty(Arrays.asList(property));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Property" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Property.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Property" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Property.Builder#end()} to return to the current builder.
         */
        public Property.Builder<? extends Subform.Builder<_B>> addProperty() {
            if (this.property == null) {
                this.property = new ArrayList<Property.Builder<Subform.Builder<_B>>>();
            }
            final Property.Builder<Subform.Builder<_B>> property_Builder = new Property.Builder<Subform.Builder<_B>>(this, null, false);
            this.property.add(property_Builder);
            return property_Builder;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public Subform.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "entity" (any previous value will be replaced)
         * 
         * @param entity
         *     New value of the "entity" property.
         */
        public Subform.Builder<_B> withEntity(final String entity) {
            this.entity = entity;
            return this;
        }

        /**
         * Sets the new value of "enabled" (any previous value will be replaced)
         * 
         * @param enabled
         *     New value of the "enabled" property.
         */
        public Subform.Builder<_B> withEnabled(final Boolean enabled) {
            this.enabled = enabled;
            return this;
        }

        /**
         * Sets the new value of "autonumbersorting" (any previous value will be replaced)
         * 
         * @param autonumbersorting
         *     New value of the "autonumbersorting" property.
         */
        public Subform.Builder<_B> withAutonumbersorting(final Boolean autonumbersorting) {
            this.autonumbersorting = autonumbersorting;
            return this;
        }

        /**
         * Sets the new value of "notcloneable" (any previous value will be replaced)
         * 
         * @param notcloneable
         *     New value of the "notcloneable" property.
         */
        public Subform.Builder<_B> withNotcloneable(final Boolean notcloneable) {
            this.notcloneable = notcloneable;
            return this;
        }

        /**
         * Sets the new value of "multieditable" (any previous value will be replaced)
         * 
         * @param multieditable
         *     New value of the "multieditable" property.
         */
        public Subform.Builder<_B> withMultieditable(final Boolean multieditable) {
            this.multieditable = multieditable;
            return this;
        }

        /**
         * Sets the new value of "toolbarorientation" (any previous value will be replaced)
         * 
         * @param toolbarorientation
         *     New value of the "toolbarorientation" property.
         */
        public Subform.Builder<_B> withToolbarorientation(final String toolbarorientation) {
            this.toolbarorientation = toolbarorientation;
            return this;
        }

        /**
         * Sets the new value of "statusbar" (any previous value will be replaced)
         * 
         * @param statusbar
         *     New value of the "statusbar" property.
         */
        public Subform.Builder<_B> withStatusbar(final Boolean statusbar) {
            this.statusbar = statusbar;
            return this;
        }

        /**
         * Sets the new value of "opaque" (any previous value will be replaced)
         * 
         * @param opaque
         *     New value of the "opaque" property.
         */
        public Subform.Builder<_B> withOpaque(final Boolean opaque) {
            this.opaque = opaque;
            return this;
        }

        /**
         * Sets the new value of "openDetailsWithTabRecycling" (any previous value will be replaced)
         * 
         * @param openDetailsWithTabRecycling
         *     New value of the "openDetailsWithTabRecycling" property.
         */
        public Subform.Builder<_B> withOpenDetailsWithTabRecycling(final Boolean openDetailsWithTabRecycling) {
            this.openDetailsWithTabRecycling = openDetailsWithTabRecycling;
            return this;
        }

        /**
         * Sets the new value of "controllertype" (any previous value will be replaced)
         * 
         * @param controllertype
         *     New value of the "controllertype" property.
         */
        public Subform.Builder<_B> withControllertype(final String controllertype) {
            this.controllertype = controllertype;
            return this;
        }

        /**
         * Sets the new value of "uniqueMastercolumn" (any previous value will be replaced)
         * 
         * @param uniqueMastercolumn
         *     New value of the "uniqueMastercolumn" property.
         */
        public Subform.Builder<_B> withUniqueMastercolumn(final String uniqueMastercolumn) {
            this.uniqueMastercolumn = uniqueMastercolumn;
            return this;
        }

        /**
         * Sets the new value of "foreignkeyfieldToParent" (any previous value will be replaced)
         * 
         * @param foreignkeyfieldToParent
         *     New value of the "foreignkeyfieldToParent" property.
         */
        public Subform.Builder<_B> withForeignkeyfieldToParent(final String foreignkeyfieldToParent) {
            this.foreignkeyfieldToParent = foreignkeyfieldToParent;
            return this;
        }

        /**
         * Sets the new value of "parentSubform" (any previous value will be replaced)
         * 
         * @param parentSubform
         *     New value of the "parentSubform" property.
         */
        public Subform.Builder<_B> withParentSubform(final String parentSubform) {
            this.parentSubform = parentSubform;
            return this;
        }

        /**
         * Sets the new value of "dynamicCellHeightsDefault" (any previous value will be replaced)
         * 
         * @param dynamicCellHeightsDefault
         *     New value of the "dynamicCellHeightsDefault" property.
         */
        public Subform.Builder<_B> withDynamicCellHeightsDefault(final Boolean dynamicCellHeightsDefault) {
            this.dynamicCellHeightsDefault = dynamicCellHeightsDefault;
            return this;
        }

        /**
         * Sets the new value of "ignoreSubLayout" (any previous value will be replaced)
         * 
         * @param ignoreSubLayout
         *     New value of the "ignoreSubLayout" property.
         */
        public Subform.Builder<_B> withIgnoreSubLayout(final Boolean ignoreSubLayout) {
            this.ignoreSubLayout = ignoreSubLayout;
            return this;
        }

        /**
         * Sets the new value of "maxEntries" (any previous value will be replaced)
         * 
         * @param maxEntries
         *     New value of the "maxEntries" property.
         */
        public Subform.Builder<_B> withMaxEntries(final String maxEntries) {
            this.maxEntries = maxEntries;
            return this;
        }

        @Override
        public Subform build() {
            if (_storedValue == null) {
                return this.init(new Subform());
            } else {
                return ((Subform) _storedValue);
            }
        }

        public Subform.Builder<_B> copyOf(final Subform _other) {
            _other.copyTo(this);
            return this;
        }

        public Subform.Builder<_B> copyOf(final Subform.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Subform.Selector<Subform.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Subform.Select _root() {
            return new Subform.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> layoutconstraints = null;
        private ClearBorder.Selector<TRoot, Subform.Selector<TRoot, TParent>> clearBorder = null;
        private com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> border = null;
        private MinimumSize.Selector<TRoot, Subform.Selector<TRoot, TParent>> minimumSize = null;
        private PreferredSize.Selector<TRoot, Subform.Selector<TRoot, TParent>> preferredSize = null;
        private StrictSize.Selector<TRoot, Subform.Selector<TRoot, TParent>> strictSize = null;
        private Font.Selector<TRoot, Subform.Selector<TRoot, TParent>> font = null;
        private InitialSortingOrder.Selector<TRoot, Subform.Selector<TRoot, TParent>> initialSortingOrder = null;
        private Background.Selector<TRoot, Subform.Selector<TRoot, TParent>> background = null;
        private com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> description = null;
        private SubformColumn.Selector<TRoot, Subform.Selector<TRoot, TParent>> subformColumn = null;
        private NewEnabled.Selector<TRoot, Subform.Selector<TRoot, TParent>> newEnabled = null;
        private EditEnabled.Selector<TRoot, Subform.Selector<TRoot, TParent>> editEnabled = null;
        private DeleteEnabled.Selector<TRoot, Subform.Selector<TRoot, TParent>> deleteEnabled = null;
        private CloneEnabled.Selector<TRoot, Subform.Selector<TRoot, TParent>> cloneEnabled = null;
        private DynamicRowColor.Selector<TRoot, Subform.Selector<TRoot, TParent>> dynamicRowColor = null;
        private Property.Selector<TRoot, Subform.Selector<TRoot, TParent>> property = null;
        private com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> entity = null;
        private com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> enabled = null;
        private com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> autonumbersorting = null;
        private com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> notcloneable = null;
        private com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> multieditable = null;
        private com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> toolbarorientation = null;
        private com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> statusbar = null;
        private com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> opaque = null;
        private com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> openDetailsWithTabRecycling = null;
        private com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> controllertype = null;
        private com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> uniqueMastercolumn = null;
        private com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> foreignkeyfieldToParent = null;
        private com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> parentSubform = null;
        private com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> dynamicCellHeightsDefault = null;
        private com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> ignoreSubLayout = null;
        private com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> maxEntries = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.layoutconstraints!= null) {
                products.put("layoutconstraints", this.layoutconstraints.init());
            }
            if (this.clearBorder!= null) {
                products.put("clearBorder", this.clearBorder.init());
            }
            if (this.border!= null) {
                products.put("border", this.border.init());
            }
            if (this.minimumSize!= null) {
                products.put("minimumSize", this.minimumSize.init());
            }
            if (this.preferredSize!= null) {
                products.put("preferredSize", this.preferredSize.init());
            }
            if (this.strictSize!= null) {
                products.put("strictSize", this.strictSize.init());
            }
            if (this.font!= null) {
                products.put("font", this.font.init());
            }
            if (this.initialSortingOrder!= null) {
                products.put("initialSortingOrder", this.initialSortingOrder.init());
            }
            if (this.background!= null) {
                products.put("background", this.background.init());
            }
            if (this.description!= null) {
                products.put("description", this.description.init());
            }
            if (this.subformColumn!= null) {
                products.put("subformColumn", this.subformColumn.init());
            }
            if (this.newEnabled!= null) {
                products.put("newEnabled", this.newEnabled.init());
            }
            if (this.editEnabled!= null) {
                products.put("editEnabled", this.editEnabled.init());
            }
            if (this.deleteEnabled!= null) {
                products.put("deleteEnabled", this.deleteEnabled.init());
            }
            if (this.cloneEnabled!= null) {
                products.put("cloneEnabled", this.cloneEnabled.init());
            }
            if (this.dynamicRowColor!= null) {
                products.put("dynamicRowColor", this.dynamicRowColor.init());
            }
            if (this.property!= null) {
                products.put("property", this.property.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.entity!= null) {
                products.put("entity", this.entity.init());
            }
            if (this.enabled!= null) {
                products.put("enabled", this.enabled.init());
            }
            if (this.autonumbersorting!= null) {
                products.put("autonumbersorting", this.autonumbersorting.init());
            }
            if (this.notcloneable!= null) {
                products.put("notcloneable", this.notcloneable.init());
            }
            if (this.multieditable!= null) {
                products.put("multieditable", this.multieditable.init());
            }
            if (this.toolbarorientation!= null) {
                products.put("toolbarorientation", this.toolbarorientation.init());
            }
            if (this.statusbar!= null) {
                products.put("statusbar", this.statusbar.init());
            }
            if (this.opaque!= null) {
                products.put("opaque", this.opaque.init());
            }
            if (this.openDetailsWithTabRecycling!= null) {
                products.put("openDetailsWithTabRecycling", this.openDetailsWithTabRecycling.init());
            }
            if (this.controllertype!= null) {
                products.put("controllertype", this.controllertype.init());
            }
            if (this.uniqueMastercolumn!= null) {
                products.put("uniqueMastercolumn", this.uniqueMastercolumn.init());
            }
            if (this.foreignkeyfieldToParent!= null) {
                products.put("foreignkeyfieldToParent", this.foreignkeyfieldToParent.init());
            }
            if (this.parentSubform!= null) {
                products.put("parentSubform", this.parentSubform.init());
            }
            if (this.dynamicCellHeightsDefault!= null) {
                products.put("dynamicCellHeightsDefault", this.dynamicCellHeightsDefault.init());
            }
            if (this.ignoreSubLayout!= null) {
                products.put("ignoreSubLayout", this.ignoreSubLayout.init());
            }
            if (this.maxEntries!= null) {
                products.put("maxEntries", this.maxEntries.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> layoutconstraints() {
            return ((this.layoutconstraints == null)?this.layoutconstraints = new com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "layoutconstraints"):this.layoutconstraints);
        }

        public ClearBorder.Selector<TRoot, Subform.Selector<TRoot, TParent>> clearBorder() {
            return ((this.clearBorder == null)?this.clearBorder = new ClearBorder.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "clearBorder"):this.clearBorder);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> border() {
            return ((this.border == null)?this.border = new com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "border"):this.border);
        }

        public MinimumSize.Selector<TRoot, Subform.Selector<TRoot, TParent>> minimumSize() {
            return ((this.minimumSize == null)?this.minimumSize = new MinimumSize.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "minimumSize"):this.minimumSize);
        }

        public PreferredSize.Selector<TRoot, Subform.Selector<TRoot, TParent>> preferredSize() {
            return ((this.preferredSize == null)?this.preferredSize = new PreferredSize.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "preferredSize"):this.preferredSize);
        }

        public StrictSize.Selector<TRoot, Subform.Selector<TRoot, TParent>> strictSize() {
            return ((this.strictSize == null)?this.strictSize = new StrictSize.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "strictSize"):this.strictSize);
        }

        public Font.Selector<TRoot, Subform.Selector<TRoot, TParent>> font() {
            return ((this.font == null)?this.font = new Font.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "font"):this.font);
        }

        public InitialSortingOrder.Selector<TRoot, Subform.Selector<TRoot, TParent>> initialSortingOrder() {
            return ((this.initialSortingOrder == null)?this.initialSortingOrder = new InitialSortingOrder.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "initialSortingOrder"):this.initialSortingOrder);
        }

        public Background.Selector<TRoot, Subform.Selector<TRoot, TParent>> background() {
            return ((this.background == null)?this.background = new Background.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "background"):this.background);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> description() {
            return ((this.description == null)?this.description = new com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "description"):this.description);
        }

        public SubformColumn.Selector<TRoot, Subform.Selector<TRoot, TParent>> subformColumn() {
            return ((this.subformColumn == null)?this.subformColumn = new SubformColumn.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "subformColumn"):this.subformColumn);
        }

        public NewEnabled.Selector<TRoot, Subform.Selector<TRoot, TParent>> newEnabled() {
            return ((this.newEnabled == null)?this.newEnabled = new NewEnabled.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "newEnabled"):this.newEnabled);
        }

        public EditEnabled.Selector<TRoot, Subform.Selector<TRoot, TParent>> editEnabled() {
            return ((this.editEnabled == null)?this.editEnabled = new EditEnabled.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "editEnabled"):this.editEnabled);
        }

        public DeleteEnabled.Selector<TRoot, Subform.Selector<TRoot, TParent>> deleteEnabled() {
            return ((this.deleteEnabled == null)?this.deleteEnabled = new DeleteEnabled.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "deleteEnabled"):this.deleteEnabled);
        }

        public CloneEnabled.Selector<TRoot, Subform.Selector<TRoot, TParent>> cloneEnabled() {
            return ((this.cloneEnabled == null)?this.cloneEnabled = new CloneEnabled.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "cloneEnabled"):this.cloneEnabled);
        }

        public DynamicRowColor.Selector<TRoot, Subform.Selector<TRoot, TParent>> dynamicRowColor() {
            return ((this.dynamicRowColor == null)?this.dynamicRowColor = new DynamicRowColor.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "dynamicRowColor"):this.dynamicRowColor);
        }

        public Property.Selector<TRoot, Subform.Selector<TRoot, TParent>> property() {
            return ((this.property == null)?this.property = new Property.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "property"):this.property);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> entity() {
            return ((this.entity == null)?this.entity = new com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "entity"):this.entity);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> enabled() {
            return ((this.enabled == null)?this.enabled = new com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "enabled"):this.enabled);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> autonumbersorting() {
            return ((this.autonumbersorting == null)?this.autonumbersorting = new com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "autonumbersorting"):this.autonumbersorting);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> notcloneable() {
            return ((this.notcloneable == null)?this.notcloneable = new com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "notcloneable"):this.notcloneable);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> multieditable() {
            return ((this.multieditable == null)?this.multieditable = new com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "multieditable"):this.multieditable);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> toolbarorientation() {
            return ((this.toolbarorientation == null)?this.toolbarorientation = new com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "toolbarorientation"):this.toolbarorientation);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> statusbar() {
            return ((this.statusbar == null)?this.statusbar = new com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "statusbar"):this.statusbar);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> opaque() {
            return ((this.opaque == null)?this.opaque = new com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "opaque"):this.opaque);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> openDetailsWithTabRecycling() {
            return ((this.openDetailsWithTabRecycling == null)?this.openDetailsWithTabRecycling = new com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "openDetailsWithTabRecycling"):this.openDetailsWithTabRecycling);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> controllertype() {
            return ((this.controllertype == null)?this.controllertype = new com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "controllertype"):this.controllertype);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> uniqueMastercolumn() {
            return ((this.uniqueMastercolumn == null)?this.uniqueMastercolumn = new com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "uniqueMastercolumn"):this.uniqueMastercolumn);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> foreignkeyfieldToParent() {
            return ((this.foreignkeyfieldToParent == null)?this.foreignkeyfieldToParent = new com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "foreignkeyfieldToParent"):this.foreignkeyfieldToParent);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> parentSubform() {
            return ((this.parentSubform == null)?this.parentSubform = new com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "parentSubform"):this.parentSubform);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> dynamicCellHeightsDefault() {
            return ((this.dynamicCellHeightsDefault == null)?this.dynamicCellHeightsDefault = new com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "dynamicCellHeightsDefault"):this.dynamicCellHeightsDefault);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> ignoreSubLayout() {
            return ((this.ignoreSubLayout == null)?this.ignoreSubLayout = new com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "ignoreSubLayout"):this.ignoreSubLayout);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>> maxEntries() {
            return ((this.maxEntries == null)?this.maxEntries = new com.kscs.util.jaxb.Selector<TRoot, Subform.Selector<TRoot, TParent>>(this._root, this, "maxEntries"):this.maxEntries);
        }

    }

}
