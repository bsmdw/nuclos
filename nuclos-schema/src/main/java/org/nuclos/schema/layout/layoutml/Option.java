
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}translations" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="value" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="label" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="mnemonic" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "translations"
})
@XmlRootElement(name = "option")
public class Option implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected Translations translations;
    @XmlAttribute(name = "name")
    @XmlSchemaType(name = "anySimpleType")
    protected String name;
    @XmlAttribute(name = "value", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String value;
    @XmlAttribute(name = "label", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String label;
    @XmlAttribute(name = "mnemonic")
    @XmlSchemaType(name = "anySimpleType")
    protected String mnemonic;

    /**
     * Gets the value of the translations property.
     * 
     * @return
     *     possible object is
     *     {@link Translations }
     *     
     */
    public Translations getTranslations() {
        return translations;
    }

    /**
     * Sets the value of the translations property.
     * 
     * @param value
     *     allowed object is
     *     {@link Translations }
     *     
     */
    public void setTranslations(Translations value) {
        this.translations = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the mnemonic property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMnemonic() {
        return mnemonic;
    }

    /**
     * Sets the value of the mnemonic property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMnemonic(String value) {
        this.mnemonic = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            Translations theTranslations;
            theTranslations = this.getTranslations();
            strategy.appendField(locator, this, "translations", buffer, theTranslations);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String theValue;
            theValue = this.getValue();
            strategy.appendField(locator, this, "value", buffer, theValue);
        }
        {
            String theLabel;
            theLabel = this.getLabel();
            strategy.appendField(locator, this, "label", buffer, theLabel);
        }
        {
            String theMnemonic;
            theMnemonic = this.getMnemonic();
            strategy.appendField(locator, this, "mnemonic", buffer, theMnemonic);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Option)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Option that = ((Option) object);
        {
            Translations lhsTranslations;
            lhsTranslations = this.getTranslations();
            Translations rhsTranslations;
            rhsTranslations = that.getTranslations();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "translations", lhsTranslations), LocatorUtils.property(thatLocator, "translations", rhsTranslations), lhsTranslations, rhsTranslations)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsValue;
            lhsValue = this.getValue();
            String rhsValue;
            rhsValue = that.getValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "value", lhsValue), LocatorUtils.property(thatLocator, "value", rhsValue), lhsValue, rhsValue)) {
                return false;
            }
        }
        {
            String lhsLabel;
            lhsLabel = this.getLabel();
            String rhsLabel;
            rhsLabel = that.getLabel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "label", lhsLabel), LocatorUtils.property(thatLocator, "label", rhsLabel), lhsLabel, rhsLabel)) {
                return false;
            }
        }
        {
            String lhsMnemonic;
            lhsMnemonic = this.getMnemonic();
            String rhsMnemonic;
            rhsMnemonic = that.getMnemonic();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "mnemonic", lhsMnemonic), LocatorUtils.property(thatLocator, "mnemonic", rhsMnemonic), lhsMnemonic, rhsMnemonic)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            Translations theTranslations;
            theTranslations = this.getTranslations();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "translations", theTranslations), currentHashCode, theTranslations);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String theValue;
            theValue = this.getValue();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "value", theValue), currentHashCode, theValue);
        }
        {
            String theLabel;
            theLabel = this.getLabel();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "label", theLabel), currentHashCode, theLabel);
        }
        {
            String theMnemonic;
            theMnemonic = this.getMnemonic();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "mnemonic", theMnemonic), currentHashCode, theMnemonic);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Option) {
            final Option copy = ((Option) draftCopy);
            if (this.translations!= null) {
                Translations sourceTranslations;
                sourceTranslations = this.getTranslations();
                Translations copyTranslations = ((Translations) strategy.copy(LocatorUtils.property(locator, "translations", sourceTranslations), sourceTranslations));
                copy.setTranslations(copyTranslations);
            } else {
                copy.translations = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.value!= null) {
                String sourceValue;
                sourceValue = this.getValue();
                String copyValue = ((String) strategy.copy(LocatorUtils.property(locator, "value", sourceValue), sourceValue));
                copy.setValue(copyValue);
            } else {
                copy.value = null;
            }
            if (this.label!= null) {
                String sourceLabel;
                sourceLabel = this.getLabel();
                String copyLabel = ((String) strategy.copy(LocatorUtils.property(locator, "label", sourceLabel), sourceLabel));
                copy.setLabel(copyLabel);
            } else {
                copy.label = null;
            }
            if (this.mnemonic!= null) {
                String sourceMnemonic;
                sourceMnemonic = this.getMnemonic();
                String copyMnemonic = ((String) strategy.copy(LocatorUtils.property(locator, "mnemonic", sourceMnemonic), sourceMnemonic));
                copy.setMnemonic(copyMnemonic);
            } else {
                copy.mnemonic = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Option();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Option.Builder<_B> _other) {
        _other.translations = ((this.translations == null)?null:this.translations.newCopyBuilder(_other));
        _other.name = this.name;
        _other.value = this.value;
        _other.label = this.label;
        _other.mnemonic = this.mnemonic;
    }

    public<_B >Option.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Option.Builder<_B>(_parentBuilder, this, true);
    }

    public Option.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Option.Builder<Void> builder() {
        return new Option.Builder<Void>(null, null, false);
    }

    public static<_B >Option.Builder<_B> copyOf(final Option _other) {
        final Option.Builder<_B> _newBuilder = new Option.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Option.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree translationsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("translations"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(translationsPropertyTree!= null):((translationsPropertyTree == null)||(!translationsPropertyTree.isLeaf())))) {
            _other.translations = ((this.translations == null)?null:this.translations.newCopyBuilder(_other, translationsPropertyTree, _propertyTreeUse));
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree valuePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("value"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuePropertyTree!= null):((valuePropertyTree == null)||(!valuePropertyTree.isLeaf())))) {
            _other.value = this.value;
        }
        final PropertyTree labelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("label"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(labelPropertyTree!= null):((labelPropertyTree == null)||(!labelPropertyTree.isLeaf())))) {
            _other.label = this.label;
        }
        final PropertyTree mnemonicPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("mnemonic"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(mnemonicPropertyTree!= null):((mnemonicPropertyTree == null)||(!mnemonicPropertyTree.isLeaf())))) {
            _other.mnemonic = this.mnemonic;
        }
    }

    public<_B >Option.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Option.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Option.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Option.Builder<_B> copyOf(final Option _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Option.Builder<_B> _newBuilder = new Option.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Option.Builder<Void> copyExcept(final Option _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Option.Builder<Void> copyOnly(final Option _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Option _storedValue;
        private Translations.Builder<Option.Builder<_B>> translations;
        private String name;
        private String value;
        private String label;
        private String mnemonic;

        public Builder(final _B _parentBuilder, final Option _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.translations = ((_other.translations == null)?null:_other.translations.newCopyBuilder(this));
                    this.name = _other.name;
                    this.value = _other.value;
                    this.label = _other.label;
                    this.mnemonic = _other.mnemonic;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Option _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree translationsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("translations"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(translationsPropertyTree!= null):((translationsPropertyTree == null)||(!translationsPropertyTree.isLeaf())))) {
                        this.translations = ((_other.translations == null)?null:_other.translations.newCopyBuilder(this, translationsPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree valuePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("value"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuePropertyTree!= null):((valuePropertyTree == null)||(!valuePropertyTree.isLeaf())))) {
                        this.value = _other.value;
                    }
                    final PropertyTree labelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("label"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(labelPropertyTree!= null):((labelPropertyTree == null)||(!labelPropertyTree.isLeaf())))) {
                        this.label = _other.label;
                    }
                    final PropertyTree mnemonicPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("mnemonic"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(mnemonicPropertyTree!= null):((mnemonicPropertyTree == null)||(!mnemonicPropertyTree.isLeaf())))) {
                        this.mnemonic = _other.mnemonic;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Option >_P init(final _P _product) {
            _product.translations = ((this.translations == null)?null:this.translations.build());
            _product.name = this.name;
            _product.value = this.value;
            _product.label = this.label;
            _product.mnemonic = this.mnemonic;
            return _product;
        }

        /**
         * Sets the new value of "translations" (any previous value will be replaced)
         * 
         * @param translations
         *     New value of the "translations" property.
         */
        public Option.Builder<_B> withTranslations(final Translations translations) {
            this.translations = ((translations == null)?null:new Translations.Builder<Option.Builder<_B>>(this, translations, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "translations" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Translations.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "translations" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Translations.Builder#end()} to return to the current builder.
         */
        public Translations.Builder<? extends Option.Builder<_B>> withTranslations() {
            if (this.translations!= null) {
                return this.translations;
            }
            return this.translations = new Translations.Builder<Option.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public Option.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "value" (any previous value will be replaced)
         * 
         * @param value
         *     New value of the "value" property.
         */
        public Option.Builder<_B> withValue(final String value) {
            this.value = value;
            return this;
        }

        /**
         * Sets the new value of "label" (any previous value will be replaced)
         * 
         * @param label
         *     New value of the "label" property.
         */
        public Option.Builder<_B> withLabel(final String label) {
            this.label = label;
            return this;
        }

        /**
         * Sets the new value of "mnemonic" (any previous value will be replaced)
         * 
         * @param mnemonic
         *     New value of the "mnemonic" property.
         */
        public Option.Builder<_B> withMnemonic(final String mnemonic) {
            this.mnemonic = mnemonic;
            return this;
        }

        @Override
        public Option build() {
            if (_storedValue == null) {
                return this.init(new Option());
            } else {
                return ((Option) _storedValue);
            }
        }

        public Option.Builder<_B> copyOf(final Option _other) {
            _other.copyTo(this);
            return this;
        }

        public Option.Builder<_B> copyOf(final Option.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Option.Selector<Option.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Option.Select _root() {
            return new Option.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private Translations.Selector<TRoot, Option.Selector<TRoot, TParent>> translations = null;
        private com.kscs.util.jaxb.Selector<TRoot, Option.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, Option.Selector<TRoot, TParent>> value = null;
        private com.kscs.util.jaxb.Selector<TRoot, Option.Selector<TRoot, TParent>> label = null;
        private com.kscs.util.jaxb.Selector<TRoot, Option.Selector<TRoot, TParent>> mnemonic = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.translations!= null) {
                products.put("translations", this.translations.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.value!= null) {
                products.put("value", this.value.init());
            }
            if (this.label!= null) {
                products.put("label", this.label.init());
            }
            if (this.mnemonic!= null) {
                products.put("mnemonic", this.mnemonic.init());
            }
            return products;
        }

        public Translations.Selector<TRoot, Option.Selector<TRoot, TParent>> translations() {
            return ((this.translations == null)?this.translations = new Translations.Selector<TRoot, Option.Selector<TRoot, TParent>>(this._root, this, "translations"):this.translations);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Option.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, Option.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Option.Selector<TRoot, TParent>> value() {
            return ((this.value == null)?this.value = new com.kscs.util.jaxb.Selector<TRoot, Option.Selector<TRoot, TParent>>(this._root, this, "value"):this.value);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Option.Selector<TRoot, TParent>> label() {
            return ((this.label == null)?this.label = new com.kscs.util.jaxb.Selector<TRoot, Option.Selector<TRoot, TParent>>(this._root, this, "label"):this.label);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Option.Selector<TRoot, TParent>> mnemonic() {
            return ((this.mnemonic == null)?this.mnemonic = new com.kscs.util.jaxb.Selector<TRoot, Option.Selector<TRoot, TParent>>(this._root, this, "mnemonic"):this.mnemonic);
        }

    }

}
