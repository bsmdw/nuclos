
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}layoutconstraints" minOccurs="0"/&gt;
 *         &lt;group ref="{}borders"/&gt;
 *         &lt;group ref="{}sizes"/&gt;
 *         &lt;element ref="{}container"/&gt;
 *         &lt;element ref="{}container"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="orientation" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
 *             &lt;enumeration value="horizontal"/&gt;
 *             &lt;enumeration value="vertical"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="dividersize" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="resizeweight" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="expandable" type="{}boolean" /&gt;
 *       &lt;attribute name="continuous-layout" type="{}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "content"
})
public class Splitpane implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElementRefs({
        @XmlElementRef(name = "layoutconstraints", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "clear-border", type = ClearBorder.class, required = false),
        @XmlElementRef(name = "border", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "minimum-size", type = MinimumSize.class, required = false),
        @XmlElementRef(name = "preferred-size", type = PreferredSize.class, required = false),
        @XmlElementRef(name = "strict-size", type = StrictSize.class, required = false),
        @XmlElementRef(name = "container", type = JAXBElement.class, required = false)
    })
    protected List<Serializable> content;
    @XmlAttribute(name = "name")
    @XmlSchemaType(name = "anySimpleType")
    protected String name;
    @XmlAttribute(name = "orientation", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String orientation;
    @XmlAttribute(name = "dividersize")
    @XmlSchemaType(name = "anySimpleType")
    protected String dividersize;
    @XmlAttribute(name = "resizeweight")
    @XmlSchemaType(name = "anySimpleType")
    protected String resizeweight;
    @XmlAttribute(name = "expandable")
    protected Boolean expandable;
    @XmlAttribute(name = "continuous-layout")
    protected Boolean continuousLayout;

    /**
     * Gets the rest of the content model. 
     * 
     * <p>
     * You are getting this "catch-all" property because of the following reason: 
     * The field name "Container" is used by two different parts of a schema. See: 
     * line 188 of file:/home/maik/Nuclos/workspaces/dev-1/nuclos/nuclos-schema/src/main/resources/org/nuclos/schema/layout/layoutml/layoutml.xsd
     * line 187 of file:/home/maik/Nuclos/workspaces/dev-1/nuclos/nuclos-schema/src/main/resources/org/nuclos/schema/layout/layoutml/layoutml.xsd
     * <p>
     * To get rid of this property, apply a property customization to one 
     * of both of the following declarations to change their names: 
     * Gets the value of the content property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the content property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     * {@link ClearBorder }
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     * {@link MinimumSize }
     * {@link PreferredSize }
     * {@link StrictSize }
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     * 
     * 
     */
    public List<Serializable> getContent() {
        if (content == null) {
            content = new ArrayList<Serializable>();
        }
        return this.content;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the orientation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrientation() {
        return orientation;
    }

    /**
     * Sets the value of the orientation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrientation(String value) {
        this.orientation = value;
    }

    /**
     * Gets the value of the dividersize property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDividersize() {
        return dividersize;
    }

    /**
     * Sets the value of the dividersize property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDividersize(String value) {
        this.dividersize = value;
    }

    /**
     * Gets the value of the resizeweight property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResizeweight() {
        return resizeweight;
    }

    /**
     * Sets the value of the resizeweight property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResizeweight(String value) {
        this.resizeweight = value;
    }

    /**
     * Gets the value of the expandable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getExpandable() {
        return expandable;
    }

    /**
     * Sets the value of the expandable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExpandable(Boolean value) {
        this.expandable = value;
    }

    /**
     * Gets the value of the continuousLayout property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getContinuousLayout() {
        return continuousLayout;
    }

    /**
     * Sets the value of the continuousLayout property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setContinuousLayout(Boolean value) {
        this.continuousLayout = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<Serializable> theContent;
            theContent = (((this.content!= null)&&(!this.content.isEmpty()))?this.getContent():null);
            strategy.appendField(locator, this, "content", buffer, theContent);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String theOrientation;
            theOrientation = this.getOrientation();
            strategy.appendField(locator, this, "orientation", buffer, theOrientation);
        }
        {
            String theDividersize;
            theDividersize = this.getDividersize();
            strategy.appendField(locator, this, "dividersize", buffer, theDividersize);
        }
        {
            String theResizeweight;
            theResizeweight = this.getResizeweight();
            strategy.appendField(locator, this, "resizeweight", buffer, theResizeweight);
        }
        {
            Boolean theExpandable;
            theExpandable = this.getExpandable();
            strategy.appendField(locator, this, "expandable", buffer, theExpandable);
        }
        {
            Boolean theContinuousLayout;
            theContinuousLayout = this.getContinuousLayout();
            strategy.appendField(locator, this, "continuousLayout", buffer, theContinuousLayout);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Splitpane)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Splitpane that = ((Splitpane) object);
        {
            List<Serializable> lhsContent;
            lhsContent = (((this.content!= null)&&(!this.content.isEmpty()))?this.getContent():null);
            List<Serializable> rhsContent;
            rhsContent = (((that.content!= null)&&(!that.content.isEmpty()))?that.getContent():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "content", lhsContent), LocatorUtils.property(thatLocator, "content", rhsContent), lhsContent, rhsContent)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsOrientation;
            lhsOrientation = this.getOrientation();
            String rhsOrientation;
            rhsOrientation = that.getOrientation();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "orientation", lhsOrientation), LocatorUtils.property(thatLocator, "orientation", rhsOrientation), lhsOrientation, rhsOrientation)) {
                return false;
            }
        }
        {
            String lhsDividersize;
            lhsDividersize = this.getDividersize();
            String rhsDividersize;
            rhsDividersize = that.getDividersize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dividersize", lhsDividersize), LocatorUtils.property(thatLocator, "dividersize", rhsDividersize), lhsDividersize, rhsDividersize)) {
                return false;
            }
        }
        {
            String lhsResizeweight;
            lhsResizeweight = this.getResizeweight();
            String rhsResizeweight;
            rhsResizeweight = that.getResizeweight();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "resizeweight", lhsResizeweight), LocatorUtils.property(thatLocator, "resizeweight", rhsResizeweight), lhsResizeweight, rhsResizeweight)) {
                return false;
            }
        }
        {
            Boolean lhsExpandable;
            lhsExpandable = this.getExpandable();
            Boolean rhsExpandable;
            rhsExpandable = that.getExpandable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "expandable", lhsExpandable), LocatorUtils.property(thatLocator, "expandable", rhsExpandable), lhsExpandable, rhsExpandable)) {
                return false;
            }
        }
        {
            Boolean lhsContinuousLayout;
            lhsContinuousLayout = this.getContinuousLayout();
            Boolean rhsContinuousLayout;
            rhsContinuousLayout = that.getContinuousLayout();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "continuousLayout", lhsContinuousLayout), LocatorUtils.property(thatLocator, "continuousLayout", rhsContinuousLayout), lhsContinuousLayout, rhsContinuousLayout)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<Serializable> theContent;
            theContent = (((this.content!= null)&&(!this.content.isEmpty()))?this.getContent():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "content", theContent), currentHashCode, theContent);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String theOrientation;
            theOrientation = this.getOrientation();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "orientation", theOrientation), currentHashCode, theOrientation);
        }
        {
            String theDividersize;
            theDividersize = this.getDividersize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dividersize", theDividersize), currentHashCode, theDividersize);
        }
        {
            String theResizeweight;
            theResizeweight = this.getResizeweight();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "resizeweight", theResizeweight), currentHashCode, theResizeweight);
        }
        {
            Boolean theExpandable;
            theExpandable = this.getExpandable();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "expandable", theExpandable), currentHashCode, theExpandable);
        }
        {
            Boolean theContinuousLayout;
            theContinuousLayout = this.getContinuousLayout();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "continuousLayout", theContinuousLayout), currentHashCode, theContinuousLayout);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Splitpane) {
            final Splitpane copy = ((Splitpane) draftCopy);
            if ((this.content!= null)&&(!this.content.isEmpty())) {
                List<Serializable> sourceContent;
                sourceContent = (((this.content!= null)&&(!this.content.isEmpty()))?this.getContent():null);
                @SuppressWarnings("unchecked")
                List<Serializable> copyContent = ((List<Serializable> ) strategy.copy(LocatorUtils.property(locator, "content", sourceContent), sourceContent));
                copy.content = null;
                if (copyContent!= null) {
                    List<Serializable> uniqueContentl = copy.getContent();
                    uniqueContentl.addAll(copyContent);
                }
            } else {
                copy.content = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.orientation!= null) {
                String sourceOrientation;
                sourceOrientation = this.getOrientation();
                String copyOrientation = ((String) strategy.copy(LocatorUtils.property(locator, "orientation", sourceOrientation), sourceOrientation));
                copy.setOrientation(copyOrientation);
            } else {
                copy.orientation = null;
            }
            if (this.dividersize!= null) {
                String sourceDividersize;
                sourceDividersize = this.getDividersize();
                String copyDividersize = ((String) strategy.copy(LocatorUtils.property(locator, "dividersize", sourceDividersize), sourceDividersize));
                copy.setDividersize(copyDividersize);
            } else {
                copy.dividersize = null;
            }
            if (this.resizeweight!= null) {
                String sourceResizeweight;
                sourceResizeweight = this.getResizeweight();
                String copyResizeweight = ((String) strategy.copy(LocatorUtils.property(locator, "resizeweight", sourceResizeweight), sourceResizeweight));
                copy.setResizeweight(copyResizeweight);
            } else {
                copy.resizeweight = null;
            }
            if (this.expandable!= null) {
                Boolean sourceExpandable;
                sourceExpandable = this.getExpandable();
                Boolean copyExpandable = ((Boolean) strategy.copy(LocatorUtils.property(locator, "expandable", sourceExpandable), sourceExpandable));
                copy.setExpandable(copyExpandable);
            } else {
                copy.expandable = null;
            }
            if (this.continuousLayout!= null) {
                Boolean sourceContinuousLayout;
                sourceContinuousLayout = this.getContinuousLayout();
                Boolean copyContinuousLayout = ((Boolean) strategy.copy(LocatorUtils.property(locator, "continuousLayout", sourceContinuousLayout), sourceContinuousLayout));
                copy.setContinuousLayout(copyContinuousLayout);
            } else {
                copy.continuousLayout = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Splitpane();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Splitpane.Builder<_B> _other) {
        if (this.content == null) {
            _other.content = null;
        } else {
            _other.content = new ArrayList<Buildable>();
            for (Serializable _item: this.content) {
                _other.content.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
            }
        }
        _other.name = this.name;
        _other.orientation = this.orientation;
        _other.dividersize = this.dividersize;
        _other.resizeweight = this.resizeweight;
        _other.expandable = this.expandable;
        _other.continuousLayout = this.continuousLayout;
    }

    public<_B >Splitpane.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Splitpane.Builder<_B>(_parentBuilder, this, true);
    }

    public Splitpane.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Splitpane.Builder<Void> builder() {
        return new Splitpane.Builder<Void>(null, null, false);
    }

    public static<_B >Splitpane.Builder<_B> copyOf(final Splitpane _other) {
        final Splitpane.Builder<_B> _newBuilder = new Splitpane.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Splitpane.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree contentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("content"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(contentPropertyTree!= null):((contentPropertyTree == null)||(!contentPropertyTree.isLeaf())))) {
            if (this.content == null) {
                _other.content = null;
            } else {
                _other.content = new ArrayList<Buildable>();
                for (Serializable _item: this.content) {
                    _other.content.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                }
            }
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree orientationPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("orientation"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(orientationPropertyTree!= null):((orientationPropertyTree == null)||(!orientationPropertyTree.isLeaf())))) {
            _other.orientation = this.orientation;
        }
        final PropertyTree dividersizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dividersize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dividersizePropertyTree!= null):((dividersizePropertyTree == null)||(!dividersizePropertyTree.isLeaf())))) {
            _other.dividersize = this.dividersize;
        }
        final PropertyTree resizeweightPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resizeweight"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resizeweightPropertyTree!= null):((resizeweightPropertyTree == null)||(!resizeweightPropertyTree.isLeaf())))) {
            _other.resizeweight = this.resizeweight;
        }
        final PropertyTree expandablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("expandable"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(expandablePropertyTree!= null):((expandablePropertyTree == null)||(!expandablePropertyTree.isLeaf())))) {
            _other.expandable = this.expandable;
        }
        final PropertyTree continuousLayoutPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("continuousLayout"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(continuousLayoutPropertyTree!= null):((continuousLayoutPropertyTree == null)||(!continuousLayoutPropertyTree.isLeaf())))) {
            _other.continuousLayout = this.continuousLayout;
        }
    }

    public<_B >Splitpane.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Splitpane.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Splitpane.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Splitpane.Builder<_B> copyOf(final Splitpane _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Splitpane.Builder<_B> _newBuilder = new Splitpane.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Splitpane.Builder<Void> copyExcept(final Splitpane _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Splitpane.Builder<Void> copyOnly(final Splitpane _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Splitpane _storedValue;
        private List<Buildable> content;
        private String name;
        private String orientation;
        private String dividersize;
        private String resizeweight;
        private Boolean expandable;
        private Boolean continuousLayout;

        public Builder(final _B _parentBuilder, final Splitpane _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.content == null) {
                        this.content = null;
                    } else {
                        this.content = new ArrayList<Buildable>();
                        for (Serializable _item: _other.content) {
                            this.content.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                        }
                    }
                    this.name = _other.name;
                    this.orientation = _other.orientation;
                    this.dividersize = _other.dividersize;
                    this.resizeweight = _other.resizeweight;
                    this.expandable = _other.expandable;
                    this.continuousLayout = _other.continuousLayout;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Splitpane _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree contentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("content"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(contentPropertyTree!= null):((contentPropertyTree == null)||(!contentPropertyTree.isLeaf())))) {
                        if (_other.content == null) {
                            this.content = null;
                        } else {
                            this.content = new ArrayList<Buildable>();
                            for (Serializable _item: _other.content) {
                                this.content.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                            }
                        }
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree orientationPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("orientation"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(orientationPropertyTree!= null):((orientationPropertyTree == null)||(!orientationPropertyTree.isLeaf())))) {
                        this.orientation = _other.orientation;
                    }
                    final PropertyTree dividersizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dividersize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dividersizePropertyTree!= null):((dividersizePropertyTree == null)||(!dividersizePropertyTree.isLeaf())))) {
                        this.dividersize = _other.dividersize;
                    }
                    final PropertyTree resizeweightPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resizeweight"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resizeweightPropertyTree!= null):((resizeweightPropertyTree == null)||(!resizeweightPropertyTree.isLeaf())))) {
                        this.resizeweight = _other.resizeweight;
                    }
                    final PropertyTree expandablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("expandable"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(expandablePropertyTree!= null):((expandablePropertyTree == null)||(!expandablePropertyTree.isLeaf())))) {
                        this.expandable = _other.expandable;
                    }
                    final PropertyTree continuousLayoutPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("continuousLayout"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(continuousLayoutPropertyTree!= null):((continuousLayoutPropertyTree == null)||(!continuousLayoutPropertyTree.isLeaf())))) {
                        this.continuousLayout = _other.continuousLayout;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Splitpane >_P init(final _P _product) {
            if (this.content!= null) {
                final List<Serializable> content = new ArrayList<Serializable>(this.content.size());
                for (Buildable _item: this.content) {
                    content.add(((Serializable) _item.build()));
                }
                _product.content = content;
            }
            _product.name = this.name;
            _product.orientation = this.orientation;
            _product.dividersize = this.dividersize;
            _product.resizeweight = this.resizeweight;
            _product.expandable = this.expandable;
            _product.continuousLayout = this.continuousLayout;
            return _product;
        }

        /**
         * Adds the given items to the value of "content"
         * 
         * @param content
         *     Items to add to the value of the "content" property
         */
        public Splitpane.Builder<_B> addContent(final Iterable<? extends Serializable> content) {
            if (content!= null) {
                if (this.content == null) {
                    this.content = new ArrayList<Buildable>();
                }
                for (Serializable _item: content) {
                    this.content.add(new Buildable.PrimitiveBuildable(_item));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "content" (any previous value will be replaced)
         * 
         * @param content
         *     New value of the "content" property.
         */
        public Splitpane.Builder<_B> withContent(final Iterable<? extends Serializable> content) {
            if (this.content!= null) {
                this.content.clear();
            }
            return addContent(content);
        }

        /**
         * Adds the given items to the value of "content"
         * 
         * @param content
         *     Items to add to the value of the "content" property
         */
        public Splitpane.Builder<_B> addContent(Serializable... content) {
            addContent(Arrays.asList(content));
            return this;
        }

        /**
         * Sets the new value of "content" (any previous value will be replaced)
         * 
         * @param content
         *     New value of the "content" property.
         */
        public Splitpane.Builder<_B> withContent(Serializable... content) {
            withContent(Arrays.asList(content));
            return this;
        }

        /**
         * Adds the given items to the value of "layoutconstraints_"
         * 
         * @param layoutconstraints_
         *     Items to add to the value of the "layoutconstraints_" property
         */
        public Splitpane.Builder<_B> addLayoutconstraints(final Iterable<? extends JAXBElement<Object>> layoutconstraints_) {
            return addContent(layoutconstraints_);
        }

        /**
         * Adds the given items to the value of "layoutconstraints_"
         * 
         * @param layoutconstraints_
         *     Items to add to the value of the "layoutconstraints_" property
         */
        public Splitpane.Builder<_B> addLayoutconstraints(JAXBElement<Object> ... layoutconstraints_) {
            return addLayoutconstraints(Arrays.asList(layoutconstraints_));
        }

        /**
         * Adds the given items to the value of "clearBorder_"
         * 
         * @param clearBorder_
         *     Items to add to the value of the "clearBorder_" property
         */
        public Splitpane.Builder<_B> addClearBorder(final Iterable<? extends ClearBorder> clearBorder_) {
            if (clearBorder_!= null) {
                if (this.content == null) {
                    this.content = new ArrayList<Buildable>();
                }
                for (ClearBorder _item: clearBorder_) {
                    this.content.add(new ClearBorder.Builder<Splitpane.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Adds the given items to the value of "clearBorder_"
         * 
         * @param clearBorder_
         *     Items to add to the value of the "clearBorder_" property
         */
        public Splitpane.Builder<_B> addClearBorder(ClearBorder... clearBorder_) {
            return addClearBorder(Arrays.asList(clearBorder_));
        }

        /**
         * Returns a new builder to build an additional value of the "clearBorder" property.
         * Use {@link org.nuclos.schema.layout.layoutml.ClearBorder.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "clearBorder" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.ClearBorder.Builder#end()} to return to the current builder.
         */
        public ClearBorder.Builder<? extends Splitpane.Builder<_B>> addClearBorder() {
            if (this.content == null) {
                this.content = new ArrayList<Buildable>();
            }
            final ClearBorder.Builder<Splitpane.Builder<_B>> clearBorder_Builder = new ClearBorder.Builder<Splitpane.Builder<_B>>(this, null, false);
            this.content.add(clearBorder_Builder);
            return clearBorder_Builder;
        }

        /**
         * Adds the given items to the value of "border_"
         * 
         * @param border_
         *     Items to add to the value of the "border_" property
         */
        public Splitpane.Builder<_B> addBorder(final Iterable<? extends JAXBElement<Object>> border_) {
            return addContent(border_);
        }

        /**
         * Adds the given items to the value of "border_"
         * 
         * @param border_
         *     Items to add to the value of the "border_" property
         */
        public Splitpane.Builder<_B> addBorder(JAXBElement<Object> ... border_) {
            return addBorder(Arrays.asList(border_));
        }

        /**
         * Adds the given items to the value of "minimumSize_"
         * 
         * @param minimumSize_
         *     Items to add to the value of the "minimumSize_" property
         */
        public Splitpane.Builder<_B> addMinimumSize(final Iterable<? extends MinimumSize> minimumSize_) {
            if (minimumSize_!= null) {
                if (this.content == null) {
                    this.content = new ArrayList<Buildable>();
                }
                for (MinimumSize _item: minimumSize_) {
                    this.content.add(new MinimumSize.Builder<Splitpane.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Adds the given items to the value of "minimumSize_"
         * 
         * @param minimumSize_
         *     Items to add to the value of the "minimumSize_" property
         */
        public Splitpane.Builder<_B> addMinimumSize(MinimumSize... minimumSize_) {
            return addMinimumSize(Arrays.asList(minimumSize_));
        }

        /**
         * Returns a new builder to build an additional value of the "minimumSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.MinimumSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "minimumSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.MinimumSize.Builder#end()} to return to the current builder.
         */
        public MinimumSize.Builder<? extends Splitpane.Builder<_B>> addMinimumSize() {
            if (this.content == null) {
                this.content = new ArrayList<Buildable>();
            }
            final MinimumSize.Builder<Splitpane.Builder<_B>> minimumSize_Builder = new MinimumSize.Builder<Splitpane.Builder<_B>>(this, null, false);
            this.content.add(minimumSize_Builder);
            return minimumSize_Builder;
        }

        /**
         * Adds the given items to the value of "preferredSize_"
         * 
         * @param preferredSize_
         *     Items to add to the value of the "preferredSize_" property
         */
        public Splitpane.Builder<_B> addPreferredSize(final Iterable<? extends PreferredSize> preferredSize_) {
            if (preferredSize_!= null) {
                if (this.content == null) {
                    this.content = new ArrayList<Buildable>();
                }
                for (PreferredSize _item: preferredSize_) {
                    this.content.add(new PreferredSize.Builder<Splitpane.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Adds the given items to the value of "preferredSize_"
         * 
         * @param preferredSize_
         *     Items to add to the value of the "preferredSize_" property
         */
        public Splitpane.Builder<_B> addPreferredSize(PreferredSize... preferredSize_) {
            return addPreferredSize(Arrays.asList(preferredSize_));
        }

        /**
         * Returns a new builder to build an additional value of the "preferredSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.PreferredSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "preferredSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.PreferredSize.Builder#end()} to return to the current builder.
         */
        public PreferredSize.Builder<? extends Splitpane.Builder<_B>> addPreferredSize() {
            if (this.content == null) {
                this.content = new ArrayList<Buildable>();
            }
            final PreferredSize.Builder<Splitpane.Builder<_B>> preferredSize_Builder = new PreferredSize.Builder<Splitpane.Builder<_B>>(this, null, false);
            this.content.add(preferredSize_Builder);
            return preferredSize_Builder;
        }

        /**
         * Adds the given items to the value of "strictSize_"
         * 
         * @param strictSize_
         *     Items to add to the value of the "strictSize_" property
         */
        public Splitpane.Builder<_B> addStrictSize(final Iterable<? extends StrictSize> strictSize_) {
            if (strictSize_!= null) {
                if (this.content == null) {
                    this.content = new ArrayList<Buildable>();
                }
                for (StrictSize _item: strictSize_) {
                    this.content.add(new StrictSize.Builder<Splitpane.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Adds the given items to the value of "strictSize_"
         * 
         * @param strictSize_
         *     Items to add to the value of the "strictSize_" property
         */
        public Splitpane.Builder<_B> addStrictSize(StrictSize... strictSize_) {
            return addStrictSize(Arrays.asList(strictSize_));
        }

        /**
         * Returns a new builder to build an additional value of the "strictSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.StrictSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "strictSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.StrictSize.Builder#end()} to return to the current builder.
         */
        public StrictSize.Builder<? extends Splitpane.Builder<_B>> addStrictSize() {
            if (this.content == null) {
                this.content = new ArrayList<Buildable>();
            }
            final StrictSize.Builder<Splitpane.Builder<_B>> strictSize_Builder = new StrictSize.Builder<Splitpane.Builder<_B>>(this, null, false);
            this.content.add(strictSize_Builder);
            return strictSize_Builder;
        }

        /**
         * Adds the given items to the value of "container_"
         * 
         * @param container_
         *     Items to add to the value of the "container_" property
         */
        public Splitpane.Builder<_B> addContainer(final Iterable<? extends JAXBElement<Object>> container_) {
            return addContent(container_);
        }

        /**
         * Adds the given items to the value of "container_"
         * 
         * @param container_
         *     Items to add to the value of the "container_" property
         */
        public Splitpane.Builder<_B> addContainer(JAXBElement<Object> ... container_) {
            return addContainer(Arrays.asList(container_));
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public Splitpane.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "orientation" (any previous value will be replaced)
         * 
         * @param orientation
         *     New value of the "orientation" property.
         */
        public Splitpane.Builder<_B> withOrientation(final String orientation) {
            this.orientation = orientation;
            return this;
        }

        /**
         * Sets the new value of "dividersize" (any previous value will be replaced)
         * 
         * @param dividersize
         *     New value of the "dividersize" property.
         */
        public Splitpane.Builder<_B> withDividersize(final String dividersize) {
            this.dividersize = dividersize;
            return this;
        }

        /**
         * Sets the new value of "resizeweight" (any previous value will be replaced)
         * 
         * @param resizeweight
         *     New value of the "resizeweight" property.
         */
        public Splitpane.Builder<_B> withResizeweight(final String resizeweight) {
            this.resizeweight = resizeweight;
            return this;
        }

        /**
         * Sets the new value of "expandable" (any previous value will be replaced)
         * 
         * @param expandable
         *     New value of the "expandable" property.
         */
        public Splitpane.Builder<_B> withExpandable(final Boolean expandable) {
            this.expandable = expandable;
            return this;
        }

        /**
         * Sets the new value of "continuousLayout" (any previous value will be replaced)
         * 
         * @param continuousLayout
         *     New value of the "continuousLayout" property.
         */
        public Splitpane.Builder<_B> withContinuousLayout(final Boolean continuousLayout) {
            this.continuousLayout = continuousLayout;
            return this;
        }

        @Override
        public Splitpane build() {
            if (_storedValue == null) {
                return this.init(new Splitpane());
            } else {
                return ((Splitpane) _storedValue);
            }
        }

        public Splitpane.Builder<_B> copyOf(final Splitpane _other) {
            _other.copyTo(this);
            return this;
        }

        public Splitpane.Builder<_B> copyOf(final Splitpane.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Splitpane.Selector<Splitpane.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Splitpane.Select _root() {
            return new Splitpane.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> content = null;
        private com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> orientation = null;
        private com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> dividersize = null;
        private com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> resizeweight = null;
        private com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> expandable = null;
        private com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> continuousLayout = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.content!= null) {
                products.put("content", this.content.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.orientation!= null) {
                products.put("orientation", this.orientation.init());
            }
            if (this.dividersize!= null) {
                products.put("dividersize", this.dividersize.init());
            }
            if (this.resizeweight!= null) {
                products.put("resizeweight", this.resizeweight.init());
            }
            if (this.expandable!= null) {
                products.put("expandable", this.expandable.init());
            }
            if (this.continuousLayout!= null) {
                products.put("continuousLayout", this.continuousLayout.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> content() {
            return ((this.content == null)?this.content = new com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>>(this._root, this, "content"):this.content);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> orientation() {
            return ((this.orientation == null)?this.orientation = new com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>>(this._root, this, "orientation"):this.orientation);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> dividersize() {
            return ((this.dividersize == null)?this.dividersize = new com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>>(this._root, this, "dividersize"):this.dividersize);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> resizeweight() {
            return ((this.resizeweight == null)?this.resizeweight = new com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>>(this._root, this, "resizeweight"):this.resizeweight);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> expandable() {
            return ((this.expandable == null)?this.expandable = new com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>>(this._root, this, "expandable"):this.expandable);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> continuousLayout() {
            return ((this.continuousLayout == null)?this.continuousLayout = new com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>>(this._root, this, "continuousLayout"):this.continuousLayout);
        }

    }

}
