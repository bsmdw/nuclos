
package org.nuclos.schema.layout.layoutml;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for controltype.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="controltype"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
 *     &lt;enumeration value="textfield"/&gt;
 *     &lt;enumeration value="id-textfield"/&gt;
 *     &lt;enumeration value="textarea"/&gt;
 *     &lt;enumeration value="combobox"/&gt;
 *     &lt;enumeration value="checkbox"/&gt;
 *     &lt;enumeration value="optiongroup"/&gt;
 *     &lt;enumeration value="listofvalues"/&gt;
 *     &lt;enumeration value="datechooser"/&gt;
 *     &lt;enumeration value="filechooser"/&gt;
 *     &lt;enumeration value="image"/&gt;
 *     &lt;enumeration value="password"/&gt;
 *     &lt;enumeration value="hyperlink"/&gt;
 *     &lt;enumeration value="email"/&gt;
 *     &lt;enumeration value="phonenumber"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "controltype")
@XmlEnum
public enum Controltype {

    @XmlEnumValue("textfield")
    TEXTFIELD("textfield"),
    @XmlEnumValue("id-textfield")
    ID_TEXTFIELD("id-textfield"),
    @XmlEnumValue("textarea")
    TEXTAREA("textarea"),
    @XmlEnumValue("combobox")
    COMBOBOX("combobox"),
    @XmlEnumValue("checkbox")
    CHECKBOX("checkbox"),
    @XmlEnumValue("optiongroup")
    OPTIONGROUP("optiongroup"),
    @XmlEnumValue("listofvalues")
    LISTOFVALUES("listofvalues"),
    @XmlEnumValue("datechooser")
    DATECHOOSER("datechooser"),
    @XmlEnumValue("filechooser")
    FILECHOOSER("filechooser"),
    @XmlEnumValue("image")
    IMAGE("image"),
    @XmlEnumValue("password")
    PASSWORD("password"),
    @XmlEnumValue("hyperlink")
    HYPERLINK("hyperlink"),
    @XmlEnumValue("email")
    EMAIL("email"),
    @XmlEnumValue("phonenumber")
    PHONENUMBER("phonenumber");
    private final String value;

    Controltype(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Controltype fromValue(String v) {
        for (Controltype c: Controltype.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
