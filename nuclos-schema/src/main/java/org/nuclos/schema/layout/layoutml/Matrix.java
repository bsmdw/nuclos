
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}layoutconstraints" minOccurs="0"/&gt;
 *         &lt;group ref="{}borders"/&gt;
 *         &lt;group ref="{}sizes"/&gt;
 *         &lt;element ref="{}font" minOccurs="0"/&gt;
 *         &lt;element ref="{}background" minOccurs="0"/&gt;
 *         &lt;element ref="{}description" minOccurs="0"/&gt;
 *         &lt;element ref="{}matrix-column" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{}new-enabled" minOccurs="0"/&gt;
 *         &lt;element ref="{}edit-enabled" minOccurs="0"/&gt;
 *         &lt;element ref="{}delete-enabled" minOccurs="0"/&gt;
 *         &lt;element ref="{}clone-enabled" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="matrix_preferences_field" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="entity_x" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="entity_y" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="entity_matrix" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="entity_field_matrix_parent" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="entity_field_matrix_x_ref_field" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="entity_matrix_value_field" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="entity_matrix_number_state" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="entity_field_categorie" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="entity_field_x" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="entity_y_parent_field" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="entity_field_y" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="entity_x_sorting_fields" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="entity_y_sorting_fields" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="entity_x_header" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="entity_y_header" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="entity_matrix_reference_field" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="entity_matrix_value_type" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="entity_x_vlp_id" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="entity_x_vlp_idfieldname" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="entity_x_vlp_fieldname" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="entity_x_vlp_reference_param_name" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="cell_input_type" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="enabled" type="{}boolean" /&gt;
 *       &lt;attribute name="opaque" type="{}boolean" /&gt;
 *       &lt;attribute name="controllertype" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="dynamic-cell-heights-default" type="{}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "layoutconstraints",
    "clearBorder",
    "border",
    "minimumSize",
    "preferredSize",
    "strictSize",
    "font",
    "background",
    "description",
    "matrixColumn",
    "newEnabled",
    "editEnabled",
    "deleteEnabled",
    "cloneEnabled"
})
public class Matrix implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "layoutconstraints", type = JAXBElement.class, required = false)
    protected JAXBElement<?> layoutconstraints;
    @XmlElement(name = "clear-border")
    protected ClearBorder clearBorder;
    @XmlElementRef(name = "border", type = JAXBElement.class, required = false)
    protected List<JAXBElement<?>> border;
    @XmlElement(name = "minimum-size")
    protected MinimumSize minimumSize;
    @XmlElement(name = "preferred-size")
    protected PreferredSize preferredSize;
    @XmlElement(name = "strict-size")
    protected StrictSize strictSize;
    protected Font font;
    protected Background background;
    protected String description;
    @XmlElement(name = "matrix-column")
    protected List<MatrixColumn> matrixColumn;
    @XmlElement(name = "new-enabled")
    protected NewEnabled newEnabled;
    @XmlElement(name = "edit-enabled")
    protected EditEnabled editEnabled;
    @XmlElement(name = "delete-enabled")
    protected DeleteEnabled deleteEnabled;
    @XmlElement(name = "clone-enabled")
    protected CloneEnabled cloneEnabled;
    @XmlAttribute(name = "name")
    @XmlSchemaType(name = "anySimpleType")
    protected String name;
    @XmlAttribute(name = "matrix_preferences_field")
    @XmlSchemaType(name = "anySimpleType")
    protected String matrixPreferencesField;
    @XmlAttribute(name = "entity_x")
    @XmlSchemaType(name = "anySimpleType")
    protected String entityX;
    @XmlAttribute(name = "entity_y")
    @XmlSchemaType(name = "anySimpleType")
    protected String entityY;
    @XmlAttribute(name = "entity_matrix")
    @XmlSchemaType(name = "anySimpleType")
    protected String entityMatrix;
    @XmlAttribute(name = "entity_field_matrix_parent")
    @XmlSchemaType(name = "anySimpleType")
    protected String entityFieldMatrixParent;
    @XmlAttribute(name = "entity_field_matrix_x_ref_field")
    @XmlSchemaType(name = "anySimpleType")
    protected String entityFieldMatrixXRefField;
    @XmlAttribute(name = "entity_matrix_value_field")
    @XmlSchemaType(name = "anySimpleType")
    protected String entityMatrixValueField;
    @XmlAttribute(name = "entity_matrix_number_state")
    @XmlSchemaType(name = "anySimpleType")
    protected String entityMatrixNumberState;
    @XmlAttribute(name = "entity_field_categorie")
    @XmlSchemaType(name = "anySimpleType")
    protected String entityFieldCategorie;
    @XmlAttribute(name = "entity_field_x")
    @XmlSchemaType(name = "anySimpleType")
    protected String entityFieldX;
    @XmlAttribute(name = "entity_y_parent_field")
    @XmlSchemaType(name = "anySimpleType")
    protected String entityYParentField;
    @XmlAttribute(name = "entity_field_y")
    @XmlSchemaType(name = "anySimpleType")
    protected String entityFieldY;
    @XmlAttribute(name = "entity_x_sorting_fields")
    @XmlSchemaType(name = "anySimpleType")
    protected String entityXSortingFields;
    @XmlAttribute(name = "entity_y_sorting_fields")
    @XmlSchemaType(name = "anySimpleType")
    protected String entityYSortingFields;
    @XmlAttribute(name = "entity_x_header")
    @XmlSchemaType(name = "anySimpleType")
    protected String entityXHeader;
    @XmlAttribute(name = "entity_y_header")
    @XmlSchemaType(name = "anySimpleType")
    protected String entityYHeader;
    @XmlAttribute(name = "entity_matrix_reference_field")
    @XmlSchemaType(name = "anySimpleType")
    protected String entityMatrixReferenceField;
    @XmlAttribute(name = "entity_matrix_value_type")
    @XmlSchemaType(name = "anySimpleType")
    protected String entityMatrixValueType;
    @XmlAttribute(name = "entity_x_vlp_id")
    @XmlSchemaType(name = "anySimpleType")
    protected String entityXVlpId;
    @XmlAttribute(name = "entity_x_vlp_idfieldname")
    @XmlSchemaType(name = "anySimpleType")
    protected String entityXVlpIdfieldname;
    @XmlAttribute(name = "entity_x_vlp_fieldname")
    @XmlSchemaType(name = "anySimpleType")
    protected String entityXVlpFieldname;
    @XmlAttribute(name = "entity_x_vlp_reference_param_name")
    @XmlSchemaType(name = "anySimpleType")
    protected String entityXVlpReferenceParamName;
    @XmlAttribute(name = "cell_input_type")
    @XmlSchemaType(name = "anySimpleType")
    protected String cellInputType;
    @XmlAttribute(name = "enabled")
    protected Boolean enabled;
    @XmlAttribute(name = "opaque")
    protected Boolean opaque;
    @XmlAttribute(name = "controllertype")
    @XmlSchemaType(name = "anySimpleType")
    protected String controllertype;
    @XmlAttribute(name = "dynamic-cell-heights-default")
    protected Boolean dynamicCellHeightsDefault;

    /**
     * Gets the value of the layoutconstraints property.
     * 
     * @return
     *     possible object is
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     *     
     */
    public JAXBElement<?> getLayoutconstraints() {
        return layoutconstraints;
    }

    /**
     * Sets the value of the layoutconstraints property.
     * 
     * @param value
     *     allowed object is
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     *     
     */
    public void setLayoutconstraints(JAXBElement<?> value) {
        this.layoutconstraints = value;
    }

    /**
     * Gets the value of the clearBorder property.
     * 
     * @return
     *     possible object is
     *     {@link ClearBorder }
     *     
     */
    public ClearBorder getClearBorder() {
        return clearBorder;
    }

    /**
     * Sets the value of the clearBorder property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClearBorder }
     *     
     */
    public void setClearBorder(ClearBorder value) {
        this.clearBorder = value;
    }

    /**
     * Gets the value of the border property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the border property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBorder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     * 
     * 
     */
    public List<JAXBElement<?>> getBorder() {
        if (border == null) {
            border = new ArrayList<JAXBElement<?>>();
        }
        return this.border;
    }

    /**
     * Gets the value of the minimumSize property.
     * 
     * @return
     *     possible object is
     *     {@link MinimumSize }
     *     
     */
    public MinimumSize getMinimumSize() {
        return minimumSize;
    }

    /**
     * Sets the value of the minimumSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link MinimumSize }
     *     
     */
    public void setMinimumSize(MinimumSize value) {
        this.minimumSize = value;
    }

    /**
     * Gets the value of the preferredSize property.
     * 
     * @return
     *     possible object is
     *     {@link PreferredSize }
     *     
     */
    public PreferredSize getPreferredSize() {
        return preferredSize;
    }

    /**
     * Sets the value of the preferredSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link PreferredSize }
     *     
     */
    public void setPreferredSize(PreferredSize value) {
        this.preferredSize = value;
    }

    /**
     * Gets the value of the strictSize property.
     * 
     * @return
     *     possible object is
     *     {@link StrictSize }
     *     
     */
    public StrictSize getStrictSize() {
        return strictSize;
    }

    /**
     * Sets the value of the strictSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link StrictSize }
     *     
     */
    public void setStrictSize(StrictSize value) {
        this.strictSize = value;
    }

    /**
     * Gets the value of the font property.
     * 
     * @return
     *     possible object is
     *     {@link Font }
     *     
     */
    public Font getFont() {
        return font;
    }

    /**
     * Sets the value of the font property.
     * 
     * @param value
     *     allowed object is
     *     {@link Font }
     *     
     */
    public void setFont(Font value) {
        this.font = value;
    }

    /**
     * Gets the value of the background property.
     * 
     * @return
     *     possible object is
     *     {@link Background }
     *     
     */
    public Background getBackground() {
        return background;
    }

    /**
     * Sets the value of the background property.
     * 
     * @param value
     *     allowed object is
     *     {@link Background }
     *     
     */
    public void setBackground(Background value) {
        this.background = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the matrixColumn property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the matrixColumn property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMatrixColumn().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MatrixColumn }
     * 
     * 
     */
    public List<MatrixColumn> getMatrixColumn() {
        if (matrixColumn == null) {
            matrixColumn = new ArrayList<MatrixColumn>();
        }
        return this.matrixColumn;
    }

    /**
     * Gets the value of the newEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link NewEnabled }
     *     
     */
    public NewEnabled getNewEnabled() {
        return newEnabled;
    }

    /**
     * Sets the value of the newEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link NewEnabled }
     *     
     */
    public void setNewEnabled(NewEnabled value) {
        this.newEnabled = value;
    }

    /**
     * Gets the value of the editEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link EditEnabled }
     *     
     */
    public EditEnabled getEditEnabled() {
        return editEnabled;
    }

    /**
     * Sets the value of the editEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link EditEnabled }
     *     
     */
    public void setEditEnabled(EditEnabled value) {
        this.editEnabled = value;
    }

    /**
     * Gets the value of the deleteEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link DeleteEnabled }
     *     
     */
    public DeleteEnabled getDeleteEnabled() {
        return deleteEnabled;
    }

    /**
     * Sets the value of the deleteEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeleteEnabled }
     *     
     */
    public void setDeleteEnabled(DeleteEnabled value) {
        this.deleteEnabled = value;
    }

    /**
     * Gets the value of the cloneEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link CloneEnabled }
     *     
     */
    public CloneEnabled getCloneEnabled() {
        return cloneEnabled;
    }

    /**
     * Sets the value of the cloneEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link CloneEnabled }
     *     
     */
    public void setCloneEnabled(CloneEnabled value) {
        this.cloneEnabled = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the matrixPreferencesField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatrixPreferencesField() {
        return matrixPreferencesField;
    }

    /**
     * Sets the value of the matrixPreferencesField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatrixPreferencesField(String value) {
        this.matrixPreferencesField = value;
    }

    /**
     * Gets the value of the entityX property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityX() {
        return entityX;
    }

    /**
     * Sets the value of the entityX property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityX(String value) {
        this.entityX = value;
    }

    /**
     * Gets the value of the entityY property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityY() {
        return entityY;
    }

    /**
     * Sets the value of the entityY property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityY(String value) {
        this.entityY = value;
    }

    /**
     * Gets the value of the entityMatrix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityMatrix() {
        return entityMatrix;
    }

    /**
     * Sets the value of the entityMatrix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityMatrix(String value) {
        this.entityMatrix = value;
    }

    /**
     * Gets the value of the entityFieldMatrixParent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityFieldMatrixParent() {
        return entityFieldMatrixParent;
    }

    /**
     * Sets the value of the entityFieldMatrixParent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityFieldMatrixParent(String value) {
        this.entityFieldMatrixParent = value;
    }

    /**
     * Gets the value of the entityFieldMatrixXRefField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityFieldMatrixXRefField() {
        return entityFieldMatrixXRefField;
    }

    /**
     * Sets the value of the entityFieldMatrixXRefField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityFieldMatrixXRefField(String value) {
        this.entityFieldMatrixXRefField = value;
    }

    /**
     * Gets the value of the entityMatrixValueField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityMatrixValueField() {
        return entityMatrixValueField;
    }

    /**
     * Sets the value of the entityMatrixValueField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityMatrixValueField(String value) {
        this.entityMatrixValueField = value;
    }

    /**
     * Gets the value of the entityMatrixNumberState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityMatrixNumberState() {
        return entityMatrixNumberState;
    }

    /**
     * Sets the value of the entityMatrixNumberState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityMatrixNumberState(String value) {
        this.entityMatrixNumberState = value;
    }

    /**
     * Gets the value of the entityFieldCategorie property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityFieldCategorie() {
        return entityFieldCategorie;
    }

    /**
     * Sets the value of the entityFieldCategorie property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityFieldCategorie(String value) {
        this.entityFieldCategorie = value;
    }

    /**
     * Gets the value of the entityFieldX property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityFieldX() {
        return entityFieldX;
    }

    /**
     * Sets the value of the entityFieldX property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityFieldX(String value) {
        this.entityFieldX = value;
    }

    /**
     * Gets the value of the entityYParentField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityYParentField() {
        return entityYParentField;
    }

    /**
     * Sets the value of the entityYParentField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityYParentField(String value) {
        this.entityYParentField = value;
    }

    /**
     * Gets the value of the entityFieldY property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityFieldY() {
        return entityFieldY;
    }

    /**
     * Sets the value of the entityFieldY property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityFieldY(String value) {
        this.entityFieldY = value;
    }

    /**
     * Gets the value of the entityXSortingFields property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityXSortingFields() {
        return entityXSortingFields;
    }

    /**
     * Sets the value of the entityXSortingFields property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityXSortingFields(String value) {
        this.entityXSortingFields = value;
    }

    /**
     * Gets the value of the entityYSortingFields property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityYSortingFields() {
        return entityYSortingFields;
    }

    /**
     * Sets the value of the entityYSortingFields property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityYSortingFields(String value) {
        this.entityYSortingFields = value;
    }

    /**
     * Gets the value of the entityXHeader property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityXHeader() {
        return entityXHeader;
    }

    /**
     * Sets the value of the entityXHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityXHeader(String value) {
        this.entityXHeader = value;
    }

    /**
     * Gets the value of the entityYHeader property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityYHeader() {
        return entityYHeader;
    }

    /**
     * Sets the value of the entityYHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityYHeader(String value) {
        this.entityYHeader = value;
    }

    /**
     * Gets the value of the entityMatrixReferenceField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityMatrixReferenceField() {
        return entityMatrixReferenceField;
    }

    /**
     * Sets the value of the entityMatrixReferenceField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityMatrixReferenceField(String value) {
        this.entityMatrixReferenceField = value;
    }

    /**
     * Gets the value of the entityMatrixValueType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityMatrixValueType() {
        return entityMatrixValueType;
    }

    /**
     * Sets the value of the entityMatrixValueType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityMatrixValueType(String value) {
        this.entityMatrixValueType = value;
    }

    /**
     * Gets the value of the entityXVlpId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityXVlpId() {
        return entityXVlpId;
    }

    /**
     * Sets the value of the entityXVlpId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityXVlpId(String value) {
        this.entityXVlpId = value;
    }

    /**
     * Gets the value of the entityXVlpIdfieldname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityXVlpIdfieldname() {
        return entityXVlpIdfieldname;
    }

    /**
     * Sets the value of the entityXVlpIdfieldname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityXVlpIdfieldname(String value) {
        this.entityXVlpIdfieldname = value;
    }

    /**
     * Gets the value of the entityXVlpFieldname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityXVlpFieldname() {
        return entityXVlpFieldname;
    }

    /**
     * Sets the value of the entityXVlpFieldname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityXVlpFieldname(String value) {
        this.entityXVlpFieldname = value;
    }

    /**
     * Gets the value of the entityXVlpReferenceParamName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityXVlpReferenceParamName() {
        return entityXVlpReferenceParamName;
    }

    /**
     * Sets the value of the entityXVlpReferenceParamName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityXVlpReferenceParamName(String value) {
        this.entityXVlpReferenceParamName = value;
    }

    /**
     * Gets the value of the cellInputType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCellInputType() {
        return cellInputType;
    }

    /**
     * Sets the value of the cellInputType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCellInputType(String value) {
        this.cellInputType = value;
    }

    /**
     * Gets the value of the enabled property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getEnabled() {
        return enabled;
    }

    /**
     * Sets the value of the enabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnabled(Boolean value) {
        this.enabled = value;
    }

    /**
     * Gets the value of the opaque property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOpaque() {
        return opaque;
    }

    /**
     * Sets the value of the opaque property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOpaque(Boolean value) {
        this.opaque = value;
    }

    /**
     * Gets the value of the controllertype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControllertype() {
        return controllertype;
    }

    /**
     * Sets the value of the controllertype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControllertype(String value) {
        this.controllertype = value;
    }

    /**
     * Gets the value of the dynamicCellHeightsDefault property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDynamicCellHeightsDefault() {
        return dynamicCellHeightsDefault;
    }

    /**
     * Sets the value of the dynamicCellHeightsDefault property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDynamicCellHeightsDefault(Boolean value) {
        this.dynamicCellHeightsDefault = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            JAXBElement<?> theLayoutconstraints;
            theLayoutconstraints = this.getLayoutconstraints();
            strategy.appendField(locator, this, "layoutconstraints", buffer, theLayoutconstraints);
        }
        {
            ClearBorder theClearBorder;
            theClearBorder = this.getClearBorder();
            strategy.appendField(locator, this, "clearBorder", buffer, theClearBorder);
        }
        {
            List<JAXBElement<?>> theBorder;
            theBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            strategy.appendField(locator, this, "border", buffer, theBorder);
        }
        {
            MinimumSize theMinimumSize;
            theMinimumSize = this.getMinimumSize();
            strategy.appendField(locator, this, "minimumSize", buffer, theMinimumSize);
        }
        {
            PreferredSize thePreferredSize;
            thePreferredSize = this.getPreferredSize();
            strategy.appendField(locator, this, "preferredSize", buffer, thePreferredSize);
        }
        {
            StrictSize theStrictSize;
            theStrictSize = this.getStrictSize();
            strategy.appendField(locator, this, "strictSize", buffer, theStrictSize);
        }
        {
            Font theFont;
            theFont = this.getFont();
            strategy.appendField(locator, this, "font", buffer, theFont);
        }
        {
            Background theBackground;
            theBackground = this.getBackground();
            strategy.appendField(locator, this, "background", buffer, theBackground);
        }
        {
            String theDescription;
            theDescription = this.getDescription();
            strategy.appendField(locator, this, "description", buffer, theDescription);
        }
        {
            List<MatrixColumn> theMatrixColumn;
            theMatrixColumn = (((this.matrixColumn!= null)&&(!this.matrixColumn.isEmpty()))?this.getMatrixColumn():null);
            strategy.appendField(locator, this, "matrixColumn", buffer, theMatrixColumn);
        }
        {
            NewEnabled theNewEnabled;
            theNewEnabled = this.getNewEnabled();
            strategy.appendField(locator, this, "newEnabled", buffer, theNewEnabled);
        }
        {
            EditEnabled theEditEnabled;
            theEditEnabled = this.getEditEnabled();
            strategy.appendField(locator, this, "editEnabled", buffer, theEditEnabled);
        }
        {
            DeleteEnabled theDeleteEnabled;
            theDeleteEnabled = this.getDeleteEnabled();
            strategy.appendField(locator, this, "deleteEnabled", buffer, theDeleteEnabled);
        }
        {
            CloneEnabled theCloneEnabled;
            theCloneEnabled = this.getCloneEnabled();
            strategy.appendField(locator, this, "cloneEnabled", buffer, theCloneEnabled);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String theMatrixPreferencesField;
            theMatrixPreferencesField = this.getMatrixPreferencesField();
            strategy.appendField(locator, this, "matrixPreferencesField", buffer, theMatrixPreferencesField);
        }
        {
            String theEntityX;
            theEntityX = this.getEntityX();
            strategy.appendField(locator, this, "entityX", buffer, theEntityX);
        }
        {
            String theEntityY;
            theEntityY = this.getEntityY();
            strategy.appendField(locator, this, "entityY", buffer, theEntityY);
        }
        {
            String theEntityMatrix;
            theEntityMatrix = this.getEntityMatrix();
            strategy.appendField(locator, this, "entityMatrix", buffer, theEntityMatrix);
        }
        {
            String theEntityFieldMatrixParent;
            theEntityFieldMatrixParent = this.getEntityFieldMatrixParent();
            strategy.appendField(locator, this, "entityFieldMatrixParent", buffer, theEntityFieldMatrixParent);
        }
        {
            String theEntityFieldMatrixXRefField;
            theEntityFieldMatrixXRefField = this.getEntityFieldMatrixXRefField();
            strategy.appendField(locator, this, "entityFieldMatrixXRefField", buffer, theEntityFieldMatrixXRefField);
        }
        {
            String theEntityMatrixValueField;
            theEntityMatrixValueField = this.getEntityMatrixValueField();
            strategy.appendField(locator, this, "entityMatrixValueField", buffer, theEntityMatrixValueField);
        }
        {
            String theEntityMatrixNumberState;
            theEntityMatrixNumberState = this.getEntityMatrixNumberState();
            strategy.appendField(locator, this, "entityMatrixNumberState", buffer, theEntityMatrixNumberState);
        }
        {
            String theEntityFieldCategorie;
            theEntityFieldCategorie = this.getEntityFieldCategorie();
            strategy.appendField(locator, this, "entityFieldCategorie", buffer, theEntityFieldCategorie);
        }
        {
            String theEntityFieldX;
            theEntityFieldX = this.getEntityFieldX();
            strategy.appendField(locator, this, "entityFieldX", buffer, theEntityFieldX);
        }
        {
            String theEntityYParentField;
            theEntityYParentField = this.getEntityYParentField();
            strategy.appendField(locator, this, "entityYParentField", buffer, theEntityYParentField);
        }
        {
            String theEntityFieldY;
            theEntityFieldY = this.getEntityFieldY();
            strategy.appendField(locator, this, "entityFieldY", buffer, theEntityFieldY);
        }
        {
            String theEntityXSortingFields;
            theEntityXSortingFields = this.getEntityXSortingFields();
            strategy.appendField(locator, this, "entityXSortingFields", buffer, theEntityXSortingFields);
        }
        {
            String theEntityYSortingFields;
            theEntityYSortingFields = this.getEntityYSortingFields();
            strategy.appendField(locator, this, "entityYSortingFields", buffer, theEntityYSortingFields);
        }
        {
            String theEntityXHeader;
            theEntityXHeader = this.getEntityXHeader();
            strategy.appendField(locator, this, "entityXHeader", buffer, theEntityXHeader);
        }
        {
            String theEntityYHeader;
            theEntityYHeader = this.getEntityYHeader();
            strategy.appendField(locator, this, "entityYHeader", buffer, theEntityYHeader);
        }
        {
            String theEntityMatrixReferenceField;
            theEntityMatrixReferenceField = this.getEntityMatrixReferenceField();
            strategy.appendField(locator, this, "entityMatrixReferenceField", buffer, theEntityMatrixReferenceField);
        }
        {
            String theEntityMatrixValueType;
            theEntityMatrixValueType = this.getEntityMatrixValueType();
            strategy.appendField(locator, this, "entityMatrixValueType", buffer, theEntityMatrixValueType);
        }
        {
            String theEntityXVlpId;
            theEntityXVlpId = this.getEntityXVlpId();
            strategy.appendField(locator, this, "entityXVlpId", buffer, theEntityXVlpId);
        }
        {
            String theEntityXVlpIdfieldname;
            theEntityXVlpIdfieldname = this.getEntityXVlpIdfieldname();
            strategy.appendField(locator, this, "entityXVlpIdfieldname", buffer, theEntityXVlpIdfieldname);
        }
        {
            String theEntityXVlpFieldname;
            theEntityXVlpFieldname = this.getEntityXVlpFieldname();
            strategy.appendField(locator, this, "entityXVlpFieldname", buffer, theEntityXVlpFieldname);
        }
        {
            String theEntityXVlpReferenceParamName;
            theEntityXVlpReferenceParamName = this.getEntityXVlpReferenceParamName();
            strategy.appendField(locator, this, "entityXVlpReferenceParamName", buffer, theEntityXVlpReferenceParamName);
        }
        {
            String theCellInputType;
            theCellInputType = this.getCellInputType();
            strategy.appendField(locator, this, "cellInputType", buffer, theCellInputType);
        }
        {
            Boolean theEnabled;
            theEnabled = this.getEnabled();
            strategy.appendField(locator, this, "enabled", buffer, theEnabled);
        }
        {
            Boolean theOpaque;
            theOpaque = this.getOpaque();
            strategy.appendField(locator, this, "opaque", buffer, theOpaque);
        }
        {
            String theControllertype;
            theControllertype = this.getControllertype();
            strategy.appendField(locator, this, "controllertype", buffer, theControllertype);
        }
        {
            Boolean theDynamicCellHeightsDefault;
            theDynamicCellHeightsDefault = this.getDynamicCellHeightsDefault();
            strategy.appendField(locator, this, "dynamicCellHeightsDefault", buffer, theDynamicCellHeightsDefault);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Matrix)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Matrix that = ((Matrix) object);
        {
            JAXBElement<?> lhsLayoutconstraints;
            lhsLayoutconstraints = this.getLayoutconstraints();
            JAXBElement<?> rhsLayoutconstraints;
            rhsLayoutconstraints = that.getLayoutconstraints();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "layoutconstraints", lhsLayoutconstraints), LocatorUtils.property(thatLocator, "layoutconstraints", rhsLayoutconstraints), lhsLayoutconstraints, rhsLayoutconstraints)) {
                return false;
            }
        }
        {
            ClearBorder lhsClearBorder;
            lhsClearBorder = this.getClearBorder();
            ClearBorder rhsClearBorder;
            rhsClearBorder = that.getClearBorder();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "clearBorder", lhsClearBorder), LocatorUtils.property(thatLocator, "clearBorder", rhsClearBorder), lhsClearBorder, rhsClearBorder)) {
                return false;
            }
        }
        {
            List<JAXBElement<?>> lhsBorder;
            lhsBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            List<JAXBElement<?>> rhsBorder;
            rhsBorder = (((that.border!= null)&&(!that.border.isEmpty()))?that.getBorder():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "border", lhsBorder), LocatorUtils.property(thatLocator, "border", rhsBorder), lhsBorder, rhsBorder)) {
                return false;
            }
        }
        {
            MinimumSize lhsMinimumSize;
            lhsMinimumSize = this.getMinimumSize();
            MinimumSize rhsMinimumSize;
            rhsMinimumSize = that.getMinimumSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "minimumSize", lhsMinimumSize), LocatorUtils.property(thatLocator, "minimumSize", rhsMinimumSize), lhsMinimumSize, rhsMinimumSize)) {
                return false;
            }
        }
        {
            PreferredSize lhsPreferredSize;
            lhsPreferredSize = this.getPreferredSize();
            PreferredSize rhsPreferredSize;
            rhsPreferredSize = that.getPreferredSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "preferredSize", lhsPreferredSize), LocatorUtils.property(thatLocator, "preferredSize", rhsPreferredSize), lhsPreferredSize, rhsPreferredSize)) {
                return false;
            }
        }
        {
            StrictSize lhsStrictSize;
            lhsStrictSize = this.getStrictSize();
            StrictSize rhsStrictSize;
            rhsStrictSize = that.getStrictSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "strictSize", lhsStrictSize), LocatorUtils.property(thatLocator, "strictSize", rhsStrictSize), lhsStrictSize, rhsStrictSize)) {
                return false;
            }
        }
        {
            Font lhsFont;
            lhsFont = this.getFont();
            Font rhsFont;
            rhsFont = that.getFont();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "font", lhsFont), LocatorUtils.property(thatLocator, "font", rhsFont), lhsFont, rhsFont)) {
                return false;
            }
        }
        {
            Background lhsBackground;
            lhsBackground = this.getBackground();
            Background rhsBackground;
            rhsBackground = that.getBackground();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "background", lhsBackground), LocatorUtils.property(thatLocator, "background", rhsBackground), lhsBackground, rhsBackground)) {
                return false;
            }
        }
        {
            String lhsDescription;
            lhsDescription = this.getDescription();
            String rhsDescription;
            rhsDescription = that.getDescription();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "description", lhsDescription), LocatorUtils.property(thatLocator, "description", rhsDescription), lhsDescription, rhsDescription)) {
                return false;
            }
        }
        {
            List<MatrixColumn> lhsMatrixColumn;
            lhsMatrixColumn = (((this.matrixColumn!= null)&&(!this.matrixColumn.isEmpty()))?this.getMatrixColumn():null);
            List<MatrixColumn> rhsMatrixColumn;
            rhsMatrixColumn = (((that.matrixColumn!= null)&&(!that.matrixColumn.isEmpty()))?that.getMatrixColumn():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "matrixColumn", lhsMatrixColumn), LocatorUtils.property(thatLocator, "matrixColumn", rhsMatrixColumn), lhsMatrixColumn, rhsMatrixColumn)) {
                return false;
            }
        }
        {
            NewEnabled lhsNewEnabled;
            lhsNewEnabled = this.getNewEnabled();
            NewEnabled rhsNewEnabled;
            rhsNewEnabled = that.getNewEnabled();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "newEnabled", lhsNewEnabled), LocatorUtils.property(thatLocator, "newEnabled", rhsNewEnabled), lhsNewEnabled, rhsNewEnabled)) {
                return false;
            }
        }
        {
            EditEnabled lhsEditEnabled;
            lhsEditEnabled = this.getEditEnabled();
            EditEnabled rhsEditEnabled;
            rhsEditEnabled = that.getEditEnabled();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "editEnabled", lhsEditEnabled), LocatorUtils.property(thatLocator, "editEnabled", rhsEditEnabled), lhsEditEnabled, rhsEditEnabled)) {
                return false;
            }
        }
        {
            DeleteEnabled lhsDeleteEnabled;
            lhsDeleteEnabled = this.getDeleteEnabled();
            DeleteEnabled rhsDeleteEnabled;
            rhsDeleteEnabled = that.getDeleteEnabled();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "deleteEnabled", lhsDeleteEnabled), LocatorUtils.property(thatLocator, "deleteEnabled", rhsDeleteEnabled), lhsDeleteEnabled, rhsDeleteEnabled)) {
                return false;
            }
        }
        {
            CloneEnabled lhsCloneEnabled;
            lhsCloneEnabled = this.getCloneEnabled();
            CloneEnabled rhsCloneEnabled;
            rhsCloneEnabled = that.getCloneEnabled();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "cloneEnabled", lhsCloneEnabled), LocatorUtils.property(thatLocator, "cloneEnabled", rhsCloneEnabled), lhsCloneEnabled, rhsCloneEnabled)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsMatrixPreferencesField;
            lhsMatrixPreferencesField = this.getMatrixPreferencesField();
            String rhsMatrixPreferencesField;
            rhsMatrixPreferencesField = that.getMatrixPreferencesField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "matrixPreferencesField", lhsMatrixPreferencesField), LocatorUtils.property(thatLocator, "matrixPreferencesField", rhsMatrixPreferencesField), lhsMatrixPreferencesField, rhsMatrixPreferencesField)) {
                return false;
            }
        }
        {
            String lhsEntityX;
            lhsEntityX = this.getEntityX();
            String rhsEntityX;
            rhsEntityX = that.getEntityX();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityX", lhsEntityX), LocatorUtils.property(thatLocator, "entityX", rhsEntityX), lhsEntityX, rhsEntityX)) {
                return false;
            }
        }
        {
            String lhsEntityY;
            lhsEntityY = this.getEntityY();
            String rhsEntityY;
            rhsEntityY = that.getEntityY();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityY", lhsEntityY), LocatorUtils.property(thatLocator, "entityY", rhsEntityY), lhsEntityY, rhsEntityY)) {
                return false;
            }
        }
        {
            String lhsEntityMatrix;
            lhsEntityMatrix = this.getEntityMatrix();
            String rhsEntityMatrix;
            rhsEntityMatrix = that.getEntityMatrix();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityMatrix", lhsEntityMatrix), LocatorUtils.property(thatLocator, "entityMatrix", rhsEntityMatrix), lhsEntityMatrix, rhsEntityMatrix)) {
                return false;
            }
        }
        {
            String lhsEntityFieldMatrixParent;
            lhsEntityFieldMatrixParent = this.getEntityFieldMatrixParent();
            String rhsEntityFieldMatrixParent;
            rhsEntityFieldMatrixParent = that.getEntityFieldMatrixParent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityFieldMatrixParent", lhsEntityFieldMatrixParent), LocatorUtils.property(thatLocator, "entityFieldMatrixParent", rhsEntityFieldMatrixParent), lhsEntityFieldMatrixParent, rhsEntityFieldMatrixParent)) {
                return false;
            }
        }
        {
            String lhsEntityFieldMatrixXRefField;
            lhsEntityFieldMatrixXRefField = this.getEntityFieldMatrixXRefField();
            String rhsEntityFieldMatrixXRefField;
            rhsEntityFieldMatrixXRefField = that.getEntityFieldMatrixXRefField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityFieldMatrixXRefField", lhsEntityFieldMatrixXRefField), LocatorUtils.property(thatLocator, "entityFieldMatrixXRefField", rhsEntityFieldMatrixXRefField), lhsEntityFieldMatrixXRefField, rhsEntityFieldMatrixXRefField)) {
                return false;
            }
        }
        {
            String lhsEntityMatrixValueField;
            lhsEntityMatrixValueField = this.getEntityMatrixValueField();
            String rhsEntityMatrixValueField;
            rhsEntityMatrixValueField = that.getEntityMatrixValueField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityMatrixValueField", lhsEntityMatrixValueField), LocatorUtils.property(thatLocator, "entityMatrixValueField", rhsEntityMatrixValueField), lhsEntityMatrixValueField, rhsEntityMatrixValueField)) {
                return false;
            }
        }
        {
            String lhsEntityMatrixNumberState;
            lhsEntityMatrixNumberState = this.getEntityMatrixNumberState();
            String rhsEntityMatrixNumberState;
            rhsEntityMatrixNumberState = that.getEntityMatrixNumberState();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityMatrixNumberState", lhsEntityMatrixNumberState), LocatorUtils.property(thatLocator, "entityMatrixNumberState", rhsEntityMatrixNumberState), lhsEntityMatrixNumberState, rhsEntityMatrixNumberState)) {
                return false;
            }
        }
        {
            String lhsEntityFieldCategorie;
            lhsEntityFieldCategorie = this.getEntityFieldCategorie();
            String rhsEntityFieldCategorie;
            rhsEntityFieldCategorie = that.getEntityFieldCategorie();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityFieldCategorie", lhsEntityFieldCategorie), LocatorUtils.property(thatLocator, "entityFieldCategorie", rhsEntityFieldCategorie), lhsEntityFieldCategorie, rhsEntityFieldCategorie)) {
                return false;
            }
        }
        {
            String lhsEntityFieldX;
            lhsEntityFieldX = this.getEntityFieldX();
            String rhsEntityFieldX;
            rhsEntityFieldX = that.getEntityFieldX();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityFieldX", lhsEntityFieldX), LocatorUtils.property(thatLocator, "entityFieldX", rhsEntityFieldX), lhsEntityFieldX, rhsEntityFieldX)) {
                return false;
            }
        }
        {
            String lhsEntityYParentField;
            lhsEntityYParentField = this.getEntityYParentField();
            String rhsEntityYParentField;
            rhsEntityYParentField = that.getEntityYParentField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityYParentField", lhsEntityYParentField), LocatorUtils.property(thatLocator, "entityYParentField", rhsEntityYParentField), lhsEntityYParentField, rhsEntityYParentField)) {
                return false;
            }
        }
        {
            String lhsEntityFieldY;
            lhsEntityFieldY = this.getEntityFieldY();
            String rhsEntityFieldY;
            rhsEntityFieldY = that.getEntityFieldY();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityFieldY", lhsEntityFieldY), LocatorUtils.property(thatLocator, "entityFieldY", rhsEntityFieldY), lhsEntityFieldY, rhsEntityFieldY)) {
                return false;
            }
        }
        {
            String lhsEntityXSortingFields;
            lhsEntityXSortingFields = this.getEntityXSortingFields();
            String rhsEntityXSortingFields;
            rhsEntityXSortingFields = that.getEntityXSortingFields();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityXSortingFields", lhsEntityXSortingFields), LocatorUtils.property(thatLocator, "entityXSortingFields", rhsEntityXSortingFields), lhsEntityXSortingFields, rhsEntityXSortingFields)) {
                return false;
            }
        }
        {
            String lhsEntityYSortingFields;
            lhsEntityYSortingFields = this.getEntityYSortingFields();
            String rhsEntityYSortingFields;
            rhsEntityYSortingFields = that.getEntityYSortingFields();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityYSortingFields", lhsEntityYSortingFields), LocatorUtils.property(thatLocator, "entityYSortingFields", rhsEntityYSortingFields), lhsEntityYSortingFields, rhsEntityYSortingFields)) {
                return false;
            }
        }
        {
            String lhsEntityXHeader;
            lhsEntityXHeader = this.getEntityXHeader();
            String rhsEntityXHeader;
            rhsEntityXHeader = that.getEntityXHeader();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityXHeader", lhsEntityXHeader), LocatorUtils.property(thatLocator, "entityXHeader", rhsEntityXHeader), lhsEntityXHeader, rhsEntityXHeader)) {
                return false;
            }
        }
        {
            String lhsEntityYHeader;
            lhsEntityYHeader = this.getEntityYHeader();
            String rhsEntityYHeader;
            rhsEntityYHeader = that.getEntityYHeader();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityYHeader", lhsEntityYHeader), LocatorUtils.property(thatLocator, "entityYHeader", rhsEntityYHeader), lhsEntityYHeader, rhsEntityYHeader)) {
                return false;
            }
        }
        {
            String lhsEntityMatrixReferenceField;
            lhsEntityMatrixReferenceField = this.getEntityMatrixReferenceField();
            String rhsEntityMatrixReferenceField;
            rhsEntityMatrixReferenceField = that.getEntityMatrixReferenceField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityMatrixReferenceField", lhsEntityMatrixReferenceField), LocatorUtils.property(thatLocator, "entityMatrixReferenceField", rhsEntityMatrixReferenceField), lhsEntityMatrixReferenceField, rhsEntityMatrixReferenceField)) {
                return false;
            }
        }
        {
            String lhsEntityMatrixValueType;
            lhsEntityMatrixValueType = this.getEntityMatrixValueType();
            String rhsEntityMatrixValueType;
            rhsEntityMatrixValueType = that.getEntityMatrixValueType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityMatrixValueType", lhsEntityMatrixValueType), LocatorUtils.property(thatLocator, "entityMatrixValueType", rhsEntityMatrixValueType), lhsEntityMatrixValueType, rhsEntityMatrixValueType)) {
                return false;
            }
        }
        {
            String lhsEntityXVlpId;
            lhsEntityXVlpId = this.getEntityXVlpId();
            String rhsEntityXVlpId;
            rhsEntityXVlpId = that.getEntityXVlpId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityXVlpId", lhsEntityXVlpId), LocatorUtils.property(thatLocator, "entityXVlpId", rhsEntityXVlpId), lhsEntityXVlpId, rhsEntityXVlpId)) {
                return false;
            }
        }
        {
            String lhsEntityXVlpIdfieldname;
            lhsEntityXVlpIdfieldname = this.getEntityXVlpIdfieldname();
            String rhsEntityXVlpIdfieldname;
            rhsEntityXVlpIdfieldname = that.getEntityXVlpIdfieldname();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityXVlpIdfieldname", lhsEntityXVlpIdfieldname), LocatorUtils.property(thatLocator, "entityXVlpIdfieldname", rhsEntityXVlpIdfieldname), lhsEntityXVlpIdfieldname, rhsEntityXVlpIdfieldname)) {
                return false;
            }
        }
        {
            String lhsEntityXVlpFieldname;
            lhsEntityXVlpFieldname = this.getEntityXVlpFieldname();
            String rhsEntityXVlpFieldname;
            rhsEntityXVlpFieldname = that.getEntityXVlpFieldname();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityXVlpFieldname", lhsEntityXVlpFieldname), LocatorUtils.property(thatLocator, "entityXVlpFieldname", rhsEntityXVlpFieldname), lhsEntityXVlpFieldname, rhsEntityXVlpFieldname)) {
                return false;
            }
        }
        {
            String lhsEntityXVlpReferenceParamName;
            lhsEntityXVlpReferenceParamName = this.getEntityXVlpReferenceParamName();
            String rhsEntityXVlpReferenceParamName;
            rhsEntityXVlpReferenceParamName = that.getEntityXVlpReferenceParamName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityXVlpReferenceParamName", lhsEntityXVlpReferenceParamName), LocatorUtils.property(thatLocator, "entityXVlpReferenceParamName", rhsEntityXVlpReferenceParamName), lhsEntityXVlpReferenceParamName, rhsEntityXVlpReferenceParamName)) {
                return false;
            }
        }
        {
            String lhsCellInputType;
            lhsCellInputType = this.getCellInputType();
            String rhsCellInputType;
            rhsCellInputType = that.getCellInputType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "cellInputType", lhsCellInputType), LocatorUtils.property(thatLocator, "cellInputType", rhsCellInputType), lhsCellInputType, rhsCellInputType)) {
                return false;
            }
        }
        {
            Boolean lhsEnabled;
            lhsEnabled = this.getEnabled();
            Boolean rhsEnabled;
            rhsEnabled = that.getEnabled();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "enabled", lhsEnabled), LocatorUtils.property(thatLocator, "enabled", rhsEnabled), lhsEnabled, rhsEnabled)) {
                return false;
            }
        }
        {
            Boolean lhsOpaque;
            lhsOpaque = this.getOpaque();
            Boolean rhsOpaque;
            rhsOpaque = that.getOpaque();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "opaque", lhsOpaque), LocatorUtils.property(thatLocator, "opaque", rhsOpaque), lhsOpaque, rhsOpaque)) {
                return false;
            }
        }
        {
            String lhsControllertype;
            lhsControllertype = this.getControllertype();
            String rhsControllertype;
            rhsControllertype = that.getControllertype();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "controllertype", lhsControllertype), LocatorUtils.property(thatLocator, "controllertype", rhsControllertype), lhsControllertype, rhsControllertype)) {
                return false;
            }
        }
        {
            Boolean lhsDynamicCellHeightsDefault;
            lhsDynamicCellHeightsDefault = this.getDynamicCellHeightsDefault();
            Boolean rhsDynamicCellHeightsDefault;
            rhsDynamicCellHeightsDefault = that.getDynamicCellHeightsDefault();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dynamicCellHeightsDefault", lhsDynamicCellHeightsDefault), LocatorUtils.property(thatLocator, "dynamicCellHeightsDefault", rhsDynamicCellHeightsDefault), lhsDynamicCellHeightsDefault, rhsDynamicCellHeightsDefault)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            JAXBElement<?> theLayoutconstraints;
            theLayoutconstraints = this.getLayoutconstraints();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "layoutconstraints", theLayoutconstraints), currentHashCode, theLayoutconstraints);
        }
        {
            ClearBorder theClearBorder;
            theClearBorder = this.getClearBorder();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "clearBorder", theClearBorder), currentHashCode, theClearBorder);
        }
        {
            List<JAXBElement<?>> theBorder;
            theBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "border", theBorder), currentHashCode, theBorder);
        }
        {
            MinimumSize theMinimumSize;
            theMinimumSize = this.getMinimumSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "minimumSize", theMinimumSize), currentHashCode, theMinimumSize);
        }
        {
            PreferredSize thePreferredSize;
            thePreferredSize = this.getPreferredSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "preferredSize", thePreferredSize), currentHashCode, thePreferredSize);
        }
        {
            StrictSize theStrictSize;
            theStrictSize = this.getStrictSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "strictSize", theStrictSize), currentHashCode, theStrictSize);
        }
        {
            Font theFont;
            theFont = this.getFont();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "font", theFont), currentHashCode, theFont);
        }
        {
            Background theBackground;
            theBackground = this.getBackground();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "background", theBackground), currentHashCode, theBackground);
        }
        {
            String theDescription;
            theDescription = this.getDescription();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "description", theDescription), currentHashCode, theDescription);
        }
        {
            List<MatrixColumn> theMatrixColumn;
            theMatrixColumn = (((this.matrixColumn!= null)&&(!this.matrixColumn.isEmpty()))?this.getMatrixColumn():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "matrixColumn", theMatrixColumn), currentHashCode, theMatrixColumn);
        }
        {
            NewEnabled theNewEnabled;
            theNewEnabled = this.getNewEnabled();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "newEnabled", theNewEnabled), currentHashCode, theNewEnabled);
        }
        {
            EditEnabled theEditEnabled;
            theEditEnabled = this.getEditEnabled();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "editEnabled", theEditEnabled), currentHashCode, theEditEnabled);
        }
        {
            DeleteEnabled theDeleteEnabled;
            theDeleteEnabled = this.getDeleteEnabled();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "deleteEnabled", theDeleteEnabled), currentHashCode, theDeleteEnabled);
        }
        {
            CloneEnabled theCloneEnabled;
            theCloneEnabled = this.getCloneEnabled();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "cloneEnabled", theCloneEnabled), currentHashCode, theCloneEnabled);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String theMatrixPreferencesField;
            theMatrixPreferencesField = this.getMatrixPreferencesField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "matrixPreferencesField", theMatrixPreferencesField), currentHashCode, theMatrixPreferencesField);
        }
        {
            String theEntityX;
            theEntityX = this.getEntityX();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityX", theEntityX), currentHashCode, theEntityX);
        }
        {
            String theEntityY;
            theEntityY = this.getEntityY();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityY", theEntityY), currentHashCode, theEntityY);
        }
        {
            String theEntityMatrix;
            theEntityMatrix = this.getEntityMatrix();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityMatrix", theEntityMatrix), currentHashCode, theEntityMatrix);
        }
        {
            String theEntityFieldMatrixParent;
            theEntityFieldMatrixParent = this.getEntityFieldMatrixParent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityFieldMatrixParent", theEntityFieldMatrixParent), currentHashCode, theEntityFieldMatrixParent);
        }
        {
            String theEntityFieldMatrixXRefField;
            theEntityFieldMatrixXRefField = this.getEntityFieldMatrixXRefField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityFieldMatrixXRefField", theEntityFieldMatrixXRefField), currentHashCode, theEntityFieldMatrixXRefField);
        }
        {
            String theEntityMatrixValueField;
            theEntityMatrixValueField = this.getEntityMatrixValueField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityMatrixValueField", theEntityMatrixValueField), currentHashCode, theEntityMatrixValueField);
        }
        {
            String theEntityMatrixNumberState;
            theEntityMatrixNumberState = this.getEntityMatrixNumberState();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityMatrixNumberState", theEntityMatrixNumberState), currentHashCode, theEntityMatrixNumberState);
        }
        {
            String theEntityFieldCategorie;
            theEntityFieldCategorie = this.getEntityFieldCategorie();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityFieldCategorie", theEntityFieldCategorie), currentHashCode, theEntityFieldCategorie);
        }
        {
            String theEntityFieldX;
            theEntityFieldX = this.getEntityFieldX();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityFieldX", theEntityFieldX), currentHashCode, theEntityFieldX);
        }
        {
            String theEntityYParentField;
            theEntityYParentField = this.getEntityYParentField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityYParentField", theEntityYParentField), currentHashCode, theEntityYParentField);
        }
        {
            String theEntityFieldY;
            theEntityFieldY = this.getEntityFieldY();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityFieldY", theEntityFieldY), currentHashCode, theEntityFieldY);
        }
        {
            String theEntityXSortingFields;
            theEntityXSortingFields = this.getEntityXSortingFields();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityXSortingFields", theEntityXSortingFields), currentHashCode, theEntityXSortingFields);
        }
        {
            String theEntityYSortingFields;
            theEntityYSortingFields = this.getEntityYSortingFields();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityYSortingFields", theEntityYSortingFields), currentHashCode, theEntityYSortingFields);
        }
        {
            String theEntityXHeader;
            theEntityXHeader = this.getEntityXHeader();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityXHeader", theEntityXHeader), currentHashCode, theEntityXHeader);
        }
        {
            String theEntityYHeader;
            theEntityYHeader = this.getEntityYHeader();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityYHeader", theEntityYHeader), currentHashCode, theEntityYHeader);
        }
        {
            String theEntityMatrixReferenceField;
            theEntityMatrixReferenceField = this.getEntityMatrixReferenceField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityMatrixReferenceField", theEntityMatrixReferenceField), currentHashCode, theEntityMatrixReferenceField);
        }
        {
            String theEntityMatrixValueType;
            theEntityMatrixValueType = this.getEntityMatrixValueType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityMatrixValueType", theEntityMatrixValueType), currentHashCode, theEntityMatrixValueType);
        }
        {
            String theEntityXVlpId;
            theEntityXVlpId = this.getEntityXVlpId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityXVlpId", theEntityXVlpId), currentHashCode, theEntityXVlpId);
        }
        {
            String theEntityXVlpIdfieldname;
            theEntityXVlpIdfieldname = this.getEntityXVlpIdfieldname();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityXVlpIdfieldname", theEntityXVlpIdfieldname), currentHashCode, theEntityXVlpIdfieldname);
        }
        {
            String theEntityXVlpFieldname;
            theEntityXVlpFieldname = this.getEntityXVlpFieldname();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityXVlpFieldname", theEntityXVlpFieldname), currentHashCode, theEntityXVlpFieldname);
        }
        {
            String theEntityXVlpReferenceParamName;
            theEntityXVlpReferenceParamName = this.getEntityXVlpReferenceParamName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityXVlpReferenceParamName", theEntityXVlpReferenceParamName), currentHashCode, theEntityXVlpReferenceParamName);
        }
        {
            String theCellInputType;
            theCellInputType = this.getCellInputType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "cellInputType", theCellInputType), currentHashCode, theCellInputType);
        }
        {
            Boolean theEnabled;
            theEnabled = this.getEnabled();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "enabled", theEnabled), currentHashCode, theEnabled);
        }
        {
            Boolean theOpaque;
            theOpaque = this.getOpaque();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "opaque", theOpaque), currentHashCode, theOpaque);
        }
        {
            String theControllertype;
            theControllertype = this.getControllertype();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "controllertype", theControllertype), currentHashCode, theControllertype);
        }
        {
            Boolean theDynamicCellHeightsDefault;
            theDynamicCellHeightsDefault = this.getDynamicCellHeightsDefault();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dynamicCellHeightsDefault", theDynamicCellHeightsDefault), currentHashCode, theDynamicCellHeightsDefault);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Matrix) {
            final Matrix copy = ((Matrix) draftCopy);
            if (this.layoutconstraints!= null) {
                JAXBElement<?> sourceLayoutconstraints;
                sourceLayoutconstraints = this.getLayoutconstraints();
                @SuppressWarnings("unchecked")
                JAXBElement<?> copyLayoutconstraints = ((JAXBElement<?> ) strategy.copy(LocatorUtils.property(locator, "layoutconstraints", sourceLayoutconstraints), sourceLayoutconstraints));
                copy.setLayoutconstraints(copyLayoutconstraints);
            } else {
                copy.layoutconstraints = null;
            }
            if (this.clearBorder!= null) {
                ClearBorder sourceClearBorder;
                sourceClearBorder = this.getClearBorder();
                ClearBorder copyClearBorder = ((ClearBorder) strategy.copy(LocatorUtils.property(locator, "clearBorder", sourceClearBorder), sourceClearBorder));
                copy.setClearBorder(copyClearBorder);
            } else {
                copy.clearBorder = null;
            }
            if ((this.border!= null)&&(!this.border.isEmpty())) {
                List<JAXBElement<?>> sourceBorder;
                sourceBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
                @SuppressWarnings("unchecked")
                List<JAXBElement<?>> copyBorder = ((List<JAXBElement<?>> ) strategy.copy(LocatorUtils.property(locator, "border", sourceBorder), sourceBorder));
                copy.border = null;
                if (copyBorder!= null) {
                    List<JAXBElement<?>> uniqueBorderl = copy.getBorder();
                    uniqueBorderl.addAll(copyBorder);
                }
            } else {
                copy.border = null;
            }
            if (this.minimumSize!= null) {
                MinimumSize sourceMinimumSize;
                sourceMinimumSize = this.getMinimumSize();
                MinimumSize copyMinimumSize = ((MinimumSize) strategy.copy(LocatorUtils.property(locator, "minimumSize", sourceMinimumSize), sourceMinimumSize));
                copy.setMinimumSize(copyMinimumSize);
            } else {
                copy.minimumSize = null;
            }
            if (this.preferredSize!= null) {
                PreferredSize sourcePreferredSize;
                sourcePreferredSize = this.getPreferredSize();
                PreferredSize copyPreferredSize = ((PreferredSize) strategy.copy(LocatorUtils.property(locator, "preferredSize", sourcePreferredSize), sourcePreferredSize));
                copy.setPreferredSize(copyPreferredSize);
            } else {
                copy.preferredSize = null;
            }
            if (this.strictSize!= null) {
                StrictSize sourceStrictSize;
                sourceStrictSize = this.getStrictSize();
                StrictSize copyStrictSize = ((StrictSize) strategy.copy(LocatorUtils.property(locator, "strictSize", sourceStrictSize), sourceStrictSize));
                copy.setStrictSize(copyStrictSize);
            } else {
                copy.strictSize = null;
            }
            if (this.font!= null) {
                Font sourceFont;
                sourceFont = this.getFont();
                Font copyFont = ((Font) strategy.copy(LocatorUtils.property(locator, "font", sourceFont), sourceFont));
                copy.setFont(copyFont);
            } else {
                copy.font = null;
            }
            if (this.background!= null) {
                Background sourceBackground;
                sourceBackground = this.getBackground();
                Background copyBackground = ((Background) strategy.copy(LocatorUtils.property(locator, "background", sourceBackground), sourceBackground));
                copy.setBackground(copyBackground);
            } else {
                copy.background = null;
            }
            if (this.description!= null) {
                String sourceDescription;
                sourceDescription = this.getDescription();
                String copyDescription = ((String) strategy.copy(LocatorUtils.property(locator, "description", sourceDescription), sourceDescription));
                copy.setDescription(copyDescription);
            } else {
                copy.description = null;
            }
            if ((this.matrixColumn!= null)&&(!this.matrixColumn.isEmpty())) {
                List<MatrixColumn> sourceMatrixColumn;
                sourceMatrixColumn = (((this.matrixColumn!= null)&&(!this.matrixColumn.isEmpty()))?this.getMatrixColumn():null);
                @SuppressWarnings("unchecked")
                List<MatrixColumn> copyMatrixColumn = ((List<MatrixColumn> ) strategy.copy(LocatorUtils.property(locator, "matrixColumn", sourceMatrixColumn), sourceMatrixColumn));
                copy.matrixColumn = null;
                if (copyMatrixColumn!= null) {
                    List<MatrixColumn> uniqueMatrixColumnl = copy.getMatrixColumn();
                    uniqueMatrixColumnl.addAll(copyMatrixColumn);
                }
            } else {
                copy.matrixColumn = null;
            }
            if (this.newEnabled!= null) {
                NewEnabled sourceNewEnabled;
                sourceNewEnabled = this.getNewEnabled();
                NewEnabled copyNewEnabled = ((NewEnabled) strategy.copy(LocatorUtils.property(locator, "newEnabled", sourceNewEnabled), sourceNewEnabled));
                copy.setNewEnabled(copyNewEnabled);
            } else {
                copy.newEnabled = null;
            }
            if (this.editEnabled!= null) {
                EditEnabled sourceEditEnabled;
                sourceEditEnabled = this.getEditEnabled();
                EditEnabled copyEditEnabled = ((EditEnabled) strategy.copy(LocatorUtils.property(locator, "editEnabled", sourceEditEnabled), sourceEditEnabled));
                copy.setEditEnabled(copyEditEnabled);
            } else {
                copy.editEnabled = null;
            }
            if (this.deleteEnabled!= null) {
                DeleteEnabled sourceDeleteEnabled;
                sourceDeleteEnabled = this.getDeleteEnabled();
                DeleteEnabled copyDeleteEnabled = ((DeleteEnabled) strategy.copy(LocatorUtils.property(locator, "deleteEnabled", sourceDeleteEnabled), sourceDeleteEnabled));
                copy.setDeleteEnabled(copyDeleteEnabled);
            } else {
                copy.deleteEnabled = null;
            }
            if (this.cloneEnabled!= null) {
                CloneEnabled sourceCloneEnabled;
                sourceCloneEnabled = this.getCloneEnabled();
                CloneEnabled copyCloneEnabled = ((CloneEnabled) strategy.copy(LocatorUtils.property(locator, "cloneEnabled", sourceCloneEnabled), sourceCloneEnabled));
                copy.setCloneEnabled(copyCloneEnabled);
            } else {
                copy.cloneEnabled = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.matrixPreferencesField!= null) {
                String sourceMatrixPreferencesField;
                sourceMatrixPreferencesField = this.getMatrixPreferencesField();
                String copyMatrixPreferencesField = ((String) strategy.copy(LocatorUtils.property(locator, "matrixPreferencesField", sourceMatrixPreferencesField), sourceMatrixPreferencesField));
                copy.setMatrixPreferencesField(copyMatrixPreferencesField);
            } else {
                copy.matrixPreferencesField = null;
            }
            if (this.entityX!= null) {
                String sourceEntityX;
                sourceEntityX = this.getEntityX();
                String copyEntityX = ((String) strategy.copy(LocatorUtils.property(locator, "entityX", sourceEntityX), sourceEntityX));
                copy.setEntityX(copyEntityX);
            } else {
                copy.entityX = null;
            }
            if (this.entityY!= null) {
                String sourceEntityY;
                sourceEntityY = this.getEntityY();
                String copyEntityY = ((String) strategy.copy(LocatorUtils.property(locator, "entityY", sourceEntityY), sourceEntityY));
                copy.setEntityY(copyEntityY);
            } else {
                copy.entityY = null;
            }
            if (this.entityMatrix!= null) {
                String sourceEntityMatrix;
                sourceEntityMatrix = this.getEntityMatrix();
                String copyEntityMatrix = ((String) strategy.copy(LocatorUtils.property(locator, "entityMatrix", sourceEntityMatrix), sourceEntityMatrix));
                copy.setEntityMatrix(copyEntityMatrix);
            } else {
                copy.entityMatrix = null;
            }
            if (this.entityFieldMatrixParent!= null) {
                String sourceEntityFieldMatrixParent;
                sourceEntityFieldMatrixParent = this.getEntityFieldMatrixParent();
                String copyEntityFieldMatrixParent = ((String) strategy.copy(LocatorUtils.property(locator, "entityFieldMatrixParent", sourceEntityFieldMatrixParent), sourceEntityFieldMatrixParent));
                copy.setEntityFieldMatrixParent(copyEntityFieldMatrixParent);
            } else {
                copy.entityFieldMatrixParent = null;
            }
            if (this.entityFieldMatrixXRefField!= null) {
                String sourceEntityFieldMatrixXRefField;
                sourceEntityFieldMatrixXRefField = this.getEntityFieldMatrixXRefField();
                String copyEntityFieldMatrixXRefField = ((String) strategy.copy(LocatorUtils.property(locator, "entityFieldMatrixXRefField", sourceEntityFieldMatrixXRefField), sourceEntityFieldMatrixXRefField));
                copy.setEntityFieldMatrixXRefField(copyEntityFieldMatrixXRefField);
            } else {
                copy.entityFieldMatrixXRefField = null;
            }
            if (this.entityMatrixValueField!= null) {
                String sourceEntityMatrixValueField;
                sourceEntityMatrixValueField = this.getEntityMatrixValueField();
                String copyEntityMatrixValueField = ((String) strategy.copy(LocatorUtils.property(locator, "entityMatrixValueField", sourceEntityMatrixValueField), sourceEntityMatrixValueField));
                copy.setEntityMatrixValueField(copyEntityMatrixValueField);
            } else {
                copy.entityMatrixValueField = null;
            }
            if (this.entityMatrixNumberState!= null) {
                String sourceEntityMatrixNumberState;
                sourceEntityMatrixNumberState = this.getEntityMatrixNumberState();
                String copyEntityMatrixNumberState = ((String) strategy.copy(LocatorUtils.property(locator, "entityMatrixNumberState", sourceEntityMatrixNumberState), sourceEntityMatrixNumberState));
                copy.setEntityMatrixNumberState(copyEntityMatrixNumberState);
            } else {
                copy.entityMatrixNumberState = null;
            }
            if (this.entityFieldCategorie!= null) {
                String sourceEntityFieldCategorie;
                sourceEntityFieldCategorie = this.getEntityFieldCategorie();
                String copyEntityFieldCategorie = ((String) strategy.copy(LocatorUtils.property(locator, "entityFieldCategorie", sourceEntityFieldCategorie), sourceEntityFieldCategorie));
                copy.setEntityFieldCategorie(copyEntityFieldCategorie);
            } else {
                copy.entityFieldCategorie = null;
            }
            if (this.entityFieldX!= null) {
                String sourceEntityFieldX;
                sourceEntityFieldX = this.getEntityFieldX();
                String copyEntityFieldX = ((String) strategy.copy(LocatorUtils.property(locator, "entityFieldX", sourceEntityFieldX), sourceEntityFieldX));
                copy.setEntityFieldX(copyEntityFieldX);
            } else {
                copy.entityFieldX = null;
            }
            if (this.entityYParentField!= null) {
                String sourceEntityYParentField;
                sourceEntityYParentField = this.getEntityYParentField();
                String copyEntityYParentField = ((String) strategy.copy(LocatorUtils.property(locator, "entityYParentField", sourceEntityYParentField), sourceEntityYParentField));
                copy.setEntityYParentField(copyEntityYParentField);
            } else {
                copy.entityYParentField = null;
            }
            if (this.entityFieldY!= null) {
                String sourceEntityFieldY;
                sourceEntityFieldY = this.getEntityFieldY();
                String copyEntityFieldY = ((String) strategy.copy(LocatorUtils.property(locator, "entityFieldY", sourceEntityFieldY), sourceEntityFieldY));
                copy.setEntityFieldY(copyEntityFieldY);
            } else {
                copy.entityFieldY = null;
            }
            if (this.entityXSortingFields!= null) {
                String sourceEntityXSortingFields;
                sourceEntityXSortingFields = this.getEntityXSortingFields();
                String copyEntityXSortingFields = ((String) strategy.copy(LocatorUtils.property(locator, "entityXSortingFields", sourceEntityXSortingFields), sourceEntityXSortingFields));
                copy.setEntityXSortingFields(copyEntityXSortingFields);
            } else {
                copy.entityXSortingFields = null;
            }
            if (this.entityYSortingFields!= null) {
                String sourceEntityYSortingFields;
                sourceEntityYSortingFields = this.getEntityYSortingFields();
                String copyEntityYSortingFields = ((String) strategy.copy(LocatorUtils.property(locator, "entityYSortingFields", sourceEntityYSortingFields), sourceEntityYSortingFields));
                copy.setEntityYSortingFields(copyEntityYSortingFields);
            } else {
                copy.entityYSortingFields = null;
            }
            if (this.entityXHeader!= null) {
                String sourceEntityXHeader;
                sourceEntityXHeader = this.getEntityXHeader();
                String copyEntityXHeader = ((String) strategy.copy(LocatorUtils.property(locator, "entityXHeader", sourceEntityXHeader), sourceEntityXHeader));
                copy.setEntityXHeader(copyEntityXHeader);
            } else {
                copy.entityXHeader = null;
            }
            if (this.entityYHeader!= null) {
                String sourceEntityYHeader;
                sourceEntityYHeader = this.getEntityYHeader();
                String copyEntityYHeader = ((String) strategy.copy(LocatorUtils.property(locator, "entityYHeader", sourceEntityYHeader), sourceEntityYHeader));
                copy.setEntityYHeader(copyEntityYHeader);
            } else {
                copy.entityYHeader = null;
            }
            if (this.entityMatrixReferenceField!= null) {
                String sourceEntityMatrixReferenceField;
                sourceEntityMatrixReferenceField = this.getEntityMatrixReferenceField();
                String copyEntityMatrixReferenceField = ((String) strategy.copy(LocatorUtils.property(locator, "entityMatrixReferenceField", sourceEntityMatrixReferenceField), sourceEntityMatrixReferenceField));
                copy.setEntityMatrixReferenceField(copyEntityMatrixReferenceField);
            } else {
                copy.entityMatrixReferenceField = null;
            }
            if (this.entityMatrixValueType!= null) {
                String sourceEntityMatrixValueType;
                sourceEntityMatrixValueType = this.getEntityMatrixValueType();
                String copyEntityMatrixValueType = ((String) strategy.copy(LocatorUtils.property(locator, "entityMatrixValueType", sourceEntityMatrixValueType), sourceEntityMatrixValueType));
                copy.setEntityMatrixValueType(copyEntityMatrixValueType);
            } else {
                copy.entityMatrixValueType = null;
            }
            if (this.entityXVlpId!= null) {
                String sourceEntityXVlpId;
                sourceEntityXVlpId = this.getEntityXVlpId();
                String copyEntityXVlpId = ((String) strategy.copy(LocatorUtils.property(locator, "entityXVlpId", sourceEntityXVlpId), sourceEntityXVlpId));
                copy.setEntityXVlpId(copyEntityXVlpId);
            } else {
                copy.entityXVlpId = null;
            }
            if (this.entityXVlpIdfieldname!= null) {
                String sourceEntityXVlpIdfieldname;
                sourceEntityXVlpIdfieldname = this.getEntityXVlpIdfieldname();
                String copyEntityXVlpIdfieldname = ((String) strategy.copy(LocatorUtils.property(locator, "entityXVlpIdfieldname", sourceEntityXVlpIdfieldname), sourceEntityXVlpIdfieldname));
                copy.setEntityXVlpIdfieldname(copyEntityXVlpIdfieldname);
            } else {
                copy.entityXVlpIdfieldname = null;
            }
            if (this.entityXVlpFieldname!= null) {
                String sourceEntityXVlpFieldname;
                sourceEntityXVlpFieldname = this.getEntityXVlpFieldname();
                String copyEntityXVlpFieldname = ((String) strategy.copy(LocatorUtils.property(locator, "entityXVlpFieldname", sourceEntityXVlpFieldname), sourceEntityXVlpFieldname));
                copy.setEntityXVlpFieldname(copyEntityXVlpFieldname);
            } else {
                copy.entityXVlpFieldname = null;
            }
            if (this.entityXVlpReferenceParamName!= null) {
                String sourceEntityXVlpReferenceParamName;
                sourceEntityXVlpReferenceParamName = this.getEntityXVlpReferenceParamName();
                String copyEntityXVlpReferenceParamName = ((String) strategy.copy(LocatorUtils.property(locator, "entityXVlpReferenceParamName", sourceEntityXVlpReferenceParamName), sourceEntityXVlpReferenceParamName));
                copy.setEntityXVlpReferenceParamName(copyEntityXVlpReferenceParamName);
            } else {
                copy.entityXVlpReferenceParamName = null;
            }
            if (this.cellInputType!= null) {
                String sourceCellInputType;
                sourceCellInputType = this.getCellInputType();
                String copyCellInputType = ((String) strategy.copy(LocatorUtils.property(locator, "cellInputType", sourceCellInputType), sourceCellInputType));
                copy.setCellInputType(copyCellInputType);
            } else {
                copy.cellInputType = null;
            }
            if (this.enabled!= null) {
                Boolean sourceEnabled;
                sourceEnabled = this.getEnabled();
                Boolean copyEnabled = ((Boolean) strategy.copy(LocatorUtils.property(locator, "enabled", sourceEnabled), sourceEnabled));
                copy.setEnabled(copyEnabled);
            } else {
                copy.enabled = null;
            }
            if (this.opaque!= null) {
                Boolean sourceOpaque;
                sourceOpaque = this.getOpaque();
                Boolean copyOpaque = ((Boolean) strategy.copy(LocatorUtils.property(locator, "opaque", sourceOpaque), sourceOpaque));
                copy.setOpaque(copyOpaque);
            } else {
                copy.opaque = null;
            }
            if (this.controllertype!= null) {
                String sourceControllertype;
                sourceControllertype = this.getControllertype();
                String copyControllertype = ((String) strategy.copy(LocatorUtils.property(locator, "controllertype", sourceControllertype), sourceControllertype));
                copy.setControllertype(copyControllertype);
            } else {
                copy.controllertype = null;
            }
            if (this.dynamicCellHeightsDefault!= null) {
                Boolean sourceDynamicCellHeightsDefault;
                sourceDynamicCellHeightsDefault = this.getDynamicCellHeightsDefault();
                Boolean copyDynamicCellHeightsDefault = ((Boolean) strategy.copy(LocatorUtils.property(locator, "dynamicCellHeightsDefault", sourceDynamicCellHeightsDefault), sourceDynamicCellHeightsDefault));
                copy.setDynamicCellHeightsDefault(copyDynamicCellHeightsDefault);
            } else {
                copy.dynamicCellHeightsDefault = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Matrix();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Matrix.Builder<_B> _other) {
        _other.layoutconstraints = this.layoutconstraints;
        _other.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.newCopyBuilder(_other));
        if (this.border == null) {
            _other.border = null;
        } else {
            _other.border = new ArrayList<Buildable>();
            for (JAXBElement<?> _item: this.border) {
                _other.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
            }
        }
        _other.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.newCopyBuilder(_other));
        _other.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.newCopyBuilder(_other));
        _other.strictSize = ((this.strictSize == null)?null:this.strictSize.newCopyBuilder(_other));
        _other.font = ((this.font == null)?null:this.font.newCopyBuilder(_other));
        _other.background = ((this.background == null)?null:this.background.newCopyBuilder(_other));
        _other.description = this.description;
        if (this.matrixColumn == null) {
            _other.matrixColumn = null;
        } else {
            _other.matrixColumn = new ArrayList<MatrixColumn.Builder<Matrix.Builder<_B>>>();
            for (MatrixColumn _item: this.matrixColumn) {
                _other.matrixColumn.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.newEnabled = ((this.newEnabled == null)?null:this.newEnabled.newCopyBuilder(_other));
        _other.editEnabled = ((this.editEnabled == null)?null:this.editEnabled.newCopyBuilder(_other));
        _other.deleteEnabled = ((this.deleteEnabled == null)?null:this.deleteEnabled.newCopyBuilder(_other));
        _other.cloneEnabled = ((this.cloneEnabled == null)?null:this.cloneEnabled.newCopyBuilder(_other));
        _other.name = this.name;
        _other.matrixPreferencesField = this.matrixPreferencesField;
        _other.entityX = this.entityX;
        _other.entityY = this.entityY;
        _other.entityMatrix = this.entityMatrix;
        _other.entityFieldMatrixParent = this.entityFieldMatrixParent;
        _other.entityFieldMatrixXRefField = this.entityFieldMatrixXRefField;
        _other.entityMatrixValueField = this.entityMatrixValueField;
        _other.entityMatrixNumberState = this.entityMatrixNumberState;
        _other.entityFieldCategorie = this.entityFieldCategorie;
        _other.entityFieldX = this.entityFieldX;
        _other.entityYParentField = this.entityYParentField;
        _other.entityFieldY = this.entityFieldY;
        _other.entityXSortingFields = this.entityXSortingFields;
        _other.entityYSortingFields = this.entityYSortingFields;
        _other.entityXHeader = this.entityXHeader;
        _other.entityYHeader = this.entityYHeader;
        _other.entityMatrixReferenceField = this.entityMatrixReferenceField;
        _other.entityMatrixValueType = this.entityMatrixValueType;
        _other.entityXVlpId = this.entityXVlpId;
        _other.entityXVlpIdfieldname = this.entityXVlpIdfieldname;
        _other.entityXVlpFieldname = this.entityXVlpFieldname;
        _other.entityXVlpReferenceParamName = this.entityXVlpReferenceParamName;
        _other.cellInputType = this.cellInputType;
        _other.enabled = this.enabled;
        _other.opaque = this.opaque;
        _other.controllertype = this.controllertype;
        _other.dynamicCellHeightsDefault = this.dynamicCellHeightsDefault;
    }

    public<_B >Matrix.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Matrix.Builder<_B>(_parentBuilder, this, true);
    }

    public Matrix.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Matrix.Builder<Void> builder() {
        return new Matrix.Builder<Void>(null, null, false);
    }

    public static<_B >Matrix.Builder<_B> copyOf(final Matrix _other) {
        final Matrix.Builder<_B> _newBuilder = new Matrix.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Matrix.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree layoutconstraintsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("layoutconstraints"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(layoutconstraintsPropertyTree!= null):((layoutconstraintsPropertyTree == null)||(!layoutconstraintsPropertyTree.isLeaf())))) {
            _other.layoutconstraints = this.layoutconstraints;
        }
        final PropertyTree clearBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("clearBorder"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(clearBorderPropertyTree!= null):((clearBorderPropertyTree == null)||(!clearBorderPropertyTree.isLeaf())))) {
            _other.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.newCopyBuilder(_other, clearBorderPropertyTree, _propertyTreeUse));
        }
        final PropertyTree borderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("border"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(borderPropertyTree!= null):((borderPropertyTree == null)||(!borderPropertyTree.isLeaf())))) {
            if (this.border == null) {
                _other.border = null;
            } else {
                _other.border = new ArrayList<Buildable>();
                for (JAXBElement<?> _item: this.border) {
                    _other.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                }
            }
        }
        final PropertyTree minimumSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minimumSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minimumSizePropertyTree!= null):((minimumSizePropertyTree == null)||(!minimumSizePropertyTree.isLeaf())))) {
            _other.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.newCopyBuilder(_other, minimumSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree preferredSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("preferredSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(preferredSizePropertyTree!= null):((preferredSizePropertyTree == null)||(!preferredSizePropertyTree.isLeaf())))) {
            _other.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.newCopyBuilder(_other, preferredSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree strictSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("strictSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(strictSizePropertyTree!= null):((strictSizePropertyTree == null)||(!strictSizePropertyTree.isLeaf())))) {
            _other.strictSize = ((this.strictSize == null)?null:this.strictSize.newCopyBuilder(_other, strictSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree fontPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("font"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fontPropertyTree!= null):((fontPropertyTree == null)||(!fontPropertyTree.isLeaf())))) {
            _other.font = ((this.font == null)?null:this.font.newCopyBuilder(_other, fontPropertyTree, _propertyTreeUse));
        }
        final PropertyTree backgroundPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("background"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(backgroundPropertyTree!= null):((backgroundPropertyTree == null)||(!backgroundPropertyTree.isLeaf())))) {
            _other.background = ((this.background == null)?null:this.background.newCopyBuilder(_other, backgroundPropertyTree, _propertyTreeUse));
        }
        final PropertyTree descriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("description"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(descriptionPropertyTree!= null):((descriptionPropertyTree == null)||(!descriptionPropertyTree.isLeaf())))) {
            _other.description = this.description;
        }
        final PropertyTree matrixColumnPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("matrixColumn"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(matrixColumnPropertyTree!= null):((matrixColumnPropertyTree == null)||(!matrixColumnPropertyTree.isLeaf())))) {
            if (this.matrixColumn == null) {
                _other.matrixColumn = null;
            } else {
                _other.matrixColumn = new ArrayList<MatrixColumn.Builder<Matrix.Builder<_B>>>();
                for (MatrixColumn _item: this.matrixColumn) {
                    _other.matrixColumn.add(((_item == null)?null:_item.newCopyBuilder(_other, matrixColumnPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree newEnabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("newEnabled"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(newEnabledPropertyTree!= null):((newEnabledPropertyTree == null)||(!newEnabledPropertyTree.isLeaf())))) {
            _other.newEnabled = ((this.newEnabled == null)?null:this.newEnabled.newCopyBuilder(_other, newEnabledPropertyTree, _propertyTreeUse));
        }
        final PropertyTree editEnabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("editEnabled"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(editEnabledPropertyTree!= null):((editEnabledPropertyTree == null)||(!editEnabledPropertyTree.isLeaf())))) {
            _other.editEnabled = ((this.editEnabled == null)?null:this.editEnabled.newCopyBuilder(_other, editEnabledPropertyTree, _propertyTreeUse));
        }
        final PropertyTree deleteEnabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("deleteEnabled"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(deleteEnabledPropertyTree!= null):((deleteEnabledPropertyTree == null)||(!deleteEnabledPropertyTree.isLeaf())))) {
            _other.deleteEnabled = ((this.deleteEnabled == null)?null:this.deleteEnabled.newCopyBuilder(_other, deleteEnabledPropertyTree, _propertyTreeUse));
        }
        final PropertyTree cloneEnabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("cloneEnabled"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(cloneEnabledPropertyTree!= null):((cloneEnabledPropertyTree == null)||(!cloneEnabledPropertyTree.isLeaf())))) {
            _other.cloneEnabled = ((this.cloneEnabled == null)?null:this.cloneEnabled.newCopyBuilder(_other, cloneEnabledPropertyTree, _propertyTreeUse));
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree matrixPreferencesFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("matrixPreferencesField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(matrixPreferencesFieldPropertyTree!= null):((matrixPreferencesFieldPropertyTree == null)||(!matrixPreferencesFieldPropertyTree.isLeaf())))) {
            _other.matrixPreferencesField = this.matrixPreferencesField;
        }
        final PropertyTree entityXPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityX"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXPropertyTree!= null):((entityXPropertyTree == null)||(!entityXPropertyTree.isLeaf())))) {
            _other.entityX = this.entityX;
        }
        final PropertyTree entityYPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityY"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityYPropertyTree!= null):((entityYPropertyTree == null)||(!entityYPropertyTree.isLeaf())))) {
            _other.entityY = this.entityY;
        }
        final PropertyTree entityMatrixPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMatrix"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMatrixPropertyTree!= null):((entityMatrixPropertyTree == null)||(!entityMatrixPropertyTree.isLeaf())))) {
            _other.entityMatrix = this.entityMatrix;
        }
        final PropertyTree entityFieldMatrixParentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityFieldMatrixParent"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityFieldMatrixParentPropertyTree!= null):((entityFieldMatrixParentPropertyTree == null)||(!entityFieldMatrixParentPropertyTree.isLeaf())))) {
            _other.entityFieldMatrixParent = this.entityFieldMatrixParent;
        }
        final PropertyTree entityFieldMatrixXRefFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityFieldMatrixXRefField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityFieldMatrixXRefFieldPropertyTree!= null):((entityFieldMatrixXRefFieldPropertyTree == null)||(!entityFieldMatrixXRefFieldPropertyTree.isLeaf())))) {
            _other.entityFieldMatrixXRefField = this.entityFieldMatrixXRefField;
        }
        final PropertyTree entityMatrixValueFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMatrixValueField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMatrixValueFieldPropertyTree!= null):((entityMatrixValueFieldPropertyTree == null)||(!entityMatrixValueFieldPropertyTree.isLeaf())))) {
            _other.entityMatrixValueField = this.entityMatrixValueField;
        }
        final PropertyTree entityMatrixNumberStatePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMatrixNumberState"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMatrixNumberStatePropertyTree!= null):((entityMatrixNumberStatePropertyTree == null)||(!entityMatrixNumberStatePropertyTree.isLeaf())))) {
            _other.entityMatrixNumberState = this.entityMatrixNumberState;
        }
        final PropertyTree entityFieldCategoriePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityFieldCategorie"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityFieldCategoriePropertyTree!= null):((entityFieldCategoriePropertyTree == null)||(!entityFieldCategoriePropertyTree.isLeaf())))) {
            _other.entityFieldCategorie = this.entityFieldCategorie;
        }
        final PropertyTree entityFieldXPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityFieldX"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityFieldXPropertyTree!= null):((entityFieldXPropertyTree == null)||(!entityFieldXPropertyTree.isLeaf())))) {
            _other.entityFieldX = this.entityFieldX;
        }
        final PropertyTree entityYParentFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityYParentField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityYParentFieldPropertyTree!= null):((entityYParentFieldPropertyTree == null)||(!entityYParentFieldPropertyTree.isLeaf())))) {
            _other.entityYParentField = this.entityYParentField;
        }
        final PropertyTree entityFieldYPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityFieldY"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityFieldYPropertyTree!= null):((entityFieldYPropertyTree == null)||(!entityFieldYPropertyTree.isLeaf())))) {
            _other.entityFieldY = this.entityFieldY;
        }
        final PropertyTree entityXSortingFieldsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityXSortingFields"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXSortingFieldsPropertyTree!= null):((entityXSortingFieldsPropertyTree == null)||(!entityXSortingFieldsPropertyTree.isLeaf())))) {
            _other.entityXSortingFields = this.entityXSortingFields;
        }
        final PropertyTree entityYSortingFieldsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityYSortingFields"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityYSortingFieldsPropertyTree!= null):((entityYSortingFieldsPropertyTree == null)||(!entityYSortingFieldsPropertyTree.isLeaf())))) {
            _other.entityYSortingFields = this.entityYSortingFields;
        }
        final PropertyTree entityXHeaderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityXHeader"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXHeaderPropertyTree!= null):((entityXHeaderPropertyTree == null)||(!entityXHeaderPropertyTree.isLeaf())))) {
            _other.entityXHeader = this.entityXHeader;
        }
        final PropertyTree entityYHeaderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityYHeader"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityYHeaderPropertyTree!= null):((entityYHeaderPropertyTree == null)||(!entityYHeaderPropertyTree.isLeaf())))) {
            _other.entityYHeader = this.entityYHeader;
        }
        final PropertyTree entityMatrixReferenceFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMatrixReferenceField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMatrixReferenceFieldPropertyTree!= null):((entityMatrixReferenceFieldPropertyTree == null)||(!entityMatrixReferenceFieldPropertyTree.isLeaf())))) {
            _other.entityMatrixReferenceField = this.entityMatrixReferenceField;
        }
        final PropertyTree entityMatrixValueTypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMatrixValueType"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMatrixValueTypePropertyTree!= null):((entityMatrixValueTypePropertyTree == null)||(!entityMatrixValueTypePropertyTree.isLeaf())))) {
            _other.entityMatrixValueType = this.entityMatrixValueType;
        }
        final PropertyTree entityXVlpIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityXVlpId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXVlpIdPropertyTree!= null):((entityXVlpIdPropertyTree == null)||(!entityXVlpIdPropertyTree.isLeaf())))) {
            _other.entityXVlpId = this.entityXVlpId;
        }
        final PropertyTree entityXVlpIdfieldnamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityXVlpIdfieldname"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXVlpIdfieldnamePropertyTree!= null):((entityXVlpIdfieldnamePropertyTree == null)||(!entityXVlpIdfieldnamePropertyTree.isLeaf())))) {
            _other.entityXVlpIdfieldname = this.entityXVlpIdfieldname;
        }
        final PropertyTree entityXVlpFieldnamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityXVlpFieldname"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXVlpFieldnamePropertyTree!= null):((entityXVlpFieldnamePropertyTree == null)||(!entityXVlpFieldnamePropertyTree.isLeaf())))) {
            _other.entityXVlpFieldname = this.entityXVlpFieldname;
        }
        final PropertyTree entityXVlpReferenceParamNamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityXVlpReferenceParamName"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXVlpReferenceParamNamePropertyTree!= null):((entityXVlpReferenceParamNamePropertyTree == null)||(!entityXVlpReferenceParamNamePropertyTree.isLeaf())))) {
            _other.entityXVlpReferenceParamName = this.entityXVlpReferenceParamName;
        }
        final PropertyTree cellInputTypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("cellInputType"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(cellInputTypePropertyTree!= null):((cellInputTypePropertyTree == null)||(!cellInputTypePropertyTree.isLeaf())))) {
            _other.cellInputType = this.cellInputType;
        }
        final PropertyTree enabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("enabled"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(enabledPropertyTree!= null):((enabledPropertyTree == null)||(!enabledPropertyTree.isLeaf())))) {
            _other.enabled = this.enabled;
        }
        final PropertyTree opaquePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("opaque"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(opaquePropertyTree!= null):((opaquePropertyTree == null)||(!opaquePropertyTree.isLeaf())))) {
            _other.opaque = this.opaque;
        }
        final PropertyTree controllertypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("controllertype"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(controllertypePropertyTree!= null):((controllertypePropertyTree == null)||(!controllertypePropertyTree.isLeaf())))) {
            _other.controllertype = this.controllertype;
        }
        final PropertyTree dynamicCellHeightsDefaultPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dynamicCellHeightsDefault"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dynamicCellHeightsDefaultPropertyTree!= null):((dynamicCellHeightsDefaultPropertyTree == null)||(!dynamicCellHeightsDefaultPropertyTree.isLeaf())))) {
            _other.dynamicCellHeightsDefault = this.dynamicCellHeightsDefault;
        }
    }

    public<_B >Matrix.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Matrix.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Matrix.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Matrix.Builder<_B> copyOf(final Matrix _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Matrix.Builder<_B> _newBuilder = new Matrix.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Matrix.Builder<Void> copyExcept(final Matrix _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Matrix.Builder<Void> copyOnly(final Matrix _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Matrix _storedValue;
        private JAXBElement<?> layoutconstraints;
        private ClearBorder.Builder<Matrix.Builder<_B>> clearBorder;
        private List<Buildable> border;
        private MinimumSize.Builder<Matrix.Builder<_B>> minimumSize;
        private PreferredSize.Builder<Matrix.Builder<_B>> preferredSize;
        private StrictSize.Builder<Matrix.Builder<_B>> strictSize;
        private Font.Builder<Matrix.Builder<_B>> font;
        private Background.Builder<Matrix.Builder<_B>> background;
        private String description;
        private List<MatrixColumn.Builder<Matrix.Builder<_B>>> matrixColumn;
        private NewEnabled.Builder<Matrix.Builder<_B>> newEnabled;
        private EditEnabled.Builder<Matrix.Builder<_B>> editEnabled;
        private DeleteEnabled.Builder<Matrix.Builder<_B>> deleteEnabled;
        private CloneEnabled.Builder<Matrix.Builder<_B>> cloneEnabled;
        private String name;
        private String matrixPreferencesField;
        private String entityX;
        private String entityY;
        private String entityMatrix;
        private String entityFieldMatrixParent;
        private String entityFieldMatrixXRefField;
        private String entityMatrixValueField;
        private String entityMatrixNumberState;
        private String entityFieldCategorie;
        private String entityFieldX;
        private String entityYParentField;
        private String entityFieldY;
        private String entityXSortingFields;
        private String entityYSortingFields;
        private String entityXHeader;
        private String entityYHeader;
        private String entityMatrixReferenceField;
        private String entityMatrixValueType;
        private String entityXVlpId;
        private String entityXVlpIdfieldname;
        private String entityXVlpFieldname;
        private String entityXVlpReferenceParamName;
        private String cellInputType;
        private Boolean enabled;
        private Boolean opaque;
        private String controllertype;
        private Boolean dynamicCellHeightsDefault;

        public Builder(final _B _parentBuilder, final Matrix _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.layoutconstraints = _other.layoutconstraints;
                    this.clearBorder = ((_other.clearBorder == null)?null:_other.clearBorder.newCopyBuilder(this));
                    if (_other.border == null) {
                        this.border = null;
                    } else {
                        this.border = new ArrayList<Buildable>();
                        for (JAXBElement<?> _item: _other.border) {
                            this.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                        }
                    }
                    this.minimumSize = ((_other.minimumSize == null)?null:_other.minimumSize.newCopyBuilder(this));
                    this.preferredSize = ((_other.preferredSize == null)?null:_other.preferredSize.newCopyBuilder(this));
                    this.strictSize = ((_other.strictSize == null)?null:_other.strictSize.newCopyBuilder(this));
                    this.font = ((_other.font == null)?null:_other.font.newCopyBuilder(this));
                    this.background = ((_other.background == null)?null:_other.background.newCopyBuilder(this));
                    this.description = _other.description;
                    if (_other.matrixColumn == null) {
                        this.matrixColumn = null;
                    } else {
                        this.matrixColumn = new ArrayList<MatrixColumn.Builder<Matrix.Builder<_B>>>();
                        for (MatrixColumn _item: _other.matrixColumn) {
                            this.matrixColumn.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.newEnabled = ((_other.newEnabled == null)?null:_other.newEnabled.newCopyBuilder(this));
                    this.editEnabled = ((_other.editEnabled == null)?null:_other.editEnabled.newCopyBuilder(this));
                    this.deleteEnabled = ((_other.deleteEnabled == null)?null:_other.deleteEnabled.newCopyBuilder(this));
                    this.cloneEnabled = ((_other.cloneEnabled == null)?null:_other.cloneEnabled.newCopyBuilder(this));
                    this.name = _other.name;
                    this.matrixPreferencesField = _other.matrixPreferencesField;
                    this.entityX = _other.entityX;
                    this.entityY = _other.entityY;
                    this.entityMatrix = _other.entityMatrix;
                    this.entityFieldMatrixParent = _other.entityFieldMatrixParent;
                    this.entityFieldMatrixXRefField = _other.entityFieldMatrixXRefField;
                    this.entityMatrixValueField = _other.entityMatrixValueField;
                    this.entityMatrixNumberState = _other.entityMatrixNumberState;
                    this.entityFieldCategorie = _other.entityFieldCategorie;
                    this.entityFieldX = _other.entityFieldX;
                    this.entityYParentField = _other.entityYParentField;
                    this.entityFieldY = _other.entityFieldY;
                    this.entityXSortingFields = _other.entityXSortingFields;
                    this.entityYSortingFields = _other.entityYSortingFields;
                    this.entityXHeader = _other.entityXHeader;
                    this.entityYHeader = _other.entityYHeader;
                    this.entityMatrixReferenceField = _other.entityMatrixReferenceField;
                    this.entityMatrixValueType = _other.entityMatrixValueType;
                    this.entityXVlpId = _other.entityXVlpId;
                    this.entityXVlpIdfieldname = _other.entityXVlpIdfieldname;
                    this.entityXVlpFieldname = _other.entityXVlpFieldname;
                    this.entityXVlpReferenceParamName = _other.entityXVlpReferenceParamName;
                    this.cellInputType = _other.cellInputType;
                    this.enabled = _other.enabled;
                    this.opaque = _other.opaque;
                    this.controllertype = _other.controllertype;
                    this.dynamicCellHeightsDefault = _other.dynamicCellHeightsDefault;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Matrix _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree layoutconstraintsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("layoutconstraints"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(layoutconstraintsPropertyTree!= null):((layoutconstraintsPropertyTree == null)||(!layoutconstraintsPropertyTree.isLeaf())))) {
                        this.layoutconstraints = _other.layoutconstraints;
                    }
                    final PropertyTree clearBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("clearBorder"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(clearBorderPropertyTree!= null):((clearBorderPropertyTree == null)||(!clearBorderPropertyTree.isLeaf())))) {
                        this.clearBorder = ((_other.clearBorder == null)?null:_other.clearBorder.newCopyBuilder(this, clearBorderPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree borderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("border"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(borderPropertyTree!= null):((borderPropertyTree == null)||(!borderPropertyTree.isLeaf())))) {
                        if (_other.border == null) {
                            this.border = null;
                        } else {
                            this.border = new ArrayList<Buildable>();
                            for (JAXBElement<?> _item: _other.border) {
                                this.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                            }
                        }
                    }
                    final PropertyTree minimumSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minimumSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minimumSizePropertyTree!= null):((minimumSizePropertyTree == null)||(!minimumSizePropertyTree.isLeaf())))) {
                        this.minimumSize = ((_other.minimumSize == null)?null:_other.minimumSize.newCopyBuilder(this, minimumSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree preferredSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("preferredSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(preferredSizePropertyTree!= null):((preferredSizePropertyTree == null)||(!preferredSizePropertyTree.isLeaf())))) {
                        this.preferredSize = ((_other.preferredSize == null)?null:_other.preferredSize.newCopyBuilder(this, preferredSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree strictSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("strictSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(strictSizePropertyTree!= null):((strictSizePropertyTree == null)||(!strictSizePropertyTree.isLeaf())))) {
                        this.strictSize = ((_other.strictSize == null)?null:_other.strictSize.newCopyBuilder(this, strictSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree fontPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("font"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fontPropertyTree!= null):((fontPropertyTree == null)||(!fontPropertyTree.isLeaf())))) {
                        this.font = ((_other.font == null)?null:_other.font.newCopyBuilder(this, fontPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree backgroundPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("background"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(backgroundPropertyTree!= null):((backgroundPropertyTree == null)||(!backgroundPropertyTree.isLeaf())))) {
                        this.background = ((_other.background == null)?null:_other.background.newCopyBuilder(this, backgroundPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree descriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("description"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(descriptionPropertyTree!= null):((descriptionPropertyTree == null)||(!descriptionPropertyTree.isLeaf())))) {
                        this.description = _other.description;
                    }
                    final PropertyTree matrixColumnPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("matrixColumn"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(matrixColumnPropertyTree!= null):((matrixColumnPropertyTree == null)||(!matrixColumnPropertyTree.isLeaf())))) {
                        if (_other.matrixColumn == null) {
                            this.matrixColumn = null;
                        } else {
                            this.matrixColumn = new ArrayList<MatrixColumn.Builder<Matrix.Builder<_B>>>();
                            for (MatrixColumn _item: _other.matrixColumn) {
                                this.matrixColumn.add(((_item == null)?null:_item.newCopyBuilder(this, matrixColumnPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree newEnabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("newEnabled"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(newEnabledPropertyTree!= null):((newEnabledPropertyTree == null)||(!newEnabledPropertyTree.isLeaf())))) {
                        this.newEnabled = ((_other.newEnabled == null)?null:_other.newEnabled.newCopyBuilder(this, newEnabledPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree editEnabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("editEnabled"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(editEnabledPropertyTree!= null):((editEnabledPropertyTree == null)||(!editEnabledPropertyTree.isLeaf())))) {
                        this.editEnabled = ((_other.editEnabled == null)?null:_other.editEnabled.newCopyBuilder(this, editEnabledPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree deleteEnabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("deleteEnabled"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(deleteEnabledPropertyTree!= null):((deleteEnabledPropertyTree == null)||(!deleteEnabledPropertyTree.isLeaf())))) {
                        this.deleteEnabled = ((_other.deleteEnabled == null)?null:_other.deleteEnabled.newCopyBuilder(this, deleteEnabledPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree cloneEnabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("cloneEnabled"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(cloneEnabledPropertyTree!= null):((cloneEnabledPropertyTree == null)||(!cloneEnabledPropertyTree.isLeaf())))) {
                        this.cloneEnabled = ((_other.cloneEnabled == null)?null:_other.cloneEnabled.newCopyBuilder(this, cloneEnabledPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree matrixPreferencesFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("matrixPreferencesField"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(matrixPreferencesFieldPropertyTree!= null):((matrixPreferencesFieldPropertyTree == null)||(!matrixPreferencesFieldPropertyTree.isLeaf())))) {
                        this.matrixPreferencesField = _other.matrixPreferencesField;
                    }
                    final PropertyTree entityXPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityX"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXPropertyTree!= null):((entityXPropertyTree == null)||(!entityXPropertyTree.isLeaf())))) {
                        this.entityX = _other.entityX;
                    }
                    final PropertyTree entityYPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityY"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityYPropertyTree!= null):((entityYPropertyTree == null)||(!entityYPropertyTree.isLeaf())))) {
                        this.entityY = _other.entityY;
                    }
                    final PropertyTree entityMatrixPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMatrix"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMatrixPropertyTree!= null):((entityMatrixPropertyTree == null)||(!entityMatrixPropertyTree.isLeaf())))) {
                        this.entityMatrix = _other.entityMatrix;
                    }
                    final PropertyTree entityFieldMatrixParentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityFieldMatrixParent"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityFieldMatrixParentPropertyTree!= null):((entityFieldMatrixParentPropertyTree == null)||(!entityFieldMatrixParentPropertyTree.isLeaf())))) {
                        this.entityFieldMatrixParent = _other.entityFieldMatrixParent;
                    }
                    final PropertyTree entityFieldMatrixXRefFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityFieldMatrixXRefField"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityFieldMatrixXRefFieldPropertyTree!= null):((entityFieldMatrixXRefFieldPropertyTree == null)||(!entityFieldMatrixXRefFieldPropertyTree.isLeaf())))) {
                        this.entityFieldMatrixXRefField = _other.entityFieldMatrixXRefField;
                    }
                    final PropertyTree entityMatrixValueFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMatrixValueField"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMatrixValueFieldPropertyTree!= null):((entityMatrixValueFieldPropertyTree == null)||(!entityMatrixValueFieldPropertyTree.isLeaf())))) {
                        this.entityMatrixValueField = _other.entityMatrixValueField;
                    }
                    final PropertyTree entityMatrixNumberStatePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMatrixNumberState"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMatrixNumberStatePropertyTree!= null):((entityMatrixNumberStatePropertyTree == null)||(!entityMatrixNumberStatePropertyTree.isLeaf())))) {
                        this.entityMatrixNumberState = _other.entityMatrixNumberState;
                    }
                    final PropertyTree entityFieldCategoriePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityFieldCategorie"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityFieldCategoriePropertyTree!= null):((entityFieldCategoriePropertyTree == null)||(!entityFieldCategoriePropertyTree.isLeaf())))) {
                        this.entityFieldCategorie = _other.entityFieldCategorie;
                    }
                    final PropertyTree entityFieldXPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityFieldX"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityFieldXPropertyTree!= null):((entityFieldXPropertyTree == null)||(!entityFieldXPropertyTree.isLeaf())))) {
                        this.entityFieldX = _other.entityFieldX;
                    }
                    final PropertyTree entityYParentFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityYParentField"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityYParentFieldPropertyTree!= null):((entityYParentFieldPropertyTree == null)||(!entityYParentFieldPropertyTree.isLeaf())))) {
                        this.entityYParentField = _other.entityYParentField;
                    }
                    final PropertyTree entityFieldYPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityFieldY"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityFieldYPropertyTree!= null):((entityFieldYPropertyTree == null)||(!entityFieldYPropertyTree.isLeaf())))) {
                        this.entityFieldY = _other.entityFieldY;
                    }
                    final PropertyTree entityXSortingFieldsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityXSortingFields"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXSortingFieldsPropertyTree!= null):((entityXSortingFieldsPropertyTree == null)||(!entityXSortingFieldsPropertyTree.isLeaf())))) {
                        this.entityXSortingFields = _other.entityXSortingFields;
                    }
                    final PropertyTree entityYSortingFieldsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityYSortingFields"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityYSortingFieldsPropertyTree!= null):((entityYSortingFieldsPropertyTree == null)||(!entityYSortingFieldsPropertyTree.isLeaf())))) {
                        this.entityYSortingFields = _other.entityYSortingFields;
                    }
                    final PropertyTree entityXHeaderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityXHeader"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXHeaderPropertyTree!= null):((entityXHeaderPropertyTree == null)||(!entityXHeaderPropertyTree.isLeaf())))) {
                        this.entityXHeader = _other.entityXHeader;
                    }
                    final PropertyTree entityYHeaderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityYHeader"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityYHeaderPropertyTree!= null):((entityYHeaderPropertyTree == null)||(!entityYHeaderPropertyTree.isLeaf())))) {
                        this.entityYHeader = _other.entityYHeader;
                    }
                    final PropertyTree entityMatrixReferenceFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMatrixReferenceField"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMatrixReferenceFieldPropertyTree!= null):((entityMatrixReferenceFieldPropertyTree == null)||(!entityMatrixReferenceFieldPropertyTree.isLeaf())))) {
                        this.entityMatrixReferenceField = _other.entityMatrixReferenceField;
                    }
                    final PropertyTree entityMatrixValueTypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMatrixValueType"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMatrixValueTypePropertyTree!= null):((entityMatrixValueTypePropertyTree == null)||(!entityMatrixValueTypePropertyTree.isLeaf())))) {
                        this.entityMatrixValueType = _other.entityMatrixValueType;
                    }
                    final PropertyTree entityXVlpIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityXVlpId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXVlpIdPropertyTree!= null):((entityXVlpIdPropertyTree == null)||(!entityXVlpIdPropertyTree.isLeaf())))) {
                        this.entityXVlpId = _other.entityXVlpId;
                    }
                    final PropertyTree entityXVlpIdfieldnamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityXVlpIdfieldname"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXVlpIdfieldnamePropertyTree!= null):((entityXVlpIdfieldnamePropertyTree == null)||(!entityXVlpIdfieldnamePropertyTree.isLeaf())))) {
                        this.entityXVlpIdfieldname = _other.entityXVlpIdfieldname;
                    }
                    final PropertyTree entityXVlpFieldnamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityXVlpFieldname"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXVlpFieldnamePropertyTree!= null):((entityXVlpFieldnamePropertyTree == null)||(!entityXVlpFieldnamePropertyTree.isLeaf())))) {
                        this.entityXVlpFieldname = _other.entityXVlpFieldname;
                    }
                    final PropertyTree entityXVlpReferenceParamNamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityXVlpReferenceParamName"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXVlpReferenceParamNamePropertyTree!= null):((entityXVlpReferenceParamNamePropertyTree == null)||(!entityXVlpReferenceParamNamePropertyTree.isLeaf())))) {
                        this.entityXVlpReferenceParamName = _other.entityXVlpReferenceParamName;
                    }
                    final PropertyTree cellInputTypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("cellInputType"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(cellInputTypePropertyTree!= null):((cellInputTypePropertyTree == null)||(!cellInputTypePropertyTree.isLeaf())))) {
                        this.cellInputType = _other.cellInputType;
                    }
                    final PropertyTree enabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("enabled"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(enabledPropertyTree!= null):((enabledPropertyTree == null)||(!enabledPropertyTree.isLeaf())))) {
                        this.enabled = _other.enabled;
                    }
                    final PropertyTree opaquePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("opaque"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(opaquePropertyTree!= null):((opaquePropertyTree == null)||(!opaquePropertyTree.isLeaf())))) {
                        this.opaque = _other.opaque;
                    }
                    final PropertyTree controllertypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("controllertype"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(controllertypePropertyTree!= null):((controllertypePropertyTree == null)||(!controllertypePropertyTree.isLeaf())))) {
                        this.controllertype = _other.controllertype;
                    }
                    final PropertyTree dynamicCellHeightsDefaultPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dynamicCellHeightsDefault"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dynamicCellHeightsDefaultPropertyTree!= null):((dynamicCellHeightsDefaultPropertyTree == null)||(!dynamicCellHeightsDefaultPropertyTree.isLeaf())))) {
                        this.dynamicCellHeightsDefault = _other.dynamicCellHeightsDefault;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Matrix >_P init(final _P _product) {
            _product.layoutconstraints = this.layoutconstraints;
            _product.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.build());
            if (this.border!= null) {
                final List<JAXBElement<?>> border = new ArrayList<JAXBElement<?>>(this.border.size());
                for (Buildable _item: this.border) {
                    border.add(((JAXBElement<?> ) _item.build()));
                }
                _product.border = border;
            }
            _product.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.build());
            _product.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.build());
            _product.strictSize = ((this.strictSize == null)?null:this.strictSize.build());
            _product.font = ((this.font == null)?null:this.font.build());
            _product.background = ((this.background == null)?null:this.background.build());
            _product.description = this.description;
            if (this.matrixColumn!= null) {
                final List<MatrixColumn> matrixColumn = new ArrayList<MatrixColumn>(this.matrixColumn.size());
                for (MatrixColumn.Builder<Matrix.Builder<_B>> _item: this.matrixColumn) {
                    matrixColumn.add(_item.build());
                }
                _product.matrixColumn = matrixColumn;
            }
            _product.newEnabled = ((this.newEnabled == null)?null:this.newEnabled.build());
            _product.editEnabled = ((this.editEnabled == null)?null:this.editEnabled.build());
            _product.deleteEnabled = ((this.deleteEnabled == null)?null:this.deleteEnabled.build());
            _product.cloneEnabled = ((this.cloneEnabled == null)?null:this.cloneEnabled.build());
            _product.name = this.name;
            _product.matrixPreferencesField = this.matrixPreferencesField;
            _product.entityX = this.entityX;
            _product.entityY = this.entityY;
            _product.entityMatrix = this.entityMatrix;
            _product.entityFieldMatrixParent = this.entityFieldMatrixParent;
            _product.entityFieldMatrixXRefField = this.entityFieldMatrixXRefField;
            _product.entityMatrixValueField = this.entityMatrixValueField;
            _product.entityMatrixNumberState = this.entityMatrixNumberState;
            _product.entityFieldCategorie = this.entityFieldCategorie;
            _product.entityFieldX = this.entityFieldX;
            _product.entityYParentField = this.entityYParentField;
            _product.entityFieldY = this.entityFieldY;
            _product.entityXSortingFields = this.entityXSortingFields;
            _product.entityYSortingFields = this.entityYSortingFields;
            _product.entityXHeader = this.entityXHeader;
            _product.entityYHeader = this.entityYHeader;
            _product.entityMatrixReferenceField = this.entityMatrixReferenceField;
            _product.entityMatrixValueType = this.entityMatrixValueType;
            _product.entityXVlpId = this.entityXVlpId;
            _product.entityXVlpIdfieldname = this.entityXVlpIdfieldname;
            _product.entityXVlpFieldname = this.entityXVlpFieldname;
            _product.entityXVlpReferenceParamName = this.entityXVlpReferenceParamName;
            _product.cellInputType = this.cellInputType;
            _product.enabled = this.enabled;
            _product.opaque = this.opaque;
            _product.controllertype = this.controllertype;
            _product.dynamicCellHeightsDefault = this.dynamicCellHeightsDefault;
            return _product;
        }

        /**
         * Sets the new value of "layoutconstraints" (any previous value will be replaced)
         * 
         * @param layoutconstraints
         *     New value of the "layoutconstraints" property.
         */
        public Matrix.Builder<_B> withLayoutconstraints(final JAXBElement<?> layoutconstraints) {
            this.layoutconstraints = layoutconstraints;
            return this;
        }

        /**
         * Sets the new value of "clearBorder" (any previous value will be replaced)
         * 
         * @param clearBorder
         *     New value of the "clearBorder" property.
         */
        public Matrix.Builder<_B> withClearBorder(final ClearBorder clearBorder) {
            this.clearBorder = ((clearBorder == null)?null:new ClearBorder.Builder<Matrix.Builder<_B>>(this, clearBorder, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "clearBorder" property.
         * Use {@link org.nuclos.schema.layout.layoutml.ClearBorder.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "clearBorder" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.ClearBorder.Builder#end()} to return to the current builder.
         */
        public ClearBorder.Builder<? extends Matrix.Builder<_B>> withClearBorder() {
            if (this.clearBorder!= null) {
                return this.clearBorder;
            }
            return this.clearBorder = new ClearBorder.Builder<Matrix.Builder<_B>>(this, null, false);
        }

        /**
         * Adds the given items to the value of "border"
         * 
         * @param border
         *     Items to add to the value of the "border" property
         */
        public Matrix.Builder<_B> addBorder(final Iterable<? extends JAXBElement<?>> border) {
            if (border!= null) {
                if (this.border == null) {
                    this.border = new ArrayList<Buildable>();
                }
                for (JAXBElement<?> _item: border) {
                    this.border.add(new Buildable.PrimitiveBuildable(_item));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "border" (any previous value will be replaced)
         * 
         * @param border
         *     New value of the "border" property.
         */
        public Matrix.Builder<_B> withBorder(final Iterable<? extends JAXBElement<?>> border) {
            if (this.border!= null) {
                this.border.clear();
            }
            return addBorder(border);
        }

        /**
         * Adds the given items to the value of "border"
         * 
         * @param border
         *     Items to add to the value of the "border" property
         */
        public Matrix.Builder<_B> addBorder(JAXBElement<?> ... border) {
            addBorder(Arrays.asList(border));
            return this;
        }

        /**
         * Sets the new value of "border" (any previous value will be replaced)
         * 
         * @param border
         *     New value of the "border" property.
         */
        public Matrix.Builder<_B> withBorder(JAXBElement<?> ... border) {
            withBorder(Arrays.asList(border));
            return this;
        }

        /**
         * Sets the new value of "minimumSize" (any previous value will be replaced)
         * 
         * @param minimumSize
         *     New value of the "minimumSize" property.
         */
        public Matrix.Builder<_B> withMinimumSize(final MinimumSize minimumSize) {
            this.minimumSize = ((minimumSize == null)?null:new MinimumSize.Builder<Matrix.Builder<_B>>(this, minimumSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "minimumSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.MinimumSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "minimumSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.MinimumSize.Builder#end()} to return to the current builder.
         */
        public MinimumSize.Builder<? extends Matrix.Builder<_B>> withMinimumSize() {
            if (this.minimumSize!= null) {
                return this.minimumSize;
            }
            return this.minimumSize = new MinimumSize.Builder<Matrix.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "preferredSize" (any previous value will be replaced)
         * 
         * @param preferredSize
         *     New value of the "preferredSize" property.
         */
        public Matrix.Builder<_B> withPreferredSize(final PreferredSize preferredSize) {
            this.preferredSize = ((preferredSize == null)?null:new PreferredSize.Builder<Matrix.Builder<_B>>(this, preferredSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "preferredSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.PreferredSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "preferredSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.PreferredSize.Builder#end()} to return to the current builder.
         */
        public PreferredSize.Builder<? extends Matrix.Builder<_B>> withPreferredSize() {
            if (this.preferredSize!= null) {
                return this.preferredSize;
            }
            return this.preferredSize = new PreferredSize.Builder<Matrix.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "strictSize" (any previous value will be replaced)
         * 
         * @param strictSize
         *     New value of the "strictSize" property.
         */
        public Matrix.Builder<_B> withStrictSize(final StrictSize strictSize) {
            this.strictSize = ((strictSize == null)?null:new StrictSize.Builder<Matrix.Builder<_B>>(this, strictSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "strictSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.StrictSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "strictSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.StrictSize.Builder#end()} to return to the current builder.
         */
        public StrictSize.Builder<? extends Matrix.Builder<_B>> withStrictSize() {
            if (this.strictSize!= null) {
                return this.strictSize;
            }
            return this.strictSize = new StrictSize.Builder<Matrix.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "font" (any previous value will be replaced)
         * 
         * @param font
         *     New value of the "font" property.
         */
        public Matrix.Builder<_B> withFont(final Font font) {
            this.font = ((font == null)?null:new Font.Builder<Matrix.Builder<_B>>(this, font, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "font" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Font.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "font" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Font.Builder#end()} to return to the current builder.
         */
        public Font.Builder<? extends Matrix.Builder<_B>> withFont() {
            if (this.font!= null) {
                return this.font;
            }
            return this.font = new Font.Builder<Matrix.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "background" (any previous value will be replaced)
         * 
         * @param background
         *     New value of the "background" property.
         */
        public Matrix.Builder<_B> withBackground(final Background background) {
            this.background = ((background == null)?null:new Background.Builder<Matrix.Builder<_B>>(this, background, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "background" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Background.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "background" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Background.Builder#end()} to return to the current builder.
         */
        public Background.Builder<? extends Matrix.Builder<_B>> withBackground() {
            if (this.background!= null) {
                return this.background;
            }
            return this.background = new Background.Builder<Matrix.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "description" (any previous value will be replaced)
         * 
         * @param description
         *     New value of the "description" property.
         */
        public Matrix.Builder<_B> withDescription(final String description) {
            this.description = description;
            return this;
        }

        /**
         * Adds the given items to the value of "matrixColumn"
         * 
         * @param matrixColumn
         *     Items to add to the value of the "matrixColumn" property
         */
        public Matrix.Builder<_B> addMatrixColumn(final Iterable<? extends MatrixColumn> matrixColumn) {
            if (matrixColumn!= null) {
                if (this.matrixColumn == null) {
                    this.matrixColumn = new ArrayList<MatrixColumn.Builder<Matrix.Builder<_B>>>();
                }
                for (MatrixColumn _item: matrixColumn) {
                    this.matrixColumn.add(new MatrixColumn.Builder<Matrix.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "matrixColumn" (any previous value will be replaced)
         * 
         * @param matrixColumn
         *     New value of the "matrixColumn" property.
         */
        public Matrix.Builder<_B> withMatrixColumn(final Iterable<? extends MatrixColumn> matrixColumn) {
            if (this.matrixColumn!= null) {
                this.matrixColumn.clear();
            }
            return addMatrixColumn(matrixColumn);
        }

        /**
         * Adds the given items to the value of "matrixColumn"
         * 
         * @param matrixColumn
         *     Items to add to the value of the "matrixColumn" property
         */
        public Matrix.Builder<_B> addMatrixColumn(MatrixColumn... matrixColumn) {
            addMatrixColumn(Arrays.asList(matrixColumn));
            return this;
        }

        /**
         * Sets the new value of "matrixColumn" (any previous value will be replaced)
         * 
         * @param matrixColumn
         *     New value of the "matrixColumn" property.
         */
        public Matrix.Builder<_B> withMatrixColumn(MatrixColumn... matrixColumn) {
            withMatrixColumn(Arrays.asList(matrixColumn));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "MatrixColumn" property.
         * Use {@link org.nuclos.schema.layout.layoutml.MatrixColumn.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "MatrixColumn" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.MatrixColumn.Builder#end()} to return to the current builder.
         */
        public MatrixColumn.Builder<? extends Matrix.Builder<_B>> addMatrixColumn() {
            if (this.matrixColumn == null) {
                this.matrixColumn = new ArrayList<MatrixColumn.Builder<Matrix.Builder<_B>>>();
            }
            final MatrixColumn.Builder<Matrix.Builder<_B>> matrixColumn_Builder = new MatrixColumn.Builder<Matrix.Builder<_B>>(this, null, false);
            this.matrixColumn.add(matrixColumn_Builder);
            return matrixColumn_Builder;
        }

        /**
         * Sets the new value of "newEnabled" (any previous value will be replaced)
         * 
         * @param newEnabled
         *     New value of the "newEnabled" property.
         */
        public Matrix.Builder<_B> withNewEnabled(final NewEnabled newEnabled) {
            this.newEnabled = ((newEnabled == null)?null:new NewEnabled.Builder<Matrix.Builder<_B>>(this, newEnabled, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "newEnabled" property.
         * Use {@link org.nuclos.schema.layout.layoutml.NewEnabled.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "newEnabled" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.NewEnabled.Builder#end()} to return to the current builder.
         */
        public NewEnabled.Builder<? extends Matrix.Builder<_B>> withNewEnabled() {
            if (this.newEnabled!= null) {
                return this.newEnabled;
            }
            return this.newEnabled = new NewEnabled.Builder<Matrix.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "editEnabled" (any previous value will be replaced)
         * 
         * @param editEnabled
         *     New value of the "editEnabled" property.
         */
        public Matrix.Builder<_B> withEditEnabled(final EditEnabled editEnabled) {
            this.editEnabled = ((editEnabled == null)?null:new EditEnabled.Builder<Matrix.Builder<_B>>(this, editEnabled, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "editEnabled" property.
         * Use {@link org.nuclos.schema.layout.layoutml.EditEnabled.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "editEnabled" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.EditEnabled.Builder#end()} to return to the current builder.
         */
        public EditEnabled.Builder<? extends Matrix.Builder<_B>> withEditEnabled() {
            if (this.editEnabled!= null) {
                return this.editEnabled;
            }
            return this.editEnabled = new EditEnabled.Builder<Matrix.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "deleteEnabled" (any previous value will be replaced)
         * 
         * @param deleteEnabled
         *     New value of the "deleteEnabled" property.
         */
        public Matrix.Builder<_B> withDeleteEnabled(final DeleteEnabled deleteEnabled) {
            this.deleteEnabled = ((deleteEnabled == null)?null:new DeleteEnabled.Builder<Matrix.Builder<_B>>(this, deleteEnabled, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "deleteEnabled" property.
         * Use {@link org.nuclos.schema.layout.layoutml.DeleteEnabled.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "deleteEnabled" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.DeleteEnabled.Builder#end()} to return to the current builder.
         */
        public DeleteEnabled.Builder<? extends Matrix.Builder<_B>> withDeleteEnabled() {
            if (this.deleteEnabled!= null) {
                return this.deleteEnabled;
            }
            return this.deleteEnabled = new DeleteEnabled.Builder<Matrix.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "cloneEnabled" (any previous value will be replaced)
         * 
         * @param cloneEnabled
         *     New value of the "cloneEnabled" property.
         */
        public Matrix.Builder<_B> withCloneEnabled(final CloneEnabled cloneEnabled) {
            this.cloneEnabled = ((cloneEnabled == null)?null:new CloneEnabled.Builder<Matrix.Builder<_B>>(this, cloneEnabled, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "cloneEnabled" property.
         * Use {@link org.nuclos.schema.layout.layoutml.CloneEnabled.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "cloneEnabled" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.CloneEnabled.Builder#end()} to return to the current builder.
         */
        public CloneEnabled.Builder<? extends Matrix.Builder<_B>> withCloneEnabled() {
            if (this.cloneEnabled!= null) {
                return this.cloneEnabled;
            }
            return this.cloneEnabled = new CloneEnabled.Builder<Matrix.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public Matrix.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "matrixPreferencesField" (any previous value will be replaced)
         * 
         * @param matrixPreferencesField
         *     New value of the "matrixPreferencesField" property.
         */
        public Matrix.Builder<_B> withMatrixPreferencesField(final String matrixPreferencesField) {
            this.matrixPreferencesField = matrixPreferencesField;
            return this;
        }

        /**
         * Sets the new value of "entityX" (any previous value will be replaced)
         * 
         * @param entityX
         *     New value of the "entityX" property.
         */
        public Matrix.Builder<_B> withEntityX(final String entityX) {
            this.entityX = entityX;
            return this;
        }

        /**
         * Sets the new value of "entityY" (any previous value will be replaced)
         * 
         * @param entityY
         *     New value of the "entityY" property.
         */
        public Matrix.Builder<_B> withEntityY(final String entityY) {
            this.entityY = entityY;
            return this;
        }

        /**
         * Sets the new value of "entityMatrix" (any previous value will be replaced)
         * 
         * @param entityMatrix
         *     New value of the "entityMatrix" property.
         */
        public Matrix.Builder<_B> withEntityMatrix(final String entityMatrix) {
            this.entityMatrix = entityMatrix;
            return this;
        }

        /**
         * Sets the new value of "entityFieldMatrixParent" (any previous value will be replaced)
         * 
         * @param entityFieldMatrixParent
         *     New value of the "entityFieldMatrixParent" property.
         */
        public Matrix.Builder<_B> withEntityFieldMatrixParent(final String entityFieldMatrixParent) {
            this.entityFieldMatrixParent = entityFieldMatrixParent;
            return this;
        }

        /**
         * Sets the new value of "entityFieldMatrixXRefField" (any previous value will be replaced)
         * 
         * @param entityFieldMatrixXRefField
         *     New value of the "entityFieldMatrixXRefField" property.
         */
        public Matrix.Builder<_B> withEntityFieldMatrixXRefField(final String entityFieldMatrixXRefField) {
            this.entityFieldMatrixXRefField = entityFieldMatrixXRefField;
            return this;
        }

        /**
         * Sets the new value of "entityMatrixValueField" (any previous value will be replaced)
         * 
         * @param entityMatrixValueField
         *     New value of the "entityMatrixValueField" property.
         */
        public Matrix.Builder<_B> withEntityMatrixValueField(final String entityMatrixValueField) {
            this.entityMatrixValueField = entityMatrixValueField;
            return this;
        }

        /**
         * Sets the new value of "entityMatrixNumberState" (any previous value will be replaced)
         * 
         * @param entityMatrixNumberState
         *     New value of the "entityMatrixNumberState" property.
         */
        public Matrix.Builder<_B> withEntityMatrixNumberState(final String entityMatrixNumberState) {
            this.entityMatrixNumberState = entityMatrixNumberState;
            return this;
        }

        /**
         * Sets the new value of "entityFieldCategorie" (any previous value will be replaced)
         * 
         * @param entityFieldCategorie
         *     New value of the "entityFieldCategorie" property.
         */
        public Matrix.Builder<_B> withEntityFieldCategorie(final String entityFieldCategorie) {
            this.entityFieldCategorie = entityFieldCategorie;
            return this;
        }

        /**
         * Sets the new value of "entityFieldX" (any previous value will be replaced)
         * 
         * @param entityFieldX
         *     New value of the "entityFieldX" property.
         */
        public Matrix.Builder<_B> withEntityFieldX(final String entityFieldX) {
            this.entityFieldX = entityFieldX;
            return this;
        }

        /**
         * Sets the new value of "entityYParentField" (any previous value will be replaced)
         * 
         * @param entityYParentField
         *     New value of the "entityYParentField" property.
         */
        public Matrix.Builder<_B> withEntityYParentField(final String entityYParentField) {
            this.entityYParentField = entityYParentField;
            return this;
        }

        /**
         * Sets the new value of "entityFieldY" (any previous value will be replaced)
         * 
         * @param entityFieldY
         *     New value of the "entityFieldY" property.
         */
        public Matrix.Builder<_B> withEntityFieldY(final String entityFieldY) {
            this.entityFieldY = entityFieldY;
            return this;
        }

        /**
         * Sets the new value of "entityXSortingFields" (any previous value will be replaced)
         * 
         * @param entityXSortingFields
         *     New value of the "entityXSortingFields" property.
         */
        public Matrix.Builder<_B> withEntityXSortingFields(final String entityXSortingFields) {
            this.entityXSortingFields = entityXSortingFields;
            return this;
        }

        /**
         * Sets the new value of "entityYSortingFields" (any previous value will be replaced)
         * 
         * @param entityYSortingFields
         *     New value of the "entityYSortingFields" property.
         */
        public Matrix.Builder<_B> withEntityYSortingFields(final String entityYSortingFields) {
            this.entityYSortingFields = entityYSortingFields;
            return this;
        }

        /**
         * Sets the new value of "entityXHeader" (any previous value will be replaced)
         * 
         * @param entityXHeader
         *     New value of the "entityXHeader" property.
         */
        public Matrix.Builder<_B> withEntityXHeader(final String entityXHeader) {
            this.entityXHeader = entityXHeader;
            return this;
        }

        /**
         * Sets the new value of "entityYHeader" (any previous value will be replaced)
         * 
         * @param entityYHeader
         *     New value of the "entityYHeader" property.
         */
        public Matrix.Builder<_B> withEntityYHeader(final String entityYHeader) {
            this.entityYHeader = entityYHeader;
            return this;
        }

        /**
         * Sets the new value of "entityMatrixReferenceField" (any previous value will be replaced)
         * 
         * @param entityMatrixReferenceField
         *     New value of the "entityMatrixReferenceField" property.
         */
        public Matrix.Builder<_B> withEntityMatrixReferenceField(final String entityMatrixReferenceField) {
            this.entityMatrixReferenceField = entityMatrixReferenceField;
            return this;
        }

        /**
         * Sets the new value of "entityMatrixValueType" (any previous value will be replaced)
         * 
         * @param entityMatrixValueType
         *     New value of the "entityMatrixValueType" property.
         */
        public Matrix.Builder<_B> withEntityMatrixValueType(final String entityMatrixValueType) {
            this.entityMatrixValueType = entityMatrixValueType;
            return this;
        }

        /**
         * Sets the new value of "entityXVlpId" (any previous value will be replaced)
         * 
         * @param entityXVlpId
         *     New value of the "entityXVlpId" property.
         */
        public Matrix.Builder<_B> withEntityXVlpId(final String entityXVlpId) {
            this.entityXVlpId = entityXVlpId;
            return this;
        }

        /**
         * Sets the new value of "entityXVlpIdfieldname" (any previous value will be replaced)
         * 
         * @param entityXVlpIdfieldname
         *     New value of the "entityXVlpIdfieldname" property.
         */
        public Matrix.Builder<_B> withEntityXVlpIdfieldname(final String entityXVlpIdfieldname) {
            this.entityXVlpIdfieldname = entityXVlpIdfieldname;
            return this;
        }

        /**
         * Sets the new value of "entityXVlpFieldname" (any previous value will be replaced)
         * 
         * @param entityXVlpFieldname
         *     New value of the "entityXVlpFieldname" property.
         */
        public Matrix.Builder<_B> withEntityXVlpFieldname(final String entityXVlpFieldname) {
            this.entityXVlpFieldname = entityXVlpFieldname;
            return this;
        }

        /**
         * Sets the new value of "entityXVlpReferenceParamName" (any previous value will be replaced)
         * 
         * @param entityXVlpReferenceParamName
         *     New value of the "entityXVlpReferenceParamName" property.
         */
        public Matrix.Builder<_B> withEntityXVlpReferenceParamName(final String entityXVlpReferenceParamName) {
            this.entityXVlpReferenceParamName = entityXVlpReferenceParamName;
            return this;
        }

        /**
         * Sets the new value of "cellInputType" (any previous value will be replaced)
         * 
         * @param cellInputType
         *     New value of the "cellInputType" property.
         */
        public Matrix.Builder<_B> withCellInputType(final String cellInputType) {
            this.cellInputType = cellInputType;
            return this;
        }

        /**
         * Sets the new value of "enabled" (any previous value will be replaced)
         * 
         * @param enabled
         *     New value of the "enabled" property.
         */
        public Matrix.Builder<_B> withEnabled(final Boolean enabled) {
            this.enabled = enabled;
            return this;
        }

        /**
         * Sets the new value of "opaque" (any previous value will be replaced)
         * 
         * @param opaque
         *     New value of the "opaque" property.
         */
        public Matrix.Builder<_B> withOpaque(final Boolean opaque) {
            this.opaque = opaque;
            return this;
        }

        /**
         * Sets the new value of "controllertype" (any previous value will be replaced)
         * 
         * @param controllertype
         *     New value of the "controllertype" property.
         */
        public Matrix.Builder<_B> withControllertype(final String controllertype) {
            this.controllertype = controllertype;
            return this;
        }

        /**
         * Sets the new value of "dynamicCellHeightsDefault" (any previous value will be replaced)
         * 
         * @param dynamicCellHeightsDefault
         *     New value of the "dynamicCellHeightsDefault" property.
         */
        public Matrix.Builder<_B> withDynamicCellHeightsDefault(final Boolean dynamicCellHeightsDefault) {
            this.dynamicCellHeightsDefault = dynamicCellHeightsDefault;
            return this;
        }

        @Override
        public Matrix build() {
            if (_storedValue == null) {
                return this.init(new Matrix());
            } else {
                return ((Matrix) _storedValue);
            }
        }

        public Matrix.Builder<_B> copyOf(final Matrix _other) {
            _other.copyTo(this);
            return this;
        }

        public Matrix.Builder<_B> copyOf(final Matrix.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Matrix.Selector<Matrix.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Matrix.Select _root() {
            return new Matrix.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> layoutconstraints = null;
        private ClearBorder.Selector<TRoot, Matrix.Selector<TRoot, TParent>> clearBorder = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> border = null;
        private MinimumSize.Selector<TRoot, Matrix.Selector<TRoot, TParent>> minimumSize = null;
        private PreferredSize.Selector<TRoot, Matrix.Selector<TRoot, TParent>> preferredSize = null;
        private StrictSize.Selector<TRoot, Matrix.Selector<TRoot, TParent>> strictSize = null;
        private Font.Selector<TRoot, Matrix.Selector<TRoot, TParent>> font = null;
        private Background.Selector<TRoot, Matrix.Selector<TRoot, TParent>> background = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> description = null;
        private MatrixColumn.Selector<TRoot, Matrix.Selector<TRoot, TParent>> matrixColumn = null;
        private NewEnabled.Selector<TRoot, Matrix.Selector<TRoot, TParent>> newEnabled = null;
        private EditEnabled.Selector<TRoot, Matrix.Selector<TRoot, TParent>> editEnabled = null;
        private DeleteEnabled.Selector<TRoot, Matrix.Selector<TRoot, TParent>> deleteEnabled = null;
        private CloneEnabled.Selector<TRoot, Matrix.Selector<TRoot, TParent>> cloneEnabled = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> matrixPreferencesField = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityX = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityY = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityMatrix = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityFieldMatrixParent = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityFieldMatrixXRefField = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityMatrixValueField = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityMatrixNumberState = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityFieldCategorie = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityFieldX = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityYParentField = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityFieldY = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityXSortingFields = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityYSortingFields = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityXHeader = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityYHeader = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityMatrixReferenceField = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityMatrixValueType = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityXVlpId = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityXVlpIdfieldname = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityXVlpFieldname = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityXVlpReferenceParamName = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> cellInputType = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> enabled = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> opaque = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> controllertype = null;
        private com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> dynamicCellHeightsDefault = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.layoutconstraints!= null) {
                products.put("layoutconstraints", this.layoutconstraints.init());
            }
            if (this.clearBorder!= null) {
                products.put("clearBorder", this.clearBorder.init());
            }
            if (this.border!= null) {
                products.put("border", this.border.init());
            }
            if (this.minimumSize!= null) {
                products.put("minimumSize", this.minimumSize.init());
            }
            if (this.preferredSize!= null) {
                products.put("preferredSize", this.preferredSize.init());
            }
            if (this.strictSize!= null) {
                products.put("strictSize", this.strictSize.init());
            }
            if (this.font!= null) {
                products.put("font", this.font.init());
            }
            if (this.background!= null) {
                products.put("background", this.background.init());
            }
            if (this.description!= null) {
                products.put("description", this.description.init());
            }
            if (this.matrixColumn!= null) {
                products.put("matrixColumn", this.matrixColumn.init());
            }
            if (this.newEnabled!= null) {
                products.put("newEnabled", this.newEnabled.init());
            }
            if (this.editEnabled!= null) {
                products.put("editEnabled", this.editEnabled.init());
            }
            if (this.deleteEnabled!= null) {
                products.put("deleteEnabled", this.deleteEnabled.init());
            }
            if (this.cloneEnabled!= null) {
                products.put("cloneEnabled", this.cloneEnabled.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.matrixPreferencesField!= null) {
                products.put("matrixPreferencesField", this.matrixPreferencesField.init());
            }
            if (this.entityX!= null) {
                products.put("entityX", this.entityX.init());
            }
            if (this.entityY!= null) {
                products.put("entityY", this.entityY.init());
            }
            if (this.entityMatrix!= null) {
                products.put("entityMatrix", this.entityMatrix.init());
            }
            if (this.entityFieldMatrixParent!= null) {
                products.put("entityFieldMatrixParent", this.entityFieldMatrixParent.init());
            }
            if (this.entityFieldMatrixXRefField!= null) {
                products.put("entityFieldMatrixXRefField", this.entityFieldMatrixXRefField.init());
            }
            if (this.entityMatrixValueField!= null) {
                products.put("entityMatrixValueField", this.entityMatrixValueField.init());
            }
            if (this.entityMatrixNumberState!= null) {
                products.put("entityMatrixNumberState", this.entityMatrixNumberState.init());
            }
            if (this.entityFieldCategorie!= null) {
                products.put("entityFieldCategorie", this.entityFieldCategorie.init());
            }
            if (this.entityFieldX!= null) {
                products.put("entityFieldX", this.entityFieldX.init());
            }
            if (this.entityYParentField!= null) {
                products.put("entityYParentField", this.entityYParentField.init());
            }
            if (this.entityFieldY!= null) {
                products.put("entityFieldY", this.entityFieldY.init());
            }
            if (this.entityXSortingFields!= null) {
                products.put("entityXSortingFields", this.entityXSortingFields.init());
            }
            if (this.entityYSortingFields!= null) {
                products.put("entityYSortingFields", this.entityYSortingFields.init());
            }
            if (this.entityXHeader!= null) {
                products.put("entityXHeader", this.entityXHeader.init());
            }
            if (this.entityYHeader!= null) {
                products.put("entityYHeader", this.entityYHeader.init());
            }
            if (this.entityMatrixReferenceField!= null) {
                products.put("entityMatrixReferenceField", this.entityMatrixReferenceField.init());
            }
            if (this.entityMatrixValueType!= null) {
                products.put("entityMatrixValueType", this.entityMatrixValueType.init());
            }
            if (this.entityXVlpId!= null) {
                products.put("entityXVlpId", this.entityXVlpId.init());
            }
            if (this.entityXVlpIdfieldname!= null) {
                products.put("entityXVlpIdfieldname", this.entityXVlpIdfieldname.init());
            }
            if (this.entityXVlpFieldname!= null) {
                products.put("entityXVlpFieldname", this.entityXVlpFieldname.init());
            }
            if (this.entityXVlpReferenceParamName!= null) {
                products.put("entityXVlpReferenceParamName", this.entityXVlpReferenceParamName.init());
            }
            if (this.cellInputType!= null) {
                products.put("cellInputType", this.cellInputType.init());
            }
            if (this.enabled!= null) {
                products.put("enabled", this.enabled.init());
            }
            if (this.opaque!= null) {
                products.put("opaque", this.opaque.init());
            }
            if (this.controllertype!= null) {
                products.put("controllertype", this.controllertype.init());
            }
            if (this.dynamicCellHeightsDefault!= null) {
                products.put("dynamicCellHeightsDefault", this.dynamicCellHeightsDefault.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> layoutconstraints() {
            return ((this.layoutconstraints == null)?this.layoutconstraints = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "layoutconstraints"):this.layoutconstraints);
        }

        public ClearBorder.Selector<TRoot, Matrix.Selector<TRoot, TParent>> clearBorder() {
            return ((this.clearBorder == null)?this.clearBorder = new ClearBorder.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "clearBorder"):this.clearBorder);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> border() {
            return ((this.border == null)?this.border = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "border"):this.border);
        }

        public MinimumSize.Selector<TRoot, Matrix.Selector<TRoot, TParent>> minimumSize() {
            return ((this.minimumSize == null)?this.minimumSize = new MinimumSize.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "minimumSize"):this.minimumSize);
        }

        public PreferredSize.Selector<TRoot, Matrix.Selector<TRoot, TParent>> preferredSize() {
            return ((this.preferredSize == null)?this.preferredSize = new PreferredSize.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "preferredSize"):this.preferredSize);
        }

        public StrictSize.Selector<TRoot, Matrix.Selector<TRoot, TParent>> strictSize() {
            return ((this.strictSize == null)?this.strictSize = new StrictSize.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "strictSize"):this.strictSize);
        }

        public Font.Selector<TRoot, Matrix.Selector<TRoot, TParent>> font() {
            return ((this.font == null)?this.font = new Font.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "font"):this.font);
        }

        public Background.Selector<TRoot, Matrix.Selector<TRoot, TParent>> background() {
            return ((this.background == null)?this.background = new Background.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "background"):this.background);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> description() {
            return ((this.description == null)?this.description = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "description"):this.description);
        }

        public MatrixColumn.Selector<TRoot, Matrix.Selector<TRoot, TParent>> matrixColumn() {
            return ((this.matrixColumn == null)?this.matrixColumn = new MatrixColumn.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "matrixColumn"):this.matrixColumn);
        }

        public NewEnabled.Selector<TRoot, Matrix.Selector<TRoot, TParent>> newEnabled() {
            return ((this.newEnabled == null)?this.newEnabled = new NewEnabled.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "newEnabled"):this.newEnabled);
        }

        public EditEnabled.Selector<TRoot, Matrix.Selector<TRoot, TParent>> editEnabled() {
            return ((this.editEnabled == null)?this.editEnabled = new EditEnabled.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "editEnabled"):this.editEnabled);
        }

        public DeleteEnabled.Selector<TRoot, Matrix.Selector<TRoot, TParent>> deleteEnabled() {
            return ((this.deleteEnabled == null)?this.deleteEnabled = new DeleteEnabled.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "deleteEnabled"):this.deleteEnabled);
        }

        public CloneEnabled.Selector<TRoot, Matrix.Selector<TRoot, TParent>> cloneEnabled() {
            return ((this.cloneEnabled == null)?this.cloneEnabled = new CloneEnabled.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "cloneEnabled"):this.cloneEnabled);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> matrixPreferencesField() {
            return ((this.matrixPreferencesField == null)?this.matrixPreferencesField = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "matrixPreferencesField"):this.matrixPreferencesField);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityX() {
            return ((this.entityX == null)?this.entityX = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "entityX"):this.entityX);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityY() {
            return ((this.entityY == null)?this.entityY = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "entityY"):this.entityY);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityMatrix() {
            return ((this.entityMatrix == null)?this.entityMatrix = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "entityMatrix"):this.entityMatrix);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityFieldMatrixParent() {
            return ((this.entityFieldMatrixParent == null)?this.entityFieldMatrixParent = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "entityFieldMatrixParent"):this.entityFieldMatrixParent);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityFieldMatrixXRefField() {
            return ((this.entityFieldMatrixXRefField == null)?this.entityFieldMatrixXRefField = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "entityFieldMatrixXRefField"):this.entityFieldMatrixXRefField);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityMatrixValueField() {
            return ((this.entityMatrixValueField == null)?this.entityMatrixValueField = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "entityMatrixValueField"):this.entityMatrixValueField);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityMatrixNumberState() {
            return ((this.entityMatrixNumberState == null)?this.entityMatrixNumberState = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "entityMatrixNumberState"):this.entityMatrixNumberState);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityFieldCategorie() {
            return ((this.entityFieldCategorie == null)?this.entityFieldCategorie = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "entityFieldCategorie"):this.entityFieldCategorie);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityFieldX() {
            return ((this.entityFieldX == null)?this.entityFieldX = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "entityFieldX"):this.entityFieldX);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityYParentField() {
            return ((this.entityYParentField == null)?this.entityYParentField = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "entityYParentField"):this.entityYParentField);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityFieldY() {
            return ((this.entityFieldY == null)?this.entityFieldY = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "entityFieldY"):this.entityFieldY);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityXSortingFields() {
            return ((this.entityXSortingFields == null)?this.entityXSortingFields = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "entityXSortingFields"):this.entityXSortingFields);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityYSortingFields() {
            return ((this.entityYSortingFields == null)?this.entityYSortingFields = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "entityYSortingFields"):this.entityYSortingFields);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityXHeader() {
            return ((this.entityXHeader == null)?this.entityXHeader = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "entityXHeader"):this.entityXHeader);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityYHeader() {
            return ((this.entityYHeader == null)?this.entityYHeader = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "entityYHeader"):this.entityYHeader);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityMatrixReferenceField() {
            return ((this.entityMatrixReferenceField == null)?this.entityMatrixReferenceField = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "entityMatrixReferenceField"):this.entityMatrixReferenceField);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityMatrixValueType() {
            return ((this.entityMatrixValueType == null)?this.entityMatrixValueType = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "entityMatrixValueType"):this.entityMatrixValueType);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityXVlpId() {
            return ((this.entityXVlpId == null)?this.entityXVlpId = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "entityXVlpId"):this.entityXVlpId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityXVlpIdfieldname() {
            return ((this.entityXVlpIdfieldname == null)?this.entityXVlpIdfieldname = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "entityXVlpIdfieldname"):this.entityXVlpIdfieldname);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityXVlpFieldname() {
            return ((this.entityXVlpFieldname == null)?this.entityXVlpFieldname = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "entityXVlpFieldname"):this.entityXVlpFieldname);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> entityXVlpReferenceParamName() {
            return ((this.entityXVlpReferenceParamName == null)?this.entityXVlpReferenceParamName = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "entityXVlpReferenceParamName"):this.entityXVlpReferenceParamName);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> cellInputType() {
            return ((this.cellInputType == null)?this.cellInputType = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "cellInputType"):this.cellInputType);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> enabled() {
            return ((this.enabled == null)?this.enabled = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "enabled"):this.enabled);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> opaque() {
            return ((this.opaque == null)?this.opaque = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "opaque"):this.opaque);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> controllertype() {
            return ((this.controllertype == null)?this.controllertype = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "controllertype"):this.controllertype);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>> dynamicCellHeightsDefault() {
            return ((this.dynamicCellHeightsDefault == null)?this.dynamicCellHeightsDefault = new com.kscs.util.jaxb.Selector<TRoot, Matrix.Selector<TRoot, TParent>>(this._root, this, "dynamicCellHeightsDefault"):this.dynamicCellHeightsDefault);
        }

    }

}
