
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}text-color" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="bold" type="{}boolean" /&gt;
 *       &lt;attribute name="italic" type="{}boolean" /&gt;
 *       &lt;attribute name="underline" type="{}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "textColor"
})
@XmlRootElement(name = "text-format")
public class TextFormat implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "text-color")
    protected TextColor textColor;
    @XmlAttribute(name = "bold")
    protected Boolean bold;
    @XmlAttribute(name = "italic")
    protected Boolean italic;
    @XmlAttribute(name = "underline")
    protected Boolean underline;

    /**
     * Gets the value of the textColor property.
     * 
     * @return
     *     possible object is
     *     {@link TextColor }
     *     
     */
    public TextColor getTextColor() {
        return textColor;
    }

    /**
     * Sets the value of the textColor property.
     * 
     * @param value
     *     allowed object is
     *     {@link TextColor }
     *     
     */
    public void setTextColor(TextColor value) {
        this.textColor = value;
    }

    /**
     * Gets the value of the bold property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getBold() {
        return bold;
    }

    /**
     * Sets the value of the bold property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBold(Boolean value) {
        this.bold = value;
    }

    /**
     * Gets the value of the italic property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getItalic() {
        return italic;
    }

    /**
     * Sets the value of the italic property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setItalic(Boolean value) {
        this.italic = value;
    }

    /**
     * Gets the value of the underline property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getUnderline() {
        return underline;
    }

    /**
     * Sets the value of the underline property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUnderline(Boolean value) {
        this.underline = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            TextColor theTextColor;
            theTextColor = this.getTextColor();
            strategy.appendField(locator, this, "textColor", buffer, theTextColor);
        }
        {
            Boolean theBold;
            theBold = this.getBold();
            strategy.appendField(locator, this, "bold", buffer, theBold);
        }
        {
            Boolean theItalic;
            theItalic = this.getItalic();
            strategy.appendField(locator, this, "italic", buffer, theItalic);
        }
        {
            Boolean theUnderline;
            theUnderline = this.getUnderline();
            strategy.appendField(locator, this, "underline", buffer, theUnderline);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof TextFormat)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final TextFormat that = ((TextFormat) object);
        {
            TextColor lhsTextColor;
            lhsTextColor = this.getTextColor();
            TextColor rhsTextColor;
            rhsTextColor = that.getTextColor();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "textColor", lhsTextColor), LocatorUtils.property(thatLocator, "textColor", rhsTextColor), lhsTextColor, rhsTextColor)) {
                return false;
            }
        }
        {
            Boolean lhsBold;
            lhsBold = this.getBold();
            Boolean rhsBold;
            rhsBold = that.getBold();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "bold", lhsBold), LocatorUtils.property(thatLocator, "bold", rhsBold), lhsBold, rhsBold)) {
                return false;
            }
        }
        {
            Boolean lhsItalic;
            lhsItalic = this.getItalic();
            Boolean rhsItalic;
            rhsItalic = that.getItalic();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "italic", lhsItalic), LocatorUtils.property(thatLocator, "italic", rhsItalic), lhsItalic, rhsItalic)) {
                return false;
            }
        }
        {
            Boolean lhsUnderline;
            lhsUnderline = this.getUnderline();
            Boolean rhsUnderline;
            rhsUnderline = that.getUnderline();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "underline", lhsUnderline), LocatorUtils.property(thatLocator, "underline", rhsUnderline), lhsUnderline, rhsUnderline)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            TextColor theTextColor;
            theTextColor = this.getTextColor();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "textColor", theTextColor), currentHashCode, theTextColor);
        }
        {
            Boolean theBold;
            theBold = this.getBold();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "bold", theBold), currentHashCode, theBold);
        }
        {
            Boolean theItalic;
            theItalic = this.getItalic();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "italic", theItalic), currentHashCode, theItalic);
        }
        {
            Boolean theUnderline;
            theUnderline = this.getUnderline();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "underline", theUnderline), currentHashCode, theUnderline);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof TextFormat) {
            final TextFormat copy = ((TextFormat) draftCopy);
            if (this.textColor!= null) {
                TextColor sourceTextColor;
                sourceTextColor = this.getTextColor();
                TextColor copyTextColor = ((TextColor) strategy.copy(LocatorUtils.property(locator, "textColor", sourceTextColor), sourceTextColor));
                copy.setTextColor(copyTextColor);
            } else {
                copy.textColor = null;
            }
            if (this.bold!= null) {
                Boolean sourceBold;
                sourceBold = this.getBold();
                Boolean copyBold = ((Boolean) strategy.copy(LocatorUtils.property(locator, "bold", sourceBold), sourceBold));
                copy.setBold(copyBold);
            } else {
                copy.bold = null;
            }
            if (this.italic!= null) {
                Boolean sourceItalic;
                sourceItalic = this.getItalic();
                Boolean copyItalic = ((Boolean) strategy.copy(LocatorUtils.property(locator, "italic", sourceItalic), sourceItalic));
                copy.setItalic(copyItalic);
            } else {
                copy.italic = null;
            }
            if (this.underline!= null) {
                Boolean sourceUnderline;
                sourceUnderline = this.getUnderline();
                Boolean copyUnderline = ((Boolean) strategy.copy(LocatorUtils.property(locator, "underline", sourceUnderline), sourceUnderline));
                copy.setUnderline(copyUnderline);
            } else {
                copy.underline = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new TextFormat();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final TextFormat.Builder<_B> _other) {
        _other.textColor = ((this.textColor == null)?null:this.textColor.newCopyBuilder(_other));
        _other.bold = this.bold;
        _other.italic = this.italic;
        _other.underline = this.underline;
    }

    public<_B >TextFormat.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new TextFormat.Builder<_B>(_parentBuilder, this, true);
    }

    public TextFormat.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static TextFormat.Builder<Void> builder() {
        return new TextFormat.Builder<Void>(null, null, false);
    }

    public static<_B >TextFormat.Builder<_B> copyOf(final TextFormat _other) {
        final TextFormat.Builder<_B> _newBuilder = new TextFormat.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final TextFormat.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree textColorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("textColor"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(textColorPropertyTree!= null):((textColorPropertyTree == null)||(!textColorPropertyTree.isLeaf())))) {
            _other.textColor = ((this.textColor == null)?null:this.textColor.newCopyBuilder(_other, textColorPropertyTree, _propertyTreeUse));
        }
        final PropertyTree boldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("bold"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boldPropertyTree!= null):((boldPropertyTree == null)||(!boldPropertyTree.isLeaf())))) {
            _other.bold = this.bold;
        }
        final PropertyTree italicPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("italic"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(italicPropertyTree!= null):((italicPropertyTree == null)||(!italicPropertyTree.isLeaf())))) {
            _other.italic = this.italic;
        }
        final PropertyTree underlinePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("underline"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(underlinePropertyTree!= null):((underlinePropertyTree == null)||(!underlinePropertyTree.isLeaf())))) {
            _other.underline = this.underline;
        }
    }

    public<_B >TextFormat.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new TextFormat.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public TextFormat.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >TextFormat.Builder<_B> copyOf(final TextFormat _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final TextFormat.Builder<_B> _newBuilder = new TextFormat.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static TextFormat.Builder<Void> copyExcept(final TextFormat _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static TextFormat.Builder<Void> copyOnly(final TextFormat _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final TextFormat _storedValue;
        private TextColor.Builder<TextFormat.Builder<_B>> textColor;
        private Boolean bold;
        private Boolean italic;
        private Boolean underline;

        public Builder(final _B _parentBuilder, final TextFormat _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.textColor = ((_other.textColor == null)?null:_other.textColor.newCopyBuilder(this));
                    this.bold = _other.bold;
                    this.italic = _other.italic;
                    this.underline = _other.underline;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final TextFormat _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree textColorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("textColor"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(textColorPropertyTree!= null):((textColorPropertyTree == null)||(!textColorPropertyTree.isLeaf())))) {
                        this.textColor = ((_other.textColor == null)?null:_other.textColor.newCopyBuilder(this, textColorPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree boldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("bold"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boldPropertyTree!= null):((boldPropertyTree == null)||(!boldPropertyTree.isLeaf())))) {
                        this.bold = _other.bold;
                    }
                    final PropertyTree italicPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("italic"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(italicPropertyTree!= null):((italicPropertyTree == null)||(!italicPropertyTree.isLeaf())))) {
                        this.italic = _other.italic;
                    }
                    final PropertyTree underlinePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("underline"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(underlinePropertyTree!= null):((underlinePropertyTree == null)||(!underlinePropertyTree.isLeaf())))) {
                        this.underline = _other.underline;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends TextFormat >_P init(final _P _product) {
            _product.textColor = ((this.textColor == null)?null:this.textColor.build());
            _product.bold = this.bold;
            _product.italic = this.italic;
            _product.underline = this.underline;
            return _product;
        }

        /**
         * Sets the new value of "textColor" (any previous value will be replaced)
         * 
         * @param textColor
         *     New value of the "textColor" property.
         */
        public TextFormat.Builder<_B> withTextColor(final TextColor textColor) {
            this.textColor = ((textColor == null)?null:new TextColor.Builder<TextFormat.Builder<_B>>(this, textColor, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "textColor" property.
         * Use {@link org.nuclos.schema.layout.layoutml.TextColor.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "textColor" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.TextColor.Builder#end()} to return to the current builder.
         */
        public TextColor.Builder<? extends TextFormat.Builder<_B>> withTextColor() {
            if (this.textColor!= null) {
                return this.textColor;
            }
            return this.textColor = new TextColor.Builder<TextFormat.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "bold" (any previous value will be replaced)
         * 
         * @param bold
         *     New value of the "bold" property.
         */
        public TextFormat.Builder<_B> withBold(final Boolean bold) {
            this.bold = bold;
            return this;
        }

        /**
         * Sets the new value of "italic" (any previous value will be replaced)
         * 
         * @param italic
         *     New value of the "italic" property.
         */
        public TextFormat.Builder<_B> withItalic(final Boolean italic) {
            this.italic = italic;
            return this;
        }

        /**
         * Sets the new value of "underline" (any previous value will be replaced)
         * 
         * @param underline
         *     New value of the "underline" property.
         */
        public TextFormat.Builder<_B> withUnderline(final Boolean underline) {
            this.underline = underline;
            return this;
        }

        @Override
        public TextFormat build() {
            if (_storedValue == null) {
                return this.init(new TextFormat());
            } else {
                return ((TextFormat) _storedValue);
            }
        }

        public TextFormat.Builder<_B> copyOf(final TextFormat _other) {
            _other.copyTo(this);
            return this;
        }

        public TextFormat.Builder<_B> copyOf(final TextFormat.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends TextFormat.Selector<TextFormat.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static TextFormat.Select _root() {
            return new TextFormat.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private TextColor.Selector<TRoot, TextFormat.Selector<TRoot, TParent>> textColor = null;
        private com.kscs.util.jaxb.Selector<TRoot, TextFormat.Selector<TRoot, TParent>> bold = null;
        private com.kscs.util.jaxb.Selector<TRoot, TextFormat.Selector<TRoot, TParent>> italic = null;
        private com.kscs.util.jaxb.Selector<TRoot, TextFormat.Selector<TRoot, TParent>> underline = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.textColor!= null) {
                products.put("textColor", this.textColor.init());
            }
            if (this.bold!= null) {
                products.put("bold", this.bold.init());
            }
            if (this.italic!= null) {
                products.put("italic", this.italic.init());
            }
            if (this.underline!= null) {
                products.put("underline", this.underline.init());
            }
            return products;
        }

        public TextColor.Selector<TRoot, TextFormat.Selector<TRoot, TParent>> textColor() {
            return ((this.textColor == null)?this.textColor = new TextColor.Selector<TRoot, TextFormat.Selector<TRoot, TParent>>(this._root, this, "textColor"):this.textColor);
        }

        public com.kscs.util.jaxb.Selector<TRoot, TextFormat.Selector<TRoot, TParent>> bold() {
            return ((this.bold == null)?this.bold = new com.kscs.util.jaxb.Selector<TRoot, TextFormat.Selector<TRoot, TParent>>(this._root, this, "bold"):this.bold);
        }

        public com.kscs.util.jaxb.Selector<TRoot, TextFormat.Selector<TRoot, TParent>> italic() {
            return ((this.italic == null)?this.italic = new com.kscs.util.jaxb.Selector<TRoot, TextFormat.Selector<TRoot, TParent>>(this._root, this, "italic"):this.italic);
        }

        public com.kscs.util.jaxb.Selector<TRoot, TextFormat.Selector<TRoot, TParent>> underline() {
            return ((this.underline == null)?this.underline = new com.kscs.util.jaxb.Selector<TRoot, TextFormat.Selector<TRoot, TParent>>(this._root, this, "underline"):this.underline);
        }

    }

}
