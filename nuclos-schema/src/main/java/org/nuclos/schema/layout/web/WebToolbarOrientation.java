
package org.nuclos.schema.layout.web;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for web-toolbar-orientation.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="web-toolbar-orientation"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="horizontal"/&gt;
 *     &lt;enumeration value="vertical"/&gt;
 *     &lt;enumeration value="hide"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "web-toolbar-orientation")
@XmlEnum
public enum WebToolbarOrientation {

    @XmlEnumValue("horizontal")
    HORIZONTAL("horizontal"),
    @XmlEnumValue("vertical")
    VERTICAL("vertical"),
    @XmlEnumValue("hide")
    HIDE("hide");
    private final String value;

    WebToolbarOrientation(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static WebToolbarOrientation fromValue(String v) {
        for (WebToolbarOrientation c: WebToolbarOrientation.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
