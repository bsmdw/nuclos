
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice maxOccurs="unbounded" minOccurs="0"&gt;
 *         &lt;element ref="{}transfer-lookedup-value"/&gt;
 *         &lt;element ref="{}clear"/&gt;
 *         &lt;element ref="{}enable"/&gt;
 *         &lt;element ref="{}refresh-valuelist"/&gt;
 *         &lt;element ref="{}reinit-subform"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transferLookedupValueOrClearOrEnable"
})
@XmlRootElement(name = "actions")
public class Actions implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElements({
        @XmlElement(name = "transfer-lookedup-value", type = TransferLookedupValue.class),
        @XmlElement(name = "clear", type = Clear.class),
        @XmlElement(name = "enable", type = Enable.class),
        @XmlElement(name = "refresh-valuelist", type = RefreshValuelist.class),
        @XmlElement(name = "reinit-subform", type = ReinitSubform.class)
    })
    protected List<Serializable> transferLookedupValueOrClearOrEnable;

    /**
     * Gets the value of the transferLookedupValueOrClearOrEnable property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transferLookedupValueOrClearOrEnable property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransferLookedupValueOrClearOrEnable().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransferLookedupValue }
     * {@link Clear }
     * {@link Enable }
     * {@link RefreshValuelist }
     * {@link ReinitSubform }
     * 
     * 
     */
    public List<Serializable> getTransferLookedupValueOrClearOrEnable() {
        if (transferLookedupValueOrClearOrEnable == null) {
            transferLookedupValueOrClearOrEnable = new ArrayList<Serializable>();
        }
        return this.transferLookedupValueOrClearOrEnable;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<Serializable> theTransferLookedupValueOrClearOrEnable;
            theTransferLookedupValueOrClearOrEnable = (((this.transferLookedupValueOrClearOrEnable!= null)&&(!this.transferLookedupValueOrClearOrEnable.isEmpty()))?this.getTransferLookedupValueOrClearOrEnable():null);
            strategy.appendField(locator, this, "transferLookedupValueOrClearOrEnable", buffer, theTransferLookedupValueOrClearOrEnable);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Actions)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Actions that = ((Actions) object);
        {
            List<Serializable> lhsTransferLookedupValueOrClearOrEnable;
            lhsTransferLookedupValueOrClearOrEnable = (((this.transferLookedupValueOrClearOrEnable!= null)&&(!this.transferLookedupValueOrClearOrEnable.isEmpty()))?this.getTransferLookedupValueOrClearOrEnable():null);
            List<Serializable> rhsTransferLookedupValueOrClearOrEnable;
            rhsTransferLookedupValueOrClearOrEnable = (((that.transferLookedupValueOrClearOrEnable!= null)&&(!that.transferLookedupValueOrClearOrEnable.isEmpty()))?that.getTransferLookedupValueOrClearOrEnable():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "transferLookedupValueOrClearOrEnable", lhsTransferLookedupValueOrClearOrEnable), LocatorUtils.property(thatLocator, "transferLookedupValueOrClearOrEnable", rhsTransferLookedupValueOrClearOrEnable), lhsTransferLookedupValueOrClearOrEnable, rhsTransferLookedupValueOrClearOrEnable)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<Serializable> theTransferLookedupValueOrClearOrEnable;
            theTransferLookedupValueOrClearOrEnable = (((this.transferLookedupValueOrClearOrEnable!= null)&&(!this.transferLookedupValueOrClearOrEnable.isEmpty()))?this.getTransferLookedupValueOrClearOrEnable():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "transferLookedupValueOrClearOrEnable", theTransferLookedupValueOrClearOrEnable), currentHashCode, theTransferLookedupValueOrClearOrEnable);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Actions) {
            final Actions copy = ((Actions) draftCopy);
            if ((this.transferLookedupValueOrClearOrEnable!= null)&&(!this.transferLookedupValueOrClearOrEnable.isEmpty())) {
                List<Serializable> sourceTransferLookedupValueOrClearOrEnable;
                sourceTransferLookedupValueOrClearOrEnable = (((this.transferLookedupValueOrClearOrEnable!= null)&&(!this.transferLookedupValueOrClearOrEnable.isEmpty()))?this.getTransferLookedupValueOrClearOrEnable():null);
                @SuppressWarnings("unchecked")
                List<Serializable> copyTransferLookedupValueOrClearOrEnable = ((List<Serializable> ) strategy.copy(LocatorUtils.property(locator, "transferLookedupValueOrClearOrEnable", sourceTransferLookedupValueOrClearOrEnable), sourceTransferLookedupValueOrClearOrEnable));
                copy.transferLookedupValueOrClearOrEnable = null;
                if (copyTransferLookedupValueOrClearOrEnable!= null) {
                    List<Serializable> uniqueTransferLookedupValueOrClearOrEnablel = copy.getTransferLookedupValueOrClearOrEnable();
                    uniqueTransferLookedupValueOrClearOrEnablel.addAll(copyTransferLookedupValueOrClearOrEnable);
                }
            } else {
                copy.transferLookedupValueOrClearOrEnable = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Actions();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Actions.Builder<_B> _other) {
        if (this.transferLookedupValueOrClearOrEnable == null) {
            _other.transferLookedupValueOrClearOrEnable = null;
        } else {
            _other.transferLookedupValueOrClearOrEnable = new ArrayList<Buildable>();
            for (Serializable _item: this.transferLookedupValueOrClearOrEnable) {
                _other.transferLookedupValueOrClearOrEnable.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
            }
        }
    }

    public<_B >Actions.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Actions.Builder<_B>(_parentBuilder, this, true);
    }

    public Actions.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Actions.Builder<Void> builder() {
        return new Actions.Builder<Void>(null, null, false);
    }

    public static<_B >Actions.Builder<_B> copyOf(final Actions _other) {
        final Actions.Builder<_B> _newBuilder = new Actions.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Actions.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree transferLookedupValueOrClearOrEnablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("transferLookedupValueOrClearOrEnable"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(transferLookedupValueOrClearOrEnablePropertyTree!= null):((transferLookedupValueOrClearOrEnablePropertyTree == null)||(!transferLookedupValueOrClearOrEnablePropertyTree.isLeaf())))) {
            if (this.transferLookedupValueOrClearOrEnable == null) {
                _other.transferLookedupValueOrClearOrEnable = null;
            } else {
                _other.transferLookedupValueOrClearOrEnable = new ArrayList<Buildable>();
                for (Serializable _item: this.transferLookedupValueOrClearOrEnable) {
                    _other.transferLookedupValueOrClearOrEnable.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                }
            }
        }
    }

    public<_B >Actions.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Actions.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Actions.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Actions.Builder<_B> copyOf(final Actions _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Actions.Builder<_B> _newBuilder = new Actions.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Actions.Builder<Void> copyExcept(final Actions _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Actions.Builder<Void> copyOnly(final Actions _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Actions _storedValue;
        private List<Buildable> transferLookedupValueOrClearOrEnable;

        public Builder(final _B _parentBuilder, final Actions _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.transferLookedupValueOrClearOrEnable == null) {
                        this.transferLookedupValueOrClearOrEnable = null;
                    } else {
                        this.transferLookedupValueOrClearOrEnable = new ArrayList<Buildable>();
                        for (Serializable _item: _other.transferLookedupValueOrClearOrEnable) {
                            this.transferLookedupValueOrClearOrEnable.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Actions _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree transferLookedupValueOrClearOrEnablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("transferLookedupValueOrClearOrEnable"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(transferLookedupValueOrClearOrEnablePropertyTree!= null):((transferLookedupValueOrClearOrEnablePropertyTree == null)||(!transferLookedupValueOrClearOrEnablePropertyTree.isLeaf())))) {
                        if (_other.transferLookedupValueOrClearOrEnable == null) {
                            this.transferLookedupValueOrClearOrEnable = null;
                        } else {
                            this.transferLookedupValueOrClearOrEnable = new ArrayList<Buildable>();
                            for (Serializable _item: _other.transferLookedupValueOrClearOrEnable) {
                                this.transferLookedupValueOrClearOrEnable.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                            }
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Actions >_P init(final _P _product) {
            if (this.transferLookedupValueOrClearOrEnable!= null) {
                final List<Serializable> transferLookedupValueOrClearOrEnable = new ArrayList<Serializable>(this.transferLookedupValueOrClearOrEnable.size());
                for (Buildable _item: this.transferLookedupValueOrClearOrEnable) {
                    transferLookedupValueOrClearOrEnable.add(((Serializable) _item.build()));
                }
                _product.transferLookedupValueOrClearOrEnable = transferLookedupValueOrClearOrEnable;
            }
            return _product;
        }

        /**
         * Adds the given items to the value of "transferLookedupValueOrClearOrEnable"
         * 
         * @param transferLookedupValueOrClearOrEnable
         *     Items to add to the value of the "transferLookedupValueOrClearOrEnable" property
         */
        public Actions.Builder<_B> addTransferLookedupValueOrClearOrEnable(final Iterable<? extends Serializable> transferLookedupValueOrClearOrEnable) {
            if (transferLookedupValueOrClearOrEnable!= null) {
                if (this.transferLookedupValueOrClearOrEnable == null) {
                    this.transferLookedupValueOrClearOrEnable = new ArrayList<Buildable>();
                }
                for (Serializable _item: transferLookedupValueOrClearOrEnable) {
                    this.transferLookedupValueOrClearOrEnable.add(new Buildable.PrimitiveBuildable(_item));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "transferLookedupValueOrClearOrEnable" (any previous value will be replaced)
         * 
         * @param transferLookedupValueOrClearOrEnable
         *     New value of the "transferLookedupValueOrClearOrEnable" property.
         */
        public Actions.Builder<_B> withTransferLookedupValueOrClearOrEnable(final Iterable<? extends Serializable> transferLookedupValueOrClearOrEnable) {
            if (this.transferLookedupValueOrClearOrEnable!= null) {
                this.transferLookedupValueOrClearOrEnable.clear();
            }
            return addTransferLookedupValueOrClearOrEnable(transferLookedupValueOrClearOrEnable);
        }

        /**
         * Adds the given items to the value of "transferLookedupValueOrClearOrEnable"
         * 
         * @param transferLookedupValueOrClearOrEnable
         *     Items to add to the value of the "transferLookedupValueOrClearOrEnable" property
         */
        public Actions.Builder<_B> addTransferLookedupValueOrClearOrEnable(Serializable... transferLookedupValueOrClearOrEnable) {
            addTransferLookedupValueOrClearOrEnable(Arrays.asList(transferLookedupValueOrClearOrEnable));
            return this;
        }

        /**
         * Sets the new value of "transferLookedupValueOrClearOrEnable" (any previous value will be replaced)
         * 
         * @param transferLookedupValueOrClearOrEnable
         *     New value of the "transferLookedupValueOrClearOrEnable" property.
         */
        public Actions.Builder<_B> withTransferLookedupValueOrClearOrEnable(Serializable... transferLookedupValueOrClearOrEnable) {
            withTransferLookedupValueOrClearOrEnable(Arrays.asList(transferLookedupValueOrClearOrEnable));
            return this;
        }

        /**
         * Adds the given items to the value of "transferLookedupValue_"
         * 
         * @param transferLookedupValue_
         *     Items to add to the value of the "transferLookedupValue_" property
         */
        public Actions.Builder<_B> addTransferLookedupValue(final Iterable<? extends TransferLookedupValue> transferLookedupValue_) {
            if (transferLookedupValue_!= null) {
                if (this.transferLookedupValueOrClearOrEnable == null) {
                    this.transferLookedupValueOrClearOrEnable = new ArrayList<Buildable>();
                }
                for (TransferLookedupValue _item: transferLookedupValue_) {
                    this.transferLookedupValueOrClearOrEnable.add(new TransferLookedupValue.Builder<Actions.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Adds the given items to the value of "transferLookedupValue_"
         * 
         * @param transferLookedupValue_
         *     Items to add to the value of the "transferLookedupValue_" property
         */
        public Actions.Builder<_B> addTransferLookedupValue(TransferLookedupValue... transferLookedupValue_) {
            return addTransferLookedupValue(Arrays.asList(transferLookedupValue_));
        }

        /**
         * Returns a new builder to build an additional value of the "transferLookedupValue" property.
         * Use {@link org.nuclos.schema.layout.layoutml.TransferLookedupValue.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "transferLookedupValue" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.TransferLookedupValue.Builder#end()} to return to the current builder.
         */
        public TransferLookedupValue.Builder<? extends Actions.Builder<_B>> addTransferLookedupValue() {
            if (this.transferLookedupValueOrClearOrEnable == null) {
                this.transferLookedupValueOrClearOrEnable = new ArrayList<Buildable>();
            }
            final TransferLookedupValue.Builder<Actions.Builder<_B>> transferLookedupValue_Builder = new TransferLookedupValue.Builder<Actions.Builder<_B>>(this, null, false);
            this.transferLookedupValueOrClearOrEnable.add(transferLookedupValue_Builder);
            return transferLookedupValue_Builder;
        }

        /**
         * Adds the given items to the value of "clear_"
         * 
         * @param clear_
         *     Items to add to the value of the "clear_" property
         */
        public Actions.Builder<_B> addClear(final Iterable<? extends Clear> clear_) {
            if (clear_!= null) {
                if (this.transferLookedupValueOrClearOrEnable == null) {
                    this.transferLookedupValueOrClearOrEnable = new ArrayList<Buildable>();
                }
                for (Clear _item: clear_) {
                    this.transferLookedupValueOrClearOrEnable.add(new Clear.Builder<Actions.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Adds the given items to the value of "clear_"
         * 
         * @param clear_
         *     Items to add to the value of the "clear_" property
         */
        public Actions.Builder<_B> addClear(Clear... clear_) {
            return addClear(Arrays.asList(clear_));
        }

        /**
         * Returns a new builder to build an additional value of the "clear" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Clear.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "clear" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Clear.Builder#end()} to return to the current builder.
         */
        public Clear.Builder<? extends Actions.Builder<_B>> addClear() {
            if (this.transferLookedupValueOrClearOrEnable == null) {
                this.transferLookedupValueOrClearOrEnable = new ArrayList<Buildable>();
            }
            final Clear.Builder<Actions.Builder<_B>> clear_Builder = new Clear.Builder<Actions.Builder<_B>>(this, null, false);
            this.transferLookedupValueOrClearOrEnable.add(clear_Builder);
            return clear_Builder;
        }

        /**
         * Adds the given items to the value of "enable_"
         * 
         * @param enable_
         *     Items to add to the value of the "enable_" property
         */
        public Actions.Builder<_B> addEnable(final Iterable<? extends Enable> enable_) {
            if (enable_!= null) {
                if (this.transferLookedupValueOrClearOrEnable == null) {
                    this.transferLookedupValueOrClearOrEnable = new ArrayList<Buildable>();
                }
                for (Enable _item: enable_) {
                    this.transferLookedupValueOrClearOrEnable.add(new Enable.Builder<Actions.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Adds the given items to the value of "enable_"
         * 
         * @param enable_
         *     Items to add to the value of the "enable_" property
         */
        public Actions.Builder<_B> addEnable(Enable... enable_) {
            return addEnable(Arrays.asList(enable_));
        }

        /**
         * Returns a new builder to build an additional value of the "enable" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Enable.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "enable" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Enable.Builder#end()} to return to the current builder.
         */
        public Enable.Builder<? extends Actions.Builder<_B>> addEnable() {
            if (this.transferLookedupValueOrClearOrEnable == null) {
                this.transferLookedupValueOrClearOrEnable = new ArrayList<Buildable>();
            }
            final Enable.Builder<Actions.Builder<_B>> enable_Builder = new Enable.Builder<Actions.Builder<_B>>(this, null, false);
            this.transferLookedupValueOrClearOrEnable.add(enable_Builder);
            return enable_Builder;
        }

        /**
         * Adds the given items to the value of "refreshValuelist_"
         * 
         * @param refreshValuelist_
         *     Items to add to the value of the "refreshValuelist_" property
         */
        public Actions.Builder<_B> addRefreshValuelist(final Iterable<? extends RefreshValuelist> refreshValuelist_) {
            if (refreshValuelist_!= null) {
                if (this.transferLookedupValueOrClearOrEnable == null) {
                    this.transferLookedupValueOrClearOrEnable = new ArrayList<Buildable>();
                }
                for (RefreshValuelist _item: refreshValuelist_) {
                    this.transferLookedupValueOrClearOrEnable.add(new RefreshValuelist.Builder<Actions.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Adds the given items to the value of "refreshValuelist_"
         * 
         * @param refreshValuelist_
         *     Items to add to the value of the "refreshValuelist_" property
         */
        public Actions.Builder<_B> addRefreshValuelist(RefreshValuelist... refreshValuelist_) {
            return addRefreshValuelist(Arrays.asList(refreshValuelist_));
        }

        /**
         * Returns a new builder to build an additional value of the "refreshValuelist" property.
         * Use {@link org.nuclos.schema.layout.layoutml.RefreshValuelist.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "refreshValuelist" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.RefreshValuelist.Builder#end()} to return to the current builder.
         */
        public RefreshValuelist.Builder<? extends Actions.Builder<_B>> addRefreshValuelist() {
            if (this.transferLookedupValueOrClearOrEnable == null) {
                this.transferLookedupValueOrClearOrEnable = new ArrayList<Buildable>();
            }
            final RefreshValuelist.Builder<Actions.Builder<_B>> refreshValuelist_Builder = new RefreshValuelist.Builder<Actions.Builder<_B>>(this, null, false);
            this.transferLookedupValueOrClearOrEnable.add(refreshValuelist_Builder);
            return refreshValuelist_Builder;
        }

        /**
         * Adds the given items to the value of "reinitSubform_"
         * 
         * @param reinitSubform_
         *     Items to add to the value of the "reinitSubform_" property
         */
        public Actions.Builder<_B> addReinitSubform(final Iterable<? extends ReinitSubform> reinitSubform_) {
            if (reinitSubform_!= null) {
                if (this.transferLookedupValueOrClearOrEnable == null) {
                    this.transferLookedupValueOrClearOrEnable = new ArrayList<Buildable>();
                }
                for (ReinitSubform _item: reinitSubform_) {
                    this.transferLookedupValueOrClearOrEnable.add(new ReinitSubform.Builder<Actions.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Adds the given items to the value of "reinitSubform_"
         * 
         * @param reinitSubform_
         *     Items to add to the value of the "reinitSubform_" property
         */
        public Actions.Builder<_B> addReinitSubform(ReinitSubform... reinitSubform_) {
            return addReinitSubform(Arrays.asList(reinitSubform_));
        }

        /**
         * Returns a new builder to build an additional value of the "reinitSubform" property.
         * Use {@link org.nuclos.schema.layout.layoutml.ReinitSubform.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "reinitSubform" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.ReinitSubform.Builder#end()} to return to the current builder.
         */
        public ReinitSubform.Builder<? extends Actions.Builder<_B>> addReinitSubform() {
            if (this.transferLookedupValueOrClearOrEnable == null) {
                this.transferLookedupValueOrClearOrEnable = new ArrayList<Buildable>();
            }
            final ReinitSubform.Builder<Actions.Builder<_B>> reinitSubform_Builder = new ReinitSubform.Builder<Actions.Builder<_B>>(this, null, false);
            this.transferLookedupValueOrClearOrEnable.add(reinitSubform_Builder);
            return reinitSubform_Builder;
        }

        @Override
        public Actions build() {
            if (_storedValue == null) {
                return this.init(new Actions());
            } else {
                return ((Actions) _storedValue);
            }
        }

        public Actions.Builder<_B> copyOf(final Actions _other) {
            _other.copyTo(this);
            return this;
        }

        public Actions.Builder<_B> copyOf(final Actions.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Actions.Selector<Actions.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Actions.Select _root() {
            return new Actions.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, Actions.Selector<TRoot, TParent>> transferLookedupValueOrClearOrEnable = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.transferLookedupValueOrClearOrEnable!= null) {
                products.put("transferLookedupValueOrClearOrEnable", this.transferLookedupValueOrClearOrEnable.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, Actions.Selector<TRoot, TParent>> transferLookedupValueOrClearOrEnable() {
            return ((this.transferLookedupValueOrClearOrEnable == null)?this.transferLookedupValueOrClearOrEnable = new com.kscs.util.jaxb.Selector<TRoot, Actions.Selector<TRoot, TParent>>(this._root, this, "transferLookedupValueOrClearOrEnable"):this.transferLookedupValueOrClearOrEnable);
        }

    }

}
