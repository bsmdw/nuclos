
package org.nuclos.schema.layout.web;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;


/**
 * Base type for input component.
 * 
 * <p>Java class for web-input-component complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="web-input-component"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:org.nuclos.schema.layout.web}web-component"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="valuelist-provider" type="{urn:org.nuclos.schema.layout.web}web-valuelist-provider" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="enabled" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="editable" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="insertable" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="columns" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "web-input-component", propOrder = {
    "valuelistProvider"
})
@XmlSeeAlso({
    WebTabcontainer.class,
    WebTextfield.class,
    WebPassword.class,
    WebEmail.class,
    WebHyperlink.class,
    WebPhonenumber.class,
    WebTextarea.class,
    WebHtmlEditor.class,
    WebDatechooser.class,
    WebListofvalues.class,
    WebCombobox.class,
    WebFile.class,
    WebButton.class,
    WebCheckbox.class,
    WebMatrix.class,
    WebSubform.class,
    WebOptiongroup.class,
    WebColorchooser.class
})
public class WebInputComponent
    extends WebComponent
    implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "valuelist-provider")
    protected WebValuelistProvider valuelistProvider;
    @XmlAttribute(name = "enabled")
    protected Boolean enabled;
    @XmlAttribute(name = "editable")
    protected Boolean editable;
    @XmlAttribute(name = "insertable")
    protected Boolean insertable;
    @XmlAttribute(name = "columns")
    @XmlSchemaType(name = "anySimpleType")
    protected String columns;

    /**
     * Gets the value of the valuelistProvider property.
     * 
     * @return
     *     possible object is
     *     {@link WebValuelistProvider }
     *     
     */
    public WebValuelistProvider getValuelistProvider() {
        return valuelistProvider;
    }

    /**
     * Sets the value of the valuelistProvider property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebValuelistProvider }
     *     
     */
    public void setValuelistProvider(WebValuelistProvider value) {
        this.valuelistProvider = value;
    }

    /**
     * Gets the value of the enabled property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnabled() {
        return enabled;
    }

    /**
     * Sets the value of the enabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnabled(Boolean value) {
        this.enabled = value;
    }

    /**
     * Gets the value of the editable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEditable() {
        return editable;
    }

    /**
     * Sets the value of the editable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEditable(Boolean value) {
        this.editable = value;
    }

    /**
     * Gets the value of the insertable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isInsertable() {
        return insertable;
    }

    /**
     * Sets the value of the insertable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInsertable(Boolean value) {
        this.insertable = value;
    }

    /**
     * Gets the value of the columns property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColumns() {
        return columns;
    }

    /**
     * Sets the value of the columns property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColumns(String value) {
        this.columns = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        super.appendFields(locator, buffer, strategy);
        {
            WebValuelistProvider theValuelistProvider;
            theValuelistProvider = this.getValuelistProvider();
            strategy.appendField(locator, this, "valuelistProvider", buffer, theValuelistProvider);
        }
        {
            Boolean theEnabled;
            theEnabled = this.isEnabled();
            strategy.appendField(locator, this, "enabled", buffer, theEnabled);
        }
        {
            Boolean theEditable;
            theEditable = this.isEditable();
            strategy.appendField(locator, this, "editable", buffer, theEditable);
        }
        {
            Boolean theInsertable;
            theInsertable = this.isInsertable();
            strategy.appendField(locator, this, "insertable", buffer, theInsertable);
        }
        {
            String theColumns;
            theColumns = this.getColumns();
            strategy.appendField(locator, this, "columns", buffer, theColumns);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof WebInputComponent)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final WebInputComponent that = ((WebInputComponent) object);
        {
            WebValuelistProvider lhsValuelistProvider;
            lhsValuelistProvider = this.getValuelistProvider();
            WebValuelistProvider rhsValuelistProvider;
            rhsValuelistProvider = that.getValuelistProvider();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "valuelistProvider", lhsValuelistProvider), LocatorUtils.property(thatLocator, "valuelistProvider", rhsValuelistProvider), lhsValuelistProvider, rhsValuelistProvider)) {
                return false;
            }
        }
        {
            Boolean lhsEnabled;
            lhsEnabled = this.isEnabled();
            Boolean rhsEnabled;
            rhsEnabled = that.isEnabled();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "enabled", lhsEnabled), LocatorUtils.property(thatLocator, "enabled", rhsEnabled), lhsEnabled, rhsEnabled)) {
                return false;
            }
        }
        {
            Boolean lhsEditable;
            lhsEditable = this.isEditable();
            Boolean rhsEditable;
            rhsEditable = that.isEditable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "editable", lhsEditable), LocatorUtils.property(thatLocator, "editable", rhsEditable), lhsEditable, rhsEditable)) {
                return false;
            }
        }
        {
            Boolean lhsInsertable;
            lhsInsertable = this.isInsertable();
            Boolean rhsInsertable;
            rhsInsertable = that.isInsertable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "insertable", lhsInsertable), LocatorUtils.property(thatLocator, "insertable", rhsInsertable), lhsInsertable, rhsInsertable)) {
                return false;
            }
        }
        {
            String lhsColumns;
            lhsColumns = this.getColumns();
            String rhsColumns;
            rhsColumns = that.getColumns();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "columns", lhsColumns), LocatorUtils.property(thatLocator, "columns", rhsColumns), lhsColumns, rhsColumns)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = super.hashCode(locator, strategy);
        {
            WebValuelistProvider theValuelistProvider;
            theValuelistProvider = this.getValuelistProvider();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "valuelistProvider", theValuelistProvider), currentHashCode, theValuelistProvider);
        }
        {
            Boolean theEnabled;
            theEnabled = this.isEnabled();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "enabled", theEnabled), currentHashCode, theEnabled);
        }
        {
            Boolean theEditable;
            theEditable = this.isEditable();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "editable", theEditable), currentHashCode, theEditable);
        }
        {
            Boolean theInsertable;
            theInsertable = this.isInsertable();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "insertable", theInsertable), currentHashCode, theInsertable);
        }
        {
            String theColumns;
            theColumns = this.getColumns();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "columns", theColumns), currentHashCode, theColumns);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof WebInputComponent) {
            final WebInputComponent copy = ((WebInputComponent) draftCopy);
            if (this.valuelistProvider!= null) {
                WebValuelistProvider sourceValuelistProvider;
                sourceValuelistProvider = this.getValuelistProvider();
                WebValuelistProvider copyValuelistProvider = ((WebValuelistProvider) strategy.copy(LocatorUtils.property(locator, "valuelistProvider", sourceValuelistProvider), sourceValuelistProvider));
                copy.setValuelistProvider(copyValuelistProvider);
            } else {
                copy.valuelistProvider = null;
            }
            if (this.enabled!= null) {
                Boolean sourceEnabled;
                sourceEnabled = this.isEnabled();
                Boolean copyEnabled = ((Boolean) strategy.copy(LocatorUtils.property(locator, "enabled", sourceEnabled), sourceEnabled));
                copy.setEnabled(copyEnabled);
            } else {
                copy.enabled = null;
            }
            if (this.editable!= null) {
                Boolean sourceEditable;
                sourceEditable = this.isEditable();
                Boolean copyEditable = ((Boolean) strategy.copy(LocatorUtils.property(locator, "editable", sourceEditable), sourceEditable));
                copy.setEditable(copyEditable);
            } else {
                copy.editable = null;
            }
            if (this.insertable!= null) {
                Boolean sourceInsertable;
                sourceInsertable = this.isInsertable();
                Boolean copyInsertable = ((Boolean) strategy.copy(LocatorUtils.property(locator, "insertable", sourceInsertable), sourceInsertable));
                copy.setInsertable(copyInsertable);
            } else {
                copy.insertable = null;
            }
            if (this.columns!= null) {
                String sourceColumns;
                sourceColumns = this.getColumns();
                String copyColumns = ((String) strategy.copy(LocatorUtils.property(locator, "columns", sourceColumns), sourceColumns));
                copy.setColumns(copyColumns);
            } else {
                copy.columns = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new WebInputComponent();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebInputComponent.Builder<_B> _other) {
        super.copyTo(_other);
        _other.valuelistProvider = ((this.valuelistProvider == null)?null:this.valuelistProvider.newCopyBuilder(_other));
        _other.enabled = this.enabled;
        _other.editable = this.editable;
        _other.insertable = this.insertable;
        _other.columns = this.columns;
    }

    @Override
    public<_B >WebInputComponent.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new WebInputComponent.Builder<_B>(_parentBuilder, this, true);
    }

    @Override
    public WebInputComponent.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static WebInputComponent.Builder<Void> builder() {
        return new WebInputComponent.Builder<Void>(null, null, false);
    }

    public static<_B >WebInputComponent.Builder<_B> copyOf(final WebComponent _other) {
        final WebInputComponent.Builder<_B> _newBuilder = new WebInputComponent.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >WebInputComponent.Builder<_B> copyOf(final WebInputComponent _other) {
        final WebInputComponent.Builder<_B> _newBuilder = new WebInputComponent.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebInputComponent.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        super.copyTo(_other, _propertyTree, _propertyTreeUse);
        final PropertyTree valuelistProviderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("valuelistProvider"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuelistProviderPropertyTree!= null):((valuelistProviderPropertyTree == null)||(!valuelistProviderPropertyTree.isLeaf())))) {
            _other.valuelistProvider = ((this.valuelistProvider == null)?null:this.valuelistProvider.newCopyBuilder(_other, valuelistProviderPropertyTree, _propertyTreeUse));
        }
        final PropertyTree enabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("enabled"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(enabledPropertyTree!= null):((enabledPropertyTree == null)||(!enabledPropertyTree.isLeaf())))) {
            _other.enabled = this.enabled;
        }
        final PropertyTree editablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("editable"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(editablePropertyTree!= null):((editablePropertyTree == null)||(!editablePropertyTree.isLeaf())))) {
            _other.editable = this.editable;
        }
        final PropertyTree insertablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("insertable"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(insertablePropertyTree!= null):((insertablePropertyTree == null)||(!insertablePropertyTree.isLeaf())))) {
            _other.insertable = this.insertable;
        }
        final PropertyTree columnsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("columns"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(columnsPropertyTree!= null):((columnsPropertyTree == null)||(!columnsPropertyTree.isLeaf())))) {
            _other.columns = this.columns;
        }
    }

    @Override
    public<_B >WebInputComponent.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new WebInputComponent.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    @Override
    public WebInputComponent.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >WebInputComponent.Builder<_B> copyOf(final WebComponent _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebInputComponent.Builder<_B> _newBuilder = new WebInputComponent.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >WebInputComponent.Builder<_B> copyOf(final WebInputComponent _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebInputComponent.Builder<_B> _newBuilder = new WebInputComponent.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static WebInputComponent.Builder<Void> copyExcept(final WebComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebInputComponent.Builder<Void> copyExcept(final WebInputComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebInputComponent.Builder<Void> copyOnly(final WebComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static WebInputComponent.Builder<Void> copyOnly(final WebInputComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >
        extends WebComponent.Builder<_B>
        implements Buildable
    {

        private WebValuelistProvider.Builder<WebInputComponent.Builder<_B>> valuelistProvider;
        private Boolean enabled;
        private Boolean editable;
        private Boolean insertable;
        private String columns;

        public Builder(final _B _parentBuilder, final WebInputComponent _other, final boolean _copy) {
            super(_parentBuilder, _other, _copy);
            if (_other!= null) {
                this.valuelistProvider = ((_other.valuelistProvider == null)?null:_other.valuelistProvider.newCopyBuilder(this));
                this.enabled = _other.enabled;
                this.editable = _other.editable;
                this.insertable = _other.insertable;
                this.columns = _other.columns;
            }
        }

        public Builder(final _B _parentBuilder, final WebInputComponent _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            super(_parentBuilder, _other, _copy, _propertyTree, _propertyTreeUse);
            if (_other!= null) {
                final PropertyTree valuelistProviderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("valuelistProvider"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuelistProviderPropertyTree!= null):((valuelistProviderPropertyTree == null)||(!valuelistProviderPropertyTree.isLeaf())))) {
                    this.valuelistProvider = ((_other.valuelistProvider == null)?null:_other.valuelistProvider.newCopyBuilder(this, valuelistProviderPropertyTree, _propertyTreeUse));
                }
                final PropertyTree enabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("enabled"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(enabledPropertyTree!= null):((enabledPropertyTree == null)||(!enabledPropertyTree.isLeaf())))) {
                    this.enabled = _other.enabled;
                }
                final PropertyTree editablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("editable"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(editablePropertyTree!= null):((editablePropertyTree == null)||(!editablePropertyTree.isLeaf())))) {
                    this.editable = _other.editable;
                }
                final PropertyTree insertablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("insertable"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(insertablePropertyTree!= null):((insertablePropertyTree == null)||(!insertablePropertyTree.isLeaf())))) {
                    this.insertable = _other.insertable;
                }
                final PropertyTree columnsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("columns"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(columnsPropertyTree!= null):((columnsPropertyTree == null)||(!columnsPropertyTree.isLeaf())))) {
                    this.columns = _other.columns;
                }
            }
        }

        protected<_P extends WebInputComponent >_P init(final _P _product) {
            _product.valuelistProvider = ((this.valuelistProvider == null)?null:this.valuelistProvider.build());
            _product.enabled = this.enabled;
            _product.editable = this.editable;
            _product.insertable = this.insertable;
            _product.columns = this.columns;
            return super.init(_product);
        }

        /**
         * Sets the new value of "valuelistProvider" (any previous value will be replaced)
         * 
         * @param valuelistProvider
         *     New value of the "valuelistProvider" property.
         */
        public WebInputComponent.Builder<_B> withValuelistProvider(final WebValuelistProvider valuelistProvider) {
            this.valuelistProvider = ((valuelistProvider == null)?null:new WebValuelistProvider.Builder<WebInputComponent.Builder<_B>>(this, valuelistProvider, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "valuelistProvider" property.
         * Use {@link org.nuclos.schema.layout.web.WebValuelistProvider.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "valuelistProvider" property.
         *     Use {@link org.nuclos.schema.layout.web.WebValuelistProvider.Builder#end()} to return to the current builder.
         */
        public WebValuelistProvider.Builder<? extends WebInputComponent.Builder<_B>> withValuelistProvider() {
            if (this.valuelistProvider!= null) {
                return this.valuelistProvider;
            }
            return this.valuelistProvider = new WebValuelistProvider.Builder<WebInputComponent.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "enabled" (any previous value will be replaced)
         * 
         * @param enabled
         *     New value of the "enabled" property.
         */
        public WebInputComponent.Builder<_B> withEnabled(final Boolean enabled) {
            this.enabled = enabled;
            return this;
        }

        /**
         * Sets the new value of "editable" (any previous value will be replaced)
         * 
         * @param editable
         *     New value of the "editable" property.
         */
        public WebInputComponent.Builder<_B> withEditable(final Boolean editable) {
            this.editable = editable;
            return this;
        }

        /**
         * Sets the new value of "insertable" (any previous value will be replaced)
         * 
         * @param insertable
         *     New value of the "insertable" property.
         */
        public WebInputComponent.Builder<_B> withInsertable(final Boolean insertable) {
            this.insertable = insertable;
            return this;
        }

        /**
         * Sets the new value of "columns" (any previous value will be replaced)
         * 
         * @param columns
         *     New value of the "columns" property.
         */
        public WebInputComponent.Builder<_B> withColumns(final String columns) {
            this.columns = columns;
            return this;
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        @Override
        public WebInputComponent.Builder<_B> addAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            super.addAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        @Override
        public WebInputComponent.Builder<_B> addAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            super.addAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        @Override
        public WebInputComponent.Builder<_B> withAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            super.withAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        @Override
        public WebInputComponent.Builder<_B> withAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            super.withAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "advancedProperties" property.
         * Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "advancedProperties" property.
         *     Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         */
        @Override
        public WebAdvancedProperty.Builder<? extends WebInputComponent.Builder<_B>> addAdvancedProperties() {
            return ((WebAdvancedProperty.Builder<? extends WebInputComponent.Builder<_B>> ) super.addAdvancedProperties());
        }

        /**
         * Sets the new value of "id" (any previous value will be replaced)
         * 
         * @param id
         *     New value of the "id" property.
         */
        @Override
        public WebInputComponent.Builder<_B> withId(final String id) {
            super.withId(id);
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        @Override
        public WebInputComponent.Builder<_B> withName(final String name) {
            super.withName(name);
            return this;
        }

        /**
         * Sets the new value of "column" (any previous value will be replaced)
         * 
         * @param column
         *     New value of the "column" property.
         */
        @Override
        public WebInputComponent.Builder<_B> withColumn(final BigInteger column) {
            super.withColumn(column);
            return this;
        }

        /**
         * Sets the new value of "row" (any previous value will be replaced)
         * 
         * @param row
         *     New value of the "row" property.
         */
        @Override
        public WebInputComponent.Builder<_B> withRow(final BigInteger row) {
            super.withRow(row);
            return this;
        }

        /**
         * Sets the new value of "colspan" (any previous value will be replaced)
         * 
         * @param colspan
         *     New value of the "colspan" property.
         */
        @Override
        public WebInputComponent.Builder<_B> withColspan(final BigInteger colspan) {
            super.withColspan(colspan);
            return this;
        }

        /**
         * Sets the new value of "rowspan" (any previous value will be replaced)
         * 
         * @param rowspan
         *     New value of the "rowspan" property.
         */
        @Override
        public WebInputComponent.Builder<_B> withRowspan(final BigInteger rowspan) {
            super.withRowspan(rowspan);
            return this;
        }

        /**
         * Sets the new value of "fontSize" (any previous value will be replaced)
         * 
         * @param fontSize
         *     New value of the "fontSize" property.
         */
        @Override
        public WebInputComponent.Builder<_B> withFontSize(final String fontSize) {
            super.withFontSize(fontSize);
            return this;
        }

        /**
         * Sets the new value of "textColor" (any previous value will be replaced)
         * 
         * @param textColor
         *     New value of the "textColor" property.
         */
        @Override
        public WebInputComponent.Builder<_B> withTextColor(final String textColor) {
            super.withTextColor(textColor);
            return this;
        }

        /**
         * Sets the new value of "bold" (any previous value will be replaced)
         * 
         * @param bold
         *     New value of the "bold" property.
         */
        @Override
        public WebInputComponent.Builder<_B> withBold(final Boolean bold) {
            super.withBold(bold);
            return this;
        }

        /**
         * Sets the new value of "italic" (any previous value will be replaced)
         * 
         * @param italic
         *     New value of the "italic" property.
         */
        @Override
        public WebInputComponent.Builder<_B> withItalic(final Boolean italic) {
            super.withItalic(italic);
            return this;
        }

        /**
         * Sets the new value of "underline" (any previous value will be replaced)
         * 
         * @param underline
         *     New value of the "underline" property.
         */
        @Override
        public WebInputComponent.Builder<_B> withUnderline(final Boolean underline) {
            super.withUnderline(underline);
            return this;
        }

        /**
         * Sets the new value of "nextFocusComponent" (any previous value will be replaced)
         * 
         * @param nextFocusComponent
         *     New value of the "nextFocusComponent" property.
         */
        @Override
        public WebInputComponent.Builder<_B> withNextFocusComponent(final String nextFocusComponent) {
            super.withNextFocusComponent(nextFocusComponent);
            return this;
        }

        /**
         * Sets the new value of "nextFocusField" (any previous value will be replaced)
         * 
         * @param nextFocusField
         *     New value of the "nextFocusField" property.
         */
        @Override
        public WebInputComponent.Builder<_B> withNextFocusField(final String nextFocusField) {
            super.withNextFocusField(nextFocusField);
            return this;
        }

        /**
         * Sets the new value of "alternativeTooltip" (any previous value will be replaced)
         * 
         * @param alternativeTooltip
         *     New value of the "alternativeTooltip" property.
         */
        @Override
        public WebInputComponent.Builder<_B> withAlternativeTooltip(final String alternativeTooltip) {
            super.withAlternativeTooltip(alternativeTooltip);
            return this;
        }

        @Override
        public WebInputComponent build() {
            if (_storedValue == null) {
                return this.init(new WebInputComponent());
            } else {
                return ((WebInputComponent) _storedValue);
            }
        }

        public WebInputComponent.Builder<_B> copyOf(final WebInputComponent _other) {
            _other.copyTo(this);
            return this;
        }

        public WebInputComponent.Builder<_B> copyOf(final WebInputComponent.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends WebInputComponent.Selector<WebInputComponent.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static WebInputComponent.Select _root() {
            return new WebInputComponent.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends WebComponent.Selector<TRoot, TParent>
    {

        private WebValuelistProvider.Selector<TRoot, WebInputComponent.Selector<TRoot, TParent>> valuelistProvider = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebInputComponent.Selector<TRoot, TParent>> enabled = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebInputComponent.Selector<TRoot, TParent>> editable = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebInputComponent.Selector<TRoot, TParent>> insertable = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebInputComponent.Selector<TRoot, TParent>> columns = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.valuelistProvider!= null) {
                products.put("valuelistProvider", this.valuelistProvider.init());
            }
            if (this.enabled!= null) {
                products.put("enabled", this.enabled.init());
            }
            if (this.editable!= null) {
                products.put("editable", this.editable.init());
            }
            if (this.insertable!= null) {
                products.put("insertable", this.insertable.init());
            }
            if (this.columns!= null) {
                products.put("columns", this.columns.init());
            }
            return products;
        }

        public WebValuelistProvider.Selector<TRoot, WebInputComponent.Selector<TRoot, TParent>> valuelistProvider() {
            return ((this.valuelistProvider == null)?this.valuelistProvider = new WebValuelistProvider.Selector<TRoot, WebInputComponent.Selector<TRoot, TParent>>(this._root, this, "valuelistProvider"):this.valuelistProvider);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebInputComponent.Selector<TRoot, TParent>> enabled() {
            return ((this.enabled == null)?this.enabled = new com.kscs.util.jaxb.Selector<TRoot, WebInputComponent.Selector<TRoot, TParent>>(this._root, this, "enabled"):this.enabled);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebInputComponent.Selector<TRoot, TParent>> editable() {
            return ((this.editable == null)?this.editable = new com.kscs.util.jaxb.Selector<TRoot, WebInputComponent.Selector<TRoot, TParent>>(this._root, this, "editable"):this.editable);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebInputComponent.Selector<TRoot, TParent>> insertable() {
            return ((this.insertable == null)?this.insertable = new com.kscs.util.jaxb.Selector<TRoot, WebInputComponent.Selector<TRoot, TParent>>(this._root, this, "insertable"):this.insertable);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebInputComponent.Selector<TRoot, TParent>> columns() {
            return ((this.columns == null)?this.columns = new com.kscs.util.jaxb.Selector<TRoot, WebInputComponent.Selector<TRoot, TParent>>(this._root, this, "columns"):this.columns);
        }

    }

}
