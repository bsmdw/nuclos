
package org.nuclos.schema.layout.web;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * 
 * 				The subform component.
 * 				TODO: Which attributes are really needed?
 * 			
 * 
 * <p>Java class for web-subform complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="web-subform"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:org.nuclos.schema.layout.web}web-input-component"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="subform-columns" type="{urn:org.nuclos.schema.layout.web}web-subform-column" maxOccurs="unbounded"/&gt;
 *         &lt;element name="controller-type" type="{urn:org.nuclos.schema.layout.web}web-component"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="bo-attr-id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entity" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="autonumber-sorting" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="not-cloneable" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="multi-editable" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="toolbar-orientation" type="{urn:org.nuclos.schema.layout.web}web-toolbar-orientation" /&gt;
 *       &lt;attribute name="opaque" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="unique-mastercolumn" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="foreignkeyfield-to-parent" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="parent-subform" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="dynamic-cell-heights-default" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="ignore-sub-layout" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="new-enabled" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="edit-enabled" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="delete-enabled" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="clone-enabled" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "web-subform", propOrder = {
    "subformColumns",
    "controllerType"
})
public class WebSubform
    extends WebInputComponent
    implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "subform-columns", required = true)
    protected List<WebSubformColumn> subformColumns;
    @XmlElement(name = "controller-type", required = true)
    protected WebComponent controllerType;
    @XmlAttribute(name = "bo-attr-id")
    protected String boAttrId;
    @XmlAttribute(name = "entity", required = true)
    protected String entity;
    @XmlAttribute(name = "autonumber-sorting")
    protected Boolean autonumberSorting;
    @XmlAttribute(name = "not-cloneable")
    protected Boolean notCloneable;
    @XmlAttribute(name = "multi-editable")
    protected Boolean multiEditable;
    @XmlAttribute(name = "toolbar-orientation")
    protected WebToolbarOrientation toolbarOrientation;
    @XmlAttribute(name = "opaque")
    protected Boolean opaque;
    @XmlAttribute(name = "unique-mastercolumn")
    protected String uniqueMastercolumn;
    @XmlAttribute(name = "foreignkeyfield-to-parent")
    protected String foreignkeyfieldToParent;
    @XmlAttribute(name = "parent-subform")
    protected String parentSubform;
    @XmlAttribute(name = "dynamic-cell-heights-default")
    protected Boolean dynamicCellHeightsDefault;
    @XmlAttribute(name = "ignore-sub-layout")
    protected Boolean ignoreSubLayout;
    @XmlAttribute(name = "new-enabled")
    protected Boolean newEnabled;
    @XmlAttribute(name = "edit-enabled")
    protected Boolean editEnabled;
    @XmlAttribute(name = "delete-enabled")
    protected Boolean deleteEnabled;
    @XmlAttribute(name = "clone-enabled")
    protected Boolean cloneEnabled;

    /**
     * Gets the value of the subformColumns property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subformColumns property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubformColumns().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WebSubformColumn }
     * 
     * 
     */
    public List<WebSubformColumn> getSubformColumns() {
        if (subformColumns == null) {
            subformColumns = new ArrayList<WebSubformColumn>();
        }
        return this.subformColumns;
    }

    /**
     * Gets the value of the controllerType property.
     * 
     * @return
     *     possible object is
     *     {@link WebComponent }
     *     
     */
    public WebComponent getControllerType() {
        return controllerType;
    }

    /**
     * Sets the value of the controllerType property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebComponent }
     *     
     */
    public void setControllerType(WebComponent value) {
        this.controllerType = value;
    }

    /**
     * Gets the value of the boAttrId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoAttrId() {
        return boAttrId;
    }

    /**
     * Sets the value of the boAttrId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoAttrId(String value) {
        this.boAttrId = value;
    }

    /**
     * Gets the value of the entity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntity() {
        return entity;
    }

    /**
     * Sets the value of the entity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntity(String value) {
        this.entity = value;
    }

    /**
     * Gets the value of the autonumberSorting property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAutonumberSorting() {
        return autonumberSorting;
    }

    /**
     * Sets the value of the autonumberSorting property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutonumberSorting(Boolean value) {
        this.autonumberSorting = value;
    }

    /**
     * Gets the value of the notCloneable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNotCloneable() {
        return notCloneable;
    }

    /**
     * Sets the value of the notCloneable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNotCloneable(Boolean value) {
        this.notCloneable = value;
    }

    /**
     * Gets the value of the multiEditable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMultiEditable() {
        return multiEditable;
    }

    /**
     * Sets the value of the multiEditable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMultiEditable(Boolean value) {
        this.multiEditable = value;
    }

    /**
     * Gets the value of the toolbarOrientation property.
     * 
     * @return
     *     possible object is
     *     {@link WebToolbarOrientation }
     *     
     */
    public WebToolbarOrientation getToolbarOrientation() {
        return toolbarOrientation;
    }

    /**
     * Sets the value of the toolbarOrientation property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebToolbarOrientation }
     *     
     */
    public void setToolbarOrientation(WebToolbarOrientation value) {
        this.toolbarOrientation = value;
    }

    /**
     * Gets the value of the opaque property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOpaque() {
        return opaque;
    }

    /**
     * Sets the value of the opaque property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOpaque(Boolean value) {
        this.opaque = value;
    }

    /**
     * Gets the value of the uniqueMastercolumn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniqueMastercolumn() {
        return uniqueMastercolumn;
    }

    /**
     * Sets the value of the uniqueMastercolumn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniqueMastercolumn(String value) {
        this.uniqueMastercolumn = value;
    }

    /**
     * Gets the value of the foreignkeyfieldToParent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForeignkeyfieldToParent() {
        return foreignkeyfieldToParent;
    }

    /**
     * Sets the value of the foreignkeyfieldToParent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForeignkeyfieldToParent(String value) {
        this.foreignkeyfieldToParent = value;
    }

    /**
     * Gets the value of the parentSubform property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentSubform() {
        return parentSubform;
    }

    /**
     * Sets the value of the parentSubform property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentSubform(String value) {
        this.parentSubform = value;
    }

    /**
     * Gets the value of the dynamicCellHeightsDefault property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDynamicCellHeightsDefault() {
        return dynamicCellHeightsDefault;
    }

    /**
     * Sets the value of the dynamicCellHeightsDefault property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDynamicCellHeightsDefault(Boolean value) {
        this.dynamicCellHeightsDefault = value;
    }

    /**
     * Gets the value of the ignoreSubLayout property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIgnoreSubLayout() {
        return ignoreSubLayout;
    }

    /**
     * Sets the value of the ignoreSubLayout property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnoreSubLayout(Boolean value) {
        this.ignoreSubLayout = value;
    }

    /**
     * Gets the value of the newEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNewEnabled() {
        return newEnabled;
    }

    /**
     * Sets the value of the newEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNewEnabled(Boolean value) {
        this.newEnabled = value;
    }

    /**
     * Gets the value of the editEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEditEnabled() {
        return editEnabled;
    }

    /**
     * Sets the value of the editEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEditEnabled(Boolean value) {
        this.editEnabled = value;
    }

    /**
     * Gets the value of the deleteEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDeleteEnabled() {
        return deleteEnabled;
    }

    /**
     * Sets the value of the deleteEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDeleteEnabled(Boolean value) {
        this.deleteEnabled = value;
    }

    /**
     * Gets the value of the cloneEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCloneEnabled() {
        return cloneEnabled;
    }

    /**
     * Sets the value of the cloneEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCloneEnabled(Boolean value) {
        this.cloneEnabled = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        super.appendFields(locator, buffer, strategy);
        {
            List<WebSubformColumn> theSubformColumns;
            theSubformColumns = (((this.subformColumns!= null)&&(!this.subformColumns.isEmpty()))?this.getSubformColumns():null);
            strategy.appendField(locator, this, "subformColumns", buffer, theSubformColumns);
        }
        {
            WebComponent theControllerType;
            theControllerType = this.getControllerType();
            strategy.appendField(locator, this, "controllerType", buffer, theControllerType);
        }
        {
            String theBoAttrId;
            theBoAttrId = this.getBoAttrId();
            strategy.appendField(locator, this, "boAttrId", buffer, theBoAttrId);
        }
        {
            String theEntity;
            theEntity = this.getEntity();
            strategy.appendField(locator, this, "entity", buffer, theEntity);
        }
        {
            Boolean theAutonumberSorting;
            theAutonumberSorting = this.isAutonumberSorting();
            strategy.appendField(locator, this, "autonumberSorting", buffer, theAutonumberSorting);
        }
        {
            Boolean theNotCloneable;
            theNotCloneable = this.isNotCloneable();
            strategy.appendField(locator, this, "notCloneable", buffer, theNotCloneable);
        }
        {
            Boolean theMultiEditable;
            theMultiEditable = this.isMultiEditable();
            strategy.appendField(locator, this, "multiEditable", buffer, theMultiEditable);
        }
        {
            WebToolbarOrientation theToolbarOrientation;
            theToolbarOrientation = this.getToolbarOrientation();
            strategy.appendField(locator, this, "toolbarOrientation", buffer, theToolbarOrientation);
        }
        {
            Boolean theOpaque;
            theOpaque = this.isOpaque();
            strategy.appendField(locator, this, "opaque", buffer, theOpaque);
        }
        {
            String theUniqueMastercolumn;
            theUniqueMastercolumn = this.getUniqueMastercolumn();
            strategy.appendField(locator, this, "uniqueMastercolumn", buffer, theUniqueMastercolumn);
        }
        {
            String theForeignkeyfieldToParent;
            theForeignkeyfieldToParent = this.getForeignkeyfieldToParent();
            strategy.appendField(locator, this, "foreignkeyfieldToParent", buffer, theForeignkeyfieldToParent);
        }
        {
            String theParentSubform;
            theParentSubform = this.getParentSubform();
            strategy.appendField(locator, this, "parentSubform", buffer, theParentSubform);
        }
        {
            Boolean theDynamicCellHeightsDefault;
            theDynamicCellHeightsDefault = this.isDynamicCellHeightsDefault();
            strategy.appendField(locator, this, "dynamicCellHeightsDefault", buffer, theDynamicCellHeightsDefault);
        }
        {
            Boolean theIgnoreSubLayout;
            theIgnoreSubLayout = this.isIgnoreSubLayout();
            strategy.appendField(locator, this, "ignoreSubLayout", buffer, theIgnoreSubLayout);
        }
        {
            Boolean theNewEnabled;
            theNewEnabled = this.isNewEnabled();
            strategy.appendField(locator, this, "newEnabled", buffer, theNewEnabled);
        }
        {
            Boolean theEditEnabled;
            theEditEnabled = this.isEditEnabled();
            strategy.appendField(locator, this, "editEnabled", buffer, theEditEnabled);
        }
        {
            Boolean theDeleteEnabled;
            theDeleteEnabled = this.isDeleteEnabled();
            strategy.appendField(locator, this, "deleteEnabled", buffer, theDeleteEnabled);
        }
        {
            Boolean theCloneEnabled;
            theCloneEnabled = this.isCloneEnabled();
            strategy.appendField(locator, this, "cloneEnabled", buffer, theCloneEnabled);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof WebSubform)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final WebSubform that = ((WebSubform) object);
        {
            List<WebSubformColumn> lhsSubformColumns;
            lhsSubformColumns = (((this.subformColumns!= null)&&(!this.subformColumns.isEmpty()))?this.getSubformColumns():null);
            List<WebSubformColumn> rhsSubformColumns;
            rhsSubformColumns = (((that.subformColumns!= null)&&(!that.subformColumns.isEmpty()))?that.getSubformColumns():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "subformColumns", lhsSubformColumns), LocatorUtils.property(thatLocator, "subformColumns", rhsSubformColumns), lhsSubformColumns, rhsSubformColumns)) {
                return false;
            }
        }
        {
            WebComponent lhsControllerType;
            lhsControllerType = this.getControllerType();
            WebComponent rhsControllerType;
            rhsControllerType = that.getControllerType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "controllerType", lhsControllerType), LocatorUtils.property(thatLocator, "controllerType", rhsControllerType), lhsControllerType, rhsControllerType)) {
                return false;
            }
        }
        {
            String lhsBoAttrId;
            lhsBoAttrId = this.getBoAttrId();
            String rhsBoAttrId;
            rhsBoAttrId = that.getBoAttrId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "boAttrId", lhsBoAttrId), LocatorUtils.property(thatLocator, "boAttrId", rhsBoAttrId), lhsBoAttrId, rhsBoAttrId)) {
                return false;
            }
        }
        {
            String lhsEntity;
            lhsEntity = this.getEntity();
            String rhsEntity;
            rhsEntity = that.getEntity();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entity", lhsEntity), LocatorUtils.property(thatLocator, "entity", rhsEntity), lhsEntity, rhsEntity)) {
                return false;
            }
        }
        {
            Boolean lhsAutonumberSorting;
            lhsAutonumberSorting = this.isAutonumberSorting();
            Boolean rhsAutonumberSorting;
            rhsAutonumberSorting = that.isAutonumberSorting();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "autonumberSorting", lhsAutonumberSorting), LocatorUtils.property(thatLocator, "autonumberSorting", rhsAutonumberSorting), lhsAutonumberSorting, rhsAutonumberSorting)) {
                return false;
            }
        }
        {
            Boolean lhsNotCloneable;
            lhsNotCloneable = this.isNotCloneable();
            Boolean rhsNotCloneable;
            rhsNotCloneable = that.isNotCloneable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "notCloneable", lhsNotCloneable), LocatorUtils.property(thatLocator, "notCloneable", rhsNotCloneable), lhsNotCloneable, rhsNotCloneable)) {
                return false;
            }
        }
        {
            Boolean lhsMultiEditable;
            lhsMultiEditable = this.isMultiEditable();
            Boolean rhsMultiEditable;
            rhsMultiEditable = that.isMultiEditable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "multiEditable", lhsMultiEditable), LocatorUtils.property(thatLocator, "multiEditable", rhsMultiEditable), lhsMultiEditable, rhsMultiEditable)) {
                return false;
            }
        }
        {
            WebToolbarOrientation lhsToolbarOrientation;
            lhsToolbarOrientation = this.getToolbarOrientation();
            WebToolbarOrientation rhsToolbarOrientation;
            rhsToolbarOrientation = that.getToolbarOrientation();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "toolbarOrientation", lhsToolbarOrientation), LocatorUtils.property(thatLocator, "toolbarOrientation", rhsToolbarOrientation), lhsToolbarOrientation, rhsToolbarOrientation)) {
                return false;
            }
        }
        {
            Boolean lhsOpaque;
            lhsOpaque = this.isOpaque();
            Boolean rhsOpaque;
            rhsOpaque = that.isOpaque();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "opaque", lhsOpaque), LocatorUtils.property(thatLocator, "opaque", rhsOpaque), lhsOpaque, rhsOpaque)) {
                return false;
            }
        }
        {
            String lhsUniqueMastercolumn;
            lhsUniqueMastercolumn = this.getUniqueMastercolumn();
            String rhsUniqueMastercolumn;
            rhsUniqueMastercolumn = that.getUniqueMastercolumn();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "uniqueMastercolumn", lhsUniqueMastercolumn), LocatorUtils.property(thatLocator, "uniqueMastercolumn", rhsUniqueMastercolumn), lhsUniqueMastercolumn, rhsUniqueMastercolumn)) {
                return false;
            }
        }
        {
            String lhsForeignkeyfieldToParent;
            lhsForeignkeyfieldToParent = this.getForeignkeyfieldToParent();
            String rhsForeignkeyfieldToParent;
            rhsForeignkeyfieldToParent = that.getForeignkeyfieldToParent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "foreignkeyfieldToParent", lhsForeignkeyfieldToParent), LocatorUtils.property(thatLocator, "foreignkeyfieldToParent", rhsForeignkeyfieldToParent), lhsForeignkeyfieldToParent, rhsForeignkeyfieldToParent)) {
                return false;
            }
        }
        {
            String lhsParentSubform;
            lhsParentSubform = this.getParentSubform();
            String rhsParentSubform;
            rhsParentSubform = that.getParentSubform();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "parentSubform", lhsParentSubform), LocatorUtils.property(thatLocator, "parentSubform", rhsParentSubform), lhsParentSubform, rhsParentSubform)) {
                return false;
            }
        }
        {
            Boolean lhsDynamicCellHeightsDefault;
            lhsDynamicCellHeightsDefault = this.isDynamicCellHeightsDefault();
            Boolean rhsDynamicCellHeightsDefault;
            rhsDynamicCellHeightsDefault = that.isDynamicCellHeightsDefault();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dynamicCellHeightsDefault", lhsDynamicCellHeightsDefault), LocatorUtils.property(thatLocator, "dynamicCellHeightsDefault", rhsDynamicCellHeightsDefault), lhsDynamicCellHeightsDefault, rhsDynamicCellHeightsDefault)) {
                return false;
            }
        }
        {
            Boolean lhsIgnoreSubLayout;
            lhsIgnoreSubLayout = this.isIgnoreSubLayout();
            Boolean rhsIgnoreSubLayout;
            rhsIgnoreSubLayout = that.isIgnoreSubLayout();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ignoreSubLayout", lhsIgnoreSubLayout), LocatorUtils.property(thatLocator, "ignoreSubLayout", rhsIgnoreSubLayout), lhsIgnoreSubLayout, rhsIgnoreSubLayout)) {
                return false;
            }
        }
        {
            Boolean lhsNewEnabled;
            lhsNewEnabled = this.isNewEnabled();
            Boolean rhsNewEnabled;
            rhsNewEnabled = that.isNewEnabled();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "newEnabled", lhsNewEnabled), LocatorUtils.property(thatLocator, "newEnabled", rhsNewEnabled), lhsNewEnabled, rhsNewEnabled)) {
                return false;
            }
        }
        {
            Boolean lhsEditEnabled;
            lhsEditEnabled = this.isEditEnabled();
            Boolean rhsEditEnabled;
            rhsEditEnabled = that.isEditEnabled();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "editEnabled", lhsEditEnabled), LocatorUtils.property(thatLocator, "editEnabled", rhsEditEnabled), lhsEditEnabled, rhsEditEnabled)) {
                return false;
            }
        }
        {
            Boolean lhsDeleteEnabled;
            lhsDeleteEnabled = this.isDeleteEnabled();
            Boolean rhsDeleteEnabled;
            rhsDeleteEnabled = that.isDeleteEnabled();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "deleteEnabled", lhsDeleteEnabled), LocatorUtils.property(thatLocator, "deleteEnabled", rhsDeleteEnabled), lhsDeleteEnabled, rhsDeleteEnabled)) {
                return false;
            }
        }
        {
            Boolean lhsCloneEnabled;
            lhsCloneEnabled = this.isCloneEnabled();
            Boolean rhsCloneEnabled;
            rhsCloneEnabled = that.isCloneEnabled();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "cloneEnabled", lhsCloneEnabled), LocatorUtils.property(thatLocator, "cloneEnabled", rhsCloneEnabled), lhsCloneEnabled, rhsCloneEnabled)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = super.hashCode(locator, strategy);
        {
            List<WebSubformColumn> theSubformColumns;
            theSubformColumns = (((this.subformColumns!= null)&&(!this.subformColumns.isEmpty()))?this.getSubformColumns():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "subformColumns", theSubformColumns), currentHashCode, theSubformColumns);
        }
        {
            WebComponent theControllerType;
            theControllerType = this.getControllerType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "controllerType", theControllerType), currentHashCode, theControllerType);
        }
        {
            String theBoAttrId;
            theBoAttrId = this.getBoAttrId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "boAttrId", theBoAttrId), currentHashCode, theBoAttrId);
        }
        {
            String theEntity;
            theEntity = this.getEntity();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entity", theEntity), currentHashCode, theEntity);
        }
        {
            Boolean theAutonumberSorting;
            theAutonumberSorting = this.isAutonumberSorting();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "autonumberSorting", theAutonumberSorting), currentHashCode, theAutonumberSorting);
        }
        {
            Boolean theNotCloneable;
            theNotCloneable = this.isNotCloneable();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "notCloneable", theNotCloneable), currentHashCode, theNotCloneable);
        }
        {
            Boolean theMultiEditable;
            theMultiEditable = this.isMultiEditable();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "multiEditable", theMultiEditable), currentHashCode, theMultiEditable);
        }
        {
            WebToolbarOrientation theToolbarOrientation;
            theToolbarOrientation = this.getToolbarOrientation();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "toolbarOrientation", theToolbarOrientation), currentHashCode, theToolbarOrientation);
        }
        {
            Boolean theOpaque;
            theOpaque = this.isOpaque();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "opaque", theOpaque), currentHashCode, theOpaque);
        }
        {
            String theUniqueMastercolumn;
            theUniqueMastercolumn = this.getUniqueMastercolumn();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "uniqueMastercolumn", theUniqueMastercolumn), currentHashCode, theUniqueMastercolumn);
        }
        {
            String theForeignkeyfieldToParent;
            theForeignkeyfieldToParent = this.getForeignkeyfieldToParent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "foreignkeyfieldToParent", theForeignkeyfieldToParent), currentHashCode, theForeignkeyfieldToParent);
        }
        {
            String theParentSubform;
            theParentSubform = this.getParentSubform();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "parentSubform", theParentSubform), currentHashCode, theParentSubform);
        }
        {
            Boolean theDynamicCellHeightsDefault;
            theDynamicCellHeightsDefault = this.isDynamicCellHeightsDefault();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dynamicCellHeightsDefault", theDynamicCellHeightsDefault), currentHashCode, theDynamicCellHeightsDefault);
        }
        {
            Boolean theIgnoreSubLayout;
            theIgnoreSubLayout = this.isIgnoreSubLayout();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ignoreSubLayout", theIgnoreSubLayout), currentHashCode, theIgnoreSubLayout);
        }
        {
            Boolean theNewEnabled;
            theNewEnabled = this.isNewEnabled();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "newEnabled", theNewEnabled), currentHashCode, theNewEnabled);
        }
        {
            Boolean theEditEnabled;
            theEditEnabled = this.isEditEnabled();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "editEnabled", theEditEnabled), currentHashCode, theEditEnabled);
        }
        {
            Boolean theDeleteEnabled;
            theDeleteEnabled = this.isDeleteEnabled();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "deleteEnabled", theDeleteEnabled), currentHashCode, theDeleteEnabled);
        }
        {
            Boolean theCloneEnabled;
            theCloneEnabled = this.isCloneEnabled();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "cloneEnabled", theCloneEnabled), currentHashCode, theCloneEnabled);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof WebSubform) {
            final WebSubform copy = ((WebSubform) draftCopy);
            if ((this.subformColumns!= null)&&(!this.subformColumns.isEmpty())) {
                List<WebSubformColumn> sourceSubformColumns;
                sourceSubformColumns = (((this.subformColumns!= null)&&(!this.subformColumns.isEmpty()))?this.getSubformColumns():null);
                @SuppressWarnings("unchecked")
                List<WebSubformColumn> copySubformColumns = ((List<WebSubformColumn> ) strategy.copy(LocatorUtils.property(locator, "subformColumns", sourceSubformColumns), sourceSubformColumns));
                copy.subformColumns = null;
                if (copySubformColumns!= null) {
                    List<WebSubformColumn> uniqueSubformColumnsl = copy.getSubformColumns();
                    uniqueSubformColumnsl.addAll(copySubformColumns);
                }
            } else {
                copy.subformColumns = null;
            }
            if (this.controllerType!= null) {
                WebComponent sourceControllerType;
                sourceControllerType = this.getControllerType();
                WebComponent copyControllerType = ((WebComponent) strategy.copy(LocatorUtils.property(locator, "controllerType", sourceControllerType), sourceControllerType));
                copy.setControllerType(copyControllerType);
            } else {
                copy.controllerType = null;
            }
            if (this.boAttrId!= null) {
                String sourceBoAttrId;
                sourceBoAttrId = this.getBoAttrId();
                String copyBoAttrId = ((String) strategy.copy(LocatorUtils.property(locator, "boAttrId", sourceBoAttrId), sourceBoAttrId));
                copy.setBoAttrId(copyBoAttrId);
            } else {
                copy.boAttrId = null;
            }
            if (this.entity!= null) {
                String sourceEntity;
                sourceEntity = this.getEntity();
                String copyEntity = ((String) strategy.copy(LocatorUtils.property(locator, "entity", sourceEntity), sourceEntity));
                copy.setEntity(copyEntity);
            } else {
                copy.entity = null;
            }
            if (this.autonumberSorting!= null) {
                Boolean sourceAutonumberSorting;
                sourceAutonumberSorting = this.isAutonumberSorting();
                Boolean copyAutonumberSorting = ((Boolean) strategy.copy(LocatorUtils.property(locator, "autonumberSorting", sourceAutonumberSorting), sourceAutonumberSorting));
                copy.setAutonumberSorting(copyAutonumberSorting);
            } else {
                copy.autonumberSorting = null;
            }
            if (this.notCloneable!= null) {
                Boolean sourceNotCloneable;
                sourceNotCloneable = this.isNotCloneable();
                Boolean copyNotCloneable = ((Boolean) strategy.copy(LocatorUtils.property(locator, "notCloneable", sourceNotCloneable), sourceNotCloneable));
                copy.setNotCloneable(copyNotCloneable);
            } else {
                copy.notCloneable = null;
            }
            if (this.multiEditable!= null) {
                Boolean sourceMultiEditable;
                sourceMultiEditable = this.isMultiEditable();
                Boolean copyMultiEditable = ((Boolean) strategy.copy(LocatorUtils.property(locator, "multiEditable", sourceMultiEditable), sourceMultiEditable));
                copy.setMultiEditable(copyMultiEditable);
            } else {
                copy.multiEditable = null;
            }
            if (this.toolbarOrientation!= null) {
                WebToolbarOrientation sourceToolbarOrientation;
                sourceToolbarOrientation = this.getToolbarOrientation();
                WebToolbarOrientation copyToolbarOrientation = ((WebToolbarOrientation) strategy.copy(LocatorUtils.property(locator, "toolbarOrientation", sourceToolbarOrientation), sourceToolbarOrientation));
                copy.setToolbarOrientation(copyToolbarOrientation);
            } else {
                copy.toolbarOrientation = null;
            }
            if (this.opaque!= null) {
                Boolean sourceOpaque;
                sourceOpaque = this.isOpaque();
                Boolean copyOpaque = ((Boolean) strategy.copy(LocatorUtils.property(locator, "opaque", sourceOpaque), sourceOpaque));
                copy.setOpaque(copyOpaque);
            } else {
                copy.opaque = null;
            }
            if (this.uniqueMastercolumn!= null) {
                String sourceUniqueMastercolumn;
                sourceUniqueMastercolumn = this.getUniqueMastercolumn();
                String copyUniqueMastercolumn = ((String) strategy.copy(LocatorUtils.property(locator, "uniqueMastercolumn", sourceUniqueMastercolumn), sourceUniqueMastercolumn));
                copy.setUniqueMastercolumn(copyUniqueMastercolumn);
            } else {
                copy.uniqueMastercolumn = null;
            }
            if (this.foreignkeyfieldToParent!= null) {
                String sourceForeignkeyfieldToParent;
                sourceForeignkeyfieldToParent = this.getForeignkeyfieldToParent();
                String copyForeignkeyfieldToParent = ((String) strategy.copy(LocatorUtils.property(locator, "foreignkeyfieldToParent", sourceForeignkeyfieldToParent), sourceForeignkeyfieldToParent));
                copy.setForeignkeyfieldToParent(copyForeignkeyfieldToParent);
            } else {
                copy.foreignkeyfieldToParent = null;
            }
            if (this.parentSubform!= null) {
                String sourceParentSubform;
                sourceParentSubform = this.getParentSubform();
                String copyParentSubform = ((String) strategy.copy(LocatorUtils.property(locator, "parentSubform", sourceParentSubform), sourceParentSubform));
                copy.setParentSubform(copyParentSubform);
            } else {
                copy.parentSubform = null;
            }
            if (this.dynamicCellHeightsDefault!= null) {
                Boolean sourceDynamicCellHeightsDefault;
                sourceDynamicCellHeightsDefault = this.isDynamicCellHeightsDefault();
                Boolean copyDynamicCellHeightsDefault = ((Boolean) strategy.copy(LocatorUtils.property(locator, "dynamicCellHeightsDefault", sourceDynamicCellHeightsDefault), sourceDynamicCellHeightsDefault));
                copy.setDynamicCellHeightsDefault(copyDynamicCellHeightsDefault);
            } else {
                copy.dynamicCellHeightsDefault = null;
            }
            if (this.ignoreSubLayout!= null) {
                Boolean sourceIgnoreSubLayout;
                sourceIgnoreSubLayout = this.isIgnoreSubLayout();
                Boolean copyIgnoreSubLayout = ((Boolean) strategy.copy(LocatorUtils.property(locator, "ignoreSubLayout", sourceIgnoreSubLayout), sourceIgnoreSubLayout));
                copy.setIgnoreSubLayout(copyIgnoreSubLayout);
            } else {
                copy.ignoreSubLayout = null;
            }
            if (this.newEnabled!= null) {
                Boolean sourceNewEnabled;
                sourceNewEnabled = this.isNewEnabled();
                Boolean copyNewEnabled = ((Boolean) strategy.copy(LocatorUtils.property(locator, "newEnabled", sourceNewEnabled), sourceNewEnabled));
                copy.setNewEnabled(copyNewEnabled);
            } else {
                copy.newEnabled = null;
            }
            if (this.editEnabled!= null) {
                Boolean sourceEditEnabled;
                sourceEditEnabled = this.isEditEnabled();
                Boolean copyEditEnabled = ((Boolean) strategy.copy(LocatorUtils.property(locator, "editEnabled", sourceEditEnabled), sourceEditEnabled));
                copy.setEditEnabled(copyEditEnabled);
            } else {
                copy.editEnabled = null;
            }
            if (this.deleteEnabled!= null) {
                Boolean sourceDeleteEnabled;
                sourceDeleteEnabled = this.isDeleteEnabled();
                Boolean copyDeleteEnabled = ((Boolean) strategy.copy(LocatorUtils.property(locator, "deleteEnabled", sourceDeleteEnabled), sourceDeleteEnabled));
                copy.setDeleteEnabled(copyDeleteEnabled);
            } else {
                copy.deleteEnabled = null;
            }
            if (this.cloneEnabled!= null) {
                Boolean sourceCloneEnabled;
                sourceCloneEnabled = this.isCloneEnabled();
                Boolean copyCloneEnabled = ((Boolean) strategy.copy(LocatorUtils.property(locator, "cloneEnabled", sourceCloneEnabled), sourceCloneEnabled));
                copy.setCloneEnabled(copyCloneEnabled);
            } else {
                copy.cloneEnabled = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new WebSubform();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebSubform.Builder<_B> _other) {
        super.copyTo(_other);
        if (this.subformColumns == null) {
            _other.subformColumns = null;
        } else {
            _other.subformColumns = new ArrayList<WebSubformColumn.Builder<WebSubform.Builder<_B>>>();
            for (WebSubformColumn _item: this.subformColumns) {
                _other.subformColumns.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.controllerType = ((this.controllerType == null)?null:this.controllerType.newCopyBuilder(_other));
        _other.boAttrId = this.boAttrId;
        _other.entity = this.entity;
        _other.autonumberSorting = this.autonumberSorting;
        _other.notCloneable = this.notCloneable;
        _other.multiEditable = this.multiEditable;
        _other.toolbarOrientation = this.toolbarOrientation;
        _other.opaque = this.opaque;
        _other.uniqueMastercolumn = this.uniqueMastercolumn;
        _other.foreignkeyfieldToParent = this.foreignkeyfieldToParent;
        _other.parentSubform = this.parentSubform;
        _other.dynamicCellHeightsDefault = this.dynamicCellHeightsDefault;
        _other.ignoreSubLayout = this.ignoreSubLayout;
        _other.newEnabled = this.newEnabled;
        _other.editEnabled = this.editEnabled;
        _other.deleteEnabled = this.deleteEnabled;
        _other.cloneEnabled = this.cloneEnabled;
    }

    @Override
    public<_B >WebSubform.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new WebSubform.Builder<_B>(_parentBuilder, this, true);
    }

    @Override
    public WebSubform.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static WebSubform.Builder<Void> builder() {
        return new WebSubform.Builder<Void>(null, null, false);
    }

    public static<_B >WebSubform.Builder<_B> copyOf(final WebComponent _other) {
        final WebSubform.Builder<_B> _newBuilder = new WebSubform.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >WebSubform.Builder<_B> copyOf(final WebInputComponent _other) {
        final WebSubform.Builder<_B> _newBuilder = new WebSubform.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >WebSubform.Builder<_B> copyOf(final WebSubform _other) {
        final WebSubform.Builder<_B> _newBuilder = new WebSubform.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebSubform.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        super.copyTo(_other, _propertyTree, _propertyTreeUse);
        final PropertyTree subformColumnsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("subformColumns"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(subformColumnsPropertyTree!= null):((subformColumnsPropertyTree == null)||(!subformColumnsPropertyTree.isLeaf())))) {
            if (this.subformColumns == null) {
                _other.subformColumns = null;
            } else {
                _other.subformColumns = new ArrayList<WebSubformColumn.Builder<WebSubform.Builder<_B>>>();
                for (WebSubformColumn _item: this.subformColumns) {
                    _other.subformColumns.add(((_item == null)?null:_item.newCopyBuilder(_other, subformColumnsPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree controllerTypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("controllerType"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(controllerTypePropertyTree!= null):((controllerTypePropertyTree == null)||(!controllerTypePropertyTree.isLeaf())))) {
            _other.controllerType = ((this.controllerType == null)?null:this.controllerType.newCopyBuilder(_other, controllerTypePropertyTree, _propertyTreeUse));
        }
        final PropertyTree boAttrIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boAttrId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boAttrIdPropertyTree!= null):((boAttrIdPropertyTree == null)||(!boAttrIdPropertyTree.isLeaf())))) {
            _other.boAttrId = this.boAttrId;
        }
        final PropertyTree entityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entity"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityPropertyTree!= null):((entityPropertyTree == null)||(!entityPropertyTree.isLeaf())))) {
            _other.entity = this.entity;
        }
        final PropertyTree autonumberSortingPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("autonumberSorting"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(autonumberSortingPropertyTree!= null):((autonumberSortingPropertyTree == null)||(!autonumberSortingPropertyTree.isLeaf())))) {
            _other.autonumberSorting = this.autonumberSorting;
        }
        final PropertyTree notCloneablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("notCloneable"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(notCloneablePropertyTree!= null):((notCloneablePropertyTree == null)||(!notCloneablePropertyTree.isLeaf())))) {
            _other.notCloneable = this.notCloneable;
        }
        final PropertyTree multiEditablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("multiEditable"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(multiEditablePropertyTree!= null):((multiEditablePropertyTree == null)||(!multiEditablePropertyTree.isLeaf())))) {
            _other.multiEditable = this.multiEditable;
        }
        final PropertyTree toolbarOrientationPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("toolbarOrientation"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(toolbarOrientationPropertyTree!= null):((toolbarOrientationPropertyTree == null)||(!toolbarOrientationPropertyTree.isLeaf())))) {
            _other.toolbarOrientation = this.toolbarOrientation;
        }
        final PropertyTree opaquePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("opaque"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(opaquePropertyTree!= null):((opaquePropertyTree == null)||(!opaquePropertyTree.isLeaf())))) {
            _other.opaque = this.opaque;
        }
        final PropertyTree uniqueMastercolumnPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("uniqueMastercolumn"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(uniqueMastercolumnPropertyTree!= null):((uniqueMastercolumnPropertyTree == null)||(!uniqueMastercolumnPropertyTree.isLeaf())))) {
            _other.uniqueMastercolumn = this.uniqueMastercolumn;
        }
        final PropertyTree foreignkeyfieldToParentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("foreignkeyfieldToParent"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(foreignkeyfieldToParentPropertyTree!= null):((foreignkeyfieldToParentPropertyTree == null)||(!foreignkeyfieldToParentPropertyTree.isLeaf())))) {
            _other.foreignkeyfieldToParent = this.foreignkeyfieldToParent;
        }
        final PropertyTree parentSubformPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("parentSubform"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(parentSubformPropertyTree!= null):((parentSubformPropertyTree == null)||(!parentSubformPropertyTree.isLeaf())))) {
            _other.parentSubform = this.parentSubform;
        }
        final PropertyTree dynamicCellHeightsDefaultPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dynamicCellHeightsDefault"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dynamicCellHeightsDefaultPropertyTree!= null):((dynamicCellHeightsDefaultPropertyTree == null)||(!dynamicCellHeightsDefaultPropertyTree.isLeaf())))) {
            _other.dynamicCellHeightsDefault = this.dynamicCellHeightsDefault;
        }
        final PropertyTree ignoreSubLayoutPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("ignoreSubLayout"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(ignoreSubLayoutPropertyTree!= null):((ignoreSubLayoutPropertyTree == null)||(!ignoreSubLayoutPropertyTree.isLeaf())))) {
            _other.ignoreSubLayout = this.ignoreSubLayout;
        }
        final PropertyTree newEnabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("newEnabled"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(newEnabledPropertyTree!= null):((newEnabledPropertyTree == null)||(!newEnabledPropertyTree.isLeaf())))) {
            _other.newEnabled = this.newEnabled;
        }
        final PropertyTree editEnabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("editEnabled"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(editEnabledPropertyTree!= null):((editEnabledPropertyTree == null)||(!editEnabledPropertyTree.isLeaf())))) {
            _other.editEnabled = this.editEnabled;
        }
        final PropertyTree deleteEnabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("deleteEnabled"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(deleteEnabledPropertyTree!= null):((deleteEnabledPropertyTree == null)||(!deleteEnabledPropertyTree.isLeaf())))) {
            _other.deleteEnabled = this.deleteEnabled;
        }
        final PropertyTree cloneEnabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("cloneEnabled"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(cloneEnabledPropertyTree!= null):((cloneEnabledPropertyTree == null)||(!cloneEnabledPropertyTree.isLeaf())))) {
            _other.cloneEnabled = this.cloneEnabled;
        }
    }

    @Override
    public<_B >WebSubform.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new WebSubform.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    @Override
    public WebSubform.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >WebSubform.Builder<_B> copyOf(final WebComponent _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebSubform.Builder<_B> _newBuilder = new WebSubform.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >WebSubform.Builder<_B> copyOf(final WebInputComponent _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebSubform.Builder<_B> _newBuilder = new WebSubform.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >WebSubform.Builder<_B> copyOf(final WebSubform _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebSubform.Builder<_B> _newBuilder = new WebSubform.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static WebSubform.Builder<Void> copyExcept(final WebComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebSubform.Builder<Void> copyExcept(final WebInputComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebSubform.Builder<Void> copyExcept(final WebSubform _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebSubform.Builder<Void> copyOnly(final WebComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static WebSubform.Builder<Void> copyOnly(final WebInputComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static WebSubform.Builder<Void> copyOnly(final WebSubform _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >
        extends WebInputComponent.Builder<_B>
        implements Buildable
    {

        private List<WebSubformColumn.Builder<WebSubform.Builder<_B>>> subformColumns;
        private WebComponent.Builder<WebSubform.Builder<_B>> controllerType;
        private String boAttrId;
        private String entity;
        private Boolean autonumberSorting;
        private Boolean notCloneable;
        private Boolean multiEditable;
        private WebToolbarOrientation toolbarOrientation;
        private Boolean opaque;
        private String uniqueMastercolumn;
        private String foreignkeyfieldToParent;
        private String parentSubform;
        private Boolean dynamicCellHeightsDefault;
        private Boolean ignoreSubLayout;
        private Boolean newEnabled;
        private Boolean editEnabled;
        private Boolean deleteEnabled;
        private Boolean cloneEnabled;

        public Builder(final _B _parentBuilder, final WebSubform _other, final boolean _copy) {
            super(_parentBuilder, _other, _copy);
            if (_other!= null) {
                if (_other.subformColumns == null) {
                    this.subformColumns = null;
                } else {
                    this.subformColumns = new ArrayList<WebSubformColumn.Builder<WebSubform.Builder<_B>>>();
                    for (WebSubformColumn _item: _other.subformColumns) {
                        this.subformColumns.add(((_item == null)?null:_item.newCopyBuilder(this)));
                    }
                }
                this.controllerType = ((_other.controllerType == null)?null:_other.controllerType.newCopyBuilder(this));
                this.boAttrId = _other.boAttrId;
                this.entity = _other.entity;
                this.autonumberSorting = _other.autonumberSorting;
                this.notCloneable = _other.notCloneable;
                this.multiEditable = _other.multiEditable;
                this.toolbarOrientation = _other.toolbarOrientation;
                this.opaque = _other.opaque;
                this.uniqueMastercolumn = _other.uniqueMastercolumn;
                this.foreignkeyfieldToParent = _other.foreignkeyfieldToParent;
                this.parentSubform = _other.parentSubform;
                this.dynamicCellHeightsDefault = _other.dynamicCellHeightsDefault;
                this.ignoreSubLayout = _other.ignoreSubLayout;
                this.newEnabled = _other.newEnabled;
                this.editEnabled = _other.editEnabled;
                this.deleteEnabled = _other.deleteEnabled;
                this.cloneEnabled = _other.cloneEnabled;
            }
        }

        public Builder(final _B _parentBuilder, final WebSubform _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            super(_parentBuilder, _other, _copy, _propertyTree, _propertyTreeUse);
            if (_other!= null) {
                final PropertyTree subformColumnsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("subformColumns"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(subformColumnsPropertyTree!= null):((subformColumnsPropertyTree == null)||(!subformColumnsPropertyTree.isLeaf())))) {
                    if (_other.subformColumns == null) {
                        this.subformColumns = null;
                    } else {
                        this.subformColumns = new ArrayList<WebSubformColumn.Builder<WebSubform.Builder<_B>>>();
                        for (WebSubformColumn _item: _other.subformColumns) {
                            this.subformColumns.add(((_item == null)?null:_item.newCopyBuilder(this, subformColumnsPropertyTree, _propertyTreeUse)));
                        }
                    }
                }
                final PropertyTree controllerTypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("controllerType"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(controllerTypePropertyTree!= null):((controllerTypePropertyTree == null)||(!controllerTypePropertyTree.isLeaf())))) {
                    this.controllerType = ((_other.controllerType == null)?null:_other.controllerType.newCopyBuilder(this, controllerTypePropertyTree, _propertyTreeUse));
                }
                final PropertyTree boAttrIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boAttrId"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boAttrIdPropertyTree!= null):((boAttrIdPropertyTree == null)||(!boAttrIdPropertyTree.isLeaf())))) {
                    this.boAttrId = _other.boAttrId;
                }
                final PropertyTree entityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entity"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityPropertyTree!= null):((entityPropertyTree == null)||(!entityPropertyTree.isLeaf())))) {
                    this.entity = _other.entity;
                }
                final PropertyTree autonumberSortingPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("autonumberSorting"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(autonumberSortingPropertyTree!= null):((autonumberSortingPropertyTree == null)||(!autonumberSortingPropertyTree.isLeaf())))) {
                    this.autonumberSorting = _other.autonumberSorting;
                }
                final PropertyTree notCloneablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("notCloneable"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(notCloneablePropertyTree!= null):((notCloneablePropertyTree == null)||(!notCloneablePropertyTree.isLeaf())))) {
                    this.notCloneable = _other.notCloneable;
                }
                final PropertyTree multiEditablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("multiEditable"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(multiEditablePropertyTree!= null):((multiEditablePropertyTree == null)||(!multiEditablePropertyTree.isLeaf())))) {
                    this.multiEditable = _other.multiEditable;
                }
                final PropertyTree toolbarOrientationPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("toolbarOrientation"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(toolbarOrientationPropertyTree!= null):((toolbarOrientationPropertyTree == null)||(!toolbarOrientationPropertyTree.isLeaf())))) {
                    this.toolbarOrientation = _other.toolbarOrientation;
                }
                final PropertyTree opaquePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("opaque"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(opaquePropertyTree!= null):((opaquePropertyTree == null)||(!opaquePropertyTree.isLeaf())))) {
                    this.opaque = _other.opaque;
                }
                final PropertyTree uniqueMastercolumnPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("uniqueMastercolumn"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(uniqueMastercolumnPropertyTree!= null):((uniqueMastercolumnPropertyTree == null)||(!uniqueMastercolumnPropertyTree.isLeaf())))) {
                    this.uniqueMastercolumn = _other.uniqueMastercolumn;
                }
                final PropertyTree foreignkeyfieldToParentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("foreignkeyfieldToParent"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(foreignkeyfieldToParentPropertyTree!= null):((foreignkeyfieldToParentPropertyTree == null)||(!foreignkeyfieldToParentPropertyTree.isLeaf())))) {
                    this.foreignkeyfieldToParent = _other.foreignkeyfieldToParent;
                }
                final PropertyTree parentSubformPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("parentSubform"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(parentSubformPropertyTree!= null):((parentSubformPropertyTree == null)||(!parentSubformPropertyTree.isLeaf())))) {
                    this.parentSubform = _other.parentSubform;
                }
                final PropertyTree dynamicCellHeightsDefaultPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dynamicCellHeightsDefault"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dynamicCellHeightsDefaultPropertyTree!= null):((dynamicCellHeightsDefaultPropertyTree == null)||(!dynamicCellHeightsDefaultPropertyTree.isLeaf())))) {
                    this.dynamicCellHeightsDefault = _other.dynamicCellHeightsDefault;
                }
                final PropertyTree ignoreSubLayoutPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("ignoreSubLayout"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(ignoreSubLayoutPropertyTree!= null):((ignoreSubLayoutPropertyTree == null)||(!ignoreSubLayoutPropertyTree.isLeaf())))) {
                    this.ignoreSubLayout = _other.ignoreSubLayout;
                }
                final PropertyTree newEnabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("newEnabled"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(newEnabledPropertyTree!= null):((newEnabledPropertyTree == null)||(!newEnabledPropertyTree.isLeaf())))) {
                    this.newEnabled = _other.newEnabled;
                }
                final PropertyTree editEnabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("editEnabled"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(editEnabledPropertyTree!= null):((editEnabledPropertyTree == null)||(!editEnabledPropertyTree.isLeaf())))) {
                    this.editEnabled = _other.editEnabled;
                }
                final PropertyTree deleteEnabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("deleteEnabled"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(deleteEnabledPropertyTree!= null):((deleteEnabledPropertyTree == null)||(!deleteEnabledPropertyTree.isLeaf())))) {
                    this.deleteEnabled = _other.deleteEnabled;
                }
                final PropertyTree cloneEnabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("cloneEnabled"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(cloneEnabledPropertyTree!= null):((cloneEnabledPropertyTree == null)||(!cloneEnabledPropertyTree.isLeaf())))) {
                    this.cloneEnabled = _other.cloneEnabled;
                }
            }
        }

        protected<_P extends WebSubform >_P init(final _P _product) {
            if (this.subformColumns!= null) {
                final List<WebSubformColumn> subformColumns = new ArrayList<WebSubformColumn>(this.subformColumns.size());
                for (WebSubformColumn.Builder<WebSubform.Builder<_B>> _item: this.subformColumns) {
                    subformColumns.add(_item.build());
                }
                _product.subformColumns = subformColumns;
            }
            _product.controllerType = ((this.controllerType == null)?null:this.controllerType.build());
            _product.boAttrId = this.boAttrId;
            _product.entity = this.entity;
            _product.autonumberSorting = this.autonumberSorting;
            _product.notCloneable = this.notCloneable;
            _product.multiEditable = this.multiEditable;
            _product.toolbarOrientation = this.toolbarOrientation;
            _product.opaque = this.opaque;
            _product.uniqueMastercolumn = this.uniqueMastercolumn;
            _product.foreignkeyfieldToParent = this.foreignkeyfieldToParent;
            _product.parentSubform = this.parentSubform;
            _product.dynamicCellHeightsDefault = this.dynamicCellHeightsDefault;
            _product.ignoreSubLayout = this.ignoreSubLayout;
            _product.newEnabled = this.newEnabled;
            _product.editEnabled = this.editEnabled;
            _product.deleteEnabled = this.deleteEnabled;
            _product.cloneEnabled = this.cloneEnabled;
            return super.init(_product);
        }

        /**
         * Adds the given items to the value of "subformColumns"
         * 
         * @param subformColumns
         *     Items to add to the value of the "subformColumns" property
         */
        public WebSubform.Builder<_B> addSubformColumns(final Iterable<? extends WebSubformColumn> subformColumns) {
            if (subformColumns!= null) {
                if (this.subformColumns == null) {
                    this.subformColumns = new ArrayList<WebSubformColumn.Builder<WebSubform.Builder<_B>>>();
                }
                for (WebSubformColumn _item: subformColumns) {
                    this.subformColumns.add(new WebSubformColumn.Builder<WebSubform.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "subformColumns" (any previous value will be replaced)
         * 
         * @param subformColumns
         *     New value of the "subformColumns" property.
         */
        public WebSubform.Builder<_B> withSubformColumns(final Iterable<? extends WebSubformColumn> subformColumns) {
            if (this.subformColumns!= null) {
                this.subformColumns.clear();
            }
            return addSubformColumns(subformColumns);
        }

        /**
         * Adds the given items to the value of "subformColumns"
         * 
         * @param subformColumns
         *     Items to add to the value of the "subformColumns" property
         */
        public WebSubform.Builder<_B> addSubformColumns(WebSubformColumn... subformColumns) {
            addSubformColumns(Arrays.asList(subformColumns));
            return this;
        }

        /**
         * Sets the new value of "subformColumns" (any previous value will be replaced)
         * 
         * @param subformColumns
         *     New value of the "subformColumns" property.
         */
        public WebSubform.Builder<_B> withSubformColumns(WebSubformColumn... subformColumns) {
            withSubformColumns(Arrays.asList(subformColumns));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "SubformColumns" property.
         * Use {@link org.nuclos.schema.layout.web.WebSubformColumn.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "SubformColumns" property.
         *     Use {@link org.nuclos.schema.layout.web.WebSubformColumn.Builder#end()} to return to the current builder.
         */
        public WebSubformColumn.Builder<? extends WebSubform.Builder<_B>> addSubformColumns() {
            if (this.subformColumns == null) {
                this.subformColumns = new ArrayList<WebSubformColumn.Builder<WebSubform.Builder<_B>>>();
            }
            final WebSubformColumn.Builder<WebSubform.Builder<_B>> subformColumns_Builder = new WebSubformColumn.Builder<WebSubform.Builder<_B>>(this, null, false);
            this.subformColumns.add(subformColumns_Builder);
            return subformColumns_Builder;
        }

        /**
         * Sets the new value of "controllerType" (any previous value will be replaced)
         * 
         * @param controllerType
         *     New value of the "controllerType" property.
         */
        public WebSubform.Builder<_B> withControllerType(final WebComponent controllerType) {
            this.controllerType = ((controllerType == null)?null:new WebComponent.Builder<WebSubform.Builder<_B>>(this, controllerType, false));
            return this;
        }

        /**
         * Sets the new value of "boAttrId" (any previous value will be replaced)
         * 
         * @param boAttrId
         *     New value of the "boAttrId" property.
         */
        public WebSubform.Builder<_B> withBoAttrId(final String boAttrId) {
            this.boAttrId = boAttrId;
            return this;
        }

        /**
         * Sets the new value of "entity" (any previous value will be replaced)
         * 
         * @param entity
         *     New value of the "entity" property.
         */
        public WebSubform.Builder<_B> withEntity(final String entity) {
            this.entity = entity;
            return this;
        }

        /**
         * Sets the new value of "autonumberSorting" (any previous value will be replaced)
         * 
         * @param autonumberSorting
         *     New value of the "autonumberSorting" property.
         */
        public WebSubform.Builder<_B> withAutonumberSorting(final Boolean autonumberSorting) {
            this.autonumberSorting = autonumberSorting;
            return this;
        }

        /**
         * Sets the new value of "notCloneable" (any previous value will be replaced)
         * 
         * @param notCloneable
         *     New value of the "notCloneable" property.
         */
        public WebSubform.Builder<_B> withNotCloneable(final Boolean notCloneable) {
            this.notCloneable = notCloneable;
            return this;
        }

        /**
         * Sets the new value of "multiEditable" (any previous value will be replaced)
         * 
         * @param multiEditable
         *     New value of the "multiEditable" property.
         */
        public WebSubform.Builder<_B> withMultiEditable(final Boolean multiEditable) {
            this.multiEditable = multiEditable;
            return this;
        }

        /**
         * Sets the new value of "toolbarOrientation" (any previous value will be replaced)
         * 
         * @param toolbarOrientation
         *     New value of the "toolbarOrientation" property.
         */
        public WebSubform.Builder<_B> withToolbarOrientation(final WebToolbarOrientation toolbarOrientation) {
            this.toolbarOrientation = toolbarOrientation;
            return this;
        }

        /**
         * Sets the new value of "opaque" (any previous value will be replaced)
         * 
         * @param opaque
         *     New value of the "opaque" property.
         */
        public WebSubform.Builder<_B> withOpaque(final Boolean opaque) {
            this.opaque = opaque;
            return this;
        }

        /**
         * Sets the new value of "uniqueMastercolumn" (any previous value will be replaced)
         * 
         * @param uniqueMastercolumn
         *     New value of the "uniqueMastercolumn" property.
         */
        public WebSubform.Builder<_B> withUniqueMastercolumn(final String uniqueMastercolumn) {
            this.uniqueMastercolumn = uniqueMastercolumn;
            return this;
        }

        /**
         * Sets the new value of "foreignkeyfieldToParent" (any previous value will be replaced)
         * 
         * @param foreignkeyfieldToParent
         *     New value of the "foreignkeyfieldToParent" property.
         */
        public WebSubform.Builder<_B> withForeignkeyfieldToParent(final String foreignkeyfieldToParent) {
            this.foreignkeyfieldToParent = foreignkeyfieldToParent;
            return this;
        }

        /**
         * Sets the new value of "parentSubform" (any previous value will be replaced)
         * 
         * @param parentSubform
         *     New value of the "parentSubform" property.
         */
        public WebSubform.Builder<_B> withParentSubform(final String parentSubform) {
            this.parentSubform = parentSubform;
            return this;
        }

        /**
         * Sets the new value of "dynamicCellHeightsDefault" (any previous value will be replaced)
         * 
         * @param dynamicCellHeightsDefault
         *     New value of the "dynamicCellHeightsDefault" property.
         */
        public WebSubform.Builder<_B> withDynamicCellHeightsDefault(final Boolean dynamicCellHeightsDefault) {
            this.dynamicCellHeightsDefault = dynamicCellHeightsDefault;
            return this;
        }

        /**
         * Sets the new value of "ignoreSubLayout" (any previous value will be replaced)
         * 
         * @param ignoreSubLayout
         *     New value of the "ignoreSubLayout" property.
         */
        public WebSubform.Builder<_B> withIgnoreSubLayout(final Boolean ignoreSubLayout) {
            this.ignoreSubLayout = ignoreSubLayout;
            return this;
        }

        /**
         * Sets the new value of "newEnabled" (any previous value will be replaced)
         * 
         * @param newEnabled
         *     New value of the "newEnabled" property.
         */
        public WebSubform.Builder<_B> withNewEnabled(final Boolean newEnabled) {
            this.newEnabled = newEnabled;
            return this;
        }

        /**
         * Sets the new value of "editEnabled" (any previous value will be replaced)
         * 
         * @param editEnabled
         *     New value of the "editEnabled" property.
         */
        public WebSubform.Builder<_B> withEditEnabled(final Boolean editEnabled) {
            this.editEnabled = editEnabled;
            return this;
        }

        /**
         * Sets the new value of "deleteEnabled" (any previous value will be replaced)
         * 
         * @param deleteEnabled
         *     New value of the "deleteEnabled" property.
         */
        public WebSubform.Builder<_B> withDeleteEnabled(final Boolean deleteEnabled) {
            this.deleteEnabled = deleteEnabled;
            return this;
        }

        /**
         * Sets the new value of "cloneEnabled" (any previous value will be replaced)
         * 
         * @param cloneEnabled
         *     New value of the "cloneEnabled" property.
         */
        public WebSubform.Builder<_B> withCloneEnabled(final Boolean cloneEnabled) {
            this.cloneEnabled = cloneEnabled;
            return this;
        }

        /**
         * Sets the new value of "valuelistProvider" (any previous value will be replaced)
         * 
         * @param valuelistProvider
         *     New value of the "valuelistProvider" property.
         */
        @Override
        public WebSubform.Builder<_B> withValuelistProvider(final WebValuelistProvider valuelistProvider) {
            super.withValuelistProvider(valuelistProvider);
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "valuelistProvider" property.
         * Use {@link org.nuclos.schema.layout.web.WebValuelistProvider.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "valuelistProvider" property.
         *     Use {@link org.nuclos.schema.layout.web.WebValuelistProvider.Builder#end()} to return to the current builder.
         */
        public WebValuelistProvider.Builder<? extends WebSubform.Builder<_B>> withValuelistProvider() {
            return ((WebValuelistProvider.Builder<? extends WebSubform.Builder<_B>> ) super.withValuelistProvider());
        }

        /**
         * Sets the new value of "enabled" (any previous value will be replaced)
         * 
         * @param enabled
         *     New value of the "enabled" property.
         */
        @Override
        public WebSubform.Builder<_B> withEnabled(final Boolean enabled) {
            super.withEnabled(enabled);
            return this;
        }

        /**
         * Sets the new value of "editable" (any previous value will be replaced)
         * 
         * @param editable
         *     New value of the "editable" property.
         */
        @Override
        public WebSubform.Builder<_B> withEditable(final Boolean editable) {
            super.withEditable(editable);
            return this;
        }

        /**
         * Sets the new value of "insertable" (any previous value will be replaced)
         * 
         * @param insertable
         *     New value of the "insertable" property.
         */
        @Override
        public WebSubform.Builder<_B> withInsertable(final Boolean insertable) {
            super.withInsertable(insertable);
            return this;
        }

        /**
         * Sets the new value of "columns" (any previous value will be replaced)
         * 
         * @param columns
         *     New value of the "columns" property.
         */
        @Override
        public WebSubform.Builder<_B> withColumns(final String columns) {
            super.withColumns(columns);
            return this;
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        @Override
        public WebSubform.Builder<_B> addAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            super.addAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        @Override
        public WebSubform.Builder<_B> addAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            super.addAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        @Override
        public WebSubform.Builder<_B> withAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            super.withAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        @Override
        public WebSubform.Builder<_B> withAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            super.withAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "advancedProperties" property.
         * Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "advancedProperties" property.
         *     Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         */
        @Override
        public WebAdvancedProperty.Builder<? extends WebSubform.Builder<_B>> addAdvancedProperties() {
            return ((WebAdvancedProperty.Builder<? extends WebSubform.Builder<_B>> ) super.addAdvancedProperties());
        }

        /**
         * Sets the new value of "id" (any previous value will be replaced)
         * 
         * @param id
         *     New value of the "id" property.
         */
        @Override
        public WebSubform.Builder<_B> withId(final String id) {
            super.withId(id);
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        @Override
        public WebSubform.Builder<_B> withName(final String name) {
            super.withName(name);
            return this;
        }

        /**
         * Sets the new value of "column" (any previous value will be replaced)
         * 
         * @param column
         *     New value of the "column" property.
         */
        @Override
        public WebSubform.Builder<_B> withColumn(final BigInteger column) {
            super.withColumn(column);
            return this;
        }

        /**
         * Sets the new value of "row" (any previous value will be replaced)
         * 
         * @param row
         *     New value of the "row" property.
         */
        @Override
        public WebSubform.Builder<_B> withRow(final BigInteger row) {
            super.withRow(row);
            return this;
        }

        /**
         * Sets the new value of "colspan" (any previous value will be replaced)
         * 
         * @param colspan
         *     New value of the "colspan" property.
         */
        @Override
        public WebSubform.Builder<_B> withColspan(final BigInteger colspan) {
            super.withColspan(colspan);
            return this;
        }

        /**
         * Sets the new value of "rowspan" (any previous value will be replaced)
         * 
         * @param rowspan
         *     New value of the "rowspan" property.
         */
        @Override
        public WebSubform.Builder<_B> withRowspan(final BigInteger rowspan) {
            super.withRowspan(rowspan);
            return this;
        }

        /**
         * Sets the new value of "fontSize" (any previous value will be replaced)
         * 
         * @param fontSize
         *     New value of the "fontSize" property.
         */
        @Override
        public WebSubform.Builder<_B> withFontSize(final String fontSize) {
            super.withFontSize(fontSize);
            return this;
        }

        /**
         * Sets the new value of "textColor" (any previous value will be replaced)
         * 
         * @param textColor
         *     New value of the "textColor" property.
         */
        @Override
        public WebSubform.Builder<_B> withTextColor(final String textColor) {
            super.withTextColor(textColor);
            return this;
        }

        /**
         * Sets the new value of "bold" (any previous value will be replaced)
         * 
         * @param bold
         *     New value of the "bold" property.
         */
        @Override
        public WebSubform.Builder<_B> withBold(final Boolean bold) {
            super.withBold(bold);
            return this;
        }

        /**
         * Sets the new value of "italic" (any previous value will be replaced)
         * 
         * @param italic
         *     New value of the "italic" property.
         */
        @Override
        public WebSubform.Builder<_B> withItalic(final Boolean italic) {
            super.withItalic(italic);
            return this;
        }

        /**
         * Sets the new value of "underline" (any previous value will be replaced)
         * 
         * @param underline
         *     New value of the "underline" property.
         */
        @Override
        public WebSubform.Builder<_B> withUnderline(final Boolean underline) {
            super.withUnderline(underline);
            return this;
        }

        /**
         * Sets the new value of "nextFocusComponent" (any previous value will be replaced)
         * 
         * @param nextFocusComponent
         *     New value of the "nextFocusComponent" property.
         */
        @Override
        public WebSubform.Builder<_B> withNextFocusComponent(final String nextFocusComponent) {
            super.withNextFocusComponent(nextFocusComponent);
            return this;
        }

        /**
         * Sets the new value of "nextFocusField" (any previous value will be replaced)
         * 
         * @param nextFocusField
         *     New value of the "nextFocusField" property.
         */
        @Override
        public WebSubform.Builder<_B> withNextFocusField(final String nextFocusField) {
            super.withNextFocusField(nextFocusField);
            return this;
        }

        /**
         * Sets the new value of "alternativeTooltip" (any previous value will be replaced)
         * 
         * @param alternativeTooltip
         *     New value of the "alternativeTooltip" property.
         */
        @Override
        public WebSubform.Builder<_B> withAlternativeTooltip(final String alternativeTooltip) {
            super.withAlternativeTooltip(alternativeTooltip);
            return this;
        }

        @Override
        public WebSubform build() {
            if (_storedValue == null) {
                return this.init(new WebSubform());
            } else {
                return ((WebSubform) _storedValue);
            }
        }

        public WebSubform.Builder<_B> copyOf(final WebSubform _other) {
            _other.copyTo(this);
            return this;
        }

        public WebSubform.Builder<_B> copyOf(final WebSubform.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends WebSubform.Selector<WebSubform.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static WebSubform.Select _root() {
            return new WebSubform.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends WebInputComponent.Selector<TRoot, TParent>
    {

        private WebSubformColumn.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> subformColumns = null;
        private WebComponent.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> controllerType = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> boAttrId = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> entity = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> autonumberSorting = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> notCloneable = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> multiEditable = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> toolbarOrientation = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> opaque = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> uniqueMastercolumn = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> foreignkeyfieldToParent = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> parentSubform = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> dynamicCellHeightsDefault = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> ignoreSubLayout = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> newEnabled = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> editEnabled = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> deleteEnabled = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> cloneEnabled = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.subformColumns!= null) {
                products.put("subformColumns", this.subformColumns.init());
            }
            if (this.controllerType!= null) {
                products.put("controllerType", this.controllerType.init());
            }
            if (this.boAttrId!= null) {
                products.put("boAttrId", this.boAttrId.init());
            }
            if (this.entity!= null) {
                products.put("entity", this.entity.init());
            }
            if (this.autonumberSorting!= null) {
                products.put("autonumberSorting", this.autonumberSorting.init());
            }
            if (this.notCloneable!= null) {
                products.put("notCloneable", this.notCloneable.init());
            }
            if (this.multiEditable!= null) {
                products.put("multiEditable", this.multiEditable.init());
            }
            if (this.toolbarOrientation!= null) {
                products.put("toolbarOrientation", this.toolbarOrientation.init());
            }
            if (this.opaque!= null) {
                products.put("opaque", this.opaque.init());
            }
            if (this.uniqueMastercolumn!= null) {
                products.put("uniqueMastercolumn", this.uniqueMastercolumn.init());
            }
            if (this.foreignkeyfieldToParent!= null) {
                products.put("foreignkeyfieldToParent", this.foreignkeyfieldToParent.init());
            }
            if (this.parentSubform!= null) {
                products.put("parentSubform", this.parentSubform.init());
            }
            if (this.dynamicCellHeightsDefault!= null) {
                products.put("dynamicCellHeightsDefault", this.dynamicCellHeightsDefault.init());
            }
            if (this.ignoreSubLayout!= null) {
                products.put("ignoreSubLayout", this.ignoreSubLayout.init());
            }
            if (this.newEnabled!= null) {
                products.put("newEnabled", this.newEnabled.init());
            }
            if (this.editEnabled!= null) {
                products.put("editEnabled", this.editEnabled.init());
            }
            if (this.deleteEnabled!= null) {
                products.put("deleteEnabled", this.deleteEnabled.init());
            }
            if (this.cloneEnabled!= null) {
                products.put("cloneEnabled", this.cloneEnabled.init());
            }
            return products;
        }

        public WebSubformColumn.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> subformColumns() {
            return ((this.subformColumns == null)?this.subformColumns = new WebSubformColumn.Selector<TRoot, WebSubform.Selector<TRoot, TParent>>(this._root, this, "subformColumns"):this.subformColumns);
        }

        public WebComponent.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> controllerType() {
            return ((this.controllerType == null)?this.controllerType = new WebComponent.Selector<TRoot, WebSubform.Selector<TRoot, TParent>>(this._root, this, "controllerType"):this.controllerType);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> boAttrId() {
            return ((this.boAttrId == null)?this.boAttrId = new com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>>(this._root, this, "boAttrId"):this.boAttrId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> entity() {
            return ((this.entity == null)?this.entity = new com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>>(this._root, this, "entity"):this.entity);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> autonumberSorting() {
            return ((this.autonumberSorting == null)?this.autonumberSorting = new com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>>(this._root, this, "autonumberSorting"):this.autonumberSorting);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> notCloneable() {
            return ((this.notCloneable == null)?this.notCloneable = new com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>>(this._root, this, "notCloneable"):this.notCloneable);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> multiEditable() {
            return ((this.multiEditable == null)?this.multiEditable = new com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>>(this._root, this, "multiEditable"):this.multiEditable);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> toolbarOrientation() {
            return ((this.toolbarOrientation == null)?this.toolbarOrientation = new com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>>(this._root, this, "toolbarOrientation"):this.toolbarOrientation);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> opaque() {
            return ((this.opaque == null)?this.opaque = new com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>>(this._root, this, "opaque"):this.opaque);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> uniqueMastercolumn() {
            return ((this.uniqueMastercolumn == null)?this.uniqueMastercolumn = new com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>>(this._root, this, "uniqueMastercolumn"):this.uniqueMastercolumn);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> foreignkeyfieldToParent() {
            return ((this.foreignkeyfieldToParent == null)?this.foreignkeyfieldToParent = new com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>>(this._root, this, "foreignkeyfieldToParent"):this.foreignkeyfieldToParent);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> parentSubform() {
            return ((this.parentSubform == null)?this.parentSubform = new com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>>(this._root, this, "parentSubform"):this.parentSubform);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> dynamicCellHeightsDefault() {
            return ((this.dynamicCellHeightsDefault == null)?this.dynamicCellHeightsDefault = new com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>>(this._root, this, "dynamicCellHeightsDefault"):this.dynamicCellHeightsDefault);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> ignoreSubLayout() {
            return ((this.ignoreSubLayout == null)?this.ignoreSubLayout = new com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>>(this._root, this, "ignoreSubLayout"):this.ignoreSubLayout);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> newEnabled() {
            return ((this.newEnabled == null)?this.newEnabled = new com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>>(this._root, this, "newEnabled"):this.newEnabled);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> editEnabled() {
            return ((this.editEnabled == null)?this.editEnabled = new com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>>(this._root, this, "editEnabled"):this.editEnabled);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> deleteEnabled() {
            return ((this.deleteEnabled == null)?this.deleteEnabled = new com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>>(this._root, this, "deleteEnabled"):this.deleteEnabled);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>> cloneEnabled() {
            return ((this.cloneEnabled == null)?this.cloneEnabled = new com.kscs.util.jaxb.Selector<TRoot, WebSubform.Selector<TRoot, TParent>>(this._root, this, "cloneEnabled"):this.cloneEnabled);
        }

    }

}
