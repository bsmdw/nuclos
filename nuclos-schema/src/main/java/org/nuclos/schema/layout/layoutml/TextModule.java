
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="entity" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="entityfield" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="entityfield-name" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="entityfield-sort" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "text-module")
public class TextModule implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "entity")
    @XmlSchemaType(name = "anySimpleType")
    protected String entity;
    @XmlAttribute(name = "entityfield")
    @XmlSchemaType(name = "anySimpleType")
    protected String entityfield;
    @XmlAttribute(name = "entityfield-name")
    @XmlSchemaType(name = "anySimpleType")
    protected String entityfieldName;
    @XmlAttribute(name = "entityfield-sort")
    @XmlSchemaType(name = "anySimpleType")
    protected String entityfieldSort;

    /**
     * Gets the value of the entity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntity() {
        return entity;
    }

    /**
     * Sets the value of the entity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntity(String value) {
        this.entity = value;
    }

    /**
     * Gets the value of the entityfield property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityfield() {
        return entityfield;
    }

    /**
     * Sets the value of the entityfield property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityfield(String value) {
        this.entityfield = value;
    }

    /**
     * Gets the value of the entityfieldName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityfieldName() {
        return entityfieldName;
    }

    /**
     * Sets the value of the entityfieldName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityfieldName(String value) {
        this.entityfieldName = value;
    }

    /**
     * Gets the value of the entityfieldSort property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityfieldSort() {
        return entityfieldSort;
    }

    /**
     * Sets the value of the entityfieldSort property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityfieldSort(String value) {
        this.entityfieldSort = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theEntity;
            theEntity = this.getEntity();
            strategy.appendField(locator, this, "entity", buffer, theEntity);
        }
        {
            String theEntityfield;
            theEntityfield = this.getEntityfield();
            strategy.appendField(locator, this, "entityfield", buffer, theEntityfield);
        }
        {
            String theEntityfieldName;
            theEntityfieldName = this.getEntityfieldName();
            strategy.appendField(locator, this, "entityfieldName", buffer, theEntityfieldName);
        }
        {
            String theEntityfieldSort;
            theEntityfieldSort = this.getEntityfieldSort();
            strategy.appendField(locator, this, "entityfieldSort", buffer, theEntityfieldSort);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof TextModule)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final TextModule that = ((TextModule) object);
        {
            String lhsEntity;
            lhsEntity = this.getEntity();
            String rhsEntity;
            rhsEntity = that.getEntity();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entity", lhsEntity), LocatorUtils.property(thatLocator, "entity", rhsEntity), lhsEntity, rhsEntity)) {
                return false;
            }
        }
        {
            String lhsEntityfield;
            lhsEntityfield = this.getEntityfield();
            String rhsEntityfield;
            rhsEntityfield = that.getEntityfield();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityfield", lhsEntityfield), LocatorUtils.property(thatLocator, "entityfield", rhsEntityfield), lhsEntityfield, rhsEntityfield)) {
                return false;
            }
        }
        {
            String lhsEntityfieldName;
            lhsEntityfieldName = this.getEntityfieldName();
            String rhsEntityfieldName;
            rhsEntityfieldName = that.getEntityfieldName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityfieldName", lhsEntityfieldName), LocatorUtils.property(thatLocator, "entityfieldName", rhsEntityfieldName), lhsEntityfieldName, rhsEntityfieldName)) {
                return false;
            }
        }
        {
            String lhsEntityfieldSort;
            lhsEntityfieldSort = this.getEntityfieldSort();
            String rhsEntityfieldSort;
            rhsEntityfieldSort = that.getEntityfieldSort();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityfieldSort", lhsEntityfieldSort), LocatorUtils.property(thatLocator, "entityfieldSort", rhsEntityfieldSort), lhsEntityfieldSort, rhsEntityfieldSort)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theEntity;
            theEntity = this.getEntity();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entity", theEntity), currentHashCode, theEntity);
        }
        {
            String theEntityfield;
            theEntityfield = this.getEntityfield();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityfield", theEntityfield), currentHashCode, theEntityfield);
        }
        {
            String theEntityfieldName;
            theEntityfieldName = this.getEntityfieldName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityfieldName", theEntityfieldName), currentHashCode, theEntityfieldName);
        }
        {
            String theEntityfieldSort;
            theEntityfieldSort = this.getEntityfieldSort();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityfieldSort", theEntityfieldSort), currentHashCode, theEntityfieldSort);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof TextModule) {
            final TextModule copy = ((TextModule) draftCopy);
            if (this.entity!= null) {
                String sourceEntity;
                sourceEntity = this.getEntity();
                String copyEntity = ((String) strategy.copy(LocatorUtils.property(locator, "entity", sourceEntity), sourceEntity));
                copy.setEntity(copyEntity);
            } else {
                copy.entity = null;
            }
            if (this.entityfield!= null) {
                String sourceEntityfield;
                sourceEntityfield = this.getEntityfield();
                String copyEntityfield = ((String) strategy.copy(LocatorUtils.property(locator, "entityfield", sourceEntityfield), sourceEntityfield));
                copy.setEntityfield(copyEntityfield);
            } else {
                copy.entityfield = null;
            }
            if (this.entityfieldName!= null) {
                String sourceEntityfieldName;
                sourceEntityfieldName = this.getEntityfieldName();
                String copyEntityfieldName = ((String) strategy.copy(LocatorUtils.property(locator, "entityfieldName", sourceEntityfieldName), sourceEntityfieldName));
                copy.setEntityfieldName(copyEntityfieldName);
            } else {
                copy.entityfieldName = null;
            }
            if (this.entityfieldSort!= null) {
                String sourceEntityfieldSort;
                sourceEntityfieldSort = this.getEntityfieldSort();
                String copyEntityfieldSort = ((String) strategy.copy(LocatorUtils.property(locator, "entityfieldSort", sourceEntityfieldSort), sourceEntityfieldSort));
                copy.setEntityfieldSort(copyEntityfieldSort);
            } else {
                copy.entityfieldSort = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new TextModule();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final TextModule.Builder<_B> _other) {
        _other.entity = this.entity;
        _other.entityfield = this.entityfield;
        _other.entityfieldName = this.entityfieldName;
        _other.entityfieldSort = this.entityfieldSort;
    }

    public<_B >TextModule.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new TextModule.Builder<_B>(_parentBuilder, this, true);
    }

    public TextModule.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static TextModule.Builder<Void> builder() {
        return new TextModule.Builder<Void>(null, null, false);
    }

    public static<_B >TextModule.Builder<_B> copyOf(final TextModule _other) {
        final TextModule.Builder<_B> _newBuilder = new TextModule.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final TextModule.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree entityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entity"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityPropertyTree!= null):((entityPropertyTree == null)||(!entityPropertyTree.isLeaf())))) {
            _other.entity = this.entity;
        }
        final PropertyTree entityfieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityfield"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityfieldPropertyTree!= null):((entityfieldPropertyTree == null)||(!entityfieldPropertyTree.isLeaf())))) {
            _other.entityfield = this.entityfield;
        }
        final PropertyTree entityfieldNamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityfieldName"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityfieldNamePropertyTree!= null):((entityfieldNamePropertyTree == null)||(!entityfieldNamePropertyTree.isLeaf())))) {
            _other.entityfieldName = this.entityfieldName;
        }
        final PropertyTree entityfieldSortPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityfieldSort"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityfieldSortPropertyTree!= null):((entityfieldSortPropertyTree == null)||(!entityfieldSortPropertyTree.isLeaf())))) {
            _other.entityfieldSort = this.entityfieldSort;
        }
    }

    public<_B >TextModule.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new TextModule.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public TextModule.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >TextModule.Builder<_B> copyOf(final TextModule _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final TextModule.Builder<_B> _newBuilder = new TextModule.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static TextModule.Builder<Void> copyExcept(final TextModule _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static TextModule.Builder<Void> copyOnly(final TextModule _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final TextModule _storedValue;
        private String entity;
        private String entityfield;
        private String entityfieldName;
        private String entityfieldSort;

        public Builder(final _B _parentBuilder, final TextModule _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.entity = _other.entity;
                    this.entityfield = _other.entityfield;
                    this.entityfieldName = _other.entityfieldName;
                    this.entityfieldSort = _other.entityfieldSort;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final TextModule _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree entityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entity"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityPropertyTree!= null):((entityPropertyTree == null)||(!entityPropertyTree.isLeaf())))) {
                        this.entity = _other.entity;
                    }
                    final PropertyTree entityfieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityfield"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityfieldPropertyTree!= null):((entityfieldPropertyTree == null)||(!entityfieldPropertyTree.isLeaf())))) {
                        this.entityfield = _other.entityfield;
                    }
                    final PropertyTree entityfieldNamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityfieldName"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityfieldNamePropertyTree!= null):((entityfieldNamePropertyTree == null)||(!entityfieldNamePropertyTree.isLeaf())))) {
                        this.entityfieldName = _other.entityfieldName;
                    }
                    final PropertyTree entityfieldSortPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityfieldSort"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityfieldSortPropertyTree!= null):((entityfieldSortPropertyTree == null)||(!entityfieldSortPropertyTree.isLeaf())))) {
                        this.entityfieldSort = _other.entityfieldSort;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends TextModule >_P init(final _P _product) {
            _product.entity = this.entity;
            _product.entityfield = this.entityfield;
            _product.entityfieldName = this.entityfieldName;
            _product.entityfieldSort = this.entityfieldSort;
            return _product;
        }

        /**
         * Sets the new value of "entity" (any previous value will be replaced)
         * 
         * @param entity
         *     New value of the "entity" property.
         */
        public TextModule.Builder<_B> withEntity(final String entity) {
            this.entity = entity;
            return this;
        }

        /**
         * Sets the new value of "entityfield" (any previous value will be replaced)
         * 
         * @param entityfield
         *     New value of the "entityfield" property.
         */
        public TextModule.Builder<_B> withEntityfield(final String entityfield) {
            this.entityfield = entityfield;
            return this;
        }

        /**
         * Sets the new value of "entityfieldName" (any previous value will be replaced)
         * 
         * @param entityfieldName
         *     New value of the "entityfieldName" property.
         */
        public TextModule.Builder<_B> withEntityfieldName(final String entityfieldName) {
            this.entityfieldName = entityfieldName;
            return this;
        }

        /**
         * Sets the new value of "entityfieldSort" (any previous value will be replaced)
         * 
         * @param entityfieldSort
         *     New value of the "entityfieldSort" property.
         */
        public TextModule.Builder<_B> withEntityfieldSort(final String entityfieldSort) {
            this.entityfieldSort = entityfieldSort;
            return this;
        }

        @Override
        public TextModule build() {
            if (_storedValue == null) {
                return this.init(new TextModule());
            } else {
                return ((TextModule) _storedValue);
            }
        }

        public TextModule.Builder<_B> copyOf(final TextModule _other) {
            _other.copyTo(this);
            return this;
        }

        public TextModule.Builder<_B> copyOf(final TextModule.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends TextModule.Selector<TextModule.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static TextModule.Select _root() {
            return new TextModule.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, TextModule.Selector<TRoot, TParent>> entity = null;
        private com.kscs.util.jaxb.Selector<TRoot, TextModule.Selector<TRoot, TParent>> entityfield = null;
        private com.kscs.util.jaxb.Selector<TRoot, TextModule.Selector<TRoot, TParent>> entityfieldName = null;
        private com.kscs.util.jaxb.Selector<TRoot, TextModule.Selector<TRoot, TParent>> entityfieldSort = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.entity!= null) {
                products.put("entity", this.entity.init());
            }
            if (this.entityfield!= null) {
                products.put("entityfield", this.entityfield.init());
            }
            if (this.entityfieldName!= null) {
                products.put("entityfieldName", this.entityfieldName.init());
            }
            if (this.entityfieldSort!= null) {
                products.put("entityfieldSort", this.entityfieldSort.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, TextModule.Selector<TRoot, TParent>> entity() {
            return ((this.entity == null)?this.entity = new com.kscs.util.jaxb.Selector<TRoot, TextModule.Selector<TRoot, TParent>>(this._root, this, "entity"):this.entity);
        }

        public com.kscs.util.jaxb.Selector<TRoot, TextModule.Selector<TRoot, TParent>> entityfield() {
            return ((this.entityfield == null)?this.entityfield = new com.kscs.util.jaxb.Selector<TRoot, TextModule.Selector<TRoot, TParent>>(this._root, this, "entityfield"):this.entityfield);
        }

        public com.kscs.util.jaxb.Selector<TRoot, TextModule.Selector<TRoot, TParent>> entityfieldName() {
            return ((this.entityfieldName == null)?this.entityfieldName = new com.kscs.util.jaxb.Selector<TRoot, TextModule.Selector<TRoot, TParent>>(this._root, this, "entityfieldName"):this.entityfieldName);
        }

        public com.kscs.util.jaxb.Selector<TRoot, TextModule.Selector<TRoot, TParent>> entityfieldSort() {
            return ((this.entityfieldSort == null)?this.entityfieldSort = new com.kscs.util.jaxb.Selector<TRoot, TextModule.Selector<TRoot, TParent>>(this._root, this, "entityfieldSort"):this.entityfieldSort);
        }

    }

}
