
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}layoutconstraints" minOccurs="0"/&gt;
 *         &lt;group ref="{}borders"/&gt;
 *         &lt;group ref="{}sizes"/&gt;
 *         &lt;element ref="{}font" minOccurs="0"/&gt;
 *         &lt;element ref="{}text-format" minOccurs="0"/&gt;
 *         &lt;element ref="{}description" minOccurs="0"/&gt;
 *         &lt;element ref="{}translations" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="enabled" type="{}boolean" /&gt;
 *       &lt;attribute name="text" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="resourceId" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "layoutconstraints",
    "clearBorder",
    "border",
    "minimumSize",
    "preferredSize",
    "strictSize",
    "font",
    "textFormat",
    "description",
    "translations"
})
@XmlRootElement(name = "label")
public class Label implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "layoutconstraints", type = JAXBElement.class, required = false)
    protected JAXBElement<?> layoutconstraints;
    @XmlElement(name = "clear-border")
    protected ClearBorder clearBorder;
    @XmlElementRef(name = "border", type = JAXBElement.class, required = false)
    protected List<JAXBElement<?>> border;
    @XmlElement(name = "minimum-size")
    protected MinimumSize minimumSize;
    @XmlElement(name = "preferred-size")
    protected PreferredSize preferredSize;
    @XmlElement(name = "strict-size")
    protected StrictSize strictSize;
    protected Font font;
    @XmlElement(name = "text-format")
    protected TextFormat textFormat;
    protected String description;
    protected Translations translations;
    @XmlAttribute(name = "name")
    @XmlSchemaType(name = "anySimpleType")
    protected String name;
    @XmlAttribute(name = "enabled")
    protected Boolean enabled;
    @XmlAttribute(name = "text", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String text;
    @XmlAttribute(name = "resourceId")
    @XmlSchemaType(name = "anySimpleType")
    protected String resourceId;

    /**
     * Gets the value of the layoutconstraints property.
     * 
     * @return
     *     possible object is
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     *     
     */
    public JAXBElement<?> getLayoutconstraints() {
        return layoutconstraints;
    }

    /**
     * Sets the value of the layoutconstraints property.
     * 
     * @param value
     *     allowed object is
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     *     
     */
    public void setLayoutconstraints(JAXBElement<?> value) {
        this.layoutconstraints = value;
    }

    /**
     * Gets the value of the clearBorder property.
     * 
     * @return
     *     possible object is
     *     {@link ClearBorder }
     *     
     */
    public ClearBorder getClearBorder() {
        return clearBorder;
    }

    /**
     * Sets the value of the clearBorder property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClearBorder }
     *     
     */
    public void setClearBorder(ClearBorder value) {
        this.clearBorder = value;
    }

    /**
     * Gets the value of the border property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the border property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBorder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     * 
     * 
     */
    public List<JAXBElement<?>> getBorder() {
        if (border == null) {
            border = new ArrayList<JAXBElement<?>>();
        }
        return this.border;
    }

    /**
     * Gets the value of the minimumSize property.
     * 
     * @return
     *     possible object is
     *     {@link MinimumSize }
     *     
     */
    public MinimumSize getMinimumSize() {
        return minimumSize;
    }

    /**
     * Sets the value of the minimumSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link MinimumSize }
     *     
     */
    public void setMinimumSize(MinimumSize value) {
        this.minimumSize = value;
    }

    /**
     * Gets the value of the preferredSize property.
     * 
     * @return
     *     possible object is
     *     {@link PreferredSize }
     *     
     */
    public PreferredSize getPreferredSize() {
        return preferredSize;
    }

    /**
     * Sets the value of the preferredSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link PreferredSize }
     *     
     */
    public void setPreferredSize(PreferredSize value) {
        this.preferredSize = value;
    }

    /**
     * Gets the value of the strictSize property.
     * 
     * @return
     *     possible object is
     *     {@link StrictSize }
     *     
     */
    public StrictSize getStrictSize() {
        return strictSize;
    }

    /**
     * Sets the value of the strictSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link StrictSize }
     *     
     */
    public void setStrictSize(StrictSize value) {
        this.strictSize = value;
    }

    /**
     * Gets the value of the font property.
     * 
     * @return
     *     possible object is
     *     {@link Font }
     *     
     */
    public Font getFont() {
        return font;
    }

    /**
     * Sets the value of the font property.
     * 
     * @param value
     *     allowed object is
     *     {@link Font }
     *     
     */
    public void setFont(Font value) {
        this.font = value;
    }

    /**
     * Gets the value of the textFormat property.
     * 
     * @return
     *     possible object is
     *     {@link TextFormat }
     *     
     */
    public TextFormat getTextFormat() {
        return textFormat;
    }

    /**
     * Sets the value of the textFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link TextFormat }
     *     
     */
    public void setTextFormat(TextFormat value) {
        this.textFormat = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the translations property.
     * 
     * @return
     *     possible object is
     *     {@link Translations }
     *     
     */
    public Translations getTranslations() {
        return translations;
    }

    /**
     * Sets the value of the translations property.
     * 
     * @param value
     *     allowed object is
     *     {@link Translations }
     *     
     */
    public void setTranslations(Translations value) {
        this.translations = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the enabled property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getEnabled() {
        return enabled;
    }

    /**
     * Sets the value of the enabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnabled(Boolean value) {
        this.enabled = value;
    }

    /**
     * Gets the value of the text property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getText() {
        return text;
    }

    /**
     * Sets the value of the text property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setText(String value) {
        this.text = value;
    }

    /**
     * Gets the value of the resourceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceId() {
        return resourceId;
    }

    /**
     * Sets the value of the resourceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceId(String value) {
        this.resourceId = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            JAXBElement<?> theLayoutconstraints;
            theLayoutconstraints = this.getLayoutconstraints();
            strategy.appendField(locator, this, "layoutconstraints", buffer, theLayoutconstraints);
        }
        {
            ClearBorder theClearBorder;
            theClearBorder = this.getClearBorder();
            strategy.appendField(locator, this, "clearBorder", buffer, theClearBorder);
        }
        {
            List<JAXBElement<?>> theBorder;
            theBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            strategy.appendField(locator, this, "border", buffer, theBorder);
        }
        {
            MinimumSize theMinimumSize;
            theMinimumSize = this.getMinimumSize();
            strategy.appendField(locator, this, "minimumSize", buffer, theMinimumSize);
        }
        {
            PreferredSize thePreferredSize;
            thePreferredSize = this.getPreferredSize();
            strategy.appendField(locator, this, "preferredSize", buffer, thePreferredSize);
        }
        {
            StrictSize theStrictSize;
            theStrictSize = this.getStrictSize();
            strategy.appendField(locator, this, "strictSize", buffer, theStrictSize);
        }
        {
            Font theFont;
            theFont = this.getFont();
            strategy.appendField(locator, this, "font", buffer, theFont);
        }
        {
            TextFormat theTextFormat;
            theTextFormat = this.getTextFormat();
            strategy.appendField(locator, this, "textFormat", buffer, theTextFormat);
        }
        {
            String theDescription;
            theDescription = this.getDescription();
            strategy.appendField(locator, this, "description", buffer, theDescription);
        }
        {
            Translations theTranslations;
            theTranslations = this.getTranslations();
            strategy.appendField(locator, this, "translations", buffer, theTranslations);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            Boolean theEnabled;
            theEnabled = this.getEnabled();
            strategy.appendField(locator, this, "enabled", buffer, theEnabled);
        }
        {
            String theText;
            theText = this.getText();
            strategy.appendField(locator, this, "text", buffer, theText);
        }
        {
            String theResourceId;
            theResourceId = this.getResourceId();
            strategy.appendField(locator, this, "resourceId", buffer, theResourceId);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Label)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Label that = ((Label) object);
        {
            JAXBElement<?> lhsLayoutconstraints;
            lhsLayoutconstraints = this.getLayoutconstraints();
            JAXBElement<?> rhsLayoutconstraints;
            rhsLayoutconstraints = that.getLayoutconstraints();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "layoutconstraints", lhsLayoutconstraints), LocatorUtils.property(thatLocator, "layoutconstraints", rhsLayoutconstraints), lhsLayoutconstraints, rhsLayoutconstraints)) {
                return false;
            }
        }
        {
            ClearBorder lhsClearBorder;
            lhsClearBorder = this.getClearBorder();
            ClearBorder rhsClearBorder;
            rhsClearBorder = that.getClearBorder();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "clearBorder", lhsClearBorder), LocatorUtils.property(thatLocator, "clearBorder", rhsClearBorder), lhsClearBorder, rhsClearBorder)) {
                return false;
            }
        }
        {
            List<JAXBElement<?>> lhsBorder;
            lhsBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            List<JAXBElement<?>> rhsBorder;
            rhsBorder = (((that.border!= null)&&(!that.border.isEmpty()))?that.getBorder():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "border", lhsBorder), LocatorUtils.property(thatLocator, "border", rhsBorder), lhsBorder, rhsBorder)) {
                return false;
            }
        }
        {
            MinimumSize lhsMinimumSize;
            lhsMinimumSize = this.getMinimumSize();
            MinimumSize rhsMinimumSize;
            rhsMinimumSize = that.getMinimumSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "minimumSize", lhsMinimumSize), LocatorUtils.property(thatLocator, "minimumSize", rhsMinimumSize), lhsMinimumSize, rhsMinimumSize)) {
                return false;
            }
        }
        {
            PreferredSize lhsPreferredSize;
            lhsPreferredSize = this.getPreferredSize();
            PreferredSize rhsPreferredSize;
            rhsPreferredSize = that.getPreferredSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "preferredSize", lhsPreferredSize), LocatorUtils.property(thatLocator, "preferredSize", rhsPreferredSize), lhsPreferredSize, rhsPreferredSize)) {
                return false;
            }
        }
        {
            StrictSize lhsStrictSize;
            lhsStrictSize = this.getStrictSize();
            StrictSize rhsStrictSize;
            rhsStrictSize = that.getStrictSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "strictSize", lhsStrictSize), LocatorUtils.property(thatLocator, "strictSize", rhsStrictSize), lhsStrictSize, rhsStrictSize)) {
                return false;
            }
        }
        {
            Font lhsFont;
            lhsFont = this.getFont();
            Font rhsFont;
            rhsFont = that.getFont();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "font", lhsFont), LocatorUtils.property(thatLocator, "font", rhsFont), lhsFont, rhsFont)) {
                return false;
            }
        }
        {
            TextFormat lhsTextFormat;
            lhsTextFormat = this.getTextFormat();
            TextFormat rhsTextFormat;
            rhsTextFormat = that.getTextFormat();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "textFormat", lhsTextFormat), LocatorUtils.property(thatLocator, "textFormat", rhsTextFormat), lhsTextFormat, rhsTextFormat)) {
                return false;
            }
        }
        {
            String lhsDescription;
            lhsDescription = this.getDescription();
            String rhsDescription;
            rhsDescription = that.getDescription();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "description", lhsDescription), LocatorUtils.property(thatLocator, "description", rhsDescription), lhsDescription, rhsDescription)) {
                return false;
            }
        }
        {
            Translations lhsTranslations;
            lhsTranslations = this.getTranslations();
            Translations rhsTranslations;
            rhsTranslations = that.getTranslations();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "translations", lhsTranslations), LocatorUtils.property(thatLocator, "translations", rhsTranslations), lhsTranslations, rhsTranslations)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            Boolean lhsEnabled;
            lhsEnabled = this.getEnabled();
            Boolean rhsEnabled;
            rhsEnabled = that.getEnabled();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "enabled", lhsEnabled), LocatorUtils.property(thatLocator, "enabled", rhsEnabled), lhsEnabled, rhsEnabled)) {
                return false;
            }
        }
        {
            String lhsText;
            lhsText = this.getText();
            String rhsText;
            rhsText = that.getText();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "text", lhsText), LocatorUtils.property(thatLocator, "text", rhsText), lhsText, rhsText)) {
                return false;
            }
        }
        {
            String lhsResourceId;
            lhsResourceId = this.getResourceId();
            String rhsResourceId;
            rhsResourceId = that.getResourceId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "resourceId", lhsResourceId), LocatorUtils.property(thatLocator, "resourceId", rhsResourceId), lhsResourceId, rhsResourceId)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            JAXBElement<?> theLayoutconstraints;
            theLayoutconstraints = this.getLayoutconstraints();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "layoutconstraints", theLayoutconstraints), currentHashCode, theLayoutconstraints);
        }
        {
            ClearBorder theClearBorder;
            theClearBorder = this.getClearBorder();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "clearBorder", theClearBorder), currentHashCode, theClearBorder);
        }
        {
            List<JAXBElement<?>> theBorder;
            theBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "border", theBorder), currentHashCode, theBorder);
        }
        {
            MinimumSize theMinimumSize;
            theMinimumSize = this.getMinimumSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "minimumSize", theMinimumSize), currentHashCode, theMinimumSize);
        }
        {
            PreferredSize thePreferredSize;
            thePreferredSize = this.getPreferredSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "preferredSize", thePreferredSize), currentHashCode, thePreferredSize);
        }
        {
            StrictSize theStrictSize;
            theStrictSize = this.getStrictSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "strictSize", theStrictSize), currentHashCode, theStrictSize);
        }
        {
            Font theFont;
            theFont = this.getFont();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "font", theFont), currentHashCode, theFont);
        }
        {
            TextFormat theTextFormat;
            theTextFormat = this.getTextFormat();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "textFormat", theTextFormat), currentHashCode, theTextFormat);
        }
        {
            String theDescription;
            theDescription = this.getDescription();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "description", theDescription), currentHashCode, theDescription);
        }
        {
            Translations theTranslations;
            theTranslations = this.getTranslations();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "translations", theTranslations), currentHashCode, theTranslations);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            Boolean theEnabled;
            theEnabled = this.getEnabled();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "enabled", theEnabled), currentHashCode, theEnabled);
        }
        {
            String theText;
            theText = this.getText();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "text", theText), currentHashCode, theText);
        }
        {
            String theResourceId;
            theResourceId = this.getResourceId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "resourceId", theResourceId), currentHashCode, theResourceId);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Label) {
            final Label copy = ((Label) draftCopy);
            if (this.layoutconstraints!= null) {
                JAXBElement<?> sourceLayoutconstraints;
                sourceLayoutconstraints = this.getLayoutconstraints();
                @SuppressWarnings("unchecked")
                JAXBElement<?> copyLayoutconstraints = ((JAXBElement<?> ) strategy.copy(LocatorUtils.property(locator, "layoutconstraints", sourceLayoutconstraints), sourceLayoutconstraints));
                copy.setLayoutconstraints(copyLayoutconstraints);
            } else {
                copy.layoutconstraints = null;
            }
            if (this.clearBorder!= null) {
                ClearBorder sourceClearBorder;
                sourceClearBorder = this.getClearBorder();
                ClearBorder copyClearBorder = ((ClearBorder) strategy.copy(LocatorUtils.property(locator, "clearBorder", sourceClearBorder), sourceClearBorder));
                copy.setClearBorder(copyClearBorder);
            } else {
                copy.clearBorder = null;
            }
            if ((this.border!= null)&&(!this.border.isEmpty())) {
                List<JAXBElement<?>> sourceBorder;
                sourceBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
                @SuppressWarnings("unchecked")
                List<JAXBElement<?>> copyBorder = ((List<JAXBElement<?>> ) strategy.copy(LocatorUtils.property(locator, "border", sourceBorder), sourceBorder));
                copy.border = null;
                if (copyBorder!= null) {
                    List<JAXBElement<?>> uniqueBorderl = copy.getBorder();
                    uniqueBorderl.addAll(copyBorder);
                }
            } else {
                copy.border = null;
            }
            if (this.minimumSize!= null) {
                MinimumSize sourceMinimumSize;
                sourceMinimumSize = this.getMinimumSize();
                MinimumSize copyMinimumSize = ((MinimumSize) strategy.copy(LocatorUtils.property(locator, "minimumSize", sourceMinimumSize), sourceMinimumSize));
                copy.setMinimumSize(copyMinimumSize);
            } else {
                copy.minimumSize = null;
            }
            if (this.preferredSize!= null) {
                PreferredSize sourcePreferredSize;
                sourcePreferredSize = this.getPreferredSize();
                PreferredSize copyPreferredSize = ((PreferredSize) strategy.copy(LocatorUtils.property(locator, "preferredSize", sourcePreferredSize), sourcePreferredSize));
                copy.setPreferredSize(copyPreferredSize);
            } else {
                copy.preferredSize = null;
            }
            if (this.strictSize!= null) {
                StrictSize sourceStrictSize;
                sourceStrictSize = this.getStrictSize();
                StrictSize copyStrictSize = ((StrictSize) strategy.copy(LocatorUtils.property(locator, "strictSize", sourceStrictSize), sourceStrictSize));
                copy.setStrictSize(copyStrictSize);
            } else {
                copy.strictSize = null;
            }
            if (this.font!= null) {
                Font sourceFont;
                sourceFont = this.getFont();
                Font copyFont = ((Font) strategy.copy(LocatorUtils.property(locator, "font", sourceFont), sourceFont));
                copy.setFont(copyFont);
            } else {
                copy.font = null;
            }
            if (this.textFormat!= null) {
                TextFormat sourceTextFormat;
                sourceTextFormat = this.getTextFormat();
                TextFormat copyTextFormat = ((TextFormat) strategy.copy(LocatorUtils.property(locator, "textFormat", sourceTextFormat), sourceTextFormat));
                copy.setTextFormat(copyTextFormat);
            } else {
                copy.textFormat = null;
            }
            if (this.description!= null) {
                String sourceDescription;
                sourceDescription = this.getDescription();
                String copyDescription = ((String) strategy.copy(LocatorUtils.property(locator, "description", sourceDescription), sourceDescription));
                copy.setDescription(copyDescription);
            } else {
                copy.description = null;
            }
            if (this.translations!= null) {
                Translations sourceTranslations;
                sourceTranslations = this.getTranslations();
                Translations copyTranslations = ((Translations) strategy.copy(LocatorUtils.property(locator, "translations", sourceTranslations), sourceTranslations));
                copy.setTranslations(copyTranslations);
            } else {
                copy.translations = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.enabled!= null) {
                Boolean sourceEnabled;
                sourceEnabled = this.getEnabled();
                Boolean copyEnabled = ((Boolean) strategy.copy(LocatorUtils.property(locator, "enabled", sourceEnabled), sourceEnabled));
                copy.setEnabled(copyEnabled);
            } else {
                copy.enabled = null;
            }
            if (this.text!= null) {
                String sourceText;
                sourceText = this.getText();
                String copyText = ((String) strategy.copy(LocatorUtils.property(locator, "text", sourceText), sourceText));
                copy.setText(copyText);
            } else {
                copy.text = null;
            }
            if (this.resourceId!= null) {
                String sourceResourceId;
                sourceResourceId = this.getResourceId();
                String copyResourceId = ((String) strategy.copy(LocatorUtils.property(locator, "resourceId", sourceResourceId), sourceResourceId));
                copy.setResourceId(copyResourceId);
            } else {
                copy.resourceId = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Label();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Label.Builder<_B> _other) {
        _other.layoutconstraints = this.layoutconstraints;
        _other.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.newCopyBuilder(_other));
        if (this.border == null) {
            _other.border = null;
        } else {
            _other.border = new ArrayList<Buildable>();
            for (JAXBElement<?> _item: this.border) {
                _other.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
            }
        }
        _other.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.newCopyBuilder(_other));
        _other.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.newCopyBuilder(_other));
        _other.strictSize = ((this.strictSize == null)?null:this.strictSize.newCopyBuilder(_other));
        _other.font = ((this.font == null)?null:this.font.newCopyBuilder(_other));
        _other.textFormat = ((this.textFormat == null)?null:this.textFormat.newCopyBuilder(_other));
        _other.description = this.description;
        _other.translations = ((this.translations == null)?null:this.translations.newCopyBuilder(_other));
        _other.name = this.name;
        _other.enabled = this.enabled;
        _other.text = this.text;
        _other.resourceId = this.resourceId;
    }

    public<_B >Label.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Label.Builder<_B>(_parentBuilder, this, true);
    }

    public Label.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Label.Builder<Void> builder() {
        return new Label.Builder<Void>(null, null, false);
    }

    public static<_B >Label.Builder<_B> copyOf(final Label _other) {
        final Label.Builder<_B> _newBuilder = new Label.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Label.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree layoutconstraintsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("layoutconstraints"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(layoutconstraintsPropertyTree!= null):((layoutconstraintsPropertyTree == null)||(!layoutconstraintsPropertyTree.isLeaf())))) {
            _other.layoutconstraints = this.layoutconstraints;
        }
        final PropertyTree clearBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("clearBorder"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(clearBorderPropertyTree!= null):((clearBorderPropertyTree == null)||(!clearBorderPropertyTree.isLeaf())))) {
            _other.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.newCopyBuilder(_other, clearBorderPropertyTree, _propertyTreeUse));
        }
        final PropertyTree borderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("border"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(borderPropertyTree!= null):((borderPropertyTree == null)||(!borderPropertyTree.isLeaf())))) {
            if (this.border == null) {
                _other.border = null;
            } else {
                _other.border = new ArrayList<Buildable>();
                for (JAXBElement<?> _item: this.border) {
                    _other.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                }
            }
        }
        final PropertyTree minimumSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minimumSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minimumSizePropertyTree!= null):((minimumSizePropertyTree == null)||(!minimumSizePropertyTree.isLeaf())))) {
            _other.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.newCopyBuilder(_other, minimumSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree preferredSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("preferredSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(preferredSizePropertyTree!= null):((preferredSizePropertyTree == null)||(!preferredSizePropertyTree.isLeaf())))) {
            _other.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.newCopyBuilder(_other, preferredSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree strictSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("strictSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(strictSizePropertyTree!= null):((strictSizePropertyTree == null)||(!strictSizePropertyTree.isLeaf())))) {
            _other.strictSize = ((this.strictSize == null)?null:this.strictSize.newCopyBuilder(_other, strictSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree fontPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("font"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fontPropertyTree!= null):((fontPropertyTree == null)||(!fontPropertyTree.isLeaf())))) {
            _other.font = ((this.font == null)?null:this.font.newCopyBuilder(_other, fontPropertyTree, _propertyTreeUse));
        }
        final PropertyTree textFormatPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("textFormat"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(textFormatPropertyTree!= null):((textFormatPropertyTree == null)||(!textFormatPropertyTree.isLeaf())))) {
            _other.textFormat = ((this.textFormat == null)?null:this.textFormat.newCopyBuilder(_other, textFormatPropertyTree, _propertyTreeUse));
        }
        final PropertyTree descriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("description"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(descriptionPropertyTree!= null):((descriptionPropertyTree == null)||(!descriptionPropertyTree.isLeaf())))) {
            _other.description = this.description;
        }
        final PropertyTree translationsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("translations"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(translationsPropertyTree!= null):((translationsPropertyTree == null)||(!translationsPropertyTree.isLeaf())))) {
            _other.translations = ((this.translations == null)?null:this.translations.newCopyBuilder(_other, translationsPropertyTree, _propertyTreeUse));
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree enabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("enabled"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(enabledPropertyTree!= null):((enabledPropertyTree == null)||(!enabledPropertyTree.isLeaf())))) {
            _other.enabled = this.enabled;
        }
        final PropertyTree textPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("text"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(textPropertyTree!= null):((textPropertyTree == null)||(!textPropertyTree.isLeaf())))) {
            _other.text = this.text;
        }
        final PropertyTree resourceIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resourceId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resourceIdPropertyTree!= null):((resourceIdPropertyTree == null)||(!resourceIdPropertyTree.isLeaf())))) {
            _other.resourceId = this.resourceId;
        }
    }

    public<_B >Label.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Label.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Label.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Label.Builder<_B> copyOf(final Label _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Label.Builder<_B> _newBuilder = new Label.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Label.Builder<Void> copyExcept(final Label _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Label.Builder<Void> copyOnly(final Label _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Label _storedValue;
        private JAXBElement<?> layoutconstraints;
        private ClearBorder.Builder<Label.Builder<_B>> clearBorder;
        private List<Buildable> border;
        private MinimumSize.Builder<Label.Builder<_B>> minimumSize;
        private PreferredSize.Builder<Label.Builder<_B>> preferredSize;
        private StrictSize.Builder<Label.Builder<_B>> strictSize;
        private Font.Builder<Label.Builder<_B>> font;
        private TextFormat.Builder<Label.Builder<_B>> textFormat;
        private String description;
        private Translations.Builder<Label.Builder<_B>> translations;
        private String name;
        private Boolean enabled;
        private String text;
        private String resourceId;

        public Builder(final _B _parentBuilder, final Label _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.layoutconstraints = _other.layoutconstraints;
                    this.clearBorder = ((_other.clearBorder == null)?null:_other.clearBorder.newCopyBuilder(this));
                    if (_other.border == null) {
                        this.border = null;
                    } else {
                        this.border = new ArrayList<Buildable>();
                        for (JAXBElement<?> _item: _other.border) {
                            this.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                        }
                    }
                    this.minimumSize = ((_other.minimumSize == null)?null:_other.minimumSize.newCopyBuilder(this));
                    this.preferredSize = ((_other.preferredSize == null)?null:_other.preferredSize.newCopyBuilder(this));
                    this.strictSize = ((_other.strictSize == null)?null:_other.strictSize.newCopyBuilder(this));
                    this.font = ((_other.font == null)?null:_other.font.newCopyBuilder(this));
                    this.textFormat = ((_other.textFormat == null)?null:_other.textFormat.newCopyBuilder(this));
                    this.description = _other.description;
                    this.translations = ((_other.translations == null)?null:_other.translations.newCopyBuilder(this));
                    this.name = _other.name;
                    this.enabled = _other.enabled;
                    this.text = _other.text;
                    this.resourceId = _other.resourceId;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Label _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree layoutconstraintsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("layoutconstraints"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(layoutconstraintsPropertyTree!= null):((layoutconstraintsPropertyTree == null)||(!layoutconstraintsPropertyTree.isLeaf())))) {
                        this.layoutconstraints = _other.layoutconstraints;
                    }
                    final PropertyTree clearBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("clearBorder"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(clearBorderPropertyTree!= null):((clearBorderPropertyTree == null)||(!clearBorderPropertyTree.isLeaf())))) {
                        this.clearBorder = ((_other.clearBorder == null)?null:_other.clearBorder.newCopyBuilder(this, clearBorderPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree borderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("border"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(borderPropertyTree!= null):((borderPropertyTree == null)||(!borderPropertyTree.isLeaf())))) {
                        if (_other.border == null) {
                            this.border = null;
                        } else {
                            this.border = new ArrayList<Buildable>();
                            for (JAXBElement<?> _item: _other.border) {
                                this.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                            }
                        }
                    }
                    final PropertyTree minimumSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minimumSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minimumSizePropertyTree!= null):((minimumSizePropertyTree == null)||(!minimumSizePropertyTree.isLeaf())))) {
                        this.minimumSize = ((_other.minimumSize == null)?null:_other.minimumSize.newCopyBuilder(this, minimumSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree preferredSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("preferredSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(preferredSizePropertyTree!= null):((preferredSizePropertyTree == null)||(!preferredSizePropertyTree.isLeaf())))) {
                        this.preferredSize = ((_other.preferredSize == null)?null:_other.preferredSize.newCopyBuilder(this, preferredSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree strictSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("strictSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(strictSizePropertyTree!= null):((strictSizePropertyTree == null)||(!strictSizePropertyTree.isLeaf())))) {
                        this.strictSize = ((_other.strictSize == null)?null:_other.strictSize.newCopyBuilder(this, strictSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree fontPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("font"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fontPropertyTree!= null):((fontPropertyTree == null)||(!fontPropertyTree.isLeaf())))) {
                        this.font = ((_other.font == null)?null:_other.font.newCopyBuilder(this, fontPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree textFormatPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("textFormat"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(textFormatPropertyTree!= null):((textFormatPropertyTree == null)||(!textFormatPropertyTree.isLeaf())))) {
                        this.textFormat = ((_other.textFormat == null)?null:_other.textFormat.newCopyBuilder(this, textFormatPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree descriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("description"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(descriptionPropertyTree!= null):((descriptionPropertyTree == null)||(!descriptionPropertyTree.isLeaf())))) {
                        this.description = _other.description;
                    }
                    final PropertyTree translationsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("translations"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(translationsPropertyTree!= null):((translationsPropertyTree == null)||(!translationsPropertyTree.isLeaf())))) {
                        this.translations = ((_other.translations == null)?null:_other.translations.newCopyBuilder(this, translationsPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree enabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("enabled"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(enabledPropertyTree!= null):((enabledPropertyTree == null)||(!enabledPropertyTree.isLeaf())))) {
                        this.enabled = _other.enabled;
                    }
                    final PropertyTree textPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("text"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(textPropertyTree!= null):((textPropertyTree == null)||(!textPropertyTree.isLeaf())))) {
                        this.text = _other.text;
                    }
                    final PropertyTree resourceIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resourceId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resourceIdPropertyTree!= null):((resourceIdPropertyTree == null)||(!resourceIdPropertyTree.isLeaf())))) {
                        this.resourceId = _other.resourceId;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Label >_P init(final _P _product) {
            _product.layoutconstraints = this.layoutconstraints;
            _product.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.build());
            if (this.border!= null) {
                final List<JAXBElement<?>> border = new ArrayList<JAXBElement<?>>(this.border.size());
                for (Buildable _item: this.border) {
                    border.add(((JAXBElement<?> ) _item.build()));
                }
                _product.border = border;
            }
            _product.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.build());
            _product.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.build());
            _product.strictSize = ((this.strictSize == null)?null:this.strictSize.build());
            _product.font = ((this.font == null)?null:this.font.build());
            _product.textFormat = ((this.textFormat == null)?null:this.textFormat.build());
            _product.description = this.description;
            _product.translations = ((this.translations == null)?null:this.translations.build());
            _product.name = this.name;
            _product.enabled = this.enabled;
            _product.text = this.text;
            _product.resourceId = this.resourceId;
            return _product;
        }

        /**
         * Sets the new value of "layoutconstraints" (any previous value will be replaced)
         * 
         * @param layoutconstraints
         *     New value of the "layoutconstraints" property.
         */
        public Label.Builder<_B> withLayoutconstraints(final JAXBElement<?> layoutconstraints) {
            this.layoutconstraints = layoutconstraints;
            return this;
        }

        /**
         * Sets the new value of "clearBorder" (any previous value will be replaced)
         * 
         * @param clearBorder
         *     New value of the "clearBorder" property.
         */
        public Label.Builder<_B> withClearBorder(final ClearBorder clearBorder) {
            this.clearBorder = ((clearBorder == null)?null:new ClearBorder.Builder<Label.Builder<_B>>(this, clearBorder, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "clearBorder" property.
         * Use {@link org.nuclos.schema.layout.layoutml.ClearBorder.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "clearBorder" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.ClearBorder.Builder#end()} to return to the current builder.
         */
        public ClearBorder.Builder<? extends Label.Builder<_B>> withClearBorder() {
            if (this.clearBorder!= null) {
                return this.clearBorder;
            }
            return this.clearBorder = new ClearBorder.Builder<Label.Builder<_B>>(this, null, false);
        }

        /**
         * Adds the given items to the value of "border"
         * 
         * @param border
         *     Items to add to the value of the "border" property
         */
        public Label.Builder<_B> addBorder(final Iterable<? extends JAXBElement<?>> border) {
            if (border!= null) {
                if (this.border == null) {
                    this.border = new ArrayList<Buildable>();
                }
                for (JAXBElement<?> _item: border) {
                    this.border.add(new Buildable.PrimitiveBuildable(_item));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "border" (any previous value will be replaced)
         * 
         * @param border
         *     New value of the "border" property.
         */
        public Label.Builder<_B> withBorder(final Iterable<? extends JAXBElement<?>> border) {
            if (this.border!= null) {
                this.border.clear();
            }
            return addBorder(border);
        }

        /**
         * Adds the given items to the value of "border"
         * 
         * @param border
         *     Items to add to the value of the "border" property
         */
        public Label.Builder<_B> addBorder(JAXBElement<?> ... border) {
            addBorder(Arrays.asList(border));
            return this;
        }

        /**
         * Sets the new value of "border" (any previous value will be replaced)
         * 
         * @param border
         *     New value of the "border" property.
         */
        public Label.Builder<_B> withBorder(JAXBElement<?> ... border) {
            withBorder(Arrays.asList(border));
            return this;
        }

        /**
         * Sets the new value of "minimumSize" (any previous value will be replaced)
         * 
         * @param minimumSize
         *     New value of the "minimumSize" property.
         */
        public Label.Builder<_B> withMinimumSize(final MinimumSize minimumSize) {
            this.minimumSize = ((minimumSize == null)?null:new MinimumSize.Builder<Label.Builder<_B>>(this, minimumSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "minimumSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.MinimumSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "minimumSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.MinimumSize.Builder#end()} to return to the current builder.
         */
        public MinimumSize.Builder<? extends Label.Builder<_B>> withMinimumSize() {
            if (this.minimumSize!= null) {
                return this.minimumSize;
            }
            return this.minimumSize = new MinimumSize.Builder<Label.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "preferredSize" (any previous value will be replaced)
         * 
         * @param preferredSize
         *     New value of the "preferredSize" property.
         */
        public Label.Builder<_B> withPreferredSize(final PreferredSize preferredSize) {
            this.preferredSize = ((preferredSize == null)?null:new PreferredSize.Builder<Label.Builder<_B>>(this, preferredSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "preferredSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.PreferredSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "preferredSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.PreferredSize.Builder#end()} to return to the current builder.
         */
        public PreferredSize.Builder<? extends Label.Builder<_B>> withPreferredSize() {
            if (this.preferredSize!= null) {
                return this.preferredSize;
            }
            return this.preferredSize = new PreferredSize.Builder<Label.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "strictSize" (any previous value will be replaced)
         * 
         * @param strictSize
         *     New value of the "strictSize" property.
         */
        public Label.Builder<_B> withStrictSize(final StrictSize strictSize) {
            this.strictSize = ((strictSize == null)?null:new StrictSize.Builder<Label.Builder<_B>>(this, strictSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "strictSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.StrictSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "strictSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.StrictSize.Builder#end()} to return to the current builder.
         */
        public StrictSize.Builder<? extends Label.Builder<_B>> withStrictSize() {
            if (this.strictSize!= null) {
                return this.strictSize;
            }
            return this.strictSize = new StrictSize.Builder<Label.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "font" (any previous value will be replaced)
         * 
         * @param font
         *     New value of the "font" property.
         */
        public Label.Builder<_B> withFont(final Font font) {
            this.font = ((font == null)?null:new Font.Builder<Label.Builder<_B>>(this, font, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "font" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Font.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "font" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Font.Builder#end()} to return to the current builder.
         */
        public Font.Builder<? extends Label.Builder<_B>> withFont() {
            if (this.font!= null) {
                return this.font;
            }
            return this.font = new Font.Builder<Label.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "textFormat" (any previous value will be replaced)
         * 
         * @param textFormat
         *     New value of the "textFormat" property.
         */
        public Label.Builder<_B> withTextFormat(final TextFormat textFormat) {
            this.textFormat = ((textFormat == null)?null:new TextFormat.Builder<Label.Builder<_B>>(this, textFormat, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "textFormat" property.
         * Use {@link org.nuclos.schema.layout.layoutml.TextFormat.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "textFormat" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.TextFormat.Builder#end()} to return to the current builder.
         */
        public TextFormat.Builder<? extends Label.Builder<_B>> withTextFormat() {
            if (this.textFormat!= null) {
                return this.textFormat;
            }
            return this.textFormat = new TextFormat.Builder<Label.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "description" (any previous value will be replaced)
         * 
         * @param description
         *     New value of the "description" property.
         */
        public Label.Builder<_B> withDescription(final String description) {
            this.description = description;
            return this;
        }

        /**
         * Sets the new value of "translations" (any previous value will be replaced)
         * 
         * @param translations
         *     New value of the "translations" property.
         */
        public Label.Builder<_B> withTranslations(final Translations translations) {
            this.translations = ((translations == null)?null:new Translations.Builder<Label.Builder<_B>>(this, translations, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "translations" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Translations.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "translations" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Translations.Builder#end()} to return to the current builder.
         */
        public Translations.Builder<? extends Label.Builder<_B>> withTranslations() {
            if (this.translations!= null) {
                return this.translations;
            }
            return this.translations = new Translations.Builder<Label.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public Label.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "enabled" (any previous value will be replaced)
         * 
         * @param enabled
         *     New value of the "enabled" property.
         */
        public Label.Builder<_B> withEnabled(final Boolean enabled) {
            this.enabled = enabled;
            return this;
        }

        /**
         * Sets the new value of "text" (any previous value will be replaced)
         * 
         * @param text
         *     New value of the "text" property.
         */
        public Label.Builder<_B> withText(final String text) {
            this.text = text;
            return this;
        }

        /**
         * Sets the new value of "resourceId" (any previous value will be replaced)
         * 
         * @param resourceId
         *     New value of the "resourceId" property.
         */
        public Label.Builder<_B> withResourceId(final String resourceId) {
            this.resourceId = resourceId;
            return this;
        }

        @Override
        public Label build() {
            if (_storedValue == null) {
                return this.init(new Label());
            } else {
                return ((Label) _storedValue);
            }
        }

        public Label.Builder<_B> copyOf(final Label _other) {
            _other.copyTo(this);
            return this;
        }

        public Label.Builder<_B> copyOf(final Label.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Label.Selector<Label.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Label.Select _root() {
            return new Label.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, Label.Selector<TRoot, TParent>> layoutconstraints = null;
        private ClearBorder.Selector<TRoot, Label.Selector<TRoot, TParent>> clearBorder = null;
        private com.kscs.util.jaxb.Selector<TRoot, Label.Selector<TRoot, TParent>> border = null;
        private MinimumSize.Selector<TRoot, Label.Selector<TRoot, TParent>> minimumSize = null;
        private PreferredSize.Selector<TRoot, Label.Selector<TRoot, TParent>> preferredSize = null;
        private StrictSize.Selector<TRoot, Label.Selector<TRoot, TParent>> strictSize = null;
        private Font.Selector<TRoot, Label.Selector<TRoot, TParent>> font = null;
        private TextFormat.Selector<TRoot, Label.Selector<TRoot, TParent>> textFormat = null;
        private com.kscs.util.jaxb.Selector<TRoot, Label.Selector<TRoot, TParent>> description = null;
        private Translations.Selector<TRoot, Label.Selector<TRoot, TParent>> translations = null;
        private com.kscs.util.jaxb.Selector<TRoot, Label.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, Label.Selector<TRoot, TParent>> enabled = null;
        private com.kscs.util.jaxb.Selector<TRoot, Label.Selector<TRoot, TParent>> text = null;
        private com.kscs.util.jaxb.Selector<TRoot, Label.Selector<TRoot, TParent>> resourceId = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.layoutconstraints!= null) {
                products.put("layoutconstraints", this.layoutconstraints.init());
            }
            if (this.clearBorder!= null) {
                products.put("clearBorder", this.clearBorder.init());
            }
            if (this.border!= null) {
                products.put("border", this.border.init());
            }
            if (this.minimumSize!= null) {
                products.put("minimumSize", this.minimumSize.init());
            }
            if (this.preferredSize!= null) {
                products.put("preferredSize", this.preferredSize.init());
            }
            if (this.strictSize!= null) {
                products.put("strictSize", this.strictSize.init());
            }
            if (this.font!= null) {
                products.put("font", this.font.init());
            }
            if (this.textFormat!= null) {
                products.put("textFormat", this.textFormat.init());
            }
            if (this.description!= null) {
                products.put("description", this.description.init());
            }
            if (this.translations!= null) {
                products.put("translations", this.translations.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.enabled!= null) {
                products.put("enabled", this.enabled.init());
            }
            if (this.text!= null) {
                products.put("text", this.text.init());
            }
            if (this.resourceId!= null) {
                products.put("resourceId", this.resourceId.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, Label.Selector<TRoot, TParent>> layoutconstraints() {
            return ((this.layoutconstraints == null)?this.layoutconstraints = new com.kscs.util.jaxb.Selector<TRoot, Label.Selector<TRoot, TParent>>(this._root, this, "layoutconstraints"):this.layoutconstraints);
        }

        public ClearBorder.Selector<TRoot, Label.Selector<TRoot, TParent>> clearBorder() {
            return ((this.clearBorder == null)?this.clearBorder = new ClearBorder.Selector<TRoot, Label.Selector<TRoot, TParent>>(this._root, this, "clearBorder"):this.clearBorder);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Label.Selector<TRoot, TParent>> border() {
            return ((this.border == null)?this.border = new com.kscs.util.jaxb.Selector<TRoot, Label.Selector<TRoot, TParent>>(this._root, this, "border"):this.border);
        }

        public MinimumSize.Selector<TRoot, Label.Selector<TRoot, TParent>> minimumSize() {
            return ((this.minimumSize == null)?this.minimumSize = new MinimumSize.Selector<TRoot, Label.Selector<TRoot, TParent>>(this._root, this, "minimumSize"):this.minimumSize);
        }

        public PreferredSize.Selector<TRoot, Label.Selector<TRoot, TParent>> preferredSize() {
            return ((this.preferredSize == null)?this.preferredSize = new PreferredSize.Selector<TRoot, Label.Selector<TRoot, TParent>>(this._root, this, "preferredSize"):this.preferredSize);
        }

        public StrictSize.Selector<TRoot, Label.Selector<TRoot, TParent>> strictSize() {
            return ((this.strictSize == null)?this.strictSize = new StrictSize.Selector<TRoot, Label.Selector<TRoot, TParent>>(this._root, this, "strictSize"):this.strictSize);
        }

        public Font.Selector<TRoot, Label.Selector<TRoot, TParent>> font() {
            return ((this.font == null)?this.font = new Font.Selector<TRoot, Label.Selector<TRoot, TParent>>(this._root, this, "font"):this.font);
        }

        public TextFormat.Selector<TRoot, Label.Selector<TRoot, TParent>> textFormat() {
            return ((this.textFormat == null)?this.textFormat = new TextFormat.Selector<TRoot, Label.Selector<TRoot, TParent>>(this._root, this, "textFormat"):this.textFormat);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Label.Selector<TRoot, TParent>> description() {
            return ((this.description == null)?this.description = new com.kscs.util.jaxb.Selector<TRoot, Label.Selector<TRoot, TParent>>(this._root, this, "description"):this.description);
        }

        public Translations.Selector<TRoot, Label.Selector<TRoot, TParent>> translations() {
            return ((this.translations == null)?this.translations = new Translations.Selector<TRoot, Label.Selector<TRoot, TParent>>(this._root, this, "translations"):this.translations);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Label.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, Label.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Label.Selector<TRoot, TParent>> enabled() {
            return ((this.enabled == null)?this.enabled = new com.kscs.util.jaxb.Selector<TRoot, Label.Selector<TRoot, TParent>>(this._root, this, "enabled"):this.enabled);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Label.Selector<TRoot, TParent>> text() {
            return ((this.text == null)?this.text = new com.kscs.util.jaxb.Selector<TRoot, Label.Selector<TRoot, TParent>>(this._root, this, "text"):this.text);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Label.Selector<TRoot, TParent>> resourceId() {
            return ((this.resourceId == null)?this.resourceId = new com.kscs.util.jaxb.Selector<TRoot, Label.Selector<TRoot, TParent>>(this._root, this, "resourceId"):this.resourceId);
        }

    }

}
