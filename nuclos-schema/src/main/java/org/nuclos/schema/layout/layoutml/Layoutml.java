
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}definitions" minOccurs="0"/&gt;
 *         &lt;element ref="{}layout"/&gt;
 *         &lt;element ref="{}dependencies" minOccurs="0"/&gt;
 *         &lt;element ref="{}rules" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "definitions",
    "layout",
    "dependencies",
    "rules"
})
@XmlRootElement(name = "layoutml")
public class Layoutml implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected Definitions definitions;
    @XmlElement(required = true)
    protected Layout layout;
    protected Dependencies dependencies;
    protected Rules rules;

    /**
     * Gets the value of the definitions property.
     * 
     * @return
     *     possible object is
     *     {@link Definitions }
     *     
     */
    public Definitions getDefinitions() {
        return definitions;
    }

    /**
     * Sets the value of the definitions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Definitions }
     *     
     */
    public void setDefinitions(Definitions value) {
        this.definitions = value;
    }

    /**
     * Gets the value of the layout property.
     * 
     * @return
     *     possible object is
     *     {@link Layout }
     *     
     */
    public Layout getLayout() {
        return layout;
    }

    /**
     * Sets the value of the layout property.
     * 
     * @param value
     *     allowed object is
     *     {@link Layout }
     *     
     */
    public void setLayout(Layout value) {
        this.layout = value;
    }

    /**
     * Gets the value of the dependencies property.
     * 
     * @return
     *     possible object is
     *     {@link Dependencies }
     *     
     */
    public Dependencies getDependencies() {
        return dependencies;
    }

    /**
     * Sets the value of the dependencies property.
     * 
     * @param value
     *     allowed object is
     *     {@link Dependencies }
     *     
     */
    public void setDependencies(Dependencies value) {
        this.dependencies = value;
    }

    /**
     * Gets the value of the rules property.
     * 
     * @return
     *     possible object is
     *     {@link Rules }
     *     
     */
    public Rules getRules() {
        return rules;
    }

    /**
     * Sets the value of the rules property.
     * 
     * @param value
     *     allowed object is
     *     {@link Rules }
     *     
     */
    public void setRules(Rules value) {
        this.rules = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            Definitions theDefinitions;
            theDefinitions = this.getDefinitions();
            strategy.appendField(locator, this, "definitions", buffer, theDefinitions);
        }
        {
            Layout theLayout;
            theLayout = this.getLayout();
            strategy.appendField(locator, this, "layout", buffer, theLayout);
        }
        {
            Dependencies theDependencies;
            theDependencies = this.getDependencies();
            strategy.appendField(locator, this, "dependencies", buffer, theDependencies);
        }
        {
            Rules theRules;
            theRules = this.getRules();
            strategy.appendField(locator, this, "rules", buffer, theRules);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Layoutml)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Layoutml that = ((Layoutml) object);
        {
            Definitions lhsDefinitions;
            lhsDefinitions = this.getDefinitions();
            Definitions rhsDefinitions;
            rhsDefinitions = that.getDefinitions();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "definitions", lhsDefinitions), LocatorUtils.property(thatLocator, "definitions", rhsDefinitions), lhsDefinitions, rhsDefinitions)) {
                return false;
            }
        }
        {
            Layout lhsLayout;
            lhsLayout = this.getLayout();
            Layout rhsLayout;
            rhsLayout = that.getLayout();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "layout", lhsLayout), LocatorUtils.property(thatLocator, "layout", rhsLayout), lhsLayout, rhsLayout)) {
                return false;
            }
        }
        {
            Dependencies lhsDependencies;
            lhsDependencies = this.getDependencies();
            Dependencies rhsDependencies;
            rhsDependencies = that.getDependencies();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dependencies", lhsDependencies), LocatorUtils.property(thatLocator, "dependencies", rhsDependencies), lhsDependencies, rhsDependencies)) {
                return false;
            }
        }
        {
            Rules lhsRules;
            lhsRules = this.getRules();
            Rules rhsRules;
            rhsRules = that.getRules();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "rules", lhsRules), LocatorUtils.property(thatLocator, "rules", rhsRules), lhsRules, rhsRules)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            Definitions theDefinitions;
            theDefinitions = this.getDefinitions();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "definitions", theDefinitions), currentHashCode, theDefinitions);
        }
        {
            Layout theLayout;
            theLayout = this.getLayout();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "layout", theLayout), currentHashCode, theLayout);
        }
        {
            Dependencies theDependencies;
            theDependencies = this.getDependencies();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dependencies", theDependencies), currentHashCode, theDependencies);
        }
        {
            Rules theRules;
            theRules = this.getRules();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "rules", theRules), currentHashCode, theRules);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Layoutml) {
            final Layoutml copy = ((Layoutml) draftCopy);
            if (this.definitions!= null) {
                Definitions sourceDefinitions;
                sourceDefinitions = this.getDefinitions();
                Definitions copyDefinitions = ((Definitions) strategy.copy(LocatorUtils.property(locator, "definitions", sourceDefinitions), sourceDefinitions));
                copy.setDefinitions(copyDefinitions);
            } else {
                copy.definitions = null;
            }
            if (this.layout!= null) {
                Layout sourceLayout;
                sourceLayout = this.getLayout();
                Layout copyLayout = ((Layout) strategy.copy(LocatorUtils.property(locator, "layout", sourceLayout), sourceLayout));
                copy.setLayout(copyLayout);
            } else {
                copy.layout = null;
            }
            if (this.dependencies!= null) {
                Dependencies sourceDependencies;
                sourceDependencies = this.getDependencies();
                Dependencies copyDependencies = ((Dependencies) strategy.copy(LocatorUtils.property(locator, "dependencies", sourceDependencies), sourceDependencies));
                copy.setDependencies(copyDependencies);
            } else {
                copy.dependencies = null;
            }
            if (this.rules!= null) {
                Rules sourceRules;
                sourceRules = this.getRules();
                Rules copyRules = ((Rules) strategy.copy(LocatorUtils.property(locator, "rules", sourceRules), sourceRules));
                copy.setRules(copyRules);
            } else {
                copy.rules = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Layoutml();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Layoutml.Builder<_B> _other) {
        _other.definitions = ((this.definitions == null)?null:this.definitions.newCopyBuilder(_other));
        _other.layout = ((this.layout == null)?null:this.layout.newCopyBuilder(_other));
        _other.dependencies = ((this.dependencies == null)?null:this.dependencies.newCopyBuilder(_other));
        _other.rules = ((this.rules == null)?null:this.rules.newCopyBuilder(_other));
    }

    public<_B >Layoutml.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Layoutml.Builder<_B>(_parentBuilder, this, true);
    }

    public Layoutml.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Layoutml.Builder<Void> builder() {
        return new Layoutml.Builder<Void>(null, null, false);
    }

    public static<_B >Layoutml.Builder<_B> copyOf(final Layoutml _other) {
        final Layoutml.Builder<_B> _newBuilder = new Layoutml.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Layoutml.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree definitionsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("definitions"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(definitionsPropertyTree!= null):((definitionsPropertyTree == null)||(!definitionsPropertyTree.isLeaf())))) {
            _other.definitions = ((this.definitions == null)?null:this.definitions.newCopyBuilder(_other, definitionsPropertyTree, _propertyTreeUse));
        }
        final PropertyTree layoutPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("layout"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(layoutPropertyTree!= null):((layoutPropertyTree == null)||(!layoutPropertyTree.isLeaf())))) {
            _other.layout = ((this.layout == null)?null:this.layout.newCopyBuilder(_other, layoutPropertyTree, _propertyTreeUse));
        }
        final PropertyTree dependenciesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dependencies"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dependenciesPropertyTree!= null):((dependenciesPropertyTree == null)||(!dependenciesPropertyTree.isLeaf())))) {
            _other.dependencies = ((this.dependencies == null)?null:this.dependencies.newCopyBuilder(_other, dependenciesPropertyTree, _propertyTreeUse));
        }
        final PropertyTree rulesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rules"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rulesPropertyTree!= null):((rulesPropertyTree == null)||(!rulesPropertyTree.isLeaf())))) {
            _other.rules = ((this.rules == null)?null:this.rules.newCopyBuilder(_other, rulesPropertyTree, _propertyTreeUse));
        }
    }

    public<_B >Layoutml.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Layoutml.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Layoutml.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Layoutml.Builder<_B> copyOf(final Layoutml _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Layoutml.Builder<_B> _newBuilder = new Layoutml.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Layoutml.Builder<Void> copyExcept(final Layoutml _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Layoutml.Builder<Void> copyOnly(final Layoutml _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Layoutml _storedValue;
        private Definitions.Builder<Layoutml.Builder<_B>> definitions;
        private Layout.Builder<Layoutml.Builder<_B>> layout;
        private Dependencies.Builder<Layoutml.Builder<_B>> dependencies;
        private Rules.Builder<Layoutml.Builder<_B>> rules;

        public Builder(final _B _parentBuilder, final Layoutml _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.definitions = ((_other.definitions == null)?null:_other.definitions.newCopyBuilder(this));
                    this.layout = ((_other.layout == null)?null:_other.layout.newCopyBuilder(this));
                    this.dependencies = ((_other.dependencies == null)?null:_other.dependencies.newCopyBuilder(this));
                    this.rules = ((_other.rules == null)?null:_other.rules.newCopyBuilder(this));
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Layoutml _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree definitionsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("definitions"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(definitionsPropertyTree!= null):((definitionsPropertyTree == null)||(!definitionsPropertyTree.isLeaf())))) {
                        this.definitions = ((_other.definitions == null)?null:_other.definitions.newCopyBuilder(this, definitionsPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree layoutPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("layout"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(layoutPropertyTree!= null):((layoutPropertyTree == null)||(!layoutPropertyTree.isLeaf())))) {
                        this.layout = ((_other.layout == null)?null:_other.layout.newCopyBuilder(this, layoutPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree dependenciesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dependencies"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dependenciesPropertyTree!= null):((dependenciesPropertyTree == null)||(!dependenciesPropertyTree.isLeaf())))) {
                        this.dependencies = ((_other.dependencies == null)?null:_other.dependencies.newCopyBuilder(this, dependenciesPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree rulesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rules"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rulesPropertyTree!= null):((rulesPropertyTree == null)||(!rulesPropertyTree.isLeaf())))) {
                        this.rules = ((_other.rules == null)?null:_other.rules.newCopyBuilder(this, rulesPropertyTree, _propertyTreeUse));
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Layoutml >_P init(final _P _product) {
            _product.definitions = ((this.definitions == null)?null:this.definitions.build());
            _product.layout = ((this.layout == null)?null:this.layout.build());
            _product.dependencies = ((this.dependencies == null)?null:this.dependencies.build());
            _product.rules = ((this.rules == null)?null:this.rules.build());
            return _product;
        }

        /**
         * Sets the new value of "definitions" (any previous value will be replaced)
         * 
         * @param definitions
         *     New value of the "definitions" property.
         */
        public Layoutml.Builder<_B> withDefinitions(final Definitions definitions) {
            this.definitions = ((definitions == null)?null:new Definitions.Builder<Layoutml.Builder<_B>>(this, definitions, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "definitions" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Definitions.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "definitions" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Definitions.Builder#end()} to return to the current builder.
         */
        public Definitions.Builder<? extends Layoutml.Builder<_B>> withDefinitions() {
            if (this.definitions!= null) {
                return this.definitions;
            }
            return this.definitions = new Definitions.Builder<Layoutml.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "layout" (any previous value will be replaced)
         * 
         * @param layout
         *     New value of the "layout" property.
         */
        public Layoutml.Builder<_B> withLayout(final Layout layout) {
            this.layout = ((layout == null)?null:new Layout.Builder<Layoutml.Builder<_B>>(this, layout, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "layout" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Layout.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "layout" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Layout.Builder#end()} to return to the current builder.
         */
        public Layout.Builder<? extends Layoutml.Builder<_B>> withLayout() {
            if (this.layout!= null) {
                return this.layout;
            }
            return this.layout = new Layout.Builder<Layoutml.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "dependencies" (any previous value will be replaced)
         * 
         * @param dependencies
         *     New value of the "dependencies" property.
         */
        public Layoutml.Builder<_B> withDependencies(final Dependencies dependencies) {
            this.dependencies = ((dependencies == null)?null:new Dependencies.Builder<Layoutml.Builder<_B>>(this, dependencies, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "dependencies" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Dependencies.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "dependencies" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Dependencies.Builder#end()} to return to the current builder.
         */
        public Dependencies.Builder<? extends Layoutml.Builder<_B>> withDependencies() {
            if (this.dependencies!= null) {
                return this.dependencies;
            }
            return this.dependencies = new Dependencies.Builder<Layoutml.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "rules" (any previous value will be replaced)
         * 
         * @param rules
         *     New value of the "rules" property.
         */
        public Layoutml.Builder<_B> withRules(final Rules rules) {
            this.rules = ((rules == null)?null:new Rules.Builder<Layoutml.Builder<_B>>(this, rules, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "rules" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Rules.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "rules" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Rules.Builder#end()} to return to the current builder.
         */
        public Rules.Builder<? extends Layoutml.Builder<_B>> withRules() {
            if (this.rules!= null) {
                return this.rules;
            }
            return this.rules = new Rules.Builder<Layoutml.Builder<_B>>(this, null, false);
        }

        @Override
        public Layoutml build() {
            if (_storedValue == null) {
                return this.init(new Layoutml());
            } else {
                return ((Layoutml) _storedValue);
            }
        }

        public Layoutml.Builder<_B> copyOf(final Layoutml _other) {
            _other.copyTo(this);
            return this;
        }

        public Layoutml.Builder<_B> copyOf(final Layoutml.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Layoutml.Selector<Layoutml.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Layoutml.Select _root() {
            return new Layoutml.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private Definitions.Selector<TRoot, Layoutml.Selector<TRoot, TParent>> definitions = null;
        private Layout.Selector<TRoot, Layoutml.Selector<TRoot, TParent>> layout = null;
        private Dependencies.Selector<TRoot, Layoutml.Selector<TRoot, TParent>> dependencies = null;
        private Rules.Selector<TRoot, Layoutml.Selector<TRoot, TParent>> rules = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.definitions!= null) {
                products.put("definitions", this.definitions.init());
            }
            if (this.layout!= null) {
                products.put("layout", this.layout.init());
            }
            if (this.dependencies!= null) {
                products.put("dependencies", this.dependencies.init());
            }
            if (this.rules!= null) {
                products.put("rules", this.rules.init());
            }
            return products;
        }

        public Definitions.Selector<TRoot, Layoutml.Selector<TRoot, TParent>> definitions() {
            return ((this.definitions == null)?this.definitions = new Definitions.Selector<TRoot, Layoutml.Selector<TRoot, TParent>>(this._root, this, "definitions"):this.definitions);
        }

        public Layout.Selector<TRoot, Layoutml.Selector<TRoot, TParent>> layout() {
            return ((this.layout == null)?this.layout = new Layout.Selector<TRoot, Layoutml.Selector<TRoot, TParent>>(this._root, this, "layout"):this.layout);
        }

        public Dependencies.Selector<TRoot, Layoutml.Selector<TRoot, TParent>> dependencies() {
            return ((this.dependencies == null)?this.dependencies = new Dependencies.Selector<TRoot, Layoutml.Selector<TRoot, TParent>>(this._root, this, "dependencies"):this.dependencies);
        }

        public Rules.Selector<TRoot, Layoutml.Selector<TRoot, TParent>> rules() {
            return ((this.rules == null)?this.rules = new Rules.Selector<TRoot, Layoutml.Selector<TRoot, TParent>>(this._root, this, "rules"):this.rules);
        }

    }

}
