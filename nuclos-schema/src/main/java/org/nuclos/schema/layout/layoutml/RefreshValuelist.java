
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="entity" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="targetcomponent" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="parameter-for-sourcecomponent" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "refresh-valuelist")
public class RefreshValuelist implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "entity")
    @XmlSchemaType(name = "anySimpleType")
    protected String entity;
    @XmlAttribute(name = "targetcomponent", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String targetcomponent;
    @XmlAttribute(name = "parameter-for-sourcecomponent")
    @XmlSchemaType(name = "anySimpleType")
    protected String parameterForSourcecomponent;

    /**
     * Gets the value of the entity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntity() {
        return entity;
    }

    /**
     * Sets the value of the entity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntity(String value) {
        this.entity = value;
    }

    /**
     * Gets the value of the targetcomponent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetcomponent() {
        return targetcomponent;
    }

    /**
     * Sets the value of the targetcomponent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetcomponent(String value) {
        this.targetcomponent = value;
    }

    /**
     * Gets the value of the parameterForSourcecomponent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParameterForSourcecomponent() {
        return parameterForSourcecomponent;
    }

    /**
     * Sets the value of the parameterForSourcecomponent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParameterForSourcecomponent(String value) {
        this.parameterForSourcecomponent = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theEntity;
            theEntity = this.getEntity();
            strategy.appendField(locator, this, "entity", buffer, theEntity);
        }
        {
            String theTargetcomponent;
            theTargetcomponent = this.getTargetcomponent();
            strategy.appendField(locator, this, "targetcomponent", buffer, theTargetcomponent);
        }
        {
            String theParameterForSourcecomponent;
            theParameterForSourcecomponent = this.getParameterForSourcecomponent();
            strategy.appendField(locator, this, "parameterForSourcecomponent", buffer, theParameterForSourcecomponent);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof RefreshValuelist)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final RefreshValuelist that = ((RefreshValuelist) object);
        {
            String lhsEntity;
            lhsEntity = this.getEntity();
            String rhsEntity;
            rhsEntity = that.getEntity();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entity", lhsEntity), LocatorUtils.property(thatLocator, "entity", rhsEntity), lhsEntity, rhsEntity)) {
                return false;
            }
        }
        {
            String lhsTargetcomponent;
            lhsTargetcomponent = this.getTargetcomponent();
            String rhsTargetcomponent;
            rhsTargetcomponent = that.getTargetcomponent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "targetcomponent", lhsTargetcomponent), LocatorUtils.property(thatLocator, "targetcomponent", rhsTargetcomponent), lhsTargetcomponent, rhsTargetcomponent)) {
                return false;
            }
        }
        {
            String lhsParameterForSourcecomponent;
            lhsParameterForSourcecomponent = this.getParameterForSourcecomponent();
            String rhsParameterForSourcecomponent;
            rhsParameterForSourcecomponent = that.getParameterForSourcecomponent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "parameterForSourcecomponent", lhsParameterForSourcecomponent), LocatorUtils.property(thatLocator, "parameterForSourcecomponent", rhsParameterForSourcecomponent), lhsParameterForSourcecomponent, rhsParameterForSourcecomponent)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theEntity;
            theEntity = this.getEntity();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entity", theEntity), currentHashCode, theEntity);
        }
        {
            String theTargetcomponent;
            theTargetcomponent = this.getTargetcomponent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "targetcomponent", theTargetcomponent), currentHashCode, theTargetcomponent);
        }
        {
            String theParameterForSourcecomponent;
            theParameterForSourcecomponent = this.getParameterForSourcecomponent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "parameterForSourcecomponent", theParameterForSourcecomponent), currentHashCode, theParameterForSourcecomponent);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof RefreshValuelist) {
            final RefreshValuelist copy = ((RefreshValuelist) draftCopy);
            if (this.entity!= null) {
                String sourceEntity;
                sourceEntity = this.getEntity();
                String copyEntity = ((String) strategy.copy(LocatorUtils.property(locator, "entity", sourceEntity), sourceEntity));
                copy.setEntity(copyEntity);
            } else {
                copy.entity = null;
            }
            if (this.targetcomponent!= null) {
                String sourceTargetcomponent;
                sourceTargetcomponent = this.getTargetcomponent();
                String copyTargetcomponent = ((String) strategy.copy(LocatorUtils.property(locator, "targetcomponent", sourceTargetcomponent), sourceTargetcomponent));
                copy.setTargetcomponent(copyTargetcomponent);
            } else {
                copy.targetcomponent = null;
            }
            if (this.parameterForSourcecomponent!= null) {
                String sourceParameterForSourcecomponent;
                sourceParameterForSourcecomponent = this.getParameterForSourcecomponent();
                String copyParameterForSourcecomponent = ((String) strategy.copy(LocatorUtils.property(locator, "parameterForSourcecomponent", sourceParameterForSourcecomponent), sourceParameterForSourcecomponent));
                copy.setParameterForSourcecomponent(copyParameterForSourcecomponent);
            } else {
                copy.parameterForSourcecomponent = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new RefreshValuelist();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final RefreshValuelist.Builder<_B> _other) {
        _other.entity = this.entity;
        _other.targetcomponent = this.targetcomponent;
        _other.parameterForSourcecomponent = this.parameterForSourcecomponent;
    }

    public<_B >RefreshValuelist.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new RefreshValuelist.Builder<_B>(_parentBuilder, this, true);
    }

    public RefreshValuelist.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static RefreshValuelist.Builder<Void> builder() {
        return new RefreshValuelist.Builder<Void>(null, null, false);
    }

    public static<_B >RefreshValuelist.Builder<_B> copyOf(final RefreshValuelist _other) {
        final RefreshValuelist.Builder<_B> _newBuilder = new RefreshValuelist.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final RefreshValuelist.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree entityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entity"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityPropertyTree!= null):((entityPropertyTree == null)||(!entityPropertyTree.isLeaf())))) {
            _other.entity = this.entity;
        }
        final PropertyTree targetcomponentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("targetcomponent"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(targetcomponentPropertyTree!= null):((targetcomponentPropertyTree == null)||(!targetcomponentPropertyTree.isLeaf())))) {
            _other.targetcomponent = this.targetcomponent;
        }
        final PropertyTree parameterForSourcecomponentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("parameterForSourcecomponent"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(parameterForSourcecomponentPropertyTree!= null):((parameterForSourcecomponentPropertyTree == null)||(!parameterForSourcecomponentPropertyTree.isLeaf())))) {
            _other.parameterForSourcecomponent = this.parameterForSourcecomponent;
        }
    }

    public<_B >RefreshValuelist.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new RefreshValuelist.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public RefreshValuelist.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >RefreshValuelist.Builder<_B> copyOf(final RefreshValuelist _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final RefreshValuelist.Builder<_B> _newBuilder = new RefreshValuelist.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static RefreshValuelist.Builder<Void> copyExcept(final RefreshValuelist _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static RefreshValuelist.Builder<Void> copyOnly(final RefreshValuelist _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final RefreshValuelist _storedValue;
        private String entity;
        private String targetcomponent;
        private String parameterForSourcecomponent;

        public Builder(final _B _parentBuilder, final RefreshValuelist _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.entity = _other.entity;
                    this.targetcomponent = _other.targetcomponent;
                    this.parameterForSourcecomponent = _other.parameterForSourcecomponent;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final RefreshValuelist _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree entityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entity"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityPropertyTree!= null):((entityPropertyTree == null)||(!entityPropertyTree.isLeaf())))) {
                        this.entity = _other.entity;
                    }
                    final PropertyTree targetcomponentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("targetcomponent"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(targetcomponentPropertyTree!= null):((targetcomponentPropertyTree == null)||(!targetcomponentPropertyTree.isLeaf())))) {
                        this.targetcomponent = _other.targetcomponent;
                    }
                    final PropertyTree parameterForSourcecomponentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("parameterForSourcecomponent"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(parameterForSourcecomponentPropertyTree!= null):((parameterForSourcecomponentPropertyTree == null)||(!parameterForSourcecomponentPropertyTree.isLeaf())))) {
                        this.parameterForSourcecomponent = _other.parameterForSourcecomponent;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends RefreshValuelist >_P init(final _P _product) {
            _product.entity = this.entity;
            _product.targetcomponent = this.targetcomponent;
            _product.parameterForSourcecomponent = this.parameterForSourcecomponent;
            return _product;
        }

        /**
         * Sets the new value of "entity" (any previous value will be replaced)
         * 
         * @param entity
         *     New value of the "entity" property.
         */
        public RefreshValuelist.Builder<_B> withEntity(final String entity) {
            this.entity = entity;
            return this;
        }

        /**
         * Sets the new value of "targetcomponent" (any previous value will be replaced)
         * 
         * @param targetcomponent
         *     New value of the "targetcomponent" property.
         */
        public RefreshValuelist.Builder<_B> withTargetcomponent(final String targetcomponent) {
            this.targetcomponent = targetcomponent;
            return this;
        }

        /**
         * Sets the new value of "parameterForSourcecomponent" (any previous value will be replaced)
         * 
         * @param parameterForSourcecomponent
         *     New value of the "parameterForSourcecomponent" property.
         */
        public RefreshValuelist.Builder<_B> withParameterForSourcecomponent(final String parameterForSourcecomponent) {
            this.parameterForSourcecomponent = parameterForSourcecomponent;
            return this;
        }

        @Override
        public RefreshValuelist build() {
            if (_storedValue == null) {
                return this.init(new RefreshValuelist());
            } else {
                return ((RefreshValuelist) _storedValue);
            }
        }

        public RefreshValuelist.Builder<_B> copyOf(final RefreshValuelist _other) {
            _other.copyTo(this);
            return this;
        }

        public RefreshValuelist.Builder<_B> copyOf(final RefreshValuelist.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends RefreshValuelist.Selector<RefreshValuelist.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static RefreshValuelist.Select _root() {
            return new RefreshValuelist.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, RefreshValuelist.Selector<TRoot, TParent>> entity = null;
        private com.kscs.util.jaxb.Selector<TRoot, RefreshValuelist.Selector<TRoot, TParent>> targetcomponent = null;
        private com.kscs.util.jaxb.Selector<TRoot, RefreshValuelist.Selector<TRoot, TParent>> parameterForSourcecomponent = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.entity!= null) {
                products.put("entity", this.entity.init());
            }
            if (this.targetcomponent!= null) {
                products.put("targetcomponent", this.targetcomponent.init());
            }
            if (this.parameterForSourcecomponent!= null) {
                products.put("parameterForSourcecomponent", this.parameterForSourcecomponent.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, RefreshValuelist.Selector<TRoot, TParent>> entity() {
            return ((this.entity == null)?this.entity = new com.kscs.util.jaxb.Selector<TRoot, RefreshValuelist.Selector<TRoot, TParent>>(this._root, this, "entity"):this.entity);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RefreshValuelist.Selector<TRoot, TParent>> targetcomponent() {
            return ((this.targetcomponent == null)?this.targetcomponent = new com.kscs.util.jaxb.Selector<TRoot, RefreshValuelist.Selector<TRoot, TParent>>(this._root, this, "targetcomponent"):this.targetcomponent);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RefreshValuelist.Selector<TRoot, TParent>> parameterForSourcecomponent() {
            return ((this.parameterForSourcecomponent == null)?this.parameterForSourcecomponent = new com.kscs.util.jaxb.Selector<TRoot, RefreshValuelist.Selector<TRoot, TParent>>(this._root, this, "parameterForSourcecomponent"):this.parameterForSourcecomponent);
        }

    }

}
