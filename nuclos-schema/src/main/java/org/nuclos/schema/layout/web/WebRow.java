
package org.nuclos.schema.layout.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Rows of a table or grid.
 * 
 * <p>Java class for web-row complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="web-row"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="cells" type="{urn:org.nuclos.schema.layout.web}web-cell" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "web-row", propOrder = {
    "cells"
})
public class WebRow implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected List<WebCell> cells;

    /**
     * Gets the value of the cells property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cells property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCells().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WebCell }
     * 
     * 
     */
    public List<WebCell> getCells() {
        if (cells == null) {
            cells = new ArrayList<WebCell>();
        }
        return this.cells;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<WebCell> theCells;
            theCells = (((this.cells!= null)&&(!this.cells.isEmpty()))?this.getCells():null);
            strategy.appendField(locator, this, "cells", buffer, theCells);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof WebRow)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final WebRow that = ((WebRow) object);
        {
            List<WebCell> lhsCells;
            lhsCells = (((this.cells!= null)&&(!this.cells.isEmpty()))?this.getCells():null);
            List<WebCell> rhsCells;
            rhsCells = (((that.cells!= null)&&(!that.cells.isEmpty()))?that.getCells():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "cells", lhsCells), LocatorUtils.property(thatLocator, "cells", rhsCells), lhsCells, rhsCells)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<WebCell> theCells;
            theCells = (((this.cells!= null)&&(!this.cells.isEmpty()))?this.getCells():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "cells", theCells), currentHashCode, theCells);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof WebRow) {
            final WebRow copy = ((WebRow) draftCopy);
            if ((this.cells!= null)&&(!this.cells.isEmpty())) {
                List<WebCell> sourceCells;
                sourceCells = (((this.cells!= null)&&(!this.cells.isEmpty()))?this.getCells():null);
                @SuppressWarnings("unchecked")
                List<WebCell> copyCells = ((List<WebCell> ) strategy.copy(LocatorUtils.property(locator, "cells", sourceCells), sourceCells));
                copy.cells = null;
                if (copyCells!= null) {
                    List<WebCell> uniqueCellsl = copy.getCells();
                    uniqueCellsl.addAll(copyCells);
                }
            } else {
                copy.cells = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new WebRow();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebRow.Builder<_B> _other) {
        if (this.cells == null) {
            _other.cells = null;
        } else {
            _other.cells = new ArrayList<WebCell.Builder<WebRow.Builder<_B>>>();
            for (WebCell _item: this.cells) {
                _other.cells.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
    }

    public<_B >WebRow.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new WebRow.Builder<_B>(_parentBuilder, this, true);
    }

    public WebRow.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static WebRow.Builder<Void> builder() {
        return new WebRow.Builder<Void>(null, null, false);
    }

    public static<_B >WebRow.Builder<_B> copyOf(final WebRow _other) {
        final WebRow.Builder<_B> _newBuilder = new WebRow.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebRow.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree cellsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("cells"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(cellsPropertyTree!= null):((cellsPropertyTree == null)||(!cellsPropertyTree.isLeaf())))) {
            if (this.cells == null) {
                _other.cells = null;
            } else {
                _other.cells = new ArrayList<WebCell.Builder<WebRow.Builder<_B>>>();
                for (WebCell _item: this.cells) {
                    _other.cells.add(((_item == null)?null:_item.newCopyBuilder(_other, cellsPropertyTree, _propertyTreeUse)));
                }
            }
        }
    }

    public<_B >WebRow.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new WebRow.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public WebRow.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >WebRow.Builder<_B> copyOf(final WebRow _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebRow.Builder<_B> _newBuilder = new WebRow.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static WebRow.Builder<Void> copyExcept(final WebRow _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebRow.Builder<Void> copyOnly(final WebRow _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final WebRow _storedValue;
        private List<WebCell.Builder<WebRow.Builder<_B>>> cells;

        public Builder(final _B _parentBuilder, final WebRow _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.cells == null) {
                        this.cells = null;
                    } else {
                        this.cells = new ArrayList<WebCell.Builder<WebRow.Builder<_B>>>();
                        for (WebCell _item: _other.cells) {
                            this.cells.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final WebRow _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree cellsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("cells"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(cellsPropertyTree!= null):((cellsPropertyTree == null)||(!cellsPropertyTree.isLeaf())))) {
                        if (_other.cells == null) {
                            this.cells = null;
                        } else {
                            this.cells = new ArrayList<WebCell.Builder<WebRow.Builder<_B>>>();
                            for (WebCell _item: _other.cells) {
                                this.cells.add(((_item == null)?null:_item.newCopyBuilder(this, cellsPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends WebRow >_P init(final _P _product) {
            if (this.cells!= null) {
                final List<WebCell> cells = new ArrayList<WebCell>(this.cells.size());
                for (WebCell.Builder<WebRow.Builder<_B>> _item: this.cells) {
                    cells.add(_item.build());
                }
                _product.cells = cells;
            }
            return _product;
        }

        /**
         * Adds the given items to the value of "cells"
         * 
         * @param cells
         *     Items to add to the value of the "cells" property
         */
        public WebRow.Builder<_B> addCells(final Iterable<? extends WebCell> cells) {
            if (cells!= null) {
                if (this.cells == null) {
                    this.cells = new ArrayList<WebCell.Builder<WebRow.Builder<_B>>>();
                }
                for (WebCell _item: cells) {
                    this.cells.add(new WebCell.Builder<WebRow.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "cells" (any previous value will be replaced)
         * 
         * @param cells
         *     New value of the "cells" property.
         */
        public WebRow.Builder<_B> withCells(final Iterable<? extends WebCell> cells) {
            if (this.cells!= null) {
                this.cells.clear();
            }
            return addCells(cells);
        }

        /**
         * Adds the given items to the value of "cells"
         * 
         * @param cells
         *     Items to add to the value of the "cells" property
         */
        public WebRow.Builder<_B> addCells(WebCell... cells) {
            addCells(Arrays.asList(cells));
            return this;
        }

        /**
         * Sets the new value of "cells" (any previous value will be replaced)
         * 
         * @param cells
         *     New value of the "cells" property.
         */
        public WebRow.Builder<_B> withCells(WebCell... cells) {
            withCells(Arrays.asList(cells));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Cells" property.
         * Use {@link org.nuclos.schema.layout.web.WebCell.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Cells" property.
         *     Use {@link org.nuclos.schema.layout.web.WebCell.Builder#end()} to return to the current builder.
         */
        public WebCell.Builder<? extends WebRow.Builder<_B>> addCells() {
            if (this.cells == null) {
                this.cells = new ArrayList<WebCell.Builder<WebRow.Builder<_B>>>();
            }
            final WebCell.Builder<WebRow.Builder<_B>> cells_Builder = new WebCell.Builder<WebRow.Builder<_B>>(this, null, false);
            this.cells.add(cells_Builder);
            return cells_Builder;
        }

        @Override
        public WebRow build() {
            if (_storedValue == null) {
                return this.init(new WebRow());
            } else {
                return ((WebRow) _storedValue);
            }
        }

        public WebRow.Builder<_B> copyOf(final WebRow _other) {
            _other.copyTo(this);
            return this;
        }

        public WebRow.Builder<_B> copyOf(final WebRow.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends WebRow.Selector<WebRow.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static WebRow.Select _root() {
            return new WebRow.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private WebCell.Selector<TRoot, WebRow.Selector<TRoot, TParent>> cells = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.cells!= null) {
                products.put("cells", this.cells.init());
            }
            return products;
        }

        public WebCell.Selector<TRoot, WebRow.Selector<TRoot, TParent>> cells() {
            return ((this.cells == null)?this.cells = new WebCell.Selector<TRoot, WebRow.Selector<TRoot, TParent>>(this._root, this, "cells"):this.cells);
        }

    }

}
