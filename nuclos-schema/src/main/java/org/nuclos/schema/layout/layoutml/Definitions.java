
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}collectable-field" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "collectableField"
})
@XmlRootElement(name = "definitions")
public class Definitions implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "collectable-field")
    protected List<CollectableField> collectableField;

    /**
     * Gets the value of the collectableField property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the collectableField property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCollectableField().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CollectableField }
     * 
     * 
     */
    public List<CollectableField> getCollectableField() {
        if (collectableField == null) {
            collectableField = new ArrayList<CollectableField>();
        }
        return this.collectableField;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<CollectableField> theCollectableField;
            theCollectableField = (((this.collectableField!= null)&&(!this.collectableField.isEmpty()))?this.getCollectableField():null);
            strategy.appendField(locator, this, "collectableField", buffer, theCollectableField);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Definitions)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Definitions that = ((Definitions) object);
        {
            List<CollectableField> lhsCollectableField;
            lhsCollectableField = (((this.collectableField!= null)&&(!this.collectableField.isEmpty()))?this.getCollectableField():null);
            List<CollectableField> rhsCollectableField;
            rhsCollectableField = (((that.collectableField!= null)&&(!that.collectableField.isEmpty()))?that.getCollectableField():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "collectableField", lhsCollectableField), LocatorUtils.property(thatLocator, "collectableField", rhsCollectableField), lhsCollectableField, rhsCollectableField)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<CollectableField> theCollectableField;
            theCollectableField = (((this.collectableField!= null)&&(!this.collectableField.isEmpty()))?this.getCollectableField():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "collectableField", theCollectableField), currentHashCode, theCollectableField);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Definitions) {
            final Definitions copy = ((Definitions) draftCopy);
            if ((this.collectableField!= null)&&(!this.collectableField.isEmpty())) {
                List<CollectableField> sourceCollectableField;
                sourceCollectableField = (((this.collectableField!= null)&&(!this.collectableField.isEmpty()))?this.getCollectableField():null);
                @SuppressWarnings("unchecked")
                List<CollectableField> copyCollectableField = ((List<CollectableField> ) strategy.copy(LocatorUtils.property(locator, "collectableField", sourceCollectableField), sourceCollectableField));
                copy.collectableField = null;
                if (copyCollectableField!= null) {
                    List<CollectableField> uniqueCollectableFieldl = copy.getCollectableField();
                    uniqueCollectableFieldl.addAll(copyCollectableField);
                }
            } else {
                copy.collectableField = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Definitions();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Definitions.Builder<_B> _other) {
        if (this.collectableField == null) {
            _other.collectableField = null;
        } else {
            _other.collectableField = new ArrayList<CollectableField.Builder<Definitions.Builder<_B>>>();
            for (CollectableField _item: this.collectableField) {
                _other.collectableField.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
    }

    public<_B >Definitions.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Definitions.Builder<_B>(_parentBuilder, this, true);
    }

    public Definitions.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Definitions.Builder<Void> builder() {
        return new Definitions.Builder<Void>(null, null, false);
    }

    public static<_B >Definitions.Builder<_B> copyOf(final Definitions _other) {
        final Definitions.Builder<_B> _newBuilder = new Definitions.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Definitions.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree collectableFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("collectableField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(collectableFieldPropertyTree!= null):((collectableFieldPropertyTree == null)||(!collectableFieldPropertyTree.isLeaf())))) {
            if (this.collectableField == null) {
                _other.collectableField = null;
            } else {
                _other.collectableField = new ArrayList<CollectableField.Builder<Definitions.Builder<_B>>>();
                for (CollectableField _item: this.collectableField) {
                    _other.collectableField.add(((_item == null)?null:_item.newCopyBuilder(_other, collectableFieldPropertyTree, _propertyTreeUse)));
                }
            }
        }
    }

    public<_B >Definitions.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Definitions.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Definitions.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Definitions.Builder<_B> copyOf(final Definitions _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Definitions.Builder<_B> _newBuilder = new Definitions.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Definitions.Builder<Void> copyExcept(final Definitions _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Definitions.Builder<Void> copyOnly(final Definitions _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Definitions _storedValue;
        private List<CollectableField.Builder<Definitions.Builder<_B>>> collectableField;

        public Builder(final _B _parentBuilder, final Definitions _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.collectableField == null) {
                        this.collectableField = null;
                    } else {
                        this.collectableField = new ArrayList<CollectableField.Builder<Definitions.Builder<_B>>>();
                        for (CollectableField _item: _other.collectableField) {
                            this.collectableField.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Definitions _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree collectableFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("collectableField"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(collectableFieldPropertyTree!= null):((collectableFieldPropertyTree == null)||(!collectableFieldPropertyTree.isLeaf())))) {
                        if (_other.collectableField == null) {
                            this.collectableField = null;
                        } else {
                            this.collectableField = new ArrayList<CollectableField.Builder<Definitions.Builder<_B>>>();
                            for (CollectableField _item: _other.collectableField) {
                                this.collectableField.add(((_item == null)?null:_item.newCopyBuilder(this, collectableFieldPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Definitions >_P init(final _P _product) {
            if (this.collectableField!= null) {
                final List<CollectableField> collectableField = new ArrayList<CollectableField>(this.collectableField.size());
                for (CollectableField.Builder<Definitions.Builder<_B>> _item: this.collectableField) {
                    collectableField.add(_item.build());
                }
                _product.collectableField = collectableField;
            }
            return _product;
        }

        /**
         * Adds the given items to the value of "collectableField"
         * 
         * @param collectableField
         *     Items to add to the value of the "collectableField" property
         */
        public Definitions.Builder<_B> addCollectableField(final Iterable<? extends CollectableField> collectableField) {
            if (collectableField!= null) {
                if (this.collectableField == null) {
                    this.collectableField = new ArrayList<CollectableField.Builder<Definitions.Builder<_B>>>();
                }
                for (CollectableField _item: collectableField) {
                    this.collectableField.add(new CollectableField.Builder<Definitions.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "collectableField" (any previous value will be replaced)
         * 
         * @param collectableField
         *     New value of the "collectableField" property.
         */
        public Definitions.Builder<_B> withCollectableField(final Iterable<? extends CollectableField> collectableField) {
            if (this.collectableField!= null) {
                this.collectableField.clear();
            }
            return addCollectableField(collectableField);
        }

        /**
         * Adds the given items to the value of "collectableField"
         * 
         * @param collectableField
         *     Items to add to the value of the "collectableField" property
         */
        public Definitions.Builder<_B> addCollectableField(CollectableField... collectableField) {
            addCollectableField(Arrays.asList(collectableField));
            return this;
        }

        /**
         * Sets the new value of "collectableField" (any previous value will be replaced)
         * 
         * @param collectableField
         *     New value of the "collectableField" property.
         */
        public Definitions.Builder<_B> withCollectableField(CollectableField... collectableField) {
            withCollectableField(Arrays.asList(collectableField));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "CollectableField" property.
         * Use {@link org.nuclos.schema.layout.layoutml.CollectableField.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "CollectableField" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.CollectableField.Builder#end()} to return to the current builder.
         */
        public CollectableField.Builder<? extends Definitions.Builder<_B>> addCollectableField() {
            if (this.collectableField == null) {
                this.collectableField = new ArrayList<CollectableField.Builder<Definitions.Builder<_B>>>();
            }
            final CollectableField.Builder<Definitions.Builder<_B>> collectableField_Builder = new CollectableField.Builder<Definitions.Builder<_B>>(this, null, false);
            this.collectableField.add(collectableField_Builder);
            return collectableField_Builder;
        }

        @Override
        public Definitions build() {
            if (_storedValue == null) {
                return this.init(new Definitions());
            } else {
                return ((Definitions) _storedValue);
            }
        }

        public Definitions.Builder<_B> copyOf(final Definitions _other) {
            _other.copyTo(this);
            return this;
        }

        public Definitions.Builder<_B> copyOf(final Definitions.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Definitions.Selector<Definitions.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Definitions.Select _root() {
            return new Definitions.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private CollectableField.Selector<TRoot, Definitions.Selector<TRoot, TParent>> collectableField = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.collectableField!= null) {
                products.put("collectableField", this.collectableField.init());
            }
            return products;
        }

        public CollectableField.Selector<TRoot, Definitions.Selector<TRoot, TParent>> collectableField() {
            return ((this.collectableField == null)?this.collectableField = new CollectableField.Selector<TRoot, Definitions.Selector<TRoot, TParent>>(this._root, this, "collectableField"):this.collectableField);
        }

    }

}
