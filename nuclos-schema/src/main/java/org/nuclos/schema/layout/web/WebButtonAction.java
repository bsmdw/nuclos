
package org.nuclos.schema.layout.web;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for web-button-action.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="web-button-action"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="dummy"/&gt;
 *     &lt;enumeration value="change-state"/&gt;
 *     &lt;enumeration value="execute-rule"/&gt;
 *     &lt;enumeration value="generate-object"/&gt;
 *     &lt;enumeration value="hyperlink"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "web-button-action")
@XmlEnum
public enum WebButtonAction {

    @XmlEnumValue("dummy")
    DUMMY("dummy"),
    @XmlEnumValue("change-state")
    CHANGE_STATE("change-state"),
    @XmlEnumValue("execute-rule")
    EXECUTE_RULE("execute-rule"),
    @XmlEnumValue("generate-object")
    GENERATE_OBJECT("generate-object"),
    @XmlEnumValue("hyperlink")
    HYPERLINK("hyperlink");
    private final String value;

    WebButtonAction(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static WebButtonAction fromValue(String v) {
        for (WebButtonAction c: WebButtonAction.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
