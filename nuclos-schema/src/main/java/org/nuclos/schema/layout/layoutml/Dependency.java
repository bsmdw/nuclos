
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="dependant-field" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="depends-on" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "dependency")
public class Dependency implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "name")
    @XmlSchemaType(name = "anySimpleType")
    protected String name;
    @XmlAttribute(name = "dependant-field", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String dependantField;
    @XmlAttribute(name = "depends-on", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String dependsOn;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the dependantField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDependantField() {
        return dependantField;
    }

    /**
     * Sets the value of the dependantField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDependantField(String value) {
        this.dependantField = value;
    }

    /**
     * Gets the value of the dependsOn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDependsOn() {
        return dependsOn;
    }

    /**
     * Sets the value of the dependsOn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDependsOn(String value) {
        this.dependsOn = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String theDependantField;
            theDependantField = this.getDependantField();
            strategy.appendField(locator, this, "dependantField", buffer, theDependantField);
        }
        {
            String theDependsOn;
            theDependsOn = this.getDependsOn();
            strategy.appendField(locator, this, "dependsOn", buffer, theDependsOn);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Dependency)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Dependency that = ((Dependency) object);
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsDependantField;
            lhsDependantField = this.getDependantField();
            String rhsDependantField;
            rhsDependantField = that.getDependantField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dependantField", lhsDependantField), LocatorUtils.property(thatLocator, "dependantField", rhsDependantField), lhsDependantField, rhsDependantField)) {
                return false;
            }
        }
        {
            String lhsDependsOn;
            lhsDependsOn = this.getDependsOn();
            String rhsDependsOn;
            rhsDependsOn = that.getDependsOn();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dependsOn", lhsDependsOn), LocatorUtils.property(thatLocator, "dependsOn", rhsDependsOn), lhsDependsOn, rhsDependsOn)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String theDependantField;
            theDependantField = this.getDependantField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dependantField", theDependantField), currentHashCode, theDependantField);
        }
        {
            String theDependsOn;
            theDependsOn = this.getDependsOn();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dependsOn", theDependsOn), currentHashCode, theDependsOn);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Dependency) {
            final Dependency copy = ((Dependency) draftCopy);
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.dependantField!= null) {
                String sourceDependantField;
                sourceDependantField = this.getDependantField();
                String copyDependantField = ((String) strategy.copy(LocatorUtils.property(locator, "dependantField", sourceDependantField), sourceDependantField));
                copy.setDependantField(copyDependantField);
            } else {
                copy.dependantField = null;
            }
            if (this.dependsOn!= null) {
                String sourceDependsOn;
                sourceDependsOn = this.getDependsOn();
                String copyDependsOn = ((String) strategy.copy(LocatorUtils.property(locator, "dependsOn", sourceDependsOn), sourceDependsOn));
                copy.setDependsOn(copyDependsOn);
            } else {
                copy.dependsOn = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Dependency();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Dependency.Builder<_B> _other) {
        _other.name = this.name;
        _other.dependantField = this.dependantField;
        _other.dependsOn = this.dependsOn;
    }

    public<_B >Dependency.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Dependency.Builder<_B>(_parentBuilder, this, true);
    }

    public Dependency.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Dependency.Builder<Void> builder() {
        return new Dependency.Builder<Void>(null, null, false);
    }

    public static<_B >Dependency.Builder<_B> copyOf(final Dependency _other) {
        final Dependency.Builder<_B> _newBuilder = new Dependency.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Dependency.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree dependantFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dependantField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dependantFieldPropertyTree!= null):((dependantFieldPropertyTree == null)||(!dependantFieldPropertyTree.isLeaf())))) {
            _other.dependantField = this.dependantField;
        }
        final PropertyTree dependsOnPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dependsOn"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dependsOnPropertyTree!= null):((dependsOnPropertyTree == null)||(!dependsOnPropertyTree.isLeaf())))) {
            _other.dependsOn = this.dependsOn;
        }
    }

    public<_B >Dependency.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Dependency.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Dependency.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Dependency.Builder<_B> copyOf(final Dependency _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Dependency.Builder<_B> _newBuilder = new Dependency.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Dependency.Builder<Void> copyExcept(final Dependency _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Dependency.Builder<Void> copyOnly(final Dependency _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Dependency _storedValue;
        private String name;
        private String dependantField;
        private String dependsOn;

        public Builder(final _B _parentBuilder, final Dependency _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.name = _other.name;
                    this.dependantField = _other.dependantField;
                    this.dependsOn = _other.dependsOn;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Dependency _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree dependantFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dependantField"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dependantFieldPropertyTree!= null):((dependantFieldPropertyTree == null)||(!dependantFieldPropertyTree.isLeaf())))) {
                        this.dependantField = _other.dependantField;
                    }
                    final PropertyTree dependsOnPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dependsOn"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dependsOnPropertyTree!= null):((dependsOnPropertyTree == null)||(!dependsOnPropertyTree.isLeaf())))) {
                        this.dependsOn = _other.dependsOn;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Dependency >_P init(final _P _product) {
            _product.name = this.name;
            _product.dependantField = this.dependantField;
            _product.dependsOn = this.dependsOn;
            return _product;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public Dependency.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "dependantField" (any previous value will be replaced)
         * 
         * @param dependantField
         *     New value of the "dependantField" property.
         */
        public Dependency.Builder<_B> withDependantField(final String dependantField) {
            this.dependantField = dependantField;
            return this;
        }

        /**
         * Sets the new value of "dependsOn" (any previous value will be replaced)
         * 
         * @param dependsOn
         *     New value of the "dependsOn" property.
         */
        public Dependency.Builder<_B> withDependsOn(final String dependsOn) {
            this.dependsOn = dependsOn;
            return this;
        }

        @Override
        public Dependency build() {
            if (_storedValue == null) {
                return this.init(new Dependency());
            } else {
                return ((Dependency) _storedValue);
            }
        }

        public Dependency.Builder<_B> copyOf(final Dependency _other) {
            _other.copyTo(this);
            return this;
        }

        public Dependency.Builder<_B> copyOf(final Dependency.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Dependency.Selector<Dependency.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Dependency.Select _root() {
            return new Dependency.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, Dependency.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, Dependency.Selector<TRoot, TParent>> dependantField = null;
        private com.kscs.util.jaxb.Selector<TRoot, Dependency.Selector<TRoot, TParent>> dependsOn = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.dependantField!= null) {
                products.put("dependantField", this.dependantField.init());
            }
            if (this.dependsOn!= null) {
                products.put("dependsOn", this.dependsOn.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, Dependency.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, Dependency.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Dependency.Selector<TRoot, TParent>> dependantField() {
            return ((this.dependantField == null)?this.dependantField = new com.kscs.util.jaxb.Selector<TRoot, Dependency.Selector<TRoot, TParent>>(this._root, this, "dependantField"):this.dependantField);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Dependency.Selector<TRoot, TParent>> dependsOn() {
            return ((this.dependsOn == null)?this.dependsOn = new com.kscs.util.jaxb.Selector<TRoot, Dependency.Selector<TRoot, TParent>>(this._root, this, "dependsOn"):this.dependsOn);
        }

    }

}
