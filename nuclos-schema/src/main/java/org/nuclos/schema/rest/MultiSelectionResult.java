
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for multi-selection-result complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="multi-selection-result"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="selectedIds" type="{urn:org.nuclos.schema.rest}object-id" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="unselectedIds" type="{urn:org.nuclos.schema.rest}object-id" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="allSelected" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "multi-selection-result", propOrder = {
    "selectedIds",
    "unselectedIds"
})
public class MultiSelectionResult implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected List<ObjectId> selectedIds;
    protected List<ObjectId> unselectedIds;
    @XmlAttribute(name = "allSelected", required = true)
    protected boolean allSelected;

    /**
     * Gets the value of the selectedIds property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the selectedIds property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSelectedIds().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectId }
     * 
     * 
     */
    public List<ObjectId> getSelectedIds() {
        if (selectedIds == null) {
            selectedIds = new ArrayList<ObjectId>();
        }
        return this.selectedIds;
    }

    /**
     * Gets the value of the unselectedIds property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the unselectedIds property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUnselectedIds().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectId }
     * 
     * 
     */
    public List<ObjectId> getUnselectedIds() {
        if (unselectedIds == null) {
            unselectedIds = new ArrayList<ObjectId>();
        }
        return this.unselectedIds;
    }

    /**
     * Gets the value of the allSelected property.
     * 
     */
    public boolean isAllSelected() {
        return allSelected;
    }

    /**
     * Sets the value of the allSelected property.
     * 
     */
    public void setAllSelected(boolean value) {
        this.allSelected = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<ObjectId> theSelectedIds;
            theSelectedIds = (((this.selectedIds!= null)&&(!this.selectedIds.isEmpty()))?this.getSelectedIds():null);
            strategy.appendField(locator, this, "selectedIds", buffer, theSelectedIds);
        }
        {
            List<ObjectId> theUnselectedIds;
            theUnselectedIds = (((this.unselectedIds!= null)&&(!this.unselectedIds.isEmpty()))?this.getUnselectedIds():null);
            strategy.appendField(locator, this, "unselectedIds", buffer, theUnselectedIds);
        }
        {
            boolean theAllSelected;
            theAllSelected = this.isAllSelected();
            strategy.appendField(locator, this, "allSelected", buffer, theAllSelected);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MultiSelectionResult)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MultiSelectionResult that = ((MultiSelectionResult) object);
        {
            List<ObjectId> lhsSelectedIds;
            lhsSelectedIds = (((this.selectedIds!= null)&&(!this.selectedIds.isEmpty()))?this.getSelectedIds():null);
            List<ObjectId> rhsSelectedIds;
            rhsSelectedIds = (((that.selectedIds!= null)&&(!that.selectedIds.isEmpty()))?that.getSelectedIds():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "selectedIds", lhsSelectedIds), LocatorUtils.property(thatLocator, "selectedIds", rhsSelectedIds), lhsSelectedIds, rhsSelectedIds)) {
                return false;
            }
        }
        {
            List<ObjectId> lhsUnselectedIds;
            lhsUnselectedIds = (((this.unselectedIds!= null)&&(!this.unselectedIds.isEmpty()))?this.getUnselectedIds():null);
            List<ObjectId> rhsUnselectedIds;
            rhsUnselectedIds = (((that.unselectedIds!= null)&&(!that.unselectedIds.isEmpty()))?that.getUnselectedIds():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "unselectedIds", lhsUnselectedIds), LocatorUtils.property(thatLocator, "unselectedIds", rhsUnselectedIds), lhsUnselectedIds, rhsUnselectedIds)) {
                return false;
            }
        }
        {
            boolean lhsAllSelected;
            lhsAllSelected = this.isAllSelected();
            boolean rhsAllSelected;
            rhsAllSelected = that.isAllSelected();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "allSelected", lhsAllSelected), LocatorUtils.property(thatLocator, "allSelected", rhsAllSelected), lhsAllSelected, rhsAllSelected)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<ObjectId> theSelectedIds;
            theSelectedIds = (((this.selectedIds!= null)&&(!this.selectedIds.isEmpty()))?this.getSelectedIds():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "selectedIds", theSelectedIds), currentHashCode, theSelectedIds);
        }
        {
            List<ObjectId> theUnselectedIds;
            theUnselectedIds = (((this.unselectedIds!= null)&&(!this.unselectedIds.isEmpty()))?this.getUnselectedIds():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "unselectedIds", theUnselectedIds), currentHashCode, theUnselectedIds);
        }
        {
            boolean theAllSelected;
            theAllSelected = this.isAllSelected();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "allSelected", theAllSelected), currentHashCode, theAllSelected);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof MultiSelectionResult) {
            final MultiSelectionResult copy = ((MultiSelectionResult) draftCopy);
            if ((this.selectedIds!= null)&&(!this.selectedIds.isEmpty())) {
                List<ObjectId> sourceSelectedIds;
                sourceSelectedIds = (((this.selectedIds!= null)&&(!this.selectedIds.isEmpty()))?this.getSelectedIds():null);
                @SuppressWarnings("unchecked")
                List<ObjectId> copySelectedIds = ((List<ObjectId> ) strategy.copy(LocatorUtils.property(locator, "selectedIds", sourceSelectedIds), sourceSelectedIds));
                copy.selectedIds = null;
                if (copySelectedIds!= null) {
                    List<ObjectId> uniqueSelectedIdsl = copy.getSelectedIds();
                    uniqueSelectedIdsl.addAll(copySelectedIds);
                }
            } else {
                copy.selectedIds = null;
            }
            if ((this.unselectedIds!= null)&&(!this.unselectedIds.isEmpty())) {
                List<ObjectId> sourceUnselectedIds;
                sourceUnselectedIds = (((this.unselectedIds!= null)&&(!this.unselectedIds.isEmpty()))?this.getUnselectedIds():null);
                @SuppressWarnings("unchecked")
                List<ObjectId> copyUnselectedIds = ((List<ObjectId> ) strategy.copy(LocatorUtils.property(locator, "unselectedIds", sourceUnselectedIds), sourceUnselectedIds));
                copy.unselectedIds = null;
                if (copyUnselectedIds!= null) {
                    List<ObjectId> uniqueUnselectedIdsl = copy.getUnselectedIds();
                    uniqueUnselectedIdsl.addAll(copyUnselectedIds);
                }
            } else {
                copy.unselectedIds = null;
            }
            {
                boolean sourceAllSelected;
                sourceAllSelected = this.isAllSelected();
                boolean copyAllSelected = strategy.copy(LocatorUtils.property(locator, "allSelected", sourceAllSelected), sourceAllSelected);
                copy.setAllSelected(copyAllSelected);
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MultiSelectionResult();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final MultiSelectionResult.Builder<_B> _other) {
        if (this.selectedIds == null) {
            _other.selectedIds = null;
        } else {
            _other.selectedIds = new ArrayList<ObjectId.Builder<MultiSelectionResult.Builder<_B>>>();
            for (ObjectId _item: this.selectedIds) {
                _other.selectedIds.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        if (this.unselectedIds == null) {
            _other.unselectedIds = null;
        } else {
            _other.unselectedIds = new ArrayList<ObjectId.Builder<MultiSelectionResult.Builder<_B>>>();
            for (ObjectId _item: this.unselectedIds) {
                _other.unselectedIds.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.allSelected = this.allSelected;
    }

    public<_B >MultiSelectionResult.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new MultiSelectionResult.Builder<_B>(_parentBuilder, this, true);
    }

    public MultiSelectionResult.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static MultiSelectionResult.Builder<Void> builder() {
        return new MultiSelectionResult.Builder<Void>(null, null, false);
    }

    public static<_B >MultiSelectionResult.Builder<_B> copyOf(final MultiSelectionResult _other) {
        final MultiSelectionResult.Builder<_B> _newBuilder = new MultiSelectionResult.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final MultiSelectionResult.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree selectedIdsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("selectedIds"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(selectedIdsPropertyTree!= null):((selectedIdsPropertyTree == null)||(!selectedIdsPropertyTree.isLeaf())))) {
            if (this.selectedIds == null) {
                _other.selectedIds = null;
            } else {
                _other.selectedIds = new ArrayList<ObjectId.Builder<MultiSelectionResult.Builder<_B>>>();
                for (ObjectId _item: this.selectedIds) {
                    _other.selectedIds.add(((_item == null)?null:_item.newCopyBuilder(_other, selectedIdsPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree unselectedIdsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("unselectedIds"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(unselectedIdsPropertyTree!= null):((unselectedIdsPropertyTree == null)||(!unselectedIdsPropertyTree.isLeaf())))) {
            if (this.unselectedIds == null) {
                _other.unselectedIds = null;
            } else {
                _other.unselectedIds = new ArrayList<ObjectId.Builder<MultiSelectionResult.Builder<_B>>>();
                for (ObjectId _item: this.unselectedIds) {
                    _other.unselectedIds.add(((_item == null)?null:_item.newCopyBuilder(_other, unselectedIdsPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree allSelectedPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("allSelected"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(allSelectedPropertyTree!= null):((allSelectedPropertyTree == null)||(!allSelectedPropertyTree.isLeaf())))) {
            _other.allSelected = this.allSelected;
        }
    }

    public<_B >MultiSelectionResult.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new MultiSelectionResult.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public MultiSelectionResult.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >MultiSelectionResult.Builder<_B> copyOf(final MultiSelectionResult _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final MultiSelectionResult.Builder<_B> _newBuilder = new MultiSelectionResult.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static MultiSelectionResult.Builder<Void> copyExcept(final MultiSelectionResult _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static MultiSelectionResult.Builder<Void> copyOnly(final MultiSelectionResult _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final MultiSelectionResult _storedValue;
        private List<ObjectId.Builder<MultiSelectionResult.Builder<_B>>> selectedIds;
        private List<ObjectId.Builder<MultiSelectionResult.Builder<_B>>> unselectedIds;
        private boolean allSelected;

        public Builder(final _B _parentBuilder, final MultiSelectionResult _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.selectedIds == null) {
                        this.selectedIds = null;
                    } else {
                        this.selectedIds = new ArrayList<ObjectId.Builder<MultiSelectionResult.Builder<_B>>>();
                        for (ObjectId _item: _other.selectedIds) {
                            this.selectedIds.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    if (_other.unselectedIds == null) {
                        this.unselectedIds = null;
                    } else {
                        this.unselectedIds = new ArrayList<ObjectId.Builder<MultiSelectionResult.Builder<_B>>>();
                        for (ObjectId _item: _other.unselectedIds) {
                            this.unselectedIds.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.allSelected = _other.allSelected;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final MultiSelectionResult _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree selectedIdsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("selectedIds"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(selectedIdsPropertyTree!= null):((selectedIdsPropertyTree == null)||(!selectedIdsPropertyTree.isLeaf())))) {
                        if (_other.selectedIds == null) {
                            this.selectedIds = null;
                        } else {
                            this.selectedIds = new ArrayList<ObjectId.Builder<MultiSelectionResult.Builder<_B>>>();
                            for (ObjectId _item: _other.selectedIds) {
                                this.selectedIds.add(((_item == null)?null:_item.newCopyBuilder(this, selectedIdsPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree unselectedIdsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("unselectedIds"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(unselectedIdsPropertyTree!= null):((unselectedIdsPropertyTree == null)||(!unselectedIdsPropertyTree.isLeaf())))) {
                        if (_other.unselectedIds == null) {
                            this.unselectedIds = null;
                        } else {
                            this.unselectedIds = new ArrayList<ObjectId.Builder<MultiSelectionResult.Builder<_B>>>();
                            for (ObjectId _item: _other.unselectedIds) {
                                this.unselectedIds.add(((_item == null)?null:_item.newCopyBuilder(this, unselectedIdsPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree allSelectedPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("allSelected"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(allSelectedPropertyTree!= null):((allSelectedPropertyTree == null)||(!allSelectedPropertyTree.isLeaf())))) {
                        this.allSelected = _other.allSelected;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends MultiSelectionResult >_P init(final _P _product) {
            if (this.selectedIds!= null) {
                final List<ObjectId> selectedIds = new ArrayList<ObjectId>(this.selectedIds.size());
                for (ObjectId.Builder<MultiSelectionResult.Builder<_B>> _item: this.selectedIds) {
                    selectedIds.add(_item.build());
                }
                _product.selectedIds = selectedIds;
            }
            if (this.unselectedIds!= null) {
                final List<ObjectId> unselectedIds = new ArrayList<ObjectId>(this.unselectedIds.size());
                for (ObjectId.Builder<MultiSelectionResult.Builder<_B>> _item: this.unselectedIds) {
                    unselectedIds.add(_item.build());
                }
                _product.unselectedIds = unselectedIds;
            }
            _product.allSelected = this.allSelected;
            return _product;
        }

        /**
         * Adds the given items to the value of "selectedIds"
         * 
         * @param selectedIds
         *     Items to add to the value of the "selectedIds" property
         */
        public MultiSelectionResult.Builder<_B> addSelectedIds(final Iterable<? extends ObjectId> selectedIds) {
            if (selectedIds!= null) {
                if (this.selectedIds == null) {
                    this.selectedIds = new ArrayList<ObjectId.Builder<MultiSelectionResult.Builder<_B>>>();
                }
                for (ObjectId _item: selectedIds) {
                    this.selectedIds.add(new ObjectId.Builder<MultiSelectionResult.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "selectedIds" (any previous value will be replaced)
         * 
         * @param selectedIds
         *     New value of the "selectedIds" property.
         */
        public MultiSelectionResult.Builder<_B> withSelectedIds(final Iterable<? extends ObjectId> selectedIds) {
            if (this.selectedIds!= null) {
                this.selectedIds.clear();
            }
            return addSelectedIds(selectedIds);
        }

        /**
         * Adds the given items to the value of "selectedIds"
         * 
         * @param selectedIds
         *     Items to add to the value of the "selectedIds" property
         */
        public MultiSelectionResult.Builder<_B> addSelectedIds(ObjectId... selectedIds) {
            addSelectedIds(Arrays.asList(selectedIds));
            return this;
        }

        /**
         * Sets the new value of "selectedIds" (any previous value will be replaced)
         * 
         * @param selectedIds
         *     New value of the "selectedIds" property.
         */
        public MultiSelectionResult.Builder<_B> withSelectedIds(ObjectId... selectedIds) {
            withSelectedIds(Arrays.asList(selectedIds));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "SelectedIds" property.
         * Use {@link org.nuclos.schema.rest.ObjectId.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "SelectedIds" property.
         *     Use {@link org.nuclos.schema.rest.ObjectId.Builder#end()} to return to the current builder.
         */
        public ObjectId.Builder<? extends MultiSelectionResult.Builder<_B>> addSelectedIds() {
            if (this.selectedIds == null) {
                this.selectedIds = new ArrayList<ObjectId.Builder<MultiSelectionResult.Builder<_B>>>();
            }
            final ObjectId.Builder<MultiSelectionResult.Builder<_B>> selectedIds_Builder = new ObjectId.Builder<MultiSelectionResult.Builder<_B>>(this, null, false);
            this.selectedIds.add(selectedIds_Builder);
            return selectedIds_Builder;
        }

        /**
         * Adds the given items to the value of "unselectedIds"
         * 
         * @param unselectedIds
         *     Items to add to the value of the "unselectedIds" property
         */
        public MultiSelectionResult.Builder<_B> addUnselectedIds(final Iterable<? extends ObjectId> unselectedIds) {
            if (unselectedIds!= null) {
                if (this.unselectedIds == null) {
                    this.unselectedIds = new ArrayList<ObjectId.Builder<MultiSelectionResult.Builder<_B>>>();
                }
                for (ObjectId _item: unselectedIds) {
                    this.unselectedIds.add(new ObjectId.Builder<MultiSelectionResult.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "unselectedIds" (any previous value will be replaced)
         * 
         * @param unselectedIds
         *     New value of the "unselectedIds" property.
         */
        public MultiSelectionResult.Builder<_B> withUnselectedIds(final Iterable<? extends ObjectId> unselectedIds) {
            if (this.unselectedIds!= null) {
                this.unselectedIds.clear();
            }
            return addUnselectedIds(unselectedIds);
        }

        /**
         * Adds the given items to the value of "unselectedIds"
         * 
         * @param unselectedIds
         *     Items to add to the value of the "unselectedIds" property
         */
        public MultiSelectionResult.Builder<_B> addUnselectedIds(ObjectId... unselectedIds) {
            addUnselectedIds(Arrays.asList(unselectedIds));
            return this;
        }

        /**
         * Sets the new value of "unselectedIds" (any previous value will be replaced)
         * 
         * @param unselectedIds
         *     New value of the "unselectedIds" property.
         */
        public MultiSelectionResult.Builder<_B> withUnselectedIds(ObjectId... unselectedIds) {
            withUnselectedIds(Arrays.asList(unselectedIds));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "UnselectedIds" property.
         * Use {@link org.nuclos.schema.rest.ObjectId.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "UnselectedIds" property.
         *     Use {@link org.nuclos.schema.rest.ObjectId.Builder#end()} to return to the current builder.
         */
        public ObjectId.Builder<? extends MultiSelectionResult.Builder<_B>> addUnselectedIds() {
            if (this.unselectedIds == null) {
                this.unselectedIds = new ArrayList<ObjectId.Builder<MultiSelectionResult.Builder<_B>>>();
            }
            final ObjectId.Builder<MultiSelectionResult.Builder<_B>> unselectedIds_Builder = new ObjectId.Builder<MultiSelectionResult.Builder<_B>>(this, null, false);
            this.unselectedIds.add(unselectedIds_Builder);
            return unselectedIds_Builder;
        }

        /**
         * Sets the new value of "allSelected" (any previous value will be replaced)
         * 
         * @param allSelected
         *     New value of the "allSelected" property.
         */
        public MultiSelectionResult.Builder<_B> withAllSelected(final boolean allSelected) {
            this.allSelected = allSelected;
            return this;
        }

        @Override
        public MultiSelectionResult build() {
            if (_storedValue == null) {
                return this.init(new MultiSelectionResult());
            } else {
                return ((MultiSelectionResult) _storedValue);
            }
        }

        public MultiSelectionResult.Builder<_B> copyOf(final MultiSelectionResult _other) {
            _other.copyTo(this);
            return this;
        }

        public MultiSelectionResult.Builder<_B> copyOf(final MultiSelectionResult.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends MultiSelectionResult.Selector<MultiSelectionResult.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static MultiSelectionResult.Select _root() {
            return new MultiSelectionResult.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private ObjectId.Selector<TRoot, MultiSelectionResult.Selector<TRoot, TParent>> selectedIds = null;
        private ObjectId.Selector<TRoot, MultiSelectionResult.Selector<TRoot, TParent>> unselectedIds = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.selectedIds!= null) {
                products.put("selectedIds", this.selectedIds.init());
            }
            if (this.unselectedIds!= null) {
                products.put("unselectedIds", this.unselectedIds.init());
            }
            return products;
        }

        public ObjectId.Selector<TRoot, MultiSelectionResult.Selector<TRoot, TParent>> selectedIds() {
            return ((this.selectedIds == null)?this.selectedIds = new ObjectId.Selector<TRoot, MultiSelectionResult.Selector<TRoot, TParent>>(this._root, this, "selectedIds"):this.selectedIds);
        }

        public ObjectId.Selector<TRoot, MultiSelectionResult.Selector<TRoot, TParent>> unselectedIds() {
            return ((this.unselectedIds == null)?this.unselectedIds = new ObjectId.Selector<TRoot, MultiSelectionResult.Selector<TRoot, TParent>>(this._root, this, "unselectedIds"):this.unselectedIds);
        }

    }

}
