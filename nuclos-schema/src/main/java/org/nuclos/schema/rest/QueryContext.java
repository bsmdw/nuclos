
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;


/**
 * <p>Java class for query-context complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="query-context"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="offset" type="{http://www.w3.org/2001/XMLSchema}long" /&gt;
 *       &lt;attribute name="chunkSize" type="{http://www.w3.org/2001/XMLSchema}long" /&gt;
 *       &lt;attribute name="countTotal" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="search" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="searchFilterId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="vlpId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="where" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="attributes" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="idOnlySelection" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="orderBy" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="skipStatesAndGenerations" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="withTitleAndInfo" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="withMetaLink" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="withLayoutLink" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="withDetailLink" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "query-context")
public class QueryContext implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "offset")
    protected Long offset;
    @XmlAttribute(name = "chunkSize")
    protected Long chunkSize;
    @XmlAttribute(name = "countTotal")
    protected Boolean countTotal;
    @XmlAttribute(name = "search")
    protected String search;
    @XmlAttribute(name = "searchFilterId")
    protected String searchFilterId;
    @XmlAttribute(name = "vlpId")
    protected String vlpId;
    @XmlAttribute(name = "where")
    protected String where;
    @XmlAttribute(name = "attributes")
    protected String attributes;
    @XmlAttribute(name = "idOnlySelection")
    protected Boolean idOnlySelection;
    @XmlAttribute(name = "orderBy")
    protected String orderBy;
    @XmlAttribute(name = "skipStatesAndGenerations")
    protected Boolean skipStatesAndGenerations;
    @XmlAttribute(name = "withTitleAndInfo")
    protected Boolean withTitleAndInfo;
    @XmlAttribute(name = "withMetaLink")
    protected Boolean withMetaLink;
    @XmlAttribute(name = "withLayoutLink")
    protected Boolean withLayoutLink;
    @XmlAttribute(name = "withDetailLink")
    protected Boolean withDetailLink;

    /**
     * Gets the value of the offset property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getOffset() {
        return offset;
    }

    /**
     * Sets the value of the offset property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setOffset(Long value) {
        this.offset = value;
    }

    /**
     * Gets the value of the chunkSize property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getChunkSize() {
        return chunkSize;
    }

    /**
     * Sets the value of the chunkSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setChunkSize(Long value) {
        this.chunkSize = value;
    }

    /**
     * Gets the value of the countTotal property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCountTotal() {
        return countTotal;
    }

    /**
     * Sets the value of the countTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCountTotal(Boolean value) {
        this.countTotal = value;
    }

    /**
     * Gets the value of the search property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSearch() {
        return search;
    }

    /**
     * Sets the value of the search property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSearch(String value) {
        this.search = value;
    }

    /**
     * Gets the value of the searchFilterId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSearchFilterId() {
        return searchFilterId;
    }

    /**
     * Sets the value of the searchFilterId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSearchFilterId(String value) {
        this.searchFilterId = value;
    }

    /**
     * Gets the value of the vlpId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVlpId() {
        return vlpId;
    }

    /**
     * Sets the value of the vlpId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVlpId(String value) {
        this.vlpId = value;
    }

    /**
     * Gets the value of the where property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWhere() {
        return where;
    }

    /**
     * Sets the value of the where property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWhere(String value) {
        this.where = value;
    }

    /**
     * Gets the value of the attributes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributes() {
        return attributes;
    }

    /**
     * Sets the value of the attributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributes(String value) {
        this.attributes = value;
    }

    /**
     * Gets the value of the idOnlySelection property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIdOnlySelection() {
        return idOnlySelection;
    }

    /**
     * Sets the value of the idOnlySelection property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIdOnlySelection(Boolean value) {
        this.idOnlySelection = value;
    }

    /**
     * Gets the value of the orderBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderBy() {
        return orderBy;
    }

    /**
     * Sets the value of the orderBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderBy(String value) {
        this.orderBy = value;
    }

    /**
     * Gets the value of the skipStatesAndGenerations property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSkipStatesAndGenerations() {
        return skipStatesAndGenerations;
    }

    /**
     * Sets the value of the skipStatesAndGenerations property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSkipStatesAndGenerations(Boolean value) {
        this.skipStatesAndGenerations = value;
    }

    /**
     * Gets the value of the withTitleAndInfo property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isWithTitleAndInfo() {
        return withTitleAndInfo;
    }

    /**
     * Sets the value of the withTitleAndInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWithTitleAndInfo(Boolean value) {
        this.withTitleAndInfo = value;
    }

    /**
     * Gets the value of the withMetaLink property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isWithMetaLink() {
        return withMetaLink;
    }

    /**
     * Sets the value of the withMetaLink property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWithMetaLink(Boolean value) {
        this.withMetaLink = value;
    }

    /**
     * Gets the value of the withLayoutLink property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isWithLayoutLink() {
        return withLayoutLink;
    }

    /**
     * Sets the value of the withLayoutLink property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWithLayoutLink(Boolean value) {
        this.withLayoutLink = value;
    }

    /**
     * Gets the value of the withDetailLink property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isWithDetailLink() {
        return withDetailLink;
    }

    /**
     * Sets the value of the withDetailLink property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWithDetailLink(Boolean value) {
        this.withDetailLink = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            Long theOffset;
            theOffset = this.getOffset();
            strategy.appendField(locator, this, "offset", buffer, theOffset);
        }
        {
            Long theChunkSize;
            theChunkSize = this.getChunkSize();
            strategy.appendField(locator, this, "chunkSize", buffer, theChunkSize);
        }
        {
            Boolean theCountTotal;
            theCountTotal = this.isCountTotal();
            strategy.appendField(locator, this, "countTotal", buffer, theCountTotal);
        }
        {
            String theSearch;
            theSearch = this.getSearch();
            strategy.appendField(locator, this, "search", buffer, theSearch);
        }
        {
            String theSearchFilterId;
            theSearchFilterId = this.getSearchFilterId();
            strategy.appendField(locator, this, "searchFilterId", buffer, theSearchFilterId);
        }
        {
            String theVlpId;
            theVlpId = this.getVlpId();
            strategy.appendField(locator, this, "vlpId", buffer, theVlpId);
        }
        {
            String theWhere;
            theWhere = this.getWhere();
            strategy.appendField(locator, this, "where", buffer, theWhere);
        }
        {
            String theAttributes;
            theAttributes = this.getAttributes();
            strategy.appendField(locator, this, "attributes", buffer, theAttributes);
        }
        {
            Boolean theIdOnlySelection;
            theIdOnlySelection = this.isIdOnlySelection();
            strategy.appendField(locator, this, "idOnlySelection", buffer, theIdOnlySelection);
        }
        {
            String theOrderBy;
            theOrderBy = this.getOrderBy();
            strategy.appendField(locator, this, "orderBy", buffer, theOrderBy);
        }
        {
            Boolean theSkipStatesAndGenerations;
            theSkipStatesAndGenerations = this.isSkipStatesAndGenerations();
            strategy.appendField(locator, this, "skipStatesAndGenerations", buffer, theSkipStatesAndGenerations);
        }
        {
            Boolean theWithTitleAndInfo;
            theWithTitleAndInfo = this.isWithTitleAndInfo();
            strategy.appendField(locator, this, "withTitleAndInfo", buffer, theWithTitleAndInfo);
        }
        {
            Boolean theWithMetaLink;
            theWithMetaLink = this.isWithMetaLink();
            strategy.appendField(locator, this, "withMetaLink", buffer, theWithMetaLink);
        }
        {
            Boolean theWithLayoutLink;
            theWithLayoutLink = this.isWithLayoutLink();
            strategy.appendField(locator, this, "withLayoutLink", buffer, theWithLayoutLink);
        }
        {
            Boolean theWithDetailLink;
            theWithDetailLink = this.isWithDetailLink();
            strategy.appendField(locator, this, "withDetailLink", buffer, theWithDetailLink);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof QueryContext)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final QueryContext that = ((QueryContext) object);
        {
            Long lhsOffset;
            lhsOffset = this.getOffset();
            Long rhsOffset;
            rhsOffset = that.getOffset();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "offset", lhsOffset), LocatorUtils.property(thatLocator, "offset", rhsOffset), lhsOffset, rhsOffset)) {
                return false;
            }
        }
        {
            Long lhsChunkSize;
            lhsChunkSize = this.getChunkSize();
            Long rhsChunkSize;
            rhsChunkSize = that.getChunkSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "chunkSize", lhsChunkSize), LocatorUtils.property(thatLocator, "chunkSize", rhsChunkSize), lhsChunkSize, rhsChunkSize)) {
                return false;
            }
        }
        {
            Boolean lhsCountTotal;
            lhsCountTotal = this.isCountTotal();
            Boolean rhsCountTotal;
            rhsCountTotal = that.isCountTotal();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "countTotal", lhsCountTotal), LocatorUtils.property(thatLocator, "countTotal", rhsCountTotal), lhsCountTotal, rhsCountTotal)) {
                return false;
            }
        }
        {
            String lhsSearch;
            lhsSearch = this.getSearch();
            String rhsSearch;
            rhsSearch = that.getSearch();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "search", lhsSearch), LocatorUtils.property(thatLocator, "search", rhsSearch), lhsSearch, rhsSearch)) {
                return false;
            }
        }
        {
            String lhsSearchFilterId;
            lhsSearchFilterId = this.getSearchFilterId();
            String rhsSearchFilterId;
            rhsSearchFilterId = that.getSearchFilterId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "searchFilterId", lhsSearchFilterId), LocatorUtils.property(thatLocator, "searchFilterId", rhsSearchFilterId), lhsSearchFilterId, rhsSearchFilterId)) {
                return false;
            }
        }
        {
            String lhsVlpId;
            lhsVlpId = this.getVlpId();
            String rhsVlpId;
            rhsVlpId = that.getVlpId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "vlpId", lhsVlpId), LocatorUtils.property(thatLocator, "vlpId", rhsVlpId), lhsVlpId, rhsVlpId)) {
                return false;
            }
        }
        {
            String lhsWhere;
            lhsWhere = this.getWhere();
            String rhsWhere;
            rhsWhere = that.getWhere();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "where", lhsWhere), LocatorUtils.property(thatLocator, "where", rhsWhere), lhsWhere, rhsWhere)) {
                return false;
            }
        }
        {
            String lhsAttributes;
            lhsAttributes = this.getAttributes();
            String rhsAttributes;
            rhsAttributes = that.getAttributes();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "attributes", lhsAttributes), LocatorUtils.property(thatLocator, "attributes", rhsAttributes), lhsAttributes, rhsAttributes)) {
                return false;
            }
        }
        {
            Boolean lhsIdOnlySelection;
            lhsIdOnlySelection = this.isIdOnlySelection();
            Boolean rhsIdOnlySelection;
            rhsIdOnlySelection = that.isIdOnlySelection();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "idOnlySelection", lhsIdOnlySelection), LocatorUtils.property(thatLocator, "idOnlySelection", rhsIdOnlySelection), lhsIdOnlySelection, rhsIdOnlySelection)) {
                return false;
            }
        }
        {
            String lhsOrderBy;
            lhsOrderBy = this.getOrderBy();
            String rhsOrderBy;
            rhsOrderBy = that.getOrderBy();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "orderBy", lhsOrderBy), LocatorUtils.property(thatLocator, "orderBy", rhsOrderBy), lhsOrderBy, rhsOrderBy)) {
                return false;
            }
        }
        {
            Boolean lhsSkipStatesAndGenerations;
            lhsSkipStatesAndGenerations = this.isSkipStatesAndGenerations();
            Boolean rhsSkipStatesAndGenerations;
            rhsSkipStatesAndGenerations = that.isSkipStatesAndGenerations();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "skipStatesAndGenerations", lhsSkipStatesAndGenerations), LocatorUtils.property(thatLocator, "skipStatesAndGenerations", rhsSkipStatesAndGenerations), lhsSkipStatesAndGenerations, rhsSkipStatesAndGenerations)) {
                return false;
            }
        }
        {
            Boolean lhsWithTitleAndInfo;
            lhsWithTitleAndInfo = this.isWithTitleAndInfo();
            Boolean rhsWithTitleAndInfo;
            rhsWithTitleAndInfo = that.isWithTitleAndInfo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "withTitleAndInfo", lhsWithTitleAndInfo), LocatorUtils.property(thatLocator, "withTitleAndInfo", rhsWithTitleAndInfo), lhsWithTitleAndInfo, rhsWithTitleAndInfo)) {
                return false;
            }
        }
        {
            Boolean lhsWithMetaLink;
            lhsWithMetaLink = this.isWithMetaLink();
            Boolean rhsWithMetaLink;
            rhsWithMetaLink = that.isWithMetaLink();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "withMetaLink", lhsWithMetaLink), LocatorUtils.property(thatLocator, "withMetaLink", rhsWithMetaLink), lhsWithMetaLink, rhsWithMetaLink)) {
                return false;
            }
        }
        {
            Boolean lhsWithLayoutLink;
            lhsWithLayoutLink = this.isWithLayoutLink();
            Boolean rhsWithLayoutLink;
            rhsWithLayoutLink = that.isWithLayoutLink();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "withLayoutLink", lhsWithLayoutLink), LocatorUtils.property(thatLocator, "withLayoutLink", rhsWithLayoutLink), lhsWithLayoutLink, rhsWithLayoutLink)) {
                return false;
            }
        }
        {
            Boolean lhsWithDetailLink;
            lhsWithDetailLink = this.isWithDetailLink();
            Boolean rhsWithDetailLink;
            rhsWithDetailLink = that.isWithDetailLink();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "withDetailLink", lhsWithDetailLink), LocatorUtils.property(thatLocator, "withDetailLink", rhsWithDetailLink), lhsWithDetailLink, rhsWithDetailLink)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            Long theOffset;
            theOffset = this.getOffset();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "offset", theOffset), currentHashCode, theOffset);
        }
        {
            Long theChunkSize;
            theChunkSize = this.getChunkSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "chunkSize", theChunkSize), currentHashCode, theChunkSize);
        }
        {
            Boolean theCountTotal;
            theCountTotal = this.isCountTotal();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "countTotal", theCountTotal), currentHashCode, theCountTotal);
        }
        {
            String theSearch;
            theSearch = this.getSearch();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "search", theSearch), currentHashCode, theSearch);
        }
        {
            String theSearchFilterId;
            theSearchFilterId = this.getSearchFilterId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "searchFilterId", theSearchFilterId), currentHashCode, theSearchFilterId);
        }
        {
            String theVlpId;
            theVlpId = this.getVlpId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "vlpId", theVlpId), currentHashCode, theVlpId);
        }
        {
            String theWhere;
            theWhere = this.getWhere();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "where", theWhere), currentHashCode, theWhere);
        }
        {
            String theAttributes;
            theAttributes = this.getAttributes();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "attributes", theAttributes), currentHashCode, theAttributes);
        }
        {
            Boolean theIdOnlySelection;
            theIdOnlySelection = this.isIdOnlySelection();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "idOnlySelection", theIdOnlySelection), currentHashCode, theIdOnlySelection);
        }
        {
            String theOrderBy;
            theOrderBy = this.getOrderBy();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "orderBy", theOrderBy), currentHashCode, theOrderBy);
        }
        {
            Boolean theSkipStatesAndGenerations;
            theSkipStatesAndGenerations = this.isSkipStatesAndGenerations();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "skipStatesAndGenerations", theSkipStatesAndGenerations), currentHashCode, theSkipStatesAndGenerations);
        }
        {
            Boolean theWithTitleAndInfo;
            theWithTitleAndInfo = this.isWithTitleAndInfo();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "withTitleAndInfo", theWithTitleAndInfo), currentHashCode, theWithTitleAndInfo);
        }
        {
            Boolean theWithMetaLink;
            theWithMetaLink = this.isWithMetaLink();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "withMetaLink", theWithMetaLink), currentHashCode, theWithMetaLink);
        }
        {
            Boolean theWithLayoutLink;
            theWithLayoutLink = this.isWithLayoutLink();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "withLayoutLink", theWithLayoutLink), currentHashCode, theWithLayoutLink);
        }
        {
            Boolean theWithDetailLink;
            theWithDetailLink = this.isWithDetailLink();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "withDetailLink", theWithDetailLink), currentHashCode, theWithDetailLink);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof QueryContext) {
            final QueryContext copy = ((QueryContext) draftCopy);
            if (this.offset!= null) {
                Long sourceOffset;
                sourceOffset = this.getOffset();
                Long copyOffset = ((Long) strategy.copy(LocatorUtils.property(locator, "offset", sourceOffset), sourceOffset));
                copy.setOffset(copyOffset);
            } else {
                copy.offset = null;
            }
            if (this.chunkSize!= null) {
                Long sourceChunkSize;
                sourceChunkSize = this.getChunkSize();
                Long copyChunkSize = ((Long) strategy.copy(LocatorUtils.property(locator, "chunkSize", sourceChunkSize), sourceChunkSize));
                copy.setChunkSize(copyChunkSize);
            } else {
                copy.chunkSize = null;
            }
            if (this.countTotal!= null) {
                Boolean sourceCountTotal;
                sourceCountTotal = this.isCountTotal();
                Boolean copyCountTotal = ((Boolean) strategy.copy(LocatorUtils.property(locator, "countTotal", sourceCountTotal), sourceCountTotal));
                copy.setCountTotal(copyCountTotal);
            } else {
                copy.countTotal = null;
            }
            if (this.search!= null) {
                String sourceSearch;
                sourceSearch = this.getSearch();
                String copySearch = ((String) strategy.copy(LocatorUtils.property(locator, "search", sourceSearch), sourceSearch));
                copy.setSearch(copySearch);
            } else {
                copy.search = null;
            }
            if (this.searchFilterId!= null) {
                String sourceSearchFilterId;
                sourceSearchFilterId = this.getSearchFilterId();
                String copySearchFilterId = ((String) strategy.copy(LocatorUtils.property(locator, "searchFilterId", sourceSearchFilterId), sourceSearchFilterId));
                copy.setSearchFilterId(copySearchFilterId);
            } else {
                copy.searchFilterId = null;
            }
            if (this.vlpId!= null) {
                String sourceVlpId;
                sourceVlpId = this.getVlpId();
                String copyVlpId = ((String) strategy.copy(LocatorUtils.property(locator, "vlpId", sourceVlpId), sourceVlpId));
                copy.setVlpId(copyVlpId);
            } else {
                copy.vlpId = null;
            }
            if (this.where!= null) {
                String sourceWhere;
                sourceWhere = this.getWhere();
                String copyWhere = ((String) strategy.copy(LocatorUtils.property(locator, "where", sourceWhere), sourceWhere));
                copy.setWhere(copyWhere);
            } else {
                copy.where = null;
            }
            if (this.attributes!= null) {
                String sourceAttributes;
                sourceAttributes = this.getAttributes();
                String copyAttributes = ((String) strategy.copy(LocatorUtils.property(locator, "attributes", sourceAttributes), sourceAttributes));
                copy.setAttributes(copyAttributes);
            } else {
                copy.attributes = null;
            }
            if (this.idOnlySelection!= null) {
                Boolean sourceIdOnlySelection;
                sourceIdOnlySelection = this.isIdOnlySelection();
                Boolean copyIdOnlySelection = ((Boolean) strategy.copy(LocatorUtils.property(locator, "idOnlySelection", sourceIdOnlySelection), sourceIdOnlySelection));
                copy.setIdOnlySelection(copyIdOnlySelection);
            } else {
                copy.idOnlySelection = null;
            }
            if (this.orderBy!= null) {
                String sourceOrderBy;
                sourceOrderBy = this.getOrderBy();
                String copyOrderBy = ((String) strategy.copy(LocatorUtils.property(locator, "orderBy", sourceOrderBy), sourceOrderBy));
                copy.setOrderBy(copyOrderBy);
            } else {
                copy.orderBy = null;
            }
            if (this.skipStatesAndGenerations!= null) {
                Boolean sourceSkipStatesAndGenerations;
                sourceSkipStatesAndGenerations = this.isSkipStatesAndGenerations();
                Boolean copySkipStatesAndGenerations = ((Boolean) strategy.copy(LocatorUtils.property(locator, "skipStatesAndGenerations", sourceSkipStatesAndGenerations), sourceSkipStatesAndGenerations));
                copy.setSkipStatesAndGenerations(copySkipStatesAndGenerations);
            } else {
                copy.skipStatesAndGenerations = null;
            }
            if (this.withTitleAndInfo!= null) {
                Boolean sourceWithTitleAndInfo;
                sourceWithTitleAndInfo = this.isWithTitleAndInfo();
                Boolean copyWithTitleAndInfo = ((Boolean) strategy.copy(LocatorUtils.property(locator, "withTitleAndInfo", sourceWithTitleAndInfo), sourceWithTitleAndInfo));
                copy.setWithTitleAndInfo(copyWithTitleAndInfo);
            } else {
                copy.withTitleAndInfo = null;
            }
            if (this.withMetaLink!= null) {
                Boolean sourceWithMetaLink;
                sourceWithMetaLink = this.isWithMetaLink();
                Boolean copyWithMetaLink = ((Boolean) strategy.copy(LocatorUtils.property(locator, "withMetaLink", sourceWithMetaLink), sourceWithMetaLink));
                copy.setWithMetaLink(copyWithMetaLink);
            } else {
                copy.withMetaLink = null;
            }
            if (this.withLayoutLink!= null) {
                Boolean sourceWithLayoutLink;
                sourceWithLayoutLink = this.isWithLayoutLink();
                Boolean copyWithLayoutLink = ((Boolean) strategy.copy(LocatorUtils.property(locator, "withLayoutLink", sourceWithLayoutLink), sourceWithLayoutLink));
                copy.setWithLayoutLink(copyWithLayoutLink);
            } else {
                copy.withLayoutLink = null;
            }
            if (this.withDetailLink!= null) {
                Boolean sourceWithDetailLink;
                sourceWithDetailLink = this.isWithDetailLink();
                Boolean copyWithDetailLink = ((Boolean) strategy.copy(LocatorUtils.property(locator, "withDetailLink", sourceWithDetailLink), sourceWithDetailLink));
                copy.setWithDetailLink(copyWithDetailLink);
            } else {
                copy.withDetailLink = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new QueryContext();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final QueryContext.Builder<_B> _other) {
        _other.offset = this.offset;
        _other.chunkSize = this.chunkSize;
        _other.countTotal = this.countTotal;
        _other.search = this.search;
        _other.searchFilterId = this.searchFilterId;
        _other.vlpId = this.vlpId;
        _other.where = this.where;
        _other.attributes = this.attributes;
        _other.idOnlySelection = this.idOnlySelection;
        _other.orderBy = this.orderBy;
        _other.skipStatesAndGenerations = this.skipStatesAndGenerations;
        _other.withTitleAndInfo = this.withTitleAndInfo;
        _other.withMetaLink = this.withMetaLink;
        _other.withLayoutLink = this.withLayoutLink;
        _other.withDetailLink = this.withDetailLink;
    }

    public<_B >QueryContext.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new QueryContext.Builder<_B>(_parentBuilder, this, true);
    }

    public QueryContext.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static QueryContext.Builder<Void> builder() {
        return new QueryContext.Builder<Void>(null, null, false);
    }

    public static<_B >QueryContext.Builder<_B> copyOf(final QueryContext _other) {
        final QueryContext.Builder<_B> _newBuilder = new QueryContext.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final QueryContext.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree offsetPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("offset"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(offsetPropertyTree!= null):((offsetPropertyTree == null)||(!offsetPropertyTree.isLeaf())))) {
            _other.offset = this.offset;
        }
        final PropertyTree chunkSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("chunkSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(chunkSizePropertyTree!= null):((chunkSizePropertyTree == null)||(!chunkSizePropertyTree.isLeaf())))) {
            _other.chunkSize = this.chunkSize;
        }
        final PropertyTree countTotalPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("countTotal"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(countTotalPropertyTree!= null):((countTotalPropertyTree == null)||(!countTotalPropertyTree.isLeaf())))) {
            _other.countTotal = this.countTotal;
        }
        final PropertyTree searchPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("search"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(searchPropertyTree!= null):((searchPropertyTree == null)||(!searchPropertyTree.isLeaf())))) {
            _other.search = this.search;
        }
        final PropertyTree searchFilterIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("searchFilterId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(searchFilterIdPropertyTree!= null):((searchFilterIdPropertyTree == null)||(!searchFilterIdPropertyTree.isLeaf())))) {
            _other.searchFilterId = this.searchFilterId;
        }
        final PropertyTree vlpIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("vlpId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(vlpIdPropertyTree!= null):((vlpIdPropertyTree == null)||(!vlpIdPropertyTree.isLeaf())))) {
            _other.vlpId = this.vlpId;
        }
        final PropertyTree wherePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("where"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(wherePropertyTree!= null):((wherePropertyTree == null)||(!wherePropertyTree.isLeaf())))) {
            _other.where = this.where;
        }
        final PropertyTree attributesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("attributes"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(attributesPropertyTree!= null):((attributesPropertyTree == null)||(!attributesPropertyTree.isLeaf())))) {
            _other.attributes = this.attributes;
        }
        final PropertyTree idOnlySelectionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("idOnlySelection"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(idOnlySelectionPropertyTree!= null):((idOnlySelectionPropertyTree == null)||(!idOnlySelectionPropertyTree.isLeaf())))) {
            _other.idOnlySelection = this.idOnlySelection;
        }
        final PropertyTree orderByPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("orderBy"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(orderByPropertyTree!= null):((orderByPropertyTree == null)||(!orderByPropertyTree.isLeaf())))) {
            _other.orderBy = this.orderBy;
        }
        final PropertyTree skipStatesAndGenerationsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("skipStatesAndGenerations"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(skipStatesAndGenerationsPropertyTree!= null):((skipStatesAndGenerationsPropertyTree == null)||(!skipStatesAndGenerationsPropertyTree.isLeaf())))) {
            _other.skipStatesAndGenerations = this.skipStatesAndGenerations;
        }
        final PropertyTree withTitleAndInfoPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("withTitleAndInfo"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(withTitleAndInfoPropertyTree!= null):((withTitleAndInfoPropertyTree == null)||(!withTitleAndInfoPropertyTree.isLeaf())))) {
            _other.withTitleAndInfo = this.withTitleAndInfo;
        }
        final PropertyTree withMetaLinkPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("withMetaLink"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(withMetaLinkPropertyTree!= null):((withMetaLinkPropertyTree == null)||(!withMetaLinkPropertyTree.isLeaf())))) {
            _other.withMetaLink = this.withMetaLink;
        }
        final PropertyTree withLayoutLinkPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("withLayoutLink"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(withLayoutLinkPropertyTree!= null):((withLayoutLinkPropertyTree == null)||(!withLayoutLinkPropertyTree.isLeaf())))) {
            _other.withLayoutLink = this.withLayoutLink;
        }
        final PropertyTree withDetailLinkPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("withDetailLink"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(withDetailLinkPropertyTree!= null):((withDetailLinkPropertyTree == null)||(!withDetailLinkPropertyTree.isLeaf())))) {
            _other.withDetailLink = this.withDetailLink;
        }
    }

    public<_B >QueryContext.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new QueryContext.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public QueryContext.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >QueryContext.Builder<_B> copyOf(final QueryContext _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final QueryContext.Builder<_B> _newBuilder = new QueryContext.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static QueryContext.Builder<Void> copyExcept(final QueryContext _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static QueryContext.Builder<Void> copyOnly(final QueryContext _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final QueryContext _storedValue;
        private Long offset;
        private Long chunkSize;
        private Boolean countTotal;
        private String search;
        private String searchFilterId;
        private String vlpId;
        private String where;
        private String attributes;
        private Boolean idOnlySelection;
        private String orderBy;
        private Boolean skipStatesAndGenerations;
        private Boolean withTitleAndInfo;
        private Boolean withMetaLink;
        private Boolean withLayoutLink;
        private Boolean withDetailLink;

        public Builder(final _B _parentBuilder, final QueryContext _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.offset = _other.offset;
                    this.chunkSize = _other.chunkSize;
                    this.countTotal = _other.countTotal;
                    this.search = _other.search;
                    this.searchFilterId = _other.searchFilterId;
                    this.vlpId = _other.vlpId;
                    this.where = _other.where;
                    this.attributes = _other.attributes;
                    this.idOnlySelection = _other.idOnlySelection;
                    this.orderBy = _other.orderBy;
                    this.skipStatesAndGenerations = _other.skipStatesAndGenerations;
                    this.withTitleAndInfo = _other.withTitleAndInfo;
                    this.withMetaLink = _other.withMetaLink;
                    this.withLayoutLink = _other.withLayoutLink;
                    this.withDetailLink = _other.withDetailLink;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final QueryContext _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree offsetPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("offset"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(offsetPropertyTree!= null):((offsetPropertyTree == null)||(!offsetPropertyTree.isLeaf())))) {
                        this.offset = _other.offset;
                    }
                    final PropertyTree chunkSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("chunkSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(chunkSizePropertyTree!= null):((chunkSizePropertyTree == null)||(!chunkSizePropertyTree.isLeaf())))) {
                        this.chunkSize = _other.chunkSize;
                    }
                    final PropertyTree countTotalPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("countTotal"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(countTotalPropertyTree!= null):((countTotalPropertyTree == null)||(!countTotalPropertyTree.isLeaf())))) {
                        this.countTotal = _other.countTotal;
                    }
                    final PropertyTree searchPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("search"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(searchPropertyTree!= null):((searchPropertyTree == null)||(!searchPropertyTree.isLeaf())))) {
                        this.search = _other.search;
                    }
                    final PropertyTree searchFilterIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("searchFilterId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(searchFilterIdPropertyTree!= null):((searchFilterIdPropertyTree == null)||(!searchFilterIdPropertyTree.isLeaf())))) {
                        this.searchFilterId = _other.searchFilterId;
                    }
                    final PropertyTree vlpIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("vlpId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(vlpIdPropertyTree!= null):((vlpIdPropertyTree == null)||(!vlpIdPropertyTree.isLeaf())))) {
                        this.vlpId = _other.vlpId;
                    }
                    final PropertyTree wherePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("where"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(wherePropertyTree!= null):((wherePropertyTree == null)||(!wherePropertyTree.isLeaf())))) {
                        this.where = _other.where;
                    }
                    final PropertyTree attributesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("attributes"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(attributesPropertyTree!= null):((attributesPropertyTree == null)||(!attributesPropertyTree.isLeaf())))) {
                        this.attributes = _other.attributes;
                    }
                    final PropertyTree idOnlySelectionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("idOnlySelection"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(idOnlySelectionPropertyTree!= null):((idOnlySelectionPropertyTree == null)||(!idOnlySelectionPropertyTree.isLeaf())))) {
                        this.idOnlySelection = _other.idOnlySelection;
                    }
                    final PropertyTree orderByPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("orderBy"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(orderByPropertyTree!= null):((orderByPropertyTree == null)||(!orderByPropertyTree.isLeaf())))) {
                        this.orderBy = _other.orderBy;
                    }
                    final PropertyTree skipStatesAndGenerationsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("skipStatesAndGenerations"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(skipStatesAndGenerationsPropertyTree!= null):((skipStatesAndGenerationsPropertyTree == null)||(!skipStatesAndGenerationsPropertyTree.isLeaf())))) {
                        this.skipStatesAndGenerations = _other.skipStatesAndGenerations;
                    }
                    final PropertyTree withTitleAndInfoPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("withTitleAndInfo"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(withTitleAndInfoPropertyTree!= null):((withTitleAndInfoPropertyTree == null)||(!withTitleAndInfoPropertyTree.isLeaf())))) {
                        this.withTitleAndInfo = _other.withTitleAndInfo;
                    }
                    final PropertyTree withMetaLinkPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("withMetaLink"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(withMetaLinkPropertyTree!= null):((withMetaLinkPropertyTree == null)||(!withMetaLinkPropertyTree.isLeaf())))) {
                        this.withMetaLink = _other.withMetaLink;
                    }
                    final PropertyTree withLayoutLinkPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("withLayoutLink"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(withLayoutLinkPropertyTree!= null):((withLayoutLinkPropertyTree == null)||(!withLayoutLinkPropertyTree.isLeaf())))) {
                        this.withLayoutLink = _other.withLayoutLink;
                    }
                    final PropertyTree withDetailLinkPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("withDetailLink"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(withDetailLinkPropertyTree!= null):((withDetailLinkPropertyTree == null)||(!withDetailLinkPropertyTree.isLeaf())))) {
                        this.withDetailLink = _other.withDetailLink;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends QueryContext >_P init(final _P _product) {
            _product.offset = this.offset;
            _product.chunkSize = this.chunkSize;
            _product.countTotal = this.countTotal;
            _product.search = this.search;
            _product.searchFilterId = this.searchFilterId;
            _product.vlpId = this.vlpId;
            _product.where = this.where;
            _product.attributes = this.attributes;
            _product.idOnlySelection = this.idOnlySelection;
            _product.orderBy = this.orderBy;
            _product.skipStatesAndGenerations = this.skipStatesAndGenerations;
            _product.withTitleAndInfo = this.withTitleAndInfo;
            _product.withMetaLink = this.withMetaLink;
            _product.withLayoutLink = this.withLayoutLink;
            _product.withDetailLink = this.withDetailLink;
            return _product;
        }

        /**
         * Sets the new value of "offset" (any previous value will be replaced)
         * 
         * @param offset
         *     New value of the "offset" property.
         */
        public QueryContext.Builder<_B> withOffset(final Long offset) {
            this.offset = offset;
            return this;
        }

        /**
         * Sets the new value of "chunkSize" (any previous value will be replaced)
         * 
         * @param chunkSize
         *     New value of the "chunkSize" property.
         */
        public QueryContext.Builder<_B> withChunkSize(final Long chunkSize) {
            this.chunkSize = chunkSize;
            return this;
        }

        /**
         * Sets the new value of "countTotal" (any previous value will be replaced)
         * 
         * @param countTotal
         *     New value of the "countTotal" property.
         */
        public QueryContext.Builder<_B> withCountTotal(final Boolean countTotal) {
            this.countTotal = countTotal;
            return this;
        }

        /**
         * Sets the new value of "search" (any previous value will be replaced)
         * 
         * @param search
         *     New value of the "search" property.
         */
        public QueryContext.Builder<_B> withSearch(final String search) {
            this.search = search;
            return this;
        }

        /**
         * Sets the new value of "searchFilterId" (any previous value will be replaced)
         * 
         * @param searchFilterId
         *     New value of the "searchFilterId" property.
         */
        public QueryContext.Builder<_B> withSearchFilterId(final String searchFilterId) {
            this.searchFilterId = searchFilterId;
            return this;
        }

        /**
         * Sets the new value of "vlpId" (any previous value will be replaced)
         * 
         * @param vlpId
         *     New value of the "vlpId" property.
         */
        public QueryContext.Builder<_B> withVlpId(final String vlpId) {
            this.vlpId = vlpId;
            return this;
        }

        /**
         * Sets the new value of "where" (any previous value will be replaced)
         * 
         * @param where
         *     New value of the "where" property.
         */
        public QueryContext.Builder<_B> withWhere(final String where) {
            this.where = where;
            return this;
        }

        /**
         * Sets the new value of "attributes" (any previous value will be replaced)
         * 
         * @param attributes
         *     New value of the "attributes" property.
         */
        public QueryContext.Builder<_B> withAttributes(final String attributes) {
            this.attributes = attributes;
            return this;
        }

        /**
         * Sets the new value of "idOnlySelection" (any previous value will be replaced)
         * 
         * @param idOnlySelection
         *     New value of the "idOnlySelection" property.
         */
        public QueryContext.Builder<_B> withIdOnlySelection(final Boolean idOnlySelection) {
            this.idOnlySelection = idOnlySelection;
            return this;
        }

        /**
         * Sets the new value of "orderBy" (any previous value will be replaced)
         * 
         * @param orderBy
         *     New value of the "orderBy" property.
         */
        public QueryContext.Builder<_B> withOrderBy(final String orderBy) {
            this.orderBy = orderBy;
            return this;
        }

        /**
         * Sets the new value of "skipStatesAndGenerations" (any previous value will be replaced)
         * 
         * @param skipStatesAndGenerations
         *     New value of the "skipStatesAndGenerations" property.
         */
        public QueryContext.Builder<_B> withSkipStatesAndGenerations(final Boolean skipStatesAndGenerations) {
            this.skipStatesAndGenerations = skipStatesAndGenerations;
            return this;
        }

        /**
         * Sets the new value of "withTitleAndInfo" (any previous value will be replaced)
         * 
         * @param withTitleAndInfo
         *     New value of the "withTitleAndInfo" property.
         */
        public QueryContext.Builder<_B> withWithTitleAndInfo(final Boolean withTitleAndInfo) {
            this.withTitleAndInfo = withTitleAndInfo;
            return this;
        }

        /**
         * Sets the new value of "withMetaLink" (any previous value will be replaced)
         * 
         * @param withMetaLink
         *     New value of the "withMetaLink" property.
         */
        public QueryContext.Builder<_B> withWithMetaLink(final Boolean withMetaLink) {
            this.withMetaLink = withMetaLink;
            return this;
        }

        /**
         * Sets the new value of "withLayoutLink" (any previous value will be replaced)
         * 
         * @param withLayoutLink
         *     New value of the "withLayoutLink" property.
         */
        public QueryContext.Builder<_B> withWithLayoutLink(final Boolean withLayoutLink) {
            this.withLayoutLink = withLayoutLink;
            return this;
        }

        /**
         * Sets the new value of "withDetailLink" (any previous value will be replaced)
         * 
         * @param withDetailLink
         *     New value of the "withDetailLink" property.
         */
        public QueryContext.Builder<_B> withWithDetailLink(final Boolean withDetailLink) {
            this.withDetailLink = withDetailLink;
            return this;
        }

        @Override
        public QueryContext build() {
            if (_storedValue == null) {
                return this.init(new QueryContext());
            } else {
                return ((QueryContext) _storedValue);
            }
        }

        public QueryContext.Builder<_B> copyOf(final QueryContext _other) {
            _other.copyTo(this);
            return this;
        }

        public QueryContext.Builder<_B> copyOf(final QueryContext.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends QueryContext.Selector<QueryContext.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static QueryContext.Select _root() {
            return new QueryContext.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> offset = null;
        private com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> chunkSize = null;
        private com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> countTotal = null;
        private com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> search = null;
        private com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> searchFilterId = null;
        private com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> vlpId = null;
        private com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> where = null;
        private com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> attributes = null;
        private com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> idOnlySelection = null;
        private com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> orderBy = null;
        private com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> skipStatesAndGenerations = null;
        private com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> withTitleAndInfo = null;
        private com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> withMetaLink = null;
        private com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> withLayoutLink = null;
        private com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> withDetailLink = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.offset!= null) {
                products.put("offset", this.offset.init());
            }
            if (this.chunkSize!= null) {
                products.put("chunkSize", this.chunkSize.init());
            }
            if (this.countTotal!= null) {
                products.put("countTotal", this.countTotal.init());
            }
            if (this.search!= null) {
                products.put("search", this.search.init());
            }
            if (this.searchFilterId!= null) {
                products.put("searchFilterId", this.searchFilterId.init());
            }
            if (this.vlpId!= null) {
                products.put("vlpId", this.vlpId.init());
            }
            if (this.where!= null) {
                products.put("where", this.where.init());
            }
            if (this.attributes!= null) {
                products.put("attributes", this.attributes.init());
            }
            if (this.idOnlySelection!= null) {
                products.put("idOnlySelection", this.idOnlySelection.init());
            }
            if (this.orderBy!= null) {
                products.put("orderBy", this.orderBy.init());
            }
            if (this.skipStatesAndGenerations!= null) {
                products.put("skipStatesAndGenerations", this.skipStatesAndGenerations.init());
            }
            if (this.withTitleAndInfo!= null) {
                products.put("withTitleAndInfo", this.withTitleAndInfo.init());
            }
            if (this.withMetaLink!= null) {
                products.put("withMetaLink", this.withMetaLink.init());
            }
            if (this.withLayoutLink!= null) {
                products.put("withLayoutLink", this.withLayoutLink.init());
            }
            if (this.withDetailLink!= null) {
                products.put("withDetailLink", this.withDetailLink.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> offset() {
            return ((this.offset == null)?this.offset = new com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>>(this._root, this, "offset"):this.offset);
        }

        public com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> chunkSize() {
            return ((this.chunkSize == null)?this.chunkSize = new com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>>(this._root, this, "chunkSize"):this.chunkSize);
        }

        public com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> countTotal() {
            return ((this.countTotal == null)?this.countTotal = new com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>>(this._root, this, "countTotal"):this.countTotal);
        }

        public com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> search() {
            return ((this.search == null)?this.search = new com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>>(this._root, this, "search"):this.search);
        }

        public com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> searchFilterId() {
            return ((this.searchFilterId == null)?this.searchFilterId = new com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>>(this._root, this, "searchFilterId"):this.searchFilterId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> vlpId() {
            return ((this.vlpId == null)?this.vlpId = new com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>>(this._root, this, "vlpId"):this.vlpId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> where() {
            return ((this.where == null)?this.where = new com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>>(this._root, this, "where"):this.where);
        }

        public com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> attributes() {
            return ((this.attributes == null)?this.attributes = new com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>>(this._root, this, "attributes"):this.attributes);
        }

        public com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> idOnlySelection() {
            return ((this.idOnlySelection == null)?this.idOnlySelection = new com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>>(this._root, this, "idOnlySelection"):this.idOnlySelection);
        }

        public com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> orderBy() {
            return ((this.orderBy == null)?this.orderBy = new com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>>(this._root, this, "orderBy"):this.orderBy);
        }

        public com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> skipStatesAndGenerations() {
            return ((this.skipStatesAndGenerations == null)?this.skipStatesAndGenerations = new com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>>(this._root, this, "skipStatesAndGenerations"):this.skipStatesAndGenerations);
        }

        public com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> withTitleAndInfo() {
            return ((this.withTitleAndInfo == null)?this.withTitleAndInfo = new com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>>(this._root, this, "withTitleAndInfo"):this.withTitleAndInfo);
        }

        public com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> withMetaLink() {
            return ((this.withMetaLink == null)?this.withMetaLink = new com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>>(this._root, this, "withMetaLink"):this.withMetaLink);
        }

        public com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> withLayoutLink() {
            return ((this.withLayoutLink == null)?this.withLayoutLink = new com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>>(this._root, this, "withLayoutLink"):this.withLayoutLink);
        }

        public com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>> withDetailLink() {
            return ((this.withDetailLink == null)?this.withDetailLink = new com.kscs.util.jaxb.Selector<TRoot, QueryContext.Selector<TRoot, TParent>>(this._root, this, "withDetailLink"):this.withDetailLink);
        }

    }

}
