
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;


/**
 * <p>Java class for matrix-y-axis-object complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="matrix-y-axis-object"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="boId" type="{http://www.w3.org/2001/XMLSchema}long" /&gt;
 *       &lt;attribute name="evaluatedHeaderTitle" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "matrix-y-axis-object")
public class MatrixYAxisObject implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "boId")
    protected Long boId;
    @XmlAttribute(name = "evaluatedHeaderTitle")
    protected String evaluatedHeaderTitle;

    /**
     * Gets the value of the boId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getBoId() {
        return boId;
    }

    /**
     * Sets the value of the boId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setBoId(Long value) {
        this.boId = value;
    }

    /**
     * Gets the value of the evaluatedHeaderTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEvaluatedHeaderTitle() {
        return evaluatedHeaderTitle;
    }

    /**
     * Sets the value of the evaluatedHeaderTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEvaluatedHeaderTitle(String value) {
        this.evaluatedHeaderTitle = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            Long theBoId;
            theBoId = this.getBoId();
            strategy.appendField(locator, this, "boId", buffer, theBoId);
        }
        {
            String theEvaluatedHeaderTitle;
            theEvaluatedHeaderTitle = this.getEvaluatedHeaderTitle();
            strategy.appendField(locator, this, "evaluatedHeaderTitle", buffer, theEvaluatedHeaderTitle);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MatrixYAxisObject)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MatrixYAxisObject that = ((MatrixYAxisObject) object);
        {
            Long lhsBoId;
            lhsBoId = this.getBoId();
            Long rhsBoId;
            rhsBoId = that.getBoId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "boId", lhsBoId), LocatorUtils.property(thatLocator, "boId", rhsBoId), lhsBoId, rhsBoId)) {
                return false;
            }
        }
        {
            String lhsEvaluatedHeaderTitle;
            lhsEvaluatedHeaderTitle = this.getEvaluatedHeaderTitle();
            String rhsEvaluatedHeaderTitle;
            rhsEvaluatedHeaderTitle = that.getEvaluatedHeaderTitle();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "evaluatedHeaderTitle", lhsEvaluatedHeaderTitle), LocatorUtils.property(thatLocator, "evaluatedHeaderTitle", rhsEvaluatedHeaderTitle), lhsEvaluatedHeaderTitle, rhsEvaluatedHeaderTitle)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            Long theBoId;
            theBoId = this.getBoId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "boId", theBoId), currentHashCode, theBoId);
        }
        {
            String theEvaluatedHeaderTitle;
            theEvaluatedHeaderTitle = this.getEvaluatedHeaderTitle();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "evaluatedHeaderTitle", theEvaluatedHeaderTitle), currentHashCode, theEvaluatedHeaderTitle);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof MatrixYAxisObject) {
            final MatrixYAxisObject copy = ((MatrixYAxisObject) draftCopy);
            if (this.boId!= null) {
                Long sourceBoId;
                sourceBoId = this.getBoId();
                Long copyBoId = ((Long) strategy.copy(LocatorUtils.property(locator, "boId", sourceBoId), sourceBoId));
                copy.setBoId(copyBoId);
            } else {
                copy.boId = null;
            }
            if (this.evaluatedHeaderTitle!= null) {
                String sourceEvaluatedHeaderTitle;
                sourceEvaluatedHeaderTitle = this.getEvaluatedHeaderTitle();
                String copyEvaluatedHeaderTitle = ((String) strategy.copy(LocatorUtils.property(locator, "evaluatedHeaderTitle", sourceEvaluatedHeaderTitle), sourceEvaluatedHeaderTitle));
                copy.setEvaluatedHeaderTitle(copyEvaluatedHeaderTitle);
            } else {
                copy.evaluatedHeaderTitle = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MatrixYAxisObject();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final MatrixYAxisObject.Builder<_B> _other) {
        _other.boId = this.boId;
        _other.evaluatedHeaderTitle = this.evaluatedHeaderTitle;
    }

    public<_B >MatrixYAxisObject.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new MatrixYAxisObject.Builder<_B>(_parentBuilder, this, true);
    }

    public MatrixYAxisObject.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static MatrixYAxisObject.Builder<Void> builder() {
        return new MatrixYAxisObject.Builder<Void>(null, null, false);
    }

    public static<_B >MatrixYAxisObject.Builder<_B> copyOf(final MatrixYAxisObject _other) {
        final MatrixYAxisObject.Builder<_B> _newBuilder = new MatrixYAxisObject.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final MatrixYAxisObject.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree boIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boIdPropertyTree!= null):((boIdPropertyTree == null)||(!boIdPropertyTree.isLeaf())))) {
            _other.boId = this.boId;
        }
        final PropertyTree evaluatedHeaderTitlePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("evaluatedHeaderTitle"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(evaluatedHeaderTitlePropertyTree!= null):((evaluatedHeaderTitlePropertyTree == null)||(!evaluatedHeaderTitlePropertyTree.isLeaf())))) {
            _other.evaluatedHeaderTitle = this.evaluatedHeaderTitle;
        }
    }

    public<_B >MatrixYAxisObject.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new MatrixYAxisObject.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public MatrixYAxisObject.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >MatrixYAxisObject.Builder<_B> copyOf(final MatrixYAxisObject _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final MatrixYAxisObject.Builder<_B> _newBuilder = new MatrixYAxisObject.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static MatrixYAxisObject.Builder<Void> copyExcept(final MatrixYAxisObject _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static MatrixYAxisObject.Builder<Void> copyOnly(final MatrixYAxisObject _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final MatrixYAxisObject _storedValue;
        private Long boId;
        private String evaluatedHeaderTitle;

        public Builder(final _B _parentBuilder, final MatrixYAxisObject _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.boId = _other.boId;
                    this.evaluatedHeaderTitle = _other.evaluatedHeaderTitle;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final MatrixYAxisObject _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree boIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boIdPropertyTree!= null):((boIdPropertyTree == null)||(!boIdPropertyTree.isLeaf())))) {
                        this.boId = _other.boId;
                    }
                    final PropertyTree evaluatedHeaderTitlePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("evaluatedHeaderTitle"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(evaluatedHeaderTitlePropertyTree!= null):((evaluatedHeaderTitlePropertyTree == null)||(!evaluatedHeaderTitlePropertyTree.isLeaf())))) {
                        this.evaluatedHeaderTitle = _other.evaluatedHeaderTitle;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends MatrixYAxisObject >_P init(final _P _product) {
            _product.boId = this.boId;
            _product.evaluatedHeaderTitle = this.evaluatedHeaderTitle;
            return _product;
        }

        /**
         * Sets the new value of "boId" (any previous value will be replaced)
         * 
         * @param boId
         *     New value of the "boId" property.
         */
        public MatrixYAxisObject.Builder<_B> withBoId(final Long boId) {
            this.boId = boId;
            return this;
        }

        /**
         * Sets the new value of "evaluatedHeaderTitle" (any previous value will be replaced)
         * 
         * @param evaluatedHeaderTitle
         *     New value of the "evaluatedHeaderTitle" property.
         */
        public MatrixYAxisObject.Builder<_B> withEvaluatedHeaderTitle(final String evaluatedHeaderTitle) {
            this.evaluatedHeaderTitle = evaluatedHeaderTitle;
            return this;
        }

        @Override
        public MatrixYAxisObject build() {
            if (_storedValue == null) {
                return this.init(new MatrixYAxisObject());
            } else {
                return ((MatrixYAxisObject) _storedValue);
            }
        }

        public MatrixYAxisObject.Builder<_B> copyOf(final MatrixYAxisObject _other) {
            _other.copyTo(this);
            return this;
        }

        public MatrixYAxisObject.Builder<_B> copyOf(final MatrixYAxisObject.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends MatrixYAxisObject.Selector<MatrixYAxisObject.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static MatrixYAxisObject.Select _root() {
            return new MatrixYAxisObject.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, MatrixYAxisObject.Selector<TRoot, TParent>> boId = null;
        private com.kscs.util.jaxb.Selector<TRoot, MatrixYAxisObject.Selector<TRoot, TParent>> evaluatedHeaderTitle = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.boId!= null) {
                products.put("boId", this.boId.init());
            }
            if (this.evaluatedHeaderTitle!= null) {
                products.put("evaluatedHeaderTitle", this.evaluatedHeaderTitle.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, MatrixYAxisObject.Selector<TRoot, TParent>> boId() {
            return ((this.boId == null)?this.boId = new com.kscs.util.jaxb.Selector<TRoot, MatrixYAxisObject.Selector<TRoot, TParent>>(this._root, this, "boId"):this.boId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, MatrixYAxisObject.Selector<TRoot, TParent>> evaluatedHeaderTitle() {
            return ((this.evaluatedHeaderTitle == null)?this.evaluatedHeaderTitle = new com.kscs.util.jaxb.Selector<TRoot, MatrixYAxisObject.Selector<TRoot, TParent>>(this._root, this, "evaluatedHeaderTitle"):this.evaluatedHeaderTitle);
        }

    }

}
