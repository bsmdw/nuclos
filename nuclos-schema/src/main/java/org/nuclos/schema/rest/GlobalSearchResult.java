
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;


/**
 * <p>Java class for global-search-result complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="global-search-result"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="pk" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="uid" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="text" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "global-search-result")
public class GlobalSearchResult implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "pk")
    protected String pk;
    @XmlAttribute(name = "uid")
    protected String uid;
    @XmlAttribute(name = "text")
    protected String text;

    /**
     * Gets the value of the pk property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPk() {
        return pk;
    }

    /**
     * Sets the value of the pk property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPk(String value) {
        this.pk = value;
    }

    /**
     * Gets the value of the uid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUid() {
        return uid;
    }

    /**
     * Sets the value of the uid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUid(String value) {
        this.uid = value;
    }

    /**
     * Gets the value of the text property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getText() {
        return text;
    }

    /**
     * Sets the value of the text property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setText(String value) {
        this.text = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String thePk;
            thePk = this.getPk();
            strategy.appendField(locator, this, "pk", buffer, thePk);
        }
        {
            String theUid;
            theUid = this.getUid();
            strategy.appendField(locator, this, "uid", buffer, theUid);
        }
        {
            String theText;
            theText = this.getText();
            strategy.appendField(locator, this, "text", buffer, theText);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof GlobalSearchResult)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final GlobalSearchResult that = ((GlobalSearchResult) object);
        {
            String lhsPk;
            lhsPk = this.getPk();
            String rhsPk;
            rhsPk = that.getPk();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "pk", lhsPk), LocatorUtils.property(thatLocator, "pk", rhsPk), lhsPk, rhsPk)) {
                return false;
            }
        }
        {
            String lhsUid;
            lhsUid = this.getUid();
            String rhsUid;
            rhsUid = that.getUid();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "uid", lhsUid), LocatorUtils.property(thatLocator, "uid", rhsUid), lhsUid, rhsUid)) {
                return false;
            }
        }
        {
            String lhsText;
            lhsText = this.getText();
            String rhsText;
            rhsText = that.getText();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "text", lhsText), LocatorUtils.property(thatLocator, "text", rhsText), lhsText, rhsText)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String thePk;
            thePk = this.getPk();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "pk", thePk), currentHashCode, thePk);
        }
        {
            String theUid;
            theUid = this.getUid();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "uid", theUid), currentHashCode, theUid);
        }
        {
            String theText;
            theText = this.getText();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "text", theText), currentHashCode, theText);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof GlobalSearchResult) {
            final GlobalSearchResult copy = ((GlobalSearchResult) draftCopy);
            if (this.pk!= null) {
                String sourcePk;
                sourcePk = this.getPk();
                String copyPk = ((String) strategy.copy(LocatorUtils.property(locator, "pk", sourcePk), sourcePk));
                copy.setPk(copyPk);
            } else {
                copy.pk = null;
            }
            if (this.uid!= null) {
                String sourceUid;
                sourceUid = this.getUid();
                String copyUid = ((String) strategy.copy(LocatorUtils.property(locator, "uid", sourceUid), sourceUid));
                copy.setUid(copyUid);
            } else {
                copy.uid = null;
            }
            if (this.text!= null) {
                String sourceText;
                sourceText = this.getText();
                String copyText = ((String) strategy.copy(LocatorUtils.property(locator, "text", sourceText), sourceText));
                copy.setText(copyText);
            } else {
                copy.text = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new GlobalSearchResult();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final GlobalSearchResult.Builder<_B> _other) {
        _other.pk = this.pk;
        _other.uid = this.uid;
        _other.text = this.text;
    }

    public<_B >GlobalSearchResult.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new GlobalSearchResult.Builder<_B>(_parentBuilder, this, true);
    }

    public GlobalSearchResult.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static GlobalSearchResult.Builder<Void> builder() {
        return new GlobalSearchResult.Builder<Void>(null, null, false);
    }

    public static<_B >GlobalSearchResult.Builder<_B> copyOf(final GlobalSearchResult _other) {
        final GlobalSearchResult.Builder<_B> _newBuilder = new GlobalSearchResult.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final GlobalSearchResult.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree pkPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("pk"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(pkPropertyTree!= null):((pkPropertyTree == null)||(!pkPropertyTree.isLeaf())))) {
            _other.pk = this.pk;
        }
        final PropertyTree uidPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("uid"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(uidPropertyTree!= null):((uidPropertyTree == null)||(!uidPropertyTree.isLeaf())))) {
            _other.uid = this.uid;
        }
        final PropertyTree textPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("text"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(textPropertyTree!= null):((textPropertyTree == null)||(!textPropertyTree.isLeaf())))) {
            _other.text = this.text;
        }
    }

    public<_B >GlobalSearchResult.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new GlobalSearchResult.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public GlobalSearchResult.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >GlobalSearchResult.Builder<_B> copyOf(final GlobalSearchResult _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final GlobalSearchResult.Builder<_B> _newBuilder = new GlobalSearchResult.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static GlobalSearchResult.Builder<Void> copyExcept(final GlobalSearchResult _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static GlobalSearchResult.Builder<Void> copyOnly(final GlobalSearchResult _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final GlobalSearchResult _storedValue;
        private String pk;
        private String uid;
        private String text;

        public Builder(final _B _parentBuilder, final GlobalSearchResult _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.pk = _other.pk;
                    this.uid = _other.uid;
                    this.text = _other.text;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final GlobalSearchResult _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree pkPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("pk"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(pkPropertyTree!= null):((pkPropertyTree == null)||(!pkPropertyTree.isLeaf())))) {
                        this.pk = _other.pk;
                    }
                    final PropertyTree uidPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("uid"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(uidPropertyTree!= null):((uidPropertyTree == null)||(!uidPropertyTree.isLeaf())))) {
                        this.uid = _other.uid;
                    }
                    final PropertyTree textPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("text"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(textPropertyTree!= null):((textPropertyTree == null)||(!textPropertyTree.isLeaf())))) {
                        this.text = _other.text;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends GlobalSearchResult >_P init(final _P _product) {
            _product.pk = this.pk;
            _product.uid = this.uid;
            _product.text = this.text;
            return _product;
        }

        /**
         * Sets the new value of "pk" (any previous value will be replaced)
         * 
         * @param pk
         *     New value of the "pk" property.
         */
        public GlobalSearchResult.Builder<_B> withPk(final String pk) {
            this.pk = pk;
            return this;
        }

        /**
         * Sets the new value of "uid" (any previous value will be replaced)
         * 
         * @param uid
         *     New value of the "uid" property.
         */
        public GlobalSearchResult.Builder<_B> withUid(final String uid) {
            this.uid = uid;
            return this;
        }

        /**
         * Sets the new value of "text" (any previous value will be replaced)
         * 
         * @param text
         *     New value of the "text" property.
         */
        public GlobalSearchResult.Builder<_B> withText(final String text) {
            this.text = text;
            return this;
        }

        @Override
        public GlobalSearchResult build() {
            if (_storedValue == null) {
                return this.init(new GlobalSearchResult());
            } else {
                return ((GlobalSearchResult) _storedValue);
            }
        }

        public GlobalSearchResult.Builder<_B> copyOf(final GlobalSearchResult _other) {
            _other.copyTo(this);
            return this;
        }

        public GlobalSearchResult.Builder<_B> copyOf(final GlobalSearchResult.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends GlobalSearchResult.Selector<GlobalSearchResult.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static GlobalSearchResult.Select _root() {
            return new GlobalSearchResult.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, GlobalSearchResult.Selector<TRoot, TParent>> pk = null;
        private com.kscs.util.jaxb.Selector<TRoot, GlobalSearchResult.Selector<TRoot, TParent>> uid = null;
        private com.kscs.util.jaxb.Selector<TRoot, GlobalSearchResult.Selector<TRoot, TParent>> text = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.pk!= null) {
                products.put("pk", this.pk.init());
            }
            if (this.uid!= null) {
                products.put("uid", this.uid.init());
            }
            if (this.text!= null) {
                products.put("text", this.text.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, GlobalSearchResult.Selector<TRoot, TParent>> pk() {
            return ((this.pk == null)?this.pk = new com.kscs.util.jaxb.Selector<TRoot, GlobalSearchResult.Selector<TRoot, TParent>>(this._root, this, "pk"):this.pk);
        }

        public com.kscs.util.jaxb.Selector<TRoot, GlobalSearchResult.Selector<TRoot, TParent>> uid() {
            return ((this.uid == null)?this.uid = new com.kscs.util.jaxb.Selector<TRoot, GlobalSearchResult.Selector<TRoot, TParent>>(this._root, this, "uid"):this.uid);
        }

        public com.kscs.util.jaxb.Selector<TRoot, GlobalSearchResult.Selector<TRoot, TParent>> text() {
            return ((this.text == null)?this.text = new com.kscs.util.jaxb.Selector<TRoot, GlobalSearchResult.Selector<TRoot, TParent>>(this._root, this, "text"):this.text);
        }

    }

}
