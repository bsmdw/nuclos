
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;


/**
 * <p>Java class for matrix-x-axis-object complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="matrix-x-axis-object"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="boId" type="{http://www.w3.org/2001/XMLSchema}long" /&gt;
 *       &lt;attribute name="_title" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "matrix-x-axis-object")
public class MatrixXAxisObject implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "boId")
    protected Long boId;
    @XmlAttribute(name = "_title")
    protected String title;

    /**
     * Gets the value of the boId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getBoId() {
        return boId;
    }

    /**
     * Sets the value of the boId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setBoId(Long value) {
        this.boId = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            Long theBoId;
            theBoId = this.getBoId();
            strategy.appendField(locator, this, "boId", buffer, theBoId);
        }
        {
            String theTitle;
            theTitle = this.getTitle();
            strategy.appendField(locator, this, "title", buffer, theTitle);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MatrixXAxisObject)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MatrixXAxisObject that = ((MatrixXAxisObject) object);
        {
            Long lhsBoId;
            lhsBoId = this.getBoId();
            Long rhsBoId;
            rhsBoId = that.getBoId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "boId", lhsBoId), LocatorUtils.property(thatLocator, "boId", rhsBoId), lhsBoId, rhsBoId)) {
                return false;
            }
        }
        {
            String lhsTitle;
            lhsTitle = this.getTitle();
            String rhsTitle;
            rhsTitle = that.getTitle();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "title", lhsTitle), LocatorUtils.property(thatLocator, "title", rhsTitle), lhsTitle, rhsTitle)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            Long theBoId;
            theBoId = this.getBoId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "boId", theBoId), currentHashCode, theBoId);
        }
        {
            String theTitle;
            theTitle = this.getTitle();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "title", theTitle), currentHashCode, theTitle);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof MatrixXAxisObject) {
            final MatrixXAxisObject copy = ((MatrixXAxisObject) draftCopy);
            if (this.boId!= null) {
                Long sourceBoId;
                sourceBoId = this.getBoId();
                Long copyBoId = ((Long) strategy.copy(LocatorUtils.property(locator, "boId", sourceBoId), sourceBoId));
                copy.setBoId(copyBoId);
            } else {
                copy.boId = null;
            }
            if (this.title!= null) {
                String sourceTitle;
                sourceTitle = this.getTitle();
                String copyTitle = ((String) strategy.copy(LocatorUtils.property(locator, "title", sourceTitle), sourceTitle));
                copy.setTitle(copyTitle);
            } else {
                copy.title = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MatrixXAxisObject();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final MatrixXAxisObject.Builder<_B> _other) {
        _other.boId = this.boId;
        _other.title = this.title;
    }

    public<_B >MatrixXAxisObject.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new MatrixXAxisObject.Builder<_B>(_parentBuilder, this, true);
    }

    public MatrixXAxisObject.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static MatrixXAxisObject.Builder<Void> builder() {
        return new MatrixXAxisObject.Builder<Void>(null, null, false);
    }

    public static<_B >MatrixXAxisObject.Builder<_B> copyOf(final MatrixXAxisObject _other) {
        final MatrixXAxisObject.Builder<_B> _newBuilder = new MatrixXAxisObject.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final MatrixXAxisObject.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree boIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boIdPropertyTree!= null):((boIdPropertyTree == null)||(!boIdPropertyTree.isLeaf())))) {
            _other.boId = this.boId;
        }
        final PropertyTree titlePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("title"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(titlePropertyTree!= null):((titlePropertyTree == null)||(!titlePropertyTree.isLeaf())))) {
            _other.title = this.title;
        }
    }

    public<_B >MatrixXAxisObject.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new MatrixXAxisObject.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public MatrixXAxisObject.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >MatrixXAxisObject.Builder<_B> copyOf(final MatrixXAxisObject _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final MatrixXAxisObject.Builder<_B> _newBuilder = new MatrixXAxisObject.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static MatrixXAxisObject.Builder<Void> copyExcept(final MatrixXAxisObject _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static MatrixXAxisObject.Builder<Void> copyOnly(final MatrixXAxisObject _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final MatrixXAxisObject _storedValue;
        private Long boId;
        private String title;

        public Builder(final _B _parentBuilder, final MatrixXAxisObject _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.boId = _other.boId;
                    this.title = _other.title;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final MatrixXAxisObject _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree boIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boIdPropertyTree!= null):((boIdPropertyTree == null)||(!boIdPropertyTree.isLeaf())))) {
                        this.boId = _other.boId;
                    }
                    final PropertyTree titlePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("title"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(titlePropertyTree!= null):((titlePropertyTree == null)||(!titlePropertyTree.isLeaf())))) {
                        this.title = _other.title;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends MatrixXAxisObject >_P init(final _P _product) {
            _product.boId = this.boId;
            _product.title = this.title;
            return _product;
        }

        /**
         * Sets the new value of "boId" (any previous value will be replaced)
         * 
         * @param boId
         *     New value of the "boId" property.
         */
        public MatrixXAxisObject.Builder<_B> withBoId(final Long boId) {
            this.boId = boId;
            return this;
        }

        /**
         * Sets the new value of "title" (any previous value will be replaced)
         * 
         * @param title
         *     New value of the "title" property.
         */
        public MatrixXAxisObject.Builder<_B> withTitle(final String title) {
            this.title = title;
            return this;
        }

        @Override
        public MatrixXAxisObject build() {
            if (_storedValue == null) {
                return this.init(new MatrixXAxisObject());
            } else {
                return ((MatrixXAxisObject) _storedValue);
            }
        }

        public MatrixXAxisObject.Builder<_B> copyOf(final MatrixXAxisObject _other) {
            _other.copyTo(this);
            return this;
        }

        public MatrixXAxisObject.Builder<_B> copyOf(final MatrixXAxisObject.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends MatrixXAxisObject.Selector<MatrixXAxisObject.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static MatrixXAxisObject.Select _root() {
            return new MatrixXAxisObject.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, MatrixXAxisObject.Selector<TRoot, TParent>> boId = null;
        private com.kscs.util.jaxb.Selector<TRoot, MatrixXAxisObject.Selector<TRoot, TParent>> title = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.boId!= null) {
                products.put("boId", this.boId.init());
            }
            if (this.title!= null) {
                products.put("title", this.title.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, MatrixXAxisObject.Selector<TRoot, TParent>> boId() {
            return ((this.boId == null)?this.boId = new com.kscs.util.jaxb.Selector<TRoot, MatrixXAxisObject.Selector<TRoot, TParent>>(this._root, this, "boId"):this.boId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, MatrixXAxisObject.Selector<TRoot, TParent>> title() {
            return ((this.title == null)?this.title = new com.kscs.util.jaxb.Selector<TRoot, MatrixXAxisObject.Selector<TRoot, TParent>>(this._root, this, "title"):this.title);
        }

    }

}
