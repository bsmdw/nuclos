
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;


/**
 * <p>Java class for menu-entry complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="menu-entry"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="entries" type="{urn:org.nuclos.schema.rest}entity-meta-base" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="path" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "menu-entry", propOrder = {
    "entries"
})
public class MenuEntry implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected List<EntityMetaBase> entries;
    @XmlAttribute(name = "path")
    protected String path;

    /**
     * Gets the value of the entries property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the entries property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEntries().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EntityMetaBase }
     * 
     * 
     */
    public List<EntityMetaBase> getEntries() {
        if (entries == null) {
            entries = new ArrayList<EntityMetaBase>();
        }
        return this.entries;
    }

    /**
     * Gets the value of the path property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPath() {
        return path;
    }

    /**
     * Sets the value of the path property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPath(String value) {
        this.path = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<EntityMetaBase> theEntries;
            theEntries = (((this.entries!= null)&&(!this.entries.isEmpty()))?this.getEntries():null);
            strategy.appendField(locator, this, "entries", buffer, theEntries);
        }
        {
            String thePath;
            thePath = this.getPath();
            strategy.appendField(locator, this, "path", buffer, thePath);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MenuEntry)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MenuEntry that = ((MenuEntry) object);
        {
            List<EntityMetaBase> lhsEntries;
            lhsEntries = (((this.entries!= null)&&(!this.entries.isEmpty()))?this.getEntries():null);
            List<EntityMetaBase> rhsEntries;
            rhsEntries = (((that.entries!= null)&&(!that.entries.isEmpty()))?that.getEntries():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entries", lhsEntries), LocatorUtils.property(thatLocator, "entries", rhsEntries), lhsEntries, rhsEntries)) {
                return false;
            }
        }
        {
            String lhsPath;
            lhsPath = this.getPath();
            String rhsPath;
            rhsPath = that.getPath();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "path", lhsPath), LocatorUtils.property(thatLocator, "path", rhsPath), lhsPath, rhsPath)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<EntityMetaBase> theEntries;
            theEntries = (((this.entries!= null)&&(!this.entries.isEmpty()))?this.getEntries():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entries", theEntries), currentHashCode, theEntries);
        }
        {
            String thePath;
            thePath = this.getPath();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "path", thePath), currentHashCode, thePath);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof MenuEntry) {
            final MenuEntry copy = ((MenuEntry) draftCopy);
            if ((this.entries!= null)&&(!this.entries.isEmpty())) {
                List<EntityMetaBase> sourceEntries;
                sourceEntries = (((this.entries!= null)&&(!this.entries.isEmpty()))?this.getEntries():null);
                @SuppressWarnings("unchecked")
                List<EntityMetaBase> copyEntries = ((List<EntityMetaBase> ) strategy.copy(LocatorUtils.property(locator, "entries", sourceEntries), sourceEntries));
                copy.entries = null;
                if (copyEntries!= null) {
                    List<EntityMetaBase> uniqueEntriesl = copy.getEntries();
                    uniqueEntriesl.addAll(copyEntries);
                }
            } else {
                copy.entries = null;
            }
            if (this.path!= null) {
                String sourcePath;
                sourcePath = this.getPath();
                String copyPath = ((String) strategy.copy(LocatorUtils.property(locator, "path", sourcePath), sourcePath));
                copy.setPath(copyPath);
            } else {
                copy.path = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MenuEntry();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final MenuEntry.Builder<_B> _other) {
        if (this.entries == null) {
            _other.entries = null;
        } else {
            _other.entries = new ArrayList<EntityMetaBase.Builder<MenuEntry.Builder<_B>>>();
            for (EntityMetaBase _item: this.entries) {
                _other.entries.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.path = this.path;
    }

    public<_B >MenuEntry.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new MenuEntry.Builder<_B>(_parentBuilder, this, true);
    }

    public MenuEntry.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static MenuEntry.Builder<Void> builder() {
        return new MenuEntry.Builder<Void>(null, null, false);
    }

    public static<_B >MenuEntry.Builder<_B> copyOf(final MenuEntry _other) {
        final MenuEntry.Builder<_B> _newBuilder = new MenuEntry.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final MenuEntry.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree entriesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entries"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entriesPropertyTree!= null):((entriesPropertyTree == null)||(!entriesPropertyTree.isLeaf())))) {
            if (this.entries == null) {
                _other.entries = null;
            } else {
                _other.entries = new ArrayList<EntityMetaBase.Builder<MenuEntry.Builder<_B>>>();
                for (EntityMetaBase _item: this.entries) {
                    _other.entries.add(((_item == null)?null:_item.newCopyBuilder(_other, entriesPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree pathPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("path"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(pathPropertyTree!= null):((pathPropertyTree == null)||(!pathPropertyTree.isLeaf())))) {
            _other.path = this.path;
        }
    }

    public<_B >MenuEntry.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new MenuEntry.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public MenuEntry.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >MenuEntry.Builder<_B> copyOf(final MenuEntry _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final MenuEntry.Builder<_B> _newBuilder = new MenuEntry.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static MenuEntry.Builder<Void> copyExcept(final MenuEntry _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static MenuEntry.Builder<Void> copyOnly(final MenuEntry _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final MenuEntry _storedValue;
        private List<EntityMetaBase.Builder<MenuEntry.Builder<_B>>> entries;
        private String path;

        public Builder(final _B _parentBuilder, final MenuEntry _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.entries == null) {
                        this.entries = null;
                    } else {
                        this.entries = new ArrayList<EntityMetaBase.Builder<MenuEntry.Builder<_B>>>();
                        for (EntityMetaBase _item: _other.entries) {
                            this.entries.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.path = _other.path;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final MenuEntry _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree entriesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entries"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entriesPropertyTree!= null):((entriesPropertyTree == null)||(!entriesPropertyTree.isLeaf())))) {
                        if (_other.entries == null) {
                            this.entries = null;
                        } else {
                            this.entries = new ArrayList<EntityMetaBase.Builder<MenuEntry.Builder<_B>>>();
                            for (EntityMetaBase _item: _other.entries) {
                                this.entries.add(((_item == null)?null:_item.newCopyBuilder(this, entriesPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree pathPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("path"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(pathPropertyTree!= null):((pathPropertyTree == null)||(!pathPropertyTree.isLeaf())))) {
                        this.path = _other.path;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends MenuEntry >_P init(final _P _product) {
            if (this.entries!= null) {
                final List<EntityMetaBase> entries = new ArrayList<EntityMetaBase>(this.entries.size());
                for (EntityMetaBase.Builder<MenuEntry.Builder<_B>> _item: this.entries) {
                    entries.add(_item.build());
                }
                _product.entries = entries;
            }
            _product.path = this.path;
            return _product;
        }

        /**
         * Adds the given items to the value of "entries"
         * 
         * @param entries
         *     Items to add to the value of the "entries" property
         */
        public MenuEntry.Builder<_B> addEntries(final Iterable<? extends EntityMetaBase> entries) {
            if (entries!= null) {
                if (this.entries == null) {
                    this.entries = new ArrayList<EntityMetaBase.Builder<MenuEntry.Builder<_B>>>();
                }
                for (EntityMetaBase _item: entries) {
                    this.entries.add(new EntityMetaBase.Builder<MenuEntry.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "entries" (any previous value will be replaced)
         * 
         * @param entries
         *     New value of the "entries" property.
         */
        public MenuEntry.Builder<_B> withEntries(final Iterable<? extends EntityMetaBase> entries) {
            if (this.entries!= null) {
                this.entries.clear();
            }
            return addEntries(entries);
        }

        /**
         * Adds the given items to the value of "entries"
         * 
         * @param entries
         *     Items to add to the value of the "entries" property
         */
        public MenuEntry.Builder<_B> addEntries(EntityMetaBase... entries) {
            addEntries(Arrays.asList(entries));
            return this;
        }

        /**
         * Sets the new value of "entries" (any previous value will be replaced)
         * 
         * @param entries
         *     New value of the "entries" property.
         */
        public MenuEntry.Builder<_B> withEntries(EntityMetaBase... entries) {
            withEntries(Arrays.asList(entries));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Entries" property.
         * Use {@link org.nuclos.schema.rest.EntityMetaBase.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Entries" property.
         *     Use {@link org.nuclos.schema.rest.EntityMetaBase.Builder#end()} to return to the current builder.
         */
        public EntityMetaBase.Builder<? extends MenuEntry.Builder<_B>> addEntries() {
            if (this.entries == null) {
                this.entries = new ArrayList<EntityMetaBase.Builder<MenuEntry.Builder<_B>>>();
            }
            final EntityMetaBase.Builder<MenuEntry.Builder<_B>> entries_Builder = new EntityMetaBase.Builder<MenuEntry.Builder<_B>>(this, null, false);
            this.entries.add(entries_Builder);
            return entries_Builder;
        }

        /**
         * Sets the new value of "path" (any previous value will be replaced)
         * 
         * @param path
         *     New value of the "path" property.
         */
        public MenuEntry.Builder<_B> withPath(final String path) {
            this.path = path;
            return this;
        }

        @Override
        public MenuEntry build() {
            if (_storedValue == null) {
                return this.init(new MenuEntry());
            } else {
                return ((MenuEntry) _storedValue);
            }
        }

        public MenuEntry.Builder<_B> copyOf(final MenuEntry _other) {
            _other.copyTo(this);
            return this;
        }

        public MenuEntry.Builder<_B> copyOf(final MenuEntry.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends MenuEntry.Selector<MenuEntry.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static MenuEntry.Select _root() {
            return new MenuEntry.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private EntityMetaBase.Selector<TRoot, MenuEntry.Selector<TRoot, TParent>> entries = null;
        private com.kscs.util.jaxb.Selector<TRoot, MenuEntry.Selector<TRoot, TParent>> path = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.entries!= null) {
                products.put("entries", this.entries.init());
            }
            if (this.path!= null) {
                products.put("path", this.path.init());
            }
            return products;
        }

        public EntityMetaBase.Selector<TRoot, MenuEntry.Selector<TRoot, TParent>> entries() {
            return ((this.entries == null)?this.entries = new EntityMetaBase.Selector<TRoot, MenuEntry.Selector<TRoot, TParent>>(this._root, this, "entries"):this.entries);
        }

        public com.kscs.util.jaxb.Selector<TRoot, MenuEntry.Selector<TRoot, TParent>> path() {
            return ((this.path == null)?this.path = new com.kscs.util.jaxb.Selector<TRoot, MenuEntry.Selector<TRoot, TParent>>(this._root, this, "path"):this.path);
        }

    }

}
