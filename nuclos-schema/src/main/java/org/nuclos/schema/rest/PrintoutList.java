
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;


/**
 * <p>Java class for printout-list complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="printout-list"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="outputFormats" type="{urn:org.nuclos.schema.rest}printout-output-format" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="printoutId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "printout-list", propOrder = {
    "outputFormats"
})
public class PrintoutList implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected List<PrintoutOutputFormat> outputFormats;
    @XmlAttribute(name = "name")
    protected String name;
    @XmlAttribute(name = "printoutId")
    protected String printoutId;

    /**
     * Gets the value of the outputFormats property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the outputFormats property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOutputFormats().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PrintoutOutputFormat }
     * 
     * 
     */
    public List<PrintoutOutputFormat> getOutputFormats() {
        if (outputFormats == null) {
            outputFormats = new ArrayList<PrintoutOutputFormat>();
        }
        return this.outputFormats;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the printoutId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrintoutId() {
        return printoutId;
    }

    /**
     * Sets the value of the printoutId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrintoutId(String value) {
        this.printoutId = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<PrintoutOutputFormat> theOutputFormats;
            theOutputFormats = (((this.outputFormats!= null)&&(!this.outputFormats.isEmpty()))?this.getOutputFormats():null);
            strategy.appendField(locator, this, "outputFormats", buffer, theOutputFormats);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String thePrintoutId;
            thePrintoutId = this.getPrintoutId();
            strategy.appendField(locator, this, "printoutId", buffer, thePrintoutId);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof PrintoutList)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final PrintoutList that = ((PrintoutList) object);
        {
            List<PrintoutOutputFormat> lhsOutputFormats;
            lhsOutputFormats = (((this.outputFormats!= null)&&(!this.outputFormats.isEmpty()))?this.getOutputFormats():null);
            List<PrintoutOutputFormat> rhsOutputFormats;
            rhsOutputFormats = (((that.outputFormats!= null)&&(!that.outputFormats.isEmpty()))?that.getOutputFormats():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "outputFormats", lhsOutputFormats), LocatorUtils.property(thatLocator, "outputFormats", rhsOutputFormats), lhsOutputFormats, rhsOutputFormats)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsPrintoutId;
            lhsPrintoutId = this.getPrintoutId();
            String rhsPrintoutId;
            rhsPrintoutId = that.getPrintoutId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "printoutId", lhsPrintoutId), LocatorUtils.property(thatLocator, "printoutId", rhsPrintoutId), lhsPrintoutId, rhsPrintoutId)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<PrintoutOutputFormat> theOutputFormats;
            theOutputFormats = (((this.outputFormats!= null)&&(!this.outputFormats.isEmpty()))?this.getOutputFormats():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "outputFormats", theOutputFormats), currentHashCode, theOutputFormats);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String thePrintoutId;
            thePrintoutId = this.getPrintoutId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "printoutId", thePrintoutId), currentHashCode, thePrintoutId);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof PrintoutList) {
            final PrintoutList copy = ((PrintoutList) draftCopy);
            if ((this.outputFormats!= null)&&(!this.outputFormats.isEmpty())) {
                List<PrintoutOutputFormat> sourceOutputFormats;
                sourceOutputFormats = (((this.outputFormats!= null)&&(!this.outputFormats.isEmpty()))?this.getOutputFormats():null);
                @SuppressWarnings("unchecked")
                List<PrintoutOutputFormat> copyOutputFormats = ((List<PrintoutOutputFormat> ) strategy.copy(LocatorUtils.property(locator, "outputFormats", sourceOutputFormats), sourceOutputFormats));
                copy.outputFormats = null;
                if (copyOutputFormats!= null) {
                    List<PrintoutOutputFormat> uniqueOutputFormatsl = copy.getOutputFormats();
                    uniqueOutputFormatsl.addAll(copyOutputFormats);
                }
            } else {
                copy.outputFormats = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.printoutId!= null) {
                String sourcePrintoutId;
                sourcePrintoutId = this.getPrintoutId();
                String copyPrintoutId = ((String) strategy.copy(LocatorUtils.property(locator, "printoutId", sourcePrintoutId), sourcePrintoutId));
                copy.setPrintoutId(copyPrintoutId);
            } else {
                copy.printoutId = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new PrintoutList();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final PrintoutList.Builder<_B> _other) {
        if (this.outputFormats == null) {
            _other.outputFormats = null;
        } else {
            _other.outputFormats = new ArrayList<PrintoutOutputFormat.Builder<PrintoutList.Builder<_B>>>();
            for (PrintoutOutputFormat _item: this.outputFormats) {
                _other.outputFormats.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.name = this.name;
        _other.printoutId = this.printoutId;
    }

    public<_B >PrintoutList.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new PrintoutList.Builder<_B>(_parentBuilder, this, true);
    }

    public PrintoutList.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static PrintoutList.Builder<Void> builder() {
        return new PrintoutList.Builder<Void>(null, null, false);
    }

    public static<_B >PrintoutList.Builder<_B> copyOf(final PrintoutList _other) {
        final PrintoutList.Builder<_B> _newBuilder = new PrintoutList.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final PrintoutList.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree outputFormatsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("outputFormats"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(outputFormatsPropertyTree!= null):((outputFormatsPropertyTree == null)||(!outputFormatsPropertyTree.isLeaf())))) {
            if (this.outputFormats == null) {
                _other.outputFormats = null;
            } else {
                _other.outputFormats = new ArrayList<PrintoutOutputFormat.Builder<PrintoutList.Builder<_B>>>();
                for (PrintoutOutputFormat _item: this.outputFormats) {
                    _other.outputFormats.add(((_item == null)?null:_item.newCopyBuilder(_other, outputFormatsPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree printoutIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("printoutId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(printoutIdPropertyTree!= null):((printoutIdPropertyTree == null)||(!printoutIdPropertyTree.isLeaf())))) {
            _other.printoutId = this.printoutId;
        }
    }

    public<_B >PrintoutList.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new PrintoutList.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public PrintoutList.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >PrintoutList.Builder<_B> copyOf(final PrintoutList _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PrintoutList.Builder<_B> _newBuilder = new PrintoutList.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static PrintoutList.Builder<Void> copyExcept(final PrintoutList _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static PrintoutList.Builder<Void> copyOnly(final PrintoutList _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final PrintoutList _storedValue;
        private List<PrintoutOutputFormat.Builder<PrintoutList.Builder<_B>>> outputFormats;
        private String name;
        private String printoutId;

        public Builder(final _B _parentBuilder, final PrintoutList _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.outputFormats == null) {
                        this.outputFormats = null;
                    } else {
                        this.outputFormats = new ArrayList<PrintoutOutputFormat.Builder<PrintoutList.Builder<_B>>>();
                        for (PrintoutOutputFormat _item: _other.outputFormats) {
                            this.outputFormats.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.name = _other.name;
                    this.printoutId = _other.printoutId;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final PrintoutList _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree outputFormatsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("outputFormats"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(outputFormatsPropertyTree!= null):((outputFormatsPropertyTree == null)||(!outputFormatsPropertyTree.isLeaf())))) {
                        if (_other.outputFormats == null) {
                            this.outputFormats = null;
                        } else {
                            this.outputFormats = new ArrayList<PrintoutOutputFormat.Builder<PrintoutList.Builder<_B>>>();
                            for (PrintoutOutputFormat _item: _other.outputFormats) {
                                this.outputFormats.add(((_item == null)?null:_item.newCopyBuilder(this, outputFormatsPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree printoutIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("printoutId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(printoutIdPropertyTree!= null):((printoutIdPropertyTree == null)||(!printoutIdPropertyTree.isLeaf())))) {
                        this.printoutId = _other.printoutId;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends PrintoutList >_P init(final _P _product) {
            if (this.outputFormats!= null) {
                final List<PrintoutOutputFormat> outputFormats = new ArrayList<PrintoutOutputFormat>(this.outputFormats.size());
                for (PrintoutOutputFormat.Builder<PrintoutList.Builder<_B>> _item: this.outputFormats) {
                    outputFormats.add(_item.build());
                }
                _product.outputFormats = outputFormats;
            }
            _product.name = this.name;
            _product.printoutId = this.printoutId;
            return _product;
        }

        /**
         * Adds the given items to the value of "outputFormats"
         * 
         * @param outputFormats
         *     Items to add to the value of the "outputFormats" property
         */
        public PrintoutList.Builder<_B> addOutputFormats(final Iterable<? extends PrintoutOutputFormat> outputFormats) {
            if (outputFormats!= null) {
                if (this.outputFormats == null) {
                    this.outputFormats = new ArrayList<PrintoutOutputFormat.Builder<PrintoutList.Builder<_B>>>();
                }
                for (PrintoutOutputFormat _item: outputFormats) {
                    this.outputFormats.add(new PrintoutOutputFormat.Builder<PrintoutList.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "outputFormats" (any previous value will be replaced)
         * 
         * @param outputFormats
         *     New value of the "outputFormats" property.
         */
        public PrintoutList.Builder<_B> withOutputFormats(final Iterable<? extends PrintoutOutputFormat> outputFormats) {
            if (this.outputFormats!= null) {
                this.outputFormats.clear();
            }
            return addOutputFormats(outputFormats);
        }

        /**
         * Adds the given items to the value of "outputFormats"
         * 
         * @param outputFormats
         *     Items to add to the value of the "outputFormats" property
         */
        public PrintoutList.Builder<_B> addOutputFormats(PrintoutOutputFormat... outputFormats) {
            addOutputFormats(Arrays.asList(outputFormats));
            return this;
        }

        /**
         * Sets the new value of "outputFormats" (any previous value will be replaced)
         * 
         * @param outputFormats
         *     New value of the "outputFormats" property.
         */
        public PrintoutList.Builder<_B> withOutputFormats(PrintoutOutputFormat... outputFormats) {
            withOutputFormats(Arrays.asList(outputFormats));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "OutputFormats" property.
         * Use {@link org.nuclos.schema.rest.PrintoutOutputFormat.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "OutputFormats" property.
         *     Use {@link org.nuclos.schema.rest.PrintoutOutputFormat.Builder#end()} to return to the current builder.
         */
        public PrintoutOutputFormat.Builder<? extends PrintoutList.Builder<_B>> addOutputFormats() {
            if (this.outputFormats == null) {
                this.outputFormats = new ArrayList<PrintoutOutputFormat.Builder<PrintoutList.Builder<_B>>>();
            }
            final PrintoutOutputFormat.Builder<PrintoutList.Builder<_B>> outputFormats_Builder = new PrintoutOutputFormat.Builder<PrintoutList.Builder<_B>>(this, null, false);
            this.outputFormats.add(outputFormats_Builder);
            return outputFormats_Builder;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public PrintoutList.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "printoutId" (any previous value will be replaced)
         * 
         * @param printoutId
         *     New value of the "printoutId" property.
         */
        public PrintoutList.Builder<_B> withPrintoutId(final String printoutId) {
            this.printoutId = printoutId;
            return this;
        }

        @Override
        public PrintoutList build() {
            if (_storedValue == null) {
                return this.init(new PrintoutList());
            } else {
                return ((PrintoutList) _storedValue);
            }
        }

        public PrintoutList.Builder<_B> copyOf(final PrintoutList _other) {
            _other.copyTo(this);
            return this;
        }

        public PrintoutList.Builder<_B> copyOf(final PrintoutList.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends PrintoutList.Selector<PrintoutList.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static PrintoutList.Select _root() {
            return new PrintoutList.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private PrintoutOutputFormat.Selector<TRoot, PrintoutList.Selector<TRoot, TParent>> outputFormats = null;
        private com.kscs.util.jaxb.Selector<TRoot, PrintoutList.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, PrintoutList.Selector<TRoot, TParent>> printoutId = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.outputFormats!= null) {
                products.put("outputFormats", this.outputFormats.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.printoutId!= null) {
                products.put("printoutId", this.printoutId.init());
            }
            return products;
        }

        public PrintoutOutputFormat.Selector<TRoot, PrintoutList.Selector<TRoot, TParent>> outputFormats() {
            return ((this.outputFormats == null)?this.outputFormats = new PrintoutOutputFormat.Selector<TRoot, PrintoutList.Selector<TRoot, TParent>>(this._root, this, "outputFormats"):this.outputFormats);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PrintoutList.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, PrintoutList.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PrintoutList.Selector<TRoot, TParent>> printoutId() {
            return ((this.printoutId == null)?this.printoutId = new com.kscs.util.jaxb.Selector<TRoot, PrintoutList.Selector<TRoot, TParent>>(this._root, this, "printoutId"):this.printoutId);
        }

    }

}
