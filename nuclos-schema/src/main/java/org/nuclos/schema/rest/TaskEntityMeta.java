
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;


/**
 * <p>Java class for task-entity-meta complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="task-entity-meta"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:org.nuclos.schema.rest}searchfilter-entity-meta"&gt;
 *       &lt;attribute name="dynamicEntityFieldName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="taskMetaId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="taskEntity" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "task-entity-meta")
public class TaskEntityMeta
    extends SearchfilterEntityMeta
    implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "dynamicEntityFieldName")
    protected String dynamicEntityFieldName;
    @XmlAttribute(name = "taskMetaId")
    protected String taskMetaId;
    @XmlAttribute(name = "taskEntity")
    protected String taskEntity;

    /**
     * Gets the value of the dynamicEntityFieldName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDynamicEntityFieldName() {
        return dynamicEntityFieldName;
    }

    /**
     * Sets the value of the dynamicEntityFieldName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDynamicEntityFieldName(String value) {
        this.dynamicEntityFieldName = value;
    }

    /**
     * Gets the value of the taskMetaId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaskMetaId() {
        return taskMetaId;
    }

    /**
     * Sets the value of the taskMetaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaskMetaId(String value) {
        this.taskMetaId = value;
    }

    /**
     * Gets the value of the taskEntity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaskEntity() {
        return taskEntity;
    }

    /**
     * Sets the value of the taskEntity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaskEntity(String value) {
        this.taskEntity = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        super.appendFields(locator, buffer, strategy);
        {
            String theDynamicEntityFieldName;
            theDynamicEntityFieldName = this.getDynamicEntityFieldName();
            strategy.appendField(locator, this, "dynamicEntityFieldName", buffer, theDynamicEntityFieldName);
        }
        {
            String theTaskMetaId;
            theTaskMetaId = this.getTaskMetaId();
            strategy.appendField(locator, this, "taskMetaId", buffer, theTaskMetaId);
        }
        {
            String theTaskEntity;
            theTaskEntity = this.getTaskEntity();
            strategy.appendField(locator, this, "taskEntity", buffer, theTaskEntity);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof TaskEntityMeta)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final TaskEntityMeta that = ((TaskEntityMeta) object);
        {
            String lhsDynamicEntityFieldName;
            lhsDynamicEntityFieldName = this.getDynamicEntityFieldName();
            String rhsDynamicEntityFieldName;
            rhsDynamicEntityFieldName = that.getDynamicEntityFieldName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dynamicEntityFieldName", lhsDynamicEntityFieldName), LocatorUtils.property(thatLocator, "dynamicEntityFieldName", rhsDynamicEntityFieldName), lhsDynamicEntityFieldName, rhsDynamicEntityFieldName)) {
                return false;
            }
        }
        {
            String lhsTaskMetaId;
            lhsTaskMetaId = this.getTaskMetaId();
            String rhsTaskMetaId;
            rhsTaskMetaId = that.getTaskMetaId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "taskMetaId", lhsTaskMetaId), LocatorUtils.property(thatLocator, "taskMetaId", rhsTaskMetaId), lhsTaskMetaId, rhsTaskMetaId)) {
                return false;
            }
        }
        {
            String lhsTaskEntity;
            lhsTaskEntity = this.getTaskEntity();
            String rhsTaskEntity;
            rhsTaskEntity = that.getTaskEntity();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "taskEntity", lhsTaskEntity), LocatorUtils.property(thatLocator, "taskEntity", rhsTaskEntity), lhsTaskEntity, rhsTaskEntity)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = super.hashCode(locator, strategy);
        {
            String theDynamicEntityFieldName;
            theDynamicEntityFieldName = this.getDynamicEntityFieldName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dynamicEntityFieldName", theDynamicEntityFieldName), currentHashCode, theDynamicEntityFieldName);
        }
        {
            String theTaskMetaId;
            theTaskMetaId = this.getTaskMetaId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "taskMetaId", theTaskMetaId), currentHashCode, theTaskMetaId);
        }
        {
            String theTaskEntity;
            theTaskEntity = this.getTaskEntity();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "taskEntity", theTaskEntity), currentHashCode, theTaskEntity);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof TaskEntityMeta) {
            final TaskEntityMeta copy = ((TaskEntityMeta) draftCopy);
            if (this.dynamicEntityFieldName!= null) {
                String sourceDynamicEntityFieldName;
                sourceDynamicEntityFieldName = this.getDynamicEntityFieldName();
                String copyDynamicEntityFieldName = ((String) strategy.copy(LocatorUtils.property(locator, "dynamicEntityFieldName", sourceDynamicEntityFieldName), sourceDynamicEntityFieldName));
                copy.setDynamicEntityFieldName(copyDynamicEntityFieldName);
            } else {
                copy.dynamicEntityFieldName = null;
            }
            if (this.taskMetaId!= null) {
                String sourceTaskMetaId;
                sourceTaskMetaId = this.getTaskMetaId();
                String copyTaskMetaId = ((String) strategy.copy(LocatorUtils.property(locator, "taskMetaId", sourceTaskMetaId), sourceTaskMetaId));
                copy.setTaskMetaId(copyTaskMetaId);
            } else {
                copy.taskMetaId = null;
            }
            if (this.taskEntity!= null) {
                String sourceTaskEntity;
                sourceTaskEntity = this.getTaskEntity();
                String copyTaskEntity = ((String) strategy.copy(LocatorUtils.property(locator, "taskEntity", sourceTaskEntity), sourceTaskEntity));
                copy.setTaskEntity(copyTaskEntity);
            } else {
                copy.taskEntity = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new TaskEntityMeta();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final TaskEntityMeta.Builder<_B> _other) {
        super.copyTo(_other);
        _other.dynamicEntityFieldName = this.dynamicEntityFieldName;
        _other.taskMetaId = this.taskMetaId;
        _other.taskEntity = this.taskEntity;
    }

    @Override
    public<_B >TaskEntityMeta.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new TaskEntityMeta.Builder<_B>(_parentBuilder, this, true);
    }

    @Override
    public TaskEntityMeta.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static TaskEntityMeta.Builder<Void> builder() {
        return new TaskEntityMeta.Builder<Void>(null, null, false);
    }

    public static<_B >TaskEntityMeta.Builder<_B> copyOf(final EntityMetaBase _other) {
        final TaskEntityMeta.Builder<_B> _newBuilder = new TaskEntityMeta.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >TaskEntityMeta.Builder<_B> copyOf(final EntityMetaOverview _other) {
        final TaskEntityMeta.Builder<_B> _newBuilder = new TaskEntityMeta.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >TaskEntityMeta.Builder<_B> copyOf(final SearchfilterEntityMeta _other) {
        final TaskEntityMeta.Builder<_B> _newBuilder = new TaskEntityMeta.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >TaskEntityMeta.Builder<_B> copyOf(final TaskEntityMeta _other) {
        final TaskEntityMeta.Builder<_B> _newBuilder = new TaskEntityMeta.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final TaskEntityMeta.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        super.copyTo(_other, _propertyTree, _propertyTreeUse);
        final PropertyTree dynamicEntityFieldNamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dynamicEntityFieldName"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dynamicEntityFieldNamePropertyTree!= null):((dynamicEntityFieldNamePropertyTree == null)||(!dynamicEntityFieldNamePropertyTree.isLeaf())))) {
            _other.dynamicEntityFieldName = this.dynamicEntityFieldName;
        }
        final PropertyTree taskMetaIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("taskMetaId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(taskMetaIdPropertyTree!= null):((taskMetaIdPropertyTree == null)||(!taskMetaIdPropertyTree.isLeaf())))) {
            _other.taskMetaId = this.taskMetaId;
        }
        final PropertyTree taskEntityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("taskEntity"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(taskEntityPropertyTree!= null):((taskEntityPropertyTree == null)||(!taskEntityPropertyTree.isLeaf())))) {
            _other.taskEntity = this.taskEntity;
        }
    }

    @Override
    public<_B >TaskEntityMeta.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new TaskEntityMeta.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    @Override
    public TaskEntityMeta.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >TaskEntityMeta.Builder<_B> copyOf(final EntityMetaBase _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final TaskEntityMeta.Builder<_B> _newBuilder = new TaskEntityMeta.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >TaskEntityMeta.Builder<_B> copyOf(final EntityMetaOverview _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final TaskEntityMeta.Builder<_B> _newBuilder = new TaskEntityMeta.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >TaskEntityMeta.Builder<_B> copyOf(final SearchfilterEntityMeta _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final TaskEntityMeta.Builder<_B> _newBuilder = new TaskEntityMeta.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >TaskEntityMeta.Builder<_B> copyOf(final TaskEntityMeta _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final TaskEntityMeta.Builder<_B> _newBuilder = new TaskEntityMeta.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static TaskEntityMeta.Builder<Void> copyExcept(final EntityMetaBase _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static TaskEntityMeta.Builder<Void> copyExcept(final EntityMetaOverview _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static TaskEntityMeta.Builder<Void> copyExcept(final SearchfilterEntityMeta _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static TaskEntityMeta.Builder<Void> copyExcept(final TaskEntityMeta _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static TaskEntityMeta.Builder<Void> copyOnly(final EntityMetaBase _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static TaskEntityMeta.Builder<Void> copyOnly(final EntityMetaOverview _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static TaskEntityMeta.Builder<Void> copyOnly(final SearchfilterEntityMeta _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static TaskEntityMeta.Builder<Void> copyOnly(final TaskEntityMeta _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >
        extends SearchfilterEntityMeta.Builder<_B>
        implements Buildable
    {

        private String dynamicEntityFieldName;
        private String taskMetaId;
        private String taskEntity;

        public Builder(final _B _parentBuilder, final TaskEntityMeta _other, final boolean _copy) {
            super(_parentBuilder, _other, _copy);
            if (_other!= null) {
                this.dynamicEntityFieldName = _other.dynamicEntityFieldName;
                this.taskMetaId = _other.taskMetaId;
                this.taskEntity = _other.taskEntity;
            }
        }

        public Builder(final _B _parentBuilder, final TaskEntityMeta _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            super(_parentBuilder, _other, _copy, _propertyTree, _propertyTreeUse);
            if (_other!= null) {
                final PropertyTree dynamicEntityFieldNamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dynamicEntityFieldName"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dynamicEntityFieldNamePropertyTree!= null):((dynamicEntityFieldNamePropertyTree == null)||(!dynamicEntityFieldNamePropertyTree.isLeaf())))) {
                    this.dynamicEntityFieldName = _other.dynamicEntityFieldName;
                }
                final PropertyTree taskMetaIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("taskMetaId"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(taskMetaIdPropertyTree!= null):((taskMetaIdPropertyTree == null)||(!taskMetaIdPropertyTree.isLeaf())))) {
                    this.taskMetaId = _other.taskMetaId;
                }
                final PropertyTree taskEntityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("taskEntity"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(taskEntityPropertyTree!= null):((taskEntityPropertyTree == null)||(!taskEntityPropertyTree.isLeaf())))) {
                    this.taskEntity = _other.taskEntity;
                }
            }
        }

        protected<_P extends TaskEntityMeta >_P init(final _P _product) {
            _product.dynamicEntityFieldName = this.dynamicEntityFieldName;
            _product.taskMetaId = this.taskMetaId;
            _product.taskEntity = this.taskEntity;
            return super.init(_product);
        }

        /**
         * Sets the new value of "dynamicEntityFieldName" (any previous value will be replaced)
         * 
         * @param dynamicEntityFieldName
         *     New value of the "dynamicEntityFieldName" property.
         */
        public TaskEntityMeta.Builder<_B> withDynamicEntityFieldName(final String dynamicEntityFieldName) {
            this.dynamicEntityFieldName = dynamicEntityFieldName;
            return this;
        }

        /**
         * Sets the new value of "taskMetaId" (any previous value will be replaced)
         * 
         * @param taskMetaId
         *     New value of the "taskMetaId" property.
         */
        public TaskEntityMeta.Builder<_B> withTaskMetaId(final String taskMetaId) {
            this.taskMetaId = taskMetaId;
            return this;
        }

        /**
         * Sets the new value of "taskEntity" (any previous value will be replaced)
         * 
         * @param taskEntity
         *     New value of the "taskEntity" property.
         */
        public TaskEntityMeta.Builder<_B> withTaskEntity(final String taskEntity) {
            this.taskEntity = taskEntity;
            return this;
        }

        /**
         * Sets the new value of "searchfilter" (any previous value will be replaced)
         * 
         * @param searchfilter
         *     New value of the "searchfilter" property.
         */
        @Override
        public TaskEntityMeta.Builder<_B> withSearchfilter(final String searchfilter) {
            super.withSearchfilter(searchfilter);
            return this;
        }

        /**
         * Sets the new value of "processMetaId" (any previous value will be replaced)
         * 
         * @param processMetaId
         *     New value of the "processMetaId" property.
         */
        @Override
        public TaskEntityMeta.Builder<_B> withProcessMetaId(final String processMetaId) {
            super.withProcessMetaId(processMetaId);
            return this;
        }

        /**
         * Sets the new value of "links" (any previous value will be replaced)
         * 
         * @param links
         *     New value of the "links" property.
         */
        @Override
        public TaskEntityMeta.Builder<_B> withLinks(final EntityMetaBaseLinks links) {
            super.withLinks(links);
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "links" property.
         * Use {@link org.nuclos.schema.rest.EntityMetaBaseLinks.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "links" property.
         *     Use {@link org.nuclos.schema.rest.EntityMetaBaseLinks.Builder#end()} to return to the current builder.
         */
        public EntityMetaBaseLinks.Builder<? extends TaskEntityMeta.Builder<_B>> withLinks() {
            return ((EntityMetaBaseLinks.Builder<? extends TaskEntityMeta.Builder<_B>> ) super.withLinks());
        }

        /**
         * Sets the new value of "boMetaId" (any previous value will be replaced)
         * 
         * @param boMetaId
         *     New value of the "boMetaId" property.
         */
        @Override
        public TaskEntityMeta.Builder<_B> withBoMetaId(final String boMetaId) {
            super.withBoMetaId(boMetaId);
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        @Override
        public TaskEntityMeta.Builder<_B> withName(final String name) {
            super.withName(name);
            return this;
        }

        /**
         * Sets the new value of "createNew" (any previous value will be replaced)
         * 
         * @param createNew
         *     New value of the "createNew" property.
         */
        @Override
        public TaskEntityMeta.Builder<_B> withCreateNew(final boolean createNew) {
            super.withCreateNew(createNew);
            return this;
        }

        @Override
        public TaskEntityMeta build() {
            if (_storedValue == null) {
                return this.init(new TaskEntityMeta());
            } else {
                return ((TaskEntityMeta) _storedValue);
            }
        }

        public TaskEntityMeta.Builder<_B> copyOf(final TaskEntityMeta _other) {
            _other.copyTo(this);
            return this;
        }

        public TaskEntityMeta.Builder<_B> copyOf(final TaskEntityMeta.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends TaskEntityMeta.Selector<TaskEntityMeta.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static TaskEntityMeta.Select _root() {
            return new TaskEntityMeta.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends SearchfilterEntityMeta.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, TaskEntityMeta.Selector<TRoot, TParent>> dynamicEntityFieldName = null;
        private com.kscs.util.jaxb.Selector<TRoot, TaskEntityMeta.Selector<TRoot, TParent>> taskMetaId = null;
        private com.kscs.util.jaxb.Selector<TRoot, TaskEntityMeta.Selector<TRoot, TParent>> taskEntity = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.dynamicEntityFieldName!= null) {
                products.put("dynamicEntityFieldName", this.dynamicEntityFieldName.init());
            }
            if (this.taskMetaId!= null) {
                products.put("taskMetaId", this.taskMetaId.init());
            }
            if (this.taskEntity!= null) {
                products.put("taskEntity", this.taskEntity.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, TaskEntityMeta.Selector<TRoot, TParent>> dynamicEntityFieldName() {
            return ((this.dynamicEntityFieldName == null)?this.dynamicEntityFieldName = new com.kscs.util.jaxb.Selector<TRoot, TaskEntityMeta.Selector<TRoot, TParent>>(this._root, this, "dynamicEntityFieldName"):this.dynamicEntityFieldName);
        }

        public com.kscs.util.jaxb.Selector<TRoot, TaskEntityMeta.Selector<TRoot, TParent>> taskMetaId() {
            return ((this.taskMetaId == null)?this.taskMetaId = new com.kscs.util.jaxb.Selector<TRoot, TaskEntityMeta.Selector<TRoot, TParent>>(this._root, this, "taskMetaId"):this.taskMetaId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, TaskEntityMeta.Selector<TRoot, TParent>> taskEntity() {
            return ((this.taskEntity == null)?this.taskEntity = new com.kscs.util.jaxb.Selector<TRoot, TaskEntityMeta.Selector<TRoot, TParent>>(this._root, this, "taskEntity"):this.taskEntity);
        }

    }

}
