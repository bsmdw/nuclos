
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;


/**
 * <p>Java class for entity-object complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="entity-object"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="links" type="{urn:org.nuclos.schema.rest}entity-object-links"/&gt;
 *         &lt;element name="attributes" type="{urn:org.nuclos.schema.rest}entity-object-attributes"/&gt;
 *         &lt;element name="attrRestrictions" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *         &lt;element name="attrImages" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="boId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="boMetaId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="canWrite" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="canDelete" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="version" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "entity-object", propOrder = {
    "links",
    "attributes",
    "attrRestrictions",
    "attrImages"
})
public class EntityObject implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected EntityObjectLinks links;
    @XmlElement(required = true)
    protected EntityObjectAttributes attributes;
    @XmlElement(required = true)
    protected Object attrRestrictions;
    @XmlElement(required = true)
    protected Object attrImages;
    @XmlAttribute(name = "boId")
    protected String boId;
    @XmlAttribute(name = "boMetaId")
    protected String boMetaId;
    @XmlAttribute(name = "canWrite")
    protected Boolean canWrite;
    @XmlAttribute(name = "canDelete")
    protected Boolean canDelete;
    @XmlAttribute(name = "version")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger version;

    /**
     * Gets the value of the links property.
     * 
     * @return
     *     possible object is
     *     {@link EntityObjectLinks }
     *     
     */
    public EntityObjectLinks getLinks() {
        return links;
    }

    /**
     * Sets the value of the links property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntityObjectLinks }
     *     
     */
    public void setLinks(EntityObjectLinks value) {
        this.links = value;
    }

    /**
     * Gets the value of the attributes property.
     * 
     * @return
     *     possible object is
     *     {@link EntityObjectAttributes }
     *     
     */
    public EntityObjectAttributes getAttributes() {
        return attributes;
    }

    /**
     * Sets the value of the attributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntityObjectAttributes }
     *     
     */
    public void setAttributes(EntityObjectAttributes value) {
        this.attributes = value;
    }

    /**
     * Gets the value of the attrRestrictions property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getAttrRestrictions() {
        return attrRestrictions;
    }

    /**
     * Sets the value of the attrRestrictions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setAttrRestrictions(Object value) {
        this.attrRestrictions = value;
    }

    /**
     * Gets the value of the attrImages property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getAttrImages() {
        return attrImages;
    }

    /**
     * Sets the value of the attrImages property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setAttrImages(Object value) {
        this.attrImages = value;
    }

    /**
     * Gets the value of the boId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoId() {
        return boId;
    }

    /**
     * Sets the value of the boId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoId(String value) {
        this.boId = value;
    }

    /**
     * Gets the value of the boMetaId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoMetaId() {
        return boMetaId;
    }

    /**
     * Sets the value of the boMetaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoMetaId(String value) {
        this.boMetaId = value;
    }

    /**
     * Gets the value of the canWrite property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCanWrite() {
        return canWrite;
    }

    /**
     * Sets the value of the canWrite property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCanWrite(Boolean value) {
        this.canWrite = value;
    }

    /**
     * Gets the value of the canDelete property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCanDelete() {
        return canDelete;
    }

    /**
     * Sets the value of the canDelete property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCanDelete(Boolean value) {
        this.canDelete = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setVersion(BigInteger value) {
        this.version = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            EntityObjectLinks theLinks;
            theLinks = this.getLinks();
            strategy.appendField(locator, this, "links", buffer, theLinks);
        }
        {
            EntityObjectAttributes theAttributes;
            theAttributes = this.getAttributes();
            strategy.appendField(locator, this, "attributes", buffer, theAttributes);
        }
        {
            Object theAttrRestrictions;
            theAttrRestrictions = this.getAttrRestrictions();
            strategy.appendField(locator, this, "attrRestrictions", buffer, theAttrRestrictions);
        }
        {
            Object theAttrImages;
            theAttrImages = this.getAttrImages();
            strategy.appendField(locator, this, "attrImages", buffer, theAttrImages);
        }
        {
            String theBoId;
            theBoId = this.getBoId();
            strategy.appendField(locator, this, "boId", buffer, theBoId);
        }
        {
            String theBoMetaId;
            theBoMetaId = this.getBoMetaId();
            strategy.appendField(locator, this, "boMetaId", buffer, theBoMetaId);
        }
        {
            Boolean theCanWrite;
            theCanWrite = this.isCanWrite();
            strategy.appendField(locator, this, "canWrite", buffer, theCanWrite);
        }
        {
            Boolean theCanDelete;
            theCanDelete = this.isCanDelete();
            strategy.appendField(locator, this, "canDelete", buffer, theCanDelete);
        }
        {
            BigInteger theVersion;
            theVersion = this.getVersion();
            strategy.appendField(locator, this, "version", buffer, theVersion);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof EntityObject)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final EntityObject that = ((EntityObject) object);
        {
            EntityObjectLinks lhsLinks;
            lhsLinks = this.getLinks();
            EntityObjectLinks rhsLinks;
            rhsLinks = that.getLinks();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "links", lhsLinks), LocatorUtils.property(thatLocator, "links", rhsLinks), lhsLinks, rhsLinks)) {
                return false;
            }
        }
        {
            EntityObjectAttributes lhsAttributes;
            lhsAttributes = this.getAttributes();
            EntityObjectAttributes rhsAttributes;
            rhsAttributes = that.getAttributes();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "attributes", lhsAttributes), LocatorUtils.property(thatLocator, "attributes", rhsAttributes), lhsAttributes, rhsAttributes)) {
                return false;
            }
        }
        {
            Object lhsAttrRestrictions;
            lhsAttrRestrictions = this.getAttrRestrictions();
            Object rhsAttrRestrictions;
            rhsAttrRestrictions = that.getAttrRestrictions();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "attrRestrictions", lhsAttrRestrictions), LocatorUtils.property(thatLocator, "attrRestrictions", rhsAttrRestrictions), lhsAttrRestrictions, rhsAttrRestrictions)) {
                return false;
            }
        }
        {
            Object lhsAttrImages;
            lhsAttrImages = this.getAttrImages();
            Object rhsAttrImages;
            rhsAttrImages = that.getAttrImages();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "attrImages", lhsAttrImages), LocatorUtils.property(thatLocator, "attrImages", rhsAttrImages), lhsAttrImages, rhsAttrImages)) {
                return false;
            }
        }
        {
            String lhsBoId;
            lhsBoId = this.getBoId();
            String rhsBoId;
            rhsBoId = that.getBoId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "boId", lhsBoId), LocatorUtils.property(thatLocator, "boId", rhsBoId), lhsBoId, rhsBoId)) {
                return false;
            }
        }
        {
            String lhsBoMetaId;
            lhsBoMetaId = this.getBoMetaId();
            String rhsBoMetaId;
            rhsBoMetaId = that.getBoMetaId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "boMetaId", lhsBoMetaId), LocatorUtils.property(thatLocator, "boMetaId", rhsBoMetaId), lhsBoMetaId, rhsBoMetaId)) {
                return false;
            }
        }
        {
            Boolean lhsCanWrite;
            lhsCanWrite = this.isCanWrite();
            Boolean rhsCanWrite;
            rhsCanWrite = that.isCanWrite();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "canWrite", lhsCanWrite), LocatorUtils.property(thatLocator, "canWrite", rhsCanWrite), lhsCanWrite, rhsCanWrite)) {
                return false;
            }
        }
        {
            Boolean lhsCanDelete;
            lhsCanDelete = this.isCanDelete();
            Boolean rhsCanDelete;
            rhsCanDelete = that.isCanDelete();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "canDelete", lhsCanDelete), LocatorUtils.property(thatLocator, "canDelete", rhsCanDelete), lhsCanDelete, rhsCanDelete)) {
                return false;
            }
        }
        {
            BigInteger lhsVersion;
            lhsVersion = this.getVersion();
            BigInteger rhsVersion;
            rhsVersion = that.getVersion();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "version", lhsVersion), LocatorUtils.property(thatLocator, "version", rhsVersion), lhsVersion, rhsVersion)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            EntityObjectLinks theLinks;
            theLinks = this.getLinks();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "links", theLinks), currentHashCode, theLinks);
        }
        {
            EntityObjectAttributes theAttributes;
            theAttributes = this.getAttributes();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "attributes", theAttributes), currentHashCode, theAttributes);
        }
        {
            Object theAttrRestrictions;
            theAttrRestrictions = this.getAttrRestrictions();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "attrRestrictions", theAttrRestrictions), currentHashCode, theAttrRestrictions);
        }
        {
            Object theAttrImages;
            theAttrImages = this.getAttrImages();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "attrImages", theAttrImages), currentHashCode, theAttrImages);
        }
        {
            String theBoId;
            theBoId = this.getBoId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "boId", theBoId), currentHashCode, theBoId);
        }
        {
            String theBoMetaId;
            theBoMetaId = this.getBoMetaId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "boMetaId", theBoMetaId), currentHashCode, theBoMetaId);
        }
        {
            Boolean theCanWrite;
            theCanWrite = this.isCanWrite();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "canWrite", theCanWrite), currentHashCode, theCanWrite);
        }
        {
            Boolean theCanDelete;
            theCanDelete = this.isCanDelete();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "canDelete", theCanDelete), currentHashCode, theCanDelete);
        }
        {
            BigInteger theVersion;
            theVersion = this.getVersion();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "version", theVersion), currentHashCode, theVersion);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof EntityObject) {
            final EntityObject copy = ((EntityObject) draftCopy);
            if (this.links!= null) {
                EntityObjectLinks sourceLinks;
                sourceLinks = this.getLinks();
                EntityObjectLinks copyLinks = ((EntityObjectLinks) strategy.copy(LocatorUtils.property(locator, "links", sourceLinks), sourceLinks));
                copy.setLinks(copyLinks);
            } else {
                copy.links = null;
            }
            if (this.attributes!= null) {
                EntityObjectAttributes sourceAttributes;
                sourceAttributes = this.getAttributes();
                EntityObjectAttributes copyAttributes = ((EntityObjectAttributes) strategy.copy(LocatorUtils.property(locator, "attributes", sourceAttributes), sourceAttributes));
                copy.setAttributes(copyAttributes);
            } else {
                copy.attributes = null;
            }
            if (this.attrRestrictions!= null) {
                Object sourceAttrRestrictions;
                sourceAttrRestrictions = this.getAttrRestrictions();
                Object copyAttrRestrictions = ((Object) strategy.copy(LocatorUtils.property(locator, "attrRestrictions", sourceAttrRestrictions), sourceAttrRestrictions));
                copy.setAttrRestrictions(copyAttrRestrictions);
            } else {
                copy.attrRestrictions = null;
            }
            if (this.attrImages!= null) {
                Object sourceAttrImages;
                sourceAttrImages = this.getAttrImages();
                Object copyAttrImages = ((Object) strategy.copy(LocatorUtils.property(locator, "attrImages", sourceAttrImages), sourceAttrImages));
                copy.setAttrImages(copyAttrImages);
            } else {
                copy.attrImages = null;
            }
            if (this.boId!= null) {
                String sourceBoId;
                sourceBoId = this.getBoId();
                String copyBoId = ((String) strategy.copy(LocatorUtils.property(locator, "boId", sourceBoId), sourceBoId));
                copy.setBoId(copyBoId);
            } else {
                copy.boId = null;
            }
            if (this.boMetaId!= null) {
                String sourceBoMetaId;
                sourceBoMetaId = this.getBoMetaId();
                String copyBoMetaId = ((String) strategy.copy(LocatorUtils.property(locator, "boMetaId", sourceBoMetaId), sourceBoMetaId));
                copy.setBoMetaId(copyBoMetaId);
            } else {
                copy.boMetaId = null;
            }
            if (this.canWrite!= null) {
                Boolean sourceCanWrite;
                sourceCanWrite = this.isCanWrite();
                Boolean copyCanWrite = ((Boolean) strategy.copy(LocatorUtils.property(locator, "canWrite", sourceCanWrite), sourceCanWrite));
                copy.setCanWrite(copyCanWrite);
            } else {
                copy.canWrite = null;
            }
            if (this.canDelete!= null) {
                Boolean sourceCanDelete;
                sourceCanDelete = this.isCanDelete();
                Boolean copyCanDelete = ((Boolean) strategy.copy(LocatorUtils.property(locator, "canDelete", sourceCanDelete), sourceCanDelete));
                copy.setCanDelete(copyCanDelete);
            } else {
                copy.canDelete = null;
            }
            if (this.version!= null) {
                BigInteger sourceVersion;
                sourceVersion = this.getVersion();
                BigInteger copyVersion = ((BigInteger) strategy.copy(LocatorUtils.property(locator, "version", sourceVersion), sourceVersion));
                copy.setVersion(copyVersion);
            } else {
                copy.version = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new EntityObject();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityObject.Builder<_B> _other) {
        _other.links = ((this.links == null)?null:this.links.newCopyBuilder(_other));
        _other.attributes = ((this.attributes == null)?null:this.attributes.newCopyBuilder(_other));
        _other.attrRestrictions = this.attrRestrictions;
        _other.attrImages = this.attrImages;
        _other.boId = this.boId;
        _other.boMetaId = this.boMetaId;
        _other.canWrite = this.canWrite;
        _other.canDelete = this.canDelete;
        _other.version = this.version;
    }

    public<_B >EntityObject.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new EntityObject.Builder<_B>(_parentBuilder, this, true);
    }

    public EntityObject.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static EntityObject.Builder<Void> builder() {
        return new EntityObject.Builder<Void>(null, null, false);
    }

    public static<_B >EntityObject.Builder<_B> copyOf(final EntityObject _other) {
        final EntityObject.Builder<_B> _newBuilder = new EntityObject.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityObject.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree linksPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("links"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(linksPropertyTree!= null):((linksPropertyTree == null)||(!linksPropertyTree.isLeaf())))) {
            _other.links = ((this.links == null)?null:this.links.newCopyBuilder(_other, linksPropertyTree, _propertyTreeUse));
        }
        final PropertyTree attributesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("attributes"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(attributesPropertyTree!= null):((attributesPropertyTree == null)||(!attributesPropertyTree.isLeaf())))) {
            _other.attributes = ((this.attributes == null)?null:this.attributes.newCopyBuilder(_other, attributesPropertyTree, _propertyTreeUse));
        }
        final PropertyTree attrRestrictionsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("attrRestrictions"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(attrRestrictionsPropertyTree!= null):((attrRestrictionsPropertyTree == null)||(!attrRestrictionsPropertyTree.isLeaf())))) {
            _other.attrRestrictions = this.attrRestrictions;
        }
        final PropertyTree attrImagesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("attrImages"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(attrImagesPropertyTree!= null):((attrImagesPropertyTree == null)||(!attrImagesPropertyTree.isLeaf())))) {
            _other.attrImages = this.attrImages;
        }
        final PropertyTree boIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boIdPropertyTree!= null):((boIdPropertyTree == null)||(!boIdPropertyTree.isLeaf())))) {
            _other.boId = this.boId;
        }
        final PropertyTree boMetaIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boMetaId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boMetaIdPropertyTree!= null):((boMetaIdPropertyTree == null)||(!boMetaIdPropertyTree.isLeaf())))) {
            _other.boMetaId = this.boMetaId;
        }
        final PropertyTree canWritePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("canWrite"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(canWritePropertyTree!= null):((canWritePropertyTree == null)||(!canWritePropertyTree.isLeaf())))) {
            _other.canWrite = this.canWrite;
        }
        final PropertyTree canDeletePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("canDelete"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(canDeletePropertyTree!= null):((canDeletePropertyTree == null)||(!canDeletePropertyTree.isLeaf())))) {
            _other.canDelete = this.canDelete;
        }
        final PropertyTree versionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("version"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(versionPropertyTree!= null):((versionPropertyTree == null)||(!versionPropertyTree.isLeaf())))) {
            _other.version = this.version;
        }
    }

    public<_B >EntityObject.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new EntityObject.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public EntityObject.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >EntityObject.Builder<_B> copyOf(final EntityObject _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final EntityObject.Builder<_B> _newBuilder = new EntityObject.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static EntityObject.Builder<Void> copyExcept(final EntityObject _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static EntityObject.Builder<Void> copyOnly(final EntityObject _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final EntityObject _storedValue;
        private EntityObjectLinks.Builder<EntityObject.Builder<_B>> links;
        private EntityObjectAttributes.Builder<EntityObject.Builder<_B>> attributes;
        private Object attrRestrictions;
        private Object attrImages;
        private String boId;
        private String boMetaId;
        private Boolean canWrite;
        private Boolean canDelete;
        private BigInteger version;

        public Builder(final _B _parentBuilder, final EntityObject _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.links = ((_other.links == null)?null:_other.links.newCopyBuilder(this));
                    this.attributes = ((_other.attributes == null)?null:_other.attributes.newCopyBuilder(this));
                    this.attrRestrictions = _other.attrRestrictions;
                    this.attrImages = _other.attrImages;
                    this.boId = _other.boId;
                    this.boMetaId = _other.boMetaId;
                    this.canWrite = _other.canWrite;
                    this.canDelete = _other.canDelete;
                    this.version = _other.version;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final EntityObject _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree linksPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("links"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(linksPropertyTree!= null):((linksPropertyTree == null)||(!linksPropertyTree.isLeaf())))) {
                        this.links = ((_other.links == null)?null:_other.links.newCopyBuilder(this, linksPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree attributesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("attributes"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(attributesPropertyTree!= null):((attributesPropertyTree == null)||(!attributesPropertyTree.isLeaf())))) {
                        this.attributes = ((_other.attributes == null)?null:_other.attributes.newCopyBuilder(this, attributesPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree attrRestrictionsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("attrRestrictions"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(attrRestrictionsPropertyTree!= null):((attrRestrictionsPropertyTree == null)||(!attrRestrictionsPropertyTree.isLeaf())))) {
                        this.attrRestrictions = _other.attrRestrictions;
                    }
                    final PropertyTree attrImagesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("attrImages"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(attrImagesPropertyTree!= null):((attrImagesPropertyTree == null)||(!attrImagesPropertyTree.isLeaf())))) {
                        this.attrImages = _other.attrImages;
                    }
                    final PropertyTree boIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boIdPropertyTree!= null):((boIdPropertyTree == null)||(!boIdPropertyTree.isLeaf())))) {
                        this.boId = _other.boId;
                    }
                    final PropertyTree boMetaIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boMetaId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boMetaIdPropertyTree!= null):((boMetaIdPropertyTree == null)||(!boMetaIdPropertyTree.isLeaf())))) {
                        this.boMetaId = _other.boMetaId;
                    }
                    final PropertyTree canWritePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("canWrite"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(canWritePropertyTree!= null):((canWritePropertyTree == null)||(!canWritePropertyTree.isLeaf())))) {
                        this.canWrite = _other.canWrite;
                    }
                    final PropertyTree canDeletePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("canDelete"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(canDeletePropertyTree!= null):((canDeletePropertyTree == null)||(!canDeletePropertyTree.isLeaf())))) {
                        this.canDelete = _other.canDelete;
                    }
                    final PropertyTree versionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("version"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(versionPropertyTree!= null):((versionPropertyTree == null)||(!versionPropertyTree.isLeaf())))) {
                        this.version = _other.version;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends EntityObject >_P init(final _P _product) {
            _product.links = ((this.links == null)?null:this.links.build());
            _product.attributes = ((this.attributes == null)?null:this.attributes.build());
            _product.attrRestrictions = this.attrRestrictions;
            _product.attrImages = this.attrImages;
            _product.boId = this.boId;
            _product.boMetaId = this.boMetaId;
            _product.canWrite = this.canWrite;
            _product.canDelete = this.canDelete;
            _product.version = this.version;
            return _product;
        }

        /**
         * Sets the new value of "links" (any previous value will be replaced)
         * 
         * @param links
         *     New value of the "links" property.
         */
        public EntityObject.Builder<_B> withLinks(final EntityObjectLinks links) {
            this.links = ((links == null)?null:new EntityObjectLinks.Builder<EntityObject.Builder<_B>>(this, links, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "links" property.
         * Use {@link org.nuclos.schema.rest.EntityObjectLinks.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "links" property.
         *     Use {@link org.nuclos.schema.rest.EntityObjectLinks.Builder#end()} to return to the current builder.
         */
        public EntityObjectLinks.Builder<? extends EntityObject.Builder<_B>> withLinks() {
            if (this.links!= null) {
                return this.links;
            }
            return this.links = new EntityObjectLinks.Builder<EntityObject.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "attributes" (any previous value will be replaced)
         * 
         * @param attributes
         *     New value of the "attributes" property.
         */
        public EntityObject.Builder<_B> withAttributes(final EntityObjectAttributes attributes) {
            this.attributes = ((attributes == null)?null:new EntityObjectAttributes.Builder<EntityObject.Builder<_B>>(this, attributes, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "attributes" property.
         * Use {@link org.nuclos.schema.rest.EntityObjectAttributes.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "attributes" property.
         *     Use {@link org.nuclos.schema.rest.EntityObjectAttributes.Builder#end()} to return to the current builder.
         */
        public EntityObjectAttributes.Builder<? extends EntityObject.Builder<_B>> withAttributes() {
            if (this.attributes!= null) {
                return this.attributes;
            }
            return this.attributes = new EntityObjectAttributes.Builder<EntityObject.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "attrRestrictions" (any previous value will be replaced)
         * 
         * @param attrRestrictions
         *     New value of the "attrRestrictions" property.
         */
        public EntityObject.Builder<_B> withAttrRestrictions(final Object attrRestrictions) {
            this.attrRestrictions = attrRestrictions;
            return this;
        }

        /**
         * Sets the new value of "attrImages" (any previous value will be replaced)
         * 
         * @param attrImages
         *     New value of the "attrImages" property.
         */
        public EntityObject.Builder<_B> withAttrImages(final Object attrImages) {
            this.attrImages = attrImages;
            return this;
        }

        /**
         * Sets the new value of "boId" (any previous value will be replaced)
         * 
         * @param boId
         *     New value of the "boId" property.
         */
        public EntityObject.Builder<_B> withBoId(final String boId) {
            this.boId = boId;
            return this;
        }

        /**
         * Sets the new value of "boMetaId" (any previous value will be replaced)
         * 
         * @param boMetaId
         *     New value of the "boMetaId" property.
         */
        public EntityObject.Builder<_B> withBoMetaId(final String boMetaId) {
            this.boMetaId = boMetaId;
            return this;
        }

        /**
         * Sets the new value of "canWrite" (any previous value will be replaced)
         * 
         * @param canWrite
         *     New value of the "canWrite" property.
         */
        public EntityObject.Builder<_B> withCanWrite(final Boolean canWrite) {
            this.canWrite = canWrite;
            return this;
        }

        /**
         * Sets the new value of "canDelete" (any previous value will be replaced)
         * 
         * @param canDelete
         *     New value of the "canDelete" property.
         */
        public EntityObject.Builder<_B> withCanDelete(final Boolean canDelete) {
            this.canDelete = canDelete;
            return this;
        }

        /**
         * Sets the new value of "version" (any previous value will be replaced)
         * 
         * @param version
         *     New value of the "version" property.
         */
        public EntityObject.Builder<_B> withVersion(final BigInteger version) {
            this.version = version;
            return this;
        }

        @Override
        public EntityObject build() {
            if (_storedValue == null) {
                return this.init(new EntityObject());
            } else {
                return ((EntityObject) _storedValue);
            }
        }

        public EntityObject.Builder<_B> copyOf(final EntityObject _other) {
            _other.copyTo(this);
            return this;
        }

        public EntityObject.Builder<_B> copyOf(final EntityObject.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends EntityObject.Selector<EntityObject.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static EntityObject.Select _root() {
            return new EntityObject.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private EntityObjectLinks.Selector<TRoot, EntityObject.Selector<TRoot, TParent>> links = null;
        private EntityObjectAttributes.Selector<TRoot, EntityObject.Selector<TRoot, TParent>> attributes = null;
        private com.kscs.util.jaxb.Selector<TRoot, EntityObject.Selector<TRoot, TParent>> attrRestrictions = null;
        private com.kscs.util.jaxb.Selector<TRoot, EntityObject.Selector<TRoot, TParent>> attrImages = null;
        private com.kscs.util.jaxb.Selector<TRoot, EntityObject.Selector<TRoot, TParent>> boId = null;
        private com.kscs.util.jaxb.Selector<TRoot, EntityObject.Selector<TRoot, TParent>> boMetaId = null;
        private com.kscs.util.jaxb.Selector<TRoot, EntityObject.Selector<TRoot, TParent>> canWrite = null;
        private com.kscs.util.jaxb.Selector<TRoot, EntityObject.Selector<TRoot, TParent>> canDelete = null;
        private com.kscs.util.jaxb.Selector<TRoot, EntityObject.Selector<TRoot, TParent>> version = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.links!= null) {
                products.put("links", this.links.init());
            }
            if (this.attributes!= null) {
                products.put("attributes", this.attributes.init());
            }
            if (this.attrRestrictions!= null) {
                products.put("attrRestrictions", this.attrRestrictions.init());
            }
            if (this.attrImages!= null) {
                products.put("attrImages", this.attrImages.init());
            }
            if (this.boId!= null) {
                products.put("boId", this.boId.init());
            }
            if (this.boMetaId!= null) {
                products.put("boMetaId", this.boMetaId.init());
            }
            if (this.canWrite!= null) {
                products.put("canWrite", this.canWrite.init());
            }
            if (this.canDelete!= null) {
                products.put("canDelete", this.canDelete.init());
            }
            if (this.version!= null) {
                products.put("version", this.version.init());
            }
            return products;
        }

        public EntityObjectLinks.Selector<TRoot, EntityObject.Selector<TRoot, TParent>> links() {
            return ((this.links == null)?this.links = new EntityObjectLinks.Selector<TRoot, EntityObject.Selector<TRoot, TParent>>(this._root, this, "links"):this.links);
        }

        public EntityObjectAttributes.Selector<TRoot, EntityObject.Selector<TRoot, TParent>> attributes() {
            return ((this.attributes == null)?this.attributes = new EntityObjectAttributes.Selector<TRoot, EntityObject.Selector<TRoot, TParent>>(this._root, this, "attributes"):this.attributes);
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityObject.Selector<TRoot, TParent>> attrRestrictions() {
            return ((this.attrRestrictions == null)?this.attrRestrictions = new com.kscs.util.jaxb.Selector<TRoot, EntityObject.Selector<TRoot, TParent>>(this._root, this, "attrRestrictions"):this.attrRestrictions);
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityObject.Selector<TRoot, TParent>> attrImages() {
            return ((this.attrImages == null)?this.attrImages = new com.kscs.util.jaxb.Selector<TRoot, EntityObject.Selector<TRoot, TParent>>(this._root, this, "attrImages"):this.attrImages);
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityObject.Selector<TRoot, TParent>> boId() {
            return ((this.boId == null)?this.boId = new com.kscs.util.jaxb.Selector<TRoot, EntityObject.Selector<TRoot, TParent>>(this._root, this, "boId"):this.boId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityObject.Selector<TRoot, TParent>> boMetaId() {
            return ((this.boMetaId == null)?this.boMetaId = new com.kscs.util.jaxb.Selector<TRoot, EntityObject.Selector<TRoot, TParent>>(this._root, this, "boMetaId"):this.boMetaId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityObject.Selector<TRoot, TParent>> canWrite() {
            return ((this.canWrite == null)?this.canWrite = new com.kscs.util.jaxb.Selector<TRoot, EntityObject.Selector<TRoot, TParent>>(this._root, this, "canWrite"):this.canWrite);
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityObject.Selector<TRoot, TParent>> canDelete() {
            return ((this.canDelete == null)?this.canDelete = new com.kscs.util.jaxb.Selector<TRoot, EntityObject.Selector<TRoot, TParent>>(this._root, this, "canDelete"):this.canDelete);
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityObject.Selector<TRoot, TParent>> version() {
            return ((this.version == null)?this.version = new com.kscs.util.jaxb.Selector<TRoot, EntityObject.Selector<TRoot, TParent>>(this._root, this, "version"):this.version);
        }

    }

}
