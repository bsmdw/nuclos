
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;


/**
 * <p>Java class for entity-object-links complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="entity-object-links"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="self" type="{urn:org.nuclos.schema.rest}rest-link"/&gt;
 *         &lt;element name="boMeta" type="{urn:org.nuclos.schema.rest}rest-link"/&gt;
 *         &lt;element name="clone" type="{urn:org.nuclos.schema.rest}rest-link"/&gt;
 *         &lt;element name="printouts" type="{urn:org.nuclos.schema.rest}rest-link"/&gt;
 *         &lt;element name="stateIcon" type="{urn:org.nuclos.schema.rest}rest-link"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "entity-object-links", propOrder = {
    "self",
    "boMeta",
    "clone",
    "printouts",
    "stateIcon"
})
public class EntityObjectLinks implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected RestLink self;
    @XmlElement(required = true)
    protected RestLink boMeta;
    @XmlElement(required = true)
    protected RestLink clone;
    @XmlElement(required = true)
    protected RestLink printouts;
    @XmlElement(required = true)
    protected RestLink stateIcon;

    /**
     * Gets the value of the self property.
     * 
     * @return
     *     possible object is
     *     {@link RestLink }
     *     
     */
    public RestLink getSelf() {
        return self;
    }

    /**
     * Sets the value of the self property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestLink }
     *     
     */
    public void setSelf(RestLink value) {
        this.self = value;
    }

    /**
     * Gets the value of the boMeta property.
     * 
     * @return
     *     possible object is
     *     {@link RestLink }
     *     
     */
    public RestLink getBoMeta() {
        return boMeta;
    }

    /**
     * Sets the value of the boMeta property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestLink }
     *     
     */
    public void setBoMeta(RestLink value) {
        this.boMeta = value;
    }

    /**
     * Gets the value of the clone property.
     * 
     * @return
     *     possible object is
     *     {@link RestLink }
     *     
     */
    public RestLink getClone() {
        return clone;
    }

    /**
     * Sets the value of the clone property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestLink }
     *     
     */
    public void setClone(RestLink value) {
        this.clone = value;
    }

    /**
     * Gets the value of the printouts property.
     * 
     * @return
     *     possible object is
     *     {@link RestLink }
     *     
     */
    public RestLink getPrintouts() {
        return printouts;
    }

    /**
     * Sets the value of the printouts property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestLink }
     *     
     */
    public void setPrintouts(RestLink value) {
        this.printouts = value;
    }

    /**
     * Gets the value of the stateIcon property.
     * 
     * @return
     *     possible object is
     *     {@link RestLink }
     *     
     */
    public RestLink getStateIcon() {
        return stateIcon;
    }

    /**
     * Sets the value of the stateIcon property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestLink }
     *     
     */
    public void setStateIcon(RestLink value) {
        this.stateIcon = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            RestLink theSelf;
            theSelf = this.getSelf();
            strategy.appendField(locator, this, "self", buffer, theSelf);
        }
        {
            RestLink theBoMeta;
            theBoMeta = this.getBoMeta();
            strategy.appendField(locator, this, "boMeta", buffer, theBoMeta);
        }
        {
            RestLink theClone;
            theClone = this.getClone();
            strategy.appendField(locator, this, "clone", buffer, theClone);
        }
        {
            RestLink thePrintouts;
            thePrintouts = this.getPrintouts();
            strategy.appendField(locator, this, "printouts", buffer, thePrintouts);
        }
        {
            RestLink theStateIcon;
            theStateIcon = this.getStateIcon();
            strategy.appendField(locator, this, "stateIcon", buffer, theStateIcon);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof EntityObjectLinks)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final EntityObjectLinks that = ((EntityObjectLinks) object);
        {
            RestLink lhsSelf;
            lhsSelf = this.getSelf();
            RestLink rhsSelf;
            rhsSelf = that.getSelf();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "self", lhsSelf), LocatorUtils.property(thatLocator, "self", rhsSelf), lhsSelf, rhsSelf)) {
                return false;
            }
        }
        {
            RestLink lhsBoMeta;
            lhsBoMeta = this.getBoMeta();
            RestLink rhsBoMeta;
            rhsBoMeta = that.getBoMeta();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "boMeta", lhsBoMeta), LocatorUtils.property(thatLocator, "boMeta", rhsBoMeta), lhsBoMeta, rhsBoMeta)) {
                return false;
            }
        }
        {
            RestLink lhsClone;
            lhsClone = this.getClone();
            RestLink rhsClone;
            rhsClone = that.getClone();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "clone", lhsClone), LocatorUtils.property(thatLocator, "clone", rhsClone), lhsClone, rhsClone)) {
                return false;
            }
        }
        {
            RestLink lhsPrintouts;
            lhsPrintouts = this.getPrintouts();
            RestLink rhsPrintouts;
            rhsPrintouts = that.getPrintouts();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "printouts", lhsPrintouts), LocatorUtils.property(thatLocator, "printouts", rhsPrintouts), lhsPrintouts, rhsPrintouts)) {
                return false;
            }
        }
        {
            RestLink lhsStateIcon;
            lhsStateIcon = this.getStateIcon();
            RestLink rhsStateIcon;
            rhsStateIcon = that.getStateIcon();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "stateIcon", lhsStateIcon), LocatorUtils.property(thatLocator, "stateIcon", rhsStateIcon), lhsStateIcon, rhsStateIcon)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            RestLink theSelf;
            theSelf = this.getSelf();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "self", theSelf), currentHashCode, theSelf);
        }
        {
            RestLink theBoMeta;
            theBoMeta = this.getBoMeta();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "boMeta", theBoMeta), currentHashCode, theBoMeta);
        }
        {
            RestLink theClone;
            theClone = this.getClone();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "clone", theClone), currentHashCode, theClone);
        }
        {
            RestLink thePrintouts;
            thePrintouts = this.getPrintouts();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "printouts", thePrintouts), currentHashCode, thePrintouts);
        }
        {
            RestLink theStateIcon;
            theStateIcon = this.getStateIcon();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "stateIcon", theStateIcon), currentHashCode, theStateIcon);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof EntityObjectLinks) {
            final EntityObjectLinks copy = ((EntityObjectLinks) draftCopy);
            if (this.self!= null) {
                RestLink sourceSelf;
                sourceSelf = this.getSelf();
                RestLink copySelf = ((RestLink) strategy.copy(LocatorUtils.property(locator, "self", sourceSelf), sourceSelf));
                copy.setSelf(copySelf);
            } else {
                copy.self = null;
            }
            if (this.boMeta!= null) {
                RestLink sourceBoMeta;
                sourceBoMeta = this.getBoMeta();
                RestLink copyBoMeta = ((RestLink) strategy.copy(LocatorUtils.property(locator, "boMeta", sourceBoMeta), sourceBoMeta));
                copy.setBoMeta(copyBoMeta);
            } else {
                copy.boMeta = null;
            }
            if (this.clone!= null) {
                RestLink sourceClone;
                sourceClone = this.getClone();
                RestLink copyClone = ((RestLink) strategy.copy(LocatorUtils.property(locator, "clone", sourceClone), sourceClone));
                copy.setClone(copyClone);
            } else {
                copy.clone = null;
            }
            if (this.printouts!= null) {
                RestLink sourcePrintouts;
                sourcePrintouts = this.getPrintouts();
                RestLink copyPrintouts = ((RestLink) strategy.copy(LocatorUtils.property(locator, "printouts", sourcePrintouts), sourcePrintouts));
                copy.setPrintouts(copyPrintouts);
            } else {
                copy.printouts = null;
            }
            if (this.stateIcon!= null) {
                RestLink sourceStateIcon;
                sourceStateIcon = this.getStateIcon();
                RestLink copyStateIcon = ((RestLink) strategy.copy(LocatorUtils.property(locator, "stateIcon", sourceStateIcon), sourceStateIcon));
                copy.setStateIcon(copyStateIcon);
            } else {
                copy.stateIcon = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new EntityObjectLinks();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityObjectLinks.Builder<_B> _other) {
        _other.self = ((this.self == null)?null:this.self.newCopyBuilder(_other));
        _other.boMeta = ((this.boMeta == null)?null:this.boMeta.newCopyBuilder(_other));
        _other.clone = ((this.clone == null)?null:this.clone.newCopyBuilder(_other));
        _other.printouts = ((this.printouts == null)?null:this.printouts.newCopyBuilder(_other));
        _other.stateIcon = ((this.stateIcon == null)?null:this.stateIcon.newCopyBuilder(_other));
    }

    public<_B >EntityObjectLinks.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new EntityObjectLinks.Builder<_B>(_parentBuilder, this, true);
    }

    public EntityObjectLinks.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static EntityObjectLinks.Builder<Void> builder() {
        return new EntityObjectLinks.Builder<Void>(null, null, false);
    }

    public static<_B >EntityObjectLinks.Builder<_B> copyOf(final EntityObjectLinks _other) {
        final EntityObjectLinks.Builder<_B> _newBuilder = new EntityObjectLinks.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityObjectLinks.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree selfPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("self"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(selfPropertyTree!= null):((selfPropertyTree == null)||(!selfPropertyTree.isLeaf())))) {
            _other.self = ((this.self == null)?null:this.self.newCopyBuilder(_other, selfPropertyTree, _propertyTreeUse));
        }
        final PropertyTree boMetaPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boMeta"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boMetaPropertyTree!= null):((boMetaPropertyTree == null)||(!boMetaPropertyTree.isLeaf())))) {
            _other.boMeta = ((this.boMeta == null)?null:this.boMeta.newCopyBuilder(_other, boMetaPropertyTree, _propertyTreeUse));
        }
        final PropertyTree clonePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("clone"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(clonePropertyTree!= null):((clonePropertyTree == null)||(!clonePropertyTree.isLeaf())))) {
            _other.clone = ((this.clone == null)?null:this.clone.newCopyBuilder(_other, clonePropertyTree, _propertyTreeUse));
        }
        final PropertyTree printoutsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("printouts"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(printoutsPropertyTree!= null):((printoutsPropertyTree == null)||(!printoutsPropertyTree.isLeaf())))) {
            _other.printouts = ((this.printouts == null)?null:this.printouts.newCopyBuilder(_other, printoutsPropertyTree, _propertyTreeUse));
        }
        final PropertyTree stateIconPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("stateIcon"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(stateIconPropertyTree!= null):((stateIconPropertyTree == null)||(!stateIconPropertyTree.isLeaf())))) {
            _other.stateIcon = ((this.stateIcon == null)?null:this.stateIcon.newCopyBuilder(_other, stateIconPropertyTree, _propertyTreeUse));
        }
    }

    public<_B >EntityObjectLinks.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new EntityObjectLinks.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public EntityObjectLinks.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >EntityObjectLinks.Builder<_B> copyOf(final EntityObjectLinks _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final EntityObjectLinks.Builder<_B> _newBuilder = new EntityObjectLinks.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static EntityObjectLinks.Builder<Void> copyExcept(final EntityObjectLinks _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static EntityObjectLinks.Builder<Void> copyOnly(final EntityObjectLinks _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final EntityObjectLinks _storedValue;
        private RestLink.Builder<EntityObjectLinks.Builder<_B>> self;
        private RestLink.Builder<EntityObjectLinks.Builder<_B>> boMeta;
        private RestLink.Builder<EntityObjectLinks.Builder<_B>> clone;
        private RestLink.Builder<EntityObjectLinks.Builder<_B>> printouts;
        private RestLink.Builder<EntityObjectLinks.Builder<_B>> stateIcon;

        public Builder(final _B _parentBuilder, final EntityObjectLinks _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.self = ((_other.self == null)?null:_other.self.newCopyBuilder(this));
                    this.boMeta = ((_other.boMeta == null)?null:_other.boMeta.newCopyBuilder(this));
                    this.clone = ((_other.clone == null)?null:_other.clone.newCopyBuilder(this));
                    this.printouts = ((_other.printouts == null)?null:_other.printouts.newCopyBuilder(this));
                    this.stateIcon = ((_other.stateIcon == null)?null:_other.stateIcon.newCopyBuilder(this));
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final EntityObjectLinks _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree selfPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("self"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(selfPropertyTree!= null):((selfPropertyTree == null)||(!selfPropertyTree.isLeaf())))) {
                        this.self = ((_other.self == null)?null:_other.self.newCopyBuilder(this, selfPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree boMetaPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boMeta"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boMetaPropertyTree!= null):((boMetaPropertyTree == null)||(!boMetaPropertyTree.isLeaf())))) {
                        this.boMeta = ((_other.boMeta == null)?null:_other.boMeta.newCopyBuilder(this, boMetaPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree clonePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("clone"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(clonePropertyTree!= null):((clonePropertyTree == null)||(!clonePropertyTree.isLeaf())))) {
                        this.clone = ((_other.clone == null)?null:_other.clone.newCopyBuilder(this, clonePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree printoutsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("printouts"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(printoutsPropertyTree!= null):((printoutsPropertyTree == null)||(!printoutsPropertyTree.isLeaf())))) {
                        this.printouts = ((_other.printouts == null)?null:_other.printouts.newCopyBuilder(this, printoutsPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree stateIconPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("stateIcon"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(stateIconPropertyTree!= null):((stateIconPropertyTree == null)||(!stateIconPropertyTree.isLeaf())))) {
                        this.stateIcon = ((_other.stateIcon == null)?null:_other.stateIcon.newCopyBuilder(this, stateIconPropertyTree, _propertyTreeUse));
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends EntityObjectLinks >_P init(final _P _product) {
            _product.self = ((this.self == null)?null:this.self.build());
            _product.boMeta = ((this.boMeta == null)?null:this.boMeta.build());
            _product.clone = ((this.clone == null)?null:this.clone.build());
            _product.printouts = ((this.printouts == null)?null:this.printouts.build());
            _product.stateIcon = ((this.stateIcon == null)?null:this.stateIcon.build());
            return _product;
        }

        /**
         * Sets the new value of "self" (any previous value will be replaced)
         * 
         * @param self
         *     New value of the "self" property.
         */
        public EntityObjectLinks.Builder<_B> withSelf(final RestLink self) {
            this.self = ((self == null)?null:new RestLink.Builder<EntityObjectLinks.Builder<_B>>(this, self, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "self" property.
         * Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "self" property.
         *     Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         */
        public RestLink.Builder<? extends EntityObjectLinks.Builder<_B>> withSelf() {
            if (this.self!= null) {
                return this.self;
            }
            return this.self = new RestLink.Builder<EntityObjectLinks.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "boMeta" (any previous value will be replaced)
         * 
         * @param boMeta
         *     New value of the "boMeta" property.
         */
        public EntityObjectLinks.Builder<_B> withBoMeta(final RestLink boMeta) {
            this.boMeta = ((boMeta == null)?null:new RestLink.Builder<EntityObjectLinks.Builder<_B>>(this, boMeta, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "boMeta" property.
         * Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "boMeta" property.
         *     Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         */
        public RestLink.Builder<? extends EntityObjectLinks.Builder<_B>> withBoMeta() {
            if (this.boMeta!= null) {
                return this.boMeta;
            }
            return this.boMeta = new RestLink.Builder<EntityObjectLinks.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "clone" (any previous value will be replaced)
         * 
         * @param clone
         *     New value of the "clone" property.
         */
        public EntityObjectLinks.Builder<_B> withClone(final RestLink clone) {
            this.clone = ((clone == null)?null:new RestLink.Builder<EntityObjectLinks.Builder<_B>>(this, clone, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "clone" property.
         * Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "clone" property.
         *     Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         */
        public RestLink.Builder<? extends EntityObjectLinks.Builder<_B>> withClone() {
            if (this.clone!= null) {
                return this.clone;
            }
            return this.clone = new RestLink.Builder<EntityObjectLinks.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "printouts" (any previous value will be replaced)
         * 
         * @param printouts
         *     New value of the "printouts" property.
         */
        public EntityObjectLinks.Builder<_B> withPrintouts(final RestLink printouts) {
            this.printouts = ((printouts == null)?null:new RestLink.Builder<EntityObjectLinks.Builder<_B>>(this, printouts, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "printouts" property.
         * Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "printouts" property.
         *     Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         */
        public RestLink.Builder<? extends EntityObjectLinks.Builder<_B>> withPrintouts() {
            if (this.printouts!= null) {
                return this.printouts;
            }
            return this.printouts = new RestLink.Builder<EntityObjectLinks.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "stateIcon" (any previous value will be replaced)
         * 
         * @param stateIcon
         *     New value of the "stateIcon" property.
         */
        public EntityObjectLinks.Builder<_B> withStateIcon(final RestLink stateIcon) {
            this.stateIcon = ((stateIcon == null)?null:new RestLink.Builder<EntityObjectLinks.Builder<_B>>(this, stateIcon, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "stateIcon" property.
         * Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "stateIcon" property.
         *     Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         */
        public RestLink.Builder<? extends EntityObjectLinks.Builder<_B>> withStateIcon() {
            if (this.stateIcon!= null) {
                return this.stateIcon;
            }
            return this.stateIcon = new RestLink.Builder<EntityObjectLinks.Builder<_B>>(this, null, false);
        }

        @Override
        public EntityObjectLinks build() {
            if (_storedValue == null) {
                return this.init(new EntityObjectLinks());
            } else {
                return ((EntityObjectLinks) _storedValue);
            }
        }

        public EntityObjectLinks.Builder<_B> copyOf(final EntityObjectLinks _other) {
            _other.copyTo(this);
            return this;
        }

        public EntityObjectLinks.Builder<_B> copyOf(final EntityObjectLinks.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends EntityObjectLinks.Selector<EntityObjectLinks.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static EntityObjectLinks.Select _root() {
            return new EntityObjectLinks.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private RestLink.Selector<TRoot, EntityObjectLinks.Selector<TRoot, TParent>> self = null;
        private RestLink.Selector<TRoot, EntityObjectLinks.Selector<TRoot, TParent>> boMeta = null;
        private RestLink.Selector<TRoot, EntityObjectLinks.Selector<TRoot, TParent>> clone = null;
        private RestLink.Selector<TRoot, EntityObjectLinks.Selector<TRoot, TParent>> printouts = null;
        private RestLink.Selector<TRoot, EntityObjectLinks.Selector<TRoot, TParent>> stateIcon = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.self!= null) {
                products.put("self", this.self.init());
            }
            if (this.boMeta!= null) {
                products.put("boMeta", this.boMeta.init());
            }
            if (this.clone!= null) {
                products.put("clone", this.clone.init());
            }
            if (this.printouts!= null) {
                products.put("printouts", this.printouts.init());
            }
            if (this.stateIcon!= null) {
                products.put("stateIcon", this.stateIcon.init());
            }
            return products;
        }

        public RestLink.Selector<TRoot, EntityObjectLinks.Selector<TRoot, TParent>> self() {
            return ((this.self == null)?this.self = new RestLink.Selector<TRoot, EntityObjectLinks.Selector<TRoot, TParent>>(this._root, this, "self"):this.self);
        }

        public RestLink.Selector<TRoot, EntityObjectLinks.Selector<TRoot, TParent>> boMeta() {
            return ((this.boMeta == null)?this.boMeta = new RestLink.Selector<TRoot, EntityObjectLinks.Selector<TRoot, TParent>>(this._root, this, "boMeta"):this.boMeta);
        }

        public RestLink.Selector<TRoot, EntityObjectLinks.Selector<TRoot, TParent>> clone() {
            return ((this.clone == null)?this.clone = new RestLink.Selector<TRoot, EntityObjectLinks.Selector<TRoot, TParent>>(this._root, this, "clone"):this.clone);
        }

        public RestLink.Selector<TRoot, EntityObjectLinks.Selector<TRoot, TParent>> printouts() {
            return ((this.printouts == null)?this.printouts = new RestLink.Selector<TRoot, EntityObjectLinks.Selector<TRoot, TParent>>(this._root, this, "printouts"):this.printouts);
        }

        public RestLink.Selector<TRoot, EntityObjectLinks.Selector<TRoot, TParent>> stateIcon() {
            return ((this.stateIcon == null)?this.stateIcon = new RestLink.Selector<TRoot, EntityObjectLinks.Selector<TRoot, TParent>>(this._root, this, "stateIcon"):this.stateIcon);
        }

    }

}
