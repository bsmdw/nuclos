
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;


/**
 * <p>Java class for mandator-links complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mandator-links"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="chooseMandator" type="{urn:org.nuclos.schema.rest}rest-link"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mandator-links", propOrder = {
    "chooseMandator"
})
public class MandatorLinks implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected RestLink chooseMandator;

    /**
     * Gets the value of the chooseMandator property.
     * 
     * @return
     *     possible object is
     *     {@link RestLink }
     *     
     */
    public RestLink getChooseMandator() {
        return chooseMandator;
    }

    /**
     * Sets the value of the chooseMandator property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestLink }
     *     
     */
    public void setChooseMandator(RestLink value) {
        this.chooseMandator = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            RestLink theChooseMandator;
            theChooseMandator = this.getChooseMandator();
            strategy.appendField(locator, this, "chooseMandator", buffer, theChooseMandator);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MandatorLinks)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MandatorLinks that = ((MandatorLinks) object);
        {
            RestLink lhsChooseMandator;
            lhsChooseMandator = this.getChooseMandator();
            RestLink rhsChooseMandator;
            rhsChooseMandator = that.getChooseMandator();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "chooseMandator", lhsChooseMandator), LocatorUtils.property(thatLocator, "chooseMandator", rhsChooseMandator), lhsChooseMandator, rhsChooseMandator)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            RestLink theChooseMandator;
            theChooseMandator = this.getChooseMandator();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "chooseMandator", theChooseMandator), currentHashCode, theChooseMandator);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof MandatorLinks) {
            final MandatorLinks copy = ((MandatorLinks) draftCopy);
            if (this.chooseMandator!= null) {
                RestLink sourceChooseMandator;
                sourceChooseMandator = this.getChooseMandator();
                RestLink copyChooseMandator = ((RestLink) strategy.copy(LocatorUtils.property(locator, "chooseMandator", sourceChooseMandator), sourceChooseMandator));
                copy.setChooseMandator(copyChooseMandator);
            } else {
                copy.chooseMandator = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MandatorLinks();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final MandatorLinks.Builder<_B> _other) {
        _other.chooseMandator = ((this.chooseMandator == null)?null:this.chooseMandator.newCopyBuilder(_other));
    }

    public<_B >MandatorLinks.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new MandatorLinks.Builder<_B>(_parentBuilder, this, true);
    }

    public MandatorLinks.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static MandatorLinks.Builder<Void> builder() {
        return new MandatorLinks.Builder<Void>(null, null, false);
    }

    public static<_B >MandatorLinks.Builder<_B> copyOf(final MandatorLinks _other) {
        final MandatorLinks.Builder<_B> _newBuilder = new MandatorLinks.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final MandatorLinks.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree chooseMandatorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("chooseMandator"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(chooseMandatorPropertyTree!= null):((chooseMandatorPropertyTree == null)||(!chooseMandatorPropertyTree.isLeaf())))) {
            _other.chooseMandator = ((this.chooseMandator == null)?null:this.chooseMandator.newCopyBuilder(_other, chooseMandatorPropertyTree, _propertyTreeUse));
        }
    }

    public<_B >MandatorLinks.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new MandatorLinks.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public MandatorLinks.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >MandatorLinks.Builder<_B> copyOf(final MandatorLinks _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final MandatorLinks.Builder<_B> _newBuilder = new MandatorLinks.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static MandatorLinks.Builder<Void> copyExcept(final MandatorLinks _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static MandatorLinks.Builder<Void> copyOnly(final MandatorLinks _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final MandatorLinks _storedValue;
        private RestLink.Builder<MandatorLinks.Builder<_B>> chooseMandator;

        public Builder(final _B _parentBuilder, final MandatorLinks _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.chooseMandator = ((_other.chooseMandator == null)?null:_other.chooseMandator.newCopyBuilder(this));
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final MandatorLinks _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree chooseMandatorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("chooseMandator"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(chooseMandatorPropertyTree!= null):((chooseMandatorPropertyTree == null)||(!chooseMandatorPropertyTree.isLeaf())))) {
                        this.chooseMandator = ((_other.chooseMandator == null)?null:_other.chooseMandator.newCopyBuilder(this, chooseMandatorPropertyTree, _propertyTreeUse));
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends MandatorLinks >_P init(final _P _product) {
            _product.chooseMandator = ((this.chooseMandator == null)?null:this.chooseMandator.build());
            return _product;
        }

        /**
         * Sets the new value of "chooseMandator" (any previous value will be replaced)
         * 
         * @param chooseMandator
         *     New value of the "chooseMandator" property.
         */
        public MandatorLinks.Builder<_B> withChooseMandator(final RestLink chooseMandator) {
            this.chooseMandator = ((chooseMandator == null)?null:new RestLink.Builder<MandatorLinks.Builder<_B>>(this, chooseMandator, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "chooseMandator" property.
         * Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "chooseMandator" property.
         *     Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         */
        public RestLink.Builder<? extends MandatorLinks.Builder<_B>> withChooseMandator() {
            if (this.chooseMandator!= null) {
                return this.chooseMandator;
            }
            return this.chooseMandator = new RestLink.Builder<MandatorLinks.Builder<_B>>(this, null, false);
        }

        @Override
        public MandatorLinks build() {
            if (_storedValue == null) {
                return this.init(new MandatorLinks());
            } else {
                return ((MandatorLinks) _storedValue);
            }
        }

        public MandatorLinks.Builder<_B> copyOf(final MandatorLinks _other) {
            _other.copyTo(this);
            return this;
        }

        public MandatorLinks.Builder<_B> copyOf(final MandatorLinks.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends MandatorLinks.Selector<MandatorLinks.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static MandatorLinks.Select _root() {
            return new MandatorLinks.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private RestLink.Selector<TRoot, MandatorLinks.Selector<TRoot, TParent>> chooseMandator = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.chooseMandator!= null) {
                products.put("chooseMandator", this.chooseMandator.init());
            }
            return products;
        }

        public RestLink.Selector<TRoot, MandatorLinks.Selector<TRoot, TParent>> chooseMandator() {
            return ((this.chooseMandator == null)?this.chooseMandator = new RestLink.Selector<TRoot, MandatorLinks.Selector<TRoot, TParent>>(this._root, this, "chooseMandator"):this.chooseMandator);
        }

    }

}
