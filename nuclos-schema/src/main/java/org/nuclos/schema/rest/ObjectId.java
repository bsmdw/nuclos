
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for object-id complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="object-id"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="long" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="string" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "object-id", propOrder = {
    "_long",
    "string"
})
public class ObjectId implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "long")
    protected Long _long;
    protected String string;

    /**
     * Gets the value of the long property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getLong() {
        return _long;
    }

    /**
     * Sets the value of the long property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setLong(Long value) {
        this._long = value;
    }

    /**
     * Gets the value of the string property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getString() {
        return string;
    }

    /**
     * Sets the value of the string property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setString(String value) {
        this.string = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            Long theLong;
            theLong = this.getLong();
            strategy.appendField(locator, this, "_long", buffer, theLong);
        }
        {
            String theString;
            theString = this.getString();
            strategy.appendField(locator, this, "string", buffer, theString);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ObjectId)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ObjectId that = ((ObjectId) object);
        {
            Long lhsLong;
            lhsLong = this.getLong();
            Long rhsLong;
            rhsLong = that.getLong();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "_long", lhsLong), LocatorUtils.property(thatLocator, "_long", rhsLong), lhsLong, rhsLong)) {
                return false;
            }
        }
        {
            String lhsString;
            lhsString = this.getString();
            String rhsString;
            rhsString = that.getString();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "string", lhsString), LocatorUtils.property(thatLocator, "string", rhsString), lhsString, rhsString)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            Long theLong;
            theLong = this.getLong();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "_long", theLong), currentHashCode, theLong);
        }
        {
            String theString;
            theString = this.getString();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "string", theString), currentHashCode, theString);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof ObjectId) {
            final ObjectId copy = ((ObjectId) draftCopy);
            if (this._long!= null) {
                Long sourceLong;
                sourceLong = this.getLong();
                Long copyLong = ((Long) strategy.copy(LocatorUtils.property(locator, "_long", sourceLong), sourceLong));
                copy.setLong(copyLong);
            } else {
                copy._long = null;
            }
            if (this.string!= null) {
                String sourceString;
                sourceString = this.getString();
                String copyString = ((String) strategy.copy(LocatorUtils.property(locator, "string", sourceString), sourceString));
                copy.setString(copyString);
            } else {
                copy.string = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ObjectId();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final ObjectId.Builder<_B> _other) {
        _other._long = this._long;
        _other.string = this.string;
    }

    public<_B >ObjectId.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new ObjectId.Builder<_B>(_parentBuilder, this, true);
    }

    public ObjectId.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static ObjectId.Builder<Void> builder() {
        return new ObjectId.Builder<Void>(null, null, false);
    }

    public static<_B >ObjectId.Builder<_B> copyOf(final ObjectId _other) {
        final ObjectId.Builder<_B> _newBuilder = new ObjectId.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final ObjectId.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree _longPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("_long"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(_longPropertyTree!= null):((_longPropertyTree == null)||(!_longPropertyTree.isLeaf())))) {
            _other._long = this._long;
        }
        final PropertyTree stringPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("string"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(stringPropertyTree!= null):((stringPropertyTree == null)||(!stringPropertyTree.isLeaf())))) {
            _other.string = this.string;
        }
    }

    public<_B >ObjectId.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new ObjectId.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public ObjectId.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >ObjectId.Builder<_B> copyOf(final ObjectId _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final ObjectId.Builder<_B> _newBuilder = new ObjectId.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static ObjectId.Builder<Void> copyExcept(final ObjectId _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static ObjectId.Builder<Void> copyOnly(final ObjectId _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final ObjectId _storedValue;
        private Long _long;
        private String string;

        public Builder(final _B _parentBuilder, final ObjectId _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this._long = _other._long;
                    this.string = _other.string;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final ObjectId _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree _longPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("_long"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(_longPropertyTree!= null):((_longPropertyTree == null)||(!_longPropertyTree.isLeaf())))) {
                        this._long = _other._long;
                    }
                    final PropertyTree stringPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("string"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(stringPropertyTree!= null):((stringPropertyTree == null)||(!stringPropertyTree.isLeaf())))) {
                        this.string = _other.string;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends ObjectId >_P init(final _P _product) {
            _product._long = this._long;
            _product.string = this.string;
            return _product;
        }

        /**
         * Sets the new value of "_long" (any previous value will be replaced)
         * 
         * @param _long
         *     New value of the "_long" property.
         */
        public ObjectId.Builder<_B> withLong(final Long _long) {
            this._long = _long;
            return this;
        }

        /**
         * Sets the new value of "string" (any previous value will be replaced)
         * 
         * @param string
         *     New value of the "string" property.
         */
        public ObjectId.Builder<_B> withString(final String string) {
            this.string = string;
            return this;
        }

        @Override
        public ObjectId build() {
            if (_storedValue == null) {
                return this.init(new ObjectId());
            } else {
                return ((ObjectId) _storedValue);
            }
        }

        public ObjectId.Builder<_B> copyOf(final ObjectId _other) {
            _other.copyTo(this);
            return this;
        }

        public ObjectId.Builder<_B> copyOf(final ObjectId.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends ObjectId.Selector<ObjectId.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static ObjectId.Select _root() {
            return new ObjectId.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, ObjectId.Selector<TRoot, TParent>> _long = null;
        private com.kscs.util.jaxb.Selector<TRoot, ObjectId.Selector<TRoot, TParent>> string = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this._long!= null) {
                products.put("_long", this._long.init());
            }
            if (this.string!= null) {
                products.put("string", this.string.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, ObjectId.Selector<TRoot, TParent>> _long() {
            return ((this._long == null)?this._long = new com.kscs.util.jaxb.Selector<TRoot, ObjectId.Selector<TRoot, TParent>>(this._root, this, "_long"):this._long);
        }

        public com.kscs.util.jaxb.Selector<TRoot, ObjectId.Selector<TRoot, TParent>> string() {
            return ((this.string == null)?this.string = new com.kscs.util.jaxb.Selector<TRoot, ObjectId.Selector<TRoot, TParent>>(this._root, this, "string"):this.string);
        }

    }

}
