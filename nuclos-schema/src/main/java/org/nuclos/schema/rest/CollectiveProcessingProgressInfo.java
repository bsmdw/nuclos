
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for collective-processing-progress-info complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="collective-processing-progress-info"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="objectInfos" type="{urn:org.nuclos.schema.rest}collective-processing-object-info" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="percent" use="required" type="{http://www.w3.org/2001/XMLSchema}long" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "collective-processing-progress-info", propOrder = {
    "objectInfos"
})
public class CollectiveProcessingProgressInfo implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected List<CollectiveProcessingObjectInfo> objectInfos;
    @XmlAttribute(name = "percent", required = true)
    protected long percent;

    /**
     * Gets the value of the objectInfos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the objectInfos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getObjectInfos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CollectiveProcessingObjectInfo }
     * 
     * 
     */
    public List<CollectiveProcessingObjectInfo> getObjectInfos() {
        if (objectInfos == null) {
            objectInfos = new ArrayList<CollectiveProcessingObjectInfo>();
        }
        return this.objectInfos;
    }

    /**
     * Gets the value of the percent property.
     * 
     */
    public long getPercent() {
        return percent;
    }

    /**
     * Sets the value of the percent property.
     * 
     */
    public void setPercent(long value) {
        this.percent = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<CollectiveProcessingObjectInfo> theObjectInfos;
            theObjectInfos = (((this.objectInfos!= null)&&(!this.objectInfos.isEmpty()))?this.getObjectInfos():null);
            strategy.appendField(locator, this, "objectInfos", buffer, theObjectInfos);
        }
        {
            long thePercent;
            thePercent = this.getPercent();
            strategy.appendField(locator, this, "percent", buffer, thePercent);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof CollectiveProcessingProgressInfo)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final CollectiveProcessingProgressInfo that = ((CollectiveProcessingProgressInfo) object);
        {
            List<CollectiveProcessingObjectInfo> lhsObjectInfos;
            lhsObjectInfos = (((this.objectInfos!= null)&&(!this.objectInfos.isEmpty()))?this.getObjectInfos():null);
            List<CollectiveProcessingObjectInfo> rhsObjectInfos;
            rhsObjectInfos = (((that.objectInfos!= null)&&(!that.objectInfos.isEmpty()))?that.getObjectInfos():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "objectInfos", lhsObjectInfos), LocatorUtils.property(thatLocator, "objectInfos", rhsObjectInfos), lhsObjectInfos, rhsObjectInfos)) {
                return false;
            }
        }
        {
            long lhsPercent;
            lhsPercent = this.getPercent();
            long rhsPercent;
            rhsPercent = that.getPercent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "percent", lhsPercent), LocatorUtils.property(thatLocator, "percent", rhsPercent), lhsPercent, rhsPercent)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<CollectiveProcessingObjectInfo> theObjectInfos;
            theObjectInfos = (((this.objectInfos!= null)&&(!this.objectInfos.isEmpty()))?this.getObjectInfos():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "objectInfos", theObjectInfos), currentHashCode, theObjectInfos);
        }
        {
            long thePercent;
            thePercent = this.getPercent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "percent", thePercent), currentHashCode, thePercent);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof CollectiveProcessingProgressInfo) {
            final CollectiveProcessingProgressInfo copy = ((CollectiveProcessingProgressInfo) draftCopy);
            if ((this.objectInfos!= null)&&(!this.objectInfos.isEmpty())) {
                List<CollectiveProcessingObjectInfo> sourceObjectInfos;
                sourceObjectInfos = (((this.objectInfos!= null)&&(!this.objectInfos.isEmpty()))?this.getObjectInfos():null);
                @SuppressWarnings("unchecked")
                List<CollectiveProcessingObjectInfo> copyObjectInfos = ((List<CollectiveProcessingObjectInfo> ) strategy.copy(LocatorUtils.property(locator, "objectInfos", sourceObjectInfos), sourceObjectInfos));
                copy.objectInfos = null;
                if (copyObjectInfos!= null) {
                    List<CollectiveProcessingObjectInfo> uniqueObjectInfosl = copy.getObjectInfos();
                    uniqueObjectInfosl.addAll(copyObjectInfos);
                }
            } else {
                copy.objectInfos = null;
            }
            {
                long sourcePercent;
                sourcePercent = this.getPercent();
                long copyPercent = strategy.copy(LocatorUtils.property(locator, "percent", sourcePercent), sourcePercent);
                copy.setPercent(copyPercent);
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new CollectiveProcessingProgressInfo();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final CollectiveProcessingProgressInfo.Builder<_B> _other) {
        if (this.objectInfos == null) {
            _other.objectInfos = null;
        } else {
            _other.objectInfos = new ArrayList<CollectiveProcessingObjectInfo.Builder<CollectiveProcessingProgressInfo.Builder<_B>>>();
            for (CollectiveProcessingObjectInfo _item: this.objectInfos) {
                _other.objectInfos.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.percent = this.percent;
    }

    public<_B >CollectiveProcessingProgressInfo.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new CollectiveProcessingProgressInfo.Builder<_B>(_parentBuilder, this, true);
    }

    public CollectiveProcessingProgressInfo.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static CollectiveProcessingProgressInfo.Builder<Void> builder() {
        return new CollectiveProcessingProgressInfo.Builder<Void>(null, null, false);
    }

    public static<_B >CollectiveProcessingProgressInfo.Builder<_B> copyOf(final CollectiveProcessingProgressInfo _other) {
        final CollectiveProcessingProgressInfo.Builder<_B> _newBuilder = new CollectiveProcessingProgressInfo.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final CollectiveProcessingProgressInfo.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree objectInfosPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("objectInfos"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(objectInfosPropertyTree!= null):((objectInfosPropertyTree == null)||(!objectInfosPropertyTree.isLeaf())))) {
            if (this.objectInfos == null) {
                _other.objectInfos = null;
            } else {
                _other.objectInfos = new ArrayList<CollectiveProcessingObjectInfo.Builder<CollectiveProcessingProgressInfo.Builder<_B>>>();
                for (CollectiveProcessingObjectInfo _item: this.objectInfos) {
                    _other.objectInfos.add(((_item == null)?null:_item.newCopyBuilder(_other, objectInfosPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree percentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("percent"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(percentPropertyTree!= null):((percentPropertyTree == null)||(!percentPropertyTree.isLeaf())))) {
            _other.percent = this.percent;
        }
    }

    public<_B >CollectiveProcessingProgressInfo.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new CollectiveProcessingProgressInfo.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public CollectiveProcessingProgressInfo.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >CollectiveProcessingProgressInfo.Builder<_B> copyOf(final CollectiveProcessingProgressInfo _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final CollectiveProcessingProgressInfo.Builder<_B> _newBuilder = new CollectiveProcessingProgressInfo.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static CollectiveProcessingProgressInfo.Builder<Void> copyExcept(final CollectiveProcessingProgressInfo _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static CollectiveProcessingProgressInfo.Builder<Void> copyOnly(final CollectiveProcessingProgressInfo _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final CollectiveProcessingProgressInfo _storedValue;
        private List<CollectiveProcessingObjectInfo.Builder<CollectiveProcessingProgressInfo.Builder<_B>>> objectInfos;
        private long percent;

        public Builder(final _B _parentBuilder, final CollectiveProcessingProgressInfo _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.objectInfos == null) {
                        this.objectInfos = null;
                    } else {
                        this.objectInfos = new ArrayList<CollectiveProcessingObjectInfo.Builder<CollectiveProcessingProgressInfo.Builder<_B>>>();
                        for (CollectiveProcessingObjectInfo _item: _other.objectInfos) {
                            this.objectInfos.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.percent = _other.percent;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final CollectiveProcessingProgressInfo _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree objectInfosPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("objectInfos"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(objectInfosPropertyTree!= null):((objectInfosPropertyTree == null)||(!objectInfosPropertyTree.isLeaf())))) {
                        if (_other.objectInfos == null) {
                            this.objectInfos = null;
                        } else {
                            this.objectInfos = new ArrayList<CollectiveProcessingObjectInfo.Builder<CollectiveProcessingProgressInfo.Builder<_B>>>();
                            for (CollectiveProcessingObjectInfo _item: _other.objectInfos) {
                                this.objectInfos.add(((_item == null)?null:_item.newCopyBuilder(this, objectInfosPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree percentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("percent"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(percentPropertyTree!= null):((percentPropertyTree == null)||(!percentPropertyTree.isLeaf())))) {
                        this.percent = _other.percent;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends CollectiveProcessingProgressInfo >_P init(final _P _product) {
            if (this.objectInfos!= null) {
                final List<CollectiveProcessingObjectInfo> objectInfos = new ArrayList<CollectiveProcessingObjectInfo>(this.objectInfos.size());
                for (CollectiveProcessingObjectInfo.Builder<CollectiveProcessingProgressInfo.Builder<_B>> _item: this.objectInfos) {
                    objectInfos.add(_item.build());
                }
                _product.objectInfos = objectInfos;
            }
            _product.percent = this.percent;
            return _product;
        }

        /**
         * Adds the given items to the value of "objectInfos"
         * 
         * @param objectInfos
         *     Items to add to the value of the "objectInfos" property
         */
        public CollectiveProcessingProgressInfo.Builder<_B> addObjectInfos(final Iterable<? extends CollectiveProcessingObjectInfo> objectInfos) {
            if (objectInfos!= null) {
                if (this.objectInfos == null) {
                    this.objectInfos = new ArrayList<CollectiveProcessingObjectInfo.Builder<CollectiveProcessingProgressInfo.Builder<_B>>>();
                }
                for (CollectiveProcessingObjectInfo _item: objectInfos) {
                    this.objectInfos.add(new CollectiveProcessingObjectInfo.Builder<CollectiveProcessingProgressInfo.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "objectInfos" (any previous value will be replaced)
         * 
         * @param objectInfos
         *     New value of the "objectInfos" property.
         */
        public CollectiveProcessingProgressInfo.Builder<_B> withObjectInfos(final Iterable<? extends CollectiveProcessingObjectInfo> objectInfos) {
            if (this.objectInfos!= null) {
                this.objectInfos.clear();
            }
            return addObjectInfos(objectInfos);
        }

        /**
         * Adds the given items to the value of "objectInfos"
         * 
         * @param objectInfos
         *     Items to add to the value of the "objectInfos" property
         */
        public CollectiveProcessingProgressInfo.Builder<_B> addObjectInfos(CollectiveProcessingObjectInfo... objectInfos) {
            addObjectInfos(Arrays.asList(objectInfos));
            return this;
        }

        /**
         * Sets the new value of "objectInfos" (any previous value will be replaced)
         * 
         * @param objectInfos
         *     New value of the "objectInfos" property.
         */
        public CollectiveProcessingProgressInfo.Builder<_B> withObjectInfos(CollectiveProcessingObjectInfo... objectInfos) {
            withObjectInfos(Arrays.asList(objectInfos));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "ObjectInfos" property.
         * Use {@link org.nuclos.schema.rest.CollectiveProcessingObjectInfo.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "ObjectInfos" property.
         *     Use {@link org.nuclos.schema.rest.CollectiveProcessingObjectInfo.Builder#end()} to return to the current builder.
         */
        public CollectiveProcessingObjectInfo.Builder<? extends CollectiveProcessingProgressInfo.Builder<_B>> addObjectInfos() {
            if (this.objectInfos == null) {
                this.objectInfos = new ArrayList<CollectiveProcessingObjectInfo.Builder<CollectiveProcessingProgressInfo.Builder<_B>>>();
            }
            final CollectiveProcessingObjectInfo.Builder<CollectiveProcessingProgressInfo.Builder<_B>> objectInfos_Builder = new CollectiveProcessingObjectInfo.Builder<CollectiveProcessingProgressInfo.Builder<_B>>(this, null, false);
            this.objectInfos.add(objectInfos_Builder);
            return objectInfos_Builder;
        }

        /**
         * Sets the new value of "percent" (any previous value will be replaced)
         * 
         * @param percent
         *     New value of the "percent" property.
         */
        public CollectiveProcessingProgressInfo.Builder<_B> withPercent(final long percent) {
            this.percent = percent;
            return this;
        }

        @Override
        public CollectiveProcessingProgressInfo build() {
            if (_storedValue == null) {
                return this.init(new CollectiveProcessingProgressInfo());
            } else {
                return ((CollectiveProcessingProgressInfo) _storedValue);
            }
        }

        public CollectiveProcessingProgressInfo.Builder<_B> copyOf(final CollectiveProcessingProgressInfo _other) {
            _other.copyTo(this);
            return this;
        }

        public CollectiveProcessingProgressInfo.Builder<_B> copyOf(final CollectiveProcessingProgressInfo.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends CollectiveProcessingProgressInfo.Selector<CollectiveProcessingProgressInfo.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static CollectiveProcessingProgressInfo.Select _root() {
            return new CollectiveProcessingProgressInfo.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private CollectiveProcessingObjectInfo.Selector<TRoot, CollectiveProcessingProgressInfo.Selector<TRoot, TParent>> objectInfos = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.objectInfos!= null) {
                products.put("objectInfos", this.objectInfos.init());
            }
            return products;
        }

        public CollectiveProcessingObjectInfo.Selector<TRoot, CollectiveProcessingProgressInfo.Selector<TRoot, TParent>> objectInfos() {
            return ((this.objectInfos == null)?this.objectInfos = new CollectiveProcessingObjectInfo.Selector<TRoot, CollectiveProcessingProgressInfo.Selector<TRoot, TParent>>(this._root, this, "objectInfos"):this.objectInfos);
        }

    }

}
