
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;


/**
 * <p>Java class for searchfilter-info complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="searchfilter-info"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="pk" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="filterName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="default" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="order" type="{http://www.w3.org/2001/XMLSchema}long" /&gt;
 *       &lt;attribute name="icon" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "searchfilter-info")
public class SearchfilterInfo implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "pk")
    protected String pk;
    @XmlAttribute(name = "filterName")
    protected String filterName;
    @XmlAttribute(name = "default")
    protected Boolean _default;
    @XmlAttribute(name = "order")
    protected Long order;
    @XmlAttribute(name = "icon")
    protected String icon;

    /**
     * Gets the value of the pk property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPk() {
        return pk;
    }

    /**
     * Sets the value of the pk property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPk(String value) {
        this.pk = value;
    }

    /**
     * Gets the value of the filterName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFilterName() {
        return filterName;
    }

    /**
     * Sets the value of the filterName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFilterName(String value) {
        this.filterName = value;
    }

    /**
     * Gets the value of the default property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDefault() {
        return _default;
    }

    /**
     * Sets the value of the default property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDefault(Boolean value) {
        this._default = value;
    }

    /**
     * Gets the value of the order property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getOrder() {
        return order;
    }

    /**
     * Sets the value of the order property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setOrder(Long value) {
        this.order = value;
    }

    /**
     * Gets the value of the icon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIcon() {
        return icon;
    }

    /**
     * Sets the value of the icon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIcon(String value) {
        this.icon = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String thePk;
            thePk = this.getPk();
            strategy.appendField(locator, this, "pk", buffer, thePk);
        }
        {
            String theFilterName;
            theFilterName = this.getFilterName();
            strategy.appendField(locator, this, "filterName", buffer, theFilterName);
        }
        {
            Boolean theDefault;
            theDefault = this.isDefault();
            strategy.appendField(locator, this, "_default", buffer, theDefault);
        }
        {
            Long theOrder;
            theOrder = this.getOrder();
            strategy.appendField(locator, this, "order", buffer, theOrder);
        }
        {
            String theIcon;
            theIcon = this.getIcon();
            strategy.appendField(locator, this, "icon", buffer, theIcon);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof SearchfilterInfo)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final SearchfilterInfo that = ((SearchfilterInfo) object);
        {
            String lhsPk;
            lhsPk = this.getPk();
            String rhsPk;
            rhsPk = that.getPk();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "pk", lhsPk), LocatorUtils.property(thatLocator, "pk", rhsPk), lhsPk, rhsPk)) {
                return false;
            }
        }
        {
            String lhsFilterName;
            lhsFilterName = this.getFilterName();
            String rhsFilterName;
            rhsFilterName = that.getFilterName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "filterName", lhsFilterName), LocatorUtils.property(thatLocator, "filterName", rhsFilterName), lhsFilterName, rhsFilterName)) {
                return false;
            }
        }
        {
            Boolean lhsDefault;
            lhsDefault = this.isDefault();
            Boolean rhsDefault;
            rhsDefault = that.isDefault();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "_default", lhsDefault), LocatorUtils.property(thatLocator, "_default", rhsDefault), lhsDefault, rhsDefault)) {
                return false;
            }
        }
        {
            Long lhsOrder;
            lhsOrder = this.getOrder();
            Long rhsOrder;
            rhsOrder = that.getOrder();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "order", lhsOrder), LocatorUtils.property(thatLocator, "order", rhsOrder), lhsOrder, rhsOrder)) {
                return false;
            }
        }
        {
            String lhsIcon;
            lhsIcon = this.getIcon();
            String rhsIcon;
            rhsIcon = that.getIcon();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "icon", lhsIcon), LocatorUtils.property(thatLocator, "icon", rhsIcon), lhsIcon, rhsIcon)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String thePk;
            thePk = this.getPk();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "pk", thePk), currentHashCode, thePk);
        }
        {
            String theFilterName;
            theFilterName = this.getFilterName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "filterName", theFilterName), currentHashCode, theFilterName);
        }
        {
            Boolean theDefault;
            theDefault = this.isDefault();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "_default", theDefault), currentHashCode, theDefault);
        }
        {
            Long theOrder;
            theOrder = this.getOrder();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "order", theOrder), currentHashCode, theOrder);
        }
        {
            String theIcon;
            theIcon = this.getIcon();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "icon", theIcon), currentHashCode, theIcon);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof SearchfilterInfo) {
            final SearchfilterInfo copy = ((SearchfilterInfo) draftCopy);
            if (this.pk!= null) {
                String sourcePk;
                sourcePk = this.getPk();
                String copyPk = ((String) strategy.copy(LocatorUtils.property(locator, "pk", sourcePk), sourcePk));
                copy.setPk(copyPk);
            } else {
                copy.pk = null;
            }
            if (this.filterName!= null) {
                String sourceFilterName;
                sourceFilterName = this.getFilterName();
                String copyFilterName = ((String) strategy.copy(LocatorUtils.property(locator, "filterName", sourceFilterName), sourceFilterName));
                copy.setFilterName(copyFilterName);
            } else {
                copy.filterName = null;
            }
            if (this._default!= null) {
                Boolean sourceDefault;
                sourceDefault = this.isDefault();
                Boolean copyDefault = ((Boolean) strategy.copy(LocatorUtils.property(locator, "_default", sourceDefault), sourceDefault));
                copy.setDefault(copyDefault);
            } else {
                copy._default = null;
            }
            if (this.order!= null) {
                Long sourceOrder;
                sourceOrder = this.getOrder();
                Long copyOrder = ((Long) strategy.copy(LocatorUtils.property(locator, "order", sourceOrder), sourceOrder));
                copy.setOrder(copyOrder);
            } else {
                copy.order = null;
            }
            if (this.icon!= null) {
                String sourceIcon;
                sourceIcon = this.getIcon();
                String copyIcon = ((String) strategy.copy(LocatorUtils.property(locator, "icon", sourceIcon), sourceIcon));
                copy.setIcon(copyIcon);
            } else {
                copy.icon = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new SearchfilterInfo();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final SearchfilterInfo.Builder<_B> _other) {
        _other.pk = this.pk;
        _other.filterName = this.filterName;
        _other._default = this._default;
        _other.order = this.order;
        _other.icon = this.icon;
    }

    public<_B >SearchfilterInfo.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new SearchfilterInfo.Builder<_B>(_parentBuilder, this, true);
    }

    public SearchfilterInfo.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static SearchfilterInfo.Builder<Void> builder() {
        return new SearchfilterInfo.Builder<Void>(null, null, false);
    }

    public static<_B >SearchfilterInfo.Builder<_B> copyOf(final SearchfilterInfo _other) {
        final SearchfilterInfo.Builder<_B> _newBuilder = new SearchfilterInfo.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final SearchfilterInfo.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree pkPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("pk"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(pkPropertyTree!= null):((pkPropertyTree == null)||(!pkPropertyTree.isLeaf())))) {
            _other.pk = this.pk;
        }
        final PropertyTree filterNamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("filterName"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(filterNamePropertyTree!= null):((filterNamePropertyTree == null)||(!filterNamePropertyTree.isLeaf())))) {
            _other.filterName = this.filterName;
        }
        final PropertyTree _defaultPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("_default"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(_defaultPropertyTree!= null):((_defaultPropertyTree == null)||(!_defaultPropertyTree.isLeaf())))) {
            _other._default = this._default;
        }
        final PropertyTree orderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("order"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(orderPropertyTree!= null):((orderPropertyTree == null)||(!orderPropertyTree.isLeaf())))) {
            _other.order = this.order;
        }
        final PropertyTree iconPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("icon"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(iconPropertyTree!= null):((iconPropertyTree == null)||(!iconPropertyTree.isLeaf())))) {
            _other.icon = this.icon;
        }
    }

    public<_B >SearchfilterInfo.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new SearchfilterInfo.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public SearchfilterInfo.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >SearchfilterInfo.Builder<_B> copyOf(final SearchfilterInfo _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final SearchfilterInfo.Builder<_B> _newBuilder = new SearchfilterInfo.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static SearchfilterInfo.Builder<Void> copyExcept(final SearchfilterInfo _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static SearchfilterInfo.Builder<Void> copyOnly(final SearchfilterInfo _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final SearchfilterInfo _storedValue;
        private String pk;
        private String filterName;
        private Boolean _default;
        private Long order;
        private String icon;

        public Builder(final _B _parentBuilder, final SearchfilterInfo _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.pk = _other.pk;
                    this.filterName = _other.filterName;
                    this._default = _other._default;
                    this.order = _other.order;
                    this.icon = _other.icon;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final SearchfilterInfo _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree pkPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("pk"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(pkPropertyTree!= null):((pkPropertyTree == null)||(!pkPropertyTree.isLeaf())))) {
                        this.pk = _other.pk;
                    }
                    final PropertyTree filterNamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("filterName"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(filterNamePropertyTree!= null):((filterNamePropertyTree == null)||(!filterNamePropertyTree.isLeaf())))) {
                        this.filterName = _other.filterName;
                    }
                    final PropertyTree _defaultPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("_default"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(_defaultPropertyTree!= null):((_defaultPropertyTree == null)||(!_defaultPropertyTree.isLeaf())))) {
                        this._default = _other._default;
                    }
                    final PropertyTree orderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("order"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(orderPropertyTree!= null):((orderPropertyTree == null)||(!orderPropertyTree.isLeaf())))) {
                        this.order = _other.order;
                    }
                    final PropertyTree iconPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("icon"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(iconPropertyTree!= null):((iconPropertyTree == null)||(!iconPropertyTree.isLeaf())))) {
                        this.icon = _other.icon;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends SearchfilterInfo >_P init(final _P _product) {
            _product.pk = this.pk;
            _product.filterName = this.filterName;
            _product._default = this._default;
            _product.order = this.order;
            _product.icon = this.icon;
            return _product;
        }

        /**
         * Sets the new value of "pk" (any previous value will be replaced)
         * 
         * @param pk
         *     New value of the "pk" property.
         */
        public SearchfilterInfo.Builder<_B> withPk(final String pk) {
            this.pk = pk;
            return this;
        }

        /**
         * Sets the new value of "filterName" (any previous value will be replaced)
         * 
         * @param filterName
         *     New value of the "filterName" property.
         */
        public SearchfilterInfo.Builder<_B> withFilterName(final String filterName) {
            this.filterName = filterName;
            return this;
        }

        /**
         * Sets the new value of "_default" (any previous value will be replaced)
         * 
         * @param _default
         *     New value of the "_default" property.
         */
        public SearchfilterInfo.Builder<_B> withDefault(final Boolean _default) {
            this._default = _default;
            return this;
        }

        /**
         * Sets the new value of "order" (any previous value will be replaced)
         * 
         * @param order
         *     New value of the "order" property.
         */
        public SearchfilterInfo.Builder<_B> withOrder(final Long order) {
            this.order = order;
            return this;
        }

        /**
         * Sets the new value of "icon" (any previous value will be replaced)
         * 
         * @param icon
         *     New value of the "icon" property.
         */
        public SearchfilterInfo.Builder<_B> withIcon(final String icon) {
            this.icon = icon;
            return this;
        }

        @Override
        public SearchfilterInfo build() {
            if (_storedValue == null) {
                return this.init(new SearchfilterInfo());
            } else {
                return ((SearchfilterInfo) _storedValue);
            }
        }

        public SearchfilterInfo.Builder<_B> copyOf(final SearchfilterInfo _other) {
            _other.copyTo(this);
            return this;
        }

        public SearchfilterInfo.Builder<_B> copyOf(final SearchfilterInfo.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends SearchfilterInfo.Selector<SearchfilterInfo.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static SearchfilterInfo.Select _root() {
            return new SearchfilterInfo.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, SearchfilterInfo.Selector<TRoot, TParent>> pk = null;
        private com.kscs.util.jaxb.Selector<TRoot, SearchfilterInfo.Selector<TRoot, TParent>> filterName = null;
        private com.kscs.util.jaxb.Selector<TRoot, SearchfilterInfo.Selector<TRoot, TParent>> _default = null;
        private com.kscs.util.jaxb.Selector<TRoot, SearchfilterInfo.Selector<TRoot, TParent>> order = null;
        private com.kscs.util.jaxb.Selector<TRoot, SearchfilterInfo.Selector<TRoot, TParent>> icon = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.pk!= null) {
                products.put("pk", this.pk.init());
            }
            if (this.filterName!= null) {
                products.put("filterName", this.filterName.init());
            }
            if (this._default!= null) {
                products.put("_default", this._default.init());
            }
            if (this.order!= null) {
                products.put("order", this.order.init());
            }
            if (this.icon!= null) {
                products.put("icon", this.icon.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, SearchfilterInfo.Selector<TRoot, TParent>> pk() {
            return ((this.pk == null)?this.pk = new com.kscs.util.jaxb.Selector<TRoot, SearchfilterInfo.Selector<TRoot, TParent>>(this._root, this, "pk"):this.pk);
        }

        public com.kscs.util.jaxb.Selector<TRoot, SearchfilterInfo.Selector<TRoot, TParent>> filterName() {
            return ((this.filterName == null)?this.filterName = new com.kscs.util.jaxb.Selector<TRoot, SearchfilterInfo.Selector<TRoot, TParent>>(this._root, this, "filterName"):this.filterName);
        }

        public com.kscs.util.jaxb.Selector<TRoot, SearchfilterInfo.Selector<TRoot, TParent>> _default() {
            return ((this._default == null)?this._default = new com.kscs.util.jaxb.Selector<TRoot, SearchfilterInfo.Selector<TRoot, TParent>>(this._root, this, "_default"):this._default);
        }

        public com.kscs.util.jaxb.Selector<TRoot, SearchfilterInfo.Selector<TRoot, TParent>> order() {
            return ((this.order == null)?this.order = new com.kscs.util.jaxb.Selector<TRoot, SearchfilterInfo.Selector<TRoot, TParent>>(this._root, this, "order"):this.order);
        }

        public com.kscs.util.jaxb.Selector<TRoot, SearchfilterInfo.Selector<TRoot, TParent>> icon() {
            return ((this.icon == null)?this.icon = new com.kscs.util.jaxb.Selector<TRoot, SearchfilterInfo.Selector<TRoot, TParent>>(this._root, this, "icon"):this.icon);
        }

    }

}
