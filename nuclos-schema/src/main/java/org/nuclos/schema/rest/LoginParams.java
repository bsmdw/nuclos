
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for login-params complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="login-params"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="username" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="password" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="datalanguage" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="locale" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="autologin" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "login-params")
public class LoginParams implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "username", required = true)
    protected String username;
    @XmlAttribute(name = "password")
    protected String password;
    @XmlAttribute(name = "datalanguage")
    protected String datalanguage;
    @XmlAttribute(name = "locale")
    protected String locale;
    @XmlAttribute(name = "autologin")
    protected Boolean autologin;

    /**
     * Gets the value of the username property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the value of the username property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsername(String value) {
        this.username = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * Gets the value of the datalanguage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDatalanguage() {
        return datalanguage;
    }

    /**
     * Sets the value of the datalanguage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDatalanguage(String value) {
        this.datalanguage = value;
    }

    /**
     * Gets the value of the locale property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocale() {
        return locale;
    }

    /**
     * Sets the value of the locale property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocale(String value) {
        this.locale = value;
    }

    /**
     * Gets the value of the autologin property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isAutologin() {
        if (autologin == null) {
            return false;
        } else {
            return autologin;
        }
    }

    /**
     * Sets the value of the autologin property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutologin(Boolean value) {
        this.autologin = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theUsername;
            theUsername = this.getUsername();
            strategy.appendField(locator, this, "username", buffer, theUsername);
        }
        {
            String thePassword;
            thePassword = this.getPassword();
            strategy.appendField(locator, this, "password", buffer, thePassword);
        }
        {
            String theDatalanguage;
            theDatalanguage = this.getDatalanguage();
            strategy.appendField(locator, this, "datalanguage", buffer, theDatalanguage);
        }
        {
            String theLocale;
            theLocale = this.getLocale();
            strategy.appendField(locator, this, "locale", buffer, theLocale);
        }
        {
            boolean theAutologin;
            theAutologin = ((this.autologin!= null)?this.isAutologin():false);
            strategy.appendField(locator, this, "autologin", buffer, theAutologin);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof LoginParams)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final LoginParams that = ((LoginParams) object);
        {
            String lhsUsername;
            lhsUsername = this.getUsername();
            String rhsUsername;
            rhsUsername = that.getUsername();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "username", lhsUsername), LocatorUtils.property(thatLocator, "username", rhsUsername), lhsUsername, rhsUsername)) {
                return false;
            }
        }
        {
            String lhsPassword;
            lhsPassword = this.getPassword();
            String rhsPassword;
            rhsPassword = that.getPassword();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "password", lhsPassword), LocatorUtils.property(thatLocator, "password", rhsPassword), lhsPassword, rhsPassword)) {
                return false;
            }
        }
        {
            String lhsDatalanguage;
            lhsDatalanguage = this.getDatalanguage();
            String rhsDatalanguage;
            rhsDatalanguage = that.getDatalanguage();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "datalanguage", lhsDatalanguage), LocatorUtils.property(thatLocator, "datalanguage", rhsDatalanguage), lhsDatalanguage, rhsDatalanguage)) {
                return false;
            }
        }
        {
            String lhsLocale;
            lhsLocale = this.getLocale();
            String rhsLocale;
            rhsLocale = that.getLocale();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "locale", lhsLocale), LocatorUtils.property(thatLocator, "locale", rhsLocale), lhsLocale, rhsLocale)) {
                return false;
            }
        }
        {
            boolean lhsAutologin;
            lhsAutologin = ((this.autologin!= null)?this.isAutologin():false);
            boolean rhsAutologin;
            rhsAutologin = ((that.autologin!= null)?that.isAutologin():false);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "autologin", lhsAutologin), LocatorUtils.property(thatLocator, "autologin", rhsAutologin), lhsAutologin, rhsAutologin)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theUsername;
            theUsername = this.getUsername();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "username", theUsername), currentHashCode, theUsername);
        }
        {
            String thePassword;
            thePassword = this.getPassword();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "password", thePassword), currentHashCode, thePassword);
        }
        {
            String theDatalanguage;
            theDatalanguage = this.getDatalanguage();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "datalanguage", theDatalanguage), currentHashCode, theDatalanguage);
        }
        {
            String theLocale;
            theLocale = this.getLocale();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "locale", theLocale), currentHashCode, theLocale);
        }
        {
            boolean theAutologin;
            theAutologin = ((this.autologin!= null)?this.isAutologin():false);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "autologin", theAutologin), currentHashCode, theAutologin);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof LoginParams) {
            final LoginParams copy = ((LoginParams) draftCopy);
            if (this.username!= null) {
                String sourceUsername;
                sourceUsername = this.getUsername();
                String copyUsername = ((String) strategy.copy(LocatorUtils.property(locator, "username", sourceUsername), sourceUsername));
                copy.setUsername(copyUsername);
            } else {
                copy.username = null;
            }
            if (this.password!= null) {
                String sourcePassword;
                sourcePassword = this.getPassword();
                String copyPassword = ((String) strategy.copy(LocatorUtils.property(locator, "password", sourcePassword), sourcePassword));
                copy.setPassword(copyPassword);
            } else {
                copy.password = null;
            }
            if (this.datalanguage!= null) {
                String sourceDatalanguage;
                sourceDatalanguage = this.getDatalanguage();
                String copyDatalanguage = ((String) strategy.copy(LocatorUtils.property(locator, "datalanguage", sourceDatalanguage), sourceDatalanguage));
                copy.setDatalanguage(copyDatalanguage);
            } else {
                copy.datalanguage = null;
            }
            if (this.locale!= null) {
                String sourceLocale;
                sourceLocale = this.getLocale();
                String copyLocale = ((String) strategy.copy(LocatorUtils.property(locator, "locale", sourceLocale), sourceLocale));
                copy.setLocale(copyLocale);
            } else {
                copy.locale = null;
            }
            if (this.autologin!= null) {
                boolean sourceAutologin;
                sourceAutologin = ((this.autologin!= null)?this.isAutologin():false);
                boolean copyAutologin = strategy.copy(LocatorUtils.property(locator, "autologin", sourceAutologin), sourceAutologin);
                copy.setAutologin(copyAutologin);
            } else {
                copy.autologin = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new LoginParams();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final LoginParams.Builder<_B> _other) {
        _other.username = this.username;
        _other.password = this.password;
        _other.datalanguage = this.datalanguage;
        _other.locale = this.locale;
        _other.autologin = this.autologin;
    }

    public<_B >LoginParams.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new LoginParams.Builder<_B>(_parentBuilder, this, true);
    }

    public LoginParams.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static LoginParams.Builder<Void> builder() {
        return new LoginParams.Builder<Void>(null, null, false);
    }

    public static<_B >LoginParams.Builder<_B> copyOf(final LoginParams _other) {
        final LoginParams.Builder<_B> _newBuilder = new LoginParams.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final LoginParams.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree usernamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("username"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(usernamePropertyTree!= null):((usernamePropertyTree == null)||(!usernamePropertyTree.isLeaf())))) {
            _other.username = this.username;
        }
        final PropertyTree passwordPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("password"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(passwordPropertyTree!= null):((passwordPropertyTree == null)||(!passwordPropertyTree.isLeaf())))) {
            _other.password = this.password;
        }
        final PropertyTree datalanguagePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("datalanguage"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(datalanguagePropertyTree!= null):((datalanguagePropertyTree == null)||(!datalanguagePropertyTree.isLeaf())))) {
            _other.datalanguage = this.datalanguage;
        }
        final PropertyTree localePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("locale"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(localePropertyTree!= null):((localePropertyTree == null)||(!localePropertyTree.isLeaf())))) {
            _other.locale = this.locale;
        }
        final PropertyTree autologinPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("autologin"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(autologinPropertyTree!= null):((autologinPropertyTree == null)||(!autologinPropertyTree.isLeaf())))) {
            _other.autologin = this.autologin;
        }
    }

    public<_B >LoginParams.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new LoginParams.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public LoginParams.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >LoginParams.Builder<_B> copyOf(final LoginParams _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final LoginParams.Builder<_B> _newBuilder = new LoginParams.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static LoginParams.Builder<Void> copyExcept(final LoginParams _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static LoginParams.Builder<Void> copyOnly(final LoginParams _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final LoginParams _storedValue;
        private String username;
        private String password;
        private String datalanguage;
        private String locale;
        private Boolean autologin;

        public Builder(final _B _parentBuilder, final LoginParams _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.username = _other.username;
                    this.password = _other.password;
                    this.datalanguage = _other.datalanguage;
                    this.locale = _other.locale;
                    this.autologin = _other.autologin;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final LoginParams _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree usernamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("username"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(usernamePropertyTree!= null):((usernamePropertyTree == null)||(!usernamePropertyTree.isLeaf())))) {
                        this.username = _other.username;
                    }
                    final PropertyTree passwordPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("password"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(passwordPropertyTree!= null):((passwordPropertyTree == null)||(!passwordPropertyTree.isLeaf())))) {
                        this.password = _other.password;
                    }
                    final PropertyTree datalanguagePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("datalanguage"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(datalanguagePropertyTree!= null):((datalanguagePropertyTree == null)||(!datalanguagePropertyTree.isLeaf())))) {
                        this.datalanguage = _other.datalanguage;
                    }
                    final PropertyTree localePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("locale"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(localePropertyTree!= null):((localePropertyTree == null)||(!localePropertyTree.isLeaf())))) {
                        this.locale = _other.locale;
                    }
                    final PropertyTree autologinPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("autologin"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(autologinPropertyTree!= null):((autologinPropertyTree == null)||(!autologinPropertyTree.isLeaf())))) {
                        this.autologin = _other.autologin;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends LoginParams >_P init(final _P _product) {
            _product.username = this.username;
            _product.password = this.password;
            _product.datalanguage = this.datalanguage;
            _product.locale = this.locale;
            _product.autologin = this.autologin;
            return _product;
        }

        /**
         * Sets the new value of "username" (any previous value will be replaced)
         * 
         * @param username
         *     New value of the "username" property.
         */
        public LoginParams.Builder<_B> withUsername(final String username) {
            this.username = username;
            return this;
        }

        /**
         * Sets the new value of "password" (any previous value will be replaced)
         * 
         * @param password
         *     New value of the "password" property.
         */
        public LoginParams.Builder<_B> withPassword(final String password) {
            this.password = password;
            return this;
        }

        /**
         * Sets the new value of "datalanguage" (any previous value will be replaced)
         * 
         * @param datalanguage
         *     New value of the "datalanguage" property.
         */
        public LoginParams.Builder<_B> withDatalanguage(final String datalanguage) {
            this.datalanguage = datalanguage;
            return this;
        }

        /**
         * Sets the new value of "locale" (any previous value will be replaced)
         * 
         * @param locale
         *     New value of the "locale" property.
         */
        public LoginParams.Builder<_B> withLocale(final String locale) {
            this.locale = locale;
            return this;
        }

        /**
         * Sets the new value of "autologin" (any previous value will be replaced)
         * 
         * @param autologin
         *     New value of the "autologin" property.
         */
        public LoginParams.Builder<_B> withAutologin(final Boolean autologin) {
            this.autologin = autologin;
            return this;
        }

        @Override
        public LoginParams build() {
            if (_storedValue == null) {
                return this.init(new LoginParams());
            } else {
                return ((LoginParams) _storedValue);
            }
        }

        public LoginParams.Builder<_B> copyOf(final LoginParams _other) {
            _other.copyTo(this);
            return this;
        }

        public LoginParams.Builder<_B> copyOf(final LoginParams.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends LoginParams.Selector<LoginParams.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static LoginParams.Select _root() {
            return new LoginParams.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, LoginParams.Selector<TRoot, TParent>> username = null;
        private com.kscs.util.jaxb.Selector<TRoot, LoginParams.Selector<TRoot, TParent>> password = null;
        private com.kscs.util.jaxb.Selector<TRoot, LoginParams.Selector<TRoot, TParent>> datalanguage = null;
        private com.kscs.util.jaxb.Selector<TRoot, LoginParams.Selector<TRoot, TParent>> locale = null;
        private com.kscs.util.jaxb.Selector<TRoot, LoginParams.Selector<TRoot, TParent>> autologin = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.username!= null) {
                products.put("username", this.username.init());
            }
            if (this.password!= null) {
                products.put("password", this.password.init());
            }
            if (this.datalanguage!= null) {
                products.put("datalanguage", this.datalanguage.init());
            }
            if (this.locale!= null) {
                products.put("locale", this.locale.init());
            }
            if (this.autologin!= null) {
                products.put("autologin", this.autologin.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, LoginParams.Selector<TRoot, TParent>> username() {
            return ((this.username == null)?this.username = new com.kscs.util.jaxb.Selector<TRoot, LoginParams.Selector<TRoot, TParent>>(this._root, this, "username"):this.username);
        }

        public com.kscs.util.jaxb.Selector<TRoot, LoginParams.Selector<TRoot, TParent>> password() {
            return ((this.password == null)?this.password = new com.kscs.util.jaxb.Selector<TRoot, LoginParams.Selector<TRoot, TParent>>(this._root, this, "password"):this.password);
        }

        public com.kscs.util.jaxb.Selector<TRoot, LoginParams.Selector<TRoot, TParent>> datalanguage() {
            return ((this.datalanguage == null)?this.datalanguage = new com.kscs.util.jaxb.Selector<TRoot, LoginParams.Selector<TRoot, TParent>>(this._root, this, "datalanguage"):this.datalanguage);
        }

        public com.kscs.util.jaxb.Selector<TRoot, LoginParams.Selector<TRoot, TParent>> locale() {
            return ((this.locale == null)?this.locale = new com.kscs.util.jaxb.Selector<TRoot, LoginParams.Selector<TRoot, TParent>>(this._root, this, "locale"):this.locale);
        }

        public com.kscs.util.jaxb.Selector<TRoot, LoginParams.Selector<TRoot, TParent>> autologin() {
            return ((this.autologin == null)?this.autologin = new com.kscs.util.jaxb.Selector<TRoot, LoginParams.Selector<TRoot, TParent>>(this._root, this, "autologin"):this.autologin);
        }

    }

}
