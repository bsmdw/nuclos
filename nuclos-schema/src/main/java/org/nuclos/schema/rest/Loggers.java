
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;


/**
 * <p>Java class for loggers complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="loggers"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="logger" type="{urn:org.nuclos.schema.rest}logger" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "loggers", propOrder = {
    "logger"
})
public class Loggers implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected List<Logger> logger;

    /**
     * Gets the value of the logger property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the logger property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLogger().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Logger }
     * 
     * 
     */
    public List<Logger> getLogger() {
        if (logger == null) {
            logger = new ArrayList<Logger>();
        }
        return this.logger;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<Logger> theLogger;
            theLogger = (((this.logger!= null)&&(!this.logger.isEmpty()))?this.getLogger():null);
            strategy.appendField(locator, this, "logger", buffer, theLogger);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Loggers)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Loggers that = ((Loggers) object);
        {
            List<Logger> lhsLogger;
            lhsLogger = (((this.logger!= null)&&(!this.logger.isEmpty()))?this.getLogger():null);
            List<Logger> rhsLogger;
            rhsLogger = (((that.logger!= null)&&(!that.logger.isEmpty()))?that.getLogger():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "logger", lhsLogger), LocatorUtils.property(thatLocator, "logger", rhsLogger), lhsLogger, rhsLogger)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<Logger> theLogger;
            theLogger = (((this.logger!= null)&&(!this.logger.isEmpty()))?this.getLogger():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "logger", theLogger), currentHashCode, theLogger);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Loggers) {
            final Loggers copy = ((Loggers) draftCopy);
            if ((this.logger!= null)&&(!this.logger.isEmpty())) {
                List<Logger> sourceLogger;
                sourceLogger = (((this.logger!= null)&&(!this.logger.isEmpty()))?this.getLogger():null);
                @SuppressWarnings("unchecked")
                List<Logger> copyLogger = ((List<Logger> ) strategy.copy(LocatorUtils.property(locator, "logger", sourceLogger), sourceLogger));
                copy.logger = null;
                if (copyLogger!= null) {
                    List<Logger> uniqueLoggerl = copy.getLogger();
                    uniqueLoggerl.addAll(copyLogger);
                }
            } else {
                copy.logger = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Loggers();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Loggers.Builder<_B> _other) {
        if (this.logger == null) {
            _other.logger = null;
        } else {
            _other.logger = new ArrayList<Logger.Builder<Loggers.Builder<_B>>>();
            for (Logger _item: this.logger) {
                _other.logger.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
    }

    public<_B >Loggers.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Loggers.Builder<_B>(_parentBuilder, this, true);
    }

    public Loggers.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Loggers.Builder<Void> builder() {
        return new Loggers.Builder<Void>(null, null, false);
    }

    public static<_B >Loggers.Builder<_B> copyOf(final Loggers _other) {
        final Loggers.Builder<_B> _newBuilder = new Loggers.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Loggers.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree loggerPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("logger"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(loggerPropertyTree!= null):((loggerPropertyTree == null)||(!loggerPropertyTree.isLeaf())))) {
            if (this.logger == null) {
                _other.logger = null;
            } else {
                _other.logger = new ArrayList<Logger.Builder<Loggers.Builder<_B>>>();
                for (Logger _item: this.logger) {
                    _other.logger.add(((_item == null)?null:_item.newCopyBuilder(_other, loggerPropertyTree, _propertyTreeUse)));
                }
            }
        }
    }

    public<_B >Loggers.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Loggers.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Loggers.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Loggers.Builder<_B> copyOf(final Loggers _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Loggers.Builder<_B> _newBuilder = new Loggers.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Loggers.Builder<Void> copyExcept(final Loggers _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Loggers.Builder<Void> copyOnly(final Loggers _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Loggers _storedValue;
        private List<Logger.Builder<Loggers.Builder<_B>>> logger;

        public Builder(final _B _parentBuilder, final Loggers _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.logger == null) {
                        this.logger = null;
                    } else {
                        this.logger = new ArrayList<Logger.Builder<Loggers.Builder<_B>>>();
                        for (Logger _item: _other.logger) {
                            this.logger.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Loggers _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree loggerPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("logger"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(loggerPropertyTree!= null):((loggerPropertyTree == null)||(!loggerPropertyTree.isLeaf())))) {
                        if (_other.logger == null) {
                            this.logger = null;
                        } else {
                            this.logger = new ArrayList<Logger.Builder<Loggers.Builder<_B>>>();
                            for (Logger _item: _other.logger) {
                                this.logger.add(((_item == null)?null:_item.newCopyBuilder(this, loggerPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Loggers >_P init(final _P _product) {
            if (this.logger!= null) {
                final List<Logger> logger = new ArrayList<Logger>(this.logger.size());
                for (Logger.Builder<Loggers.Builder<_B>> _item: this.logger) {
                    logger.add(_item.build());
                }
                _product.logger = logger;
            }
            return _product;
        }

        /**
         * Adds the given items to the value of "logger"
         * 
         * @param logger
         *     Items to add to the value of the "logger" property
         */
        public Loggers.Builder<_B> addLogger(final Iterable<? extends Logger> logger) {
            if (logger!= null) {
                if (this.logger == null) {
                    this.logger = new ArrayList<Logger.Builder<Loggers.Builder<_B>>>();
                }
                for (Logger _item: logger) {
                    this.logger.add(new Logger.Builder<Loggers.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "logger" (any previous value will be replaced)
         * 
         * @param logger
         *     New value of the "logger" property.
         */
        public Loggers.Builder<_B> withLogger(final Iterable<? extends Logger> logger) {
            if (this.logger!= null) {
                this.logger.clear();
            }
            return addLogger(logger);
        }

        /**
         * Adds the given items to the value of "logger"
         * 
         * @param logger
         *     Items to add to the value of the "logger" property
         */
        public Loggers.Builder<_B> addLogger(Logger... logger) {
            addLogger(Arrays.asList(logger));
            return this;
        }

        /**
         * Sets the new value of "logger" (any previous value will be replaced)
         * 
         * @param logger
         *     New value of the "logger" property.
         */
        public Loggers.Builder<_B> withLogger(Logger... logger) {
            withLogger(Arrays.asList(logger));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Logger" property.
         * Use {@link org.nuclos.schema.rest.Logger.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Logger" property.
         *     Use {@link org.nuclos.schema.rest.Logger.Builder#end()} to return to the current builder.
         */
        public Logger.Builder<? extends Loggers.Builder<_B>> addLogger() {
            if (this.logger == null) {
                this.logger = new ArrayList<Logger.Builder<Loggers.Builder<_B>>>();
            }
            final Logger.Builder<Loggers.Builder<_B>> logger_Builder = new Logger.Builder<Loggers.Builder<_B>>(this, null, false);
            this.logger.add(logger_Builder);
            return logger_Builder;
        }

        @Override
        public Loggers build() {
            if (_storedValue == null) {
                return this.init(new Loggers());
            } else {
                return ((Loggers) _storedValue);
            }
        }

        public Loggers.Builder<_B> copyOf(final Loggers _other) {
            _other.copyTo(this);
            return this;
        }

        public Loggers.Builder<_B> copyOf(final Loggers.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Loggers.Selector<Loggers.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Loggers.Select _root() {
            return new Loggers.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private Logger.Selector<TRoot, Loggers.Selector<TRoot, TParent>> logger = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.logger!= null) {
                products.put("logger", this.logger.init());
            }
            return products;
        }

        public Logger.Selector<TRoot, Loggers.Selector<TRoot, TParent>> logger() {
            return ((this.logger == null)?this.logger = new Logger.Selector<TRoot, Loggers.Selector<TRoot, TParent>>(this._root, this, "logger"):this.logger);
        }

    }

}
