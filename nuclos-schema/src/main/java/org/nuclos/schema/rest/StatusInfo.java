
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;


/**
 * <p>Java class for status-info complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="status-info"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="statename" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="numeral" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="description" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="color" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="buttonIcon" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "status-info")
public class StatusInfo implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "statename")
    protected String statename;
    @XmlAttribute(name = "numeral")
    protected String numeral;
    @XmlAttribute(name = "description")
    protected String description;
    @XmlAttribute(name = "color")
    protected String color;
    @XmlAttribute(name = "buttonIcon")
    protected String buttonIcon;

    /**
     * Gets the value of the statename property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatename() {
        return statename;
    }

    /**
     * Sets the value of the statename property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatename(String value) {
        this.statename = value;
    }

    /**
     * Gets the value of the numeral property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeral() {
        return numeral;
    }

    /**
     * Sets the value of the numeral property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeral(String value) {
        this.numeral = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the color property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColor() {
        return color;
    }

    /**
     * Sets the value of the color property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColor(String value) {
        this.color = value;
    }

    /**
     * Gets the value of the buttonIcon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getButtonIcon() {
        return buttonIcon;
    }

    /**
     * Sets the value of the buttonIcon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setButtonIcon(String value) {
        this.buttonIcon = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theStatename;
            theStatename = this.getStatename();
            strategy.appendField(locator, this, "statename", buffer, theStatename);
        }
        {
            String theNumeral;
            theNumeral = this.getNumeral();
            strategy.appendField(locator, this, "numeral", buffer, theNumeral);
        }
        {
            String theDescription;
            theDescription = this.getDescription();
            strategy.appendField(locator, this, "description", buffer, theDescription);
        }
        {
            String theColor;
            theColor = this.getColor();
            strategy.appendField(locator, this, "color", buffer, theColor);
        }
        {
            String theButtonIcon;
            theButtonIcon = this.getButtonIcon();
            strategy.appendField(locator, this, "buttonIcon", buffer, theButtonIcon);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof StatusInfo)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final StatusInfo that = ((StatusInfo) object);
        {
            String lhsStatename;
            lhsStatename = this.getStatename();
            String rhsStatename;
            rhsStatename = that.getStatename();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "statename", lhsStatename), LocatorUtils.property(thatLocator, "statename", rhsStatename), lhsStatename, rhsStatename)) {
                return false;
            }
        }
        {
            String lhsNumeral;
            lhsNumeral = this.getNumeral();
            String rhsNumeral;
            rhsNumeral = that.getNumeral();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "numeral", lhsNumeral), LocatorUtils.property(thatLocator, "numeral", rhsNumeral), lhsNumeral, rhsNumeral)) {
                return false;
            }
        }
        {
            String lhsDescription;
            lhsDescription = this.getDescription();
            String rhsDescription;
            rhsDescription = that.getDescription();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "description", lhsDescription), LocatorUtils.property(thatLocator, "description", rhsDescription), lhsDescription, rhsDescription)) {
                return false;
            }
        }
        {
            String lhsColor;
            lhsColor = this.getColor();
            String rhsColor;
            rhsColor = that.getColor();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "color", lhsColor), LocatorUtils.property(thatLocator, "color", rhsColor), lhsColor, rhsColor)) {
                return false;
            }
        }
        {
            String lhsButtonIcon;
            lhsButtonIcon = this.getButtonIcon();
            String rhsButtonIcon;
            rhsButtonIcon = that.getButtonIcon();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "buttonIcon", lhsButtonIcon), LocatorUtils.property(thatLocator, "buttonIcon", rhsButtonIcon), lhsButtonIcon, rhsButtonIcon)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theStatename;
            theStatename = this.getStatename();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "statename", theStatename), currentHashCode, theStatename);
        }
        {
            String theNumeral;
            theNumeral = this.getNumeral();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "numeral", theNumeral), currentHashCode, theNumeral);
        }
        {
            String theDescription;
            theDescription = this.getDescription();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "description", theDescription), currentHashCode, theDescription);
        }
        {
            String theColor;
            theColor = this.getColor();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "color", theColor), currentHashCode, theColor);
        }
        {
            String theButtonIcon;
            theButtonIcon = this.getButtonIcon();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "buttonIcon", theButtonIcon), currentHashCode, theButtonIcon);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof StatusInfo) {
            final StatusInfo copy = ((StatusInfo) draftCopy);
            if (this.statename!= null) {
                String sourceStatename;
                sourceStatename = this.getStatename();
                String copyStatename = ((String) strategy.copy(LocatorUtils.property(locator, "statename", sourceStatename), sourceStatename));
                copy.setStatename(copyStatename);
            } else {
                copy.statename = null;
            }
            if (this.numeral!= null) {
                String sourceNumeral;
                sourceNumeral = this.getNumeral();
                String copyNumeral = ((String) strategy.copy(LocatorUtils.property(locator, "numeral", sourceNumeral), sourceNumeral));
                copy.setNumeral(copyNumeral);
            } else {
                copy.numeral = null;
            }
            if (this.description!= null) {
                String sourceDescription;
                sourceDescription = this.getDescription();
                String copyDescription = ((String) strategy.copy(LocatorUtils.property(locator, "description", sourceDescription), sourceDescription));
                copy.setDescription(copyDescription);
            } else {
                copy.description = null;
            }
            if (this.color!= null) {
                String sourceColor;
                sourceColor = this.getColor();
                String copyColor = ((String) strategy.copy(LocatorUtils.property(locator, "color", sourceColor), sourceColor));
                copy.setColor(copyColor);
            } else {
                copy.color = null;
            }
            if (this.buttonIcon!= null) {
                String sourceButtonIcon;
                sourceButtonIcon = this.getButtonIcon();
                String copyButtonIcon = ((String) strategy.copy(LocatorUtils.property(locator, "buttonIcon", sourceButtonIcon), sourceButtonIcon));
                copy.setButtonIcon(copyButtonIcon);
            } else {
                copy.buttonIcon = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new StatusInfo();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final StatusInfo.Builder<_B> _other) {
        _other.statename = this.statename;
        _other.numeral = this.numeral;
        _other.description = this.description;
        _other.color = this.color;
        _other.buttonIcon = this.buttonIcon;
    }

    public<_B >StatusInfo.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new StatusInfo.Builder<_B>(_parentBuilder, this, true);
    }

    public StatusInfo.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static StatusInfo.Builder<Void> builder() {
        return new StatusInfo.Builder<Void>(null, null, false);
    }

    public static<_B >StatusInfo.Builder<_B> copyOf(final StatusInfo _other) {
        final StatusInfo.Builder<_B> _newBuilder = new StatusInfo.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final StatusInfo.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree statenamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("statename"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(statenamePropertyTree!= null):((statenamePropertyTree == null)||(!statenamePropertyTree.isLeaf())))) {
            _other.statename = this.statename;
        }
        final PropertyTree numeralPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("numeral"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(numeralPropertyTree!= null):((numeralPropertyTree == null)||(!numeralPropertyTree.isLeaf())))) {
            _other.numeral = this.numeral;
        }
        final PropertyTree descriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("description"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(descriptionPropertyTree!= null):((descriptionPropertyTree == null)||(!descriptionPropertyTree.isLeaf())))) {
            _other.description = this.description;
        }
        final PropertyTree colorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("color"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(colorPropertyTree!= null):((colorPropertyTree == null)||(!colorPropertyTree.isLeaf())))) {
            _other.color = this.color;
        }
        final PropertyTree buttonIconPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("buttonIcon"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(buttonIconPropertyTree!= null):((buttonIconPropertyTree == null)||(!buttonIconPropertyTree.isLeaf())))) {
            _other.buttonIcon = this.buttonIcon;
        }
    }

    public<_B >StatusInfo.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new StatusInfo.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public StatusInfo.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >StatusInfo.Builder<_B> copyOf(final StatusInfo _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final StatusInfo.Builder<_B> _newBuilder = new StatusInfo.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static StatusInfo.Builder<Void> copyExcept(final StatusInfo _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static StatusInfo.Builder<Void> copyOnly(final StatusInfo _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final StatusInfo _storedValue;
        private String statename;
        private String numeral;
        private String description;
        private String color;
        private String buttonIcon;

        public Builder(final _B _parentBuilder, final StatusInfo _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.statename = _other.statename;
                    this.numeral = _other.numeral;
                    this.description = _other.description;
                    this.color = _other.color;
                    this.buttonIcon = _other.buttonIcon;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final StatusInfo _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree statenamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("statename"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(statenamePropertyTree!= null):((statenamePropertyTree == null)||(!statenamePropertyTree.isLeaf())))) {
                        this.statename = _other.statename;
                    }
                    final PropertyTree numeralPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("numeral"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(numeralPropertyTree!= null):((numeralPropertyTree == null)||(!numeralPropertyTree.isLeaf())))) {
                        this.numeral = _other.numeral;
                    }
                    final PropertyTree descriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("description"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(descriptionPropertyTree!= null):((descriptionPropertyTree == null)||(!descriptionPropertyTree.isLeaf())))) {
                        this.description = _other.description;
                    }
                    final PropertyTree colorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("color"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(colorPropertyTree!= null):((colorPropertyTree == null)||(!colorPropertyTree.isLeaf())))) {
                        this.color = _other.color;
                    }
                    final PropertyTree buttonIconPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("buttonIcon"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(buttonIconPropertyTree!= null):((buttonIconPropertyTree == null)||(!buttonIconPropertyTree.isLeaf())))) {
                        this.buttonIcon = _other.buttonIcon;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends StatusInfo >_P init(final _P _product) {
            _product.statename = this.statename;
            _product.numeral = this.numeral;
            _product.description = this.description;
            _product.color = this.color;
            _product.buttonIcon = this.buttonIcon;
            return _product;
        }

        /**
         * Sets the new value of "statename" (any previous value will be replaced)
         * 
         * @param statename
         *     New value of the "statename" property.
         */
        public StatusInfo.Builder<_B> withStatename(final String statename) {
            this.statename = statename;
            return this;
        }

        /**
         * Sets the new value of "numeral" (any previous value will be replaced)
         * 
         * @param numeral
         *     New value of the "numeral" property.
         */
        public StatusInfo.Builder<_B> withNumeral(final String numeral) {
            this.numeral = numeral;
            return this;
        }

        /**
         * Sets the new value of "description" (any previous value will be replaced)
         * 
         * @param description
         *     New value of the "description" property.
         */
        public StatusInfo.Builder<_B> withDescription(final String description) {
            this.description = description;
            return this;
        }

        /**
         * Sets the new value of "color" (any previous value will be replaced)
         * 
         * @param color
         *     New value of the "color" property.
         */
        public StatusInfo.Builder<_B> withColor(final String color) {
            this.color = color;
            return this;
        }

        /**
         * Sets the new value of "buttonIcon" (any previous value will be replaced)
         * 
         * @param buttonIcon
         *     New value of the "buttonIcon" property.
         */
        public StatusInfo.Builder<_B> withButtonIcon(final String buttonIcon) {
            this.buttonIcon = buttonIcon;
            return this;
        }

        @Override
        public StatusInfo build() {
            if (_storedValue == null) {
                return this.init(new StatusInfo());
            } else {
                return ((StatusInfo) _storedValue);
            }
        }

        public StatusInfo.Builder<_B> copyOf(final StatusInfo _other) {
            _other.copyTo(this);
            return this;
        }

        public StatusInfo.Builder<_B> copyOf(final StatusInfo.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends StatusInfo.Selector<StatusInfo.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static StatusInfo.Select _root() {
            return new StatusInfo.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> statename = null;
        private com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> numeral = null;
        private com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> description = null;
        private com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> color = null;
        private com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> buttonIcon = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.statename!= null) {
                products.put("statename", this.statename.init());
            }
            if (this.numeral!= null) {
                products.put("numeral", this.numeral.init());
            }
            if (this.description!= null) {
                products.put("description", this.description.init());
            }
            if (this.color!= null) {
                products.put("color", this.color.init());
            }
            if (this.buttonIcon!= null) {
                products.put("buttonIcon", this.buttonIcon.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> statename() {
            return ((this.statename == null)?this.statename = new com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>>(this._root, this, "statename"):this.statename);
        }

        public com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> numeral() {
            return ((this.numeral == null)?this.numeral = new com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>>(this._root, this, "numeral"):this.numeral);
        }

        public com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> description() {
            return ((this.description == null)?this.description = new com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>>(this._root, this, "description"):this.description);
        }

        public com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> color() {
            return ((this.color == null)?this.color = new com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>>(this._root, this, "color"):this.color);
        }

        public com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> buttonIcon() {
            return ((this.buttonIcon == null)?this.buttonIcon = new com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>>(this._root, this, "buttonIcon"):this.buttonIcon);
        }

    }

}
