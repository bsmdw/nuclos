
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Replaces and extends LegalDisclaimers.
 * 
 * <p>Java class for news complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="news"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="title" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="content" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="revision" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" /&gt;
 *       &lt;attribute name="active" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="confirmationRequired" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="showAtStartup" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="validFrom" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *       &lt;attribute name="validUntil" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *       &lt;attribute name="privacyPolicy" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "news")
public class News implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "id", required = true)
    protected String id;
    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "title", required = true)
    protected String title;
    @XmlAttribute(name = "content", required = true)
    protected String content;
    @XmlAttribute(name = "revision", required = true)
    @XmlSchemaType(name = "unsignedInt")
    protected long revision;
    @XmlAttribute(name = "active", required = true)
    protected boolean active;
    @XmlAttribute(name = "confirmationRequired", required = true)
    protected boolean confirmationRequired;
    @XmlAttribute(name = "showAtStartup", required = true)
    protected boolean showAtStartup;
    @XmlAttribute(name = "validFrom")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar validFrom;
    @XmlAttribute(name = "validUntil")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar validUntil;
    @XmlAttribute(name = "privacyPolicy", required = true)
    protected boolean privacyPolicy;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the content property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContent() {
        return content;
    }

    /**
     * Sets the value of the content property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContent(String value) {
        this.content = value;
    }

    /**
     * Gets the value of the revision property.
     * 
     */
    public long getRevision() {
        return revision;
    }

    /**
     * Sets the value of the revision property.
     * 
     */
    public void setRevision(long value) {
        this.revision = value;
    }

    /**
     * Gets the value of the active property.
     * 
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Sets the value of the active property.
     * 
     */
    public void setActive(boolean value) {
        this.active = value;
    }

    /**
     * Gets the value of the confirmationRequired property.
     * 
     */
    public boolean isConfirmationRequired() {
        return confirmationRequired;
    }

    /**
     * Sets the value of the confirmationRequired property.
     * 
     */
    public void setConfirmationRequired(boolean value) {
        this.confirmationRequired = value;
    }

    /**
     * Gets the value of the showAtStartup property.
     * 
     */
    public boolean isShowAtStartup() {
        return showAtStartup;
    }

    /**
     * Sets the value of the showAtStartup property.
     * 
     */
    public void setShowAtStartup(boolean value) {
        this.showAtStartup = value;
    }

    /**
     * Gets the value of the validFrom property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getValidFrom() {
        return validFrom;
    }

    /**
     * Sets the value of the validFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setValidFrom(XMLGregorianCalendar value) {
        this.validFrom = value;
    }

    /**
     * Gets the value of the validUntil property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getValidUntil() {
        return validUntil;
    }

    /**
     * Sets the value of the validUntil property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setValidUntil(XMLGregorianCalendar value) {
        this.validUntil = value;
    }

    /**
     * Gets the value of the privacyPolicy property.
     * 
     */
    public boolean isPrivacyPolicy() {
        return privacyPolicy;
    }

    /**
     * Sets the value of the privacyPolicy property.
     * 
     */
    public void setPrivacyPolicy(boolean value) {
        this.privacyPolicy = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theId;
            theId = this.getId();
            strategy.appendField(locator, this, "id", buffer, theId);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String theTitle;
            theTitle = this.getTitle();
            strategy.appendField(locator, this, "title", buffer, theTitle);
        }
        {
            String theContent;
            theContent = this.getContent();
            strategy.appendField(locator, this, "content", buffer, theContent);
        }
        {
            long theRevision;
            theRevision = this.getRevision();
            strategy.appendField(locator, this, "revision", buffer, theRevision);
        }
        {
            boolean theActive;
            theActive = this.isActive();
            strategy.appendField(locator, this, "active", buffer, theActive);
        }
        {
            boolean theConfirmationRequired;
            theConfirmationRequired = this.isConfirmationRequired();
            strategy.appendField(locator, this, "confirmationRequired", buffer, theConfirmationRequired);
        }
        {
            boolean theShowAtStartup;
            theShowAtStartup = this.isShowAtStartup();
            strategy.appendField(locator, this, "showAtStartup", buffer, theShowAtStartup);
        }
        {
            XMLGregorianCalendar theValidFrom;
            theValidFrom = this.getValidFrom();
            strategy.appendField(locator, this, "validFrom", buffer, theValidFrom);
        }
        {
            XMLGregorianCalendar theValidUntil;
            theValidUntil = this.getValidUntil();
            strategy.appendField(locator, this, "validUntil", buffer, theValidUntil);
        }
        {
            boolean thePrivacyPolicy;
            thePrivacyPolicy = this.isPrivacyPolicy();
            strategy.appendField(locator, this, "privacyPolicy", buffer, thePrivacyPolicy);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof News)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final News that = ((News) object);
        {
            String lhsId;
            lhsId = this.getId();
            String rhsId;
            rhsId = that.getId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "id", lhsId), LocatorUtils.property(thatLocator, "id", rhsId), lhsId, rhsId)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsTitle;
            lhsTitle = this.getTitle();
            String rhsTitle;
            rhsTitle = that.getTitle();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "title", lhsTitle), LocatorUtils.property(thatLocator, "title", rhsTitle), lhsTitle, rhsTitle)) {
                return false;
            }
        }
        {
            String lhsContent;
            lhsContent = this.getContent();
            String rhsContent;
            rhsContent = that.getContent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "content", lhsContent), LocatorUtils.property(thatLocator, "content", rhsContent), lhsContent, rhsContent)) {
                return false;
            }
        }
        {
            long lhsRevision;
            lhsRevision = this.getRevision();
            long rhsRevision;
            rhsRevision = that.getRevision();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "revision", lhsRevision), LocatorUtils.property(thatLocator, "revision", rhsRevision), lhsRevision, rhsRevision)) {
                return false;
            }
        }
        {
            boolean lhsActive;
            lhsActive = this.isActive();
            boolean rhsActive;
            rhsActive = that.isActive();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "active", lhsActive), LocatorUtils.property(thatLocator, "active", rhsActive), lhsActive, rhsActive)) {
                return false;
            }
        }
        {
            boolean lhsConfirmationRequired;
            lhsConfirmationRequired = this.isConfirmationRequired();
            boolean rhsConfirmationRequired;
            rhsConfirmationRequired = that.isConfirmationRequired();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "confirmationRequired", lhsConfirmationRequired), LocatorUtils.property(thatLocator, "confirmationRequired", rhsConfirmationRequired), lhsConfirmationRequired, rhsConfirmationRequired)) {
                return false;
            }
        }
        {
            boolean lhsShowAtStartup;
            lhsShowAtStartup = this.isShowAtStartup();
            boolean rhsShowAtStartup;
            rhsShowAtStartup = that.isShowAtStartup();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "showAtStartup", lhsShowAtStartup), LocatorUtils.property(thatLocator, "showAtStartup", rhsShowAtStartup), lhsShowAtStartup, rhsShowAtStartup)) {
                return false;
            }
        }
        {
            XMLGregorianCalendar lhsValidFrom;
            lhsValidFrom = this.getValidFrom();
            XMLGregorianCalendar rhsValidFrom;
            rhsValidFrom = that.getValidFrom();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "validFrom", lhsValidFrom), LocatorUtils.property(thatLocator, "validFrom", rhsValidFrom), lhsValidFrom, rhsValidFrom)) {
                return false;
            }
        }
        {
            XMLGregorianCalendar lhsValidUntil;
            lhsValidUntil = this.getValidUntil();
            XMLGregorianCalendar rhsValidUntil;
            rhsValidUntil = that.getValidUntil();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "validUntil", lhsValidUntil), LocatorUtils.property(thatLocator, "validUntil", rhsValidUntil), lhsValidUntil, rhsValidUntil)) {
                return false;
            }
        }
        {
            boolean lhsPrivacyPolicy;
            lhsPrivacyPolicy = this.isPrivacyPolicy();
            boolean rhsPrivacyPolicy;
            rhsPrivacyPolicy = that.isPrivacyPolicy();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "privacyPolicy", lhsPrivacyPolicy), LocatorUtils.property(thatLocator, "privacyPolicy", rhsPrivacyPolicy), lhsPrivacyPolicy, rhsPrivacyPolicy)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theId;
            theId = this.getId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "id", theId), currentHashCode, theId);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String theTitle;
            theTitle = this.getTitle();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "title", theTitle), currentHashCode, theTitle);
        }
        {
            String theContent;
            theContent = this.getContent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "content", theContent), currentHashCode, theContent);
        }
        {
            long theRevision;
            theRevision = this.getRevision();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "revision", theRevision), currentHashCode, theRevision);
        }
        {
            boolean theActive;
            theActive = this.isActive();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "active", theActive), currentHashCode, theActive);
        }
        {
            boolean theConfirmationRequired;
            theConfirmationRequired = this.isConfirmationRequired();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "confirmationRequired", theConfirmationRequired), currentHashCode, theConfirmationRequired);
        }
        {
            boolean theShowAtStartup;
            theShowAtStartup = this.isShowAtStartup();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "showAtStartup", theShowAtStartup), currentHashCode, theShowAtStartup);
        }
        {
            XMLGregorianCalendar theValidFrom;
            theValidFrom = this.getValidFrom();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "validFrom", theValidFrom), currentHashCode, theValidFrom);
        }
        {
            XMLGregorianCalendar theValidUntil;
            theValidUntil = this.getValidUntil();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "validUntil", theValidUntil), currentHashCode, theValidUntil);
        }
        {
            boolean thePrivacyPolicy;
            thePrivacyPolicy = this.isPrivacyPolicy();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "privacyPolicy", thePrivacyPolicy), currentHashCode, thePrivacyPolicy);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof News) {
            final News copy = ((News) draftCopy);
            if (this.id!= null) {
                String sourceId;
                sourceId = this.getId();
                String copyId = ((String) strategy.copy(LocatorUtils.property(locator, "id", sourceId), sourceId));
                copy.setId(copyId);
            } else {
                copy.id = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.title!= null) {
                String sourceTitle;
                sourceTitle = this.getTitle();
                String copyTitle = ((String) strategy.copy(LocatorUtils.property(locator, "title", sourceTitle), sourceTitle));
                copy.setTitle(copyTitle);
            } else {
                copy.title = null;
            }
            if (this.content!= null) {
                String sourceContent;
                sourceContent = this.getContent();
                String copyContent = ((String) strategy.copy(LocatorUtils.property(locator, "content", sourceContent), sourceContent));
                copy.setContent(copyContent);
            } else {
                copy.content = null;
            }
            {
                long sourceRevision;
                sourceRevision = this.getRevision();
                long copyRevision = strategy.copy(LocatorUtils.property(locator, "revision", sourceRevision), sourceRevision);
                copy.setRevision(copyRevision);
            }
            {
                boolean sourceActive;
                sourceActive = this.isActive();
                boolean copyActive = strategy.copy(LocatorUtils.property(locator, "active", sourceActive), sourceActive);
                copy.setActive(copyActive);
            }
            {
                boolean sourceConfirmationRequired;
                sourceConfirmationRequired = this.isConfirmationRequired();
                boolean copyConfirmationRequired = strategy.copy(LocatorUtils.property(locator, "confirmationRequired", sourceConfirmationRequired), sourceConfirmationRequired);
                copy.setConfirmationRequired(copyConfirmationRequired);
            }
            {
                boolean sourceShowAtStartup;
                sourceShowAtStartup = this.isShowAtStartup();
                boolean copyShowAtStartup = strategy.copy(LocatorUtils.property(locator, "showAtStartup", sourceShowAtStartup), sourceShowAtStartup);
                copy.setShowAtStartup(copyShowAtStartup);
            }
            if (this.validFrom!= null) {
                XMLGregorianCalendar sourceValidFrom;
                sourceValidFrom = this.getValidFrom();
                XMLGregorianCalendar copyValidFrom = ((XMLGregorianCalendar) strategy.copy(LocatorUtils.property(locator, "validFrom", sourceValidFrom), sourceValidFrom));
                copy.setValidFrom(copyValidFrom);
            } else {
                copy.validFrom = null;
            }
            if (this.validUntil!= null) {
                XMLGregorianCalendar sourceValidUntil;
                sourceValidUntil = this.getValidUntil();
                XMLGregorianCalendar copyValidUntil = ((XMLGregorianCalendar) strategy.copy(LocatorUtils.property(locator, "validUntil", sourceValidUntil), sourceValidUntil));
                copy.setValidUntil(copyValidUntil);
            } else {
                copy.validUntil = null;
            }
            {
                boolean sourcePrivacyPolicy;
                sourcePrivacyPolicy = this.isPrivacyPolicy();
                boolean copyPrivacyPolicy = strategy.copy(LocatorUtils.property(locator, "privacyPolicy", sourcePrivacyPolicy), sourcePrivacyPolicy);
                copy.setPrivacyPolicy(copyPrivacyPolicy);
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new News();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final News.Builder<_B> _other) {
        _other.id = this.id;
        _other.name = this.name;
        _other.title = this.title;
        _other.content = this.content;
        _other.revision = this.revision;
        _other.active = this.active;
        _other.confirmationRequired = this.confirmationRequired;
        _other.showAtStartup = this.showAtStartup;
        _other.validFrom = ((this.validFrom == null)?null:((XMLGregorianCalendar) this.validFrom.clone()));
        _other.validUntil = ((this.validUntil == null)?null:((XMLGregorianCalendar) this.validUntil.clone()));
        _other.privacyPolicy = this.privacyPolicy;
    }

    public<_B >News.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new News.Builder<_B>(_parentBuilder, this, true);
    }

    public News.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static News.Builder<Void> builder() {
        return new News.Builder<Void>(null, null, false);
    }

    public static<_B >News.Builder<_B> copyOf(final News _other) {
        final News.Builder<_B> _newBuilder = new News.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final News.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree idPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("id"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(idPropertyTree!= null):((idPropertyTree == null)||(!idPropertyTree.isLeaf())))) {
            _other.id = this.id;
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree titlePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("title"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(titlePropertyTree!= null):((titlePropertyTree == null)||(!titlePropertyTree.isLeaf())))) {
            _other.title = this.title;
        }
        final PropertyTree contentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("content"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(contentPropertyTree!= null):((contentPropertyTree == null)||(!contentPropertyTree.isLeaf())))) {
            _other.content = this.content;
        }
        final PropertyTree revisionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("revision"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(revisionPropertyTree!= null):((revisionPropertyTree == null)||(!revisionPropertyTree.isLeaf())))) {
            _other.revision = this.revision;
        }
        final PropertyTree activePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("active"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(activePropertyTree!= null):((activePropertyTree == null)||(!activePropertyTree.isLeaf())))) {
            _other.active = this.active;
        }
        final PropertyTree confirmationRequiredPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("confirmationRequired"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(confirmationRequiredPropertyTree!= null):((confirmationRequiredPropertyTree == null)||(!confirmationRequiredPropertyTree.isLeaf())))) {
            _other.confirmationRequired = this.confirmationRequired;
        }
        final PropertyTree showAtStartupPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("showAtStartup"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(showAtStartupPropertyTree!= null):((showAtStartupPropertyTree == null)||(!showAtStartupPropertyTree.isLeaf())))) {
            _other.showAtStartup = this.showAtStartup;
        }
        final PropertyTree validFromPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("validFrom"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(validFromPropertyTree!= null):((validFromPropertyTree == null)||(!validFromPropertyTree.isLeaf())))) {
            _other.validFrom = ((this.validFrom == null)?null:((XMLGregorianCalendar) this.validFrom.clone()));
        }
        final PropertyTree validUntilPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("validUntil"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(validUntilPropertyTree!= null):((validUntilPropertyTree == null)||(!validUntilPropertyTree.isLeaf())))) {
            _other.validUntil = ((this.validUntil == null)?null:((XMLGregorianCalendar) this.validUntil.clone()));
        }
        final PropertyTree privacyPolicyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("privacyPolicy"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(privacyPolicyPropertyTree!= null):((privacyPolicyPropertyTree == null)||(!privacyPolicyPropertyTree.isLeaf())))) {
            _other.privacyPolicy = this.privacyPolicy;
        }
    }

    public<_B >News.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new News.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public News.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >News.Builder<_B> copyOf(final News _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final News.Builder<_B> _newBuilder = new News.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static News.Builder<Void> copyExcept(final News _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static News.Builder<Void> copyOnly(final News _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final News _storedValue;
        private String id;
        private String name;
        private String title;
        private String content;
        private long revision;
        private boolean active;
        private boolean confirmationRequired;
        private boolean showAtStartup;
        private XMLGregorianCalendar validFrom;
        private XMLGregorianCalendar validUntil;
        private boolean privacyPolicy;

        public Builder(final _B _parentBuilder, final News _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.id = _other.id;
                    this.name = _other.name;
                    this.title = _other.title;
                    this.content = _other.content;
                    this.revision = _other.revision;
                    this.active = _other.active;
                    this.confirmationRequired = _other.confirmationRequired;
                    this.showAtStartup = _other.showAtStartup;
                    this.validFrom = ((_other.validFrom == null)?null:((XMLGregorianCalendar) _other.validFrom.clone()));
                    this.validUntil = ((_other.validUntil == null)?null:((XMLGregorianCalendar) _other.validUntil.clone()));
                    this.privacyPolicy = _other.privacyPolicy;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final News _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree idPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("id"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(idPropertyTree!= null):((idPropertyTree == null)||(!idPropertyTree.isLeaf())))) {
                        this.id = _other.id;
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree titlePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("title"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(titlePropertyTree!= null):((titlePropertyTree == null)||(!titlePropertyTree.isLeaf())))) {
                        this.title = _other.title;
                    }
                    final PropertyTree contentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("content"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(contentPropertyTree!= null):((contentPropertyTree == null)||(!contentPropertyTree.isLeaf())))) {
                        this.content = _other.content;
                    }
                    final PropertyTree revisionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("revision"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(revisionPropertyTree!= null):((revisionPropertyTree == null)||(!revisionPropertyTree.isLeaf())))) {
                        this.revision = _other.revision;
                    }
                    final PropertyTree activePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("active"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(activePropertyTree!= null):((activePropertyTree == null)||(!activePropertyTree.isLeaf())))) {
                        this.active = _other.active;
                    }
                    final PropertyTree confirmationRequiredPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("confirmationRequired"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(confirmationRequiredPropertyTree!= null):((confirmationRequiredPropertyTree == null)||(!confirmationRequiredPropertyTree.isLeaf())))) {
                        this.confirmationRequired = _other.confirmationRequired;
                    }
                    final PropertyTree showAtStartupPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("showAtStartup"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(showAtStartupPropertyTree!= null):((showAtStartupPropertyTree == null)||(!showAtStartupPropertyTree.isLeaf())))) {
                        this.showAtStartup = _other.showAtStartup;
                    }
                    final PropertyTree validFromPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("validFrom"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(validFromPropertyTree!= null):((validFromPropertyTree == null)||(!validFromPropertyTree.isLeaf())))) {
                        this.validFrom = ((_other.validFrom == null)?null:((XMLGregorianCalendar) _other.validFrom.clone()));
                    }
                    final PropertyTree validUntilPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("validUntil"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(validUntilPropertyTree!= null):((validUntilPropertyTree == null)||(!validUntilPropertyTree.isLeaf())))) {
                        this.validUntil = ((_other.validUntil == null)?null:((XMLGregorianCalendar) _other.validUntil.clone()));
                    }
                    final PropertyTree privacyPolicyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("privacyPolicy"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(privacyPolicyPropertyTree!= null):((privacyPolicyPropertyTree == null)||(!privacyPolicyPropertyTree.isLeaf())))) {
                        this.privacyPolicy = _other.privacyPolicy;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends News >_P init(final _P _product) {
            _product.id = this.id;
            _product.name = this.name;
            _product.title = this.title;
            _product.content = this.content;
            _product.revision = this.revision;
            _product.active = this.active;
            _product.confirmationRequired = this.confirmationRequired;
            _product.showAtStartup = this.showAtStartup;
            _product.validFrom = this.validFrom;
            _product.validUntil = this.validUntil;
            _product.privacyPolicy = this.privacyPolicy;
            return _product;
        }

        /**
         * Sets the new value of "id" (any previous value will be replaced)
         * 
         * @param id
         *     New value of the "id" property.
         */
        public News.Builder<_B> withId(final String id) {
            this.id = id;
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public News.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "title" (any previous value will be replaced)
         * 
         * @param title
         *     New value of the "title" property.
         */
        public News.Builder<_B> withTitle(final String title) {
            this.title = title;
            return this;
        }

        /**
         * Sets the new value of "content" (any previous value will be replaced)
         * 
         * @param content
         *     New value of the "content" property.
         */
        public News.Builder<_B> withContent(final String content) {
            this.content = content;
            return this;
        }

        /**
         * Sets the new value of "revision" (any previous value will be replaced)
         * 
         * @param revision
         *     New value of the "revision" property.
         */
        public News.Builder<_B> withRevision(final long revision) {
            this.revision = revision;
            return this;
        }

        /**
         * Sets the new value of "active" (any previous value will be replaced)
         * 
         * @param active
         *     New value of the "active" property.
         */
        public News.Builder<_B> withActive(final boolean active) {
            this.active = active;
            return this;
        }

        /**
         * Sets the new value of "confirmationRequired" (any previous value will be replaced)
         * 
         * @param confirmationRequired
         *     New value of the "confirmationRequired" property.
         */
        public News.Builder<_B> withConfirmationRequired(final boolean confirmationRequired) {
            this.confirmationRequired = confirmationRequired;
            return this;
        }

        /**
         * Sets the new value of "showAtStartup" (any previous value will be replaced)
         * 
         * @param showAtStartup
         *     New value of the "showAtStartup" property.
         */
        public News.Builder<_B> withShowAtStartup(final boolean showAtStartup) {
            this.showAtStartup = showAtStartup;
            return this;
        }

        /**
         * Sets the new value of "validFrom" (any previous value will be replaced)
         * 
         * @param validFrom
         *     New value of the "validFrom" property.
         */
        public News.Builder<_B> withValidFrom(final XMLGregorianCalendar validFrom) {
            this.validFrom = validFrom;
            return this;
        }

        /**
         * Sets the new value of "validUntil" (any previous value will be replaced)
         * 
         * @param validUntil
         *     New value of the "validUntil" property.
         */
        public News.Builder<_B> withValidUntil(final XMLGregorianCalendar validUntil) {
            this.validUntil = validUntil;
            return this;
        }

        /**
         * Sets the new value of "privacyPolicy" (any previous value will be replaced)
         * 
         * @param privacyPolicy
         *     New value of the "privacyPolicy" property.
         */
        public News.Builder<_B> withPrivacyPolicy(final boolean privacyPolicy) {
            this.privacyPolicy = privacyPolicy;
            return this;
        }

        @Override
        public News build() {
            if (_storedValue == null) {
                return this.init(new News());
            } else {
                return ((News) _storedValue);
            }
        }

        public News.Builder<_B> copyOf(final News _other) {
            _other.copyTo(this);
            return this;
        }

        public News.Builder<_B> copyOf(final News.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends News.Selector<News.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static News.Select _root() {
            return new News.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, News.Selector<TRoot, TParent>> id = null;
        private com.kscs.util.jaxb.Selector<TRoot, News.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, News.Selector<TRoot, TParent>> title = null;
        private com.kscs.util.jaxb.Selector<TRoot, News.Selector<TRoot, TParent>> content = null;
        private com.kscs.util.jaxb.Selector<TRoot, News.Selector<TRoot, TParent>> validFrom = null;
        private com.kscs.util.jaxb.Selector<TRoot, News.Selector<TRoot, TParent>> validUntil = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.id!= null) {
                products.put("id", this.id.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.title!= null) {
                products.put("title", this.title.init());
            }
            if (this.content!= null) {
                products.put("content", this.content.init());
            }
            if (this.validFrom!= null) {
                products.put("validFrom", this.validFrom.init());
            }
            if (this.validUntil!= null) {
                products.put("validUntil", this.validUntil.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, News.Selector<TRoot, TParent>> id() {
            return ((this.id == null)?this.id = new com.kscs.util.jaxb.Selector<TRoot, News.Selector<TRoot, TParent>>(this._root, this, "id"):this.id);
        }

        public com.kscs.util.jaxb.Selector<TRoot, News.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, News.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, News.Selector<TRoot, TParent>> title() {
            return ((this.title == null)?this.title = new com.kscs.util.jaxb.Selector<TRoot, News.Selector<TRoot, TParent>>(this._root, this, "title"):this.title);
        }

        public com.kscs.util.jaxb.Selector<TRoot, News.Selector<TRoot, TParent>> content() {
            return ((this.content == null)?this.content = new com.kscs.util.jaxb.Selector<TRoot, News.Selector<TRoot, TParent>>(this._root, this, "content"):this.content);
        }

        public com.kscs.util.jaxb.Selector<TRoot, News.Selector<TRoot, TParent>> validFrom() {
            return ((this.validFrom == null)?this.validFrom = new com.kscs.util.jaxb.Selector<TRoot, News.Selector<TRoot, TParent>>(this._root, this, "validFrom"):this.validFrom);
        }

        public com.kscs.util.jaxb.Selector<TRoot, News.Selector<TRoot, TParent>> validUntil() {
            return ((this.validUntil == null)?this.validUntil = new com.kscs.util.jaxb.Selector<TRoot, News.Selector<TRoot, TParent>>(this._root, this, "validUntil"):this.validUntil);
        }

    }

}
