
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;


/**
 * <p>Java class for matrix-data complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="matrix-data"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="xAxisBoList" type="{urn:org.nuclos.schema.rest}matrix-x-axis-object" maxOccurs="unbounded"/&gt;
 *         &lt;element name="yAxisBoList" type="{urn:org.nuclos.schema.rest}matrix-y-axis-object" maxOccurs="unbounded"/&gt;
 *         &lt;element name="entityMatrixBoDict" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded"/&gt;
 *         &lt;element name="xrefData" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="cellInputType" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="editable" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "matrix-data", propOrder = {
    "xAxisBoList",
    "yAxisBoList",
    "entityMatrixBoDict",
    "xrefData"
})
public class MatrixData implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected List<MatrixXAxisObject> xAxisBoList;
    @XmlElement(required = true)
    protected List<MatrixYAxisObject> yAxisBoList;
    @XmlElement(required = true)
    protected List<Object> entityMatrixBoDict;
    @XmlElement(required = true)
    protected List<Object> xrefData;
    @XmlAttribute(name = "cellInputType")
    protected String cellInputType;
    @XmlAttribute(name = "editable")
    protected Boolean editable;

    /**
     * Gets the value of the xAxisBoList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the xAxisBoList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getXAxisBoList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MatrixXAxisObject }
     * 
     * 
     */
    public List<MatrixXAxisObject> getXAxisBoList() {
        if (xAxisBoList == null) {
            xAxisBoList = new ArrayList<MatrixXAxisObject>();
        }
        return this.xAxisBoList;
    }

    /**
     * Gets the value of the yAxisBoList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the yAxisBoList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getYAxisBoList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MatrixYAxisObject }
     * 
     * 
     */
    public List<MatrixYAxisObject> getYAxisBoList() {
        if (yAxisBoList == null) {
            yAxisBoList = new ArrayList<MatrixYAxisObject>();
        }
        return this.yAxisBoList;
    }

    /**
     * Gets the value of the entityMatrixBoDict property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the entityMatrixBoDict property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEntityMatrixBoDict().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getEntityMatrixBoDict() {
        if (entityMatrixBoDict == null) {
            entityMatrixBoDict = new ArrayList<Object>();
        }
        return this.entityMatrixBoDict;
    }

    /**
     * Gets the value of the xrefData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the xrefData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getXrefData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getXrefData() {
        if (xrefData == null) {
            xrefData = new ArrayList<Object>();
        }
        return this.xrefData;
    }

    /**
     * Gets the value of the cellInputType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCellInputType() {
        return cellInputType;
    }

    /**
     * Sets the value of the cellInputType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCellInputType(String value) {
        this.cellInputType = value;
    }

    /**
     * Gets the value of the editable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEditable() {
        return editable;
    }

    /**
     * Sets the value of the editable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEditable(Boolean value) {
        this.editable = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<MatrixXAxisObject> theXAxisBoList;
            theXAxisBoList = (((this.xAxisBoList!= null)&&(!this.xAxisBoList.isEmpty()))?this.getXAxisBoList():null);
            strategy.appendField(locator, this, "xAxisBoList", buffer, theXAxisBoList);
        }
        {
            List<MatrixYAxisObject> theYAxisBoList;
            theYAxisBoList = (((this.yAxisBoList!= null)&&(!this.yAxisBoList.isEmpty()))?this.getYAxisBoList():null);
            strategy.appendField(locator, this, "yAxisBoList", buffer, theYAxisBoList);
        }
        {
            List<Object> theEntityMatrixBoDict;
            theEntityMatrixBoDict = (((this.entityMatrixBoDict!= null)&&(!this.entityMatrixBoDict.isEmpty()))?this.getEntityMatrixBoDict():null);
            strategy.appendField(locator, this, "entityMatrixBoDict", buffer, theEntityMatrixBoDict);
        }
        {
            List<Object> theXrefData;
            theXrefData = (((this.xrefData!= null)&&(!this.xrefData.isEmpty()))?this.getXrefData():null);
            strategy.appendField(locator, this, "xrefData", buffer, theXrefData);
        }
        {
            String theCellInputType;
            theCellInputType = this.getCellInputType();
            strategy.appendField(locator, this, "cellInputType", buffer, theCellInputType);
        }
        {
            Boolean theEditable;
            theEditable = this.isEditable();
            strategy.appendField(locator, this, "editable", buffer, theEditable);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MatrixData)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MatrixData that = ((MatrixData) object);
        {
            List<MatrixXAxisObject> lhsXAxisBoList;
            lhsXAxisBoList = (((this.xAxisBoList!= null)&&(!this.xAxisBoList.isEmpty()))?this.getXAxisBoList():null);
            List<MatrixXAxisObject> rhsXAxisBoList;
            rhsXAxisBoList = (((that.xAxisBoList!= null)&&(!that.xAxisBoList.isEmpty()))?that.getXAxisBoList():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "xAxisBoList", lhsXAxisBoList), LocatorUtils.property(thatLocator, "xAxisBoList", rhsXAxisBoList), lhsXAxisBoList, rhsXAxisBoList)) {
                return false;
            }
        }
        {
            List<MatrixYAxisObject> lhsYAxisBoList;
            lhsYAxisBoList = (((this.yAxisBoList!= null)&&(!this.yAxisBoList.isEmpty()))?this.getYAxisBoList():null);
            List<MatrixYAxisObject> rhsYAxisBoList;
            rhsYAxisBoList = (((that.yAxisBoList!= null)&&(!that.yAxisBoList.isEmpty()))?that.getYAxisBoList():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "yAxisBoList", lhsYAxisBoList), LocatorUtils.property(thatLocator, "yAxisBoList", rhsYAxisBoList), lhsYAxisBoList, rhsYAxisBoList)) {
                return false;
            }
        }
        {
            List<Object> lhsEntityMatrixBoDict;
            lhsEntityMatrixBoDict = (((this.entityMatrixBoDict!= null)&&(!this.entityMatrixBoDict.isEmpty()))?this.getEntityMatrixBoDict():null);
            List<Object> rhsEntityMatrixBoDict;
            rhsEntityMatrixBoDict = (((that.entityMatrixBoDict!= null)&&(!that.entityMatrixBoDict.isEmpty()))?that.getEntityMatrixBoDict():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityMatrixBoDict", lhsEntityMatrixBoDict), LocatorUtils.property(thatLocator, "entityMatrixBoDict", rhsEntityMatrixBoDict), lhsEntityMatrixBoDict, rhsEntityMatrixBoDict)) {
                return false;
            }
        }
        {
            List<Object> lhsXrefData;
            lhsXrefData = (((this.xrefData!= null)&&(!this.xrefData.isEmpty()))?this.getXrefData():null);
            List<Object> rhsXrefData;
            rhsXrefData = (((that.xrefData!= null)&&(!that.xrefData.isEmpty()))?that.getXrefData():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "xrefData", lhsXrefData), LocatorUtils.property(thatLocator, "xrefData", rhsXrefData), lhsXrefData, rhsXrefData)) {
                return false;
            }
        }
        {
            String lhsCellInputType;
            lhsCellInputType = this.getCellInputType();
            String rhsCellInputType;
            rhsCellInputType = that.getCellInputType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "cellInputType", lhsCellInputType), LocatorUtils.property(thatLocator, "cellInputType", rhsCellInputType), lhsCellInputType, rhsCellInputType)) {
                return false;
            }
        }
        {
            Boolean lhsEditable;
            lhsEditable = this.isEditable();
            Boolean rhsEditable;
            rhsEditable = that.isEditable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "editable", lhsEditable), LocatorUtils.property(thatLocator, "editable", rhsEditable), lhsEditable, rhsEditable)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<MatrixXAxisObject> theXAxisBoList;
            theXAxisBoList = (((this.xAxisBoList!= null)&&(!this.xAxisBoList.isEmpty()))?this.getXAxisBoList():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "xAxisBoList", theXAxisBoList), currentHashCode, theXAxisBoList);
        }
        {
            List<MatrixYAxisObject> theYAxisBoList;
            theYAxisBoList = (((this.yAxisBoList!= null)&&(!this.yAxisBoList.isEmpty()))?this.getYAxisBoList():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "yAxisBoList", theYAxisBoList), currentHashCode, theYAxisBoList);
        }
        {
            List<Object> theEntityMatrixBoDict;
            theEntityMatrixBoDict = (((this.entityMatrixBoDict!= null)&&(!this.entityMatrixBoDict.isEmpty()))?this.getEntityMatrixBoDict():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityMatrixBoDict", theEntityMatrixBoDict), currentHashCode, theEntityMatrixBoDict);
        }
        {
            List<Object> theXrefData;
            theXrefData = (((this.xrefData!= null)&&(!this.xrefData.isEmpty()))?this.getXrefData():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "xrefData", theXrefData), currentHashCode, theXrefData);
        }
        {
            String theCellInputType;
            theCellInputType = this.getCellInputType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "cellInputType", theCellInputType), currentHashCode, theCellInputType);
        }
        {
            Boolean theEditable;
            theEditable = this.isEditable();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "editable", theEditable), currentHashCode, theEditable);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof MatrixData) {
            final MatrixData copy = ((MatrixData) draftCopy);
            if ((this.xAxisBoList!= null)&&(!this.xAxisBoList.isEmpty())) {
                List<MatrixXAxisObject> sourceXAxisBoList;
                sourceXAxisBoList = (((this.xAxisBoList!= null)&&(!this.xAxisBoList.isEmpty()))?this.getXAxisBoList():null);
                @SuppressWarnings("unchecked")
                List<MatrixXAxisObject> copyXAxisBoList = ((List<MatrixXAxisObject> ) strategy.copy(LocatorUtils.property(locator, "xAxisBoList", sourceXAxisBoList), sourceXAxisBoList));
                copy.xAxisBoList = null;
                if (copyXAxisBoList!= null) {
                    List<MatrixXAxisObject> uniqueXAxisBoListl = copy.getXAxisBoList();
                    uniqueXAxisBoListl.addAll(copyXAxisBoList);
                }
            } else {
                copy.xAxisBoList = null;
            }
            if ((this.yAxisBoList!= null)&&(!this.yAxisBoList.isEmpty())) {
                List<MatrixYAxisObject> sourceYAxisBoList;
                sourceYAxisBoList = (((this.yAxisBoList!= null)&&(!this.yAxisBoList.isEmpty()))?this.getYAxisBoList():null);
                @SuppressWarnings("unchecked")
                List<MatrixYAxisObject> copyYAxisBoList = ((List<MatrixYAxisObject> ) strategy.copy(LocatorUtils.property(locator, "yAxisBoList", sourceYAxisBoList), sourceYAxisBoList));
                copy.yAxisBoList = null;
                if (copyYAxisBoList!= null) {
                    List<MatrixYAxisObject> uniqueYAxisBoListl = copy.getYAxisBoList();
                    uniqueYAxisBoListl.addAll(copyYAxisBoList);
                }
            } else {
                copy.yAxisBoList = null;
            }
            if ((this.entityMatrixBoDict!= null)&&(!this.entityMatrixBoDict.isEmpty())) {
                List<Object> sourceEntityMatrixBoDict;
                sourceEntityMatrixBoDict = (((this.entityMatrixBoDict!= null)&&(!this.entityMatrixBoDict.isEmpty()))?this.getEntityMatrixBoDict():null);
                @SuppressWarnings("unchecked")
                List<Object> copyEntityMatrixBoDict = ((List<Object> ) strategy.copy(LocatorUtils.property(locator, "entityMatrixBoDict", sourceEntityMatrixBoDict), sourceEntityMatrixBoDict));
                copy.entityMatrixBoDict = null;
                if (copyEntityMatrixBoDict!= null) {
                    List<Object> uniqueEntityMatrixBoDictl = copy.getEntityMatrixBoDict();
                    uniqueEntityMatrixBoDictl.addAll(copyEntityMatrixBoDict);
                }
            } else {
                copy.entityMatrixBoDict = null;
            }
            if ((this.xrefData!= null)&&(!this.xrefData.isEmpty())) {
                List<Object> sourceXrefData;
                sourceXrefData = (((this.xrefData!= null)&&(!this.xrefData.isEmpty()))?this.getXrefData():null);
                @SuppressWarnings("unchecked")
                List<Object> copyXrefData = ((List<Object> ) strategy.copy(LocatorUtils.property(locator, "xrefData", sourceXrefData), sourceXrefData));
                copy.xrefData = null;
                if (copyXrefData!= null) {
                    List<Object> uniqueXrefDatal = copy.getXrefData();
                    uniqueXrefDatal.addAll(copyXrefData);
                }
            } else {
                copy.xrefData = null;
            }
            if (this.cellInputType!= null) {
                String sourceCellInputType;
                sourceCellInputType = this.getCellInputType();
                String copyCellInputType = ((String) strategy.copy(LocatorUtils.property(locator, "cellInputType", sourceCellInputType), sourceCellInputType));
                copy.setCellInputType(copyCellInputType);
            } else {
                copy.cellInputType = null;
            }
            if (this.editable!= null) {
                Boolean sourceEditable;
                sourceEditable = this.isEditable();
                Boolean copyEditable = ((Boolean) strategy.copy(LocatorUtils.property(locator, "editable", sourceEditable), sourceEditable));
                copy.setEditable(copyEditable);
            } else {
                copy.editable = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MatrixData();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final MatrixData.Builder<_B> _other) {
        if (this.xAxisBoList == null) {
            _other.xAxisBoList = null;
        } else {
            _other.xAxisBoList = new ArrayList<MatrixXAxisObject.Builder<MatrixData.Builder<_B>>>();
            for (MatrixXAxisObject _item: this.xAxisBoList) {
                _other.xAxisBoList.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        if (this.yAxisBoList == null) {
            _other.yAxisBoList = null;
        } else {
            _other.yAxisBoList = new ArrayList<MatrixYAxisObject.Builder<MatrixData.Builder<_B>>>();
            for (MatrixYAxisObject _item: this.yAxisBoList) {
                _other.yAxisBoList.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        if (this.entityMatrixBoDict == null) {
            _other.entityMatrixBoDict = null;
        } else {
            _other.entityMatrixBoDict = new ArrayList<Buildable>();
            for (Object _item: this.entityMatrixBoDict) {
                _other.entityMatrixBoDict.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
            }
        }
        if (this.xrefData == null) {
            _other.xrefData = null;
        } else {
            _other.xrefData = new ArrayList<Buildable>();
            for (Object _item: this.xrefData) {
                _other.xrefData.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
            }
        }
        _other.cellInputType = this.cellInputType;
        _other.editable = this.editable;
    }

    public<_B >MatrixData.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new MatrixData.Builder<_B>(_parentBuilder, this, true);
    }

    public MatrixData.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static MatrixData.Builder<Void> builder() {
        return new MatrixData.Builder<Void>(null, null, false);
    }

    public static<_B >MatrixData.Builder<_B> copyOf(final MatrixData _other) {
        final MatrixData.Builder<_B> _newBuilder = new MatrixData.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final MatrixData.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree xAxisBoListPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("xAxisBoList"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(xAxisBoListPropertyTree!= null):((xAxisBoListPropertyTree == null)||(!xAxisBoListPropertyTree.isLeaf())))) {
            if (this.xAxisBoList == null) {
                _other.xAxisBoList = null;
            } else {
                _other.xAxisBoList = new ArrayList<MatrixXAxisObject.Builder<MatrixData.Builder<_B>>>();
                for (MatrixXAxisObject _item: this.xAxisBoList) {
                    _other.xAxisBoList.add(((_item == null)?null:_item.newCopyBuilder(_other, xAxisBoListPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree yAxisBoListPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("yAxisBoList"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(yAxisBoListPropertyTree!= null):((yAxisBoListPropertyTree == null)||(!yAxisBoListPropertyTree.isLeaf())))) {
            if (this.yAxisBoList == null) {
                _other.yAxisBoList = null;
            } else {
                _other.yAxisBoList = new ArrayList<MatrixYAxisObject.Builder<MatrixData.Builder<_B>>>();
                for (MatrixYAxisObject _item: this.yAxisBoList) {
                    _other.yAxisBoList.add(((_item == null)?null:_item.newCopyBuilder(_other, yAxisBoListPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree entityMatrixBoDictPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMatrixBoDict"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMatrixBoDictPropertyTree!= null):((entityMatrixBoDictPropertyTree == null)||(!entityMatrixBoDictPropertyTree.isLeaf())))) {
            if (this.entityMatrixBoDict == null) {
                _other.entityMatrixBoDict = null;
            } else {
                _other.entityMatrixBoDict = new ArrayList<Buildable>();
                for (Object _item: this.entityMatrixBoDict) {
                    _other.entityMatrixBoDict.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                }
            }
        }
        final PropertyTree xrefDataPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("xrefData"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(xrefDataPropertyTree!= null):((xrefDataPropertyTree == null)||(!xrefDataPropertyTree.isLeaf())))) {
            if (this.xrefData == null) {
                _other.xrefData = null;
            } else {
                _other.xrefData = new ArrayList<Buildable>();
                for (Object _item: this.xrefData) {
                    _other.xrefData.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                }
            }
        }
        final PropertyTree cellInputTypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("cellInputType"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(cellInputTypePropertyTree!= null):((cellInputTypePropertyTree == null)||(!cellInputTypePropertyTree.isLeaf())))) {
            _other.cellInputType = this.cellInputType;
        }
        final PropertyTree editablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("editable"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(editablePropertyTree!= null):((editablePropertyTree == null)||(!editablePropertyTree.isLeaf())))) {
            _other.editable = this.editable;
        }
    }

    public<_B >MatrixData.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new MatrixData.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public MatrixData.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >MatrixData.Builder<_B> copyOf(final MatrixData _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final MatrixData.Builder<_B> _newBuilder = new MatrixData.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static MatrixData.Builder<Void> copyExcept(final MatrixData _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static MatrixData.Builder<Void> copyOnly(final MatrixData _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final MatrixData _storedValue;
        private List<MatrixXAxisObject.Builder<MatrixData.Builder<_B>>> xAxisBoList;
        private List<MatrixYAxisObject.Builder<MatrixData.Builder<_B>>> yAxisBoList;
        private List<Buildable> entityMatrixBoDict;
        private List<Buildable> xrefData;
        private String cellInputType;
        private Boolean editable;

        public Builder(final _B _parentBuilder, final MatrixData _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.xAxisBoList == null) {
                        this.xAxisBoList = null;
                    } else {
                        this.xAxisBoList = new ArrayList<MatrixXAxisObject.Builder<MatrixData.Builder<_B>>>();
                        for (MatrixXAxisObject _item: _other.xAxisBoList) {
                            this.xAxisBoList.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    if (_other.yAxisBoList == null) {
                        this.yAxisBoList = null;
                    } else {
                        this.yAxisBoList = new ArrayList<MatrixYAxisObject.Builder<MatrixData.Builder<_B>>>();
                        for (MatrixYAxisObject _item: _other.yAxisBoList) {
                            this.yAxisBoList.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    if (_other.entityMatrixBoDict == null) {
                        this.entityMatrixBoDict = null;
                    } else {
                        this.entityMatrixBoDict = new ArrayList<Buildable>();
                        for (Object _item: _other.entityMatrixBoDict) {
                            this.entityMatrixBoDict.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                        }
                    }
                    if (_other.xrefData == null) {
                        this.xrefData = null;
                    } else {
                        this.xrefData = new ArrayList<Buildable>();
                        for (Object _item: _other.xrefData) {
                            this.xrefData.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                        }
                    }
                    this.cellInputType = _other.cellInputType;
                    this.editable = _other.editable;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final MatrixData _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree xAxisBoListPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("xAxisBoList"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(xAxisBoListPropertyTree!= null):((xAxisBoListPropertyTree == null)||(!xAxisBoListPropertyTree.isLeaf())))) {
                        if (_other.xAxisBoList == null) {
                            this.xAxisBoList = null;
                        } else {
                            this.xAxisBoList = new ArrayList<MatrixXAxisObject.Builder<MatrixData.Builder<_B>>>();
                            for (MatrixXAxisObject _item: _other.xAxisBoList) {
                                this.xAxisBoList.add(((_item == null)?null:_item.newCopyBuilder(this, xAxisBoListPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree yAxisBoListPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("yAxisBoList"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(yAxisBoListPropertyTree!= null):((yAxisBoListPropertyTree == null)||(!yAxisBoListPropertyTree.isLeaf())))) {
                        if (_other.yAxisBoList == null) {
                            this.yAxisBoList = null;
                        } else {
                            this.yAxisBoList = new ArrayList<MatrixYAxisObject.Builder<MatrixData.Builder<_B>>>();
                            for (MatrixYAxisObject _item: _other.yAxisBoList) {
                                this.yAxisBoList.add(((_item == null)?null:_item.newCopyBuilder(this, yAxisBoListPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree entityMatrixBoDictPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMatrixBoDict"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMatrixBoDictPropertyTree!= null):((entityMatrixBoDictPropertyTree == null)||(!entityMatrixBoDictPropertyTree.isLeaf())))) {
                        if (_other.entityMatrixBoDict == null) {
                            this.entityMatrixBoDict = null;
                        } else {
                            this.entityMatrixBoDict = new ArrayList<Buildable>();
                            for (Object _item: _other.entityMatrixBoDict) {
                                this.entityMatrixBoDict.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                            }
                        }
                    }
                    final PropertyTree xrefDataPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("xrefData"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(xrefDataPropertyTree!= null):((xrefDataPropertyTree == null)||(!xrefDataPropertyTree.isLeaf())))) {
                        if (_other.xrefData == null) {
                            this.xrefData = null;
                        } else {
                            this.xrefData = new ArrayList<Buildable>();
                            for (Object _item: _other.xrefData) {
                                this.xrefData.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                            }
                        }
                    }
                    final PropertyTree cellInputTypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("cellInputType"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(cellInputTypePropertyTree!= null):((cellInputTypePropertyTree == null)||(!cellInputTypePropertyTree.isLeaf())))) {
                        this.cellInputType = _other.cellInputType;
                    }
                    final PropertyTree editablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("editable"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(editablePropertyTree!= null):((editablePropertyTree == null)||(!editablePropertyTree.isLeaf())))) {
                        this.editable = _other.editable;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends MatrixData >_P init(final _P _product) {
            if (this.xAxisBoList!= null) {
                final List<MatrixXAxisObject> xAxisBoList = new ArrayList<MatrixXAxisObject>(this.xAxisBoList.size());
                for (MatrixXAxisObject.Builder<MatrixData.Builder<_B>> _item: this.xAxisBoList) {
                    xAxisBoList.add(_item.build());
                }
                _product.xAxisBoList = xAxisBoList;
            }
            if (this.yAxisBoList!= null) {
                final List<MatrixYAxisObject> yAxisBoList = new ArrayList<MatrixYAxisObject>(this.yAxisBoList.size());
                for (MatrixYAxisObject.Builder<MatrixData.Builder<_B>> _item: this.yAxisBoList) {
                    yAxisBoList.add(_item.build());
                }
                _product.yAxisBoList = yAxisBoList;
            }
            if (this.entityMatrixBoDict!= null) {
                final List<Object> entityMatrixBoDict = new ArrayList<Object>(this.entityMatrixBoDict.size());
                for (Buildable _item: this.entityMatrixBoDict) {
                    entityMatrixBoDict.add(((Object) _item.build()));
                }
                _product.entityMatrixBoDict = entityMatrixBoDict;
            }
            if (this.xrefData!= null) {
                final List<Object> xrefData = new ArrayList<Object>(this.xrefData.size());
                for (Buildable _item: this.xrefData) {
                    xrefData.add(((Object) _item.build()));
                }
                _product.xrefData = xrefData;
            }
            _product.cellInputType = this.cellInputType;
            _product.editable = this.editable;
            return _product;
        }

        /**
         * Adds the given items to the value of "xAxisBoList"
         * 
         * @param xAxisBoList
         *     Items to add to the value of the "xAxisBoList" property
         */
        public MatrixData.Builder<_B> addXAxisBoList(final Iterable<? extends MatrixXAxisObject> xAxisBoList) {
            if (xAxisBoList!= null) {
                if (this.xAxisBoList == null) {
                    this.xAxisBoList = new ArrayList<MatrixXAxisObject.Builder<MatrixData.Builder<_B>>>();
                }
                for (MatrixXAxisObject _item: xAxisBoList) {
                    this.xAxisBoList.add(new MatrixXAxisObject.Builder<MatrixData.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "xAxisBoList" (any previous value will be replaced)
         * 
         * @param xAxisBoList
         *     New value of the "xAxisBoList" property.
         */
        public MatrixData.Builder<_B> withXAxisBoList(final Iterable<? extends MatrixXAxisObject> xAxisBoList) {
            if (this.xAxisBoList!= null) {
                this.xAxisBoList.clear();
            }
            return addXAxisBoList(xAxisBoList);
        }

        /**
         * Adds the given items to the value of "xAxisBoList"
         * 
         * @param xAxisBoList
         *     Items to add to the value of the "xAxisBoList" property
         */
        public MatrixData.Builder<_B> addXAxisBoList(MatrixXAxisObject... xAxisBoList) {
            addXAxisBoList(Arrays.asList(xAxisBoList));
            return this;
        }

        /**
         * Sets the new value of "xAxisBoList" (any previous value will be replaced)
         * 
         * @param xAxisBoList
         *     New value of the "xAxisBoList" property.
         */
        public MatrixData.Builder<_B> withXAxisBoList(MatrixXAxisObject... xAxisBoList) {
            withXAxisBoList(Arrays.asList(xAxisBoList));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "XAxisBoList" property.
         * Use {@link org.nuclos.schema.rest.MatrixXAxisObject.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "XAxisBoList" property.
         *     Use {@link org.nuclos.schema.rest.MatrixXAxisObject.Builder#end()} to return to the current builder.
         */
        public MatrixXAxisObject.Builder<? extends MatrixData.Builder<_B>> addXAxisBoList() {
            if (this.xAxisBoList == null) {
                this.xAxisBoList = new ArrayList<MatrixXAxisObject.Builder<MatrixData.Builder<_B>>>();
            }
            final MatrixXAxisObject.Builder<MatrixData.Builder<_B>> xAxisBoList_Builder = new MatrixXAxisObject.Builder<MatrixData.Builder<_B>>(this, null, false);
            this.xAxisBoList.add(xAxisBoList_Builder);
            return xAxisBoList_Builder;
        }

        /**
         * Adds the given items to the value of "yAxisBoList"
         * 
         * @param yAxisBoList
         *     Items to add to the value of the "yAxisBoList" property
         */
        public MatrixData.Builder<_B> addYAxisBoList(final Iterable<? extends MatrixYAxisObject> yAxisBoList) {
            if (yAxisBoList!= null) {
                if (this.yAxisBoList == null) {
                    this.yAxisBoList = new ArrayList<MatrixYAxisObject.Builder<MatrixData.Builder<_B>>>();
                }
                for (MatrixYAxisObject _item: yAxisBoList) {
                    this.yAxisBoList.add(new MatrixYAxisObject.Builder<MatrixData.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "yAxisBoList" (any previous value will be replaced)
         * 
         * @param yAxisBoList
         *     New value of the "yAxisBoList" property.
         */
        public MatrixData.Builder<_B> withYAxisBoList(final Iterable<? extends MatrixYAxisObject> yAxisBoList) {
            if (this.yAxisBoList!= null) {
                this.yAxisBoList.clear();
            }
            return addYAxisBoList(yAxisBoList);
        }

        /**
         * Adds the given items to the value of "yAxisBoList"
         * 
         * @param yAxisBoList
         *     Items to add to the value of the "yAxisBoList" property
         */
        public MatrixData.Builder<_B> addYAxisBoList(MatrixYAxisObject... yAxisBoList) {
            addYAxisBoList(Arrays.asList(yAxisBoList));
            return this;
        }

        /**
         * Sets the new value of "yAxisBoList" (any previous value will be replaced)
         * 
         * @param yAxisBoList
         *     New value of the "yAxisBoList" property.
         */
        public MatrixData.Builder<_B> withYAxisBoList(MatrixYAxisObject... yAxisBoList) {
            withYAxisBoList(Arrays.asList(yAxisBoList));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "YAxisBoList" property.
         * Use {@link org.nuclos.schema.rest.MatrixYAxisObject.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "YAxisBoList" property.
         *     Use {@link org.nuclos.schema.rest.MatrixYAxisObject.Builder#end()} to return to the current builder.
         */
        public MatrixYAxisObject.Builder<? extends MatrixData.Builder<_B>> addYAxisBoList() {
            if (this.yAxisBoList == null) {
                this.yAxisBoList = new ArrayList<MatrixYAxisObject.Builder<MatrixData.Builder<_B>>>();
            }
            final MatrixYAxisObject.Builder<MatrixData.Builder<_B>> yAxisBoList_Builder = new MatrixYAxisObject.Builder<MatrixData.Builder<_B>>(this, null, false);
            this.yAxisBoList.add(yAxisBoList_Builder);
            return yAxisBoList_Builder;
        }

        /**
         * Adds the given items to the value of "entityMatrixBoDict"
         * 
         * @param entityMatrixBoDict
         *     Items to add to the value of the "entityMatrixBoDict" property
         */
        public MatrixData.Builder<_B> addEntityMatrixBoDict(final Iterable<?> entityMatrixBoDict) {
            if (entityMatrixBoDict!= null) {
                if (this.entityMatrixBoDict == null) {
                    this.entityMatrixBoDict = new ArrayList<Buildable>();
                }
                for (Object _item: entityMatrixBoDict) {
                    this.entityMatrixBoDict.add(new Buildable.PrimitiveBuildable(_item));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "entityMatrixBoDict" (any previous value will be replaced)
         * 
         * @param entityMatrixBoDict
         *     New value of the "entityMatrixBoDict" property.
         */
        public MatrixData.Builder<_B> withEntityMatrixBoDict(final Iterable<?> entityMatrixBoDict) {
            if (this.entityMatrixBoDict!= null) {
                this.entityMatrixBoDict.clear();
            }
            return addEntityMatrixBoDict(entityMatrixBoDict);
        }

        /**
         * Adds the given items to the value of "entityMatrixBoDict"
         * 
         * @param entityMatrixBoDict
         *     Items to add to the value of the "entityMatrixBoDict" property
         */
        public MatrixData.Builder<_B> addEntityMatrixBoDict(Object... entityMatrixBoDict) {
            addEntityMatrixBoDict(Arrays.asList(entityMatrixBoDict));
            return this;
        }

        /**
         * Sets the new value of "entityMatrixBoDict" (any previous value will be replaced)
         * 
         * @param entityMatrixBoDict
         *     New value of the "entityMatrixBoDict" property.
         */
        public MatrixData.Builder<_B> withEntityMatrixBoDict(Object... entityMatrixBoDict) {
            withEntityMatrixBoDict(Arrays.asList(entityMatrixBoDict));
            return this;
        }

        /**
         * Adds the given items to the value of "xrefData"
         * 
         * @param xrefData
         *     Items to add to the value of the "xrefData" property
         */
        public MatrixData.Builder<_B> addXrefData(final Iterable<?> xrefData) {
            if (xrefData!= null) {
                if (this.xrefData == null) {
                    this.xrefData = new ArrayList<Buildable>();
                }
                for (Object _item: xrefData) {
                    this.xrefData.add(new Buildable.PrimitiveBuildable(_item));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "xrefData" (any previous value will be replaced)
         * 
         * @param xrefData
         *     New value of the "xrefData" property.
         */
        public MatrixData.Builder<_B> withXrefData(final Iterable<?> xrefData) {
            if (this.xrefData!= null) {
                this.xrefData.clear();
            }
            return addXrefData(xrefData);
        }

        /**
         * Adds the given items to the value of "xrefData"
         * 
         * @param xrefData
         *     Items to add to the value of the "xrefData" property
         */
        public MatrixData.Builder<_B> addXrefData(Object... xrefData) {
            addXrefData(Arrays.asList(xrefData));
            return this;
        }

        /**
         * Sets the new value of "xrefData" (any previous value will be replaced)
         * 
         * @param xrefData
         *     New value of the "xrefData" property.
         */
        public MatrixData.Builder<_B> withXrefData(Object... xrefData) {
            withXrefData(Arrays.asList(xrefData));
            return this;
        }

        /**
         * Sets the new value of "cellInputType" (any previous value will be replaced)
         * 
         * @param cellInputType
         *     New value of the "cellInputType" property.
         */
        public MatrixData.Builder<_B> withCellInputType(final String cellInputType) {
            this.cellInputType = cellInputType;
            return this;
        }

        /**
         * Sets the new value of "editable" (any previous value will be replaced)
         * 
         * @param editable
         *     New value of the "editable" property.
         */
        public MatrixData.Builder<_B> withEditable(final Boolean editable) {
            this.editable = editable;
            return this;
        }

        @Override
        public MatrixData build() {
            if (_storedValue == null) {
                return this.init(new MatrixData());
            } else {
                return ((MatrixData) _storedValue);
            }
        }

        public MatrixData.Builder<_B> copyOf(final MatrixData _other) {
            _other.copyTo(this);
            return this;
        }

        public MatrixData.Builder<_B> copyOf(final MatrixData.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends MatrixData.Selector<MatrixData.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static MatrixData.Select _root() {
            return new MatrixData.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private MatrixXAxisObject.Selector<TRoot, MatrixData.Selector<TRoot, TParent>> xAxisBoList = null;
        private MatrixYAxisObject.Selector<TRoot, MatrixData.Selector<TRoot, TParent>> yAxisBoList = null;
        private com.kscs.util.jaxb.Selector<TRoot, MatrixData.Selector<TRoot, TParent>> entityMatrixBoDict = null;
        private com.kscs.util.jaxb.Selector<TRoot, MatrixData.Selector<TRoot, TParent>> xrefData = null;
        private com.kscs.util.jaxb.Selector<TRoot, MatrixData.Selector<TRoot, TParent>> cellInputType = null;
        private com.kscs.util.jaxb.Selector<TRoot, MatrixData.Selector<TRoot, TParent>> editable = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.xAxisBoList!= null) {
                products.put("xAxisBoList", this.xAxisBoList.init());
            }
            if (this.yAxisBoList!= null) {
                products.put("yAxisBoList", this.yAxisBoList.init());
            }
            if (this.entityMatrixBoDict!= null) {
                products.put("entityMatrixBoDict", this.entityMatrixBoDict.init());
            }
            if (this.xrefData!= null) {
                products.put("xrefData", this.xrefData.init());
            }
            if (this.cellInputType!= null) {
                products.put("cellInputType", this.cellInputType.init());
            }
            if (this.editable!= null) {
                products.put("editable", this.editable.init());
            }
            return products;
        }

        public MatrixXAxisObject.Selector<TRoot, MatrixData.Selector<TRoot, TParent>> xAxisBoList() {
            return ((this.xAxisBoList == null)?this.xAxisBoList = new MatrixXAxisObject.Selector<TRoot, MatrixData.Selector<TRoot, TParent>>(this._root, this, "xAxisBoList"):this.xAxisBoList);
        }

        public MatrixYAxisObject.Selector<TRoot, MatrixData.Selector<TRoot, TParent>> yAxisBoList() {
            return ((this.yAxisBoList == null)?this.yAxisBoList = new MatrixYAxisObject.Selector<TRoot, MatrixData.Selector<TRoot, TParent>>(this._root, this, "yAxisBoList"):this.yAxisBoList);
        }

        public com.kscs.util.jaxb.Selector<TRoot, MatrixData.Selector<TRoot, TParent>> entityMatrixBoDict() {
            return ((this.entityMatrixBoDict == null)?this.entityMatrixBoDict = new com.kscs.util.jaxb.Selector<TRoot, MatrixData.Selector<TRoot, TParent>>(this._root, this, "entityMatrixBoDict"):this.entityMatrixBoDict);
        }

        public com.kscs.util.jaxb.Selector<TRoot, MatrixData.Selector<TRoot, TParent>> xrefData() {
            return ((this.xrefData == null)?this.xrefData = new com.kscs.util.jaxb.Selector<TRoot, MatrixData.Selector<TRoot, TParent>>(this._root, this, "xrefData"):this.xrefData);
        }

        public com.kscs.util.jaxb.Selector<TRoot, MatrixData.Selector<TRoot, TParent>> cellInputType() {
            return ((this.cellInputType == null)?this.cellInputType = new com.kscs.util.jaxb.Selector<TRoot, MatrixData.Selector<TRoot, TParent>>(this._root, this, "cellInputType"):this.cellInputType);
        }

        public com.kscs.util.jaxb.Selector<TRoot, MatrixData.Selector<TRoot, TParent>> editable() {
            return ((this.editable == null)?this.editable = new com.kscs.util.jaxb.Selector<TRoot, MatrixData.Selector<TRoot, TParent>>(this._root, this, "editable"):this.editable);
        }

    }

}
