
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;


/**
 * <p>Java class for entity-object-result-list complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="entity-object-result-list"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="bos" type="{urn:org.nuclos.schema.rest}entity-object" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="all" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="total" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&gt;
 *       &lt;attribute name="canCreateBo" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "entity-object-result-list", propOrder = {
    "bos"
})
public class EntityObjectResultList implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected List<EntityObject> bos;
    @XmlAttribute(name = "all")
    protected Boolean all;
    @XmlAttribute(name = "total")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger total;
    @XmlAttribute(name = "canCreateBo")
    protected Boolean canCreateBo;

    /**
     * Gets the value of the bos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EntityObject }
     * 
     * 
     */
    public List<EntityObject> getBos() {
        if (bos == null) {
            bos = new ArrayList<EntityObject>();
        }
        return this.bos;
    }

    /**
     * Gets the value of the all property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAll() {
        return all;
    }

    /**
     * Sets the value of the all property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAll(Boolean value) {
        this.all = value;
    }

    /**
     * Gets the value of the total property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTotal() {
        return total;
    }

    /**
     * Sets the value of the total property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTotal(BigInteger value) {
        this.total = value;
    }

    /**
     * Gets the value of the canCreateBo property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCanCreateBo() {
        return canCreateBo;
    }

    /**
     * Sets the value of the canCreateBo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCanCreateBo(Boolean value) {
        this.canCreateBo = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<EntityObject> theBos;
            theBos = (((this.bos!= null)&&(!this.bos.isEmpty()))?this.getBos():null);
            strategy.appendField(locator, this, "bos", buffer, theBos);
        }
        {
            Boolean theAll;
            theAll = this.isAll();
            strategy.appendField(locator, this, "all", buffer, theAll);
        }
        {
            BigInteger theTotal;
            theTotal = this.getTotal();
            strategy.appendField(locator, this, "total", buffer, theTotal);
        }
        {
            Boolean theCanCreateBo;
            theCanCreateBo = this.isCanCreateBo();
            strategy.appendField(locator, this, "canCreateBo", buffer, theCanCreateBo);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof EntityObjectResultList)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final EntityObjectResultList that = ((EntityObjectResultList) object);
        {
            List<EntityObject> lhsBos;
            lhsBos = (((this.bos!= null)&&(!this.bos.isEmpty()))?this.getBos():null);
            List<EntityObject> rhsBos;
            rhsBos = (((that.bos!= null)&&(!that.bos.isEmpty()))?that.getBos():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "bos", lhsBos), LocatorUtils.property(thatLocator, "bos", rhsBos), lhsBos, rhsBos)) {
                return false;
            }
        }
        {
            Boolean lhsAll;
            lhsAll = this.isAll();
            Boolean rhsAll;
            rhsAll = that.isAll();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "all", lhsAll), LocatorUtils.property(thatLocator, "all", rhsAll), lhsAll, rhsAll)) {
                return false;
            }
        }
        {
            BigInteger lhsTotal;
            lhsTotal = this.getTotal();
            BigInteger rhsTotal;
            rhsTotal = that.getTotal();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "total", lhsTotal), LocatorUtils.property(thatLocator, "total", rhsTotal), lhsTotal, rhsTotal)) {
                return false;
            }
        }
        {
            Boolean lhsCanCreateBo;
            lhsCanCreateBo = this.isCanCreateBo();
            Boolean rhsCanCreateBo;
            rhsCanCreateBo = that.isCanCreateBo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "canCreateBo", lhsCanCreateBo), LocatorUtils.property(thatLocator, "canCreateBo", rhsCanCreateBo), lhsCanCreateBo, rhsCanCreateBo)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<EntityObject> theBos;
            theBos = (((this.bos!= null)&&(!this.bos.isEmpty()))?this.getBos():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "bos", theBos), currentHashCode, theBos);
        }
        {
            Boolean theAll;
            theAll = this.isAll();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "all", theAll), currentHashCode, theAll);
        }
        {
            BigInteger theTotal;
            theTotal = this.getTotal();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "total", theTotal), currentHashCode, theTotal);
        }
        {
            Boolean theCanCreateBo;
            theCanCreateBo = this.isCanCreateBo();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "canCreateBo", theCanCreateBo), currentHashCode, theCanCreateBo);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof EntityObjectResultList) {
            final EntityObjectResultList copy = ((EntityObjectResultList) draftCopy);
            if ((this.bos!= null)&&(!this.bos.isEmpty())) {
                List<EntityObject> sourceBos;
                sourceBos = (((this.bos!= null)&&(!this.bos.isEmpty()))?this.getBos():null);
                @SuppressWarnings("unchecked")
                List<EntityObject> copyBos = ((List<EntityObject> ) strategy.copy(LocatorUtils.property(locator, "bos", sourceBos), sourceBos));
                copy.bos = null;
                if (copyBos!= null) {
                    List<EntityObject> uniqueBosl = copy.getBos();
                    uniqueBosl.addAll(copyBos);
                }
            } else {
                copy.bos = null;
            }
            if (this.all!= null) {
                Boolean sourceAll;
                sourceAll = this.isAll();
                Boolean copyAll = ((Boolean) strategy.copy(LocatorUtils.property(locator, "all", sourceAll), sourceAll));
                copy.setAll(copyAll);
            } else {
                copy.all = null;
            }
            if (this.total!= null) {
                BigInteger sourceTotal;
                sourceTotal = this.getTotal();
                BigInteger copyTotal = ((BigInteger) strategy.copy(LocatorUtils.property(locator, "total", sourceTotal), sourceTotal));
                copy.setTotal(copyTotal);
            } else {
                copy.total = null;
            }
            if (this.canCreateBo!= null) {
                Boolean sourceCanCreateBo;
                sourceCanCreateBo = this.isCanCreateBo();
                Boolean copyCanCreateBo = ((Boolean) strategy.copy(LocatorUtils.property(locator, "canCreateBo", sourceCanCreateBo), sourceCanCreateBo));
                copy.setCanCreateBo(copyCanCreateBo);
            } else {
                copy.canCreateBo = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new EntityObjectResultList();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityObjectResultList.Builder<_B> _other) {
        if (this.bos == null) {
            _other.bos = null;
        } else {
            _other.bos = new ArrayList<EntityObject.Builder<EntityObjectResultList.Builder<_B>>>();
            for (EntityObject _item: this.bos) {
                _other.bos.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.all = this.all;
        _other.total = this.total;
        _other.canCreateBo = this.canCreateBo;
    }

    public<_B >EntityObjectResultList.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new EntityObjectResultList.Builder<_B>(_parentBuilder, this, true);
    }

    public EntityObjectResultList.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static EntityObjectResultList.Builder<Void> builder() {
        return new EntityObjectResultList.Builder<Void>(null, null, false);
    }

    public static<_B >EntityObjectResultList.Builder<_B> copyOf(final EntityObjectResultList _other) {
        final EntityObjectResultList.Builder<_B> _newBuilder = new EntityObjectResultList.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityObjectResultList.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree bosPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("bos"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(bosPropertyTree!= null):((bosPropertyTree == null)||(!bosPropertyTree.isLeaf())))) {
            if (this.bos == null) {
                _other.bos = null;
            } else {
                _other.bos = new ArrayList<EntityObject.Builder<EntityObjectResultList.Builder<_B>>>();
                for (EntityObject _item: this.bos) {
                    _other.bos.add(((_item == null)?null:_item.newCopyBuilder(_other, bosPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree allPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("all"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(allPropertyTree!= null):((allPropertyTree == null)||(!allPropertyTree.isLeaf())))) {
            _other.all = this.all;
        }
        final PropertyTree totalPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("total"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(totalPropertyTree!= null):((totalPropertyTree == null)||(!totalPropertyTree.isLeaf())))) {
            _other.total = this.total;
        }
        final PropertyTree canCreateBoPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("canCreateBo"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(canCreateBoPropertyTree!= null):((canCreateBoPropertyTree == null)||(!canCreateBoPropertyTree.isLeaf())))) {
            _other.canCreateBo = this.canCreateBo;
        }
    }

    public<_B >EntityObjectResultList.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new EntityObjectResultList.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public EntityObjectResultList.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >EntityObjectResultList.Builder<_B> copyOf(final EntityObjectResultList _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final EntityObjectResultList.Builder<_B> _newBuilder = new EntityObjectResultList.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static EntityObjectResultList.Builder<Void> copyExcept(final EntityObjectResultList _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static EntityObjectResultList.Builder<Void> copyOnly(final EntityObjectResultList _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final EntityObjectResultList _storedValue;
        private List<EntityObject.Builder<EntityObjectResultList.Builder<_B>>> bos;
        private Boolean all;
        private BigInteger total;
        private Boolean canCreateBo;

        public Builder(final _B _parentBuilder, final EntityObjectResultList _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.bos == null) {
                        this.bos = null;
                    } else {
                        this.bos = new ArrayList<EntityObject.Builder<EntityObjectResultList.Builder<_B>>>();
                        for (EntityObject _item: _other.bos) {
                            this.bos.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.all = _other.all;
                    this.total = _other.total;
                    this.canCreateBo = _other.canCreateBo;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final EntityObjectResultList _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree bosPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("bos"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(bosPropertyTree!= null):((bosPropertyTree == null)||(!bosPropertyTree.isLeaf())))) {
                        if (_other.bos == null) {
                            this.bos = null;
                        } else {
                            this.bos = new ArrayList<EntityObject.Builder<EntityObjectResultList.Builder<_B>>>();
                            for (EntityObject _item: _other.bos) {
                                this.bos.add(((_item == null)?null:_item.newCopyBuilder(this, bosPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree allPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("all"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(allPropertyTree!= null):((allPropertyTree == null)||(!allPropertyTree.isLeaf())))) {
                        this.all = _other.all;
                    }
                    final PropertyTree totalPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("total"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(totalPropertyTree!= null):((totalPropertyTree == null)||(!totalPropertyTree.isLeaf())))) {
                        this.total = _other.total;
                    }
                    final PropertyTree canCreateBoPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("canCreateBo"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(canCreateBoPropertyTree!= null):((canCreateBoPropertyTree == null)||(!canCreateBoPropertyTree.isLeaf())))) {
                        this.canCreateBo = _other.canCreateBo;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends EntityObjectResultList >_P init(final _P _product) {
            if (this.bos!= null) {
                final List<EntityObject> bos = new ArrayList<EntityObject>(this.bos.size());
                for (EntityObject.Builder<EntityObjectResultList.Builder<_B>> _item: this.bos) {
                    bos.add(_item.build());
                }
                _product.bos = bos;
            }
            _product.all = this.all;
            _product.total = this.total;
            _product.canCreateBo = this.canCreateBo;
            return _product;
        }

        /**
         * Adds the given items to the value of "bos"
         * 
         * @param bos
         *     Items to add to the value of the "bos" property
         */
        public EntityObjectResultList.Builder<_B> addBos(final Iterable<? extends EntityObject> bos) {
            if (bos!= null) {
                if (this.bos == null) {
                    this.bos = new ArrayList<EntityObject.Builder<EntityObjectResultList.Builder<_B>>>();
                }
                for (EntityObject _item: bos) {
                    this.bos.add(new EntityObject.Builder<EntityObjectResultList.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "bos" (any previous value will be replaced)
         * 
         * @param bos
         *     New value of the "bos" property.
         */
        public EntityObjectResultList.Builder<_B> withBos(final Iterable<? extends EntityObject> bos) {
            if (this.bos!= null) {
                this.bos.clear();
            }
            return addBos(bos);
        }

        /**
         * Adds the given items to the value of "bos"
         * 
         * @param bos
         *     Items to add to the value of the "bos" property
         */
        public EntityObjectResultList.Builder<_B> addBos(EntityObject... bos) {
            addBos(Arrays.asList(bos));
            return this;
        }

        /**
         * Sets the new value of "bos" (any previous value will be replaced)
         * 
         * @param bos
         *     New value of the "bos" property.
         */
        public EntityObjectResultList.Builder<_B> withBos(EntityObject... bos) {
            withBos(Arrays.asList(bos));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Bos" property.
         * Use {@link org.nuclos.schema.rest.EntityObject.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Bos" property.
         *     Use {@link org.nuclos.schema.rest.EntityObject.Builder#end()} to return to the current builder.
         */
        public EntityObject.Builder<? extends EntityObjectResultList.Builder<_B>> addBos() {
            if (this.bos == null) {
                this.bos = new ArrayList<EntityObject.Builder<EntityObjectResultList.Builder<_B>>>();
            }
            final EntityObject.Builder<EntityObjectResultList.Builder<_B>> bos_Builder = new EntityObject.Builder<EntityObjectResultList.Builder<_B>>(this, null, false);
            this.bos.add(bos_Builder);
            return bos_Builder;
        }

        /**
         * Sets the new value of "all" (any previous value will be replaced)
         * 
         * @param all
         *     New value of the "all" property.
         */
        public EntityObjectResultList.Builder<_B> withAll(final Boolean all) {
            this.all = all;
            return this;
        }

        /**
         * Sets the new value of "total" (any previous value will be replaced)
         * 
         * @param total
         *     New value of the "total" property.
         */
        public EntityObjectResultList.Builder<_B> withTotal(final BigInteger total) {
            this.total = total;
            return this;
        }

        /**
         * Sets the new value of "canCreateBo" (any previous value will be replaced)
         * 
         * @param canCreateBo
         *     New value of the "canCreateBo" property.
         */
        public EntityObjectResultList.Builder<_B> withCanCreateBo(final Boolean canCreateBo) {
            this.canCreateBo = canCreateBo;
            return this;
        }

        @Override
        public EntityObjectResultList build() {
            if (_storedValue == null) {
                return this.init(new EntityObjectResultList());
            } else {
                return ((EntityObjectResultList) _storedValue);
            }
        }

        public EntityObjectResultList.Builder<_B> copyOf(final EntityObjectResultList _other) {
            _other.copyTo(this);
            return this;
        }

        public EntityObjectResultList.Builder<_B> copyOf(final EntityObjectResultList.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends EntityObjectResultList.Selector<EntityObjectResultList.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static EntityObjectResultList.Select _root() {
            return new EntityObjectResultList.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private EntityObject.Selector<TRoot, EntityObjectResultList.Selector<TRoot, TParent>> bos = null;
        private com.kscs.util.jaxb.Selector<TRoot, EntityObjectResultList.Selector<TRoot, TParent>> all = null;
        private com.kscs.util.jaxb.Selector<TRoot, EntityObjectResultList.Selector<TRoot, TParent>> total = null;
        private com.kscs.util.jaxb.Selector<TRoot, EntityObjectResultList.Selector<TRoot, TParent>> canCreateBo = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.bos!= null) {
                products.put("bos", this.bos.init());
            }
            if (this.all!= null) {
                products.put("all", this.all.init());
            }
            if (this.total!= null) {
                products.put("total", this.total.init());
            }
            if (this.canCreateBo!= null) {
                products.put("canCreateBo", this.canCreateBo.init());
            }
            return products;
        }

        public EntityObject.Selector<TRoot, EntityObjectResultList.Selector<TRoot, TParent>> bos() {
            return ((this.bos == null)?this.bos = new EntityObject.Selector<TRoot, EntityObjectResultList.Selector<TRoot, TParent>>(this._root, this, "bos"):this.bos);
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityObjectResultList.Selector<TRoot, TParent>> all() {
            return ((this.all == null)?this.all = new com.kscs.util.jaxb.Selector<TRoot, EntityObjectResultList.Selector<TRoot, TParent>>(this._root, this, "all"):this.all);
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityObjectResultList.Selector<TRoot, TParent>> total() {
            return ((this.total == null)?this.total = new com.kscs.util.jaxb.Selector<TRoot, EntityObjectResultList.Selector<TRoot, TParent>>(this._root, this, "total"):this.total);
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityObjectResultList.Selector<TRoot, TParent>> canCreateBo() {
            return ((this.canCreateBo == null)?this.canCreateBo = new com.kscs.util.jaxb.Selector<TRoot, EntityObjectResultList.Selector<TRoot, TParent>>(this._root, this, "canCreateBo"):this.canCreateBo);
        }

    }

}
