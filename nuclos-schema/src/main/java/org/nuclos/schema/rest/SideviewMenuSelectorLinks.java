
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;


/**
 * <p>Java class for sideview-menu-selector-links complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="sideview-menu-selector-links"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="tree" type="{urn:org.nuclos.schema.rest}rest-link"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sideview-menu-selector-links", propOrder = {
    "tree"
})
public class SideviewMenuSelectorLinks implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected RestLink tree;

    /**
     * Gets the value of the tree property.
     * 
     * @return
     *     possible object is
     *     {@link RestLink }
     *     
     */
    public RestLink getTree() {
        return tree;
    }

    /**
     * Sets the value of the tree property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestLink }
     *     
     */
    public void setTree(RestLink value) {
        this.tree = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            RestLink theTree;
            theTree = this.getTree();
            strategy.appendField(locator, this, "tree", buffer, theTree);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof SideviewMenuSelectorLinks)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final SideviewMenuSelectorLinks that = ((SideviewMenuSelectorLinks) object);
        {
            RestLink lhsTree;
            lhsTree = this.getTree();
            RestLink rhsTree;
            rhsTree = that.getTree();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "tree", lhsTree), LocatorUtils.property(thatLocator, "tree", rhsTree), lhsTree, rhsTree)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            RestLink theTree;
            theTree = this.getTree();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "tree", theTree), currentHashCode, theTree);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof SideviewMenuSelectorLinks) {
            final SideviewMenuSelectorLinks copy = ((SideviewMenuSelectorLinks) draftCopy);
            if (this.tree!= null) {
                RestLink sourceTree;
                sourceTree = this.getTree();
                RestLink copyTree = ((RestLink) strategy.copy(LocatorUtils.property(locator, "tree", sourceTree), sourceTree));
                copy.setTree(copyTree);
            } else {
                copy.tree = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new SideviewMenuSelectorLinks();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final SideviewMenuSelectorLinks.Builder<_B> _other) {
        _other.tree = ((this.tree == null)?null:this.tree.newCopyBuilder(_other));
    }

    public<_B >SideviewMenuSelectorLinks.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new SideviewMenuSelectorLinks.Builder<_B>(_parentBuilder, this, true);
    }

    public SideviewMenuSelectorLinks.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static SideviewMenuSelectorLinks.Builder<Void> builder() {
        return new SideviewMenuSelectorLinks.Builder<Void>(null, null, false);
    }

    public static<_B >SideviewMenuSelectorLinks.Builder<_B> copyOf(final SideviewMenuSelectorLinks _other) {
        final SideviewMenuSelectorLinks.Builder<_B> _newBuilder = new SideviewMenuSelectorLinks.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final SideviewMenuSelectorLinks.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree treePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("tree"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(treePropertyTree!= null):((treePropertyTree == null)||(!treePropertyTree.isLeaf())))) {
            _other.tree = ((this.tree == null)?null:this.tree.newCopyBuilder(_other, treePropertyTree, _propertyTreeUse));
        }
    }

    public<_B >SideviewMenuSelectorLinks.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new SideviewMenuSelectorLinks.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public SideviewMenuSelectorLinks.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >SideviewMenuSelectorLinks.Builder<_B> copyOf(final SideviewMenuSelectorLinks _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final SideviewMenuSelectorLinks.Builder<_B> _newBuilder = new SideviewMenuSelectorLinks.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static SideviewMenuSelectorLinks.Builder<Void> copyExcept(final SideviewMenuSelectorLinks _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static SideviewMenuSelectorLinks.Builder<Void> copyOnly(final SideviewMenuSelectorLinks _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final SideviewMenuSelectorLinks _storedValue;
        private RestLink.Builder<SideviewMenuSelectorLinks.Builder<_B>> tree;

        public Builder(final _B _parentBuilder, final SideviewMenuSelectorLinks _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.tree = ((_other.tree == null)?null:_other.tree.newCopyBuilder(this));
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final SideviewMenuSelectorLinks _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree treePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("tree"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(treePropertyTree!= null):((treePropertyTree == null)||(!treePropertyTree.isLeaf())))) {
                        this.tree = ((_other.tree == null)?null:_other.tree.newCopyBuilder(this, treePropertyTree, _propertyTreeUse));
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends SideviewMenuSelectorLinks >_P init(final _P _product) {
            _product.tree = ((this.tree == null)?null:this.tree.build());
            return _product;
        }

        /**
         * Sets the new value of "tree" (any previous value will be replaced)
         * 
         * @param tree
         *     New value of the "tree" property.
         */
        public SideviewMenuSelectorLinks.Builder<_B> withTree(final RestLink tree) {
            this.tree = ((tree == null)?null:new RestLink.Builder<SideviewMenuSelectorLinks.Builder<_B>>(this, tree, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "tree" property.
         * Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "tree" property.
         *     Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         */
        public RestLink.Builder<? extends SideviewMenuSelectorLinks.Builder<_B>> withTree() {
            if (this.tree!= null) {
                return this.tree;
            }
            return this.tree = new RestLink.Builder<SideviewMenuSelectorLinks.Builder<_B>>(this, null, false);
        }

        @Override
        public SideviewMenuSelectorLinks build() {
            if (_storedValue == null) {
                return this.init(new SideviewMenuSelectorLinks());
            } else {
                return ((SideviewMenuSelectorLinks) _storedValue);
            }
        }

        public SideviewMenuSelectorLinks.Builder<_B> copyOf(final SideviewMenuSelectorLinks _other) {
            _other.copyTo(this);
            return this;
        }

        public SideviewMenuSelectorLinks.Builder<_B> copyOf(final SideviewMenuSelectorLinks.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends SideviewMenuSelectorLinks.Selector<SideviewMenuSelectorLinks.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static SideviewMenuSelectorLinks.Select _root() {
            return new SideviewMenuSelectorLinks.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private RestLink.Selector<TRoot, SideviewMenuSelectorLinks.Selector<TRoot, TParent>> tree = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.tree!= null) {
                products.put("tree", this.tree.init());
            }
            return products;
        }

        public RestLink.Selector<TRoot, SideviewMenuSelectorLinks.Selector<TRoot, TParent>> tree() {
            return ((this.tree == null)?this.tree = new RestLink.Selector<TRoot, SideviewMenuSelectorLinks.Selector<TRoot, TParent>>(this._root, this, "tree"):this.tree);
        }

    }

}
