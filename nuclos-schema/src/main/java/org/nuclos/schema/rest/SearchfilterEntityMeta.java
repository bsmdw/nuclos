
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;


/**
 * <p>Java class for searchfilter-entity-meta complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="searchfilter-entity-meta"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:org.nuclos.schema.rest}entity-meta-overview"&gt;
 *       &lt;attribute name="searchfilter" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "searchfilter-entity-meta")
@XmlSeeAlso({
    TaskEntityMeta.class
})
public class SearchfilterEntityMeta
    extends EntityMetaOverview
    implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "searchfilter")
    protected String searchfilter;

    /**
     * Gets the value of the searchfilter property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSearchfilter() {
        return searchfilter;
    }

    /**
     * Sets the value of the searchfilter property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSearchfilter(String value) {
        this.searchfilter = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        super.appendFields(locator, buffer, strategy);
        {
            String theSearchfilter;
            theSearchfilter = this.getSearchfilter();
            strategy.appendField(locator, this, "searchfilter", buffer, theSearchfilter);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof SearchfilterEntityMeta)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final SearchfilterEntityMeta that = ((SearchfilterEntityMeta) object);
        {
            String lhsSearchfilter;
            lhsSearchfilter = this.getSearchfilter();
            String rhsSearchfilter;
            rhsSearchfilter = that.getSearchfilter();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "searchfilter", lhsSearchfilter), LocatorUtils.property(thatLocator, "searchfilter", rhsSearchfilter), lhsSearchfilter, rhsSearchfilter)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = super.hashCode(locator, strategy);
        {
            String theSearchfilter;
            theSearchfilter = this.getSearchfilter();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "searchfilter", theSearchfilter), currentHashCode, theSearchfilter);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof SearchfilterEntityMeta) {
            final SearchfilterEntityMeta copy = ((SearchfilterEntityMeta) draftCopy);
            if (this.searchfilter!= null) {
                String sourceSearchfilter;
                sourceSearchfilter = this.getSearchfilter();
                String copySearchfilter = ((String) strategy.copy(LocatorUtils.property(locator, "searchfilter", sourceSearchfilter), sourceSearchfilter));
                copy.setSearchfilter(copySearchfilter);
            } else {
                copy.searchfilter = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new SearchfilterEntityMeta();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final SearchfilterEntityMeta.Builder<_B> _other) {
        super.copyTo(_other);
        _other.searchfilter = this.searchfilter;
    }

    @Override
    public<_B >SearchfilterEntityMeta.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new SearchfilterEntityMeta.Builder<_B>(_parentBuilder, this, true);
    }

    @Override
    public SearchfilterEntityMeta.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static SearchfilterEntityMeta.Builder<Void> builder() {
        return new SearchfilterEntityMeta.Builder<Void>(null, null, false);
    }

    public static<_B >SearchfilterEntityMeta.Builder<_B> copyOf(final EntityMetaBase _other) {
        final SearchfilterEntityMeta.Builder<_B> _newBuilder = new SearchfilterEntityMeta.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >SearchfilterEntityMeta.Builder<_B> copyOf(final EntityMetaOverview _other) {
        final SearchfilterEntityMeta.Builder<_B> _newBuilder = new SearchfilterEntityMeta.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >SearchfilterEntityMeta.Builder<_B> copyOf(final SearchfilterEntityMeta _other) {
        final SearchfilterEntityMeta.Builder<_B> _newBuilder = new SearchfilterEntityMeta.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final SearchfilterEntityMeta.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        super.copyTo(_other, _propertyTree, _propertyTreeUse);
        final PropertyTree searchfilterPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("searchfilter"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(searchfilterPropertyTree!= null):((searchfilterPropertyTree == null)||(!searchfilterPropertyTree.isLeaf())))) {
            _other.searchfilter = this.searchfilter;
        }
    }

    @Override
    public<_B >SearchfilterEntityMeta.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new SearchfilterEntityMeta.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    @Override
    public SearchfilterEntityMeta.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >SearchfilterEntityMeta.Builder<_B> copyOf(final EntityMetaBase _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final SearchfilterEntityMeta.Builder<_B> _newBuilder = new SearchfilterEntityMeta.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >SearchfilterEntityMeta.Builder<_B> copyOf(final EntityMetaOverview _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final SearchfilterEntityMeta.Builder<_B> _newBuilder = new SearchfilterEntityMeta.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >SearchfilterEntityMeta.Builder<_B> copyOf(final SearchfilterEntityMeta _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final SearchfilterEntityMeta.Builder<_B> _newBuilder = new SearchfilterEntityMeta.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static SearchfilterEntityMeta.Builder<Void> copyExcept(final EntityMetaBase _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static SearchfilterEntityMeta.Builder<Void> copyExcept(final EntityMetaOverview _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static SearchfilterEntityMeta.Builder<Void> copyExcept(final SearchfilterEntityMeta _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static SearchfilterEntityMeta.Builder<Void> copyOnly(final EntityMetaBase _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static SearchfilterEntityMeta.Builder<Void> copyOnly(final EntityMetaOverview _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static SearchfilterEntityMeta.Builder<Void> copyOnly(final SearchfilterEntityMeta _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >
        extends EntityMetaOverview.Builder<_B>
        implements Buildable
    {

        private String searchfilter;

        public Builder(final _B _parentBuilder, final SearchfilterEntityMeta _other, final boolean _copy) {
            super(_parentBuilder, _other, _copy);
            if (_other!= null) {
                this.searchfilter = _other.searchfilter;
            }
        }

        public Builder(final _B _parentBuilder, final SearchfilterEntityMeta _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            super(_parentBuilder, _other, _copy, _propertyTree, _propertyTreeUse);
            if (_other!= null) {
                final PropertyTree searchfilterPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("searchfilter"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(searchfilterPropertyTree!= null):((searchfilterPropertyTree == null)||(!searchfilterPropertyTree.isLeaf())))) {
                    this.searchfilter = _other.searchfilter;
                }
            }
        }

        protected<_P extends SearchfilterEntityMeta >_P init(final _P _product) {
            _product.searchfilter = this.searchfilter;
            return super.init(_product);
        }

        /**
         * Sets the new value of "searchfilter" (any previous value will be replaced)
         * 
         * @param searchfilter
         *     New value of the "searchfilter" property.
         */
        public SearchfilterEntityMeta.Builder<_B> withSearchfilter(final String searchfilter) {
            this.searchfilter = searchfilter;
            return this;
        }

        /**
         * Sets the new value of "processMetaId" (any previous value will be replaced)
         * 
         * @param processMetaId
         *     New value of the "processMetaId" property.
         */
        @Override
        public SearchfilterEntityMeta.Builder<_B> withProcessMetaId(final String processMetaId) {
            super.withProcessMetaId(processMetaId);
            return this;
        }

        /**
         * Sets the new value of "links" (any previous value will be replaced)
         * 
         * @param links
         *     New value of the "links" property.
         */
        @Override
        public SearchfilterEntityMeta.Builder<_B> withLinks(final EntityMetaBaseLinks links) {
            super.withLinks(links);
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "links" property.
         * Use {@link org.nuclos.schema.rest.EntityMetaBaseLinks.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "links" property.
         *     Use {@link org.nuclos.schema.rest.EntityMetaBaseLinks.Builder#end()} to return to the current builder.
         */
        public EntityMetaBaseLinks.Builder<? extends SearchfilterEntityMeta.Builder<_B>> withLinks() {
            return ((EntityMetaBaseLinks.Builder<? extends SearchfilterEntityMeta.Builder<_B>> ) super.withLinks());
        }

        /**
         * Sets the new value of "boMetaId" (any previous value will be replaced)
         * 
         * @param boMetaId
         *     New value of the "boMetaId" property.
         */
        @Override
        public SearchfilterEntityMeta.Builder<_B> withBoMetaId(final String boMetaId) {
            super.withBoMetaId(boMetaId);
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        @Override
        public SearchfilterEntityMeta.Builder<_B> withName(final String name) {
            super.withName(name);
            return this;
        }

        /**
         * Sets the new value of "createNew" (any previous value will be replaced)
         * 
         * @param createNew
         *     New value of the "createNew" property.
         */
        @Override
        public SearchfilterEntityMeta.Builder<_B> withCreateNew(final boolean createNew) {
            super.withCreateNew(createNew);
            return this;
        }

        @Override
        public SearchfilterEntityMeta build() {
            if (_storedValue == null) {
                return this.init(new SearchfilterEntityMeta());
            } else {
                return ((SearchfilterEntityMeta) _storedValue);
            }
        }

        public SearchfilterEntityMeta.Builder<_B> copyOf(final SearchfilterEntityMeta _other) {
            _other.copyTo(this);
            return this;
        }

        public SearchfilterEntityMeta.Builder<_B> copyOf(final SearchfilterEntityMeta.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends SearchfilterEntityMeta.Selector<SearchfilterEntityMeta.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static SearchfilterEntityMeta.Select _root() {
            return new SearchfilterEntityMeta.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends EntityMetaOverview.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, SearchfilterEntityMeta.Selector<TRoot, TParent>> searchfilter = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.searchfilter!= null) {
                products.put("searchfilter", this.searchfilter.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, SearchfilterEntityMeta.Selector<TRoot, TParent>> searchfilter() {
            return ((this.searchfilter == null)?this.searchfilter = new com.kscs.util.jaxb.Selector<TRoot, SearchfilterEntityMeta.Selector<TRoot, TParent>>(this._root, this, "searchfilter"):this.searchfilter);
        }

    }

}
