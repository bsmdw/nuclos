import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ${ADDON_NAME_CAMEL_CASE}Service } from './${ADDON_FILE_NAME}.service';
import { ${ADDON_NAME_CAMEL_CASE}Component } from './${ADDON_FILE_NAME}.component';

export function ${ADDON_NAME_CAMEL_CASE}ServiceFactory() {
	return new ${ADDON_NAME_CAMEL_CASE}Service();
}

@NgModule({
	imports: [
		CommonModule
	],
	providers: [
		{ provide: ${ADDON_NAME_CAMEL_CASE}Service, useFactory: ${ADDON_NAME_CAMEL_CASE}ServiceFactory }
	],
	declarations: [
		${ADDON_NAME_CAMEL_CASE}Component
	],
	exports: [
		${ADDON_NAME_CAMEL_CASE}Component
	],
	entryComponents: [
		${ADDON_NAME_CAMEL_CASE}Component
	]
})
export class ${ADDON_NAME_CAMEL_CASE}Module { }
