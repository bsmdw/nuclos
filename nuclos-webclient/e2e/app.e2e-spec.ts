import { NuclosWebclient2Page } from './app.po';

describe('nuclos-webclient2 App', function() {
  let page: NuclosWebclient2Page;

  beforeEach(() => {
    page = new NuclosWebclient2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
