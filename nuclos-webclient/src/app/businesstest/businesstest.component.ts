///<reference path="../../../node_modules/@types/oboe/index.d.ts"/>
import { Component, OnInit } from '@angular/core';
import { concat as observableConcat, Observable } from 'rxjs';
import { BusinesstestLogHandler, BusinesstestService } from './shared/businesstest.service';

@Component({
	selector: 'nuc-businesstest',
	templateUrl: './businesstest.component.html',
	styleUrls: ['./businesstest.component.css']
})
export class BusinesstestComponent implements OnInit {
	started;
	progress;
	executionResult = '';
	logHandler: BusinesstestLogHandler;

	constructor(private businesstestService: BusinesstestService) {
		this.started = false;
		this.progress = 0;

		this.logHandler = {
			start: () => {
				this.progress = 0;
				this.executionResult = '';
			},
			receiveMessage: message => {
				this.executionResult += message;
				let output = $('#output');
				if (output.length) {
					output.scrollTop(output[0].scrollHeight - output.height());
				}
			},
			receiveProgress: progress => {
				this.progress = progress;
			},
			done: () => {
				this.progress = 100;
			},
			fail: () => {
			}
		};
	}

	ngOnInit() {
	}

	private generateAllTests(): Observable<any> {
		return this.businesstestService.generateAllTests(this.logHandler);
	};


	private runAllTests(): Observable<any> {
		return this.businesstestService.runAllTests(this.logHandler);
	};

	generateAndRun(): void {
		this.started = true;
		observableConcat(
			this.generateAllTests(),
			this.runAllTests()
		).subscribe({
			complete: () => this.started = false,
			error: () => this.started = false
		});
	}
}
