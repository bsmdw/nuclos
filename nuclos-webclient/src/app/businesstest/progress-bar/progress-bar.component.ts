import { Component, Input, OnInit } from '@angular/core';

@Component({
	selector: 'nuc-progress-bar',
	templateUrl: './progress-bar.component.html',
	styleUrls: ['./progress-bar.component.css']
})
export class ProgressBarComponent implements OnInit {
	@Input() progress = 0;
	@Input() maxProgress = 100;

	constructor() {
	}

	ngOnInit() {
	}

}
