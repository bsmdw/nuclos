import { RouterModule, Routes } from '@angular/router';
import { MaintenanceComponent } from './maintenance.component';

export const ROUTE_CONFIG: Routes = [
	{
		path: 'maintenance',
		component: MaintenanceComponent
	}
];

export const MaintenanceRoutesModule = RouterModule.forChild(ROUTE_CONFIG);
