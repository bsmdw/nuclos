import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaintenanceComponent } from './maintenance.component';
import { MaintenanceRoutesModule } from './maintenance.routes';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { I18nModule } from '../../i18n/i18n.module';
import { MaintenanceService } from './maintenance.service';

@NgModule({
	declarations: [MaintenanceComponent],
	imports: [
		FormsModule,
		CommonModule,
		RouterModule,
		I18nModule,
		MaintenanceRoutesModule
	],
	providers: [
		MaintenanceService
	]
})
export class MaintenanceModule {
}
