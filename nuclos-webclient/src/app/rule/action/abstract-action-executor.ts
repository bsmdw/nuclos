import { DataService } from '../../entity-object-data/shared/data.service';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';

export abstract class AbstractActionExecutor<T extends RuleAction> {
	constructor(
		protected event: RuleEvent,
		protected action: T
	) {
	}

	abstract execute(eo: EntityObject, dataService?: DataService);
}
