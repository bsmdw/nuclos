import { IEntityObjectDependents } from '@nuclos/nuclos-addon-api';
import { BehaviorSubject, Observable, ReplaySubject } from 'rxjs';
import { EntityObjectDependents } from './entity-object-dependents';
import { EntityObject, SubEntityObject } from './entity-object.class';

export class EntityObjectDependentsManager {

	/**
	 * Holds a map of dependents (subform data), where the key is the attribute-FQN of the dependent.
	 *
	 * @type {Map<string, "../../Observable".Observable<EntityObject[]>>}
	 */
	private dependents = new Map<string, IEntityObjectDependents>();

	/**
	 * Changes in subforms, mapped by entity.
	 *
	 * @type {Map<string, ReplaySubject<SubEntityObject[]>>}
	 */
	private selectionChanges = new Map<string, BehaviorSubject<SubEntityObject[]>>();

	constructor(
		private eo: EntityObject
	) {
	}

	getDependents(attributeId: string): IEntityObjectDependents {
		let dependents = this.dependents.get(attributeId);

		if (!dependents) {
			dependents = new EntityObjectDependents(this.eo, attributeId);
			this.dependents.set(attributeId, dependents);
		}

		return dependents;
	}

	private getSelectionChangeSubject(entityClassId: string) {
		let subject = this.selectionChanges.get(entityClassId);

		if (!subject) {
			subject = new BehaviorSubject<SubEntityObject[]>([]);

			this.selectionChanges.set(entityClassId, subject);
		}

		return subject;
	}

	observeSelectionChange(
		entityClassId: string
	): Observable<SubEntityObject[]> {
		return this.getSelectionChangeSubject(entityClassId);
	}

	getSelection(
		entityClassId: string
	): SubEntityObject[] {
		let subject = this.getSelectionChangeSubject(entityClassId);

		if (subject) {
			return subject.getValue();
		}

		return [];
	}

	clearSelection() {
		this.selectionChanges.forEach(
			subject => subject.next([])
		);
	}

	selectionChanged(
		entityClassId: string,
		selectedSubEos: SubEntityObject[]
	) {
		this.getSelectionChangeSubject(entityClassId).next(selectedSubEos);
	}

	getAllDependents() {
		return this.dependents;
	}

	clear() {
		this.dependents.forEach(
			dependents => dependents.clear()
		);
		// this.dependents.clear();
	}
}
