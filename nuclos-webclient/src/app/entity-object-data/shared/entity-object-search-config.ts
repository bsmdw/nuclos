export interface EntityObjectSearchConfig {
	searchCondition?: string | undefined;
	sort?: string | undefined;
}
