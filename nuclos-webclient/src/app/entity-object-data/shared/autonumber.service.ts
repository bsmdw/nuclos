import { Injectable } from '@angular/core';
import { IEntityObjectDependents, ISubEntityObject } from '@nuclos/nuclos-addon-api';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Logger } from '../../log/shared/logger';
import { EntityAttrMeta } from './bo-view.model';
import { SubEntityObject } from './entity-object.class';

@Injectable()
export class AutonumberService {

	static instance: AutonumberService;

	private updateInProgress: IEntityObjectDependents[] = [];

	constructor(
		private $log: Logger
	) {
		AutonumberService.instance = this;
	}

	/**
	 * Returns the first autonumber attribute from the meta data, or undefined.
	 */
	findAutonumberAttribute(dependents: IEntityObjectDependents): Observable<EntityAttrMeta | undefined> {
		let subEos = dependents.current();
		if (!subEos || subEos.length === 0) {
			return Observable.of(undefined);
		}

		return (subEos[0] as SubEntityObject).getMeta().pipe(
			map(meta => {
				let result;
				meta.getAttributes().forEach(attribute => {
					if (!result && attribute.isAutonumber()) {
						result = attribute;
					}
				});
				return result;
			}),
		);
	}

	hasAutonumber(dependents: IEntityObjectDependents): Observable<boolean> {
		return this.findAutonumberAttribute(dependents).pipe(
			map(attribute => attribute !== undefined)
		);
	}

	updateAutonumbers(dependents: IEntityObjectDependents) {
		this.findAutonumberAttribute(dependents).subscribe(attribute => {
			if (!attribute) {
				return;
			}
			this.updateAutonumbersForAttribute(dependents, attribute);
		});
	}

	updateAutonumbersForAttribute(
		dependents: IEntityObjectDependents,
		attributeMeta: EntityAttrMeta
	) {
		if (this.updateInProgress.indexOf(dependents) > -1) {
			this.$log.warn('Update of autonumbers for attribute %o on %o already in progress', attributeMeta, dependents);
			return;
		}

		this.$log.debug('Updating autonumbers for attribute %o on %o', attributeMeta, dependents);

		let subEos = dependents.current();
		if (!subEos || subEos.length === 0) {
			this.$log.warn('Could not update autonumbers - no dependents');
			return;
		}

		let attributeId = attributeMeta.getAttributeID();
		let forUpdate: ISubEntityObject[] = [];
		let forDelete: ISubEntityObject[] = [];

		subEos.forEach(subEo => {
			if (subEo.isMarkedAsDeleted()) {
				forDelete.push(subEo);
			} else {
				forUpdate.push(subEo);
			}
		});

		forUpdate.sort(
			(eo1, eo2) => {
				let number1 = eo1.getAttribute(attributeId);
				let number2 = eo2.getAttribute(attributeId);

				if (typeof number1 === 'number' && typeof number2 !== 'number') {
					return -1;
				} else if (typeof number1 !== 'number' && typeof number2 === 'number') {
					return 1;
				}

				return number1 - number2;
			}
		);

		this.updateInProgress.push(dependents);
		this.writeAutonumbers(forUpdate, attributeId);
		this.deleteAutonumbers(forDelete, attributeId);
		delete this.updateInProgress[this.updateInProgress.indexOf(dependents)];
	}

	private writeAutonumbers(clonedDependents: ISubEntityObject[], attributeId: string) {
		try {
			clonedDependents.forEach((eo, index) => {
				eo.setAttribute(attributeId, index + 1);
			});
		} catch (e) {
			this.$log.error(e);
		}
	}

	private deleteAutonumbers(forDelete: ISubEntityObject[], attributeId: string) {
		try {
			forDelete.forEach(eo => eo.setAttribute(attributeId, undefined));
		} catch (e) {
			this.$log.error(e);
		}
	}
}
