import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EMPTY, throwError as observableThrowError } from 'rxjs';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { Logger } from '../../log/shared/logger';
import { DialogService } from '../../popup/dialog/dialog.service';
import { StringUtils } from '../../shared/string-utils';
import { ValidationError } from '../../validation/validation-error';
import { EntityObjectEventService } from './entity-object-event.service';
import { EntityObject } from './entity-object.class';

@Injectable()
export class EntityObjectErrorService {

	constructor(
		private eoEventService: EntityObjectEventService,
		private dialog: DialogService,
		private $log: Logger,
		private nuclosI18nService: NuclosI18nService,
		private dialogService: DialogService,
	) {
		this.eoEventService.observeSelectedEo().subscribe(
			eo => {
				let error = eo && eo.getError();
				if (error) {
					this.$log.debug('Error %o in selected EO %o', error, eo);
					this.dialog.alert({
						title: 'Error',
						message: error
					});
				}
			}
		);
	}

	handleError(error: HttpErrorResponse | any, eo?: EntityObject) {
		let errorMessage = this.getErrorMessage(error);

		this.handleValidationErrors(error, eo);

		if (errorMessage) {
			this.showErrorMessage(errorMessage, error);
			return EMPTY;
		}

		return observableThrowError(errorMessage);
	}

	private getErrorMessage(error: HttpErrorResponse | any) {
		let errorMessage: string | undefined;

		let messageFromResponse = this.getErrorMessageFromResponse(error);
		if (messageFromResponse) {
			errorMessage = messageFromResponse;
		} else if (error.status) {
			errorMessage = 'webclient.error.code' + error.status;
		} else {
			errorMessage = error.message ? error.message : error.toString();
		}

		if (errorMessage) {
			errorMessage = this.nuclosI18nService.getI18n(errorMessage);
		}

		return errorMessage;
	}

	private getErrorMessageFromResponse(error: HttpErrorResponse | any) {
		let result = undefined;

		try {
			const json = error.error || '';
			result = json.message
				.replace('<html>', '')
				.replace('</html>', '')
				.replace('<body>', '')
				.replace('</body>', '');
		} catch (e) {
		}

		return result;
	}

	private showErrorMessage(errorMessage: string, error: Response | any) {
		this.$log.error(errorMessage, error);

		this.dialogService.alert(
			{
				title: this.nuclosI18nService.getI18n('webclient.error.title'),
				message: StringUtils.textToHtml(errorMessage)
			}
		);
	}

	private handleValidationErrors(error: Response | any, eo?: EntityObject) {
		if (eo) {
			let validationErrors: ValidationError[] = this.getValidationErrors(error);

			if (validationErrors) {
				for (let validationError of validationErrors) {
					eo.setAttributeValidationError(validationError);
				}
			}
		}
	}

	private getValidationErrors(error: any) {
		let json;
		try {
			json = error.error;
		} catch (e) {
		}

		return json && json.validationErrors;
	}
}
