import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Logger } from '../../log/shared/logger';

@Component({
	selector: 'nuc-set-locale',
	templateUrl: './set-locale.component.html',
	styleUrls: ['./set-locale.component.css']
})
export class SetLocaleComponent implements OnInit {

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private $log: Logger
	) {
		/**
		 * TODO: There should be a better way to apply the changed locale than by navigating to another route.
		 */
		this.route.queryParams.subscribe(
			params => {
				let url = params['url'].replace('#', '');
				this.$log.debug('Navigating to URL: %o', url);
				this.router.navigateByUrl(url);
			}
		);
	}

	ngOnInit() {
	}
}
