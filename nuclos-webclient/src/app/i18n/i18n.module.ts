import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CookieModule } from '../cookie/cookie.module';
import { LogModule } from '../log/log.module';
import { I18nComponent } from './i18n.component';
import { LocaleComponent } from './locale/locale.component';
import { SetLocaleComponent } from './set-locale/set-locale.component';
import { I18nPipe } from './shared/i18n.pipe';
import { NuclosI18nService } from './shared/nuclos-i18n.service';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,

		LogModule,
		CookieModule
	],
	declarations: [
		I18nComponent,
		I18nPipe,
		LocaleComponent,
		SetLocaleComponent
	],
	exports: [
		I18nPipe,
		LocaleComponent,
		SetLocaleComponent
	],
	providers: [
		NuclosI18nService
	]
})
export class I18nModule {
}
