import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Rx';
import { EntityMeta } from '../../entity-object-data/shared/bo-view.model';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { SearchService } from '../shared/search.service';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { SystemParameter } from '../../shared/system-parameters';
import { NuclosConfigService } from '../../shared/nuclos-config.service';

@Component({
	selector: 'nuc-text-search',
	templateUrl: './text-search.component.html',
	styleUrls: ['./text-search.component.css']
})
export class TextSearchComponent implements OnInit {

	@Input() eo: EntityObject | undefined;
	@Input() meta: EntityMeta;

	textSearch: string;

	/**
	 * provides text input changes for debounced search
	 */
	private textSearchUpdated: Subject<string> = new Subject<string>();

	quicksearchDelayTime: number;

	constructor(
		private config: NuclosConfigService,
		private i18n: NuclosI18nService,
		private searchService: SearchService
	) {
	}

	ngOnInit() {
		this.config.getSystemParameters().subscribe(params => {
			this.quicksearchDelayTime = params.get(SystemParameter.QUICKSEARCH_DELAY_TIME);

			// debounce search input
			this.textSearchUpdated.asObservable()
				.debounceTime(this.quicksearchDelayTime)
				.distinctUntilChanged().subscribe(
				(searchInputText) => {
					this.searchService.updateSearchInputText(searchInputText);
					this.searchService.initiateDataLoad();
					this.scrollSidebarToTop();
				}
			);
		});
	}

	getPlaceholder(): string {
		return (this.meta ? (this.meta.getEntityName() + ' ') : '') + this.i18n.getI18n('webclient.search.quickfind');
	}

	doTextSearch(doSearch = this.isAutosearch()): void {
		if (doSearch) {
			this.textSearchUpdated.next(this.textSearch);
		}
	}

	isDisabled() {
		return this.eo && this.eo.isDirty();
	}

	isAutosearch() {
		return this.quicksearchDelayTime >= 0;
	}

	/**
	 * scroll sidebar to top
	 * @deprecated: TODO: Sidebar should do this by itself, instead of being manipulated here
	 */
	private scrollSidebarToTop(): void {
		$('#sidebar-list-container').scrollTop(0);
	}
}
