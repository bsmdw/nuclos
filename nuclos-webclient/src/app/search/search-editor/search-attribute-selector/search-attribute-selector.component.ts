import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { EntityMeta } from '../../../entity-object-data/shared/bo-view.model';
import { EntityObjectSearchfilterService } from '../../../entity-object-data/shared/entity-object-searchfilter.service';
import { MetaService } from '../../../entity-object-data/shared/meta.service';
import {
	Preference,
	SearchtemplateAttribute,
	SearchtemplatePreferenceContent
} from '../../../preferences/preferences.model';
import { Selectable } from '../../../two-side-multi-select/two-side-multi-select.model';
import { SearchfilterService } from '../../shared/searchfilter.service';

export class EntityMetaRenderer {
	meta: EntityMeta;
	ec: EntityContext | undefined;
	name = '';
	mainEntity = false;

	constructor(meta: EntityMeta, ec: EntityContext | undefined, mainEntity: boolean) {
		this.meta = meta;
		this.ec = ec;
		this.mainEntity = mainEntity;
		let entityName = meta.getEntityName();
		if (entityName !== undefined) {
			this.name = entityName;
		}
		if (this.name === undefined) {
			this.name = meta.getEntityClassId();
		}
		if (mainEntity) {
			this.name = '< ' + this.name + ' >';
		}
	}
}

@Component({
	selector: 'nuc-search-attribute-selector',
	templateUrl: './search-attribute-selector.component.html',
	styleUrls: ['./search-attribute-selector.component.css']
})
export class SearchAttributeSelectorComponent implements OnInit, OnDestroy {

	_meta: EntityMeta;
	@Input() searchfilter: Preference<SearchtemplatePreferenceContent>;

	@Output() handleChangeOfSelectedSearchfilter = new EventEmitter();

	subformSearchEnabled = false;

	filter = '';
	selectedValue;
	availableAttributes: SearchtemplateAttribute[] = [];
	selectedAttributes: Selectable[] = [];

	filterEntity = '';
	selectedEntity;
	mainEntity;
	allDependentEntities: EntityMetaRenderer[] = [];
	availableEntities: EntityMetaRenderer[] = [];

	private subscriptions: Subscription[] = [];

	constructor(
		private searchfilterService: SearchfilterService,
		private eoSearchfilterService: EntityObjectSearchfilterService,
		private metaService: MetaService,
	) {
	}

	ngOnInit() {
		this.updateAttributes('');
		this.loadEntities('');
		if (this.searchfilter) {
			let withDependentColumns = this.searchfilter.content.columns.find(c => c.dependentBoMetaId !== undefined);
			if (withDependentColumns) {
				this.subformSearchEnabled = true;
			}
		}
	}

	ngOnDestroy(): void {
		for (let subscription of this.subscriptions) {
			subscription.unsubscribe();
		}
	}

	@Input()
	set meta(meta: EntityMeta) {
		this._meta = meta;
		this.updateAttributes('');
		this.loadEntities('');
	}

	get meta(): EntityMeta {
		return this._meta;
	}

	private updateAttributes(query: string) {
		this.updateSelectedAttributes();
		this.updateAvailableAttributes(query);
	}

	private updateAvailableAttributes(query: string) {
		let meta = this._meta;
		let dependentEntityFieldId;
		if (this.selectedEntity !== undefined && this.selectedEntity.meta !== undefined) {
			meta = this.selectedEntity.meta;
			dependentEntityFieldId = this.selectedEntity.ec.dependentEntityFieldId;
		}
		if (meta) {
			let emptySearchPref = this.searchfilterService.createSearchfilter(meta);
			if (!this.searchfilter) {
				this.searchfilter = emptySearchPref;
			}

			this.availableAttributes = (emptySearchPref.content.columns as SearchtemplateAttribute[]).filter(
				attribute => attribute.name && attribute.name.toLowerCase().indexOf(query.toLowerCase()) >= 0
					&& !this.isSelected(attribute.boAttrId) && dependentEntityFieldId !== attribute.boAttrId
			);
			this.availableAttributes.sort((a, b) => {
				if (a.name && b.name) {
					if (a.system !== true && b.system === true) {
						return -1;
					}
					if (a.system === true && b.system !== true) {
						return 1;
					}
					return a.name.toLowerCase() < b.name.toLowerCase() ? -1 : 1;
				}
				return 0;
			})
		} else {
			this.availableAttributes = [];
		}
	}

	private updateSelectedAttributes() {
		if (this.searchfilter) {
			this.selectedAttributes = (this.searchfilter.content.columns as Selectable[])
				.filter(it => it.selected);
		} else {
			this.selectedAttributes = [];
		}
	}

	selectAttribute(attribute: Selectable) {
		let meta = this._meta;
		let dependentBoMetaId;
		if (this.selectedEntity !== undefined) {
			meta = this.selectedEntity.meta;
			dependentBoMetaId = meta.getEntityClassId();
			attribute.name = meta.getEntityName() + '.' + attribute.name;
		}
		let fromSearchfilter = this.findAttributeInSearchfilter(attribute.name);
		if (fromSearchfilter) {
			fromSearchfilter.selected = true;
			fromSearchfilter.enableSearch = true;
			fromSearchfilter.focusInput = true;
			let index = this.searchfilter.content.columns.indexOf(fromSearchfilter, 0);
			if (index > -1) {
				this.searchfilter.content.columns.splice(index, 1);
			}
			this.searchfilter.content.columns.push(fromSearchfilter);
		} else {
			attribute.selected = true;
			let newAttribute = attribute as SearchtemplateAttribute;
			newAttribute.enableSearch = true;
			newAttribute.focusInput = true;
			newAttribute.dependentBoMetaId = dependentBoMetaId;
			let attributeMeta = meta.getAttributeMetaByFqn(newAttribute.boAttrId);
			if (attributeMeta && attributeMeta.isBoolean()) {
				newAttribute.value = 'false';
			}
			this.searchfilter.content.columns.push(newAttribute);
		}

		this.filter = '';
		this.selectedValue = {name: ''};

		this.eoSearchfilterService.updateColumns(this.searchfilter);
		if (this.handleChangeOfSelectedSearchfilter) {
			this.handleChangeOfSelectedSearchfilter.emit();
		}
	}

	loadAttributes(query: string) {
		this.updateAttributes(query);
	}

	private isSelected(boAttriId: string) {
		return (this.selectedAttributes as SearchtemplateAttribute[]).find(it => it.boAttrId === boAttriId);
	}

	private findAttributeInSearchfilter(name: string) {
		let result;
		if (this.searchfilter) {
			result = (this.searchfilter.content.columns as Selectable[]).find(
				it => it.name === name
			);
		}
		return result;
	}

	modelChange($event) {
		if (typeof $event === 'string') {
			this.filter = $event;
		}
	}

	enableSubformSearch() {
		this.subformSearchEnabled = true;
		this.updateEntitiesIfNecessary();
	}

	hasDependentEntities(): boolean {
		return this.allDependentEntities.length > 0;
	}

	updateEntitiesIfNecessary() {
		if (this._meta) {
			if (this.mainEntity === undefined || this.mainEntity !== this._meta.getEntityClassId()) {
				let entityContexts = this._meta.getEntityContexts();
				if (entityContexts) {
					let entityClassIds: Set<string> = new Set();
					entityContexts.forEach(ec => entityClassIds.add(ec.dependentEntityClassId));
					this.subscriptions.push(this.metaService.getEntityMetas(entityClassIds).subscribe(mapMeta => {
						let allDeps: EntityMetaRenderer[] = [];
						if (entityContexts) {
							entityContexts.forEach(ec => {
								let meta = mapMeta.get(ec.dependentEntityClassId);
								if (meta) {
									allDeps.push(new EntityMetaRenderer(meta, ec, false))
								}
							});
							allDeps.sort((a, b) => {
								if (a.mainEntity && !b.mainEntity) {
									return -1;
								} else if (!a.mainEntity && b.mainEntity) {
									return 1;
								}
								return a.name.toLocaleString().localeCompare(b.name.toLocaleString());
							});
						}
						this.allDependentEntities = allDeps;
						this.filterAvailableEntities();
					}));
				}
			}
			this.mainEntity = this._meta.getEntityClassId();
		}
	}

	private filterAvailableEntities() {
		let filter = this.filterEntity;
		let result: EntityMetaRenderer[] = [];
		let all = this.allDependentEntities;
		if (this._meta) {
			result.push(new EntityMetaRenderer(this._meta, undefined, true));
		}
		all .filter(er => '' === filter || er.name.toLowerCase().indexOf(filter.toLowerCase()) >= 0)
			.forEach(er => result.push(er));
		this.availableEntities = result;
	}

	selectEntity(entityRenderer: EntityMetaRenderer) {
		if (entityRenderer.mainEntity) {
			this.selectedEntity = undefined;
		} else {
			this.selectedEntity = entityRenderer;
		}
		this.updateAttributes(this.filter);
	}

	loadEntities(query: string) {
		this.filterEntity = query;
		this.updateEntitiesIfNecessary();
		this.filterAvailableEntities();
	}

	modelChangeEntity($event) {
		if (typeof $event === 'string') {
			this.loadEntities($event);
		}
	}
}
