export interface Operator {
	operator: string;
	isUnary: boolean;
	label: string;
}
