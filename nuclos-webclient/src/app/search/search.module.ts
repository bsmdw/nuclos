import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { ClickOutsideModule } from '../click-outside/click-outside.module';
import { I18nModule } from '../i18n/i18n.module';
import { UiComponentsModule } from '../ui-components/ui-components.module';
import { SearchAttributeSelectorComponent } from './search-editor/search-attribute-selector/search-attribute-selector.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { SearchEditorComponent } from './search-editor/search-editor.component';
import { SearchEditorButtonComponent } from './search-editor-button/search-editor-button.component';
import { SearchFilterSelectorComponent } from './search-editor/search-filter-selector/search-filter-selector.component';
import { SearchItemBooleanComponent } from './search-editor/search-item-boolean/search-item-boolean.component';
import { SearchItemDatechooserComponent } from './search-editor/search-item-datechooser/search-item-datechooser.component';
import { SearchItemLovComponent } from './search-editor/search-item-lov/search-item-lov.component';
import { SearchService } from './shared/search.service';
import { SearchfilterService } from './shared/searchfilter.service';
import { TextSearchComponent } from './text-search/text-search.component';
import { ResizableModule } from 'angular-resizable-element';
import { TwoSideMultiSelectModule } from '../two-side-multi-select/two-side-multi-select.module';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,

		AutoCompleteModule,
		NgbModule,

		ClickOutsideModule,
		I18nModule,
		UiComponentsModule,
		ResizableModule,
		TwoSideMultiSelectModule
	],
	declarations: [
		SearchBarComponent,
		SearchEditorComponent,
		SearchEditorButtonComponent,
		TextSearchComponent,
		SearchItemBooleanComponent,
		SearchItemDatechooserComponent,
		SearchItemLovComponent,
		SearchAttributeSelectorComponent,
		SearchFilterSelectorComponent,
	],
	exports: [
		SearchBarComponent,
		SearchEditorComponent
	],
	providers: [
		SearchService,
		SearchfilterService
	]
})
export class SearchModule {
}
