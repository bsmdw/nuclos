import { Operator } from './operator';

export class OperatorDefinitions {
	[ index: string ]: Operator[];
}
