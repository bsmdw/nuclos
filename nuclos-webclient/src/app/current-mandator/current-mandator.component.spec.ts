import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentMandatorComponent } from './current-mandator.component';

xdescribe('CurrentMandatorComponent', () => {
	let component: CurrentMandatorComponent;
	let fixture: ComponentFixture<CurrentMandatorComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [CurrentMandatorComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(CurrentMandatorComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
