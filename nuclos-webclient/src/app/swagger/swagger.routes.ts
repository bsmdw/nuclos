import { RouterModule, Routes } from '@angular/router';
import { SwaggerUiComponent } from './swagger-ui/swagger-ui.component';

export const ROUTE_CONFIG: Routes = [
	{
		path: 'swagger-ui',
		component: SwaggerUiComponent
	},
];

export const SwaggerRoutesModule = RouterModule.forChild(ROUTE_CONFIG);
