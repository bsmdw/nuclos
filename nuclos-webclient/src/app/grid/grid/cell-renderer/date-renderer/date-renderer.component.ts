import { Component } from '@angular/core';
import { DatetimeService } from '../../../../shared/datetime.service';
import { AbstractCellRenderer } from '../abstract-cell-renderer';

// TODO refactor with SubformDateRendererComponent
@Component({
	selector: 'nuc-date-renderer',
	templateUrl: './date-renderer.component.html',
	styleUrls: ['./date-renderer.component.css']
})
export class DateRendererComponent extends AbstractCellRenderer {

	formattedValue: string;

	constructor(private datetimeService: DatetimeService) {
		super();
	}

	agInit(params: any) {
		super.agInit(params);
		this.refresh(params);
	}

	refresh(params: any): boolean {
		super.refresh(params);
		let attributeMeta = this.getAttributeMeta();
		if (attributeMeta && attributeMeta.isTimestamp()) {
			this.formattedValue = this.datetimeService.formatTimestamp(params.value);
		} else {
			this.formattedValue = this.datetimeService.formatDate(params.value);
		}
		return false;
	}

}
