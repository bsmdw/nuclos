import { ColDef, ICellRendererParams } from 'ag-grid';
import { AgRendererComponent } from 'ag-grid-angular';
import { EntityAttrMeta } from '../../../entity-object-data/shared/bo-view.model';

export class AbstractCellRenderer implements AgRendererComponent {

	private entityAttrMeta: EntityAttrMeta;

	private params;

	constructor() {
	}

	agInit(params: ICellRendererParams): void {
		this.refresh(params);
		let colDef: ColDef = params.colDef;
		if (colDef.cellEditorParams) {
			this.entityAttrMeta = colDef.cellEditorParams['attrMeta'];
		}
	}

	refresh(params: any): boolean {
		this.params = params;
		return false;
	}

	protected getAttributeMeta(): EntityAttrMeta | undefined {
		return this.entityAttrMeta;
	}

}
