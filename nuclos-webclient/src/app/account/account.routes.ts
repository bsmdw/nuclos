import { RouterModule, Routes } from '@angular/router';
import { ActivationComponent } from './activation/activation.component';
import { ForgotLoginDetailsComponent } from './forgot-login-details/forgot-login-details.component';
import { ResetPasswordComponent } from './forgot-login-details/reset-password/reset-password.component';
import { PasswordChangeComponent } from './password-change/password-change.component';
import { RegistrationComponent } from './registration/registration.component';

export const ROUTE_CONFIG: Routes = [
	{
		path: 'account/register',
		component: RegistrationComponent
	},
	{
		path: 'account/activate/:username/:activationCode',
		component: ActivationComponent
	},
	{
		// Changes the password - requires a valid login and the old password
		path: 'account/changePassword',
		component: PasswordChangeComponent
	},
	{
		path: 'account/forgotLoginDetails',
		component: ForgotLoginDetailsComponent
	},
	{
		// Resets the password - requires a valid code, but neither a valid login nor the old password
		path: 'account/resetPassword/:username/:passwordResetCode',
		component: ResetPasswordComponent
	},
];

export const AccountRoutesModule = RouterModule.forChild(ROUTE_CONFIG);
