import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { concat, skip, take } from 'rxjs/operators';
import { AuthenticationService } from '../../authentication';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { NuclosHttpService } from '../../shared/nuclos-http.service';
import { AccountData } from './account-data';

@Injectable()
export class NuclosAccountService {

	constructor(
		private config: NuclosConfigService,
		private http: NuclosHttpService,
		private authService: AuthenticationService
	) {
	}

	create(account: AccountData): Observable<any> {
		return this.http.post(this.config.getRestHost() + '/user/register', account);
	}

	activate(username: any, activationCode: any): Observable<any> {
		// Activation is only possible after anonymous login... (NUCLOS-5614)
		return this.authService.waitForLogin().pipe(
			take(1),
			skip(1),
			concat(
				this.http.post(
					this.config.getRestHost() + '/user/' + username + '/activate/' + activationCode,
					{}
				)
			)
		);
	}

	changePassword(passwordItem: any) {
		let url = this.config.getRestHost() + '/user/' + passwordItem.userName + '/password';

		return this.http.put(
			url, passwordItem
		);
	}

	resetPassword(passwordItem: any) {
		let url = this.config.getRestHost() + '/user/' + passwordItem.userName + '/passwordReset';

		return this.http.post(
			url, passwordItem
		);
	}

	requestUsername(email: string) {
		let url = this.config.getRestHost() + '/user/forgotUsername/' + email;

		return this.http.post(url, {});
	}

	requestPasswordReset(username: string) {
		let url = this.config.getRestHost() + '/user/' + username + '/forgotPassword';

		return this.http.post(url, {});
	}
}
