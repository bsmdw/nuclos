import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { I18nModule } from '../i18n/i18n.module';
import { AccountRoutesModule } from './account.routes';
import { ActivationComponent } from './activation/activation.component';
import { ForgotLoginDetailsComponent } from './forgot-login-details/forgot-login-details.component';
import { ResetPasswordComponent } from './forgot-login-details/reset-password/reset-password.component';
import { PasswordChangeComponent } from './password-change/password-change.component';
import { RegistrationComponent } from './registration/registration.component';
import { NuclosAccountService } from './shared/nuclos-account.service';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		I18nModule,
		AccountRoutesModule
	],
	declarations: [
		RegistrationComponent,
		ActivationComponent,
		PasswordChangeComponent,
		ForgotLoginDetailsComponent,
		ResetPasswordComponent
	],
	providers: [
		NuclosAccountService
	]
})
export class AccountModule {
}
