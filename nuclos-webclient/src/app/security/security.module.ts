import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SecurityService } from './shared/security.service';
import { ISecurityService } from '@nuclos/nuclos-addon-api';

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [],
	providers: [
		{
			provide: ISecurityService,
			useClass: SecurityService
		}
	]
})
export class SecurityModule {
}
