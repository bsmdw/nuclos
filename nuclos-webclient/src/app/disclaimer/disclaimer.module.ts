import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { I18nModule } from '../i18n/i18n.module';
import { NewsModule } from '../news/news.module';
import { CookieDisclaimerComponent } from './cookie-disclaimer/cookie-disclaimer.component';

@NgModule({
	imports: [
		CommonModule,

		I18nModule,
		NewsModule,
	],
	declarations: [
		CookieDisclaimerComponent,
	],
	exports: [
		CookieDisclaimerComponent,
	],
	providers: [
	]
})
export class DisclaimerModule {
}
