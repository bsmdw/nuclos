import { RouterModule, Routes } from '@angular/router';
import { SetLocaleComponent } from './i18n/set-locale/set-locale.component';

export const ROUTE_CONFIG: Routes = [
	{
		path: '',
		pathMatch: 'full',
		redirectTo: 'login'
	},
	{
		path: 'index.html',
		pathMatch: 'full',
		redirectTo: 'login'
	},
	{
		path: 'locale/:locale',
		component: SetLocaleComponent
	},
	{
		path: '**',
		redirectTo: 'error/404'
	}
];

export const AppRoutesModule = RouterModule.forRoot(ROUTE_CONFIG, {
	useHash: true
});
