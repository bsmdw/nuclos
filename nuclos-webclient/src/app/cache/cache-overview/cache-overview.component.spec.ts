/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CacheOverviewComponent } from './cache-overview.component';

xdescribe('CacheOverviewComponent', () => {
	let component: CacheOverviewComponent;
	let fixture: ComponentFixture<CacheOverviewComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [CacheOverviewComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(CacheOverviewComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
