import { Logger } from '../log/shared/logger';
import { Preference, SideviewmenuPreferenceContent } from '../preferences/preferences.model';
import { ILayoutInfo } from './layout-info';
import { IPerspective } from './perspective';
import { PerspectiveModel } from './perspective-model';
import { SubformEntry } from './subform-entry';

export class PerspectiveModelImpl implements PerspectiveModel {

	perspectives: Array<Preference<IPerspective>> = [];
	selectedPerspective: IPerspective | undefined;

	searchTemplatePrefs: Array<Preference<any>> = [];
	sideviewMenuPrefs: Array<Preference<any>> = [];
	layoutInfos: Array<ILayoutInfo> = [];
	subformTablePrefs = Array<Preference<SideviewmenuPreferenceContent>>();
	subformTablePrefsBySubEntity: SubformEntry[] = [];

	constructor(
		private $log: Logger
	) {
	}

	getSearchTemplate(searchTemplatePrefId: string) {
		if (!searchTemplatePrefId) {
			return null;
		}

		for (let pref of this.searchTemplatePrefs) {
			if (pref.prefId === searchTemplatePrefId) {
				return pref;
			}
		}

		return null;
	}

	getSideviewMenu(prefId: string): Preference<any> | null {
		if (!prefId) {
			return null;
		}

		for (let pref of this.sideviewMenuPrefs) {
			if (pref.prefId === prefId) {
				return pref;
			}
		}

		return null;
	}

	getLayoutInfo(layoutUID: string): ILayoutInfo | null {
		if (!layoutUID) {
			return null;
		}

		for (let info of this.layoutInfos) {
			if (info.uid.uid === layoutUID) {
				return info;
			}
		}

		return null;
	}

	togglePerspective(perspective: IPerspective) {
		let selected;
		for (let p of this.perspectives) {
			if (p.content === perspective) {
				p.selected = !p.selected;
				this.selectedPerspective = perspective;
				selected = p.selected;
			} else {
				p.selected = false;
			}
		}

		// If the current perspective is not selected anymore, set it to null
		if (!selected) {
			this.selectedPerspective = undefined;
		}

		this.$log.debug('selected perspective: %o', this.selectedPerspective);
	}

	removePerspective(perspectivePref: Preference<IPerspective>) {
		let index = this.perspectives.indexOf(perspectivePref);
		if (index >= 0) {
			this.perspectives.splice(index, 1);
		}
		this.$log.debug('Removed perspective %o at index %o from model %o', perspectivePref, index, this);
	}

	addPerspective(perspectivePref: Preference<IPerspective>): void {
		this.setDefaultValues(perspectivePref.content);

		let existingPref = this.findPerspectivePref(perspectivePref.prefId);
		if (existingPref) {
			let index = this.perspectives.indexOf(existingPref);
			this.perspectives[index] = perspectivePref;
			this.$log.debug('updated perspective pref: %o', perspectivePref);
		} else {
			this.perspectives.push(perspectivePref);
		}
	}

	findPerspectivePref(prefId: string | undefined) {
		for (let pref of this.perspectives) {
			if (pref.prefId === prefId) {
				return pref;
			}
		}
		return null;
	}

	setPerspectives(perspectives: Array<Preference<IPerspective>>) {
		this.perspectives = perspectives;

		for (let p of perspectives) {
			this.setDefaultValues(p.content);
			if (p.selected) {
				this.selectedPerspective = p.content;
				this.$log.debug('selectedPerspective = %o', this.selectedPerspective);
			}
		}
	}

	getSideviewMenuPrefs(): Array<Preference<any>> {
		return this.sideviewMenuPrefs;
	}

	getSearchTemplatePrefs(): Array<Preference<any>> {
		return this.searchTemplatePrefs;
	}

	setDefaultValues(perspective: IPerspective) {
		// Layout is always also for new, if not explicitly set to false
		perspective.layoutForNew = perspective.layoutForNew !== false;
	}

	/**
	 * Checks if the given perspective is valid.
	 * I.e. it contains a boMetaId, a name and at least one configured property (layout, sideview menu, search template, ...).
	 *
	 * @param perspective
	 * @returns {boolean}
	 */
	isValidPerspective(perspective: IPerspective): boolean {
		return !!(
			perspective
			&& perspective.boMetaId
			&& perspective.name
			&& (perspective.layoutId || perspective.searchTemplatePrefId || perspective.sideviewMenuPrefId)
		);
	}

	getSubformTablePrefs(layoutId: string) {
		return this.subformTablePrefs.filter(
			pref => !pref.layoutId || pref.layoutId === layoutId
		);
	}

	getSubformPreferencesForSelectedLayout(): SubformEntry[] {
		// this.$log.debug('getSubformPreferencesForSelectedLayout: %o', this.subformTablePrefsBySubEntity);
		return this.subformTablePrefsBySubEntity;
	}

	getLayoutFqn(layoutUid: string | undefined) {
		for (let info of this.layoutInfos) {
			if (info.uid.uid === layoutUid) {
				return info.fqn;
			}
		}

		return layoutUid;
	}
}
