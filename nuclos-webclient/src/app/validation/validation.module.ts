import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NuclosValidationService } from './nuclos-validation.service';

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [],
	providers: [
		NuclosValidationService
	]
})
export class ValidationModule {
}
