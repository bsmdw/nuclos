import { Injectable } from '@angular/core';
import { Logger } from '@nuclos/nuclos-addon-api';
import { EMPTY, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { NuclosConfigService } from '../shared/nuclos-config.service';
import { NuclosHttpService } from '../shared/nuclos-http.service';
import { WsTab } from './explorertrees.model';

@Injectable()
export class ExplorerTreesService {

	constructor(
		private http: NuclosHttpService,
		private nuclosConfig: NuclosConfigService,
		private $log: Logger,
	) {
	}

	getMenuSelector(): Observable<WsTab[]> {

		return this.http.get<WsTab[]>(
			this.nuclosConfig.getRestHost() + '/meta/sideviewmenuselector'
		).pipe(
			catchError(e => {
				this.$log.warn('Could not load sideview menu selector: %o', e);
				return EMPTY;
			})
		);
	}

}
