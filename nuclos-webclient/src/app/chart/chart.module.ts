import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PrettyJsonModule } from 'angular2-prettyjson';
import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { ClickOutsideModule } from '../click-outside/click-outside.module';
import { I18nModule } from '../i18n/i18n.module';
import { LogModule } from '../log/log.module';
import { ModalModule } from '../popup/modal/modal.module';
import { UiComponentsModule } from '../ui-components/ui-components.module';
import { ChartButtonComponent } from './chart-button/chart-button.component';
import { ChartFilterComponent } from './chart-filter/chart-filter.component';
import { ChartModalComponent } from './chart-modal/chart-modal.component';
import { ChartTabsComponent } from './chart-modal/chart-tabs/chart-tabs.component';
import { ChartViewComponent } from './chart-modal/detail/chart-view/chart-view.component';
import { ConfigComponent } from './chart-modal/detail/config/config.component';
import { ExtendedComponent } from './chart-modal/detail/config/extended/extended.component';
import { OptionsComponent } from './chart-modal/detail/config/options/options.component';
import { SeriesComponent } from './chart-modal/detail/config/options/series/series.component';
import { DetailComponent } from './chart-modal/detail/detail.component';
import { ToolbarComponent } from './chart-modal/detail/toolbar/toolbar.component';
import { CsvExportComponent } from './csv-export/csv-export.component';
import { ChartService } from './shared/chart.service';
import { ChartViewService } from './chart-modal/detail/chart-view/chart-view.service';
import { ChartFilterLovComponent } from './chart-filter/chart-filter-lov.component';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,

		I18nModule,
		LogModule,
		ModalModule,
		UiComponentsModule,

		// TODO: Should not be necessary here
		ClickOutsideModule,

		AutoCompleteModule,
		NgbModule,
		PrettyJsonModule,
	],
	declarations: [
		ChartButtonComponent,
		ChartFilterComponent,
		ChartFilterLovComponent,
		ChartModalComponent,
		ConfigComponent,
		CsvExportComponent,
		DetailComponent,
		ChartViewComponent,
		OptionsComponent,
		ToolbarComponent,
		ExtendedComponent,
		SeriesComponent,
		ChartTabsComponent,
	],
	exports: [
		ChartButtonComponent,
	],
	entryComponents: [
		ChartModalComponent,
	],
	providers: [
		ChartService,
		ChartViewService,
	]
})
export class ChartModule {
}
