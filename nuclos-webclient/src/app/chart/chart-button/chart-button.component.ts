import { Component, Input, OnInit } from '@angular/core';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { ModalService } from '../../popup/modal/shared/modal.service';
import { ChartModalComponent } from '../chart-modal/chart-modal.component';

@Component({
	selector: 'nuc-chart-button',
	templateUrl: './chart-button.component.html',
	styleUrls: ['./chart-button.component.css']
})
export class ChartButtonComponent implements OnInit {

	@Input()
	eo: EntityObject;

	constructor(
		private modalService: ModalService,
	) {
	}

	ngOnInit() {
	}

	openModal() {
		this.modalService.open(
			ChartModalComponent,
			{
				eo: this.eo
			}
		);
	}
}
