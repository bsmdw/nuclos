import { Injectable } from '@angular/core';
import { SubEntityObject } from '../../../../entity-object-data/shared/entity-object.class';
import { EntityAttrMeta, EntityMeta } from '../../../../entity-object-data/shared/bo-view.model';
import { FqnService } from '../../../../shared/fqn.service';
import { EoChartWrapper } from '../../../shared/eo-chart-wrapper';
import { ChartService } from '../../../shared/chart.service';
import { ChartData } from './chart-view.model';

@Injectable()
export class ChartViewService {

	constructor(
		private chartService: ChartService,
	) {
	}

	/**
	 * transform EnitityObject data into ChartData
	 */
	public toChartData(eoChart: EoChartWrapper, subMeta: EntityMeta, subEos: SubEntityObject[]): ChartData {
		let result: ChartData = {
			category: {
				values: []
			},
			series: []
		};
		const chartContent = eoChart.chartPreference.content;
		const series = chartContent.chart.primaryChart.series;

		if (subEos.length === 0) {
			return result;
		}

		const subEntityClassId = subMeta.getEntityClassId();

		const categoryAttributeName = FqnService.getShortAttributeNameFailsafe(
			subEntityClassId,
			chartContent.chart.primaryChart.categoryBoAttrId
		);

		for (let i = 0; i < series.length; i++) {
			let serie = series[i];
			if (!this.chartService.isValidSeries(serie)) {
				continue;
			}
			let label = serie.label.de;
			let seriesAttributeName = FqnService.getShortAttributeName(subEntityClassId, serie.boAttrId);


			let seriesData = this.getSeriesData(subEos, subMeta, seriesAttributeName, categoryAttributeName);

			let categoryValues = subEos.map(s => s.getAttribute(categoryAttributeName));

			result.category.values = categoryValues;

			result.series.push({
				// TODO use localized string: key: this.localizedString(serie.label),
				key: label,
				values: seriesData,
				color: serie.color,
				// TODO crosshair: crosshairData,
			});
		}
		return result;
	}

	private getSeriesData(
		subEos: SubEntityObject[],
		subMeta: EntityMeta,
		seriesAttributeName: string,
		categoryAttributeName: string
	) {
		let result: any[] = [];

		let categoryAttributeMeta = subMeta.getAttributeMeta(categoryAttributeName);
		let seriesAttributeMeta = subMeta.getAttributeMeta(seriesAttributeName);

		subEos.forEach((subEo, index) => {
			let value = subEo.getAttribute(seriesAttributeName);
			let categoryValue = subEo.getAttribute(categoryAttributeName);

			if (categoryAttributeMeta) {
				categoryValue = this.deserializeValue(categoryAttributeMeta, categoryValue);
			}

			if (seriesAttributeMeta) {
				value = this.deserializeValue(seriesAttributeMeta, value);
			}

			result.push({
				key: (<any>seriesAttributeMeta).attrMetaData.boAttrName,
				categoryValue: categoryValue,
				index: index,
				seriesValue: value
				// , tooltip: 'TODO: tooltip'
			});
		});

		return result;
	}

	private deserializeValue(meta: EntityAttrMeta, value: any) {
		let result = value;

		if (meta) {
			if (meta.isDate() || meta.isTimestamp()) {
				result = new Date(value);
			} else if (meta.isReference()) {
				result = value && value.name;
			}
		}

		return result;
	}

}
