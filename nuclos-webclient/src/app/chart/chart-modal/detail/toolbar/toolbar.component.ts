import { Component, Input, OnInit } from '@angular/core';
import { ChartService } from '../../../shared/chart.service';
import { EoChartWrapper } from '../../../shared/eo-chart-wrapper';
import { DialogService } from '../../../../popup/dialog/dialog.service';
import { NuclosI18nService } from '../../../../i18n/shared/nuclos-i18n.service';

@Component({
	selector: 'nuc-toolbar',
	templateUrl: './toolbar.component.html',
	styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

	@Input() eoChart: EoChartWrapper
	@Input() eoCharts: EoChartWrapper[];

	constructor(
		private chartService: ChartService,
		private nuclosI18nService: NuclosI18nService,
		private dialogService: DialogService
	) {
	}

	ngOnInit() {
	}

	canConfigureCharts() {
		return this.chartService.canConfigureCharts();
	}

	save() {
		this.chartService.saveChart(this.eoChart).subscribe();
	}

	delete() {

		this.dialogService.confirm({
				title: this.nuclosI18nService.getI18n('webclient.charts.delete'),
				message: this.nuclosI18nService.getI18n('webclient.charts.delete.confirm')
			}
		).then(
			() => {
				// ok, delete
				if (this.eoChart.chartPreference.prefId) {
					this.chartService.deleteChart(this.eoChart.chartPreference).subscribe(() => {
						this.removeChart()
					});
				} else {
					this.removeChart()
				}
			},
			() => {
				// cancel
			}
		);
	}

	private removeChart() {
		let index = this.eoCharts.indexOf(this.eoChart);
		if (index >= 0) {
			this.eoCharts.splice(index, 1);
		}
	}

	toggleConfig() {
		this.chartService.toggleConfig();
	}

	exportChart() {
		this.chartService.exportAsCsv(this.eoChart);
	}

	isExportAvailable() {
		let configurationValid = this.chartService.isValidChartPreference(this.eoChart.chartPreference.content);
		let dataCount = this.eoChart.getCurrentDataCount();
		return configurationValid && dataCount > 0;
	}
}
