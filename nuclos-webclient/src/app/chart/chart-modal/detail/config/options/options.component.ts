import { Component, Input, OnInit } from '@angular/core';
import { ChartService } from '../../../../shared/chart.service';
import { EoChartWrapper } from '../../../../shared/eo-chart-wrapper';

@Component({
	selector: 'nuc-options',
	templateUrl: './options.component.html',
	styleUrls: ['./options.component.css']
})
export class OptionsComponent implements OnInit {

	@Input() eoChart: EoChartWrapper;

	chartTypes = ChartService.chartTypes;
	referenceAttributes: { id: string, name: string }[];
	entityAttributes: { id: string, name: string }[];

	constructor(
		private chartService: ChartService
	) {
	}

	ngOnInit() {
		this.referenceAttributes = this.chartService.findReferenceAttributes(this.eoChart.eo);
		this.updateCategoryAttributes();
	}

	private updateCategoryAttributes() {
		this.entityAttributes = [];

		let referenceAttributeId = this.getReferenceAttributeId();
		if (referenceAttributeId) {
			this.chartService.getSubEntityMeta(this.eoChart.eo, referenceAttributeId).subscribe(
				entityMeta => {
					let attributes = entityMeta.getAttributes();
					attributes.forEach(attributeMeta => {
						this.entityAttributes.push({
							id: attributeMeta.getAttributeID(),
							name: attributeMeta.getName() || attributeMeta.getAttributeID()
						});
					});
				}
			);
		}
	}

	setName(name: string) {
		this.eoChart.chartPreference.name = name;
		this.eoChart.chartPreference.dirty = true;
	}

	setChartType(chartType: string) {
		this.eoChart.chartPreference.content.chart.type = chartType;

		// TODO: Create proper wrappers for preferences with change- and event-handling
		this.chartChanged();
	}

	getReferenceAttributeId() {
		return this.eoChart.chartPreference
			&& this.eoChart.chartPreference.content
			&& this.eoChart.chartPreference.content.chart
			&& this.eoChart.chartPreference.content.chart.primaryChart
			&& this.eoChart.chartPreference.content.chart.primaryChart.refBoAttrId;
	}

	setReferenceAttributeId(attributeId: string) {
		this.eoChart.chartPreference.content.chart.primaryChart.refBoAttrId = attributeId;
		this.eoChart.chartPreference.dirty = true;
		this.updateCategoryAttributes();
	}

	setCategoryAttributeId(attributeId: string) {
		this.eoChart.chartPreference.content.chart.primaryChart.categoryBoAttrId = attributeId;
		this.reloadChartData();
	}

	chartChanged() {
		this.eoChart.chartPreference.dirty = true;
		this.eoChart.chartOptionsChanged.next();
	}

	reloadChartData() {
		this.chartChanged();
		this.chartService.search(this.eoChart);
	}
}
