import { Component, Input, OnInit } from '@angular/core';
import { EntityObject } from '../../../entity-object-data/shared/entity-object.class';
import { ChartService } from '../../shared/chart.service';
import { EoChartWrapper } from '../../shared/eo-chart-wrapper';

@Component({
	selector: 'nuc-chart-tabs',
	templateUrl: './chart-tabs.component.html',
	styleUrls: ['./chart-tabs.component.css']
})
export class ChartTabsComponent implements OnInit {

	@Input() eo: EntityObject;

	selectedEoChart: EoChartWrapper | undefined;

	eoCharts: EoChartWrapper[] = [];

	constructor(
		private chartService: ChartService
	) {
	}

	ngOnInit() {
		this.chartService.getCharts(this.eo).subscribe(
			charts => {
				this.eoCharts = [];
				for (let chart of charts) {
					this.eoCharts.push(
						new EoChartWrapper(
							this.eo,
							chart
						)
					);
				};
				if (this.eoCharts.length > 0) {
					this.selectedEoChart = this.eoCharts[0];
				}
			}
		);
	}

	tabId(index: number) {
		return 'chart-tab-' + index;
	}

	newChart() {
		let eoChart = this.chartService.newChart(this.eo);
		this.eoCharts.push(eoChart);

		if (!this.chartService.isShowConfig()) {
			this.chartService.toggleConfig();
		}

		this.selectChart(eoChart);
	}

	selectChart(eoChart) {
		this.selectedEoChart = eoChart;
	}

	canConfigureChart() {
		return this.chartService.canConfigureCharts();
	}
}
