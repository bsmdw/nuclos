import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DndModule } from 'ng2-dnd';
import { I18nModule } from '../i18n/i18n.module';
import { ListModule } from '../list/list.module';
import { TwoSideMultiSelectComponent } from './two-side-multi-select.component';

@NgModule({
	imports: [
		CommonModule,
		ListModule,
		I18nModule,
		DndModule
	],
	declarations: [
		TwoSideMultiSelectComponent
	],
	exports: [
		TwoSideMultiSelectComponent
	]
})
export class TwoSideMultiSelectModule {
}
