/* tslint:disable */

interface EntityContext extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    dependentEntityClassId: string;
    dependentEntityFieldId: string;
}

interface EntityFieldVlpConfig extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    vlpContexts?: EntityFieldVlpContext[];
    vlpId: string;
    valueField: string;
    idField?: string;
}

interface EntityFieldVlpContext extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    vlpParams?: EntityFieldVlpParam[];
    entityClassId?: string;
    search: boolean;
    input: boolean;
}

interface EntityFieldVlpParam extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    name: string;
    value?: string;
    valueFromEntityFieldId?: string;
}

interface ObjectFactory {
}

interface Serializable {
}

interface Cloneable {
}

interface CopyTo {
}

interface Equals {
}

interface HashCode {
}

interface ToString {
}
