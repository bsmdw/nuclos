export class HttpMethod {
	static GET = 'GET';
	static POST = 'POST';
	static PUT = 'PUT';
	static DELETE = 'DELETE';
}

export interface Link {
	href: string;
	methods?: Array<HttpMethod>;
}

export interface LinkContainer {
	self: Link;
	defaultLayout?: Link;
	defaultGeneration?: Link;
	printouts?: Link;
	bos?: Link;
	boMeta?: Link;
	layout?: Link;
	insert?: Link;
	clone?: Link;
	stateIcon?: Link;
	lock?: Link;
	detail?: Link;
}

export interface SubEOLinkContainer {
	bos: Link;
	boMeta: Link;
	clone?: Link;
}
