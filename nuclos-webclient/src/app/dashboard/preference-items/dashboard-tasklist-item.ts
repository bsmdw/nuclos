import { TaskList } from '../../task/shared/task-list';
import { DashboardGridItem } from '../shared/dashboard-grid-item';

export class DashboardTasklistItem extends DashboardGridItem {
	taskList: TaskList;

	constructor(
		taskListId: string,
		taskListName: string,
		searchFilterId?: string
	) {
		super();

		this.type = 'tasklist';

		this.taskList = {
			entityClassId: taskListId,
			name: taskListName,
			searchfilter: searchFilterId
		};
	}
}
