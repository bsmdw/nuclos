import { AttributeSelectionContent } from '../../preferences/preferences.model';

export class TaskListPreferenceContent extends AttributeSelectionContent {
	taskListId: string;
}
