export type DashboardGridItemType = 'tasklist';

export class DashboardGridItem {

	type: DashboardGridItemType;

	x: number;
	y: number;

	rows?: number;
	cols?: number;
}
