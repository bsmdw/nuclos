import { Component, NgZone, OnInit } from '@angular/core';
import { DashboardService } from '../shared/dashboard.service';

@Component({
	selector: 'nuc-dashboard-notification-count',
	templateUrl: './dashboard-notification-count.component.html',
	styleUrls: ['./dashboard-notification-count.component.css']
})
export class DashboardNotificationCountComponent implements OnInit {
	notificationCount = 0;

	constructor(
		private dashboardService: DashboardService,
		private ngZone: NgZone,
	) {
	}

	ngOnInit() {
		this.notificationCount = this.dashboardService.getNotificationCount();

		// TODO: Disabled for now - better implement some kind of push notification (Websocket)
		// // Run the interval outside of Angular, or NgZone.hasPendingMacrotasks will be forever true
		// this.ngZone.runOutsideAngular(() => {
		// 	// TODO: Should the update interval somehow configurable?
		// 	// TODO: The dashboard-service should trigger the updates and provide an Observable, to which we simply can subscribe here
		// 	Observable.interval(1000 * 3).subscribe(
		// 		// Re-enter Angular to make changes
		// 		() => this.ngZone.run(() =>
		// 			this.notificationCount = this.dashboardService.getNotificationCount()
		// 		)
		// 	);
		// })
	}

	getNotificationCount() {
		return this.notificationCount;
	}
}
