import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of as observableOf, throwError as observableThrowError } from 'rxjs';

import { catchError, mergeMap, retryWhen, tap } from 'rxjs/operators';
import { EntityObjectErrorService } from '../../entity-object-data/shared/entity-object-error.service';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { DetailModalService } from '../../entity-object/shared/detail-modal.service';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { InputRequiredService } from '../../input-required/shared/input-required.service';
import { Logger } from '../../log/shared/logger';
import { DialogService } from '../../popup/dialog/dialog.service';
import { NuclosHttpService } from '../../shared/nuclos-http.service';
import { GenerationResultComponent } from '../generation-result/generation-result.component';
import { Generation, GenerationResult } from './generation';

@Injectable()
export class NuclosGenerationService {

	constructor(
		private http: NuclosHttpService,
		private inputRequired: InputRequiredService,
		private modalService: NgbModal,
		private eoErrorService: EntityObjectErrorService,
		private nuclosI18n: NuclosI18nService,
		private dialog: DialogService,
		private detailModalService: DetailModalService,
		private $log: Logger
	) {
	}

	confirmAndGenerate(
		eo: EntityObject,
		generation: Generation,
		popupParameter?: string,
	) {
		if (!generation) {
			return;
		}

		eo.getMeta().subscribe(meta => {
			let title = this.nuclosI18n.getI18n(
				'webclient.dialog.generate.header',
				generation.target
			);

			let message = this.nuclosI18n.getI18n(
				'webclient.dialog.generate.message',
				meta.getEntityName(),
				generation.target
			);

			if (generation.nonstop) {
				this.$log.debug('Generate %o', generation);
				this.generateEO(
					eo,
					generation.generationId,
					popupParameter
				).subscribe();
			} else {
				this.dialog.confirm({
					title: title,
					message: message
				}).then(() => {
					this.$log.debug('Generate %o', generation);
					this.generateEO(
						eo,
						generation.generationId,
						popupParameter
					).subscribe();
				}).catch(() => this.$log.debug('Generation aborted'));
			}
		});
	}

	/**
	 * Executes the given generation.
	 * Possibly opens the result in the given target window (or else closes it).
	 *
	 * Saves the source EO first, if it is dirty.
	 */
	generateEO(
		sourceEo: EntityObject,
		generationId: string,
		popupparameter?: string
	): Observable<GenerationResult> {

		return observableOf(sourceEo).pipe(
			mergeMap(eo => eo.isDirty() ? eo.save() : observableOf(eo)),
			mergeMap(eo => {
				let generation = eo.getGeneration(generationId);
				if (!generation) {
					return observableThrowError('Unknown generation: ' + generationId);
				}
				let postData = {};
				return this.http.post(generation.links.generate.href, postData).pipe(
					tap((result: GenerationResult) => {
						if (!result.complete) {
							result.bo.businessError = result.businessError;
							localStorage.setItem('' + result.bo.temporaryId, JSON.stringify(result.bo));
						}

						this.showResult(result, eo, popupparameter);

						if (result.refreshSource) {
							eo.reload().subscribe(
								reloadedEo => reloadedEo.select()
							);
						}
					}),
					retryWhen(
						// Retry until all InputRequired exceptions are handled
						errors => errors.pipe(mergeMap(
							error => this.inputRequired.handleError(error, postData, eo)
						))
					),
					catchError(
						e => this.eoErrorService.handleError(e)
					),
				);
			}),
		);
	}

	private showResult(result: GenerationResult, sourceEo: EntityObject, popupparameter?: string) {
		if (result.showGenerated || !result.complete) {

			if (result.openInOverlay) {
				let eo = new EntityObject(result.bo);
				this.detailModalService.openEoInModal(eo, sourceEo, true);
			} else {
				let url = window.location.href;
				let eoLink = url.substring(0, url.indexOf('#'))
					+ (popupparameter ? '#/popup/' : '#/view/')
					+ result.bo.boMetaId
					+ '/' + (result.bo.boId || result.bo.temporaryId)
					+ '?refreshothertabs';

				let newWindow = window.open(eoLink, '_blank', popupparameter);
				if (!newWindow) {
					this.showResultModal(result, eoLink);
				}
			}
		}
	}

	private showResultModal(result: GenerationResult, eoLink: string) {
		let ngbModalRef = this.modalService.open(
			GenerationResultComponent
		);

		ngbModalRef.componentInstance.generationResult = result;
		ngbModalRef.componentInstance.targetURL = eoLink;

		ngbModalRef.result.then(res => {
			this.$log.debug('Result: %o', res);
		});
	}
}
