import { Component, Input, OnInit } from '@angular/core';
import { EntityObject } from '../entity-object-data/shared/entity-object.class';
import { Logger } from '../log/shared/logger';
import { Generation } from './shared/generation';
import { NuclosGenerationService } from './shared/nuclos-generation.service';

@Component({
	selector: 'nuc-generation',
	templateUrl: './generation.component.html',
	styleUrls: ['./generation.component.css']
})
export class GenerationComponent implements OnInit {
	@Input() eo: EntityObject;

	constructor(
		private $log: Logger,
		private generationService: NuclosGenerationService
	) {
	}

	ngOnInit() {
	}

	generate(generation: Generation) {
		this.generationService.confirmAndGenerate(
			this.eo,
			generation
		);
	}
}
