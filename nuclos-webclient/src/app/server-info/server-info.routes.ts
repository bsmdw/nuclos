import { RouterModule, Routes } from '@angular/router';
import { ServerInfoComponent } from './server-info.component';

export const ROUTE_CONFIG: Routes = [
	{
		path: 'serverinfo',
		component: ServerInfoComponent
	}
];

export const ServerInfoRoutes = RouterModule.forChild(ROUTE_CONFIG);
