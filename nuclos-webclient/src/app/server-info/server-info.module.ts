import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { I18nModule } from '../i18n/i18n.module';
import { LogModule } from '../log/log.module';
import { IsLastComponent } from './is-last/is-last.component';
import { ServerInfoComponent } from './server-info.component';
import { ServerInfoRoutes } from './server-info.routes';
import { ServerLogOptionsComponent } from './server-log-options/server-log-options.component';
import { ServerLogFilterPipe } from './server-log-viewer/server-log-filter.pipe';
import { ServerLogViewerComponent } from './server-log-viewer/server-log-viewer.component';
import { HighlightPipe } from './shared/highlight.pipe';
import { ServerLogService } from './shared/server-log.service';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,

		LogModule,
		I18nModule,

		ServerInfoRoutes
	],
	declarations: [
		ServerInfoComponent,
		ServerLogViewerComponent,
		IsLastComponent,
		ServerLogOptionsComponent,
		ServerLogFilterPipe,
		HighlightPipe
	],
	providers: [
		ServerLogFilterPipe,
		ServerLogService
	]
})
export class ServerInfoModule {
}
