import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { I18nModule } from '../i18n/i18n.module';
import { InputRequiredDialogComponent } from './input-required-dialog/input-required-dialog.component';
import { InputRequiredComponent } from './input-required.component';
import { InputRequiredService } from './shared/input-required.service';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,

		I18nModule
	],
	declarations: [
		InputRequiredComponent,
		InputRequiredDialogComponent
	],
	providers: [
		InputRequiredService
	],
	entryComponents: [
		InputRequiredDialogComponent
	]
})
export class InputRequiredModule {
}
