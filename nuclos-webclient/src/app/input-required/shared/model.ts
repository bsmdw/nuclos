export interface InputRequired {
	specification: InputRequiredSepecification;
	result: any; // TODO
}

export interface InputRequiredSepecification {
	defaultoption: any;
	options?: Array<string>;
	key: string;
	type: any;
	message: string;
	delegateComponent?: string;
	data?: Map<string, any>;
}
