import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IEntityObject } from '@nuclos/nuclos-addon-api';
import {
	EMPTY,
	from as observableFrom,
	Observable,
	of as observableOf,
	throwError as observableThrowError
} from 'rxjs';

import { catchError, tap } from 'rxjs/operators';
import { AddonService } from '../../addons/addon.service';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { Logger } from '../../log/shared/logger';
import { DialogService } from '../../popup/dialog/dialog.service';
import { StringUtils } from '../../shared/string-utils';
import { InputRequiredDialogComponent } from '../input-required-dialog/input-required-dialog.component';
import { InputRequired } from './model';

/**
 * Handles "Input Required" which can occur whenever an EO is saved.
 * This service does not save the EO itself or some strange callback magic.
 */
@Injectable()
export class InputRequiredService {

	constructor(private modalService: NgbModal,
				private addonService: AddonService,
				private dialogService: DialogService,
				private nuclosI18nService: NuclosI18nService,
				private $log: Logger
	) {
	}

	/**
	 * Handles possible InputRequired "errors".
	 * Returns an Observable that handles the InputRequired specification
	 * or just an Observable containing the given error if no InputRequired was found.
	 *
	 * @param response The error response that occurred while sending the data. Could contain an InputRequired specification.
	 * @param data The original request data.
	 * @returns {any}
	 */
	handleError(
		response: HttpErrorResponse,
		data: any,
		eo: IEntityObject
	): Observable<any> {
		if (response.error && response.error.inputrequired) {
			let inputRequired = <InputRequired>response.error.inputrequired;
			return this.handleInputRequired(inputRequired, eo).pipe(
				tap(result => {
					// data might have a "inputrequired" attribute from a previous InputRequired error.
					// The previous result must be merged with the current one and sent again.
					let nextInputRequired = data.inputrequired || inputRequired;
					if (!nextInputRequired.result) {
						nextInputRequired.result = {};
					}
					if (inputRequired.specification.key) {
						nextInputRequired.result[inputRequired.specification.key] = result;
					} else {
						// addon (InputDelegateSpecification has no key/message/type)
						for (const key of Object.keys(result)) {
							const value = result[key];
							nextInputRequired.result[key] = value;
						}
					}
					data.inputrequired = nextInputRequired;
				})
			);
		}

		// If no InputRequired was handled, just re-throw the error
		return observableThrowError(response);
	}

	/**
	 * Handles a concrete InputRequired object.
	 */
	private handleInputRequired(
		inputRequired: InputRequired,
		eo: IEntityObject
	): Observable<any> {
		if (!inputRequired || !inputRequired.specification) {
			return observableOf(undefined);
		}

		// if the InputRequiredException is called with InputDelegateSpecification an addon will be instantiated (counterpart to client extension)
		let inputRequiredDialogComponent = InputRequiredDialogComponent;
		const delegateComponentPackageName = inputRequired.specification.delegateComponent;
		if (delegateComponentPackageName) {
			// use 'class-name' for instantiating addon, package will be ignored
			// this makes it possible to use InputRequiredExceptions with InputDelegateSpecification for Java-Extensions and WebAddons
			const delegateComponentName = delegateComponentPackageName.split('.').pop();
			if (delegateComponentName) {
				const delegateComponent = this.addonService.getComponentFactoryClass(delegateComponentName);
				if (!delegateComponent) {
					let errorMessage = 'Unable to find addon component ' + delegateComponentPackageName + ' for input required exception.';
					this.$log.error(errorMessage);

					this.dialogService.alert(
						{
							title: this.nuclosI18nService.getI18n('webclient.error.title'),
							message: StringUtils.textToHtml(errorMessage)
						}
					);
					return EMPTY;
				} else {
					inputRequiredDialogComponent = <any>delegateComponent;
				}
			}
		}

		const ngbModalRef = this.modalService.open(inputRequiredDialogComponent, {size: 'lg'});
		ngbModalRef.componentInstance.specification = inputRequired.specification;
		ngbModalRef.componentInstance.eo = eo;
		ngbModalRef.componentInstance.ngbModalRef = ngbModalRef;

		return observableFrom(ngbModalRef.result).pipe(
			// modal throws error when closing via ESC or click outside of modal
			catchError(error => {
				this.$log.error('Modal closed', error);
				return EMPTY;
			})
		);
	}
}
