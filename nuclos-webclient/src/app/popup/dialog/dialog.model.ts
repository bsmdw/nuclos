export class DialogOptions {

	/**
	 * id used for getting modal reference
	 */
	dialogId?: number;

	/**
	 * The title of the confirmation modalRef
	 */
	title: string;

	/**
	 * The message in the confirmation modalRef
	 */
	message: string;
}
