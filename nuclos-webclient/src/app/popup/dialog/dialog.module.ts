import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { I18nModule } from '../../i18n/i18n.module';
import { AlertComponent } from './alert/alert.component';
import { AlertDirective } from './alert/alert.directive';
import { ConfirmComponent } from './confirm/confirm.component';
import { ConfirmDirective } from './confirm/confirm.directive';
import { DialogComponent } from './dialog.component';
import { DialogOptions } from './dialog.model';
import { DialogService, DialogState } from './dialog.service';
import { ReplaceLinebreaksPipe } from './replace-linebreaks.pipe';

@NgModule({
	imports: [
		CommonModule,
		I18nModule,

		NgbModule.forRoot(),
	],
	declarations: [
		AlertDirective,
		ReplaceLinebreaksPipe,
		ConfirmDirective,
		AlertComponent,
		ConfirmComponent,
		ConfirmDirective,
		DialogComponent
	],
	providers: [
		DialogService,
		DialogState,
		DialogOptions
	],
	exports: [
		ReplaceLinebreaksPipe,
		DialogComponent
	]
})
export class DialogModule {
}
