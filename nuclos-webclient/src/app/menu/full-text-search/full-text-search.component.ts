import { Component, Input, OnInit } from '@angular/core';
import { FullTextSearchResultItem } from './full-text-search.model';
import { FullTextSearchService } from './full-text-search.service';

@Component({
	selector: 'nuc-full-text-search',
	templateUrl: './full-text-search.component.html',
	styleUrls: ['./full-text-search.component.css']
})
export class FullTextSearchComponent implements OnInit {

	showFullTextSearch = false;

	@Input() searchtext: string;

	results: FullTextSearchResultItem[] = [];


	constructor(private fullTextSearchService: FullTextSearchService) {
	}


	ngOnInit() {
		this.fullTextSearchService.isFullTextSearchEnabled().subscribe(enabled => this.showFullTextSearch = enabled);
	}


	searchtextChanged() {
		if (this.searchtext) {
			if (this.searchtext.length === 0) {
				this.results = [];
			}

			this.fullTextSearchService.search(this.searchtext).subscribe(
				(results: FullTextSearchResultItem[]) => {
					this.results = results;
				}
			);
		}
	}
}
