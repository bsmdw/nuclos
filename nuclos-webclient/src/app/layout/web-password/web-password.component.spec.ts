/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WebPasswordComponent } from './web-password.component';

xdescribe('WebPasswordComponent', () => {
	let component: WebPasswordComponent;
	let fixture: ComponentFixture<WebPasswordComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [WebPasswordComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebPasswordComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
