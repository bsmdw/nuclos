import { Component, Injector } from '@angular/core';
import { WebButtonComponent } from '../web-button.component';

@Component({
	selector: 'nuc-web-button-dummy',
	templateUrl: '../web-button.component.html',
	styleUrls: ['../web-button.component.css']
})
export class WebButtonDummyComponent extends WebButtonComponent<WebButtonDummy> {

	constructor(injector: Injector) {
		super(injector);
	}

	buttonClicked() {
		alert('Dummy!');
	}

	getCssClass(): string {
		return 'nuc-button-dummy';
	}
}
