/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WebMatrixComponent } from './web-matrix.component';

xdescribe('WebMatrixComponent', () => {
	let component: WebMatrixComponent;
	let fixture: ComponentFixture<WebMatrixComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [WebMatrixComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebMatrixComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
