import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';

@Component({
	selector: 'nuc-web-grid-calculated',
	templateUrl: './web-grid-calculated.component.html',
	styleUrls: ['./web-grid-calculated.component.css']
})
export class WebGridCalculatedComponent implements OnInit, OnChanges {

	@Input() webGridCalculated: WebGridCalculated;
	@Input() eo: EntityObject;

	containerStyle;
	cellStyles;

	constructor() {
	}

	ngOnInit() {
		this.updateStyles();
	}

	ngOnChanges(changes: SimpleChanges): void {
		this.updateStyles();
	}

	private updateStyles() {
		this.containerStyle = {
			minWidth: this.webGridCalculated.minWidth + 'px',
			minHeight: this.webGridCalculated.minHeight + 'px'
		};

		this.cellStyles = [];
		if (this.webGridCalculated && this.webGridCalculated.cells) {
			for (let cell of this.webGridCalculated.cells) {
				this.cellStyles.push(
					{
						position: 'absolute',
						left: cell.left,
						top: cell.top,
						width: cell.width,
						height: cell.height,
						minWidth: cell.width,
						minHeight: cell.height
					}
				);
			}
		}
	}
}
