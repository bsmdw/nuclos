import { Component, Injector, OnInit } from '@angular/core';
import { HyperlinkService } from '../../shared/hyperlink.service';
import { AbstractInputComponent } from '../shared/abstract-input-component';

@Component({
	selector: 'nuc-web-hyperlink',
	templateUrl: './web-hyperlink.component.html',
	styleUrls: ['./web-hyperlink.component.css']
})
export class WebHyperlinkComponent extends AbstractInputComponent<WebHyperlink> implements OnInit {

	constructor(
		injector: Injector,
		private hyperlinkService: HyperlinkService
	) {
		super(injector);
	}

	ngOnInit() {
	}

	openLink() {
		let url = this.getUrl();
		if (url) {
			this.hyperlinkService.open(url);
		}
	}

	getUrl(): string {
		let url = this.getHyperlinkAttribute();

		if (url) {
			url = this.hyperlinkService.validateURL(url);
		}

		return url;
	}

	private getHyperlinkAttribute(): any {
		return this.eo.getAttribute(this.webComponent.name);
	}
}
