import { Component, ElementRef, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { LovEntry } from '../../../../entity-object-data/shared/bo-view.model';
import { EntityObjectEventService } from '../../../../entity-object-data/shared/entity-object-event.service';
import { EntityObjectService } from '../../../../entity-object-data/shared/entity-object.service';
import { LovDataService } from '../../../../entity-object-data/shared/lov-data.service';
import { FqnService } from '../../../../shared/fqn.service';
import { IdFactoryService } from '../../../../shared/id-factory.service';
import { ComboboxUtils } from '../../../shared/combobox-utils';
import { AbstractLovEditorComponent } from '../abstract-lov-editor.component';

@Component({
	selector: 'nuc-subform-combobox-editor',
	templateUrl: '../subform-lov-editor/subform-lov-editor.component.html',
	styleUrls: ['../subform-lov-editor/subform-lov-editor.component.scss']
})
export class SubformComboboxEditorComponent extends AbstractLovEditorComponent {

	constructor(
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService,
		protected elementRef: ElementRef,
		protected idFactory: IdFactoryService,
		protected fqnService: FqnService,
		injector: Injector
	) {
		super(
			entityObjectService,
			eoEventService,
			elementRef,
			idFactory,
			fqnService,
			injector
		);
	}

	loadEntries(): Observable<LovEntry[]> {
		return this.subEO.getLovEntries(
			this.attributeMeta.getAttributeID(),
			this.getVlp()
		).pipe(take(1));
	}

	loadFilteredEntries(search: string): Observable<LovEntry[]> {
		return ComboboxUtils.filter(this.loadEntries(), search);
	}

	getLovDataService(): LovDataService | undefined {
		return undefined;
	}
}
