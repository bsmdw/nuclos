import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SubformStringEditorComponent } from './subform-string-editor.component';

xdescribe('SubformStringEditorComponent', () => {
	let component: SubformStringEditorComponent;
	let fixture: ComponentFixture<SubformStringEditorComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [SubformStringEditorComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(SubformStringEditorComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
