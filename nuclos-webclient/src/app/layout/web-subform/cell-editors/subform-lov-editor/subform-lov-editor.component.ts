import { Component, ElementRef, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { LovEntry } from '../../../../entity-object-data/shared/bo-view.model';
import { EntityObjectEventService } from '../../../../entity-object-data/shared/entity-object-event.service';
import { EntityObjectService } from '../../../../entity-object-data/shared/entity-object.service';
import { LovDataService } from '../../../../entity-object-data/shared/lov-data.service';
import { FqnService } from '../../../../shared/fqn.service';
import { IdFactoryService } from '../../../../shared/id-factory.service';
import { AbstractLovEditorComponent } from '../abstract-lov-editor.component';

@Component({
	selector: 'nuc-subform-lov-editor',
	templateUrl: './subform-lov-editor.component.html',
	styleUrls: ['./subform-lov-editor.component.scss']
})
export class SubformLovEditorComponent extends AbstractLovEditorComponent {

	constructor(
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService,
		protected lovDataService: LovDataService,
		protected elementRef: ElementRef,
		protected idFactory: IdFactoryService,
		fqnService: FqnService,
		injector: Injector
	) {
		super(
			entityObjectService,
			eoEventService,
			elementRef,
			idFactory,
			fqnService,
			injector
		);
	}

	loadEntries(): Observable<LovEntry[]> {
		return this.loadFilteredEntries('');
	}

	loadFilteredEntries(search: string): Observable<LovEntry[]> {
		return this.lovDataService.loadLovEntries(this.getLovSearchConfig(search));
	}

	stopEditing(): void {
		this.getGridApi().stopEditing();
	}

	getLovDataService(): LovDataService | undefined {
		return this.lovDataService;
	}
}
