import { Component, ElementRef, HostListener, ViewChild } from '@angular/core';
import { EntityObjectEventService } from '../../../../entity-object-data/shared/entity-object-event.service';
import { EntityObjectService } from '../../../../entity-object-data/shared/entity-object.service';
import { AbstractEditorComponent } from '../abstract-editor-component';


/**
 * TODO: This is almost an exact duplicate of SubformBooleanRendererComponent.
 */
@Component({
	selector: 'nuc-subform-boolean-editor',
	templateUrl: './subform-boolean-editor.component.html',
	styleUrls: ['./subform-boolean-editor.component.css']
})
export class SubformBooleanEditorComponent extends AbstractEditorComponent {

	@ViewChild('checkSpan') checkSpan: ElementRef;

	constructor(
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService,
		ref: ElementRef
	) {
		super(entityObjectService, eoEventService, ref);
	}

	@HostListener('window:keydown', ['$event'])
	handleKeyboardEvent(event: KeyboardEvent) {
		if (event.code === 'Space' || event.code === 'Enter') {
			this.toggleValue();
		}
		if (event.code === 'Tab') {
			if (event.shiftKey) {
				this.getGridApi().tabToPreviousCell();
			} else {
				this.getGridApi().tabToNextCell();
			}
		}
	}

	toggleValue() {
		this.setValue(!this.getValue());
		let eo = this.getEntityObject();
		if (eo) {
			let colDef = this.getColDef();
			if (colDef && colDef.field) {
				this.getSubEntityObject().setAttribute(colDef.field, this.getValue());
			}
		}
	}

	isPopup(): boolean {
		// TODO: This is the only editor without popup.
		// It would be better if all subform editors behaved the same way.
		return false;
	}
}
