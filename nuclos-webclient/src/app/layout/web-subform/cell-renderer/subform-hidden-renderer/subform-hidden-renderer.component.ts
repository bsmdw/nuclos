import { Component } from '@angular/core';
import { EntityObjectEventService } from '../../../../entity-object-data/shared/entity-object-event.service';
import { EntityObjectService } from '../../../../entity-object-data/shared/entity-object.service';
import { AbstractRendererComponent } from '../abstract-renderer-component';

@Component({
	selector: 'nuc-hidden-icon-renderer',
	templateUrl: './subform-hidden-renderer.component.html',
	styleUrls: ['./subform-hidden-renderer.component.css']
})
export class SubformHiddenRendererComponent extends AbstractRendererComponent {

	constructor(
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService
	) {
		super(entityObjectService, eoEventService);
	}

	agInit(params: any) {
		super.agInit(params);
	}

	refresh(params: any): boolean {
		return false;
	}

}
