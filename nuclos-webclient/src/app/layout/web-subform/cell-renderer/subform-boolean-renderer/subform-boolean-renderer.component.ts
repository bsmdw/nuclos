import { Component } from '@angular/core';
import { EntityObjectEventService } from '../../../../entity-object-data/shared/entity-object-event.service';
import { EntityObjectService } from '../../../../entity-object-data/shared/entity-object.service';
import { NuclosCellRendererParams } from '../../web-subform.model';
import { AbstractRendererComponent } from '../abstract-renderer-component';

@Component({
	selector: 'nuc-subform-boolean-renderer',
	templateUrl: '../../cell-editors/subform-boolean-editor/subform-boolean-editor.component.html',
	styleUrls: ['subform-boolean-renderer.component.css']
})
export class SubformBooleanRendererComponent extends AbstractRendererComponent {

	private isEditable = false;

	constructor(
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService
	) {
		super(entityObjectService, eoEventService);
	}

	agInit(params: any) {
		super.agInit(params);
		let nuclosCellRenderParams = params.colDef.cellRendererParams.nuclosCellRenderParams as NuclosCellRendererParams;
		this.isEditable = nuclosCellRenderParams.editable;
	}

	toggleValue() {
		if (!this.isEditable) {
			return;
		}

		this.setValue(!this.getValue());
		let eo = this.getEntityObject();
		if (eo) {
			let colDef = this.getColDef();
			if (colDef && colDef.field) {
				this.getSubEntityObject().setAttribute(colDef.field, this.getValue());
			}
		}
	}
}
