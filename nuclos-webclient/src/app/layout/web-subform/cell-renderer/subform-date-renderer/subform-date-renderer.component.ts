import { Component } from '@angular/core';
import { EntityObjectEventService } from '../../../../entity-object-data/shared/entity-object-event.service';
import { EntityObjectService } from '../../../../entity-object-data/shared/entity-object.service';
import { DatetimeService } from '../../../../shared/datetime.service';
import { AbstractRendererComponent } from '../abstract-renderer-component';

@Component({
	selector: 'nuc-subform-date-renderer',
	templateUrl: './subform-date-renderer.component.html',
	styleUrls: ['./subform-date-renderer.component.css']
})
export class SubformDateRendererComponent extends AbstractRendererComponent {

	value: string;

	constructor(
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService,
		private datetimeService: DatetimeService
	) {
		super(entityObjectService, eoEventService);
	}

	agInit(params: any) {
		super.agInit(params);
		this.refresh(params);
	}

	refresh(params: any): boolean {
		if (this.getNuclosCellRenderParams() && this.getNuclosCellRenderParams().attrMeta.isTimestamp()) {
			this.value = this.datetimeService.formatTimestamp(params.value);
		} else {
			this.value = this.datetimeService.formatDate(params.value);
		}
		return false;
	}
}

