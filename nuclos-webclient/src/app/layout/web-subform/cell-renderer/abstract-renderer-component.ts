import { AgRendererComponent } from 'ag-grid-angular';
import { EntityObjectEventService } from '../../../entity-object-data/shared/entity-object-event.service';
import { EntityObjectService } from '../../../entity-object-data/shared/entity-object.service';
import { AbstractCellComponent } from '../abstract-cell-component';
import { NuclosCellRendererParams } from '../web-subform.model';

export abstract class AbstractRendererComponent
	extends AbstractCellComponent<NuclosCellRendererParams>
	implements AgRendererComponent {

	nuclosCellRenderParams: NuclosCellRendererParams;

	constructor(
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService,
	) {
		super(entityObjectService, eoEventService, undefined);
	}

	agInit(params: any) {
		super.init(params);
		this.refresh(params);

		this.nuclosCellRenderParams = params.nuclosCellRenderParams;
	}

	refresh(params: any): boolean {
		super.updateParams(params);
		return false;
	}

	getNuclosCellRenderParams(): NuclosCellRendererParams {
		return this.nuclosCellRenderParams;
	}
}
