import { Component } from '@angular/core';
import { EntityObjectEventService } from '../../../../entity-object-data/shared/entity-object-event.service';
import { EntityObjectService } from '../../../../entity-object-data/shared/entity-object.service';
import { NuclosConfigService } from '../../../../shared/nuclos-config.service';
import { AbstractRendererComponent } from '../abstract-renderer-component';

@Component({
	selector: 'nuc-subform-state-icon-renderer',
	templateUrl: './subform-state-icon-renderer.component.html',
	styleUrls: ['./subform-state-icon-renderer.component.css']
})
export class SubformStateIconRendererComponent extends AbstractRendererComponent {

	stateIconUrl: string;

	constructor(
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService,
		private nuclosConfigService: NuclosConfigService
	) {
		super(entityObjectService, eoEventService);
	}

	agInit(params: any) {
		super.agInit(params);
		this.updateStateIconUrl();
	}

	refresh(params: any): boolean {
		super.refresh(params);
		this.updateStateIconUrl();
		return false;
	}

	private updateStateIconUrl() {
		let state = this.getSubEntityObject().getState();
		// TODO why is state.links.stateIcon not set in REST?
		if (state && state.nuclosStateId) {
			this.stateIconUrl = this.nuclosConfigService.getRestHost() + '/resources/stateIcons/' + state.nuclosStateId;
		} else {
			this.stateIconUrl = '';
		}
	}
}
