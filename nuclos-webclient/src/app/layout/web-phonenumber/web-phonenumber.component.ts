import { Component, Injector, OnInit } from '@angular/core';
import { AbstractInputComponent } from '../shared/abstract-input-component';

@Component({
	selector: 'nuc-web-phonenumber',
	templateUrl: './web-phonenumber.component.html',
	styleUrls: ['./web-phonenumber.component.css']
})
export class WebPhonenumberComponent extends AbstractInputComponent<WebPhonenumber> implements OnInit {

	constructor(injector: Injector) {
		super(injector);
	}

	ngOnInit() {
	}

}
