import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LovEntry } from '../../entity-object-data/shared/bo-view.model';
import { StringUtils } from '../../shared/string-utils';

export class ComboboxUtils {
	static filter(observable: Observable<LovEntry[]>, search: string) {
		let regex = StringUtils.regexFromSearchString(search);

		return observable.pipe(map(
			entries => entries.filter(entry => {
				let match = entry.name.match(regex);
				return match;
			})
		));
	}
}
