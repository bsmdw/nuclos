/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WebTableComponent } from './web-table.component';

xdescribe('WebTableComponent', () => {
	let component: WebTableComponent;
	let fixture: ComponentFixture<WebTableComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [WebTableComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebTableComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
