import { Component, Injector, OnInit } from '@angular/core';
import { NuclosValidationService } from '../../validation/nuclos-validation.service';
import { AbstractInputComponent } from '../shared/abstract-input-component';

@Component({
	selector: 'nuc-web-email',
	templateUrl: './web-email.component.html',
	styleUrls: ['./web-email.component.css']
})
export class WebEmailComponent extends AbstractInputComponent<WebEmail> implements OnInit {

	constructor(
		injector: Injector,
		private validationService: NuclosValidationService
	) {
		super(injector);
	}

	ngOnInit() {
	}

	isValid(): boolean {
		return this.validationService.isValidForInput(this.getModelValue(), 'email');
	}
}
