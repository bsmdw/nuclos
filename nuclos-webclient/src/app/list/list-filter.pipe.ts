import { Pipe, PipeTransform } from '@angular/core';

/**
 * filters an array by multiple property values
 *
 * usage:
 * list | {attribute1: value, attribute2: value, ...}
 *
 * example:
 * personList | {firstName: 'Mustermann'}
 */
@Pipe({
	name: 'listFilter',
	pure: false
})
export class ListFilterPipe implements PipeTransform {

	transform(value: any, args: any[]): any {
		let filter = args;

		if (filter && Array.isArray(value)) {
			return this.filterArray(value, filter);
		} else {
			return value;
		}
	}


	private filterArray(list: any[], filter: object): any {
		let filterKeys = Object.keys(filter);
		return list.filter(
			item =>
				filterKeys.reduce(
					(filterItem, keyName) => {
						if (filterItem) {
							if (filter[keyName] === undefined || filter[keyName].length === 0) {
								return true;
							}
							if (item[keyName] && item[keyName].toLowerCase && filter[keyName]) {
								return item[keyName].toLowerCase().indexOf(filter[keyName].toLowerCase()) !== -1;
							}
						}
						return filterItem && item[keyName] === filter[keyName];
					},
					true
				)
		);
	}
}
