import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { StateInfo } from '../shared/state';

@Component({
	selector: 'nuc-next-state',
	templateUrl: './next-state.component.html',
	styleUrls: ['./next-state.component.css']
})
export class NextStateComponent implements OnInit {
	@Input() state: StateInfo;
	@Input() saveInProgress: boolean;

	@Output() changeState = new EventEmitter<StateInfo>();

	constructor() {
	}

	ngOnInit() {
	}

}
