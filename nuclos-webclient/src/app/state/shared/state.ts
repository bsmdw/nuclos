import { Link } from '../../shared/link.model';

export interface State {
	nuclosStateId: string;
	name: string;
	number: number;

	links: {
		stateIcon?: Link
	};
}

export interface StateInfo extends State {
	nonstop: boolean;
	buttonLabel: string;
	currentState?: boolean;

	links: {
		stateIcon?: Link,
		change?: Link
	};
}

/**
 * Represents 'statusinfo' from '/data/statusinfo/' + uid
 * We need another interface here, because the attribute names are different...
 * TODO: Define a common interface for state infos
 * TODO: Create a new REST service for this
 */
export interface OldStateInfo {
	description: string;
	numeral: number;
	statename: string;
	color?: string;

	/**
	 * Button icon resource UID, if configured.
	 */
	buttonIcon?: string;
	/**
	 * The corresponding resource URL for the button icon.
	 */
	buttonIconResourceURL?: string;
}
