import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NewsMenuComponent } from './news-menu/news-menu.component';
import { NewsRouteComponent } from './news-route/news-route.component';
import { NewsRoutes } from './news.routes';

@NgModule({
	imports: [
		CommonModule,

		NewsRoutes,
	],
	declarations: [
		NewsMenuComponent,
		NewsRouteComponent,
	],
	exports: [
		NewsMenuComponent,
		NewsRouteComponent,
	]
})
export class NewsModule {
}
