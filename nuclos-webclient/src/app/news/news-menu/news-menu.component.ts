import { Component, OnInit } from '@angular/core';
import { NewsService } from '../shared/news.service';

@Component({
	selector: 'nuc-news-menu',
	templateUrl: './news-menu.component.html',
	styleUrls: ['./news-menu.component.css']
})
export class NewsMenuComponent implements OnInit {
	news: News[];

	constructor(
		private newsService: NewsService,
	) {
	}

	ngOnInit() {
		this.newsService.getNews().subscribe(
			data => {
				if (data && data.length > 0) {
					this.news = data;
				}
			}
		);
	}

	showNews(news) {
		this.newsService.showNews(news);
	}
}
