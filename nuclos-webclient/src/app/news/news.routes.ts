import { RouterModule, Routes } from '@angular/router';
import { CanNeverActivateGuard } from '../shared/can-never-activate-guard';
import { NewsRouteComponent } from './news-route/news-route.component';

export const ROUTE_CONFIG: Routes = [
	{
		path: 'news/:idOrNameOrTitle',
		component: NewsRouteComponent,
		canActivate: [
			CanNeverActivateGuard
		]
	},
	{
		// For backward compatibility to old /disclaimer route
		path: 'disclaimer/:idOrNameOrTitle',
		component: NewsRouteComponent,
		canActivate: [
			CanNeverActivateGuard
		]
	},
];
//
export const NewsRoutes = RouterModule.forChild(ROUTE_CONFIG);
