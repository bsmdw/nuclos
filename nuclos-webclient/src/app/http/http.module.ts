import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpHandler } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { CacheModule } from '../cache/cache.module';
import { NuclosCacheService } from '../cache/shared/nuclos-cache.service';
import { LogModule } from '../log/log.module';
import { Logger } from '../log/shared/logger';
import { NuclosHttpService } from '../shared/nuclos-http.service';
import { LoadingIndicatorComponent } from './loading-indicator/loading-indicator.component';

export function httpFactory(
	handler: HttpHandler,
	$log: Logger,
	cacheService: NuclosCacheService,
	injector: Injector
) {
	return new NuclosHttpService(handler, $log, cacheService, injector);
}

@NgModule({
	imports: [
		CommonModule,
		HttpClientModule,

		LogModule,
		CacheModule,
	],
	declarations: [
		LoadingIndicatorComponent
	],
	exports: [
		LoadingIndicatorComponent
	],
	providers: [
		{
			provide: NuclosHttpService,
			useFactory: httpFactory,
			deps: [HttpHandler, Logger, NuclosCacheService, Injector]
		},
	]
})
export class HttpModule {
}
