import { ChangeDetectorRef, Component, ElementRef, NgZone, OnInit } from '@angular/core';
import { ANIMATION_TYPES } from 'ngx-loading';
import { BrowserDetectionService } from './shared/browser-detection.service';
import { BusyService } from './shared/busy.service';

@Component({
	selector: 'nuc-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
	title = 'Nuclos Webclient v2';

	busy = false;
	loadingAnimationType = ANIMATION_TYPES.threeBounce;

	constructor(
		private elementRef: ElementRef,
		private browserDetectionService: BrowserDetectionService,
		private busyService: BusyService,
		private ngZone: NgZone,
		private changeDetectorRef: ChangeDetectorRef,
	) {
		window['NgZone'] = this.ngZone;
	}

	ngOnInit(): void {
		let canonicalUrl = window.location.href.replace(/([^:]\/)\/+/g, '$1');
		if (canonicalUrl !== window.location.href) {
			window.location.href = canonicalUrl;
		}

		if (this.browserDetectionService.isSafari()) {
			$(this.elementRef.nativeElement).addClass('browser-safari');
		}

		this.busyService.isBusy().subscribe(busy => {
			this.busy = busy;

			// https://github.com/angular/angular/issues/17572#issuecomment-323465737
			this.changeDetectorRef.detectChanges();
		});
	}
}


