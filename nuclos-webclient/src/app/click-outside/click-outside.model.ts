import { ElementRef } from '@angular/core';
import { Subject } from 'rxjs';
import { EvaluateClickOutside } from './evaluate-click-outside.model';

export class OutsideClick {
	element: ElementRef;
	subject: Subject<any>;
	clickHandler: EvaluateClickOutside;
}
