/* tslint:disable:no-unused-variable */
import { inject, TestBed } from '@angular/core/testing';
import { ClickOutsideService } from './click-outside.service';

describe('Service: ClickOutside', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [ClickOutsideService]
		});
	});

	it('should ...', inject([ClickOutsideService], (service: ClickOutsideService) => {
		expect(service).toBeTruthy();
	}));
});
