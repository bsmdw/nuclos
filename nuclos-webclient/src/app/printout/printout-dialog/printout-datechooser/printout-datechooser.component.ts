import { Component, Injector, Input, ViewChild } from '@angular/core';
import { NgbInputDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { DatetimeService } from '../../../shared/datetime.service';

@Component({
	selector: 'nuc-printout-datechooser',
	templateUrl: './printout-datechooser.component.html'
})
export class PrintoutDatechooserComponent {
	@ViewChild('datepicker') datepicker: NgbInputDatepicker;
	@Input() param: { value: string, parameter: any, model?: any };

	datePattern: string;

	constructor(
		injector: Injector,
		private datetimeService: DatetimeService,
	) {
	}

	ngOnInit() {
		this.datePattern = this.datetimeService.getDatePattern();
	}



	toggleDatepicker() {
		this.datepicker.toggle();
	}

	setDateValue(date: { year: number, month: number, day: number }) {
		this.param.value = moment(
			{
				year: date.year,
				month: date.month - 1,
				day: date.day
			}
		).format('DD.MM.YYYY');
	}
}
