import { inject, TestBed } from '@angular/core/testing';
import { CommandService } from './command.service';

xdescribe('CommandService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [CommandService]
		});
	});

	it('should ...', inject([CommandService], (service: CommandService) => {
		expect(service).toBeTruthy();
	}));
});
