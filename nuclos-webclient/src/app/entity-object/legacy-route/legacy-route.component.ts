import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EntityObjectRouteParams } from '../entity-object-route-params';

@Component({
	selector: 'nuc-legacy-route',
	templateUrl: './legacy-route.component.html',
	styleUrls: ['./legacy-route.component.css']
})
export class LegacyRouteComponent implements OnInit {

	constructor(
		private route: ActivatedRoute,
		private router: Router
	) {
	}

	ngOnInit() {
		this.route.params.subscribe((params: EntityObjectRouteParams) => {
			if (params.entityObjectId) {
				this.router.navigate(['/view', params.entityClassId, params.entityObjectId]);
			} else if (params.search) {
				this.router.navigate(['/view', params.entityClassId, 'search', params.entityObjectId]);
			} else {
				this.router.navigate(['/view', params.entityClassId]);
			}
		});
	}

}
