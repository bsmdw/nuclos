import { Component, OnInit } from '@angular/core';
import { EntityObjectResultUpdateService } from '../../entity-object-data/shared/entity-object-result-update.service';
import { EntityObjectResultService } from '../../entity-object-data/shared/entity-object-result.service';
import { SubEntityObject } from '../../entity-object-data/shared/entity-object.class';
import { EntityObjectService } from '../../entity-object-data/shared/entity-object.service';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { Logger } from '../../log/shared/logger';
import { EntityObjectGridMultiSelectionResult } from '../entity-object-grid/entity-object-grid-multi-selection-result';
import { GridOptions, RowNode, ColDef } from 'ag-grid';
import { MultiSelectionEditorComponent } from '../entity-object-grid/multi-selection-editor/multi-selection-editor.component';
import { MultiSelectionHeaderComponent } from '../entity-object-grid/multi-selection-header/multi-selection-header.component';
import { MultiSelectionRendererComponent } from '../entity-object-grid/multi-selection-renderer/multi-selection-renderer.component';

@Component({
	selector: 'nuc-collective-processing-progress',
	templateUrl: 'collective-processing-progress.component.html',
	styleUrls: ['collective-processing-progress.component.scss']
})
export class CollectiveProcessingProgressComponent implements OnInit {

	gridOptions: GridOptions = <GridOptions>{};

	columnDefs = [
		{headerName: '#', field: 'rowNumber' },
		{headerName: 'Objekt', field: 'object' },
		{headerName: 'Ergebnis', field: 'result' },
	];

	rowData = [ {} ];

	constructor(
		protected i18n: NuclosI18nService,
		private eoResultUpdateService: EntityObjectResultUpdateService,
		private $log: Logger,
	) {
	}

	ngOnInit() {
		this.eoResultUpdateService.subscribeCollectiveProcessingProgress().subscribe(info => {
				if (info.objectInfos) {
					info.objectInfos.forEach(oInfo => {
						let progressRow = {
							rowNumber: oInfo.infoRowNumber,
							object: oInfo.name,
							result: oInfo.result
						};
						this.rowData[oInfo.infoRowNumber - 1] = progressRow;
					});
					if (this.gridOptions && this.gridOptions.api) {
						this.gridOptions.api.setRowData(this.rowData);
					}
				}
			}
		)
	}



}
