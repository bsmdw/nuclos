import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntityObjectGridComponent } from './entity-object-grid.component';

xdescribe('EntityObjectGridComponent', () => {
	let component: EntityObjectGridComponent;
	let fixture: ComponentFixture<EntityObjectGridComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [EntityObjectGridComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(EntityObjectGridComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
