import { inject, TestBed } from '@angular/core/testing';

import { EntityObjectGridService } from './entity-object-grid.service';

xdescribe('EntityObjectGridService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [EntityObjectGridService]
		});
	});

	it('should be created', inject([EntityObjectGridService], (service: EntityObjectGridService) => {
		expect(service).toBeTruthy();
	}));
});
