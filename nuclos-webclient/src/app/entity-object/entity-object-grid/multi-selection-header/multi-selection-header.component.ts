import { Component } from '@angular/core';
import { IAfterGuiAttachedParams, IHeaderParams } from 'ag-grid';
import { IHeaderAngularComp } from 'ag-grid-angular';
import { Subject } from 'rxjs';

@Component({
	selector: 'nuc-multi-selection-header',
	templateUrl: './multi-selection-header.component.html',
	styleUrls: ['./multi-selection-header.component.css']
})
export class MultiSelectionHeaderComponent implements IHeaderAngularComp {

	protected params: IHeaderParams;

	private headerMultiSelectionChangeObserver: Subject<boolean>;

	private context: any;

	constructor(
	) {
	}

	agInit(params: IHeaderParams): void {
		this.params = params;
		this.context = this.params.context;
		this.setValue(this.params.context.headerMultiSelectionAll === true);
		this.headerMultiSelectionChangeObserver = params.context.headerMultiSelectionChangeObserver;
	}

	toggleValue() {
		this.setValue(!this.getValue(), true);
	}

	getValue(): any {
		return this.params.context.headerMultiSelectionAll === true;
	}

	setValue(value: any, notifyObserver?: boolean) {
		if (this.context.multiSelectionDisabled === true) {
			return;
		}
		this.params.context.headerMultiSelectionAll = value === true;
		if (notifyObserver === true) {
			this.headerMultiSelectionChangeObserver.next(this.getValue());
		}
	}

}
