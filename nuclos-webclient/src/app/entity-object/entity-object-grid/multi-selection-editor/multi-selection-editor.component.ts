import { Component, ElementRef, HostListener, ViewChild } from '@angular/core';
import { getValue } from '@angular/core/src/render3/styling/class_and_style_bindings';
import { IEntityObject } from '@nuclos/nuclos-addon-api';
import { AgEditorComponent } from 'ag-grid-angular/src/interfaces';
import { Subject } from 'rxjs';
import { EntityObjectEventService } from '../../../entity-object-data/shared/entity-object-event.service';
import { IAfterGuiAttachedParams, ICellEditorParams } from 'ag-grid';

@Component({
	selector: 'nuc-multi-selection-editor',
	templateUrl: './multi-selection-editor.component.html',
	styleUrls: ['./multi-selection-editor.component.css']
})
export class MultiSelectionEditorComponent implements AgEditorComponent {

	protected params: ICellEditorParams;

	private rowMultiSelectionChangeObserver: Subject<any>;

	private context: any;

	constructor(
		protected ref: ElementRef
	) {
	}

	agInit(params: ICellEditorParams): void {
		this.params = params;
		this.context = this.params.context;
		this.rowMultiSelectionChangeObserver = this.params.context.rowMultiSelectionChangeObserver;
	}

	@HostListener('window:keydown', ['$event'])
	handleKeyboardEvent(event: KeyboardEvent) {
		if (event.code === 'Space' || event.code === 'Enter') {
			// this.toggleValue();
		}
		if (event.code === 'Tab') {
			if (event.shiftKey) {
				// this.getGridApi().tabToPreviousCell();
			} else {
				// this.getGridApi().tabToNextCell();
			}
		}
	}

	toggleValue() {
		this.setValue(!this.getValue());
	}

	isPopup(): boolean {
		return false;
	}

	afterGuiAttached(params?: IAfterGuiAttachedParams): void {
		this.toggleValue();
	}

	focusIn(): void {
	}

	focusOut(): void {
	}

	getValue(): any {
		if (this.params.node && this.params.node.data) {
			if (this.params.context.headerMultiSelectionAll === true) {
				return !(this.params.node.data.unselected === true);
			} else {
				return this.params.node.data.selected === true;
			}
		}
		return false;
	}

	setValue(value) {
		if (this.context.multiSelectionDisabled === true) {
			return;
		}
		if (this.params.node && this.params.node.data) {
		if (this.params.node.data.id == null) {
			return;
		}
		if (this.params.node) {
			if (value !== this.getValue()) {
				if (this.params.context.headerMultiSelectionAll === true) {
					this.params.node.data.selected = undefined;
					this.params.node.data.unselected = value === false;
				} else {
					this.params.node.data.selected = value === true;
					this.params.node.data.unselected = undefined;
				}
			}
			this.rowMultiSelectionChangeObserver.next(this.params.node);
		}
		}
	}

	isCancelAfterEnd(): boolean {
		return false;
	}

	isCancelBeforeStart(): boolean {
		return false;
	}
}
