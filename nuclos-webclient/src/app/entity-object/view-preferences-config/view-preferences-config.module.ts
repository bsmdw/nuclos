import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { I18nModule } from '../../i18n/i18n.module';
import {
	PreferenceDialogState,
	ViewPreferencesModalComponent,
	ViewPreferencesModalContainerComponent,
	ViewPreferencesModalDirective
} from './view-preferences-config.component';
import { TwoSideMultiSelectModule } from '../../two-side-multi-select/two-side-multi-select.module';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		I18nModule,
		TwoSideMultiSelectModule
	],
	declarations: [
		ViewPreferencesModalDirective,
		ViewPreferencesModalContainerComponent,
		ViewPreferencesModalComponent,
	],
	providers: [
		PreferenceDialogState
	],
	exports: [
		ViewPreferencesModalContainerComponent,
		ViewPreferencesModalComponent
	],
	entryComponents: [
		ViewPreferencesModalComponent
	]
})
export class ViewPreferencesConfigModule {
}
