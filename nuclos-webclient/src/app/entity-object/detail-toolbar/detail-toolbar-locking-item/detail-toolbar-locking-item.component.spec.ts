import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailToolbarLockingItemComponent } from './detail-toolbar-locking-item.component';

xdescribe('DetailToolbarLockingItemComponent', () => {
	let component: DetailToolbarLockingItemComponent;
	let fixture: ComponentFixture<DetailToolbarLockingItemComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [DetailToolbarLockingItemComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(DetailToolbarLockingItemComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
