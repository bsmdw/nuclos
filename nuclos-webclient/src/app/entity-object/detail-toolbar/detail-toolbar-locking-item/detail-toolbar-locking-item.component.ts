import { Component, Input, OnInit } from '@angular/core';
import { EntityObject } from '../../../entity-object-data/shared/entity-object.class';

@Component({
	selector: 'nuc-detail-toolbar-locking-item',
	templateUrl: './detail-toolbar-locking-item.component.html',
	styleUrls: ['./detail-toolbar-locking-item.component.css']
})
export class DetailToolbarLockingItemComponent implements OnInit {
	@Input() eo: EntityObject;

	constructor() {
	}

	ngOnInit() {
	}

	isVisible(): boolean {
		return this.isLocked() || this.canLock();
	}

	private isLocked(): boolean {
		if (!this.eo) {
			return false;
		}

		return this.eo.isLocked();
	}

	private canLock(): boolean {
		if (!this.eo) {
			return false;
		}

		let links = this.eo.getLinks();
		return !!(links && links.lock);
	}
}
