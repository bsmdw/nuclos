import { TestBed, inject } from '@angular/core/testing';

import { DetailModalService } from './detail-modal.service';

xdescribe('DetailModalServiceService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [DetailModalService]
		});
	});

	it('should ...', inject([DetailModalService], (service: DetailModalService) => {
		expect(service).toBeTruthy();
	}));
});
