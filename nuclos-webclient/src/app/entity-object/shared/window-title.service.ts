import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root',
})
export class WindowTitleService {

	constructor(
		@Inject(DOCUMENT) private document
	) {
	}

	setDefaultWindowTitle() {
		this.setWindowTitle('Nuclos');
	}

	setWindowTitle(title: string) {
		this.document.title = title;
	}
}
