import { Component, Input, OnInit } from '@angular/core';
import { EntityMeta } from '../../../../entity-object-data/shared/bo-view.model';
import { DataService } from '../../../../entity-object-data/shared/data.service';
import { ExportBoListTemplateState } from '../statusbar.component';

@Component({
	selector: 'nuc-export-bo-list',
	templateUrl: './export-bo-list.component.html',
	styleUrls: ['./export-bo-list.component.css']
})
export class ExportBoListComponent implements OnInit {

	format: string;
	pageOrientationLandscape = false;
	isColumnScaled = false;
	exportLink: string | undefined;

	@Input()
	meta: EntityMeta;


	constructor(
		private state: ExportBoListTemplateState,
		private dataService: DataService
	) {
	}

	ngOnInit() {
	}

	executeListExport() {
		this.dataService.exportBoList(
			this.state.boId,
			this.state.refAttrId,
			this.state.meta ? this.state.meta : this.meta,
			this.state.columnPrefrenrence,
			this.state.searchtemplatePreference,
			this.format,
			this.pageOrientationLandscape,
			this.isColumnScaled
		).subscribe(
			exportLink => {
				this.exportLink = exportLink;
				window.location.href = exportLink;
			}
		);
	}

	close() {
		this.state.modalRef.close('confirmed');
	}

}
