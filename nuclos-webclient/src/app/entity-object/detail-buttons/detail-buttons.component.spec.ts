/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DetailButtonsComponent } from './detail-buttons.component';

xdescribe('DetailButtonsComponent', () => {
	let component: DetailButtonsComponent;
	let fixture: ComponentFixture<DetailButtonsComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [DetailButtonsComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(DetailButtonsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
