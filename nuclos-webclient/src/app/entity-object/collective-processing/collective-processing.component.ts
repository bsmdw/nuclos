import { Component, OnInit } from '@angular/core';
import { EntityObjectResultUpdateService } from '../../entity-object-data/shared/entity-object-result-update.service';
import { EntityObjectResultService } from '../../entity-object-data/shared/entity-object-result.service';
import { EntityObjectService } from '../../entity-object-data/shared/entity-object.service';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { Logger } from '../../log/shared/logger';
import { EntityObjectGridMultiSelectionResult } from '../entity-object-grid/entity-object-grid-multi-selection-result';

@Component({
	selector: 'nuc-collective-processing',
	templateUrl: 'collective-processing.component.html',
	styleUrls: ['collective-processing.component.scss']
})
export class CollectiveProcessingComponent implements OnInit {

	multiSelectionResult: EntityObjectGridMultiSelectionResult;

	selectionOptions: CollectiveProcessingSelectionOptions;

	constructor(
		protected i18n: NuclosI18nService,
		private eoService: EntityObjectService,
		private eoResultService: EntityObjectResultService,
		private eoResultUpdateService: EntityObjectResultUpdateService,
		private $log: Logger,
	) {
	}

	ngOnInit() {
		this.setMultiSelectionResult(this.eoResultService.getMultiSelectionResult());
		this.eoResultService.observeMultiSelectionChange().subscribe(
			multiSelectionResult => {
				this.setMultiSelectionResult(multiSelectionResult);
			}
		);
	}

	private setMultiSelectionResult(multiSelectionResult: EntityObjectGridMultiSelectionResult) {
		this.multiSelectionResult = multiSelectionResult;
		if (multiSelectionResult.headerMultiSelectionAll || multiSelectionResult.hasIds()) {
			this.loadSelectionOptions(multiSelectionResult);
		}
	}

	private loadSelectionOptions(multiSelectionResult: EntityObjectGridMultiSelectionResult) {
		this.eoResultUpdateService.loadCollectiveProcessingSelectionOptions(multiSelectionResult).subscribe(
			selOptions => {
				this.selectionOptions = selOptions;
				// this.$log.warn('selOptions received: %o', selOptions.stateChanges);
			});
	}

	executeStateChange(action: CollectiveProcessingAction) {
		this.eoResultUpdateService.executeCollectiveProcessing(this.multiSelectionResult, action.links.execute.href!).subscribe();
	}

	showProgress() {
		return this.eoResultService.forceCollectiveProcessingView;
	}

	closeProgress() {
		this.eoResultService.forceCollectiveProcessingView = false;
		this.backToDetails();
	}

	backToDetails() {
		this.eoResultService.showCollectiveProcessing = false;
	}

}
