import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatechooserComponent } from './datechooser.component';

xdescribe('DatechooserComponent', () => {
	let component: DatechooserComponent;
	let fixture: ComponentFixture<DatechooserComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [DatechooserComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(DatechooserComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
