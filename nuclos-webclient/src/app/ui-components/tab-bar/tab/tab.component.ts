import { Component, ContentChild, ElementRef, Input, OnInit, QueryList, TemplateRef } from '@angular/core';
import { TabTitleDirective } from '../tab-title.directive';
import { TabContentDirective } from '../tab-content.directive';

@Component({
	selector: 'nuc-tab',
	templateUrl: './tab.component.html',
	styleUrls: ['./tab.component.css']
})
export class TabComponent implements OnInit {

	@Input() id: any;
	@Input() value: any;
	@Input() index: any;
	@Input() title: string;

	@ContentChild(TabTitleDirective) titleTemplate: TabTitleDirective;
	@ContentChild(TabContentDirective) contentTemplate: TabContentDirective;

	constructor() {
	}

	ngOnInit() {
	}

}
