import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CssFromSystemparameterComponent } from './css-from-systemparameter.component';

xdescribe('CssFromSystemparameterComponent', () => {
	let component: CssFromSystemparameterComponent;
	let fixture: ComponentFixture<CssFromSystemparameterComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [CssFromSystemparameterComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(CssFromSystemparameterComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
