#!/usr/bin/env node

/**
 * prints the command to install addon dependencies
 *
 * this is useful when using the addon development mode
 *
 * usage: $(./install-addon-dependencies.js)
 */

const fs = require('fs');
const path = require('path');
const child_process = require('child_process');

const isDirectory = source => fs.lstatSync(source).isDirectory();
const getDirectories = source => fs.existsSync(source)
	? fs.readdirSync(source)
		// Must not be a hidden file which typically start with a dot, e.g. .git
		.filter(name => !name.startsWith('.'))
		.map(name => path.join(source, name))
		.filter(isDirectory)
		.filter(dir => fs.existsSync(dir + '/package.json'))
	: [];

let dependencies = new Set();
[
	...getDirectories('addons'),
	...getDirectories('src/addons') // addon-dev-mode
]
	.forEach(dir => {
	const content = fs.readFileSync(dir + '/package.json');
	const jsonContent = JSON.parse(content);
	for (const key in jsonContent.dependencies) {
		const value = jsonContent.dependencies[key];
		if (key.indexOf('@angular') === -1) {
			dependencies.add(key + '@' + value);
		}
	}
});
let installDependenciesCommand = 'npm install --save-exact ' + Array.from(dependencies.values()).join(' ');
console.log('Installing: ', installDependenciesCommand);
child_process.execSync(installDependenciesCommand);
