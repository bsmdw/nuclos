let fs = require('fs');
let hostport = process.env.HOST_PORT || 8080;
let conf = {
	"nuclosURL": "http://<host>:" + hostport + "/nuclos-war"
};
fs.writeFile(
	'src/assets/config.json',
	JSON.stringify(conf),
	error => {console.error(error);}
);
