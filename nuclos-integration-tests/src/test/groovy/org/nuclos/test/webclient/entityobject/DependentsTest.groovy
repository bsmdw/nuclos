package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Sidebar

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class DependentsTest extends AbstractWebclientTest {
	Map<String, ?> multirefBo = [name: 'Test Multiref']


	@Test()
	void _01_createBo() {
		EntityObjectComponent eo = EntityObjectComponent.createEO(TestEntities.NUCLET_TEST_RULES_TESTMULTIREF, multirefBo)

		assert eo.getAttribute('countmultirefa') == '0'
		assert eo.getAttribute('countmultirefb') == '0'

		assert eo.getAttribute('countmultirefanew') == '0'
		assert eo.getAttribute('countmultirefbnew') == '0'

		assert eo.getAttribute('countmultirefaupdate') == '0'
		assert eo.getAttribute('countmultirefbupdate') == '0'

		assert eo.getAttribute('countmultirefadelete') == '0'
		assert eo.getAttribute('countmultirefbdelete') == '0'

		assert eo.getAttribute('countmultirefanone') == '0'
		assert eo.getAttribute('countmultirefbnone') == '0'
	}

	@Test
	void _02_insertDependentA() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.clickButton('Insert dependent A')

		assert eo.getAttribute('countmultirefa') == '1'
		assert eo.getAttribute('countmultirefb') == '0'

		assert eo.getAttribute('countmultirefanew') == '1'
		assert eo.getAttribute('countmultirefbnew') == '0'

		assert eo.getAttribute('countmultirefaupdate') == '0'
		assert eo.getAttribute('countmultirefbupdate') == '0'

		assert eo.getAttribute('countmultirefadelete') == '0'
		assert eo.getAttribute('countmultirefbdelete') == '0'

		assert eo.getAttribute('countmultirefanone') == '0'
		assert eo.getAttribute('countmultirefbnone') == '0'

		eo.clickButton('Insert dependent A')

		assert eo.getAttribute('countmultirefa') == '2'
		assert eo.getAttribute('countmultirefb') == '0'

		assert eo.getAttribute('countmultirefanew') == '1'
		assert eo.getAttribute('countmultirefbnew') == '0'

		assert eo.getAttribute('countmultirefaupdate') == '0'
		assert eo.getAttribute('countmultirefbupdate') == '0'

		assert eo.getAttribute('countmultirefadelete') == '0'
		assert eo.getAttribute('countmultirefbdelete') == '0'

		assert eo.getAttribute('countmultirefanone') == '1'
		assert eo.getAttribute('countmultirefbnone') == '0'
	}

	@Test
	void _03_insertDependentB() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.clickButton('Insert dependent B')

		assert eo.getAttribute('countmultirefa') == '2'
		assert eo.getAttribute('countmultirefb') == '1'

		assert eo.getAttribute('countmultirefanew') == '0'
		assert eo.getAttribute('countmultirefbnew') == '1'

		assert eo.getAttribute('countmultirefaupdate') == '0'
		assert eo.getAttribute('countmultirefbupdate') == '0'

		assert eo.getAttribute('countmultirefadelete') == '0'
		assert eo.getAttribute('countmultirefbdelete') == '0'

		assert eo.getAttribute('countmultirefanone') == '2'
		assert eo.getAttribute('countmultirefbnone') == '0'
	}

	@Test
	void _04_updateDependentA() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.clickButton('Update first dependent A')

		assert eo.getAttribute('countmultirefa') == '2'
		assert eo.getAttribute('countmultirefb') == '1'

		assert eo.getAttribute('countmultirefanew') == '0'
		assert eo.getAttribute('countmultirefbnew') == '0'

		assert eo.getAttribute('countmultirefaupdate') == '1'
		assert eo.getAttribute('countmultirefbupdate') == '0'

		assert eo.getAttribute('countmultirefadelete') == '0'
		assert eo.getAttribute('countmultirefbdelete') == '0'

		assert eo.getAttribute('countmultirefanone') == '1'
		assert eo.getAttribute('countmultirefbnone') == '1'
	}

	@Test
	void _05_updateDependentB() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.clickButton('Update first dependent B')

		assert eo.getAttribute('countmultirefa') == '2'
		assert eo.getAttribute('countmultirefb') == '1'

		assert eo.getAttribute('countmultirefanew') == '0'
		assert eo.getAttribute('countmultirefbnew') == '0'

		assert eo.getAttribute('countmultirefaupdate') == '0'
		assert eo.getAttribute('countmultirefbupdate') == '1'

		assert eo.getAttribute('countmultirefadelete') == '0'
		assert eo.getAttribute('countmultirefbdelete') == '0'

		assert eo.getAttribute('countmultirefanone') == '2'
		assert eo.getAttribute('countmultirefbnone') == '0'
	}

	@Test
	void _06_deleteDependentA() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.clickButton('Delete first dependent A')

		assert eo.getAttribute('countmultirefa') == '1'
		assert eo.getAttribute('countmultirefb') == '1'

		assert eo.getAttribute('countmultirefanew') == '0'
		assert eo.getAttribute('countmultirefbnew') == '0'

		assert eo.getAttribute('countmultirefaupdate') == '0'
		assert eo.getAttribute('countmultirefbupdate') == '0'

		assert eo.getAttribute('countmultirefadelete') == '1'
		assert eo.getAttribute('countmultirefbdelete') == '0'

		assert eo.getAttribute('countmultirefanone') == '1'
		assert eo.getAttribute('countmultirefbnone') == '1'

		eo.clickButton('Delete first dependent A')

		assert eo.getAttribute('countmultirefa') == '0'
		assert eo.getAttribute('countmultirefb') == '1'

		assert eo.getAttribute('countmultirefanew') == '0'
		assert eo.getAttribute('countmultirefbnew') == '0'

		assert eo.getAttribute('countmultirefaupdate') == '0'
		assert eo.getAttribute('countmultirefbupdate') == '0'

		assert eo.getAttribute('countmultirefadelete') == '1'
		assert eo.getAttribute('countmultirefbdelete') == '0'

		assert eo.getAttribute('countmultirefanone') == '0'
		assert eo.getAttribute('countmultirefbnone') == '1'
	}

	@Test
	void _06_deleteDependentB() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.clickButton('Delete first dependent B')

		assert eo.getAttribute('countmultirefa') == '0'
		assert eo.getAttribute('countmultirefb') == '0'

		assert eo.getAttribute('countmultirefanew') == '0'
		assert eo.getAttribute('countmultirefbnew') == '0'

		assert eo.getAttribute('countmultirefaupdate') == '0'
		assert eo.getAttribute('countmultirefbupdate') == '0'

		assert eo.getAttribute('countmultirefadelete') == '0'
		assert eo.getAttribute('countmultirefbdelete') == '1'

		assert eo.getAttribute('countmultirefanone') == '0'
		assert eo.getAttribute('countmultirefbnone') == '0'
	}

	@Test
	void _07_createDependentsDirectly() {
		Sidebar.refresh()

		EntityObjectComponent.createEO(TestEntities.NUCLET_TEST_RULES_TESTMULTIREFDEPENDENT, [name: 'Dependent A 1', multirefa: multirefBo.name])
		EntityObjectComponent.createEO(TestEntities.NUCLET_TEST_RULES_TESTMULTIREFDEPENDENT, [name: 'Dependent A 2', multirefa: multirefBo.name])
		EntityObjectComponent.createEO(TestEntities.NUCLET_TEST_RULES_TESTMULTIREFDEPENDENT, [name: 'Dependent B 1', multirefb: multirefBo.name])
		EntityObjectComponent.createEO(TestEntities.NUCLET_TEST_RULES_TESTMULTIREFDEPENDENT, [name: 'Dependent A+B 1', multirefa: multirefBo.name, multirefb: multirefBo.name])

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_RULES_TESTMULTIREF)
		eo.clickButton('Update Counts')

		assert eo.getAttribute('countmultirefa') == '3'
		assert eo.getAttribute('countmultirefb') == '2'

		assert eo.getAttribute('countmultirefanew') == '0'
		assert eo.getAttribute('countmultirefbnew') == '0'

		assert eo.getAttribute('countmultirefaupdate') == '0'
		assert eo.getAttribute('countmultirefbupdate') == '0'

		assert eo.getAttribute('countmultirefadelete') == '0'
		assert eo.getAttribute('countmultirefbdelete') == '0'

		assert eo.getAttribute('countmultirefanone') == '3'
		assert eo.getAttribute('countmultirefbnone') == '2'
	}
}
