package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class APITest extends AbstractWebclientTest {

	@Test()
	void _00_createEo() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_RULES_TESTAPI)

		eo.addNew()
		eo.setAttribute('test', 'test123')

		assert eo.getAttribute('gettersettermethods') == ''

		eo.save()

		assert eo.getAttribute('test') == 'test123'
		assert eo.getAttribute('123') == ''
		assert eo.getAttribute('_a') == ''
		assert eo.getAttribute('x') == ''
		assert eo.getAttribute('ref_user') == ''
		assert eo.getAttribute('under_score') == ''

		assert eo.getAttribute('gettersettermethods') == ''

		eo.clickButton('Run test rule')

		waitForAngularRequestsToFinish()

		assert eo.getAttribute('test') == 'test'
		assert eo.getAttribute('123') == '123'
		assert eo.getAttribute('_a') == 'a'
		assert eo.getAttribute('x') == 'x'
		assert eo.getAttribute('ref_user') == 'nuclos'
		assert eo.getAttribute('under_score') == 'under_score'

		assert eo.getAttribute('gettersettermethods') == 'ok'
		assert eo.getAttribute('foreignkeyattribute') == 'ok'

		assert eo.getLabelText('123') == '123'
		assert eo.getLabelText('_a') == '_a'
		assert eo.getLabelText('under_score') == 'under_score'
	}
}
