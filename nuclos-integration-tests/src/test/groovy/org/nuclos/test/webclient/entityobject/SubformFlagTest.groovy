package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.subform.Subform

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SubformFlagTest extends AbstractWebclientTest {

	private static final String TEXT = 'text'

	private static final String SUBFORM_INSERTS = 'subforminserts'
	private static final String SUBFORM_UPDATES = 'subformupdates'
	private static final String SUBFORM_DELETES = 'subformdeletes'
	private static final String SUBFORM_NONE = 'subformnone'
	private static final String SUBFORM_TOTAL = 'subformtotal'

	@Test
	void _05_insertEoAndSubform() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_SUBFORM_PARENT)

		eo.addNew()
		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent')

		insertEoAndSubform(eo, subform)
		updateEo(eo, subform)
		updateSubform(eo, subform)
		updateSubformAndDiscard(eo, subform)
		deleteSubform(eo, subform)
	}

	private void insertEoAndSubform(EntityObjectComponent eo, Subform subform) {
		eo.setAttribute(TEXT, System.currentTimeMillis())

		subform.newRow().enterValue(TEXT, 'insert')
		subform.newRow().enterValue(TEXT, 'insert2')
		subform.newRow().enterValue(TEXT, 'insert3')

		eo.save()

		assert eo.getAttribute(SUBFORM_INSERTS) == '3'
		assert eo.getAttribute(SUBFORM_UPDATES) == '0'
		assert eo.getAttribute(SUBFORM_DELETES) == '0'
		assert eo.getAttribute(SUBFORM_NONE) == '0'
		assert eo.getAttribute(SUBFORM_TOTAL) == '3'
	}

	private void updateEo(EntityObjectComponent eo, Subform subform) {
		eo.setAttribute(TEXT, System.currentTimeMillis())
		eo.save()

		assert eo.getAttribute(SUBFORM_INSERTS) == '0'
		assert eo.getAttribute(SUBFORM_UPDATES) == '0'
		assert eo.getAttribute(SUBFORM_DELETES) == '0'
		assert eo.getAttribute(SUBFORM_NONE) == '3'
		assert eo.getAttribute(SUBFORM_TOTAL) == '3'
	}

	private void updateSubform(EntityObjectComponent eo, Subform subform) {
		subform.getRow(0).enterValue(TEXT, System.currentTimeMillis().toString())
		eo.save()

		assert eo.getAttribute(SUBFORM_INSERTS) == '0'
		assert eo.getAttribute(SUBFORM_UPDATES) == '1'
		assert eo.getAttribute(SUBFORM_DELETES) == '0'
		assert eo.getAttribute(SUBFORM_NONE) == '2'
		assert eo.getAttribute(SUBFORM_TOTAL) == '3'
	}

	private void updateSubformAndDiscard(EntityObjectComponent eo, Subform subform) {
		subform.getRow(0).enterValue(TEXT, System.currentTimeMillis().toString())

		eo.cancel()
		eo.setAttribute(TEXT, System.currentTimeMillis())
		eo.save()

		assert eo.getAttribute(SUBFORM_INSERTS) == '0'
		assert eo.getAttribute(SUBFORM_UPDATES) == '0'
		assert eo.getAttribute(SUBFORM_DELETES) == '0'
		assert eo.getAttribute(SUBFORM_NONE) == '3'
		assert eo.getAttribute(SUBFORM_TOTAL) == '3'
	}

	private void deleteSubform(EntityObjectComponent eo, Subform subform) {
		subform.getRow(0).selected = true
		subform.deleteSelectedRows()

		eo.save()

		assert eo.getAttribute(SUBFORM_INSERTS) == '0'
		assert eo.getAttribute(SUBFORM_UPDATES) == '0'
		assert eo.getAttribute(SUBFORM_DELETES) == '1'
		assert eo.getAttribute(SUBFORM_NONE) == '2'
		assert eo.getAttribute(SUBFORM_TOTAL) == '3'
	}
}
