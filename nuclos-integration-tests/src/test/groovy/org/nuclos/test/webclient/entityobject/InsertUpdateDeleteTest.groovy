package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.log.Log
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.*
import org.nuclos.test.webclient.pageobjects.subform.Subform

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class InsertUpdateDeleteTest extends AbstractWebclientTest {
	static String testEntryName = null

	@Test
	void _05_layoutUsage() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTUSAGE)

		eo.addNew()

		def isLayoutForNew = {
			$$('label').find{ it.text.contains('Layout for "new"') }
		}
		assert isLayoutForNew()

		eo.setAttribute('name', 'Test')
		eo.save()

		assert !isLayoutForNew()
	}

	@Test
	void _10_openEntity() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)
		assert $('#logout')
	}

	@Test
	void _20_createNewEntry() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Log.debug 'createNewEntry'
		int entryCountBefore = Sidebar.listEntryCount

		screenshot('createnewentry-a')
		eo.addNew()
		screenshot('createnewentry-b')

		// TODO fix sidebar view update after first
		// assert Sidebar.listEntryCount == entryCountBefore + 1

		// testEntryName = 'testentry '+formatDate(new Date());
		testEntryName = '42'

		eo.setAttribute('customerNumber', testEntryName)
		eo.setAttribute('name', testEntryName)

		assert eo.dirty

//		Sideview.fillRequiredFields(testEntryName)

		screenshot('createnewentry-c')
		eo.save()
		screenshot('createnewentry-d')

		assert !eo.dirty

		/* TODO fix handling of english dateformat
		// seconds are not shown in detail statusbar - so check if timestamp is max 1min before now
		def nowMillis = new Date().getTime()
		def changedAtMillis = eo.getChangedAt().getTime()
		assert changedAtMillis < nowMillis
		assert changedAtMillis > nowMillis - 60000

		def createdAtMillis = eo.getCreatedAt().getTime()
		assert createdAtMillis < nowMillis
		assert createdAtMillis > nowMillis - 60000
		*/

		assert eo.getChangedBy() == 'test'

		def entryCountAfter = Sidebar.listEntryCount
		assert entryCountAfter > entryCountBefore
	}

	@Test
	void _25_checkDynamicEntityColumns() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.selectTab('Dynamic Entity');
		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_MYORDERSANDCUSTOMERSDYN.fqn + '_INTIDTUDGENERICOBJECT')

		// Column is defined as "Changed By" - with a space
		assert subform.columnHeaders.contains('Changed By')

		assert subform.rowCount > 0

		// The attribute name is "ChangedBy" - without space
		assert subform.getRow(0).getValue('ChangedBy') == 'test'
	}

//	@Test
//	void _4findNewEntry() {
//		Log.debug 'findNewEntry'
//		screenshot('search-new-entry-start')
//		Searchtemplate.search(testEntryName)
//		screenshot('search-new-entry-end')
//
//		def foundEntries = $$('.sideview-list-entry')
//		assert foundEntries.size() == 1
//	}
//
//	@Test
//	void _5openSideviewAndFindNewEntry() {
//
//		Sideview.openSideview('example_rest_Customer')
//
//		screenshot('before-search')
//		Searchtemplate.search(testEntryName)
//
//		screenshot('search-entry-again')
//
//		def foundEntries = $$('.sideview-list-entry')
//		assert foundEntries.size() == 1
//
//		// open found entry again
//		$('.sideview-list-entry').click()
//	}

	@Test
	void _30_updateNewEntry() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Log.debug 'updateNewEntry'
		testEntryName = '43'

		assert !eo.dirty

		def inputs = $$('nuc-detail input[type="text"]')
		inputs*.clear()
		waitForAngularRequestsToFinish()

		eo.setAttribute('customerNumber', testEntryName)
		eo.setAttribute('name', testEntryName)

		assert eo.dirty
		assert Sidebar.isEntryMarkedDirty(0)

//		Sideview.fillRequiredFields(testEntryName)
		// TODO: Assert the EO is marked as dirty now and we cannot navigate away from it before we save or cancel

		screenshot('updatenewentry-beforesave')
		eo.save()
		screenshot('updatenewentry-aftersave')

		assert !eo.dirty

		assert eo.getAttribute('customerNumber') == testEntryName
		assert eo.getAttribute('name') == testEntryName

		// TODO: Assert the EO is saved and it is possible to navigate to another EO or to create a new one
	}

	@Test
	void _35_cancelUpdate() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		def oldEntryName = '43'
		testEntryName = '44'

		assert eo.getAttribute('customerNumber') == oldEntryName
		assert eo.getAttribute('name') == oldEntryName

		def inputs = $$('nuc-detail input[type="text"]')
		inputs*.clear()
		waitForAngularRequestsToFinish()

		eo.setAttribute('customerNumber', testEntryName)
		eo.setAttribute('name', testEntryName)

		// EO should be dirty anymore and the field values should be set to the new value
		assert eo.dirty
		assert eo.getAttribute('customerNumber') == testEntryName
		assert eo.getAttribute('name') == testEntryName

		eo.cancel()

		// EO should not be dirty anymore and the field values should be reset to the old value
		assert !eo.dirty
		assert eo.getAttribute('customerNumber') == oldEntryName
		assert eo.getAttribute('name') == oldEntryName
	}

	@Test
	void _37_cancelNew() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		def previousEntryCount = Sidebar.listEntryCount

		def previousEntryName = '43'

		eo.addNew()

		assert Sidebar.listEntryCount == previousEntryCount + 1

		assert eo.getAttribute('customerNumber') == ''
		assert eo.getAttribute('name') == ''

		eo.cancel()

		assert Sidebar.listEntryCount == previousEntryCount

		assert !eo.dirty

		assert eo.getAttribute('customerNumber') == previousEntryName
		assert eo.getAttribute('name') == previousEntryName
	}

	@Test
	void _38_preventNavigationFromDirty() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		2.times {
			eo.addNew()
			eo.setAttribute('customerNumber', 100 + it)
			eo.setAttribute('name', 100 + it)
			eo.save()
		}

		eo.addNew()
		eo.setAttribute('customerNumber', testEntryName)

		assert eo.dirty
		assert Sidebar.listEntryCount == 4
		// FIXME assert Sidebar.selectedIndex == 0

		Sidebar.selectEntry(1)
		checkPopover(true)

		MenuComponent.toggleUserMenu()
		MenuComponent.clickChangePasswordLink()
		checkPopover(true)

		Sidebar.selectEntry(1)
		checkPopover(true)

		eo.cancel()
		assert Sidebar.selectedIndex == 0
		assert !DetailButtonsComponent.popoverTitle
		assert !DetailButtonsComponent.popoverText

		// NUCLOS-7434 Popover should not re-appear after new record was cancelled
		Sidebar.selectEntry(2)
		checkPopover(false)
		assert Sidebar.selectedIndex == 2
	}

//	@Test
//	void _7findUpdatedEntry() {
//		Log.debug 'findUpdatedEntry'
//		screenshot('search-updated-entry-start')
//		Searchtemplate.search(testEntryName)
//		screenshot('search-updated-entry-end')
//
//		assert $$('.sideview-list-entry').size() == 1
//	}

	@Test
	void _40_staleVersionError() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		assert !StateComponent.currentState, 'This test must be run on an entity without state model'

		EntityObject entityObject = nuclosSession.getEntityObject(
				TestEntities.EXAMPLE_REST_CUSTOMER,
				eo.id
		)
		nuclosSession.save(entityObject)

		eo.setAttribute('customerNumber', 50)
		eo.save()

		assertStaleVersionErrorAndConfirm()
	}

	@Test
	void _50_deleteNewentry() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Log.debug 'deleteNewentry'

		screenshot('delete-eo-start')
		eo.delete()

		screenshot('delete-eo-end')

		assert Sidebar.listEntryCount == 2
	}

	@Test
	void _60_addNewWithSubformAndExistingRecords() {
		addRecord:
		{
			EntityObject eo = new EntityObject(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTRULES)
			eo.setAttribute('name', 'Pre-existing record')
			nuclosSession.save(eo)
		}

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTRULES)
		eo.addNew()
		eo.enterText('name', 'New record')
		Subform subform = eo.getSubform('nuclet_test_other_TestLayoutRulesSubform_testlayoutrules')
		subform.newRow()

		EntityObjectModal modal = EntityObjectComponent.forModal()
		modal.enterText('textvaluechanged', 'Test')
		EntityObjectComponent.clickButtonOk()

		eo.save()

		assert !eo.dirty
		assert !LoadingIndicatorComponent.loading
		assert Sidebar.listEntryCount == 2
	}

	private void assertStaleVersionErrorAndConfirm() {
		MessageModal modal = messageModal

		String message = modal.message
		assert message.contains('Version conflict') || message.contains('Versions-Konflikt')

		modal.confirm()
	}

	private void checkPopover(boolean shouldBeVisible) {
		assert (DetailButtonsComponent.popoverTitle == 'Unsaved changes' || DetailButtonsComponent.popoverTitle == 'Nicht gespeicherte Änderungen') == shouldBeVisible
		assert !!DetailButtonsComponent.popoverText == shouldBeVisible
	}
}
