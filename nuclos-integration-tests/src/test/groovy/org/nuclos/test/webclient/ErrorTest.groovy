package org.nuclos.test.webclient

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityClass
import org.nuclos.test.IntegrationTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class ErrorTest extends AbstractWebclientTest {

	@Test()
	void _10_test403() {
		getUrlHash('/businesstests')

		assertAccessDenied()

	}

	@Test()
	void _20_test404() {
		getUrlHash('/some_unknown_url_path')

		assertNotFound()
	}

	@Test()
	void _30_testEntityObject404() {
		EntityObjectComponent.open(new EntityClass<Object>() {
			@Override
			String getFqn() {
				return 'not_existing_Entity'
			}
		})

		assertNotFound()
	}
}
