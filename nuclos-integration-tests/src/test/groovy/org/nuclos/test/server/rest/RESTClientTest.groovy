package org.nuclos.test.server.rest

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class RESTClientTest extends AbstractNuclosTest {
	static RESTClient client = new RESTClient('nuclos', '')
	static EntityObject<Long> customer

	@Test
	void _00_login() {
		client.login()
		assert client.sessionId
	}

	@Test
	void _05_insertEo() {
		customer = new EntityObject(TestEntities.EXAMPLE_REST_CUSTOMER)

		customer.setAttribute('customerNumber', 283742983)
		customer.setAttribute('name', 'RESTClient test customer')

		assert !customer.id

		client.save(customer)

		assert customer.id
	}

	@Test
	void _10_readEo() {
		EntityObject<Long> savedCustomer = client.getEntityObject(TestEntities.EXAMPLE_REST_CUSTOMER, customer.id)
		assert savedCustomer.id
	}

	@Test
	void _15_updateEo() {
		String newName = 'RESTClient test customer updated'
		customer.setAttribute('name', newName)
		customer.save()

		EntityObject<Long> savedCustomer = client.getEntityObject(TestEntities.EXAMPLE_REST_CUSTOMER, customer.id)
		assert savedCustomer.getAttribute('name') == newName
	}

	@Test
	void _20_addDependents() {
		List<EntityObject<Long>> dependents = customer.getDependents(
				TestEntities.EXAMPLE_REST_CUSTOMERADDRESS,
				'customer'
		)

		3.times { i ->
			dependents << new EntityObject(TestEntities.EXAMPLE_REST_CUSTOMERADDRESS).with {
				it.setAttribute('street', 'Street ' + i)
				it.setAttribute('city', 'City ' + i)
				it.setAttribute('zipCode', 'Zip Code ' + i)
				return it
			}
		}

		customer.save()

		List<EntityObject<Long>> savedDependents = customer.loadDependents(
				TestEntities.EXAMPLE_REST_CUSTOMERADDRESS,
				'customer'
		)

		assert savedDependents*.getAttribute('street').sort() == ['Street 0', 'Street 1', 'Street 2']
	}

	@Test
	void _25_updateDependents() {
		List<EntityObject<Long>> dependents = customer.getDependents(
				TestEntities.EXAMPLE_REST_CUSTOMERADDRESS,
				'customer'
		)

		dependents.find {
			it.getAttribute('street') == 'Street 1'
		}.setAttribute('street', 'New Street')

		customer.save()

		List<EntityObject<Long>> savedDependents = customer.loadDependents(
				TestEntities.EXAMPLE_REST_CUSTOMERADDRESS,
				'customer'
		)

		assert savedDependents.size() == 3
		assert savedDependents.find {
			it.getAttribute('street') == 'New Street'
		}
	}

	@Test
	void _30_deleteDependents() {
		List<EntityObject<Long>> dependents = customer.getDependents(
				TestEntities.EXAMPLE_REST_CUSTOMERADDRESS,
				'customer'
		)

		dependents.find {
			it.getAttribute('street') == 'Street 0'
		}.flagDeleted()

		customer.save()

		List<EntityObject<Long>> savedDependents = customer.loadDependents(
				TestEntities.EXAMPLE_REST_CUSTOMERADDRESS,
				'customer'
		)

		assert savedDependents*.getAttribute('street').sort() == ['New Street', 'Street 2']
	}
}