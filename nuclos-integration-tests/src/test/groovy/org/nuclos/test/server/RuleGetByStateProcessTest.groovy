package org.nuclos.test.server

import javax.ws.rs.core.Response

import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.rules.ExpectedException
import org.junit.runners.MethodSorters
import org.nuclos.common.UID
import org.nuclos.test.*
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTException
import org.nuclos.test.rest.RESTHelper
import org.springframework.http.HttpMethod

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class RuleGetByStateProcessTest extends AbstractNuclosTest {

	@Rule
	public final ExpectedException exception = ExpectedException.none()

	static RESTClient client;
	final static String BASE_PATH = '/execute/nuclet.test.rules.GetByStateProcess/'

	@Test
	void _01_createTestUser() {
		RESTHelper.createUser('test', 'test', ['Example user'], nuclosSession)
		client = new RESTClient('test', 'test')
	}



	@Test
	void _05_getByState() {
		client.login()
		RESTHelper.requestString(BASE_PATH + 'getByState', HttpMethod.GET, client.sessionId)
	}

	@Test
	void _10_getByProcess() {
		RESTHelper.requestString(BASE_PATH + 'getByProcess', HttpMethod.GET, client.sessionId)
	}
}
