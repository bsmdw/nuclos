package org.nuclos.test.server

import javax.ws.rs.core.Response

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.webclient.LoginParams

import groovy.transform.CompileStatic
import net.lightbody.bmp.core.har.Har

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SecuritySessionTimeoutTest extends AbstractNuclosTest {
	static final int EXPIRES_AT_END_OF_BROWSER_SESSION = -1

	RESTClient client = new RESTClient('nuclos', '')

	@Test
	void _05_sessionTimeoutDefault() {
		expectSessionCookieLifetime(client, EXPIRES_AT_END_OF_BROWSER_SESSION)
		// TODO: Check the timeout of the HTTP session somehow (server side)

		client.logout()
	}

	@Test
	void _10_sessionTimeout() {
		nuclosSession.setSystemParameters([
				'SECURITY_SESSION_TIMEOUT_IN_SECONDS'         : '5'
		])

		expectSessionCookieLifetime(client, EXPIRES_AT_END_OF_BROWSER_SESSION)

		client.getEntityObjects(TestEntities.EXAMPLE_REST_ORDER)

		// Sleep until the session has timed out
		sleep 6000

		expectErrorStatus(Response.Status.UNAUTHORIZED) {
			client.getEntityObjects(TestEntities.EXAMPLE_REST_ORDER)
		}
	}

	@Test
	void _15_permanentLogin() {
		nuclosSession.setSystemParameters([
				'SECURITY_SESSION_TIMEOUT_IN_SECONDS'         : '1'
		])

		client = new RESTClient(new LoginParams(
				username: 'test',
				password: 'test',
				autologin: true
		))
		expectSessionCookieLifetime(client, 60 * 60 * 24 * 365)

		client.getEntityObjects(TestEntities.EXAMPLE_REST_ORDER)

		// Make sure the HTTP session really does not expire on server side
		sleep 3000

		client.getEntityObjects(TestEntities.EXAMPLE_REST_ORDER)

		client.logout()
	}

	/**
	 * Performs a login and asserts that the session cookie expiration is within
	 * 3 seconds of the expected lifetime.
	 *
	 * @param lifetimeInSeconds The expected lifetime in seconds, or -1 if the cookie is expected
	 *                          to last as long as the browser session.
	 */
	static void expectSessionCookieLifetime(RESTClient client, long lifetimeInSeconds) {
		client.getHar {
			client.login()
		}.with { Har har ->
			assert har.log.entries.size() == 1

			def cookies = har.log.entries.first().response.cookies
			assert !cookies.empty

			def sessionCookie = cookies.find { it.name == 'JSESSIONID' }

			if (lifetimeInSeconds == EXPIRES_AT_END_OF_BROWSER_SESSION) {
				// "expires" is null, if max-age is set to -1 via Set-Cookie header
				assert !sessionCookie.expires
			} else {
				def timeLeftInSeconds = (sessionCookie.expires.time - System.currentTimeMillis()) / 1000

				// Cookie expiration should be within 10 seconds of the expected lifetime
				assert timeLeftInSeconds >= lifetimeInSeconds - 10
				assert timeLeftInSeconds <= lifetimeInSeconds
			}
		}
	}
}