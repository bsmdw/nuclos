package org.nuclos.test.server

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class QueryProviderTest extends AbstractNuclosTest {

	@Test
	void _00_runTest() {
		EntityObject<Long> eo = new EntityObject(TestEntities.NUCLET_TEST_RULES_TESTQUERYPROVIDER)
		eo.setAttribute('name', 'Test')

		nuclosSession.save(eo)

		assert !eo.getAttribute('timestampsearch')
		assert !eo.getAttribute('datesearch')

		nuclosSession.executeCustomRule(eo, 'nuclet.test.rules.TestQueryProviderSearch')

		assert eo.getAttribute('timestampsearch') == 'ok'
		assert eo.getAttribute('datesearch') == 'ok'
	}

}
