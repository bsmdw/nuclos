package org.nuclos.test.server

import javax.ws.rs.core.Response

import org.apache.commons.io.IOUtils
import org.apache.http.client.methods.CloseableHttpResponse
import org.junit.AfterClass
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.IntegrationTest
import org.nuclos.test.rest.RESTHelper

import com.reprezen.kaizen.oasparser.OpenApiParser
import com.reprezen.kaizen.oasparser.model3.OpenApi3

import groovy.transform.CompileStatic
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.parser.OpenAPIV3Parser

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SwaggerTest extends AbstractNuclosTest {

	@AfterClass
	static void cleanup() {
		nuclosSession.setSystemParameters([:])
	}

	@Test
	void _00_readOpenApiWithoutLogin() {
		nuclosSession.setSystemParameters([:])

		OpenAPI api = new OpenAPIV3Parser().read(RESTHelper.REST_BASE_URL + '/openapi')

		expectPublicEndpoints(api)

		assert !api.paths.find {
			it.key == '/rest/execute/example.rest.CustomRestTestRule/getTest'
		}
	}

	@Test
	void _05_readOpenApiAsSuperuser() {
		nuclosSession.setSystemParameters([:])

		CloseableHttpResponse response = nuclosSession.getUrl(RESTHelper.REST_BASE_URL + '/openapi.yaml')
		String yaml = IOUtils.toString(response.getEntity().content)
		OpenAPI api = new OpenAPIV3Parser().readContents(yaml).openAPI

		expectPublicEndpoints(api)

		api.paths.find {
			it.key == '/rest/execute/example.rest.CustomRestTestRule/getTest'
		}.with {
			assert it.value.get.deprecated
			assert it.value.get.description == 'Test description'
//			assert it.value.get.extensions

			assert it.value.get.externalDocs
			assert it.value.get.externalDocs.description == 'Test External Documentation'
			assert it.value.get.externalDocs.url == 'http://www.nuclos.de'

			assert it.value.get.operationId == 'example.rest.CustomRestTestRule.getTest'

			assert it.value.get.parameters
			it.value.get.parameters.first().with { param ->
				assert param.name == 'Test Parameter 1'
				assert param.description == 'Test Parameter 1 description'
				assert param.required
				assert param.deprecated
				assert param.allowEmptyValue
				assert param.explode
//				assert param.allowReserved	// TODO: Is generated, but missing here
				assert param.example == 'Direct Example 1'
				assert param.in == 'query'

				assert param.examples.size() == 1
				param.examples.each { name, example ->
					assert example.description == 'Example 1'
					assert example.summary == 'Test Parameter 1 Example 1'
					assert example.value == 'Example Value 1'
					assert example.externalValue == 'External Value 1'
				}
			}

			assert it.value.get.requestBody
			it.value.get.requestBody.with { body ->
				assert body.content
				assert body.description == 'Request Body description'
				assert body.required

				assert body.content.containsKey('application/json')
				body.content.get('application/json').with { content ->
					// TODO
				}
			}

			assert it.value.get.responses
			it.value.get.responses.with {
				assert it.size() == 1
				assert it.containsKey('123')
				it.get('123').with {
					assert it.content
					it.content.with {
						assert it.containsKey('application/json')
						it.get('application/json').with {
							assert it.examples
							assert it.examples.containsKey('Test Example Object')
							it.examples.get('Test Example Object').with {
								assert it.summary == 'Test Example Object summary'
								assert it.value == 'Test value'
								assert it.externalValue == 'Test external value'
							}
						}
					}
					assert it.description == 'Test Response'
					assert it.headers

					assert it.links
					assert it.links.containsKey('Test Link')
					it.links.get('Test Link').with {
						assert it.operationId == 'example.rest.CustomRestTestRule.getTest'
						assert it.parameters.get('Test Parameter 1') == 'Test expression'
						assert it.requestBody == 'Test link request body'
						assert it.description == 'Test link description'
						it.server.with {
							assert it.url == 'http://www.nuclos.de'
							assert it.description == 'Nuclos Home'
						}
					}
				}
			}

			assert it.value.get.security
			it.value.get.security.with {
				assert it.size() == 1
				it.first().with { item ->
					assert item
					assert item.containsKey('Test Security Requirement')
					assert item.get('Test Security Requirement') == ['Scope 1', 'Scope 2']
				}
			}
			assert it.value.get.servers
			assert it.value.get.servers.size() == 1
			it.value.get.servers.first().with { server ->
				assert server.url == 'http://www.nuclos.de'
				assert server.description == 'Nuclos Homepage'
				assert server.variables.size() == 1
				server.variables.get('Test Server Variable').with { var ->
					assert var
					assert var.default == '1'
					assert var.enum == ['1', '2']
					assert var.description == 'Test Server Variable description'
				}
			}

			assert it.value.get.summary == 'Test summary'
			assert it.value.get.tags == ['test']
		}
	}

	@Test
	void _10_validate() {

		CloseableHttpResponse response = nuclosSession.getUrl(RESTHelper.REST_BASE_URL + '/openapi.yaml')
		String yaml = IOUtils.toString(response.getEntity().content)

		OpenApi3 api = (OpenApi3) new OpenApiParser().parse(yaml, new URL(RESTHelper.REST_BASE_URL))

		assert api.valid
		assert api.validationResults.items.empty
	}

	@Test
	void _90_deactivateSwagger() {
		nuclosSession.setSystemParameters(['SWAGGER_ACTIVE': 'false'])

		// All Swagger endpoints should return Status 204 - No Content now
		[
				RESTHelper.REST_BASE_URL + '/openapi',
				RESTHelper.REST_BASE_URL + '/openapi.yaml',
				RESTHelper.REST_BASE_URL + '/openapi.json',
		].each { url ->
			nuclosSession.getUrl(url).with {
				assert it.statusLine.statusCode == Response.Status.NO_CONTENT.statusCode
				assert !it.entity
			}
		}
	}

	private void expectPublicEndpoints(OpenAPI api) {
		assert api.paths.size() > 100
		assert api.paths.keySet().contains("/rest/login")

		api.paths.get("/rest/login").post.with {
			assert !it.security
			assert it.summary
		}
		api.paths.get("/rest/login").get.with {
			assert it.security
			assert it.summary
		}
		api.paths.get("/rest/login").delete.with {
			assert it.security
			assert it.summary
		}

		assert !api.paths.get("/rest/login").put
	}
}
