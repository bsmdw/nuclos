package org.nuclos.test.server

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SearchFilterTest extends AbstractNuclosTest {

	@Test
	void _00_setup() {
		def customer =
				[
						boMetaId: TestEntities.EXAMPLE_REST_CUSTOMER.fqn,
						attributes: [
								name: 'Test-Customer',
								customerNumber: 100,
								discount: 0.1,
								active: true
						]
				]
		customer = RESTHelper.createBo(customer, nuclosSession)
		def order =
				[
						boMetaId: TestEntities.EXAMPLE_REST_ORDER.fqn,
						attributes: [
								'orderNumber': 200,
								'customer': [id: customer.get('boId')],
								'orderDate': new Date(),// TODO
								'note': ''
						]
				]
		def orderPrio =
				[
						boMetaId: TestEntities.EXAMPLE_REST_ORDER.fqn,
						attributes: [
								'orderNumber': 201,
								'nuclosProcess': [id: 'example_rest_Order_Priorityorder'],
								'customer': [id: customer.get('boId')],
								'orderDate': new Date(),// TODO
								'note': ''
						]
				]
		RESTHelper.createBo(order, nuclosSession)
		RESTHelper.createBo(orderPrio, nuclosSession)
		RESTHelper.createUser('SearchFilterTest', 'SearchFilterTest', ['Example user'], nuclosSession)
	}

	@Test
	void _01_testResultWithSearchFilterByQueryContext() {
		RESTClient client = new RESTClient('SearchFilterTest', 'SearchFilterTest').login()
		def queryContext =
				[
						searchFilterId: 'ejivuTGFM12caaoGdFA1'
				]
		def result = RESTHelper.getBosByQueryContext(TestEntities.EXAMPLE_REST_ORDER.fqn, queryContext, client) as Map
		def bos = result.get('bos') as List
		assert bos.size() == 1
	}

	@Test
	void _02_testResultWithSearchFilterByParam() {
		RESTClient client = new RESTClient('SearchFilterTest', 'SearchFilterTest').login()
		def result = RESTHelper.getBos(TestEntities.EXAMPLE_REST_ORDER.fqn, client, 'ejivuTGFM12caaoGdFA1') as Map
		def bos = result.get('bos') as List
		assert bos.size() == 1
	}

	@Test
	void _03_testResultWithoutSearchFilter() {
		RESTClient client = new RESTClient('SearchFilterTest', 'SearchFilterTest').login()
		def result = RESTHelper.getBos(TestEntities.EXAMPLE_REST_ORDER.fqn, client) as Map
		def bos = result.get('bos') as List
		assert bos.size() == 2
	}

}
