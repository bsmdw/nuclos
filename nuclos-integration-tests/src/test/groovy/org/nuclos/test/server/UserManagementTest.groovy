package org.nuclos.test.server


import javax.ws.rs.core.Response

import org.apache.http.client.methods.HttpPost
import org.apache.http.client.methods.HttpRequestBase
import org.apache.http.entity.StringEntity
import org.json.JSONArray
import org.json.JSONObject
import org.junit.Test
import org.junit.FixMethodOrder
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.common.UID
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper
import org.springframework.http.HttpMethod

import groovy.transform.CompileStatic

/**
 * @author Oliver Brausch <oliver.brausch@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class UserManagementTest extends AbstractNuclosTest {
	static String testRoleName = 'TestRole'
	static RESTClient client2

	@Test
	void _01setup() {
		createOrDeleteTestUsers(false)
		assert createOrDeleteTestUsers(true) == 3

		deleteTestRoles()

		client2 = new RESTClient('TestUser2', 'TestUser2')
	}

	@Test
	void _05createRole() {
		EntityObject<UID> eo = new EntityObject(TestEntities.NUCLOS_ROLES)
		eo.setAttribute('name', testRoleName)

		assert !eo.id
		nuclosSession.save(eo)

		assert eo.id

	}


	@Test
	void _20checkPermissionsWithoutRoles() {

		expectErrorStatus(Response.Status.FORBIDDEN) {
			client2.login()
		}

		expectErrorStatus(Response.Status.UNAUTHORIZED) {
			client2.getEntityObjects(TestEntities.EXAMPLE_REST_AUFTRAG)
		}
	}


	@Test
	void _25assigneRolesAndRecheck() {
		assert grantOrRevokeRolesTestUsers(true) == 3

		client2.login()
		assert client2.sessionId

		List<EntityObject<Long>> eos = client2.getEntityObjects(TestEntities.EXAMPLE_REST_AUFTRAG)
		assert eos.empty
	}


	@Test
	void _30revokeRolesAndRecheck() {

		assert grantOrRevokeRolesTestUsers(false) == 3

		expectErrorStatus(Response.Status.UNAUTHORIZED) {
			client2.getEntityObjects(TestEntities.EXAMPLE_REST_AUFTRAG)
		}

	}

	@Test
	void _45cleanupUsers() {
		assert createOrDeleteTestUsers(false) == 3

	}

	@Test
	void _55createSimilarRoleNotPossible() {
		EntityObject<UID> eo = new EntityObject(TestEntities.NUCLOS_ROLES)
		eo.setAttribute('name', testRoleName + "()")

		assert !eo.id
		expectErrorStatus(Response.Status.PRECONDITION_FAILED) {
			nuclosSession.save(eo)
		}

		assert !eo.id

	}

	@Test
	void _60createSimilarRolePossible() {
		EntityObject<UID> eo = new EntityObject(TestEntities.NUCLOS_ROLES)
		eo.setAttribute('name', testRoleName + '(1)')

		assert !eo.id
		nuclosSession.save(eo)

		assert eo.id

		// Rename it
		eo.setAttribute('name', testRoleName + ' 2')
		nuclosSession.save(eo)

		//Try to rename to not a name that is not possible
		eo.setAttribute('name', testRoleName + '+')

		expectErrorStatus(Response.Status.PRECONDITION_FAILED) {
			nuclosSession.save(eo)
		}

	}

	@Test
	void _65createSimilarRoleNotPossible2() {
		EntityObject<UID> eo = new EntityObject(TestEntities.NUCLOS_ROLES)
		eo.setAttribute('name', testRoleName + '2')

		assert !eo.id
		expectErrorStatus(Response.Status.PRECONDITION_FAILED) {
			nuclosSession.save(eo)
		}

		assert !eo.id
	}

	final static String BASE_PATH = '/execute/example.rest.UserManagementRule/'
	final static JSONArray jsonArray = new JSONArray().put("TestUser1").put("TestUser2").put("TestUser3")
	final static JSONObject postDataUsers = new JSONObject().put('users', jsonArray)

	@Test
	void _90cleanUpRoles() {
		assert deleteTestRoles() == 2
	}

	static int grantOrRevokeRolesTestUsers(boolean grant) {
		if (grant) {
			HttpPost post = new HttpPost(RESTHelper.REST_BASE_URL + BASE_PATH + 'grantRoleToUser')
			post.setEntity(new StringEntity(postDataUsers.toString()))

			// TODO maxSqlCount has been 11, but then suddently needed to be 14
			return callCustomRestRule(post, 14, 3)
		}
		return callCustomRestRule('revokeRoleFromUser', HttpMethod.DELETE, postDataUsers.toString())
	}

	static int createOrDeleteTestUsers(boolean create) {
		if (create) {
			HttpPost post = new HttpPost(RESTHelper.REST_BASE_URL + BASE_PATH + 'createUsers')
			post.setEntity(new StringEntity(postDataUsers.toString()))

			return callCustomRestRule(post, 15, 3)
		}

		return callCustomRestRule('deleteUsers', HttpMethod.DELETE, postDataUsers.toString())
	}

	static int deleteTestRoles() {
		String input = '{"search": "' + testRoleName + '"}'
		return callCustomRestRule('deleteRoles', HttpMethod.DELETE, input)
	}

	static int callCustomRestRule(HttpRequestBase base, int maxSqlCount, int maxSqlWriteCount) {
		String s = RESTHelper.callCustomRestRuleWithSqlCountCheck(nuclosSession, base, maxSqlCount, maxSqlWriteCount)
		return Integer.valueOf(s)
	}

	static int callCustomRestRule(String method, HttpMethod httpMethod, String json) {
		String response = RESTHelper.requestString(BASE_PATH + method, httpMethod, nuclosSession.sessionId, json)
		return Integer.valueOf(response)
	}

}
