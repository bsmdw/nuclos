package org.nuclos.test.server


import org.junit.Ignore
import org.junit.Test
import org.nuclos.server.rest.services.helper.RecursiveDependency
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient

/**
 * Created by Sebastian Debring on 11/27/2018.
 */
class CloneTest extends AbstractNuclosTest {

	private static EntityObject<Long> parent
	private static List<EntityObject<Long>> subformData

	private static RecursiveDependency dependency

	static RESTClient testUser = new RESTClient('test', 'test')

	@Test
	void _00_setup() {
		parent = new EntityObject(TestEntities.NUCLET_TEST_SUBFORM_PARENT)
		parent.setAttribute('text', 'parent')

		List<EntityObject<Long>> subform = parent.getDependents(
				TestEntities.NUCLET_TEST_SUBFORM_SUBFORM,
				'parent'
		)

		3.times { subCount ->
			EntityObject subEo = new EntityObject(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM)
			subEo.setAttribute('text', subCount.toString())
			subform << subEo

			List<EntityObject<Long>> subsubform = subEo.getDependents(
					TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM,
					'subform'
			)
			3.times { subsubCount ->
				EntityObject subsubEo = new EntityObject(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM)
				subsubEo.setAttribute('text', "$subCount-$subsubCount".toString())
				subsubform << subsubEo

				List<EntityObject<Long>> subsubsubform = subsubEo.getDependents(
						TestEntities.NUCLET_TEST_SUBFORM_SUBSUBSUBFORM,
						'subsubform'
				)
				3.times { subsubsubCount ->
					EntityObject subsubsubEo = new EntityObject(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBSUBFORM)
					subsubsubEo.setAttribute('text', "$subCount-$subsubCount-$subsubsubCount".toString())
					subsubsubform << subsubsubEo
				}
			}
		}

		nuclosSession.save(parent)

		subformData = nuclosSession.loadDependents(
				parent,
				parent.entityClass,
				TestEntities.NUCLET_TEST_SUBFORM_SUBFORM.fqn + '_parent'
		)

		dependency = RecursiveDependency.forRoot(parent.id as String)
				.dependency(
				TestEntities.NUCLET_TEST_SUBFORM_SUBFORM.fqn + '_parent',
				subformData.first().id as String
		).dependency(
				TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM.fqn + '_subform'
		)
	}

	@Test
	void _01_cloneWithOutDependents() {
		EntityObject<Long> clonedEo = parent.clone();

		assert clonedEo.getAttribute('text') == parent.getAttribute('text')
		assert clonedEo.getAttribute('subforminserts').equals(null)
		assert clonedEo.getAttribute('subformupates').equals(null)
		assert clonedEo.getAttribute('subformdeletes').equals(null)
		assert clonedEo.getAttribute('subformnone').equals(null)
	}

	@Test
	void _02_cloneWithOutDependentsWithLayoutUid() {
		EntityObject<Long> clonedEo = parent.clone('HrYQmIZGWI8sSQrKWRxa');

		assert clonedEo.getAttribute('text') == parent.getAttribute('text')
		assert clonedEo.getAttribute('subforminserts') == parent.getAttribute('subforminserts')
		assert clonedEo.getAttribute('subformupdates').equals(null)
		assert clonedEo.getAttribute('subformdeletes') == parent.getAttribute('subformdeletes')
		assert clonedEo.getAttribute('subformnone').equals(null)
	}

	@Test
	void _03_cloneWithOutDependentsWithLayoutFqn() {
		EntityObject<Long> clonedEo = parent.clone('nuclet_test_subform_ParentFQNLO');

		assert clonedEo.getAttribute('text') == parent.getAttribute('text')
		assert clonedEo.getAttribute('subforminserts').equals(null)
		assert clonedEo.getAttribute('subformupdates').equals(null)
		assert clonedEo.getAttribute('subformdeletes') == parent.getAttribute('subformdeletes')
		assert clonedEo.getAttribute('subformnone') == parent.getAttribute('subformnone')
	}

	//TODO extend with test for dependents cloning
}
