package org.nuclos.test.server

import javax.ws.rs.core.Response

import org.json.JSONArray
import org.json.JSONObject
import org.junit.AfterClass
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.common.UID
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.SystemEntities
import org.nuclos.test.rest.QueryOptions
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper
import org.springframework.http.HttpMethod

import groovy.json.JsonOutput
import groovy.transform.CompileStatic

/**
 * Tests the general login functionality.
 * Some security features regarding login are also tested in {@link org.nuclos.test.server.SecurityIpBlockTest}
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class LoginTest extends AbstractNuclosTest {

	final static String USER_UTILS_BASE_PATH = '/execute/nuclet.test.other.UserUtilsRule/'

	final static String NEW_API_USER_NAME = UID.generateString(28);
	final static String NEW_API_USER_EMAIL = NEW_API_USER_NAME + '@nuclos.de'
	final static String NEW_API_USER_EMAIL_2 = NEW_API_USER_NAME + '@novabit.de'

	final static RESTClient SUPERUSER_CLIENT = new RESTClient('nuclos', '').login()

	@AfterClass
	static void cleanup() {
		nuclosSession.stopMaintenance()

		List<EntityObject<String>> eos = nuclosSession.getEntityObjects(SystemEntities.MANDATOR)
		nuclosSession.deleteAll(eos)
	}

	@Test
	void _00_setup() {
		deleteUserIfExists('logintest')

		RESTHelper.createUser('logintest', 'logintest', ['Example user'], nuclosSession)
		RESTHelper.createUser('logintest2', 'logintest2', [], nuclosSession)
	}

	@Test
	void _10_unknownUser() {
		expectErrorStatus(Response.Status.UNAUTHORIZED) {
			new RESTClient('unknownuser', '').login()
		}
	}

	@Test
	void _20_knownUserWrongPassword() {
		EntityObject<String> user = getUser('logintest')
		assert !user.getAttribute('loginAttempts')
		assert !user.getAttribute('lastLogin')

		3.times {
			expectErrorStatus(Response.Status.UNAUTHORIZED) {
				new RESTClient('logintest', '').login()
			}

			user = getUser('logintest')
			assert user.getAttribute('loginAttempts') == it + 1
			assert !user.getAttribute('lastLogin')
		}
	}

	@Test
	void _30_correctLogin() {
		new RESTClient('logintest', 'logintest').login()

		EntityObject<String> user = getUser('logintest')
		assert !user.getAttribute('loginAttempts')
		assert user.getAttribute('lastLogin')
	}

	@Test
	void _40_correctLoginNoRestApi() {
		expectErrorStatus(Response.Status.FORBIDDEN) {
			new RESTClient('logintest2', 'logintest2').login()
		}
	}

	@Test
	void _50_newUserViaRuleApi() {
		Map userData = [
				email      : NEW_API_USER_EMAIL,
				firstname  : 'userBy',
				lastname   : 'RuleApi',
				username   : NEW_API_USER_NAME,
				password   : 'password'
		]
		JSONObject response = RESTHelper.postJson(USER_UTILS_BASE_PATH + 'postNewEmailLoginSuperuser', JsonOutput.toJson(userData), SUPERUSER_CLIENT)
		assert response.get("status") == 'user-ok'
	}

	@Test
	void _51_testLoginWithEmail() {
		RESTClient client = new RESTClient(NEW_API_USER_EMAIL, 'password').login()
		String response = RESTHelper.requestString(USER_UTILS_BASE_PATH + 'getTestValueFromRule', HttpMethod.GET, client.sessionId)
		assert response == '1-2-3-4'
	}

	@Test
	void _52_testChangeOfEmailAndResetPassword() {
		Map userData = [
				email      : NEW_API_USER_EMAIL_2,
				username   : NEW_API_USER_NAME,
				password   : 'passwordX'
		]
		JSONObject response = RESTHelper.putJson(USER_UTILS_BASE_PATH + 'putChangeEmailAndResetPassword', JsonOutput.toJson(userData), SUPERUSER_CLIENT)
		assert response.get("status") == 'user-ok'
	}

	@Test
	void _53_testLiginWithEmailAfterModification() {
		RESTClient client = new RESTClient(NEW_API_USER_EMAIL_2, 'passwordX').login()
		String response = RESTHelper.requestString(USER_UTILS_BASE_PATH + 'getTestValueFromRule', HttpMethod.GET, client.sessionId)
		assert response == '1-2-3-4'
	}

	@Test
	void _80_loginWithMandators() {
		2.times {
			EntityObject eo = new EntityObject(SystemEntities.MANDATOR)
			eo.setAttribute('name', "Mandator ${it + 1}")
			nuclosSession.save(eo)
			return eo
		}

		RESTClient client = new RESTClient('nuclos', '')
		JSONObject loginResult = RESTHelper.login(client)

		// The login result must contain infos about the 2 mandators
		// (Next login step would be to choose a mandator)
		JSONArray mandators = loginResult.getJSONArray('mandators')
		assert mandators.size() == 2

		// Make sure a mandator has a "chooseMandator" link
		JSONObject chooseMandator = mandators.getJSONObject(0).getJSONObject('links').getJSONObject('chooseMandator')
		assert chooseMandator.has('href')
		assert chooseMandator.has('methods')
	}

	@Test
	void _90_maintenance() {
		nuclosSession.startMaintenance()

		assert nuclosSession.maintenance

		// A normal user should get a "service unavailable" error
		expectErrorStatus(Response.Status.SERVICE_UNAVAILABLE) {
			new RESTClient('test', 'test').login()
		}

		// Superuser should be able to login
		new RESTClient('nuclos', '').login()
	}

	private static void deleteUserIfExists(String username) {
		try {
			getUser(username).delete()
		} catch (Exception e) {
			// ignore
		}
	}

	private static EntityObject getUser(String username) {
		nuclosSession.getEntityObjects(
				SystemEntities.USER,
				new QueryOptions(where: "org_nuclos_system_User_username='$username'")
		).first()
	}

	private EntityObject getRole(String name) {
		nuclosSession.getEntityObjects(
				SystemEntities.ROLE,
				new QueryOptions(where: "org_nuclos_system_Role_name='$name'")
		).first()
	}
}
