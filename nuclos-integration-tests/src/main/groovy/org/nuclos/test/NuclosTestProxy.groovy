package org.nuclos.test

import org.apache.http.HttpHost
import org.nuclos.test.log.Log
import org.nuclos.test.rest.RestResponse
import org.nuclos.test.rest.request.RequestCounts
import org.nuclos.test.webclient.FailureHandler

import groovy.transform.CompileStatic
import io.netty.handler.codec.http.DefaultFullHttpResponse
import io.netty.handler.codec.http.HttpMethod
import io.netty.handler.codec.http.HttpRequest
import io.netty.handler.codec.http.HttpResponse
import io.netty.handler.codec.http.HttpResponseStatus
import io.netty.handler.codec.http.HttpVersion
import net.lightbody.bmp.BrowserMobProxyServer
import net.lightbody.bmp.core.har.Har
import net.lightbody.bmp.filters.RequestFilter
import net.lightbody.bmp.filters.ResponseFilter
import net.lightbody.bmp.filters.ResponseFilterAdapter
import net.lightbody.bmp.proxy.CaptureType
import net.lightbody.bmp.util.HttpMessageContents
import net.lightbody.bmp.util.HttpMessageInfo

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class NuclosTestProxy {

	private final NuclosTestContext context = NuclosTestContext.instance

	/**
	 * Whitelist for URIs which should never be filtered.
	 * I.e. a URI will not be filtered, if it contains any string from this list.
	 */
	private final List<String> uriWhitelist = ['http://127.0.0.1']

	protected BrowserMobProxyServer browserMobProxy

	NuclosTestProxy(
			final List<String> uriWhitelist = null
	) {
		this.uriWhitelist << context.nuclosServer
		if (uriWhitelist) {
			this.uriWhitelist.addAll(uriWhitelist)
		}
	}

	void start() {
		if (browserMobProxy && browserMobProxy.started) {
			return
		}

		InetAddress host = InetAddress.getByAddress([0, 0, 0, 0] as byte[])

		browserMobProxy = new BrowserMobProxyServer() {
			/**
			 * Overriding this because the default content length limit of 2097152 bytes is not enough.
			 */
			@Override
			void addResponseFilter(final ResponseFilter filter) {
				addLastHttpFilterFactory(
						new ResponseFilterAdapter.FilterSource(
								filter,
								100 * 1024 * 1024
						)
				)
			}
		}

		browserMobProxy.addRequestFilter(new RequestFilter() {
			@Override
			HttpResponse filterRequest(
					final HttpRequest httpRequest,
					final HttpMessageContents httpMessageContents,
					final HttpMessageInfo httpMessageInfo
			) {
				final String uri = httpMessageInfo.originalRequest.uri
				if (isFiltered(uri)) {
					println "FILTERED $uri"
					return new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.NO_CONTENT)
				}

				httpMessageInfo.originalRequest.headers().set('start-time', System.currentTimeMillis().toString())
				return null
			}
		})

		browserMobProxy.addResponseFilter(new ResponseFilter() {
			@Override
			void filterResponse(
					final HttpResponse response,
					final HttpMessageContents contents,
					final HttpMessageInfo messageInfo
			) {
				String requestLine = "$messageInfo.originalRequest.method $messageInfo.url - ${response.status.code()}"
				try {
					def duration
					Long startTime = messageInfo.originalRequest?.headers().get('start-time')?.toLong()
					if (startTime) {
						duration = System.currentTimeMillis() - startTime
					}
					requestLine += " - ${duration} ms"

					println requestLine

					if (messageInfo.getOriginalRequest().getMethod() == HttpMethod.GET
							// TODO: Fix and unignore the following URL
							&& !messageInfo.originalUrl.endsWith('/rest')
							&& !messageInfo.originalUrl.endsWith('/rest/meta/addonusages') // creates new session -> UPDATE T_AD_SESSIONS
							&& !messageInfo.originalUrl.endsWith('/rest/meta/sideviewmenuselector')	// Can cause Workspace INSERT...
							&& !messageInfo.originalUrl.contains('/rest/openapi')	// TODO: Problem occurs only on Jenkins - reason not yet clear
					) {
						RestResponse restResponse = new RestResponse(response)
						// GET calls are not allowed to change the server state!
						// The insert-update-delete count must be null (for non-Nuclos Requests) or 0.
						assert !restResponse.sqlInsertUpdateDeleteCount
					}
				} catch (Throwable t) {
					FailureHandler.fail("Request failed: $requestLine", t)
				}
			}
		})

		browserMobProxy.start(0, host)
		int port = browserMobProxy.getPort()

		println "Proxy server started on port: $port"

		Runtime.addShutdownHook {
			stop()
		}
	}

	boolean isFiltered(String uri) {
		return uri.contains('/sockjs-node/') || !uriWhitelist.any {
			uri.contains(it)
		}
	}

	void stop() {
		try {
			if (browserMobProxy?.started && !browserMobProxy?.stopped) {
				browserMobProxy.stop()
			}
		} catch (Exception e) {
			Log.warn 'Failed to stop proxy', e
		}
	}

	boolean isStarted() {
		if (browserMobProxy?.stopped) {
			return false
		}

		return browserMobProxy?.started
	}

	RequestCounts countRequests(Closure c) {
		Har har = getHar(c)
		new RequestCounts(har)
	}

	/**
	 * Gets the HAR with default capture settings.
	 * Does not capture response content.
	 *
	 * @param c
	 * @return
	 */
	Har getHar(Closure c) {
		browserMobProxy.newHar()

		Har har
		try {
			c()
		} finally {
			har = browserMobProxy.endHar()
		}

		return har
	}

	/**
	 * Gets the HAR using the given capture settings.
	 * Here you can capture response content.
	 *
	 * @param captureTypes
	 * @param c
	 * @return
	 */
	Har getHar(List<CaptureType> captureTypes, Closure c) {
		// Backup previous CaptureTypes
		EnumSet<CaptureType> previousTypes = browserMobProxy.getHarCaptureTypes()

		Har har
		try {
			browserMobProxy.setHarCaptureTypes(captureTypes.toSet())
			har = getHar(c)
		} finally {
			// Restore previous CaptureTypes
			browserMobProxy.setHarCaptureTypes(previousTypes)
		}

		return har
	}

	String getHostname() {
		browserMobProxy?.clientBindAddress?.hostName
	}

	int getPort() {
		browserMobProxy?.port
	}

	HttpHost getHttpHost() {
		new HttpHost(hostname, port)
	}
}
