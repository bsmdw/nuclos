package org.nuclos.test.webclient.pageobjects.search


import org.nuclos.test.webclient.NuclosWebElement

import groovy.transform.CompileStatic

@CompileStatic
class BooleanSearchComponent {
	final NuclosWebElement containerElement
	final NuclosWebElement yesElement
	final NuclosWebElement noElement

	BooleanSearchComponent(final NuclosWebElement containerElement) {
		this.containerElement = containerElement
		yesElement = getYesElement()
		noElement = getNoElement()
	}

	protected NuclosWebElement getYesElement() {
		containerElement.$('input[id=value-yes]')
	}

	protected NuclosWebElement getNoElement() {
		containerElement.$('input[id=value-no]')
	}

	public selectYes() {
		yesElement.click()
	}

	public selectNo() {
		noElement.click()
	}

	public isYes() {
		yesElement.selected
	}

	public isNo() {
		noElement.selected
	}
}
