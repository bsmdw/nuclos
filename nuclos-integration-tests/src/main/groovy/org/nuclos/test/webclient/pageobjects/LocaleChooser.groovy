package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.$
import static org.nuclos.test.webclient.AbstractWebclientTest.waitForAngularRequestsToFinish

import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic

/**
 * Represents the locale chooser component, which should be available on all Webclient pages.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class LocaleChooser extends AbstractPageObject {
	/**
	 * Returns the currently via the locale chooser component selected locale, e.g. "de".
	 * @return
	 */
	static String getLocale() {
		element?.getAttribute('title')
	}

	/**
	 * Sets the given locale for the tests and selects it in the Webclient
	 * via the locale chooser component.
	 *
	 * TODO: Do nothing if the correct locale is already selected
	 *
	 * @param locale
	 */
	static void setLocale(Locale locale) {
		element.click()
		waitForAngularRequestsToFinish()

		$(element, "a[title=$locale.language]").click()
		waitForAngularRequestsToFinish()
	}

	static WebElement getElement() {
		$('nuc-locale .locale-chooser')
	}
}
