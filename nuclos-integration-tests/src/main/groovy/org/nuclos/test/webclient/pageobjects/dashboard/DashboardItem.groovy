package org.nuclos.test.webclient.pageobjects.dashboard


import org.nuclos.test.webclient.pageobjects.AbstractPageObject

import groovy.transform.CompileStatic
import groovy.transform.Immutable

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
@Immutable
class DashboardItem extends AbstractPageObject {
	String type

	int x
	int y
	Integer rows
	Integer cols
}