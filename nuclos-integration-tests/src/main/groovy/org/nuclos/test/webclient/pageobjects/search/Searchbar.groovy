package org.nuclos.test.webclient.pageobjects.search

import static org.nuclos.test.webclient.AbstractWebclientTest.*

import java.util.stream.Collectors

import org.nuclos.test.EntityClass
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.*
import org.openqa.selenium.By
import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class Searchbar {

	static void clear() {
		boolean searchEditorWasClosedBefore = false
		NuclosWebElement clearSearchFilter = getClearSearchFilter()
		if (clearSearchFilter == null) {
			searchEditorWasClosedBefore = true
			openSearchEditor()
			clearSearchFilter = getClearSearchFilter()
		}
		if (clearSearchFilter != null) {
			clearSearchFilter.click()
			if (searchEditorWasClosedBefore) {
				closeSearchEditor()
			}
		}
	}

	private static NuclosWebElement getClearSearchFilter() {
		return $('#searchfilter-clear');
	}

	static void saveAsSearchfilter() {
		$('#meta-edit').click()
		$('#meta-editor-make-temp-persistent').click()
	}

	static void editName(String name) {
		$('#meta-edit').click()
		$('#meta-editor-name-input').value = name
	}

	static void setFavorite(String icon, Integer position) {
		$('#meta-edit').click()
		$('#meta-editor-show-as-favorite').click()
		if (icon) {
			$('.icon-picker').click()
			$('#icon_' + icon).click()
		}
		if (position) {
			$('#meta-editor-position-input').click()
			sendKeys(position + '')
		}
	}

	static void selectFavorite(String name) {
		$('.searchfilter-favorite-button-' +
		name.toLowerCase().replaceAll('[^a-z0-9-_]', '-')).click();
	}

	static void done() {
		$('#done-button').click()
	}

	static void edit() {
		$('#meta-edit').click()
	}

	static void save() {
		$('#meta-editor-save-changes').click()
	}

	static void undo() {
		$('#meta-editor-undo-changes').click()
	}

	static void resetCustomization() {
		$('#meta-editor-reset-customization').click()
	}

	static void delete() {
		$('#meta-edit').click()
		$('#meta-editor-delete-searchfilter').click()
		EntityObjectComponent.clickButtonOk()
	}

	static boolean isDeleteAllowed() {
		NuclosWebElement deleteButton = $('#meta-editor-delete-searchfilter')
		return deleteButton && deleteButton.isDisplayed() && deleteButton.isEnabled()
	}

	static String getSelectedFilter() {
		searchfilterSelector.selectedEntry
	}

	static void selectSearchfilter(String name) {
		searchfilterSelector.selectEntry(name)
		// Searcheditor is closed after selection, reopen it
		openSearchEditor()
	}

	static void selectAttribute(String attribute) {
		attributeLov.selectEntry(attribute)
	}

	static List<String> getFilters() {
		return searchfilterSelector.entries;
	}

	static List<String> getFavorites() {
		return $$('.searchfilter-favorite-button').stream().map { item ->
			def icon = ((NuclosWebElement)item).findElement(By.className('material-icons'))
			def iconName = icon ? icon.getText() : ''
			return ((NuclosWebElement)item).getText().replace(iconName, '').trim()
		}.collect(Collectors.toList())
	}

	/**
	 * TODO: Select the corresponding automatically when setting a search condition.
	 */
	static def setCondition(final EntityClass<?> entityClass, final Searchtemplate.SearchTemplateItem item) {
		Searchtemplate.setSearchCondition(entityClass.fqn + '_' + item.name, item)
	}

	static toggleItemSelection(final EntityClass<?> entityClass, final String itemName) {
		def fqn = entityClass.fqn + '_' + itemName
		$('#attribute-label-' + fqn).mouseover()
		$('#search-item-checkbox-' + fqn).click()
		// driver.findElement(By.id('search-item-checkbox-' + entityClass.fqn + '_' + itemName)).click()
		waitForAngularRequestsToFinish(2000)
	}

	static SearchfilterDropdown getSearchfilterSelector() {
		openSearchEditor()
		new SearchfilterDropdown()
	}

	static openSearchEditor() {
		if ($('#search-editor') == null) {
			$('.search-editor-button').click()
		}
	}

	static closeSearchEditor() {
		if ($('#search-editor') != null) {
			$('.search-editor-button').click()
		}
	}

	static ListOfValues getAttributeLov() {
		ListOfValues.fromElement(
				null,
				$('#nuc-search-attribute-selector-dropdown')
		)
	}

	static ListOfValues getSubformLov() {
		ListOfValues.fromElement(
				null,
				$('#nuc-search-attribute-selector-subform-dropdown')
		)
	}

	static void removeAttribute(final String fqn) {
		openSearchEditor()
		$('#attribute-label-' + fqn).mouseover()
		$('#search-remove-' + fqn).click()
	}

	static WebElement getSearchfilter() {
		$('#text-search-input')
	}

	static void search(String searchString) {
		clearTextSearchfilter()
		searchfilter.sendKeys(searchString)

		waitForAngularRequestsToFinish()
	}

	static void clearTextSearchfilter() {
		if ($('.clear-searchfilter') != null) {
			$('.clear-searchfilter').click()
		}
	}

	static ListOfValues getValueLov(final EntityClass<?> entityClass, final String attributeName) {
		def attributeFqn = entityClass.fqn + '_' + attributeName
		NuclosWebElement element = new NuclosWebElement(driver.findElement(By.id('search-value-lov-' + attributeFqn)))
		ListOfValues.fromElement(attributeFqn, element)
	}

	static Datepicker getValueDatepicker(final EntityClass<?> entityClass, final String attributeName) {
		def attributeFqn = entityClass.fqn + '_' + attributeName
		NuclosWebElement element = new NuclosWebElement(driver.findElement(By.id('search-value-datepicker-' + attributeFqn)))
		new Datepicker(element)
	}

	static BooleanSearchComponent getValueBoolean(final EntityClass<?> entityClass, final String attributeName) {
		def attributeFqn = entityClass.fqn + '_' + attributeName
		NuclosWebElement element = new NuclosWebElement(driver.findElement(By.id('search-value-boolean-' + attributeFqn)))
		new BooleanSearchComponent(element)
	}

	static StringSearchComponent getValueString(final EntityClass<?> entityClass, final String attributeName) {
		def attributeFqn = entityClass.fqn + '_' + attributeName
		NuclosWebElement element = new NuclosWebElement(driver.findElement(By.id('search-value-string-' + attributeFqn)))
		new StringSearchComponent(element)
	}

	static void enableSubformSearch() {
		NuclosWebElement element = new NuclosWebElement(driver.findElement(By.id('nuc-search-enable-subform-button')))
		element.click()
	}
}
