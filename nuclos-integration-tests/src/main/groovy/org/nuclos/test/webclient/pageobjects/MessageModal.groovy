package org.nuclos.test.webclient.pageobjects

import org.nuclos.test.webclient.NuclosWebElement

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class MessageModal {
	NuclosWebElement element

	String title
	String message

	void confirm() {
		element.$('#button-ok').click()
	}

	void decline() {
		element.$('button.close').click()
	}
}
