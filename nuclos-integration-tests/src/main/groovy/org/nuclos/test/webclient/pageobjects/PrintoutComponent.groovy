package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.$
import static org.nuclos.test.webclient.AbstractWebclientTest.$$

import org.nuclos.test.webclient.NuclosWebElement

import groovy.transform.CompileStatic

/**
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class PrintoutComponent extends AbstractPageObject {
	static String containerSelector = '#activation-container'

	static void open() {
		$('nuc-printout button').click()
	}

	static void selectPrintout(int index) {
		$$('.output-format-checkbox')[index].click()
	}

	static void execute() {
		$('#execute-printout-button').click()
	}

	static NuclosWebElement getDownloadLink() {
		List<NuclosWebElement> links = $$('.printout-downoload-link')
		assert links.size() == 1

		links.first()
	}

	static void closeModal() {
		// TODO: Modal component should be used here (currently not possible because of inconsistent modals in the webclient)
		$('#button-cancel').click()
	}
}
