package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.$

import org.nuclos.test.webclient.NuclosWebElement
import org.openqa.selenium.support.ui.Select

import groovy.transform.CompileStatic

@CompileStatic
class Datepicker {
	final NuclosWebElement containerElement
	final NuclosWebElement inputElement
	final String componentId

	Datepicker(final NuclosWebElement containerElement) {
		this.containerElement = containerElement
		inputElement = getInputElement()
		componentId = getComponentId()
	}

	protected String getComponentId() {
		inputElement?.id
	}

	protected NuclosWebElement getInputElement() {
		containerElement.$('input')
	}

	/**
	 * Finds the actual NGB Datepicker panel.
	 * Only available after the datepicker was opened.
	 */
	NuclosWebElement getDatepicker() {
		return $("ngb-datepicker > span[for-id='$componentId']")?.parent
	}

	void open() {
		if (!open) {
			containerElement.click()
		}
	}

	void close() {
		if (open) {
			containerElement.click()
		}
	}

	boolean isOpen() {
		datepicker
	}

	void setInput(String value) {
		inputElement.click()
		inputElement.value = value
	}

	void focus() {
		inputElement.focus()
	}

	void blur() {
		inputElement.blur()
	}

	String getMonth() {
		NuclosWebElement select = datepicker.$$('select')[0]
		new Select(select.element).firstSelectedOption.text.trim()
	}

	int getYear() {
		NuclosWebElement select = datepicker.$$('select')[1]
		new Select(select.element).firstSelectedOption.text.trim().toInteger()
	}

	void clickDayOfCurrentMonth(int day) {
		for (NuclosWebElement dayElement : allDays) {
			if (!dayElement.hasClass('outside') && dayElement.text.trim().toInteger() == day) {
				dayElement.click()
				break
			}
		}
	}

	private List<NuclosWebElement> getAllDays() {
		datepicker.$$('.custom-day')
	}

	void commit() {
		blur()
	}

	private List<NuclosWebElement> getArrowButtons() {
		datepicker.$$('.ngb-dp-arrow')
	}

	def previousMonth() {
		arrowButtons.first().click()
	}

	def nextMonth() {
		arrowButtons.get(1).click()
	}

	String getInput() {
		inputElement.value
	}
}
