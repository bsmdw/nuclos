package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.$

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class Modal {
	protected static final String modalSelector = 'nuc-modal'

	static String getTitle() {
		$(modalSelector + ' .modal-title').text
	}

	static String getBody() {
		$(modalSelector + ' .modal-body').text
	}

	static void close() {
		$(modalSelector + ' button.close').click()
	}

	static boolean isVisible() {
		$(modalSelector)?.displayed
	}
}
