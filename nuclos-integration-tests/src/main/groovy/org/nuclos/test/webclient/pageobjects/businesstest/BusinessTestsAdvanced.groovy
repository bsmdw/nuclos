package org.nuclos.test.webclient.pageobjects.businesstest

import static org.nuclos.test.webclient.AbstractWebclientTest.$
import static org.nuclos.test.webclient.AbstractWebclientTest.$$
import static org.nuclos.test.webclient.AbstractWebclientTest.getUrlHash
import static org.nuclos.test.webclient.AbstractWebclientTest.waitFor
import static org.nuclos.test.webclient.AbstractWebclientTest.waitForAngularRequestsToFinish

import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.AbstractPageObject

import groovy.transform.CompileStatic

/**
 * Represents the business test overview page for advanced users ("Expertenmodus").
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class BusinessTestsAdvanced extends AbstractPageObject {
	static void open() {
		getUrlHash('#/businesstests/advanced')
	}

	static boolean isStarted() {
		!$('#generateAllTests').enabled
	}

	static void generateAndRun() {
		$('#generateAndRun').click()
		waitForAngularRequestsToFinish(120)
		waitFor(120) {
			!BusinessTestsAdvanced.started
		}
	}

	static void generateAllTests() {
		$('#generateAllTests').click()
		waitForAngularRequestsToFinish(120)
		waitFor(120) {
			!BusinessTestsAdvanced.started
		}
	}

	static void executeAllTests() {
		$('#executeAllTests').click()
		waitForAngularRequestsToFinish(120)
		waitFor(120) {
			!BusinessTestsAdvanced.started
		}
	}

	static void deleteAllTests() {
		$('#deleteAllTests').click()
		waitForAngularRequestsToFinish()
		clickButtonOk()
		waitForAngularRequestsToFinish()
	}

	static void deleteTest(def id) {
		$('#delete_' + id).click()
		waitForAngularRequestsToFinish()
	}

	static List<NuclosWebElement> getTests() {
		$$('tr[id^=test_]').findAll { it.displayed }
	}

	static String getLog() {
		$('#output').text.trim()
	}

	static int getTestsTotal() {
		$('#overview_testsTotal').text.toInteger()
	}

	static String getState() {
		$('.overview-state').text
	}

	static int getTestsGreen() {
		$('#overview_testsGreen').text.toInteger()
	}

	static int getTestsYellow() {
		$('#overview_testsYellow').text.toInteger()
	}

	static int getTestsRed() {
		$('#overview_testsRed').text.toInteger()
	}

	static int getDuration() {
		$('#overview_duration').text.replace(' ms', '').toInteger()
	}
}