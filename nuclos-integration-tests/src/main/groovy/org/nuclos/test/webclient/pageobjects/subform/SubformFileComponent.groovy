package org.nuclos.test.webclient.pageobjects.subform

import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.FileComponent

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class SubformFileComponent extends FileComponent {
	SubformFileComponent(final NuclosWebElement element) {
		super(element)
	}

	private boolean isEditing() {
		element.$('input') as boolean
	}

	private edit() {
		if (!editing) {
			element.click()
		}
	}

	@Override
	void setFile(final File file) {
		edit()
		super.setFile(file)
	}

	@Override
	String getImageUrl() {
		edit()
		return super.getImageUrl()
	}

	@Override
	boolean hasImage() {
		edit()
		return super.hasImage()
	}

	@Override
	NuclosWebElement getFileuploadContainer() {
		element.$('.fileupload-container')
	}
}
