package org.nuclos.test.webclient.util

import java.text.DecimalFormat
import java.text.NumberFormat

import org.nuclos.test.webclient.AbstractWebclientTest

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class ValueFormatter {

	static String formatNumber(final Number number) {
		DecimalFormat df = (DecimalFormat) NumberFormat.getInstance(AbstractWebclientTest.context.locale)
		return df.format(number)
	}
}
