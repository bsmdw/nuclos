package org.nuclos.test.rest.request

import java.util.regex.Pattern

import org.nuclos.test.rest.RestResponse

import groovy.transform.CompileStatic
import net.lightbody.bmp.core.har.Har
import net.lightbody.bmp.core.har.HarEntry

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class RequestCounts {
	private static final List<Pattern> BLACKLIST = [
			Pattern.compile('^OPTIONS .*'),
			Pattern.compile('^\\w+ .*\\.gvt1\\.com/.*'),    // Used by Chrome for updates
	]

	private Map<RequestType, RequestCount> countsByType = new HashMap<>()

	RequestCounts(Har har) {
		har.log.entries.each { entry ->
			String request = entryToString(entry)

			int sqlCount = entry.response.headers.find {
				it.name == RestResponse.NUCLOS_SQL_COUNT
			}?.value?.toInteger() ?: 0

			long sqlTime = entry.response.headers.find {
				it.name == RestResponse.NUCLOS_SQL_TIME
			}?.value?.toLong() ?: 0

			increaseCount(request, sqlCount, sqlTime)
		}
	}

	static boolean isBlacklisted(final String s) {
		BLACKLIST.find {
			it.matcher(s)
		}
	}

	/**
	 * Analyzes a list of requests in the form '<METHOD> <URL>'
	 *
	 * @param requests
	 */
	RequestCounts(List<String> requests) {
		this.countsByType.clear()

		requests.each { request ->
			increaseCount(request)
		}
	}

	private static String entryToString(HarEntry entry) {
		"$entry.request.method $entry.request.url".toString()
	}

	private Set<RequestType> findTypes(String methodAndUrl) {
		if (isBlacklisted(methodAndUrl)) {
			return [].toSet()
		}

		RequestType.values().findAll {
			it.matches(methodAndUrl)
		}.toSet()
	}

	private void increaseCount(
			String request
	) {
		increaseCount(request, 0, 0)
	}

	private void increaseCount(
			String request,
			int sqlCount,
			long sqlTime
	) {
		findTypes(request).each { type ->
			if (!countsByType.containsKey(type)) {
				countsByType.put(type, new RequestCount(type))
			}

			RequestCount count = countsByType[type]

			count.requestCount++
			count.sqlCount += sqlCount
			count.sqlTime += sqlTime
			count.requests << request
		}
	}

	int getRequestCount(RequestType type = RequestType.ALL) {
		return countsByType[type]?.requestCount ?: 0
	}

	int getSqlCount(RequestType type = RequestType.ALL) {
		return countsByType[type].sqlCount
	}

	long getSqlTime(RequestType type = RequestType.ALL) {
		return countsByType[type].sqlTime
	}

	private static class RequestCount {
		private final RequestType type

		int requestCount = 0
		int sqlCount = 0
		long sqlTime = 0
		List<String> requests = []

		RequestCount(final RequestType type) {
			this.type = type
		}
	}

	@Override
	String toString() {
		return "${getClass().simpleName} [\n" +
				countsByType.collect { key, value -> "\t$key=$value.requests" }.join('\n') +
				"\n]"
	}
}
