package org.nuclos.test.rest

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
enum ServerLogger {
	SQL_LOGGER('SQLLogger'),
	SQL_TIMER('SQLTimer'),
	SQL_UPDATE('SQLUpdate')

	private final String name

	ServerLogger(String name) {
		this.name = name
	}

	@Override
	String toString() {
		name
	}
}
