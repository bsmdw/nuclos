package org.nuclos.test.rest

import org.apache.logging.log4j.Level
import org.nuclos.test.EntityObject
import org.nuclos.test.SystemEntities
import org.nuclos.test.rest.service.BusinessTestService

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class SuperuserRESTClient extends RESTClient {

	SuperuserRESTClient(final String user, final String password) {
		super(user, password)
	}

	@Override
	SuperuserRESTClient login() {
		return (SuperuserRESTClient) super.login()
	}

	Map<String, String> getSimpleSystemParameters() {
		RESTHelper.getSimpleSystemParameters(this)
	}

	List<EntityObject<String>> getSystemParameters(
			SuperuserRESTClient client
	) {
		RESTHelper.getEntityObjects(SystemEntities.PARAMETER, client)
	}

	List<EntityObject<String>> setSystemParameters(
			Map<String, String> newParams
	) {
		RESTHelper.setSystemParameters(newParams, this)
	}

	List<EntityObject<String>> patchSystemParameters(
			Map<String, String> newParams
	) {
		RESTHelper.patchSystemParameters(newParams, this)
	}

	boolean deleteUserByEmail(
			String email
	) {
		RESTHelper.deleteUserByEmail(email, this)
	}

	String invalidateServerCaches() {
		RESTHelper.invalidateServerCaches(this)
	}

	String managementConsole(
			String command,
			String arguments = null
	) {
		RESTHelper.managementConsole(command, arguments, this)
	}

	void setServerLogLevel(ServerLogger logger, Level level) {
		RESTHelper.putServerLogLevel(this, logger, level)
	}

	BusinessTestService getBusinesstestService() {
		new BusinessTestService(this)
	}

	void startMaintenance() {
		RESTHelper.managementConsole('startMaintenance', null, this)
	}

	void stopMaintenance() {
		RESTHelper.managementConsole('stopMaintenance', null, this)
	}

	boolean isMaintenance() {
		RESTHelper.managementConsole('showMaintenance', null, this).startsWith('on')
	}
}
