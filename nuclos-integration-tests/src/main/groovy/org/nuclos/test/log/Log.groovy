package org.nuclos.test.log

import org.apache.logging.log4j.Level
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.codehaus.groovy.reflection.ReflectionUtils

/**
 * A dynamic Log Handler that tries to get a Logger for the calling class.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class Log {
	static void trace(def message) {
		logger?.trace(message)
	}

	static void debug(def message) {
		logger?.debug(message)
	}

	static void info(def message) {
		logger?.info(message)
	}

	static void warn(def message, Throwable t = null) {
		if (t) {
			logger?.warn(message, t)
		} else {
			logger?.warn(message)
		}
	}

	static void error(def message, Throwable t = null) {
		if (t) {
			logger?.error(message, t)
		} else {
			logger?.error(message)
		}
	}

	private static Logger getLogger() {
		try {
			final Logger log = LogManager.getLogger(ReflectionUtils.getCallingClass(1, ['org.nuclos.test.log']))
			Level logLevel = Level.INFO;
			if (System.getProperty('log')) {
				try {
					logLevel = Level.valueOf(System.getProperty('log').toUpperCase())
				}
				catch (Exception ex) {

				}
			}
			log.level = logLevel

			return log
		}
		catch (Exception ex) {
			println "Failed to get logger"
			ex.printStackTrace()
		}

		return null
	}
}
